import os
import cv2

import numpy as np

class BinarySegmentation(object):
    def __init__(self, 
                input_size, 
                class_names, 
                weights_path,
                use_fp16=False,
                confidence_th=0.3,
                channels=3):
        self.input_size = input_size
        self.class_names   = list(class_names)
        self.nb_class = len(self.class_names)
        self.weights_path = weights_path
        self.use_fp16 = use_fp16
        self.confidence_th = confidence_th
        self.channels = channels

        # Load swig lib
        self.swig_lib = self.load_swig()
        self.trt_runner = self.init_model(weights_path)

    def load_swig(self):
        import tensorrt_lib.bin._swig_yolo as swig_lib
        print("Loaded Binary Segmentation for tensorrt")
        return swig_lib

    def init_model(self, weights_path):
        print('Binary Segmentation TensorRT - loading model from %s' % weights_path)
        return self.swig_lib.loadModelAndCreateRunner(weights_path, self.channels, self.use_fp16)

    def infer(self, input_img, reverse_channels=True):
        output_size = self.input_size[0] * self.self.input_size[1]
        m1_p1_norm = False
        # TODO-support 1 image for debug purposes, currently supportting only batches of 2
        out = np.zeros(2 * output_size, np.float32)
        n_images = len(input_img)
        if n_images==1:
            self.swig_lib.execute_2_frames(self.trt_runner, input_img[0], input_img[0], out, m1_p1_norm, reverse_channels)
            return [out[:output_size]]
        else:
            self.swig_lib.execute_2_frames(self.trt_runner, input_img[0], input_img[1], out, m1_p1_norm, reverse_channels)
            return [out[:output_size], out[output_size:]]

    def predict(self,images, reverse_channels=True):
        if not isinstance(images, list):
            images = [images]
        H, W, _ = images[0].shape
        net_input = []
        for image in images:
            # TODO - Solve GPU resize issue for more than 4 channels 
            image = cv2.resize(image.copy(), (self.input_size, self.input_size))
            net_input.append(image)
            import ipdb; ipdb.set_trace(context=11)

        outputs = self.infer(net_input, reverse_channels=reverse_channels)
        all_boxes = []
        for out in outputs:
            if self.weights_path.endswith('.onnx'):
                out_tensor = out.reshape(self.nb_box, 5 + self.nb_class, 8, 8).transpose(2,3,0,1)
            else:
                out_tensor = out.reshape(8, 8, self.nb_box, 5 + self.nb_class)

            boxes  = decode_netout_vec(out_tensor, self.anchors, self.nb_class,
                                    obj_threshold=self.confidence_th,
                                    nms_threshold=self.nms_th)

            # Convert to image coordinates
            for b in boxes:
                b.renormalize(H,W)
            all_boxes.append(boxes)
        
        if len(all_boxes)==1:
            all_boxes = all_boxes[0]

        return all_boxes


# Sanity test
if __name__=='__main__':
    import cv2
    import yaml
    # path = os.path.join(main_dir, 'ourFirstCNN','detector','yolo',r'config_aldi.json')
    config_path = '/home/walkout05/mini_proj/segmentation/first_model/seg_unet_02_contx_64_00100/seg_unet_02_contx_64_00100_config.yaml'
    configSeg = yaml.safe_load(open(config_path, 'r'))
    weights_path = '/home/walkout05/mini_proj/segmentation/first_model/seg_unet_02_contx_64_00100/seg_unet_02_contx_64_00100.onnx'
    img_path = '/home/walkout05/mini_proj/segmentation/sample_img.jpg'
    img = cv2.imread(img_path)

    my_seg = BinarySegmentation(input_size=configSeg['img_size'],
                class_names=configSeg['class_names'],
                weights_path=weights_path)
    # Run detector
    img_mask = my_seg.predict(img)