import torch

def remove_prefix(state_dict, prefix):
    state_dict_out = dict()
    for k, v in state_dict.items():
        if k.startswith(prefix):
            state_dict_out[k.replace(prefix,'')] = v
        else:
            state_dict_out[k] = v
    return state_dict_out

class UnetSegmentation(torch.nn.Module):
    def __init__(self, n_class, encoder_arch='mobilenet_v2', 
                input_channels=3, encoder_depth=3, 
                decoder_channels=(256, 128, 64),
                preproc_img=True):
        torch.nn.Module.__init__(self)
        import segmentation_models_pytorch as smp
        from segmentation_models_pytorch.encoders import get_preprocessing_fn
        assert encoder_arch in ('resnet18', 'resnet34','mobilenet_v2')
        assert encoder_depth in (3,4,5)
        assert len(decoder_channels)==encoder_depth
        self.encoder_arch = encoder_arch
        self.input_channels = input_channels
        self.n_class = n_class
        self.model = smp.Unet(encoder_name=encoder_arch,
                              encoder_weights="imagenet",
                              in_channels=input_channels,
                              classes=n_class,)
        self.preproc_img = preproc_img
        if self.preproc_img:
            self.preproc_fn = get_preprocessing_fn(encoder_arch, pretrained='imagenet')

    def forward(self, p_in):
        if self.preproc_img:
            if isinstance(p_in, torch.Tensor):
                p_in = p_in.data.cpu().numpy().transpose(0,2,3,1)
            p_in = self.preproc_fn(p_in).transpose(0,3,1,2)
            p_in = torch.from_numpy(p_in).cuda().float()
        else:
            p_in = p_in.cuda()
        out = self.model(p_in)
        return out

    def load_weights(self, state_dict):
        state_dict = remove_prefix(state_dict, 'module.')
        self.load_state_dict(state_dict)
        self.model.cuda()