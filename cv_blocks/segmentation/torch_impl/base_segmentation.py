import os
import cv2

import numpy as np
import torch
from cv_blocks.segmentation.torch_impl.models import UnetSegmentation


class BinarySegmentation(object):
    def __init__(self, 
                input_size, 
                class_names, 
                weights_path,
                use_fp16=False,
                confidence_th=0.3,
                channels=3):
        self.input_size = tuple(input_size)
        self.class_names   = list(class_names)
        self.nb_class = len(self.class_names)
        self.weights_path = weights_path
        self.use_fp16 = use_fp16
        self.confidence_th = confidence_th
        self.channels = channels

        # Load model
        checkpoint = torch.load(weights_path)
        self.model = UnetSegmentation(self.nb_class)
        self.model.load_weights(checkpoint['model_state_dict'])
        self.model.eval()


    def predict(self,image):
        # resize image
        orig_shape = image.shape[:2][::-1]
        image = cv2.resize(image, self.input_size)
        image = image[:,:,::-1] # BGR->RGB
        image_4d = image.reshape(1, self.input_size[0], self.input_size[1], 3)
        # Run model
        seg_logits = self.model.forward(image_4d)
        p_fg_torch = torch.functional.F.softmax(seg_logits, dim=1).squeeze()[1]
        p_fg_np = p_fg_torch.data.cpu().numpy().astype(np.float32)

        # resize back to original size
        p_fg_np = cv2.resize(p_fg_np, orig_shape)

        return p_fg_np
    
    def fg_mask(self, image):
        p_fg = self.predict(image)
        fg_mask = p_fg > self.confidence_th
        return fg_mask




# Sanity test
if __name__=='__main__':
    import cv2
    import yaml
    # path = os.path.join(main_dir, 'ourFirstCNN','detector','yolo',r'config_aldi.json')
    config_path = '/home/walkout05/mini_proj/segmentation/first_model/seg_unet_02_contx_64_00100/seg_unet_02_contx_64_00100_config.yaml'
    configSeg = yaml.safe_load(open(config_path, 'r'))
    weights_path = '/home/walkout05/mini_proj/segmentation/first_model/seg_unet_02_contx_64_00100.pth'
    img_path = '/home/walkout05/mini_proj/segmentation/sample_img.jpg'
    img = cv2.imread(img_path)

    my_seg = BinarySegmentation(input_size=configSeg['img_size'],
                class_names=configSeg['class_names'],
                weights_path=weights_path)
    # Run detector
    img_mask = my_seg.fg_mask(img)

    # visualize
    viz_img_masked = img.copy()
    viz_img_masked[~img_mask] = 0
    viz_both = np.hstack((img, viz_img_masked))
    cv2.imshow('Sample Image', viz_both)
    cv2.waitKey()