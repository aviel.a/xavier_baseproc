import argparse
import getpass
import os
import pandas as pd
import numpy as np
import cv2
from cv_blocks.misc.logger import BaseLogger
from cv_blocks.misc.aws_download import find_and_download
from tqdm import tqdm
import imutils
import yaml
from cv_blocks.common.types import RotatedBoundingBox2D

def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            path = os.path.join(root, movie_name)
            if os.path.isfile(path) or os.path.isdir(path):
                return root

    print('no movies found in next paths :')
    for optional_dir in optional_dirs:
        print(os.path.join(optional_dir, movie_name))

def get_recording_cache(cache_file, root_dir):
    cache_dir = locate_movie(cache_file, root_dir)
    if cache_dir is None:
        cache_dir = find_and_download(cache_file, 'regression_data', root_dir)
    cache_full_path = os.path.join(cache_dir, cache_file)
    
    return cache_full_path


DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='product classifier benchmark')
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--movies-file', help='path to csv file containing files to run',type=str, required=True)
    parser.add_argument('--model-path', help='path to segmentation model',type=str, required=True)
    parser.add_argument('--config-file', help='path to segmentation model',type=str, required=True)
    parser.add_argument('--output-dir', help='directory to output modified files',type=str, required=True)
    parser.add_argument('--confidence-th', help='confidence level',type=float, default=0.3)
    parser.add_argument('--align', help='align image according to segmentation mask', action='store_true')
    parser.add_argument('--debug', help='debug flow', action='store_true')
    parser.add_argument('--mask-type', help='type of segmentation mask',type=str, default='wnoise')

    args = parser.parse_args()

    assert args.mask_type in ('wnoise', 'zero')
    # Build segmentation network
    # ==========================
    if args.model_path.endswith('.pth'):
        from cv_blocks.segmentation.torch_impl.base_segmentation import BinarySegmentation
    else:
        raise NotImplementedError
    print('Initializing Segmentation network')
    configSeg = yaml.safe_load(open(args.config_file, 'r'))
    seg_net = BinarySegmentation(input_size=configSeg['img_size'],
                                 class_names=configSeg['class_names'],
                                 weights_path=args.model_path,
                                 confidence_th=args.confidence_th)

    # Load list of recordings to evaluate
    rec_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()

    log_reader = BaseLogger(True)
    log_writer = BaseLogger(True)
    os.makedirs(args.output_dir, exist_ok=True)
    print('Converting capsules')
    for rec_file in tqdm(rec_list):
        capsule_path = get_recording_cache(rec_file, args.root_dir)
        # Parse cache file
        # ================
        el_list = log_reader.from_data(capsule_path)
        capsule_out_path = capsule_path.replace(args.root_dir, args.output_dir)
        log_writer.open_custom(capsule_out_path)
        for el_idx, _ in enumerate(el_list):
            if el_list[el_idx]['type']=='event_aggregator_out':
                mv_data = el_list[el_idx]['element'].data['movement_vector']
                for mv_idx in range(len(mv_data)):
                    cam_type = mv_data[mv_idx].cam_name
                    
                    # Convert image
                    img_type_in = 'context' if mv_data[mv_idx].context_image is not None \
                        else 'cropped'
                    img_type_out = 'cropped'
                    orig_is_rgb = True
                    if img_type_in=='context':
                        orig_input_img = mv_data[mv_idx].context_image.copy()
                    elif img_type_in=='cropped':
                        orig_input_img = mv_data[mv_idx].cropped_image.copy()
                    
                    # Run segmentation network
                    if orig_is_rgb:
                        input_img = orig_input_img[:,:,::-1] # RGB->BGR
                    else:
                        input_img = orig_input_img
                    h, w = input_img.shape[:2]
                    img_mask = seg_net.fg_mask(input_img)
                    if args.mask_type=='wnoise':
                        mask_img =(np.random.rand(h,w,3) * 255).astype(np.uint8)
                    elif args.mask_type=='zero':
                        mask_img =np.zeros((h,w,3)).astype(np.uint8)
                    viz_img_masked = input_img.copy()
                    viz_img_masked[~img_mask] = mask_img[~img_mask]
                    if orig_is_rgb:
                        viz_img_masked = viz_img_masked[:,:,::-1] # RGB->BGR
                    # Rotate image
                    if args.align:
                        cnts = cv2.findContours(img_mask.astype(np.uint8), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                        cnts = imutils.grab_contours(cnts)
                        if len(cnts)>0:
                            obj_cnt = max(cnts, key=cv2.contourArea)
                            obj_cnt_conv = cv2.convexHull(obj_cnt)
                            rect = RotatedBoundingBox2D.from_poly(obj_cnt_conv) 
                            viz_img_masked_rotated = rect.warp(viz_img_masked)
                        else:
                            viz_img_masked_rotated = viz_img_masked
                        if args.debug:
                            debug_viz = viz_img_masked_rotated.copy()
                            if orig_is_rgb:
                                debug_viz = debug_viz[:,:,::-1] # RGB->BGR
                            cv2.imshow('Capsule Rotated img %s,%s' % (cam_type, img_type_in) , debug_viz)
                            cv2.waitKey()
                        viz_img_masked = viz_img_masked_rotated

                    if args.debug:
                        debug_viz = np.hstack((orig_input_img, viz_img_masked))
                        if orig_is_rgb:
                            debug_viz = debug_viz[:,:,::-1] # RGB->BGR
                        cv2.imshow('Capsule img %s,%s' % (cam_type, img_type_in) , debug_viz)
                        cv2.waitKey()
                        cv2.destroyAllWindows()
                    
                    # Write image back
                    if img_type_out=='context':
                        mv_data[mv_idx].context_image = viz_img_masked
                    elif img_type_out=='cropped':
                        mv_data[mv_idx].cropped_image = viz_img_masked
                    
                # Write to capsule
                el_list[el_idx]['element'].data['movement_vector'] = [v.pack() for v in mv_data]

            log_writer.log(el_list[el_idx]['element'].data, el_list[el_idx]['type'], frame=el_list[el_idx]['frame'])
        log_writer.close()
                    