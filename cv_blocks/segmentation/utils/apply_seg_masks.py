import os
import argparse
import yaml
import cv2
import numpy as np

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='product classifier benchmark')
    parser.add_argument('--model-path', help='path to segmentation model',type=str, required=True)
    parser.add_argument('--config-file', help='path to segmentation model',type=str, required=True)
    parser.add_argument('--backend', help='type of backend to run (torch|trt)',type=str, required=True)
    parser.add_argument('--input-dir', help='path to directory of which we want to run',type=str, required=True)
    parser.add_argument('--output-dir', help='path to directory of which we want to run',type=str, required=True)
    parser.add_argument('--output-type', help='what type of output is required',type=str, default='wnoise-img')
    parser.add_argument('--valid-subdirs', help='allowed subdirs',type=str, default='front_rgb_crop,back_rgb_crop,cart_bottom_rgb_crop')
    parser.add_argument('--confidence-th', help='confidence level',type=float, default=0.3)

    args = parser.parse_args()
    valid_subdirs = args.valid_subdirs.split(',')
    assert args.backend in ('torch', 'trt')
    assert args.output_type in ('wnoise-img', 'black-img', 'binary-mask')
    assert os.path.exists(args.model_path)
    assert os.path.exists(args.config_file)
    if args.backend=='torch':
        from cv_blocks.segmentation.torch_impl.base_segmentation import BinarySegmentation
        assert args.model_path.endswith('.pth')
    elif args.backend=='trt':
        raise NotImplementedError
        # from cv_blocks.segmentation.trt_impl.base_segmentation import BinarySegmentation
        # assert args.model_path.endswith('.onnx')
    configSeg = yaml.safe_load(open(args.config_file, 'r'))

    # Initialize Segmentation network
    seg_net = BinarySegmentation(input_size=configSeg['img_size'],
                                 class_names=configSeg['class_names'],
                                 weights_path=args.model_path,
                                 confidence_th=args.confidence_th)
    
    # Run over images
    for root, dirs, files in os.walk(args.input_dir):
        for fname in files:
            if fname.lower().endswith(('.jpg', 'jpeg', 'png')):
                sub_dir = root.split(args.input_dir)[-1]
                if sub_dir.startswith('/'):
                    sub_dir = sub_dir[1:]
                if os.path.basename(sub_dir) not in valid_subdirs:
                    break
                output_sub_dir = os.path.join(args.output_dir, sub_dir)
                os.makedirs(output_sub_dir, exist_ok=True)
                input_file = os.path.join(root, fname)
                output_file = os.path.join(output_sub_dir, fname)
                # Run network on single image
                # ===========================
                img = cv2.imread(input_file)
                h, w = img.shape[:2]
                img_mask = seg_net.fg_mask(img)
                if args.output_type=='wnoise-img':
                    wnoise_img =(np.random.rand(h,w,3) * 255).astype(np.uint8)
                    viz_img_masked = img.copy()
                    viz_img_masked[~img_mask] = wnoise_img[~img_mask]
                elif args.output_type=='black-img':
                    viz_img_masked = img.copy()
                    viz_img_masked[~img_mask] = 0
                # Save image
                cv2.imwrite(output_file, viz_img_masked)