import os
import cv2

import numpy as np
import torch
from cv_blocks.object_detector.utils import decode_multi_att_vec
from .backbones import load_backbone
from cv_blocks.object_detector.yolo_detector.torch_impl.yolo import YoloObjectDetector
import misc


class MultiAttYoloObjectDetector(YoloObjectDetector):
    def __init__(self, backbone,
                       input_size, 
                       attributes, 
                       anchors,
                       weights_path=None,
                       use_fp16=False,
                       confidence_th=0.3,
                       nms_th=0.3,
                       channels=3):
        self.input_size = input_size
        self.attributes = attributes
        self.att_metadata = self.get_att_indices(attributes)
        self.tot_att_size = sum([len(aa) for a in attributes for aa in a.values()])
        self.nb_box   = len(anchors) // 2
        self.anchors  = anchors
        self.weights_path = weights_path
        self.use_fp16 = use_fp16
        self.confidence_th = confidence_th
        self.nms_th = nms_th
        self.channels = channels
        self.grid_size = 8 # TODO: read from config

        # Load backbone
        self.backbone = load_backbone(backbone)(self.attributes,
                                                n_box=self.nb_box,
                                                channels=self.channels)
        self.model = self.backbone # TODO: change

        # Load weights
        assert os.path.exists(weights_path)
        print('Multi-Att YOLO pytorch - loading weights from %s' % weights_path)
        checkpoint = torch.load(weights_path)
        self.model.load_state_dict(checkpoint['model_state_dict'], strict=False)
        self.model.eval()

    def get_att_indices(self, attributes):
        att_metadata = []
        att_idx = 0
        for single_att in attributes:
            att_vals = list(single_att.values())[0]
            att_name = list(single_att.keys())[0]
            att_data = dict(start_idx=att_idx, end_idx=att_idx + len(att_vals), name=att_name, vals=att_vals)
            att_metadata.append(att_data)
            att_idx += len(att_vals)
        return att_metadata

    def predict(self, image, reverse_channels=True):
        H, W, _ = image.shape
        image = cv2.resize(image, (self.input_size, self.input_size))
        if reverse_channels:
            image = image[:,:,::-1].copy()
        image = torch.from_numpy(image).permute(2,0,1).unsqueeze(dim=0)
        
        image = self.backbone.normalize(image)
        out_tensor = self.model.forward(image)

        b_dim, box_dim, h_dim, w_dim = out_tensor.shape
        pred_dim = 4 + 1 + self.tot_att_size
        out_tensor = out_tensor.contiguous().permute(0,2,3,1).view(b_dim, h_dim, w_dim, box_dim // (pred_dim), pred_dim)
        out_tensor = out_tensor.data.cpu().numpy().squeeze()

        boxes  = decode_multi_att_vec(out_tensor, self.anchors, self.att_metadata,
                                obj_threshold=self.confidence_th,
                                nms_threshold=self.nms_th)

        # Convert to image coordinates
        for b in boxes:
            b.renormalize(H,W)

        return boxes