import os
import torch
import numpy as np
import cv2
from cv_blocks.object_detector.yolo_detector.torch_impl.backbones import load_backbone
from cv_blocks.object_detector.utils import decode_netout_vec

class YoloObjectDetector(object):
    def __init__(self, backbone,
                       input_size, 
                       labels, 
                       anchors,
                       weights_path=None,
                       confidence_th=0.3,
                       nms_th=0.3,
                       channels=3,
                       nb_class=None):
        self.input_size = input_size
        self.labels   = list(labels)
        self.nb_class = len(self.labels) if nb_class is None else nb_class
        self.nb_box   = len(anchors) // 2
        self.class_wt = np.ones(self.nb_class, dtype='float32')
        self.anchors  = anchors
        self.weights_path = weights_path
        self.confidence_th = confidence_th
        self.nms_th = nms_th
        self.channels = channels

        # Load backbone
        self.backbone = load_backbone(backbone)(n_class=self.nb_class,
                                                n_box=self.nb_box,
                                                channels=self.channels)
        self.model = self.backbone # TODO: change

        # Load weights
        assert os.path.exists(weights_path)
        print('YOLO pytorch - loading weights from %s' % weights_path)
        checkpoint = torch.load(weights_path)
        self.model.load_state_dict(checkpoint['model_state_dict'])
        self.model.eval()

    def predict(self, image, reverse_channels=True):
        H, W, _ = image.shape
        image = cv2.resize(image, (self.input_size, self.input_size))
        if reverse_channels:
            image = image[:,:,::-1].copy()
        image = torch.from_numpy(image).permute(2,0,1).unsqueeze(dim=0)
        
        image = self.backbone.normalize(image)
        out_tensor = self.model.forward(image)

        b_dim, box_dim, h_dim, w_dim = out_tensor.shape
        pred_dim = 4 + 1 + self.nb_class
        out_tensor = out_tensor.contiguous().permute(0,2,3,1).view(b_dim, h_dim, w_dim, box_dim // (pred_dim), pred_dim)
        out_tensor = out_tensor.data.cpu().numpy().squeeze()

        boxes  = decode_netout_vec(out_tensor, self.anchors, self.nb_class,
                                   obj_threshold=self.confidence_th,
                                   nms_threshold=self.nms_th)

        # Convert to image coordinates
        for b in boxes:
            b.renormalize(H,W)

        return boxes

# Sanity test
if __name__=='__main__':
    import cv2
    import json
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    main_dir = os.path.join(curr_dir, '..', '..', '..')
    path = os.path.join(curr_dir,'..', 'config', 'config_mobilenet.json')
    with open(path) as config_buffer:
        configYolo = json.load(config_buffer)
    weights_path = os.path.join(curr_dir, '..', '..', 'pretrained_models','yolo_mobilenet_aldi.pth')
    img_path = os.path.join(curr_dir, '..','..', 'data', 'sample_img.png')
    img = cv2.imread(img_path)

    my_yolo = YoloObjectDetector(configYolo['model']['backbone'],
                input_size=configYolo['model']['input_size'],
                labels=configYolo['model']['labels'],
                anchors=configYolo['model']['anchors'],
                weights_path=weights_path)
    # Run detector
    boxes = my_yolo.predict(img)