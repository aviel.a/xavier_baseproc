import torch
from torch import nn
# import torchvision.models as tvm
from .mobilenet import mobilenet_v2

class Conv2d(nn.Module):
    def __init__(self, n_in, n_out, kernel_size=3, stride=1,
                 padding=None, use_bn=True, activation='relu', groups=1):
        super(Conv2d, self).__init__()
        if padding is None:
            padding = kernel_size // 2
        self.conv = nn.Conv2d(n_in, n_out, kernel_size=kernel_size, stride=stride,
                              padding=padding, groups=groups)
        assert activation in ['relu', 'linear', 'relu6', 'leaky_relu']
        # Activation
        if activation=='relu':
            self.activation = nn.ReLU(inplace=True)
        elif activation=='relu6':
            self.activation = nn.ReLU6(inplace=True)
        elif activation=='leaky_relu':
            self.activation = nn.LeakyReLU(inplace=True)
        elif activation=='linear':
            self.activation = lambda x: x
        # Batch Norm
        if use_bn:
            self.bn = nn.BatchNorm2d(n_out)
        else:
            self.bn = lambda x: x

        # initialize weight -currently xavier normal
        nn.init.xavier_normal(self.conv.weight)
        self.conv.bias.data.fill_(0.0)

    def forward(self, x):
        out = self.activation(self.bn(self.conv(x)))
        return out

class Conv2dDwSep(nn.Module):
    def __init__(self, n_in, n_out, kernel_size=3, stride=1, depth_mult=1,
                 padding=None, use_bn=True, activation='relu'):
        super(Conv2dDwSep, self).__init__()
        self.depthwise = Conv2d(n_in, n_in * depth_mult, kernel_size=kernel_size,
                                stride=stride, padding=padding, groups=n_in,
                                use_bn=use_bn, activation=activation)
        self.pointwise = Conv2d(n_in * depth_mult, n_out, kernel_size=1,
                                use_bn=use_bn, activation=activation)

    def forward(self, x):
        out = self.depthwise(x)
        out = self.pointwise(out)
        return out

class YoloMobilenet(torch.nn.Module):
    def __init__(self, n_box=5):
        super(YoloMobilenet, self).__init__()
        self.n_box = n_box
        self.grid_size = None

        self.conv1 = Conv2d(3, 32, kernel_size=3, stride=2)
        self.conv2 = Conv2dDwSep(32, 64, kernel_size=3)

        self.conv3 = Conv2dDwSep(64, 128, kernel_size=3, stride=2)
        self.conv4 = Conv2dDwSep(128, 128, kernel_size=3)

        self.conv5 = Conv2dDwSep(128, 256, kernel_size=3, stride=2)
        self.conv6 = Conv2dDwSep(256, 256, kernel_size=3)

        self.conv7 = Conv2dDwSep(256, 512, kernel_size=3, stride=2)
        self.conv8 = Conv2dDwSep(512, 512, kernel_size=3)
        self.conv9 = Conv2dDwSep(512, 512, kernel_size=3)
        self.conv10 = Conv2dDwSep(512, 512, kernel_size=3)
        self.conv11 = Conv2dDwSep(512, 512, kernel_size=3)
        self.conv12 = Conv2dDwSep(512, 512, kernel_size=3)

        self.conv13 = Conv2dDwSep(512, 1024, kernel_size=3, stride=2)
        self.conv14 = Conv2dDwSep(1024, 1024, kernel_size=3)

        # YOLO Head
        n_feat_out = self.n_box * (4 + 1) # note we assume single object
        self.yolo = Conv2d(1024, n_feat_out, kernel_size=1, activation='linear')

    def forward(self, p_in):
        img_size = (p_in.size(2), p_in.size(3))
        x = self.conv1(p_in)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)
        x = self.conv6(x)
        x = self.conv7(x)
        x = self.conv8(x)
        x = self.conv9(x)
        x = self.conv10(x)
        x = self.conv11(x)
        x = self.conv12(x)
        x = self.conv13(x)
        x = self.conv14(x)
        # yolo
        out = self.yolo(x)

        return out

    def normalize(self, image):
        image = image.float() / 255.

        return image		

class YoloMobilenetV2(YoloMobilenet):
    def __init__(self, n_class=1, n_box=5, channels=3):
        super(YoloMobilenetV2, self).__init__(n_class)
        self.n_box = n_box
        self.n_class = n_class
        self.channels = channels
        self.grid_size = None
        self.model = mobilenet_v2(channels=channels)

        # YOLO Head
        mobilenet_feat_dim = self.model.features[-1][0].weight.shape[0]
        n_feat_out = self.n_box * (4 + 1 + n_class) # note we assume single object
        self.yolo = Conv2d(mobilenet_feat_dim, n_feat_out, kernel_size=1, activation='linear')

    def forward(self, p_in):
        x = self.model.features(p_in)
        out = self.yolo(x)

        return out

class DiffYoloMobilenetV2(YoloMobilenetV2):
    def __init__(self, n_class=2, n_box=5, 
                 channels=6, img_size=(256,256),
                 nms_th=0.2,
                 confidence_th=0.3):
        torch.nn.Module.__init__(self)

        self.n_box = n_box
        self.n_class = n_class
        self.img_size = img_size
        self.nms_th = nms_th
        self.confidence_th = confidence_th
        self.channels = channels
        self.model = mobilenet_v2(channels=channels)

        # YOLO Head
        mobilenet_feat_dim = self.model.features[-1][0].weight.shape[0]
        n_feat_out = self.n_box * (4 + 1 + n_class) # note we assume single object
        self.yolo = Conv2d(mobilenet_feat_dim, n_feat_out, kernel_size=1, activation='linear')

    def forward(self, p_in):
        x = self.model.features(p_in)
        out = self.yolo(x)

        return out

class YoloMobilenetV2MultiAtts(YoloMobilenet):
    def __init__(self, attributes, n_box=5,
                 channels=3, box_size=4):
        torch.nn.Module.__init__(self)

        self.n_box = n_box
        self.max_box = 10
        self.attributes = attributes
        self.n_attributes = len(self.attributes)
        self.tot_att_size = sum([len(aa) for a in attributes for aa in a.values()])
        self.model = mobilenet_v2(channels=channels, pretrained=True)

        # YOLO Head
        mobilenet_feat_dim = self.model.features[-1][0].weight.shape[0]
        n_feat_out = self.n_box * (box_size + 1 + self.tot_att_size)
        self.yolo = Conv2d(mobilenet_feat_dim, n_feat_out, kernel_size=1, activation='linear')


    def forward(self, p_in):
        x = self.model.features(p_in)
        out = self.yolo(x)

        return out

def load_backbone(backbone_name):
    if backbone_name == 'MobileNet':
        backbone = YoloMobilenet
    elif backbone_name == 'MobileNetV2':
        backbone = YoloMobilenetV2 # TODO!!
    elif backbone_name == 'DiffMobileNetV2':
        backbone = DiffYoloMobilenetV2 # TODO!!
    elif backbone_name == 'YoloMobilenetV2MultiAtts':
        backbone = YoloMobilenetV2MultiAtts
    else:
        raise Exception('Architecture not supported! Only support MobileNet at the moment!')
    return backbone