import os
import cv2

import numpy as np
from cv_blocks.object_detector.utils import decode_multi_att_vec
# from .backbones import load_backbone
from cv_blocks.object_detector.yolo_detector.trt_impl.yolo import YoloObjectDetector
import misc


class MultiAttYoloObjectDetector(YoloObjectDetector):
    def __init__(self, backbone,
                       input_size, 
                       attributes, 
                       anchors,
                       weights_path=None,
                       use_fp16=False,
                       confidence_th=0.5,
                       nms_th=0.3,
                       channels=3,
                       connect_hands=False,
                       predict_products_only=False):
        self.input_size = input_size
        self.predict_products_only = predict_products_only
        self.attributes = dict()
        for attribute in attributes:
            att_name = list(attribute.keys())[0]
            att_val = attribute[att_name]
            self.attributes[att_name] = att_val
        if 'hand' in self.attributes['crop_type'] and 'hand-holding-product' in self.attributes['crop_type']:
            self.inter_att_nms = dict(att='crop_type',
                                       vals=[self.attributes['crop_type'].index('hand'),
                                             self.attributes['crop_type'].index('hand-holding-product')],
                                       nms=0.3)
        else:
            self.inter_att_nms = None
        self.att_metadata = self.get_att_indices(attributes)
        self.tot_att_size = sum([len(aa) for a in attributes for aa in a.values()])
        self.nb_box   = len(anchors) // 2
        self.anchors  = anchors
        self.weights_path = weights_path
        self.use_fp16 = use_fp16
        self.confidence_th = confidence_th
        self.nms_th = nms_th
        self.channels = channels
        self.grid_size = 8 # TODO: read from config
        self.connect_hands = connect_hands

        # Load swig lib
        self.swig_lib = self.load_swig()
        self.trt_runner = self.init_model(weights_path)

    def get_att_indices(self, attributes):
        att_metadata = []
        att_idx = 0
        for single_att in attributes:
            att_vals = list(single_att.values())[0]
            att_name = list(single_att.keys())[0]
            att_data = dict(start_idx=att_idx, end_idx=att_idx + len(att_vals), name=att_name, vals=att_vals)
            att_metadata.append(att_data)
            att_idx += len(att_vals)
        return att_metadata

    def load_swig(self):
        import tensorrt_lib.bin._swig_yolo as swig_lib
        print("Loaded YOLO for tensorrt")
        return swig_lib

    def init_model(self, weights_path):
        print('YOLO TensorRT - loading model from %s' % weights_path)
        return self.swig_lib.loadModelAndCreateRunner(weights_path, self.channels, self.use_fp16)

    def infer(self, input_img, reverse_channels=True):
        output_size = self.grid_size * self.grid_size * self.nb_box * (4 + 1 + self.tot_att_size)
        m1_p1_norm = True if self.weights_path.endswith('.uff') else False
        # TODO-support 1 image for debug purposes, currently supportting only batches of 2
        out = np.zeros(2 * output_size, np.float32)
        n_images = len(input_img)
        if n_images==1:
            self.swig_lib.execute_2_frames(self.trt_runner, input_img[0], input_img[0], out, m1_p1_norm, reverse_channels)
            return [out[:output_size]]
        else:
            self.swig_lib.execute_2_frames(self.trt_runner, input_img[0], input_img[1], out, m1_p1_norm, reverse_channels)
            return [out[:output_size], out[output_size:]]

    def predict(self,images, reverse_channels=True):
        if not isinstance(images, list):
            images = [images]
        H, W, _ = images[0].shape
        net_input = []
        for image in images:
            # TODO - Solve GPU resize issue for more than 4 channels 
            image = cv2.resize(image.copy(), (self.input_size, self.input_size))
            net_input.append(image)

        outputs = self.infer(net_input, reverse_channels=reverse_channels)
        all_boxes = []
        for out in outputs:
            if self.weights_path.endswith('.onnx'):
                out_tensor = out.reshape(self.nb_box, 4 + 1 + self.tot_att_size, 8, 8).transpose(2,3,0,1)
            else:
                out_tensor = out.reshape(8, 8, 4 + 1 + self.tot_att_size)
            
            boxes  = decode_multi_att_vec(out_tensor, self.anchors, self.att_metadata, self.attributes, obj_threshold=self.confidence_th,
                                    nms_threshold=self.nms_th, inter_att_nms=self.inter_att_nms,
                                    connect_hands=self.connect_hands, products_only=self.predict_products_only)

            # Convert to image coordinates
            for b in boxes:
                b.renormalize(H,W)
            all_boxes.append(boxes)
        
        if len(all_boxes)==1:
            all_boxes = all_boxes[0]

        return all_boxes