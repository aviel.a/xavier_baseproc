import os
import cv2

import numpy as np
from cv_blocks.object_detector.utils import decode_netout_vec
from .backbones import load_backbone
import misc


class YoloObjectDetector(object):
    def __init__(self, backbone,
                       input_size, 
                       labels, 
                       anchors,
                       weights_path=None,
                       system_type='pc',
                       use_fp16=False,
                       confidence_th=0.3,
                       nms_th=0.3,
                       channels=3,
                       nb_class=None):
        self.input_size = input_size
        self.labels   = list(labels)
        self.nb_class = len(self.labels) if nb_class is None else nb_class
        self.nb_box   = len(anchors) // 2
        self.class_wt = np.ones(self.nb_class, dtype='float32')
        self.anchors  = anchors
        self.weights_path = weights_path
        assert system_type in ['pc', 'jetson']
        self.system_type = system_type
        self.use_fp16 = use_fp16
        self.confidence_th = confidence_th
        self.nms_th = nms_th
        self.channels = channels
        self.grid_size = 8 # TODO: read from config

        # Load swig lib
        self.swig_lib = self.load_swig()
        self.trt_runner = self.init_model(weights_path)

    def load_swig(self):
        import tensorrt_lib.bin._swig_yolo as swig_lib
        print("Loaded YOLO for tensorrt")
        return swig_lib

    def init_model(self, weights_path):
        print('YOLO TensorRT - loading model from %s' % weights_path)
        return self.swig_lib.loadModelAndCreateRunner(weights_path, self.channels, self.use_fp16)

    def infer(self, input_img, reverse_channels=True):
        output_size = self.grid_size * self.grid_size * self.nb_box * (5 + self.nb_class)
        m1_p1_norm = True if self.weights_path.endswith('.uff') else False
        # TODO-support 1 image for debug purposes, currently supportting only batches of 2
        out = np.zeros(2 * output_size, np.float32)
        n_images = len(input_img)
        if n_images==1:
            self.swig_lib.execute_2_frames(self.trt_runner, input_img[0], input_img[0], out, m1_p1_norm, reverse_channels)
            return [out[:output_size]]
        else:
            self.swig_lib.execute_2_frames(self.trt_runner, input_img[0], input_img[1], out, m1_p1_norm, reverse_channels)
            return [out[:output_size], out[output_size:]]

    def predict(self,images, reverse_channels=True):
        if not isinstance(images, list):
            images = [images]
        H, W, _ = images[0].shape
        net_input = []
        for image in images:
            # TODO - Solve GPU resize issue for more than 4 channels 
            image = cv2.resize(image.copy(), (self.input_size, self.input_size))
            net_input.append(image)

        outputs = self.infer(net_input, reverse_channels=reverse_channels)
        all_boxes = []
        for out in outputs:
            if self.weights_path.endswith('.onnx'):
                out_tensor = out.reshape(self.nb_box, 5 + self.nb_class, 8, 8).transpose(2,3,0,1)
            else:
                out_tensor = out.reshape(8, 8, self.nb_box, 5 + self.nb_class)

            boxes  = decode_netout_vec(out_tensor, self.anchors, self.nb_class,
                                    obj_threshold=self.confidence_th,
                                    nms_threshold=self.nms_th)

            # Convert to image coordinates
            for b in boxes:
                b.renormalize(H,W)
            all_boxes.append(boxes)
        
        if len(all_boxes)==1:
            all_boxes = all_boxes[0]

        return all_boxes


# Sanity test
if __name__=='__main__':
    import cv2
    import json
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    main_dir = os.path.join(curr_dir, '..', '..', '..')
    path = os.path.join(curr_dir,'..', 'config', 'config_tiny_yolo.json')
    # path = os.path.join(main_dir, 'ourFirstCNN','detector','yolo',r'config_aldi.json')
    with open(path) as config_buffer:
        configYolo = json.load(config_buffer)
    weights_path = os.path.join(curr_dir, '..', '..', 'pretrained_models','yolo_aldi_basler_2.uff')
    img_path = os.path.join(curr_dir, '..','data', 'sample_img.png')
    img = cv2.imread(img_path)

    my_yolo = YoloObjectDetector(configYolo['model']['backbone'],
                input_size=configYolo['model']['input_size'],
                labels=configYolo['model']['labels'],
                anchors=configYolo['model']['anchors'],
                weights_path=weights_path)
    # Run detector
    boxes = my_yolo.predict(img)