
class BaseFeatureExtractor(object):
    """docstring for ClassName"""

    # to be defined in each subclass
    def __init__(self, input_size):
        pass

class FullYoloFeature(BaseFeatureExtractor):
    @staticmethod
    def normalize(image):
        return image / 255.

class TinyYoloFeature(BaseFeatureExtractor):
    @staticmethod
    def normalize(image):
        return image / 255.

class MobileNetFeature(BaseFeatureExtractor):
    @staticmethod
    def normalize(image):
        image = image / 255.
        image = image - 0.5
        image = image * 2.

        return image		

class SqueezeNetFeature(BaseFeatureExtractor):
    @staticmethod
    def normalize(image):
        image = image[..., ::-1]
        image = image.astype('float')

        image[..., 0] -= 103.939
        image[..., 1] -= 116.779
        image[..., 2] -= 123.68

        return image    

class Inception3Feature(BaseFeatureExtractor):
    @staticmethod
    def normalize(image):
        image = image / 255.
        image = image - 0.5
        image = image * 2.

        return image

class VGG16Feature(BaseFeatureExtractor):
    @staticmethod
    def normalize(image):
        image = image[..., ::-1]
        image = image.astype('float')

        image[..., 0] -= 103.939
        image[..., 1] -= 116.779
        image[..., 2] -= 123.68

        return image 

class ResNet50Feature(BaseFeatureExtractor):
    @staticmethod
    def normalize(image):
        image = image[..., ::-1]
        image = image.astype('float')

        image[..., 0] -= 103.939
        image[..., 1] -= 116.779
        image[..., 2] -= 123.68

        return image 

def load_backbone(backbone_name):
    if backbone_name == 'Inception3':
        backbone = Inception3Feature 
    elif backbone_name == 'SqueezeNet':
        backbone = SqueezeNetFeature 
    elif backbone_name in ('MobileNet', 'MobileNetV2', 'DiffMobileNetV2'):
        backbone = MobileNetFeature
    elif backbone_name == 'Full Yolo':
        backbone = FullYoloFeature
    elif backbone_name == 'Tiny Yolo':
        backbone = TinyYoloFeature
    elif backbone_name == 'VGG16':
        backbone = VGG16Feature
    elif backbone_name == 'ResNet50':
        backbone = ResNet50Feature
    elif backbone_name == 'Tiny Yolo trt':
        backbone = TinyYoloFeatureNoBatch
    else:
        raise Exception('Architecture not supported! Only support Full Yolo, Tiny Yolo, MobileNet, SqueezeNet, VGG16, ResNet50, and Inception3 at the moment!')
    return backbone