import os
import numpy as np
import cv2
from .backbones import load_backbone
from cv_blocks.object_detector.utils import decode_netout_vec
from keras.models import Model
from keras.layers import Reshape, Conv2D, Input, Lambda

class YoloObjectDetector(object):
    def __init__(self, backbone,
                       input_size, 
                       labels, 
                       anchors,
                       weights_path=None,
                       confidence_th=0.3,
                       nms_th=0.3,
                       channels=3):
        self.input_size = input_size
        self.labels   = list(labels)
        self.nb_class = len(self.labels)
        self.nb_box   = len(anchors) // 2
        self.class_wt = np.ones(self.nb_class, dtype='float32')
        self.anchors  = anchors
        self.max_box_per_image = 3 # was used for training. TODO-remove
        self.weights_path = weights_path
        self.confidence_th = confidence_th
        self.nms_th = nms_th
        self.channels = channels

        input_image     = Input(shape=(input_size, input_size, channels))
        self.true_boxes = Input(shape=(1, 1, 1, self.max_box_per_image , 4))  
        
        # Load backbone
        self.backbone = load_backbone(backbone)(input_size, channels)
        print("grid size: {}".format(self.backbone.get_output_shape()))

        # Build object detection layer
        self.grid_h, self.grid_w = self.backbone.get_output_shape()        
        features = self.backbone.feature_extractor.output
        output = Conv2D(self.nb_box * (4 + 1 + self.nb_class), 
                        (1,1), strides=(1,1), 
                        padding='same', 
                        name='DetectionLayer')(features)
        output = Reshape((self.grid_h, self.grid_w, self.nb_box, 4 + 1 + self.nb_class))(output)
        output = Lambda(lambda args: args[0])([output, self.true_boxes])
        self.model = Model([self.backbone.feature_extractor.input, self.true_boxes], output)

        # Load weights
        assert os.path.exists(weights_path)
        print('YOLO keras - loading weights from %s' % weights_path)
        self.model.load_weights(weights_path)

        # Print a summary of the whole model
        self.model.summary()

    def predict(self, image):
        H, W, _ = image.shape
        image = cv2.resize(image, (self.input_size, self.input_size))
        image = self.backbone.normalize(image)

        input_image = image[:,:,::-1]
        input_image = np.expand_dims(input_image, 0)
        dummy_array = np.zeros((1,1,1,1,self.max_box_per_image,4))

        out_tensor = self.model.predict([input_image, dummy_array])[0]

        boxes  = decode_netout_vec(out_tensor, self.anchors, self.nb_class,
                                   obj_threshold=self.confidence_th,
                                   nms_threshold=self.nms_th)

        # Convert to image coordinates
        for b in boxes:
            b.renormalize(H,W)

        return boxes

# Sanity test
if __name__=='__main__':
    import cv2
    import json
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    main_dir = os.path.join(curr_dir, '..', '..', '..')
    path = os.path.join(main_dir, 'ourFirstCNN','detector','yolo',r'config_aldi.json')
    with open(path) as config_buffer:
        configYolo = json.load(config_buffer)
    weights_path = os.path.join(curr_dir, '..', 'pretrained_models','yolo_aldi_basler_2.h5')
    img_path = os.path.join(curr_dir, '..','data', 'sample_img.png')
    img = cv2.imread(img_path)

    my_yolo = YoloObjectDetector(configYolo['model']['backend'],
                input_size=configYolo['model']['input_size'],
                labels=configYolo['model']['labels'],
                max_box_per_image=configYolo['model']['max_box_per_image'],
                anchors=configYolo['model']['anchors'],
                weights_path=weights_path)
    # Run detector
    boxes = my_yolo.predict(img)