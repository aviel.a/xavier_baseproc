import csv
import os
import time
import argparse
import pandas as pd

from random import sample

from utils.picker_list_gen.picker_list import upload_single_picker_list

from enum import Enum
class ONBOARDING_SESSION_TYPE(Enum):
    DATA_SINGLES = 1
    DATA_USER_JOURNEY = 2
    TEST_USER_JOURNEY = 3

def init_product_list(path):

    assert os.path.exists(path)

    df_prod = pd.read_csv(path)
    if not 'probability_factor' in df_prod:
        df_prod['probability_factor'] = 1.
    skuList = df_prod['product_name'].astype(str).tolist() # TODO - should be product_sku
    names = df_prod['english_name'].astype(str).tolist()
    probs = df_prod['probability_factor'].tolist()

    namesDict = {}
    probDict = {}
    for sku, name, pr in zip(skuList, names, probs):
        namesDict[sku] = name
        probDict[sku] = pr

    return skuList, namesDict, probDict


def init_user_journey(i, save_images, user_journeys, user_journeys_dir):
    print("\nUser Journey #{}".format(i))
    user_journey = user_journeys[i]
    if save_images:
        user_journey_dir = os.path.join(user_journeys_dir, "user_journey_{}".format(i + 1))
        os.makedirs(user_journey_dir)
    return user_journey, user_journey_dir



def generate_session(skuList, session_type):
    session = []
    if session_type== ONBOARDING_SESSION_TYPE.DATA_SINGLES:
        # session with single item in single item out 10 times
        session = sample(skuList, len(skuList))
    elif session_type== ONBOARDING_SESSION_TYPE.DATA_USER_JOURNEY:
        # session with items inserted one by one
        session = sample(skuList, len(skuList))
    elif session_type== ONBOARDING_SESSION_TYPE.TEST_USER_JOURNEY:
        # session with items inserted one by one and then removed
        all_in = sample(skuList, len(skuList))
        all_out = all_in.copy()
        all_out.reverse()
        all_in = [(i, 1)  for i in all_in] # create tuples of (item,direction=1)
        all_out= [(i, -1) for i in all_out] # create tuples of (item,direction= -1 )
        session = all_in + all_out
    return session

def upload_session(session_content, session_type, session_idx, destination,plan_time_stamp,target_date = None):
    # upload session
    if session_type == ONBOARDING_SESSION_TYPE.DATA_SINGLES:
        purpose = 'ob-single'
    elif session_type == ONBOARDING_SESSION_TYPE.DATA_USER_JOURNEY:
        purpose = 'ob-pile'
    elif session_type == ONBOARDING_SESSION_TYPE.TEST_USER_JOURNEY:
        purpose = 'test'
    # id is "<time>_<session_idx>"
    # time can be converted to human readable by time.ctime(<time>)
    id = str(plan_time_stamp) + '_' + str(session_idx).zfill(3)
    print('id: {}'.format(id))
    upload_single_picker_list(session_content, purpose, destination, toUpload=True, id=id, target_date = target_date,verbose=False)


def print_session(session_idx,session_type, session_content, namesDict ):
    print("session #{}, type:{}".format(session_idx + 1, ONBOARDING_SESSION_TYPE(session_type).name))
    for event in session_content:
        strng = ""
        if isinstance(event, str):  # single item which is the SKU
            direction = 'in'
            sku = event
        else:
            direction = 'in' if event[1] == 1 else 'out'
            sku = event[0]

        strng += "SKU:" + sku.ljust(20)
        strng += "direction: " + direction.ljust(10)
        strng += "name:" + namesDict[sku].ljust(20)

        print(strng)

def create_onboarding_plan(product_list, destination ,reps, verbose=1):

    # read product list + names
    skuList, namesDict, probDict = init_product_list(product_list)

    if len(skuList) < 1 or 25 < len(skuList):
        raise NameError("onboarding should have 1-25 products")

    target_date = None #'2020-07-21' #None - will be created for today #cloud.api.generate_target_date yyyy-mm-dd

    plan_sessions = [ONBOARDING_SESSION_TYPE.DATA_SINGLES]       * reps['ob_single'] + \
                    [ONBOARDING_SESSION_TYPE.DATA_USER_JOURNEY] * reps['ob_pile'] + \
                    [ONBOARDING_SESSION_TYPE.TEST_USER_JOURNEY] * reps['reg']


    plan_time_stamp = time.time()
    for session_idx, session_type in enumerate(plan_sessions) :

        session_content = generate_session(skuList,session_type)

        if verbose:
            print_session(session_idx,session_type,session_content,namesDict)

        upload_session(session_content, session_type, session_idx, destination,plan_time_stamp,target_date = target_date)





DEFAULT_N_OB_SINGLE = 1
DEFAULT_N_OB_PILE = 5
DEFAULT_N_REGRESSION = 2
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='onboarding plan generation')
    parser.add_argument('--product-list', help='path to product list to generate plan from',type=str, required=True)
    parser.add_argument('--destination', help='root images directory',type=str, default='walkout')
    parser.add_argument('--n-ob-single', help='number of sessions for onboarding,single',type=int, default=DEFAULT_N_OB_SINGLE)
    parser.add_argument('--n-ob-pile', help='number of sessions for onboarding,pile',type=int, default=DEFAULT_N_OB_PILE)
    parser.add_argument('--n-regression', help='number of sessions for regressions',type=int, default=DEFAULT_N_REGRESSION)
    args = parser.parse_args()

    reps = dict(ob_single=args.n_ob_single, ob_pile=args.n_ob_pile, reg=args.n_regression)

    create_onboarding_plan(args.product_list, args.destination, reps,verbose=True )






