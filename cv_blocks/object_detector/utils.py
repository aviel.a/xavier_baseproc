import numpy as np
from cv_blocks.common.types import BoundingBox2D, RotatedBoundingBox2D

def _sigmoid(x):
    return 1. / (1. + np.exp(-x))

def _softmax(x, axis=-1, t=-100.):
    x = x - np.max(x)
    
    if np.min(x) < t:
        x = x/np.min(x)*t
        
    e_x = np.exp(x)
    
    return e_x / e_x.sum(axis, keepdims=True)

def decode_netout_vec(netout, anchors, nb_class, obj_threshold=0.3, nms_threshold=0.3):
    # We require 1/(1+e^-x)>th => x>log(th/(1-th))
    # This way we avoid the sigmoid for the whole output
    th_before_sigmoid = np.log(obj_threshold / (1 - obj_threshold))

    # decode the output by the network
    if nb_class==1:
        # Prune output with confidence below th
        valid_idx = np.argwhere(netout[...,4] > th_before_sigmoid)
    else:
        netout[..., 4]  = _sigmoid(netout[..., 4])
        netout[..., 5:] = netout[..., 4][..., np.newaxis] * _softmax(netout[..., 5:])
        # Prune output with confidence below th
        valid_idx = np.argwhere(netout[...,4] > obj_threshold)
    valid_outputs = netout[valid_idx[:,0],valid_idx[:,1], valid_idx[:,2]]
    h_idx, w_idx, anchor_idx = valid_idx.T

    grid_h, grid_w, nb_box = netout.shape[:3]

    w_ancr = np.array(anchors[0::2])
    h_ancr = np.array(anchors[1::2])
    x = (w_idx + _sigmoid(valid_outputs[...,0])) / grid_w
    y = (h_idx + _sigmoid(valid_outputs[...,1])) / grid_h
    w = w_ancr[anchor_idx] * np.exp(valid_outputs[...,2]) / grid_w
    h = h_ancr[anchor_idx] * np.exp(valid_outputs[...,3]) / grid_h
    if nb_class==1: # classes is the same as confidence
        confidence = _sigmoid(valid_outputs[...,4])
        classes = confidence[..., np.newaxis]
    else: # no need to apply sigmoid & softmax, it was already applied before
        confidence = valid_outputs[...,4]
        classes = valid_outputs[...,5:]

    #x-w/2, y-h/2, x+w/2, y+h/2
    x_min = (x - w/2).clip(0, 1)
    y_min = (y - h/2).clip(0, 1)
    x_max = (x + w/2).clip(0, 1)
    y_max = (y + h/2).clip(0, 1)

    boxes = []
    for idx in range(len(x)):
        # assuming single object - no softmax needed
        box = BoundingBox2D((x_min[idx], y_min[idx], x_max[idx], y_max[idx], confidence[idx]), 
                             classes=classes[idx])
        boxes.append(box)
    if len(boxes)==0:
        return []

    # suppress non-maximal boxes
    # for each class, do the following
    # 1. sort boxes by confidence
    # 2. insert every box to chosen list, if it does not have large overlap with the already chosen boxes
    sorted_indices = list(reversed(np.argsort([max(box.classes) for box in boxes])))
    for i in range(len(sorted_indices)):
        index_i = sorted_indices[i]
        c = np.argmax(boxes[index_i].classes)
        if boxes[index_i].classes[c] == 0:
            continue
        else:
            for j in range(i+1, len(sorted_indices)):
                index_j = sorted_indices[j]
                if BoundingBox2D.iou(boxes[index_i], boxes[index_j]) >= nms_threshold:
                    boxes[index_j].classes[:] = 0
                    boxes[index_j].score = 0

    # remove the boxes which are less likely than a obj_threshold
    boxes = [box for box in boxes if box.get_score() > obj_threshold]

    return boxes

def decode_rotated_netout_vec(netout, anchors, nb_class, obj_threshold=0.3, nms_threshold=0.3, max_rot_rad=np.pi/4):
    # We require 1/(1+e^-x)>th => x>log(th/(1-th))
    # This way we avoid the sigmoid for the whole output
    th_before_sigmoid = np.log(obj_threshold / (1 - obj_threshold))

    # decode the output by the network
    if nb_class==1:
        # Prune output with confidence below th
        valid_idx = np.argwhere(netout[...,5] > th_before_sigmoid)
    else:
        netout[..., 5]  = _sigmoid(netout[..., 5])
        netout[..., 6:] = netout[..., 5][..., np.newaxis] * _softmax(netout[..., 6:])
        # Prune output with confidence below th
        valid_idx = np.argwhere(netout[...,5] > obj_threshold)
    valid_outputs = netout[valid_idx[:,0],valid_idx[:,1], valid_idx[:,2]]
    h_idx, w_idx, anchor_idx = valid_idx.T

    grid_h, grid_w, nb_box = netout.shape[:3]

    w_ancr = np.array(anchors[0::2])
    h_ancr = np.array(anchors[1::2])
    x = (w_idx + _sigmoid(valid_outputs[...,0])) / grid_w
    y = (h_idx + _sigmoid(valid_outputs[...,1])) / grid_h
    w = w_ancr[anchor_idx] * np.exp(valid_outputs[...,2]) / grid_w
    h = h_ancr[anchor_idx] * np.exp(valid_outputs[...,3]) / grid_h
    angle_rad = 2. * (_sigmoid(valid_outputs[...,4]) - 0.5) * max_rot_rad
    if nb_class==1: # classes is the same as confidence
        confidence = _sigmoid(valid_outputs[...,5])
        classes = confidence[..., np.newaxis]
    else: # no need to apply sigmoid & softmax, it was already applied before
        confidence = valid_outputs[...,5]
        classes = valid_outputs[...,6:]

    boxes = []
    for idx in range(len(x)):
        # assuming single object - no softmax needed
        box = RotatedBoundingBox2D((x[idx], y[idx], w[idx], h[idx], angle_rad[idx], confidence[idx]), 
                             classes=classes[idx])
        boxes.append(box)
    if len(boxes)==0:
        return []

    # suppress non-maximal boxes
    # for each class, do the following
    # 1. sort boxes by confidence
    # 2. insert every box to chosen list, if it does not have large overlap with the already chosen boxes
    sorted_indices = list(reversed(np.argsort([max(box.classes) for box in boxes])))
    for i in range(len(sorted_indices)):
        index_i = sorted_indices[i]
        c = np.argmax(boxes[index_i].classes)
        if boxes[index_i].classes[c] == 0:
            continue
        else:
            for j in range(i+1, len(sorted_indices)):
                index_j = sorted_indices[j]
                if RotatedBoundingBox2D.iou(boxes[index_i], boxes[index_j]) >= nms_threshold:
                    boxes[index_j].classes[:] = 0
                    boxes[index_j].score = 0

    # remove the boxes which are less likely than a obj_threshold
    boxes = [box for box in boxes if box.get_score() > obj_threshold]

    return boxes

def decode_multi_att_vec(netout, anchors, att_metadata, att_dict, obj_threshold=0.3, thrown_obj_threshold=0.1,
                         nms_threshold=0.3, inter_att_nms=None, connect_hands=False, products_only=False):
    netout[..., 4]  = _sigmoid(netout[..., 4])
    # netout[..., 5:] = netout[..., 4][..., np.newaxis] * _softmax(netout[..., 5:])
    # Prune output with confidence below th
    valid_idx = np.argwhere(netout[...,4] > thrown_obj_threshold)
    valid_outputs = netout[valid_idx[:,0],valid_idx[:,1], valid_idx[:,2]]
    if len(valid_outputs)==0:
        return []
    h_idx, w_idx, anchor_idx = valid_idx.T

    grid_h, grid_w, nb_box = netout.shape[:3]

    w_ancr = np.array(anchors[0::2])
    h_ancr = np.array(anchors[1::2])
    x = (w_idx + _sigmoid(valid_outputs[...,0])) / grid_w
    y = (h_idx + _sigmoid(valid_outputs[...,1])) / grid_h
    w = w_ancr[anchor_idx] * np.exp(valid_outputs[...,2]) / grid_w
    h = h_ancr[anchor_idx] * np.exp(valid_outputs[...,3]) / grid_h
    
    # no need to apply sigmoid, it was already applied before
    confidence = valid_outputs[...,4]

    # Extract attribute outputs
    att_data = valid_outputs[...,5:]
    att_info = dict()
    for single_att in att_metadata:
        att_info[single_att['name']] = _softmax(att_data[:, single_att['start_idx'] : single_att['end_idx']])

    #x-w/2, y-h/2, x+w/2, y+h/2
    x_min = (x - w/2).clip(0, 1)
    y_min = (y - h/2).clip(0, 1)
    x_max = (x + w/2).clip(0, 1)
    y_max = (y + h/2).clip(0, 1)

    boxes = []
    for idx in range(len(x)):
        box_atts = {k: v[idx] for k, v in att_info.items()}
        # assuming single object - no softmax needed
        box = BoundingBox2D((x_min[idx], y_min[idx], x_max[idx], y_max[idx], confidence[idx]), attributes=box_atts)
        box_state = box.get_top_attr('crop_state', att_dict)
        box_type = box.get_top_attr('crop_type', att_dict)
        # Increase thrown & small product crop confidence so they will pass the threshold
        if box_state['name']=='thrown' and box_type['name']=='product' and box.score < obj_threshold and box.area() < 0.05:
            box.score = obj_threshold + 0.01
        if box.get_score() > obj_threshold:
            boxes.append(box)
    if len(boxes)==0:
        return []

    # suppress non-maximal boxes
    # for each class, do the following
    # 1. sort boxes by confidence
    # 2. insert every box to chosen list, if it does not have large overlap with the already chosen boxes
    # TODO - consider taking product over hand or product & hand, even if they have high iou
    sorted_indices = list(reversed(np.argsort([max(box.attributes['crop_type'] * box.score) for box in boxes])))
    # sorted_indices = list(reversed(np.argsort([box.score for box in boxes])))
    nms_threshold = 0.2
    for i in range(len(sorted_indices)):
        index_i = sorted_indices[i]
        c = np.argmax(boxes[index_i].attributes['crop_type'])
        if products_only and c != 0:
            boxes[index_i].score = 0
        if boxes[index_i].score==0 or boxes[index_i].attributes['crop_type'][c] == 0:
            continue
        else:
            for j in range(i+1, len(sorted_indices)):
                index_j = sorted_indices[j]
                box_type = np.argmax(boxes[index_j].attributes['crop_type'])
                if box_type==c and BoundingBox2D.iou(boxes[index_i], boxes[index_j]) >= nms_threshold:
                    boxes[index_j].attributes['crop_type'][:] = 0
                    boxes[index_j].score = 0
                elif inter_att_nms is not None:
                    if c in inter_att_nms['vals'] and box_type in inter_att_nms['vals'] and \
                    BoundingBox2D.iou(boxes[index_i], boxes[index_j]) >= inter_att_nms['nms']:
                        boxes[index_j].attributes['crop_type'][:] = 0
                        boxes[index_j].score = 0

    # remove the boxes which are less likely than a obj_threshold
    boxes = [box for box in boxes if box.get_score() > obj_threshold]

    # Assign hands that are related to products
    remove_indices = []
    if connect_hands and len(boxes)>1:
        for i in range(len(boxes)):
            c = np.argmax(boxes[i].attributes['crop_type'])
            if c == 0: # product
                continue
            else:
                box_location = boxes[i].get_top_attr('crop_location', att_dict)
                # Compute all ious with boxes
                ious = []
                for j , bx in enumerate(boxes):
                    crop_type = np.argmax(bx.attributes['crop_type'])
                    ious.append(-1 if j==i or crop_type!=0 else BoundingBox2D.iou(boxes[i], bx))
                ious = np.array(ious)
                iou_th = 0.1 if box_location['name']==('inside-cart', 'on-cart-bottom') else 0.
                if ious.max() < iou_th:
                    continue
                # Choose best product match and assign 
                box_idx_to_assign = np.argmax(ious)
                boxes[box_idx_to_assign].add_connected_box(boxes[i])
                remove_indices.append(i)
        
        # Remove boxes that were assigned to other boxes
        boxes = [box for bx_idx, box in enumerate(boxes) if bx_idx not in remove_indices]

    return boxes