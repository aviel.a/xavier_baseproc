import os
import cv2
import yaml
from cv_blocks.misc import aws_download
import numpy as np
try:
    cmd = 'import tensorrt_lib.bin._swig_yaeen as swig_cls'
    exec(cmd)
except:
    raise NameError('missing swig_yaeen compilation for given arch,FP,training data')


class StackedClassifier(object):
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.sample_n_images = config.get('sample_n_images', 2)

    def __init__(self, weights, model_config=None, img_size=(64,64), use_fp16=True, n_classes=4, batch_size=2, debug=False, sync=True,
                 sc_config=dict()):
        assert weights is not None
        self.params = self.Config(sc_config)
        self.weights = weights
        print('initializing Stacked Classifier')
        self.use_fp16 = use_fp16
        self.batch_size = batch_size
        self.img_size = img_size
        self.n_classes = n_classes
        self.load_classifier(weights, config=model_config, sync=sync)
        self.debug = debug

    def load_classifier(self, weights, config, sync):
        weights, conf_file = self.get_model(weights, sync=sync)
        if config is not None:
            conf_file = config
        with open(conf_file) as config_f:
            config = yaml.safe_load(config_f)
        if 'img_size' in config:
            self.img_size = tuple(config['img_size'])
        assert 'class_names' in config
        self.class_names = config['class_names']
        self.n_classes = len(config['class_names'])
        self.input_channels = config.get('input_channels', 3)
        self.frames_stacked = config.get('frames_stacked', 1)
        self.is_gray = self.input_channels==self.frames_stacked

        print('initializing  Cart In/Out Classifier')
        self.engine = swig_cls.loadModelAndCreateEngine(weights, self.use_fp16, self.batch_size)
        print('loading context')
        self.context = swig_cls.create_context(self.engine)
        print('finished loading context')

    def get_model(self, weights, sync):
        model_root_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'models')
        model_name, model_ext = os.path.splitext(weights)
        assert model_ext in ('.pth', '.onnx')
        full_weights_path = os.path.join(model_root_dir, model_name, weights)
        full_conf_path = full_weights_path.replace(model_ext, '_config.yaml')
        if not os.path.exists(full_weights_path) and sync:
            full_model_dir = os.path.dirname(full_weights_path)
            os.makedirs(full_model_dir, exist_ok=True)
            # Get model & config file
            aws_download.download("models/products_counter", model_name, full_model_dir)

        assert os.path.exists(full_weights_path) and os.path.exists(full_conf_path)
        return full_weights_path, full_conf_path

    def predict(self, img):
        if not isinstance(img, (list, tuple)):
            img = [img]
        bs = len(img)
        img_re = [cv2.resize(i, self.img_size).astype(np.float32) for i in img]
        assert bs <= self.batch_size
        img = np.stack(img_re)
        img = img.astype(np.float32) / 255.
        img = np.transpose(img, (0, 3, 1, 2))
        data = img.ravel()
        probs = np.zeros(self.n_classes * bs, np.float32)
        swig_cls.execute(self.engine,self.context,data,probs, bs)
        probs = np.reshape(probs, (bs, -1))
        # if self.debug:
        #     self.visualize_input(img_re, p_inside_cart, cam)
        return probs

    def visualize_input(self, image, probs, gt=None, show=False):
        probs = probs[0]
        viz_img = cv2.resize(image, (512, 512))
        if self.is_gray:
            viz_img = cv2.cvtColor(viz_img, cv2.COLOR_GRAY2BGR)
        top_indices = np.argsort(probs)[::-1]
        cv2.putText(viz_img, '#1st pred - c={}, p={:.2f}'.format(self.class_names[top_indices[0]], probs[top_indices[0]]),
                    (5, 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 0, 0))
        cv2.putText(viz_img, '#2nd pred - c={}, p={:.2f}'.format(self.class_names[top_indices[1]], probs[top_indices[1]]),
                    (5, 45), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 0, 0))
        if gt is not None:
            cv2.putText(viz_img, 'gt - c={}'.format(gt), (5, 75), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 0, 0))
        if show:
            cv2.imshow("Stacked classifier input", viz_img)
            cv2.waitKey(0)
        return viz_img
