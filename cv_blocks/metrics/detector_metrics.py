import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import precision_recall_curve, average_precision_score
import cv2

def _interval_overlap(interval_a, interval_b):
    x1, x2 = interval_a
    x3, x4 = interval_b

    if x3 < x1:
        if x4 < x1:
            return 0
        else:
            return min(x2,x4) - x1
    else:
        if x2 < x3:
             return 0
        else:
            return min(x2,x4) - x3          

def bbox_iou(box1, box2):
    intersect_w = _interval_overlap([box1.x1, box1.x2], [box2.x1, box2.x2])
    intersect_h = _interval_overlap([box1.y1, box1.y2], [box2.y1, box2.y2])  
    
    intersect = intersect_w * intersect_h

    w1, h1 = box1.x2-box1.x1, box1.y2-box1.y1
    w2, h2 = box2.x2-box2.x1, box2.y2-box2.y1
    
    union = w1*h1 + w2*h2 - intersect
    
    return float(intersect) / union

def compute_detector_metric(detection_data, output_dir, weight_path, iou_th = 0.3, attributes=None, visualizer=None):
    weight_name = os.path.basename(weight_path)
    cols = ['img_name', 'score', 'iou', 'label']
    df_all = pd.DataFrame(columns=cols)
    fp, fn, low_iou = 0, 0, 0
    if attributes is not None:
        confusion_matrices = dict()
        for attr_type, attr_val in attributes.items():
            confusion_matrices[attr_type] = np.zeros((len(attr_val), len(attr_val)))
    for d in detection_data:
        det_idx = 0
        # Add detections with valid IOU
        while det_idx < len(d['matched_gt']):
            if attributes is not None:
                for attr_type, attr_val in attributes.items():
                    # ignore empty/not sure annotations
                    if d['matched_gt'][det_idx].attributes[attr_type] not in ['-----', 'not-sure']:
                        gt_idx = attr_val.index(d['matched_gt'][det_idx].attributes[attr_type])
                        pred_idx = np.argmax(d['pred'][det_idx].attributes[attr_type])
                        confusion_matrices[attr_type][gt_idx, pred_idx] += 1
                        if pred_idx != gt_idx:
                            image_path = os.path.join(output_dir, "sorted_failures", attr_type, "(gt)"+attr_val[gt_idx],
                                                      "(pred)"+attr_val[pred_idx])
                            os.makedirs(image_path, exist_ok=True)
                            visualizer.visualize(cv2.imread(d['img_name']), d['pred'], d['matched_gt'] + d['unmatched_gt'],
                                                 d['iou'], name=os.path.basename(d['img_name']), path=image_path)
            data = [d['img_name'], d['pred'][det_idx].score, d['iou'][det_idx], 1.]
            df = pd.DataFrame([data], columns=cols)
            df_all = df_all.append(df, ignore_index=True)
            if d['iou'][det_idx] < iou_th:
                low_iou += 1
            det_idx += 1
        # Add false positives
        while det_idx < len(d['pred']):
            data = [d['img_name'], d['pred'][det_idx].score, 0., 0.]
            df = pd.DataFrame([data], columns=cols)
            df_all = df_all.append(df, ignore_index=True)
            det_idx += 1
            fp += 1
        # Add false negatives
        det_idx = 0
        while det_idx < len(d['unmatched_gt']):
            data = [d['img_name'], 0., 0., 1.]
            df = pd.DataFrame([data], columns=cols)
            df_all = df_all.append(df, ignore_index=True)
            det_idx += 1
            fn += 1

    total_gt_bboxes = df_all['label'].sum()
    mAP_ious = np.arange(0.5, 1, 0.05)
    save_ious = [0.5, 0.75]
    x_base = df_all['score'].to_numpy()
    ap = []
    for mAP_iou in mAP_ious:
        mAP_iou = round(mAP_iou, 2)
        y = ((df_all['iou'] > mAP_iou) * df_all['label']).to_numpy()  # change labels to 0 if iou < th
        add_rows = total_gt_bboxes - y.sum()
        y = np.concatenate((y, np.zeros((int(add_rows)))), axis=0)  # add false negatives as the number of times labels were changed from 1 to 0 above
        x = np.concatenate((x_base, np.ones((int(add_rows)))), axis=0)
        if mAP_iou in save_ious:
            plot_precision_recall_graph(x, y, output_dir, weight_name, mAP_iou)
        ap.append(average_precision_score(y, x))

    plot_iou_curve(df_all, fp, fn, len(detection_data), output_dir, weight_name)
    map = np.mean(ap)
    plot_map_graph(ap, mAP_ious, map, output_dir, weight_name)

    if attributes is not None:
        with pd.ExcelWriter(os.path.join(output_dir, "confusion_matrices.xlsx")) as writer:
            accuracy = pd.DataFrame(columns=["attribute", "accuracy"])
            for attr_type, attr_val in attributes.items():
                columns = ["pred {}".format(val) for val in attr_val]
                rows = ["gt {}".format(val) for val in attr_val]
                events = np.sum(confusion_matrices[attr_type])
                success = np.trace(confusion_matrices[attr_type])
                pd.DataFrame(confusion_matrices[attr_type].astype(np.int), columns=columns, index=rows).to_excel(
                    writer, sheet_name='{}-total'.format(attr_type))
                pd.DataFrame(confusion_matrices[attr_type] / events, columns=columns, index=rows).to_excel(
                    writer, sheet_name='{}-normalized'.format(attr_type))
                acc = success / events
                accuracy.loc[accuracy.shape[0]] = [attr_type, acc]
            accuracy.to_excel(writer, 'accuracy', index=False)

    return df_all

def plot_map_graph(y, x, map_value, output_dir, weight_name):
    plt.figure()
    plt.plot(x, y, '-r')
    plt.title('%s\nAverage Precision per iou, mAP=%.4f' % (weight_name, map_value))
    plt.xlabel('IOU threshold')
    plt.ylabel('Average Precision')
    if output_dir is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, 'map_graph.png'))

def plot_precision_recall_graph(x, y, output_dir, weight_name, iou):
    precision, recall, th = precision_recall_curve(y, x)
    ap = average_precision_score(y, x)
    plt.figure()
    plt.plot(th, recall[:-1], '-r', label='Recall')
    plt.plot(th, precision[:-1], '-b', label='Precision')
    plt.legend()
    plt.title('%s\nPrecision & Recall, iou = %.2f, AP=%.4f' % (weight_name, iou, ap))
    plt.xlabel('Detector threshold[0-1]')
    if output_dir is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, 'precision_recall_iou_{}.png'.format(iou)))

def plot_iou_curve(results, fp, fn, total_imgs, output_dir, weight_name):
    # plot IOU curve
    ious = np.sort(results['iou'])
    x_vec = np.arange(ious.shape[0])
    auc_y = 1 - np.cumsum(x_vec) / (x_vec.sum())
    auc_x = ious
    ious_filtered = ious[ious>0]  # remove fp / fn cases
    iou_30 = (ious_filtered > 0.3).mean()
    iou_50 = (ious_filtered > 0.5).mean()
    iou_70 = (ious_filtered > 0.7).mean()
    txt = 'fp = %.4f (%d)\nfn = %.4f (%d)\n iou>0.3 = %.4f\n iou>0.5 = %.4f\n iou>0.7 = %.4f' % \
          (fp / total_imgs, fp, fn / total_imgs, fn, iou_30, iou_50, iou_70)
    plt.figure()
    plt.xlim((0, 1))
    plt.plot(auc_x, auc_y)
    plt.title('%s\nIOU curve. avg: %.2f, med:%.2f' % (weight_name, ious_filtered.mean(), np.median(ious_filtered)))
    plt.xlabel('IOU[0-1]')
    plt.ylabel('Ratio of detection above IOU[0-1]')
    plt.xlim([0.5, 1.0])
    plt.text(0.5, 0.8, txt)
    if output_dir is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, 'detection_metric.png'))
        results.to_csv(os.path.join(output_dir, 'detection_data.csv'))