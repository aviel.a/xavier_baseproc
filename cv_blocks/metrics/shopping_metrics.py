import enum
import numpy as np
import copy
from cv_blocks.events.event_logic import EventType

LOW_CONF_STATES = (EventType.NotConfidentEventTriggerUnrecognized,
                   EventType.NotConfidentEventTriggerPlu,
                   EventType.NotConfidentEventTriggerBarcode)

class ItemStatus(enum.Enum):
    Unassigned = -1
    Assigned = 0
    EventDetected = 1
    ProductDetected = 2
    ProductDetectedInTopK = 3
    EventMissed = 4
    EventMadeUp = 5
    EventDetectedLowConf = 6
    EventFalseLowConf = 7
    Ignored = 8

def assign_event_msco(gt_idx, gt, gt_status, wo_event, mode, max_frame_diff=500):
    gt_val = None
    event_dict = None
    if mode=='valid_insertion':
        tmp_gt_idx = gt_idx
        while True:
            if len(gt) < tmp_gt_idx + 1:
                break
            if wo_event['frameNum'] >= gt[tmp_gt_idx]['frameNum'] - 10 and wo_event['frameNum'] - gt[tmp_gt_idx]['frameNum'] < \
                    max_frame_diff and wo_event['direction'] == gt[tmp_gt_idx]['direction'] and gt_status[tmp_gt_idx] == ItemStatus.Unassigned:
                gt_val = ItemStatus.ProductDetectedInTopK
                gt_idx = tmp_gt_idx
                tmp_gt_idx = tmp_gt_idx + 1
                while True:  # find a better match
                    if len(gt) < tmp_gt_idx + 1 or wo_event['frameNum'] < gt[tmp_gt_idx]['frameNum'] - 10:
                        break
                    if wo_event['direction'] == gt[tmp_gt_idx]['direction'] and gt_status[tmp_gt_idx] == ItemStatus.Unassigned:
                        gt_idx = tmp_gt_idx
                    tmp_gt_idx = tmp_gt_idx + 1
                event_dict = copy.copy(gt[gt_idx])
                event_dict['pred_name'] = wo_event['name']
                event_dict['pred_frame'] = wo_event['frameNum']
                event_dict['pred_probs'] = wo_event['topProb']
                event_dict['state'] = gt_val
                break
            elif wo_event['frameNum'] >= gt[gt_idx]['frameNum'] - 10:
                tmp_gt_idx += 1
            else:
                break
    elif mode=='valid_removal':
        tmp_gt_idx = gt_idx
        while True:
            if tmp_gt_idx <= 0:
                break
            if wo_event['frameNum'] < gt[tmp_gt_idx]['frameNum'] + 10 and wo_event['direction'] == gt[tmp_gt_idx]['direction'] and \
                    gt_status[tmp_gt_idx] == ItemStatus.Unassigned:
                gt_val = ItemStatus.ProductDetectedInTopK
                gt_idx = tmp_gt_idx
                tmp_gt_idx = tmp_gt_idx - 1
                while True:  # find a better match
                    if tmp_gt_idx <= 0 or wo_event['frameNum'] > gt[tmp_gt_idx]['frameNum'] + 10:
                        break
                    if wo_event['direction'] == gt[tmp_gt_idx]['direction'] and gt_status[tmp_gt_idx] == ItemStatus.Unassigned:
                        gt_idx = tmp_gt_idx
                    tmp_gt_idx = tmp_gt_idx - 1
                event_dict = copy.copy(gt[gt_idx])
                event_dict['pred_name'] = wo_event['name']
                event_dict['pred_frame'] = wo_event['frameNum']
                event_dict['pred_probs'] = wo_event['topProb']
                event_dict['state'] = gt_val
                break
            elif wo_event['frameNum'] < gt[gt_idx]['frameNum'] + 10:
                tmp_gt_idx -= 1
            else:
                break
    return gt_val, gt_idx, event_dict

def assign_event(gt_event, walkout, walkout_status, max_frame_diff_valid=30, valid_states=None):
    gt_val = None
    event_dict = copy.copy(gt_event)
    possible_prod_det = [dict(idx=idx, event=wa_event) for idx, wa_event in enumerate(walkout) if \
        wa_event['name']==gt_event['name'] and walkout_status[idx]==ItemStatus.Unassigned and \
        abs(int(wa_event['frameNum']) - int(gt_event['frameNum'])) < max_frame_diff_valid and \
            wa_event['direction']==gt_event['direction']]
    if len(possible_prod_det)==1: # single candidate for correct product detection
        wa_idx = possible_prod_det[0]['idx']
        if valid_states is None or ItemStatus.ProductDetected in valid_states:
            walkout_status[wa_idx] = ItemStatus.ProductDetected
        gt_val = ItemStatus.ProductDetected
        event_dict['pred_name'] = walkout[wa_idx]['name']
        event_dict['pred_frame'] = walkout[wa_idx]['frameNum']
        event_dict['pred_probs'] = walkout[wa_idx]['topProb']
    elif len(possible_prod_det)>0: # multiple candidates, choose nearest one in time
        best_idx = np.argmin(np.array([abs(int(ppd['event']['frameNum']) - int(gt_event['frameNum'])) for ppd in possible_prod_det]))
        wa_idx = possible_prod_det[best_idx]['idx']
        if valid_states is None or ItemStatus.ProductDetected in valid_states:
            walkout_status[wa_idx] = ItemStatus.ProductDetected
        gt_val = ItemStatus.ProductDetected
        event_dict['pred_name'] = walkout[wa_idx]['name']
        event_dict['pred_frame'] = walkout[wa_idx]['frameNum']
        event_dict['pred_probs'] = walkout[wa_idx]['topProb']
    elif len(possible_prod_det)==0: # no candidate, look for unknown event in which one of the candidates is correct
        possible_top_k_det = [dict(idx=idx, event=wa_event) for idx, wa_event in enumerate(walkout) if \
            wa_event['name'] in ['UNKNOWN', 'unrecognized_pending'] and walkout_status[idx]==ItemStatus.Unassigned and \
            abs(int(wa_event['frameNum']) - int(gt_event['frameNum'])) < max_frame_diff_valid and \
                wa_event['direction']==gt_event['direction']]
        if len(possible_top_k_det) > 0:
            # currently treat only nearest unknown in time. TODO-handle multiple unknown that meet criteria
            best_idx = np.argmin(np.array([abs(int(ppd['event']['frameNum']) - int(gt_event['frameNum'])) for ppd in possible_top_k_det]))
            wa_idx = possible_top_k_det[best_idx]['idx']
            wa_event = possible_top_k_det[best_idx]['event']
            top_2_walkout_products = [prod_prob[0] for prod_prob in wa_event['topProb'][:2]]
            ret_val = ItemStatus.ProductDetectedInTopK if gt_event['name'] in top_2_walkout_products \
                else ItemStatus.EventDetectedLowConf if wa_event['event_type'] in LOW_CONF_STATES \
                else ItemStatus.EventDetectedLowConf if wa_event['name']=='unrecognized_pending' \
                else ItemStatus.EventDetected
            if valid_states is None or ret_val in valid_states:
                walkout_status[wa_idx] = ret_val
            gt_val = ret_val
            event_dict['pred_name'] = walkout[wa_idx]['name']
            event_dict['pred_frame'] = walkout[wa_idx]['frameNum']
            event_dict['pred_probs'] = walkout[wa_idx]['topProb']
        else: # check for event detected but mis-classified, including hand detections
            possible_event_det = [dict(idx=idx, event=wa_event) for idx, wa_event in enumerate(walkout) if \
                walkout_status[idx]==ItemStatus.Unassigned and \
                abs(int(wa_event['frameNum']) - int(gt_event['frameNum'])) < max_frame_diff_valid and \
                    wa_event['direction']==gt_event['direction']]
            if len(possible_event_det) > 0: # event was detected
                best_idx = np.argmin(np.array([abs(int(ppd['event']['frameNum']) - int(gt_event['frameNum'])) for ppd in possible_event_det]))
                wa_idx = possible_event_det[best_idx]['idx']
                if valid_states is None or ItemStatus.EventDetected in valid_states:
                    walkout_status[wa_idx] = ItemStatus.EventDetected
                gt_val = ItemStatus.EventDetected
                event_dict['pred_name'] = walkout[wa_idx]['name']
                event_dict['pred_frame'] = walkout[wa_idx]['frameNum']
                event_dict['pred_probs'] = walkout[wa_idx]['topProb']
            else: # event missed
                gt_val = ItemStatus.EventMissed
                event_dict['pred_name'] = 'N/A'
                event_dict['pred_frame'] = 'N/A'
    
    event_dict['state'] = gt_val
    return gt_val, event_dict

def is_timeless_gt(gt):
    return len(gt) > 0 and gt[0]['frameNum']==None

def assign_frames_by_prediction(gt, pred):
    def _is_anchor(gt_pair, pred_pair):
        return gt_pair[0]['direction']==pred_pair[0]['direction'] and gt_pair[0]['name']==pred_pair[0]['name'] and \
               gt_pair[1]['direction']==pred_pair[1]['direction'] and gt_pair[1]['name']==pred_pair[1]['name']
    # Find anchors - 2 consequtive predictions where gt=pred
    last_anchor_idx_found = 0
    for gt_idx in range(len(gt) - 1):
        for pred_idx in range(last_anchor_idx_found, len(pred) - 1):
            # Check if anchor
            if _is_anchor(gt[gt_idx:gt_idx+2], pred[pred_idx:pred_idx+2]):
                last_anchor_idx_found = pred_idx
                if gt[gt_idx]['frameNum'] is None:
                    gt[gt_idx]['frameNum'] = pred[pred_idx]['frameNum']
                if gt[gt_idx+1]['frameNum'] is None:
                    gt[gt_idx+1]['frameNum'] = pred[pred_idx+1]['frameNum']
                break
    
    # Complete missing times with diffs between anchors
    for gt_idx in range(len(gt)):
        if gt[gt_idx]['frameNum'] is not None:
            continue
        # Find left anchor
        left_offset = None
        sub_list = gt[:gt_idx][::-1]
        for idx in range(len(sub_list)):
            if sub_list[idx]['frameNum'] is not None:
                left_offset = idx + 1
                break
        # Find right anchor
        right_offset = None
        sub_list = gt[gt_idx+1:]
        for idx in range(len(sub_list)):
            if sub_list[idx]['frameNum'] is not None:
                right_offset = idx + 1
                break
        
        if left_offset is not None and right_offset is not None:
            frame = (gt[gt_idx - left_offset]['frameNum'] + gt[gt_idx + right_offset]['frameNum']) // 2
        elif left_offset is not None:
            frame = gt[gt_idx - left_offset]['frameNum'] + (50 * left_offset)
            if frame < pred[-1]['frameNum']:
                frame = (frame + pred[-1]['frameNum']) // 2
        elif right_offset is not None:
            frame = gt[gt_idx + right_offset]['frameNum'] - (50 * right_offset)
            if frame > pred[0]['frameNum']:
                frame = (frame + pred[0]['frameNum']) // 2
        
        gt[gt_idx]['frameNum'] = frame
        
    return gt


def accuracy_per_sku(results_dict, analyzed_events):
    for analyzed_event in analyzed_events:
        state = analyzed_event['state']
        if state == ItemStatus.Ignored:
            continue
        product = analyzed_event['pred_name'] if state == ItemStatus.EventMadeUp else \
            analyzed_event['topProb'][0][0] if state == ItemStatus.EventFalseLowConf else analyzed_event['name']
        if product not in results_dict.keys():
            results_dict[product] = dict(events=0, successes=0, true_interactions=0, false_interactions=0,
                                            errors=0, cls_errors=0, misses=0, made_ups=0)
        results_dict[product]['events'] += 1
        if state == ItemStatus.ProductDetected:
            results_dict[product]['successes'] += 1
        elif state == ItemStatus.EventMissed:
            results_dict[product]['misses'] += 1
            results_dict[product]['errors'] += 1
        elif state == ItemStatus.EventMadeUp:
            results_dict[product]['made_ups'] += 1
            results_dict[product]['errors'] += 1
        elif state == ItemStatus.EventDetected:
            results_dict[product]['cls_errors'] += 1
            results_dict[product]['errors'] += 1
        elif state in (ItemStatus.EventDetectedLowConf, ItemStatus.ProductDetectedInTopK):
            results_dict[product]['true_interactions'] += 1
        elif state == ItemStatus.EventFalseLowConf:
            results_dict[product]['false_interactions'] += 1
    return results_dict


def shopping_accuracy_msco(walkout, gt, ignore_items=['aa_hand'], ignore_made_ups_before_first_scan=True, last_frame=None):
    if last_frame is None and len(walkout) > 0:
        last_frame = walkout[-1]['frameNum']

    # Filter non-product (e.g, hand) from GT if exist
    gt = [gt_item for gt_item in gt if gt_item['name'] not in ignore_items]

    walkout_status = [ItemStatus.Unassigned] * len(walkout)
    gt_status = [ItemStatus.Unassigned] * len(gt)
    analyzed_events = []

    # Assign only valid 'in' predictions
    gt_idx = 0
    for wo_idx, wo_event in enumerate(walkout):
        if wo_event['direction'] != 'in':
            continue
        gt_val, gt_idx, event_info = assign_event_msco(gt_idx, gt, gt_status, wo_event, mode='valid_insertion')
        if gt_val==ItemStatus.ProductDetectedInTopK:
            analyzed_events.append(event_info)
            walkout_status[wo_idx] = gt_val
            gt_status[gt_idx] = gt_val
            gt_idx += 1

    # Assign only valid 'out' predictions
    gt_idx = len(gt) - 1
    for wo_idx, wo_event in reversed(list(enumerate(walkout))):
        if wo_event['direction'] != 'out':
            continue
        gt_val, gt_idx, event_info = assign_event_msco(gt_idx, gt, gt_status, wo_event, mode='valid_removal')
        if gt_val==ItemStatus.ProductDetectedInTopK:
            analyzed_events.append(event_info)
            walkout_status[wo_idx] = gt_val
            gt_status[gt_idx] = gt_val
            gt_idx -= 1

    # Fix gt shifts
    shift_allowed = 100
    unassigned_gt = [(i, gt_event) for i, gt_event in enumerate(gt) if gt_status[i]==ItemStatus.Unassigned]
    unassigned_wo = [(i, wo_event) for i, wo_event in enumerate(walkout) if walkout_status[i]==ItemStatus.Unassigned]
    if len(unassigned_gt) > 0 and len(unassigned_wo) > 0:
        gt_idx = 0
        for wo_idx, wo_event in unassigned_wo:
            while True:
                if gt_idx >= len(unassigned_gt):
                    break
                if abs(wo_event['frameNum'] - gt[unassigned_gt[gt_idx][0]]['frameNum']) <= shift_allowed and \
                    wo_event['direction'] == gt[unassigned_gt[gt_idx][0]]['direction']:
                    # check no assigned events are between these unassigned
                    if wo_event['frameNum'] > gt[unassigned_gt[gt_idx][0]]['frameNum']:
                        check_gt_idx = unassigned_gt[gt_idx][0] + 1
                        check_wo_idx = wo_idx - 1
                        if (check_gt_idx < len(gt) and wo_event['frameNum'] > gt[check_gt_idx]['frameNum'] > gt[unassigned_gt[gt_idx][0]]['frameNum']) or \
                                check_wo_idx >= 0 and wo_event['frameNum'] > walkout[check_wo_idx]['frameNum'] > gt[unassigned_gt[gt_idx][0]]['frameNum']:
                            break
                    else:
                        check_gt_idx = unassigned_gt[gt_idx][0] - 1
                        check_wo_idx = wo_idx + 1
                        if (check_gt_idx >= 0 and gt[unassigned_gt[gt_idx][0]]['frameNum'] > gt[check_gt_idx]['frameNum'] > wo_event['frameNum']) or \
                                check_wo_idx >= 0 and gt[unassigned_gt[gt_idx][0]]['frameNum'] > walkout[check_wo_idx]['frameNum'] > wo_event['frameNum']:
                            break
                    walkout_status[wo_idx] = ItemStatus.ProductDetectedInTopK
                    gt_status[unassigned_gt[gt_idx][0]] = ItemStatus.ProductDetectedInTopK
                    event_dict = copy.copy(gt[unassigned_gt[gt_idx][0]])
                    event_dict['pred_name'] = wo_event['name']
                    event_dict['pred_frame'] = wo_event['frameNum']
                    event_dict['pred_probs'] = wo_event['topProb']
                    event_dict['state'] = ItemStatus.ProductDetectedInTopK
                    analyzed_events.append(event_dict)
                    gt_idx += 1
                    break
                elif wo_event['frameNum'] > gt[unassigned_gt[gt_idx][0]]['frameNum']:
                    gt_idx += 1
                else:
                    break

    # All remaining unassigned gt events are misses
    for gt_idx, gt_st in enumerate(gt_status):
        if gt_st == ItemStatus.Unassigned:
            gt_st = ItemStatus.EventMissed if gt[gt_idx]['frameNum'] < last_frame + 50 else ItemStatus.Ignored
            event_info = copy.copy(gt[gt_idx])
            event_info['pred_name'] = 'N/A'
            event_info['pred_frame'] = 'N/A'
            event_info['state'] = gt_st
            analyzed_events.append(event_info)

    # All remaining unassigned walkout events are made ups
    for wo_idx, wo_st in enumerate(walkout_status):
        if wo_st == ItemStatus.Unassigned:
            if ignore_made_ups_before_first_scan and walkout[wo_idx]['frameNum'] < gt[0]['frameNum']:
                continue
            wo_st = ItemStatus.EventMadeUp
            event_info = copy.copy(walkout[wo_idx])
            event_info['state'] = wo_st
            event_info['pred_name'] = walkout[wo_idx]['name']
            event_info['pred_frame'] = walkout[wo_idx]['frameNum']
            event_info['name'] = 'N/A'
            analyzed_events.append(event_info)

    merged_events = sorted(analyzed_events, key=lambda k: int(k['frameNum']))
    return merged_events


def shopping_accuracy(walkout, gt, ignore_items=['aa_hand'], shift_gt_frames=True, location_predictions=None,
                      ignore_made_ups_before_first_scan=True):
    '''
    Compare and analyze predicted shopping list w.r.t GT shopping list
    '''
    # Filter non-product (e.g, hand) from GT if exist
    gt = [gt_item for gt_item in gt if gt_item['name'] not in ignore_items]

    # Assign frame numbers if missing
    if is_timeless_gt(gt):
        gt = assign_frames_by_prediction(gt, walkout)

    walkout_status = [ItemStatus.Unassigned] * len(walkout)
    gt_status = [ItemStatus.Unassigned] * len(gt)

    # First pass - assign only correct classifications
    analyzed_events = []
    for gt_idx, gt_event in enumerate(gt):
        gt_val, event_info = assign_event(gt_event, walkout, walkout_status, valid_states=[ItemStatus.ProductDetected])
        if gt_val==ItemStatus.ProductDetected:
            analyzed_events.append(event_info)
            gt_status[gt_idx] = gt_val

    # First pass(b) - assign only correct classifications with higher frame difference
    # This comes to compensate on early/late barcode scanning annotation
    for ts in (60, 120, 240):
        for gt_idx, gt_event in enumerate(gt):
            if gt_status[gt_idx]==ItemStatus.Unassigned:
                gt_val, event_info = assign_event(gt_event, walkout, walkout_status, valid_states=[ItemStatus.ProductDetected], max_frame_diff_valid=ts)
                if gt_val==ItemStatus.ProductDetected:
                    analyzed_events.append(event_info)
                    gt_status[gt_idx] = gt_val
    

    # Assign successful event - nearest correct product if exists or near event
    for ts in (30, 60, 120, 240):
        for gt_idx, gt_event in enumerate(gt):
            if gt_status[gt_idx]==ItemStatus.Unassigned:
                gt_val, event_info = assign_event(gt_event, walkout, walkout_status,
                                                  valid_states=[ItemStatus.EventDetected,
                                                                ItemStatus.ProductDetectedInTopK,
                                                                ItemStatus.EventDetectedLowConf],
                                                  max_frame_diff_valid=ts)
                if gt_val in (ItemStatus.EventDetected, ItemStatus.ProductDetectedInTopK, ItemStatus.EventDetectedLowConf):
                    analyzed_events.append(event_info)
                    gt_status[gt_idx] = gt_val

    # Assign rest of events - nearest correct product if exists or near event
    for ts in (30, 60, 120, 240):
        for gt_idx, gt_event in enumerate(gt):
            if gt_status[gt_idx]==ItemStatus.Unassigned:
                gt_val, event_info = assign_event(gt_event, walkout, walkout_status, max_frame_diff_valid=ts)
                analyzed_events.append(event_info)
                gt_status[gt_idx] = gt_val
    
    # Remove ignored items that are unassigned
    remove_mask = [ev['name'] in ignore_items and st==ItemStatus.Unassigned for ev,st in zip(walkout, walkout_status)]
    walkout_status = [st for rm, st in zip(remove_mask, walkout_status) if not rm]
    walkout = [ev for rm, ev in zip(remove_mask, walkout) if not rm]

    # Fix unknowns / errors of 'out' events if location prediction is correct
    if location_predictions is not None and len(location_predictions) > 0:
        for analyzed_event in analyzed_events:
            if analyzed_event['state'] in (ItemStatus.ProductDetectedInTopK, ItemStatus.EventDetected) and \
                    analyzed_event['direction'] == 'out':
                out_gt_prod = analyzed_event['name']
                event_frame_out = analyzed_event['pred_frame']
                event_frame_in = [pred['in_frame'] for pred in location_predictions if
                                  pred['out_frame'] == event_frame_out]
                if len(event_frame_in) == 1:
                    optional_in_events = [in_event for in_event in analyzed_events if
                                          in_event['pred_frame'] == event_frame_in[0] and in_event['direction'] == 'in']
                    if len(optional_in_events) == 1:
                        in_gt_prod = optional_in_events[0]['name']
                        if out_gt_prod == in_gt_prod:
                            analyzed_event['state'] = ItemStatus.ProductDetected

    # Mark the rest of unassigned (except low confidence events) as made-up event
    made_up_events = []
    for wa_idx in range(len(walkout_status)):
        if walkout_status[wa_idx]==ItemStatus.Unassigned and walkout[wa_idx]['name']!='low_conf_item':
            if walkout[wa_idx]['event_type'] in (EventType.NotConfidentEventTriggerUnrecognized,
                                                 EventType.NotConfidentEventTriggerPlu,
                                                 EventType.NotConfidentEventTriggerBarcode):
                walkout_status[wa_idx] = ItemStatus.EventFalseLowConf
                walkout[wa_idx]['state'] = ItemStatus.EventFalseLowConf
            else:
                walkout_status[wa_idx] = ItemStatus.EventMadeUp
                walkout[wa_idx]['state'] = ItemStatus.EventMadeUp
            walkout[wa_idx]['pred_name'] = walkout[wa_idx]['name']
            walkout[wa_idx]['pred_frame'] = walkout[wa_idx]['frameNum']
            walkout[wa_idx]['name'] = 'N/A'
            made_up_events.append(walkout[wa_idx])

    # ignore out/in or in/out made ups of the same product and made ups before the first scanned product
    if ignore_made_ups_before_first_scan:
        mu_idx = 0
        while mu_idx < len(made_up_events):
            if len(gt) > 0 and gt[0]['frameNum'] > made_up_events[mu_idx]['frameNum']:
                made_up_events[mu_idx]['state'] = ItemStatus.Ignored
            elif mu_idx + 1 < len(made_up_events) and \
                    made_up_events[mu_idx]['pred_name'] == made_up_events[mu_idx + 1]['pred_name'] and\
                    made_up_events[mu_idx]['direction'] != made_up_events[mu_idx + 1]['direction'] and\
                    abs(made_up_events[mu_idx]['frameNum'] - made_up_events[mu_idx + 1]['frameNum']) < 300:
                made_up_events[mu_idx]['state'] = ItemStatus.Ignored
                made_up_events[mu_idx + 1]['state'] = ItemStatus.Ignored
                mu_idx += 1
            mu_idx += 1

    # Mark unassigned low confidence events as False low conf
    false_low_conf_events = []
    for wa_idx in range(len(walkout_status)):
        if walkout_status[wa_idx]==ItemStatus.Unassigned:
            if gt[0]['frameNum'] > walkout[wa_idx]['frameNum']:
                walkout[wa_idx]['state'] = ItemStatus.Ignored
            else:
                assert walkout[wa_idx]['name']=='low_conf_item'
                walkout_status[wa_idx] = ItemStatus.EventFalseLowConf
                walkout[wa_idx]['state'] = ItemStatus.EventFalseLowConf
                walkout[wa_idx]['pred_name'] = 'low_conf_item'
                walkout[wa_idx]['pred_frame'] = walkout[wa_idx]['frameNum']
                walkout[wa_idx]['name'] = 'N/A'
                false_low_conf_events.append(walkout[wa_idx])
    
    # Move detected low confidence events from mis-classified to low conf detection
    for ev_idx in range(len(analyzed_events)):
        if analyzed_events[ev_idx]['pred_name']=='low_conf_item' and analyzed_events[ev_idx]['state']==ItemStatus.EventDetected:
            analyzed_events[ev_idx]['state'] = ItemStatus.EventDetectedLowConf
    
    # Remove in->out/out->in events of the same product if they are very near in time, in case
    # None is detected
     # Make sure events before made-ups are sorted
    analyzed_events = sorted(analyzed_events, key=lambda k: int(k['frameNum']))
    idx_to_remove = []
    for gt_idx in range(len(gt[:-1])):
        if fast_in_out(gt[gt_idx], gt[gt_idx+1]):
            if gt_status[gt_idx]==ItemStatus.EventMissed and gt_status[gt_idx+1]==ItemStatus.EventMissed:
                idx_to_remove +=[gt_idx, gt_idx+1]
                gt_idx+=1
    analyzed_events = [ev for idx, ev in enumerate(analyzed_events) if idx not in idx_to_remove]

    # Shift GT timestamps if needed
    if shift_gt_frames:
        for gt_idx in range(len(gt)):
            for el in analyzed_events:
                if gt[gt_idx]['frameNum']==el['frameNum'] and el['state']!=ItemStatus.EventMissed:
                    gt[gt_idx]['frameNum'] = el['pred_frame']
                    break

    # Merge made-up events into all events
    merged_events = sorted(analyzed_events + made_up_events + false_low_conf_events, key=lambda k: int(k['frameNum']))

    # Find miss-madeup pairs that originate from bad GT time
    valid_pairs = ((ItemStatus.EventMissed, ItemStatus.EventMadeUp), (ItemStatus.EventMadeUp, ItemStatus.EventMissed))
    test_idx = [idx for idx,(e1,e2) in enumerate(zip(merged_events[:-1], merged_events[1:])) if (e1['state'], e2['state']) in valid_pairs and \
        e1['direction']==e2['direction']]
    if len(test_idx) > 0:
        del_indices = []
        for idx in test_idx:
            miss_first = merged_events[idx]['state'] == ItemStatus.EventMissed
            if miss_first:
                gt_event = merged_events[idx]
                pred = [merged_events[idx+1]]
                change_idx = idx + 1
                del_idx = idx
            else:
                gt_event = merged_events[idx+1]
                pred = [merged_events[idx]]
                change_idx = idx
                del_idx = idx + 1

            pred[0]['name'] = pred[0]['topProb'][0][0]
            ws = [ItemStatus.Unassigned]
            gt_val, event_info = assign_event(gt_event, pred, ws, max_frame_diff_valid=1500)
            merged_events[change_idx] = event_info
            if idx + 1 in test_idx and gt_val not in [ItemStatus.EventMissed, ItemStatus.EventMadeUp]:
                test_idx.remove(idx + 1)
            if gt_val!=ItemStatus.EventMissed:
                del_indices.append(del_idx)

        merged_events = [m for idx,m in enumerate(merged_events) if idx not in del_indices]

    return merged_events

def fast_in_out(ev1, ev2, near_event_max_frames=15):
    return ev1['name']==ev2['name'] and ev1['direction']!=ev2['direction'] and \
        abs(int(ev1['frameNum']) - int(ev2['frameNum'])) < near_event_max_frames

def summarize_recording_metrics(analyzed_events, catalog, excel_writer=None, log_errors=True, ignore_products=[],
                                log_interactions=True):
    """
    """

    if log_errors:
        print('errors description')
    for event in analyzed_events:
        if event['state']!=ItemStatus.EventMadeUp:
            event_dict = {
                'frameNum':           event['frameNum'],
                'eventType':          event["direction"],
                'groundTruth_barcode': event['name'],
                'groundTruth_name': catalog.get_english_name(event['name']),
            }
            if event['state']==ItemStatus.ProductDetected:
                event_dict['product_upc'] = event['pred_name']
                event_dict['product_barcode'] = catalog.get_barcode(event['pred_name']),
                event_dict['prediction_name'] = catalog.get_english_name(event['pred_name'])
                event_dict['success'] =  'Yes'
                event_dict['details'] = ''
            elif event['state']==ItemStatus.ProductDetectedInTopK and log_interactions:
                event_dict['product_upc'] = event['pred_name']
                event_dict['prediction_name'] = catalog.get_english_name(event['pred_name'])
                event_dict['success'] =  'No'
                event_dict['details'] = 'Classified as Unknown'
                if log_errors:
                    log_prefix = 'Event Unknown(Product @ top-K)'
                    if event['name'] in ignore_products:
                        log_prefix = '[Non-supported Product]' + log_prefix
                    print('{}: walkout saw {} going [{}], frame {} whereas GT is {}:{}({}) in frame {}'.format(
                        log_prefix,
                        event['pred_name'],
                        event['direction'],
                        event['pred_frame'],
                        event['name'],
                        catalog.get_barcode(event['name']),
                        catalog.get_english_name(event['name']),
                        event['frameNum']))
            elif event['state']==ItemStatus.EventDetectedLowConf and log_interactions:
                event_dict['product_upc'] = event['pred_name']
                event_dict['prediction_name'] = catalog.get_english_name(event['pred_name'])
                event_dict['product_barcode'] = catalog.get_barcode(event['pred_name']),
                event_dict['success'] =  'No'
                event_dict['details'] = 'Event detected with low confidence. requires user interaction'
                if log_errors:
                    log_prefix = 'Event with low confidence'
                    if event['name'] in ignore_products:
                        log_prefix = '[Non-supported Product]' + log_prefix
                    print('{}: walkout saw SOMETHING going [{}], frame {} whereas GT is {}({}) in frame {}'.format(
                        log_prefix,
                        event['direction'],
                        event['pred_frame'],
                        event['name'],
                        catalog.get_english_name(event['name']),
                        event['frameNum']))
            elif event['state']==ItemStatus.EventDetected:
                event_dict['product_upc'] = event['pred_name']
                event_dict['product_barcode'] = catalog.get_barcode(event['pred_name']),
                event_dict['prediction_name'] = catalog.get_english_name(event['pred_name'])
                event_dict['success'] =  'No'
                event_dict['details'] = 'Miss-Classified'
                if log_errors:
                    log_prefix = 'Event Miss-Clasified'
                    if event['name'] in ignore_products:
                        log_prefix = '[Non-supported Product]' + log_prefix
                    print('{}: walkout saw {}({}) going [{}], frame {} whereas GT is {}:{}({}) in frame {}'.format(
                        log_prefix,
                        event['pred_name'],
                        catalog.get_english_name(event['pred_name']),
                        event['direction'],
                        event['pred_frame'],
                        event['name'],
                        catalog.get_barcode(event['name']),
                        catalog.get_english_name(event['name']),
                        event['frameNum']))
            elif event['state']==ItemStatus.EventMissed:
                event_dict['product_upc'] = 'N/A'
                event_dict['product_barcode'] = 'N/A'
                event_dict['prediction_name'] = 'N/A'
                event_dict['success'] =  'No'
                event_dict['details'] = 'Event was not caught by walkout'
                if log_errors:
                    log_prefix = 'Event missed'
                    if event['name'] in ignore_products:
                        log_prefix = '[Non-supported Product]' + log_prefix
                    print('{}: GT has {}:{}({}) going[{}] in frame {}'.format(
                        log_prefix,
                        event['name'],
                        catalog.get_barcode(event['name']),
                        catalog.get_english_name(event['name']),
                        event['direction'],
                        event['frameNum']))
            elif event['state']==ItemStatus.EventFalseLowConf:
                if log_errors:
                    print('False positive low confidence: walkout saw SOMETHING going [{}], frame {}'.format(
                            event['direction'],event['pred_frame']))
                continue
        else:
            event_dict = {
                'frameNum':           event['frameNum'],
                'eventType':          event["direction"],
                'groundTruth_barcode': 'N/A',
                'groundTruth_name': 'N/A',
                'product_upc': event['pred_name'],
                'product_barcode': catalog.get_barcode(event['pred_name']),
                'prediction_name': catalog.get_english_name(event['pred_name']),
                'success': 'No',
                'details': 'Walkout reported on event when it did not really occur'
            }
            if log_errors:
                print('Event made up: Walkout has {}:{}({}) going[{}] in frame {}'.format(
                    event['pred_name'],
                    catalog.get_barcode(event['pred_name']),
                    catalog.get_english_name(event['pred_name']),
                    event['direction'],
                    event['frameNum']))

        if excel_writer is not None:
            excel_writer.add_event(event_dict)
    cls_success = len([1 for ev in analyzed_events if ev['state']==ItemStatus.ProductDetected])
    cls_total = len([1 for ev in analyzed_events if ev['state'] in (ItemStatus.ProductDetected, ItemStatus.EventDetected, ItemStatus.ProductDetectedInTopK, ItemStatus.EventDetectedLowConf)])
    md_success = len([1 for ev in analyzed_events if ev['state'] in (ItemStatus.ProductDetected, ItemStatus.EventDetected, ItemStatus.ProductDetectedInTopK, ItemStatus.EventDetectedLowConf)])
    md_total = len(analyzed_events) - len([1 for ev in analyzed_events if ev['state'] in (ItemStatus.EventFalseLowConf, ItemStatus.Ignored)])
    unknowns = len([1 for ev in analyzed_events if ev['state'] in (ItemStatus.ProductDetectedInTopK, ItemStatus.EventDetectedLowConf)])
    errors = len([1 for ev in analyzed_events if ev['state'] in (ItemStatus.EventMissed, ItemStatus.EventMadeUp, ItemStatus.EventDetected)])
    n_missed = len([1 for ev in analyzed_events if ev['state']==ItemStatus.EventMissed])
    n_madeup = len([1 for ev in analyzed_events if ev['state']==ItemStatus.EventMadeUp])
    n_false_low_conf = len([1 for ev in analyzed_events if ev['state']==ItemStatus.EventFalseLowConf])

    if excel_writer is not None:
        excel_writer.add_summary(errors, unknowns, md_total)

    out_dict = {'clsSuccess': cls_success,
                'clsTested': cls_total,
                'MDSuccess': md_success,
                'MDTested':md_total,
                'unknownItems': unknowns,
                'n_missed':n_missed,
                'n_madeup': n_madeup,
                'errors': errors,
                'n_false_low_conf': n_false_low_conf}

    return out_dict

def apply_split(split, catalog):
    # Filter ops
    def _split_in_out_events(ev, val, catalog, att):
        return [e for ee in ev for e in ee if e['direction']==val]
    def _split_by_attribute(ev, val, catalog, att):
        all_events = [e for ee in ev for e in ee if e['name'] not in ('UNKNOWN', 'low_conf_item')] # ignore unknowns
        filt_events = []
        for e in all_events:
            name_key = 'name' if e['name'] not in ('N/A', 'low_conf_item') else 'pred_name'
            if e[name_key]=='low_conf_item' or (name_key=='pred_name' and e[name_key]=='UNKNOWN'):
                continue
            try:
                if catalog.attribute_dict[att][e[name_key]]==val:
                    filt_events.append(e)
            except:
                print('Warning: ignoring event analysis on split by %s: ' % att, e)
        return filt_events

    if split['type']=='event':
        if split['val']=='dir':
            filter_fn = _split_in_out_events
            split_vals = ('in', 'out')
    elif split['type']=='attribute':
        filter_fn = _split_by_attribute
        split_vals = [v for v in set(catalog.attribute_dict[split['val']].values()) if v not in (None, '', 'none')]
    
    return filter_fn, split_vals

def aggregate_events(all_events, catalog, split=None, top_md_err=None, top_cls_err=None):
    split_results = dict()
    if split is not None:
        if split['type']=='attribute' and split['val']=='product_name':
            # Enable this split also without catalog
            unique_prod_names = [e['name'] for ee in all_events for e in ee if e['name'] not in ('UNKNOWN', 'low_conf_item')]
            unique_prod_names = tuple(set(unique_prod_names))
            att_dict = {pn : pn for pn in unique_prod_names}
            catalog.set_attribute('product_name', att_dict)
        filter_fn, split_vals = apply_split(split, catalog)
    for unique_val in split_vals:
        filtered_events = filter_fn(all_events, unique_val, catalog, split['val'])
        filtered_results = summarize_recording_metrics(filtered_events, catalog, log_errors=False)
        split_results[unique_val] = filtered_results
    if top_md_err is not None:
        split_results = {k: v for k, v in sorted(split_results.items(), key=lambda item: item[1]['MDSuccess']) \
            if v['n_missed'] + v['n_madeup'] > 0}
        split_results = {k: v for k, v in sorted(split_results.items(), key=lambda item: item[1]['MDSuccess'])[:top_md_err]}
    elif top_cls_err is not None:
        split_results = {k: v for k, v in sorted(split_results.items(), key=lambda item: item[1]['clsSuccess']) \
            if v['clsTested'] > 0 and v['clsTested'] > v['clsSuccess']}
        split_results = {k: v for k, v in sorted(split_results.items(), key=lambda item: item[1]['clsSuccess'])[:top_cls_err]}

    return split_results

def get_all_products(all_events):
    all_products = []
    for single_rec_ev in all_events:
        for single_ev in single_rec_ev:
            if single_ev['name'] not in all_products and \
                single_ev['name'] not in ('N/A', 'low_conf_item'):
                all_products.append(single_ev['name'])
    return all_products

def get_only_classifier_supported_events(all_events, classifier):
    all_products = get_all_products(all_events)
    # Get products not supported by classifier
    non_supported_products = [prod for prod in all_products if not classifier.is_class_supported(prod)]
    all_supported_events = []
    all_supported = len(non_supported_products)==0
    if not all_supported:
        print('\033[93m','Non-Supported products by classifier:', non_supported_products, '\033[00m')
        for single_rec_ev in all_events:
            all_rec_events = [ev for ev in single_rec_ev if ev['name'] not in non_supported_products]
            all_supported_events.append(all_rec_events)
    else:
        print('\031[92m','All products by classifier:', '\033[00m')
    return all_supported_events, all_supported


def get_failure_events(analyzed_events):
    return [ae for ae in analyzed_events if ae['state']!=ItemStatus.ProductDetected]

if __name__=='__main__':
    import pickle
    from cart_blocks import catalog as Catalog
    catalog = Catalog.Catalog()
    sample_result = pickle.load(open('sample_shopping_result.pkl', 'rb'))
    analyzed_events = shopping_accuracy(sample_result['walkout'], sample_result['gt'])
    out_dict = summarize_recording_metrics(analyzed_events, catalog)