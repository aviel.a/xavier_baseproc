import numpy as np
import cv2
import enum
from cv_blocks.cart_bottom.static_snapshot_detector import SDState
import os
import yaml
from cv_blocks.misc import aws_download
from config import config
try:
    cmd = 'import tensorrt_lib.bin._swig_yaeen as swig_cls'
    exec(cmd)
except:
    raise NameError('missing swig_yaeen compilation for given arch,FP,training data')

class CartInOutDecider(object):
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.sample_n_images = config.get('sample_n_images', 2)
            self.bottom_crop_prob_th = config.get('bottom_crop_prob_th', 0.3)
            self.cb_prob_th = config.get('cb_prob_th', 0.3)

    def __init__(self, cio_config=dict()):
        self.params = self.Config(cio_config)
        self.cls = CartInOutClassifier()

    def is_inside_cart(self, event, cam):
        # Take relevant crops from event - TODO: Take lowest images - don't assume single cam
        assert cam in ('front', 'back')
        # Take bottom crops, prioritize Detector crops
        y_vals = np.array([e.coords.y1  if e.detector_conf > 0 else e.coords.y1 - 1. for e in event])
        bottom_crop_idx = np.argsort(-y_vals)[:self.params.sample_n_images]
        sample_images = [event[ix].context_image if hasattr(event[ix], 'context_image') and event[ix].context_image is not None else event[ix].cropped_image for ix in bottom_crop_idx]
        p_inside_cart = self.cls.predict(sample_images, cam)

        # check conditions
        images_out_of_cart =  p_inside_cart.mean() < self.params.bottom_crop_prob_th
        bottom_very_confident = p_inside_cart.min() < 0.1 * self.params.bottom_crop_prob_th
        out_of_cart = images_out_of_cart or bottom_very_confident

        return not out_of_cart

    def is_cb_inside_cart(self, cb):
        assert len(cb) == 1
        # CB image is in RGB format, needs to be converted to BGR
        # TODO - fix predictor format
        img = cb[0].cropped_image
        cb_duplicated = [img, img]
        p_inside_cart = self.cls.predict(cb_duplicated)
        return not p_inside_cart[0] < self.params.cb_prob_th


class CIOState(enum.Enum):
    InsideCart = 0
    OutsideCart = 1

class CartInOutClassifier(object):
    '''
    A class implementing inside cart/out side cart crop classifier
    '''
    def __init__(self, weights=None, img_size=(64,64), use_fp16=True, n_classes=2, batch_size=2, debug=False, sync=True):
        if weights is None:
            weights = config.DEFAULT_CART_IN_OUT_CLASSIFIER_WEIGHTS
        self.weights = weights
        print('initializing Cart In/Out Classifier')
        self.use_fp16 = use_fp16
        self.batch_size = batch_size
        self.img_size = img_size
        self.load_classifier(weights, sync=sync)
        self.n_classes = n_classes
        self.debug = debug

    def load_classifier(self, weights, sync):
        weights, conf_file = self.get_model(weights, sync=sync)
        with open(conf_file) as config_f:
            config = yaml.safe_load(config_f)
        if 'img_size' in config:
            self.img_size = tuple(config['img_size'])
        if 'class_names' in config:
            self.n_classes = len(config['class_names'])
        if 'hist_eq' in config:
            self.hist_eq = config['hist_eq']
        else:
            self.hist_eq = False
        self.input_channels = config.get('input_channels', 3)
        self.frames_stacked = config.get('frames_stacked', 1)
        self.trained_on_rgb = config.get('trained_on_rgb', False)
        self.is_gray = self.input_channels==self.frames_stacked

        print('initializing  Cart In/Out Classifier')
        self.engine = swig_cls.loadModelAndCreateEngine(weights, self.use_fp16, self.batch_size)
        print('loading context')
        self.context = swig_cls.create_context(self.engine)
        print('finished loading context')

    def get_model(self, weights, sync):
        model_root_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'models')
        model_name, model_ext = os.path.splitext(weights)
        assert model_ext in ('.pth', '.onnx')
        full_weights_path = os.path.join(model_root_dir, model_name, weights)
        full_conf_path = full_weights_path.replace(model_ext, '_config.yaml')
        if not os.path.exists(full_weights_path) and sync:
            full_model_dir = os.path.dirname(full_weights_path)
            os.makedirs(full_model_dir, exist_ok=True)
            # Get model & config file
            aws_download.download("models/cart_in_out_classifier", model_name, full_model_dir)
        
        assert os.path.exists(full_weights_path) and os.path.exists(full_conf_path)
        return full_weights_path, full_conf_path
    
    def predict(self,img, cam='front'):
        if not isinstance(img, (list, tuple)):
            img = [img]
        bs = len(img)
        if self.is_gray:
            img = [cv2.cvtColor(i, cv2.COLOR_RGB2GRAY) for i in img]
        elif not self.trained_on_rgb:
            img = [cv2.cvtColor(i, cv2.COLOR_RGB2BGR) for i in img]
        img_re = [cv2.resize(i, self.img_size) for i in img]
        assert bs <= self.batch_size
        img = np.stack(img_re)
        img = img.astype(np.float32) / 255.
        if not self.is_gray:
            img = np.transpose(img[...,::-1], (0, 3, 1, 2))
        data = img.ravel()
        probs = np.zeros(self.n_classes * bs, np.float32)
        swig_cls.execute(self.engine,self.context,data,probs, bs)
        probs = np.reshape(probs, (bs, -1))
        p_inside_cart = probs[:, CIOState.InsideCart.value]
        print('Inside cart probs:', p_inside_cart)
        if self.debug:
            self.visualize_input(img_re, p_inside_cart, cam)
        return p_inside_cart
    
    def visualize_input(self, img_re, p_event, cam):
        input_viz = []
        n_images = img_re[0].shape[-1]
        for cam_input, prob in zip(img_re, p_event):
            single_cam_viz = cv2.resize(cam_input, (256,256))
            if self.is_gray:
                single_cam_viz = cv2.cvtColor(single_cam_viz, cv2.COLOR_GRAY2BGR)
            cv2.putText(single_cam_viz, 'p=%.2f' % prob, (5, 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255,0,0))
            input_viz.append(single_cam_viz)
            
        input_viz = np.vstack(input_viz)
        cv2.imshow("Cart net input - {}".format(cam), input_viz)

    @staticmethod
    def get_cart_in_out_context_image(bbox, full_size_image, default_size=64, rotate=False, none_if_big=True):
        x, y, w, h = bbox
        shape = full_size_image.shape
        wnoise_l, wnoise_r, wnoise_u, wnoise_d = 0, 0, 0, 0
        if w * h > default_size * default_size:
            if none_if_big:
                return None
            context_image = full_size_image[y:y + h, x:x + w]
        else:
            if w >= default_size:
                x_min, x_max = int(x + w / 2 - default_size / 2), int(x + w / 2 + default_size / 2)
            else:
                x_min = int(max(0, x + w / 2 - default_size / 2))
                x_max = int(min(shape[1], x + w / 2 + default_size / 2))
                if x_min == 0:
                    wnoise_l = default_size - (x_max - x_min)
                elif x_max == shape[1]:
                    wnoise_r = default_size - (x_max - x_min)
            if h >= default_size:
                y_min, y_max = int(y + h / 2 - default_size / 2), int(y + h / 2 + default_size / 2)
            else:
                y_min = int(max(0, y + h / 2 - default_size / 2))
                y_max = int(min(shape[0], y + h / 2 + default_size / 2))
                if y_min == 0:
                    wnoise_u = default_size - (y_max - y_min)
                elif y_max == shape[0]:
                    wnoise_d = default_size - (y_max - y_min)
            context_image = full_size_image[y_min:y_max, x_min:x_max]
            # add white noise if the crop is near the edge
            if wnoise_l > 0:
                context_image = np.hstack((np.random.uniform(0, 255, (context_image.shape[0], wnoise_l, 3)),context_image)).astype(np.uint8)
            elif wnoise_r > 0:
                context_image = np.hstack((context_image, np.random.uniform(0, 255, (context_image.shape[0], wnoise_r, 3)).astype(np.uint8)))
            if wnoise_u > 0:
                context_image = np.vstack((np.random.uniform(0, 255, (wnoise_u, context_image.shape[1], 3)), context_image)).astype(np.uint8)
            elif wnoise_d > 0:
                context_image = np.vstack((context_image, np.random.uniform(0, 255, (wnoise_d, context_image.shape[1], 3)))).astype(np.uint8)
        if context_image.shape != (default_size, default_size, 3):
            context_image = cv2.resize(context_image, (default_size, default_size))
        if rotate:
            context_image = cv2.rotate(context_image, cv2.ROTATE_90_CLOCKWISE)
        context_image = cv2.cvtColor(context_image, cv2.COLOR_BGR2RGB)
        return context_image

if __name__=='__main__':
    img1_path = 'data/single_crop_sample_out.jpg'
    img1 = cv2.imread(img1_path)
    img2_path = 'data/single_crop_sample_in.jpg'
    img2 = cv2.imread(img2_path)
    my_cio_cls = CartInOutClassifier(debug=True)
    my_cio_cls.predict([img1, img2])