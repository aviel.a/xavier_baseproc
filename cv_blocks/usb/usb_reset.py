import subprocess as sp
import os
import re
import argparse

def reset_usb_device(bus, device):
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    bin_path = './' + os.path.join(curr_dir, 'usb_reset')
    cmd = ['sudo ./usb_reset /dev/bus/usb/%s/%s' % (bus, device)]
    print(cmd)
    p = sp.Popen(cmd, stdout=sp.PIPE,shell=True, cwd=curr_dir)
    p.wait()

def find_usb_by_name(name):
    p = sp.Popen(['lsusb'], stdout=sp.PIPE)
    output = p.stdout.read()
    output = output.decode('utf-8').split('\n')
    usb_lines = [x for x in output if name in x]
    device_list = []
    for single_usb in usb_lines:
        m = re.match(r'Bus (?P<bus>.*) Device (?P<device>.*): ID*', single_usb)
        device_list.append(dict(bus=m.group('bus'), device=m.group('device')))
    return device_list

def reset_usb_by_name(name):
    ret = find_usb_by_name(name)
    for r in ret:
        reset_usb_device(r['bus'], r['device'])

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='parser')
    parser.add_argument('--name', help='usb name to reset',type=str, required=True)
    args = parser.parse_args()

    reset_usb_by_name(args.name)