import pickle
import os
from datetime import datetime
from config import config
import subprocess
from cv_blocks.events.movement_vector import MovementVector
from cv_blocks.events.mv_manager import MotionVecLogger


class MarkedEventLogger(object):
    def __init__(self, event_id, event_type, metadata=None):
        self.event_id = event_id
        self.event_type = event_type
        self.metadata = metadata

    @staticmethod
    def pack(event_id, event_type, metadata=None):
        return dict(event_id=event_id, event_type=event_type, metadata=metadata)

    @staticmethod
    def unpack(data):
        event_type = data['event_type'] if 'event_type' in data else None
        metadata = data['metadata'] if 'metadata' in data else None
        return MarkedEventLogger(data['event_id'], event_type, metadata=metadata)


class BaseLogger(object):
    def __init__(self, eval_mode, use_shop_report=False, compress=False, include_barocde=False):
        self.eval_mode = eval_mode
        self.use_shop_report = use_shop_report
        self.compress = compress
        self.type_map = dict(movement_vector_delete=MovementVector,
                             mv_manager_out=MotionVecLogger,
                             event_aggregator_out=MotionVecLogger,
                             user_marked_event=MarkedEventLogger,
                             )
        if include_barocde:
            from md_blocks.barcode.barcode_handler import BarcodeImgLogger
            self.type_map['barcode_event_img'] = BarcodeImgLogger
        self.log_points = ['event_aggregator_out',
                        #    'movement_vector_delete',
                           'barcode_event_img',
                           'user_marked_event'] # TODO:to config

    def __del__(self):
        self.close()

    def add_log_point(self, capsule_type):
        assert capsule_type in self.type_map.keys() 
        self.log_points.append(capsule_type)

    def should_log(self, capsule_type):
        return capsule_type in self.log_points

    def open(self, time_info, session_id=None):
        if self.use_shop_report:
            from cv_blocks.misc.ShopReport import ShopReport
            self.shop_report = ShopReport(session_id=session_id)
        else:
            self.shop_report = None
        self.filename = self.get_file_name(time_info, session_id)
        os.makedirs(os.path.dirname(self.filename), exist_ok=True)
        print('\033[93m','Opening Event Logger','\033[0m', flush=True)
        self.fp = open(self.filename, 'wb')
        self.log(MarkedEventLogger.pack(-1, 'SessionStart'), 'user_marked_event')

    def open_custom(self, filename):
        self.filename = filename
        os.makedirs(os.path.dirname(self.filename), exist_ok=True)
        print('\033[93m','Opening Event Logger','\033[0m', flush=True)
        self.fp = open(self.filename, 'wb')

    def close(self):
        print('\033[93m','Closing Event Logger','\033[0m', flush=True)
        if hasattr(self, 'fp'):
            self.log(MarkedEventLogger.pack(-1, 'SessionEnd'), 'user_marked_event')
            print('\033[93m','Wrote capsule to %s' % self.filename,'\033[0m', flush=True)
            self.fp.close()
            del self.fp
        if hasattr(self, 'shop_report'):
            del self.shop_report
        if self.compress:
            print('\033[93m','Compressing log file','\033[0m', flush=True)
            zip_file = self.filename + '.zip'
            cmd = ['zip', '-m', zip_file, self.filename]
            print(' '.join(cmd))
            p = subprocess.Popen(cmd,stdout=subprocess.PIPE)
            p.wait()

    def log(self, data, capsule_type, frame=None):
        timestamp = datetime.utcnow().strftime('%Y-%m-%d_%H:%M:%S.%f')[:-3]
        print('\033[93m','Writing capsule to Logger:%s, frame=%s, timestamp=%s' % (capsule_type, str(frame), timestamp),'\033[0m', flush=True)
        assert capsule_type in self.type_map.keys()
        capsule = dict(type=capsule_type, data=data, frame=frame, timestamp=timestamp)
        if self.shop_report is not None:
            self.shop_report.handle_capsule(capsule)
        pickle.dump(capsule, self.fp, protocol=pickle.HIGHEST_PROTOCOL)

    def get_file_name(self, time_info, session_id):
        session_prefix = config.SAVE_REALTIME_MOVIE_PREFIX if config.SAVE_REALTIME_MOVIE_PREFIX!='' else 'shop_mode_log_'
        if session_id is not None:
            mdName = session_prefix + time_info['timestamp'] + '_id_%s' % session_id + '.pkl'
        else:
            mdName = session_prefix + time_info['timestamp'] + '.pkl'
        dir = config.MOVIES_PATH if self.eval_mode else os.path.join(config.MOVIES_PATH, time_info['date'])
        full_path = os.path.join(dir, mdName)
        return full_path

    def from_data(self, data_file, is_shop_report=False):
        print('\033[93m','Loading data to logger: %s' % data_file,'\033[0m')
        el_list = []
        with open(data_file, 'rb') as fr:
            try:
                while True:
                    obj = pickle.load(fr)
                    assert obj['type'] in self.type_map.keys()
                    if is_shop_report:
                        el_list.append(obj)
                    else:
                        element = self.type_map[obj['type']].unpack(obj['data'])
                        frame = frame = obj['frame'] if 'frame' in obj else obj['data']['event_frame']
                        timestamp = obj['timestamp'] if 'timestamp' in obj else None
                        el_list.append(dict(element=element, type=obj['type'], frame=frame, timestamp=timestamp))
            # except EOFError:
            except:
                pass
        return el_list


if __name__=='__main__':
    logger_file = '/home/walkout05/git_repo/walkout/movies/2020-04-13/mv_log_2020-04-13-13_45_50.pkl'
    logger_file = '/home/walkout05/git_repo/walkout/movies/2020-04-14/shop_mode_log_RG_aldi70_2020-03-25-15_39_27.pkl'
    my_mv_logger = BaseLogger()
    el_list = my_mv_logger.from_data(logger_file)
    for el in el_list:
        el['element'].visualize(path='/tmp/mv_log', prefix=el['type'])
