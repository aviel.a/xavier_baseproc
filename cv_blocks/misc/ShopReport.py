import json
import logging
import uuid
import os
import numpy as np
import cv2
import subprocess
import yaml
import sys
from datetime import datetime

SAS_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        '..', '..', 'user_files/SASclient')
CONF_FILE = os.path.join(os.path.dirname(
    os.path.realpath(__file__)), '..', '..', 'user_files', 'config_branch.yaml')


class ShopReport:
    def __init__(self, session_id):
        self.logger = logging
        self.session_id = session_id
        self.sas = ''
        self.generate_date = str(datetime.now()).split(' ')[0]
        configs = yaml.safe_load(open(CONF_FILE, 'r'))

        if 'walkout' not in configs['RETAILER'].lower():
            self.storage_account = 'walkout4{}'.format(configs['RETAILER'].lower())
        else: 
            self.storage_account = configs['RETAILER'].lower() 
        self.getSAS()
        print('new shop report start for session {}'.format(session_id), flush=True)
        self.ignored_types = ['barcode_event_img']
        os.makedirs(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                 '..', '..', 'logs/{}/{}'.format(self.generate_date, self.session_id)), exist_ok=True)
        self.log_path = os.path.join(os.path.dirname(
            os.path.realpath(__file__)), '..', '..', 'logs')
        self.logger.basicConfig(filename=self.log_path + '/shop_report.log', filemode='a', level=logging.INFO,
                                format='%(message)s')

    def getSAS(self):
        try:
            output = subprocess.check_output(SAS_PATH, shell=True)
            json_out = json.loads(output)
            self.sas = json_out['message']
        except:
            pass
        
    def handle_image(self, img, path, convert_channel=True):
        if convert_channel:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        cv2.imwrite(path, img)

    def gen_url_for(self, image, capsule_uuid, index):
        """
            here we generate data for SAS,
            retailer storage account, base url, branch name, etc'
            not its only place holder
        """
        return 'https://{}.blob.core.windows.net/capsules/{}/{}/{}/{}_{}.jpg'.format(
            self.storage_account, self.generate_date, self.session_id, capsule_uuid, index, image)

    def handle_capsule(self, capsule):
        ev_txt = ''
        timestamp = capsule['timestamp'].split('_')
        date = timestamp[0]
        time = timestamp[1].split('.')[0]
        capsule_uuid = str(uuid.uuid4())
        timestamp = str(date + ' ' + time)
        row = dict(timestamp=timestamp, log_level='info',
                   UUID=self.session_id)
        if capsule['type'] == 'event_aggregator_out':
            row['frame'] = capsule['frame']
            row['event_id'] = capsule['data']['event_id']
            row['event_type'] = 'VisualEvent'
            event_metadata = {
                k: v for k, v in capsule['data'].items() if k != 'movement_vector'}
            movement_vector = []
            if 'movement_vector' in capsule['data']:
                for index, item in enumerate(capsule['data']['movement_vector']):
                    movement_vector_item = dict()
                    for k, v in item.items():
                        if k in ['coords', 'context_image', 'cropped_image', 'global_image', 'attributes']:
                            if k == 'coords':
                                movement_vector_item['coords'] = v.to_dict()
                            elif k=='attributes' and v is not None:
                                movement_vector_item['attributes'] = {att_k: att_v.tolist() for att_k, att_v in v.items()}
                            else:
                                os.makedirs(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..',
                                                         'logs/{}/{}/{}'.format(self.generate_date, self.session_id,
                                                                                capsule_uuid)),
                                            exist_ok=True)
                                if k == 'context_image' and v is not None:
                                    self.handle_image(v, self.log_path + '/{}/{}/{}/{}_cropped_image.jpg'
                                                      .format(self.generate_date, self.session_id, capsule_uuid, index))
                                    movement_vector_item[
                                        'cropped_image'] = self.gen_url_for('cropped_image', capsule_uuid, index)

                                elif k == 'cropped_image' and v is not None:
                                    self.handle_image(v, self.log_path + '/{}/{}/{}/{}_cropped_image.jpg'
                                                      .format(self.generate_date, self.session_id, capsule_uuid, index))
                                    movement_vector_item[
                                        'cropped_image'] = self.gen_url_for('cropped_image', capsule_uuid, index)
                                if k == 'global_image' and v is not None:
                                    self.handle_image(v,
                                                      self.log_path +
                                                      '/{}/{}/{}/{}_global_image.jpg'.format(self.generate_date,
                                                                                             self.session_id,
                                                                                             capsule_uuid,
                                                                                             index),
                                                      convert_channel=False)
                                    movement_vector_item[
                                        'global_image'] = self.gen_url_for('global_image', capsule_uuid, index)
                        elif isinstance(v, np.ndarray):
                            movement_vector_item[k] = v.tolist()
                        elif type(v) in (np.float32, np.float64):
                            movement_vector_item[k] = float(v)
                        elif k == 'boxes_summary' and v is not None:
                            boxes_summary = []
                            for box in v:
                                boxes_summary.append(
                                    {"id": box['id'], "score": float(box['score'])})
                            movement_vector_item['boxes_summary'] = boxes_summary
                        else:
                            movement_vector_item[k] = v
                    movement_vector.append(movement_vector_item)
                with open(self.log_path+'/uploadCapsuleList','a') as metaFile:
                   metaFile.write(self.storage_account+":"+ self.sas+":"+ self.log_path+"/"+ self.generate_date+"/"+ self.session_id+"/"+ capsule_uuid+"\n")    
            event_metadata['movement_vector'] = movement_vector
            event_metadata['event_size'] = len(
                capsule['data']['movement_vector'])
            if 'init_coords' in event_metadata and event_metadata['init_coords'] is not None:
                init_coords = event_metadata['init_coords'].to_dict()
                event_metadata['init_coords'] = init_coords
            ev_txt = json.dumps(event_metadata)
        elif capsule['type'] == 'user_marked_event':
            row['frame'] = capsule['frame']
            row['event_id'] = capsule['data']['event_id']
            row['event_type'] = capsule['data']['event_type']
            if capsule['data']['metadata'] is None:
                capsule['data']['metadata'] = {}
            if 'prediction' in capsule['data']['metadata'] and capsule['data']['metadata'][
                'prediction'] is not None:
                prediction = []
                for pred in capsule['data']['metadata']['prediction']:
                    prediction.append([pred[0], format(pred[1], 'f')])
                capsule['data']['metadata']['prediction'] = prediction
            if 'part_of_bc_flow' in capsule['data']['metadata'] and capsule['data']['metadata'][
                'part_of_bc_flow'] is not None:
                part_of_bc_flow = capsule['data']['metadata']['part_of_bc_flow']
                capsule['data']['metadata']['part_of_bc_flow'] = bool(
                    part_of_bc_flow)
            ev_txt = json.dumps(capsule['data']['metadata'])

        row['event_payload'] = ev_txt
        self.log_event_to_file(row)

    def pkl_wrapper(self, el_list):
        for el in el_list:
            if el['type'] not in self.ignored_types:
                self.handle_capsule(el)

    def log_event_to_file(self, event):
        self.logger.info('{} {}: {} event_type:{}, event_id:{}, event_payload:{}'
                         .format(event['timestamp'], event['log_level'], event['UUID'], event['event_type'],
                                 event['event_id'],
                                 event['event_payload']))


if __name__ == '__main__':
    from cv_blocks.misc.logger import BaseLogger

    my_logger = BaseLogger(True, include_barocde=True, use_shop_report=True)
    my_logger.open(session_id=str(uuid.uuid4()),
                   time_info=dict(timestamp='', date=''))
    el_list = my_logger.from_data(
        '/home/walkout08/Downloads/shop_mode_log_2021-06-17-13_38_41_id_58e93db4-3e1c-4095-8b5b-d1cd5b5d78b5.pkl',
        is_shop_report=True)
    my_logger.shop_report.pkl_wrapper(el_list)
