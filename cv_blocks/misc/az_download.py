from posixpath import basename
import json
import subprocess
import socket
import sys
import os
import re
from datetime import datetime
# from azure.storage.blob import ContainerClient

accountName='walkout'
containerName='general-storage'



def internet_on():
    """
       check the internet connection
    """
    try:
        host = socket.gethostbyname('www.google.com')
        s = socket.create_connection((host, 80), 2)
        s.close()
        return True
    except:
        pass
    return False


# def download(from_path, folder, directory):
#     internet = internet_on()
#     if internet:
#         p = subprocess.Popen("aws s3 sync s3://{}/RND/{}/{}/ .".format(bucket, from_path, folder),
#                              stdout=subprocess.PIPE,
#                              shell=True,
#                              cwd=directory)
#         for line in p.stdout.readlines():
#             print(line, flush=True)
#         retval = p.wait()

def wait_with_print(p):
    while True:
        output = p.stdout.readline()
        if output in (b'', '') and p.poll() is not None:
            break
        if output:
            print(output.decode("utf-8"), flush=True)
        rc = p.poll()

# def download_cart_data(src_path, dst_path, include=None):
#     internet = internet_on()
#     if internet:
#         blobName = 'cart_data/'+src_path
#         SAS = ''
#         blobClient = BlobClient(account_URL, containerName, blobName, SAS)  
        
#         if include is not None:
#             cmd += ' --exclude="*"'
#             for inc in include:
#                 cmd += ' --include="*%s*"' % inc
                
#         dst_path
        
#         print(cmd)
#         p = subprocess.Popen(cmd,
#                              stdout=subprocess.PIPE, shell=True)
#         wait_with_print(p)

def movie_download(movie_filename, root_dir, az_search_root='cart_data', include=None, is_capsule=False):
    internet = internet_on()
    if internet:
        # Get movie date
        match = re.search(r'\d{4}-\d{2}-\d{2}', movie_filename)
        date_obj = datetime.strptime(match.group(), '%Y-%m-%d')
        date_str = '%04d-%02d-%02d' % (date_obj.year, date_obj.month, date_obj.day)
        # Find movie path in aws
        try:
            output = subprocess.check_output(
                'az storage blob list --auth-mode login --account-name {} --container-name {}  --prefix {}/{}/ --query "[?contains(name, \'{}\')].name"'.format(accountName, containerName, az_search_root, date_str, movie_filename), shell=True)
        except:
            output = None
        if output is None or output.decode("utf-8") == '[]\n':
            output = subprocess.check_output(
                'az storage blob list --auth-mode login --account-name {} --container-name {} --prefix {} --query "[?contains(name, \'{}\')].name"'.format(accountName, containerName, az_search_root, movie_filename), shell=True)
        rel_az_dir = str(output.strip()).split(az_search_root)[1]
        rel_az_dir = rel_az_dir.split(movie_filename)[0]
        if rel_az_dir.startswith('/'):
            rel_az_dir = rel_az_dir[1:]
        # Download movie
        base_name  = movie_filename.split('_L.avi')[0]
        if is_capsule:
            az_path = os.path.join(az_search_root, rel_az_dir)
            # capsule_dir = os.path.join(root_dir, base_name)
            cmd = "az storage blob directory download --auth-mode login --account-name {} --container {} --recursive --source-path {} --destination-path {}".format(accountName, containerName, az_path, root_dir)
            print(cmd)
            try:
                # p = subprocess.check_output(cmd, stdout=subprocess.PIPE, shell=True)
                p = subprocess.check_output(cmd, shell=True)
            except Exception as e:
                print('Did not find %s. Skipping. Exception was %s' % (base_name, str(e)))
        else:
            valid_postfix = ['script.json', '_prop.json', '_L.csv',  '_L.avi', '_R.avi', '_B.avi']
            for postfix in valid_postfix:
                if include is not None:
                    if postfix not in include:
                        continue
                filename = base_name + postfix
                file_path=os.path.join(root_dir,filename)
                az_path = os.path.join(az_search_root, rel_az_dir, filename)
                cmd = "az storage blob download --auth-mode login --account-name {} --container-name {} --name {} --file {}".format(accountName, containerName, az_path, file_path)
                print(cmd)
                try:
                    # p = subprocess.check_output(cmd, stdout=subprocess.PIPE, shell=True)
                    p = subprocess.check_output(cmd, shell=True)
                except Exception as e:
                    print('Did not find %s. Skipping. Exception was %s' % (filename, str(e)))
                    os.remove(file_path)
        
        return root_dir
        
        
        # SASBinary = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../user_files/SASclient')
        
        # # Get SAS 
        # try:
        #     SAS = json.loads(subprocess.check_output(SASBinary).decode("utf8"))['message']
        # except Exception as e:
        #     print("Unable to get SAS. reason: "+str(e))
        #     return
    
        # # Get movie date
        # match = re.search(r'\d{4}-\d{2}-\d{2}', movie_filename)
        # date_obj = datetime.strptime(match.group(), '%Y-%m-%d')
        # date_str = '%04d-%02d-%02d' % (date_obj.year, date_obj.month, date_obj.day)
        # print(date_str)
        
        # containerURL = account_URL+containerName+'?'+SAS
        # tryPath = os.path.join(az_search_root,date_str)
        
        # try:
        #     containerClient = ContainerClient.from_container_url(containerURL)
            
        # except Exception as e:
        #     print(str(e))
        #     return 
        
        # # Find movie path in az
        # blobs = containerClient.list_blobs(name_starts_with=tryPath)
        # base_name = None
        
        # for blob in blobs:
        #     if movie_filename in blob.name:
        #         base_name = '_'.join(blob.name.split('_')[:-1])
        #         print("found " +base_name)
        #         continue    

        
        # if None == base_name:
        #     blobs = containerClient.list_blobs(name_starts_with=az_search_root)
        #     for blob in blobs:
        #         if movie_filename in blob.name:
        #             base_name = '_'.join(blob.name.split('_')[:-1])
        #             print("found " +base_name)
        #             continue    
        
    
        # capsuleBlobs = containerClient.list_blobs(name_starts_with=base_name)
        # capsuleDir = os.path.join(root_dir,os.path.basename(base_name))
        # if not os.path.exists(capsuleDir):
        #     os.mkdir(capsuleDir)
        # for blob in capsuleBlobs:
        #     stream = containerClient.download_blob(blob)
        #     fileName = os.path.basename(blob.name)
        #     localFileName = os.path.join(capsuleDir, fileName)
        #     print("downloading "+blob.name+" to "+localFileName)
        #     try:
        #         with open(localFileName, "wb") as fileStream:
        #             stream.readinto(fileStream)
        #     except Exception as e:
        #         print("Error: "+str(e))
        # print("DONE")
            

# def find_and_download(filename, aws_search_root, dst_dir):
#     internet = internet_on()
#     if internet:
#         # Find movie path in aws
#         output = subprocess.check_output("aws s3 ls s3://{}/{}/ --recursive | grep {}".format(bucket, aws_search_root, filename), shell=True)
#         rel_aws_dir = str(output.strip()).split(aws_search_root)[1]
#         rel_aws_dir = rel_aws_dir.split(filename)[0]
#         if rel_aws_dir.startswith('/'):
#             rel_aws_dir = rel_aws_dir[1:]
#         aws_path = os.path.join(bucket, aws_search_root, rel_aws_dir, filename)
#         cmd = "aws s3 cp s3://{} {}".format(aws_path, dst_dir)
#         print(cmd)
#         try:
#             p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
#             wait_with_print(p)
#         except:
#             print('Did not find %s. Skipping' % filename)
#         return dst_dir

# def remote_ls(src_dir, recursive=False, dirs_only=False, filter_str=None):
#     internet = internet_on()
#     if internet:
#         if recursive:
#             cmd = "aws s3 ls s3://{}/{}/ --recursive".format(bucket, src_dir)
#         else:
#             cmd = "aws s3 ls s3://{}/{}/".format(bucket, src_dir)
#         output = subprocess.check_output(cmd, shell=True)
#         raw_list = str(output).split('\\n')
#         sp_val = 'PRE ' if dirs_only else ' '
#         f_list = [r.split(sp_val)[-1] for r in raw_list if sp_val in r]
#         f_list = [f[:-1] if f.endswith('/') else f for f in f_list]
#         if filter_str:
#             f_list = [f for f in f_list if filter_str in f]
#         return f_list

# def upload_file(src, dst):
#     SASBinary = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../user_files/SASclient')
    
#     # Get SAS 
#     try:
#         SAS = json.loads(subprocess.check_output(SASBinary).decode("utf8"))['message']
#     except Exception as e:
#         print("Unable to get SAS. reason: "+str(e))
#         return
    
#     # get AZ container client
#     containerURL = account_URL+containerName+'?'+SAS
#     try:
#         containerClient = ContainerClient.from_container_url(containerURL)
        
#     except Exception as e:
#         print(str(e))
#         return 

#     try:
#         with open(src, "rb") as data:
#             containerClient.upload_blob(name=dst, data=data)
    
#     except Exception as e:
#         print("Unable to upload file: "+str(e))

#     return dst