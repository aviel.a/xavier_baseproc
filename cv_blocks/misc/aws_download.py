import subprocess
import socket
import sys
import os
import re
from datetime import datetime

bucket = 'walkout-main'


def internet_on():
    """
       check the internet connection
    """
    try:
        host = socket.gethostbyname('www.google.com')
        s = socket.create_connection((host, 80), 2)
        s.close()
        return True
    except:
        pass
    return False


def download(from_path, folder, directory):
    internet = internet_on()
    if internet:
        p = subprocess.Popen("aws s3 sync s3://{}/RND/{}/{}/ .".format(bucket, from_path, folder),
                             stdout=subprocess.PIPE,
                             shell=True,
                             cwd=directory)
        wait_with_print(p)

def general_download(src, dst):
    internet = internet_on()
    if internet:
        p = subprocess.Popen("aws s3 cp s3://{}/{} {}".format(bucket, src, dst),
                             stdout=subprocess.PIPE,
                             shell=True)
        wait_with_print(p)

def wait_with_print(p):
    while True:
        output = p.stdout.readline()
        if output in (b'', '') and p.poll() is not None:
            break
        if output:
            print(output.decode("utf-8"), flush=True)
        rc = p.poll()

def download_cart_data(src_path, dst_path, include=None):
    internet = internet_on()
    if internet:
        cmd = "aws s3 sync s3://{}/{}/ {}/ ".format(bucket, src_path, dst_path)
        if include is not None:
            cmd += ' --exclude="*"'
            for inc in include:
                cmd += ' --include="*%s*"' % inc
        print(cmd)
        p = subprocess.Popen(cmd,
                             stdout=subprocess.PIPE, shell=True)
        wait_with_print(p)

def movie_download(movie_filename, root_dir, aws_search_root='cart_data', include=None, is_capsule=False):
    internet = internet_on()
    if internet:
        # Get movie date
        match = re.search(r'\d{4}-\d{2}-\d{2}', movie_filename)
        date_obj = datetime.strptime(match.group(), '%Y-%m-%d')
        date_str = '%04d-%02d-%02d' % (date_obj.year, date_obj.month, date_obj.day)
        # Find movie path in aws
        try:
            output = subprocess.check_output("aws s3 ls s3://{}/{}/{}/ --recursive | grep {}".format(bucket, aws_search_root, date_str, movie_filename), shell=True)
        except:
            output = subprocess.check_output(
                "aws s3 ls s3://{}/{}/ --recursive | grep {}".format(bucket, aws_search_root, movie_filename),
                shell=True)
        rel_aws_dir = str(output.strip()).split(aws_search_root)[1]
        rel_aws_dir = rel_aws_dir.split(movie_filename)[0]
        if rel_aws_dir.startswith('/'):
            rel_aws_dir = rel_aws_dir[1:]
        # Download movie
        base_name  = movie_filename.split('_L.avi')[0]
        if is_capsule:
            aws_path = os.path.join(bucket, aws_search_root, rel_aws_dir, base_name)
            capsule_dir = os.path.join(root_dir, base_name)
            cmd = "aws s3 sync s3://{} {}".format(aws_path, capsule_dir)
            print(cmd)
            try:
                p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
                wait_with_print(p)
            except:
                print('Did not find %s. Skipping' % base_name)
        else:
            valid_postfix = ['script.json', '_prop.json', '_L.csv',  '_L.avi', '_R.avi', '_B.avi']
            for postfix in valid_postfix:
                if include is not None:
                    if postfix not in include:
                        continue
                filename = base_name + postfix
                aws_path = os.path.join(bucket, aws_search_root, rel_aws_dir, filename)
                cmd = "aws s3 cp s3://{} {}".format(aws_path, root_dir)
                print(cmd)
                try:
                    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
                    wait_with_print(p)
                except:
                    print('Did not find %s. Skipping' % filename)
        return root_dir

def find_and_download(filename, aws_search_root, dst_dir):
    internet = internet_on()
    if internet:
        # Find movie path in aws
        output = subprocess.check_output("aws s3 ls s3://{}/{}/ --recursive | grep {}".format(bucket, aws_search_root, filename), shell=True)
        rel_aws_dir = str(output.strip()).split(aws_search_root)[1]
        rel_aws_dir = rel_aws_dir.split(filename)[0]
        if rel_aws_dir.startswith('/'):
            rel_aws_dir = rel_aws_dir[1:]
        aws_path = os.path.join(bucket, aws_search_root, rel_aws_dir, filename)
        cmd = "aws s3 cp s3://{} {}".format(aws_path, dst_dir)
        print(cmd)
        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
            wait_with_print(p)
        except:
            print('Did not find %s. Skipping' % filename)
        return dst_dir

def remote_ls(src_dir, recursive=False, dirs_only=False, filter_str=None):
    internet = internet_on()
    if internet:
        if recursive:
            cmd = "aws s3 ls s3://{}/{}/ --recursive".format(bucket, src_dir)
        else:
            cmd = "aws s3 ls s3://{}/{}/".format(bucket, src_dir)
        output = subprocess.check_output(cmd, shell=True)
        raw_list = str(output).split('\\n')
        sp_val = 'PRE ' if dirs_only else ' '
        f_list = [r.split(sp_val)[-1] for r in raw_list if sp_val in r]
        f_list = [f[:-1] if f.endswith('/') else f for f in f_list]
        #if filter_str:
        #    f_list = [f for f in f_list if filter_str in f]
        if filter_str:
            filt_list = []
            #filt_list = f_list
            if (type(filter_str) == str):
                filt_list = [f for f in f_list if filter_str in f]
            else:
                for filt in filter_str:
                    filt_list += [f for f in f_list if filt in f]
            return filt_list

        else:
            return f_list

def upload_file(src, dst):
    aws_path = os.path.join(bucket, dst)
    cmd = "aws s3 cp {} s3://{}".format(src, aws_path)
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    wait_with_print(p)
    return aws_path