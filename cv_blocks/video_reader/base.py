import cv2
import numpy as np
from collections import deque
from threading import Thread, RLock
import time
import os
import argparse


class BaseImageGrabber(Thread):
    '''
    Basic image grabber
    '''
    def __init__(self, stream, max_buffer_size=20, target_ms=40,
                 debug=False):
        assert hasattr(stream, 'read')
        Thread.__init__(self)
        self.stream = stream
        self.target_ms = target_ms
        # TODO - check that cam source is valid
        self.max_buffer_size = max_buffer_size
        self.image_que = deque()
        self.last_frame_ts = time.time() * 1000 # time is ms
        self.last_acquisition_ts = 0 # time is ms
        self.keep_running = True
        self.debug = debug
        self.lock = RLock()
        self.n_buffer_frames = 5
        self.long_cycle_ms = target_ms * 1.2
        self.isRealTime = self.stream.is_real_time
    
    def set_traget_fps(self, fps):
        self.lock.acquire()
        self.target_ms = 1000 * (1. / fps)
        self.lock.release()
    
    def run(self):
        start_counter = 0
        while self.keep_running:
            loop_start_time_ms = time.time() * 1000
            time_since_last_grab_ms = loop_start_time_ms - self.last_frame_ts
            if time_since_last_grab_ms > self.long_cycle_ms:
                print('Image USB grab cycle was too long : %.2f' % time_since_last_grab_ms)
            self.stream.grab() # grab for next time
            if self.debug:
                print('Time diff ms(grabber call): %.2f' % time_since_last_grab_ms)
            if start_counter < self.n_buffer_frames:
                start_counter += 1
            else:
                ret, image_data = self.stream.read()
                if ret:
                    queue_item = dict(data=image_data, ts=loop_start_time_ms)
                    if 'mean_ts' in image_data:
                        queue_item['ts'] = image_data['mean_ts']
                    self.lock.acquire()
                    self.image_que.append(queue_item)
                    self.lock.release()
                else:
                    ret_status = ['%s:%r' %(k, val['ret']) for k, val in image_data.items()]
                    print('Warning:Failed reading image:', ret_status)
            loop_end_time_ms = time.time() * 1000
            loop_time_ms = loop_end_time_ms - loop_start_time_ms
            if loop_time_ms < self.target_ms:
                time.sleep((self.target_ms - loop_time_ms) * 1e-3)
            self.last_frame_ts = loop_start_time_ms
    
    def queue_size(self):
        self.lock.acquire()
        queue_size = len(self.image_que)
        self.lock.release()
        return queue_size
    
    def grab(self, can_skip=False):
        # Wait until queue is not empty - blocking
        queue_size = self.queue_size()
        while queue_size<=0:
            time.sleep(1e-3)
            queue_size = self.queue_size()
        # pop 2 frames when no motion and queue is long
        
        skip_frame = can_skip and queue_size >= max(self.max_buffer_size * 0.5, 2)
        self.lock.acquire()
        if queue_size > 0:
            if self.debug:
                print('Queue size = %d' % queue_size)
            queue_item = self.image_que.popleft()
            if skip_frame:
                if self.debug:
                    print('Skipping image due to long queue')
                queue_item = self.image_que.popleft()
            self.lock.release()
            acquisition_diff_ms = (queue_item['ts'] - self.last_acquisition_ts)
            if acquisition_diff_ms > self.long_cycle_ms and not skip_frame:
                print('Image acquisition cycle was too long : %.2f' % acquisition_diff_ms)
            if self.debug:
                print('Time diff ms(throughput): %.2f' % acquisition_diff_ms)
                print('Time diff ms(latency): %.2f' % (time.time() * 1e3 - queue_item['ts']))
            self.last_acquisition_ts = queue_item['ts']
            return queue_item, skip_frame
        else:
            self.lock.release()
            return None, skip_frame
    
    def white_balance(self):
        self.stream.white_balance()
    
    # Dummy functions for videoStereoStreams compatability, to be refactored
    def isCamBExist(self):
        return True
    
    def get_max_disparity(self):
        return 10

    def getDepthImgSizingFactor(self):
        IMG_REDUCTION_TO_RETURN = 0.5
        return IMG_REDUCTION_TO_RETURN

    def get_movie_name(self):
        if hasattr(self,'_movie_name'):
            return self._movie_name
        else:
            return ''
    
    
if __name__=='__main__':
    parser = argparse.ArgumentParser(description='base video reader sanity')
    parser.add_argument('--target-ms', help='take image every X ms', type=float, default=40)
    parser.add_argument('--playback', help='use playback mode', action='store_true')
    parser.add_argument('--debug', help='debug logs', action='store_true')
    args = parser.parse_args()
    if args.playback:
        from cv_blocks.cameras.playback import PlaybackMultiCam
        base_rec_path = '/home/walkout05/Desktop/movies/DS2p39_2019-11-19-19_48_18'
        camera_dict = dict(left=base_rec_path +'_L.avi',
                           right=base_rec_path +'_R.avi',
                           back=base_rec_path +'_B.avi',
                        )
        cam_source = PlaybackMultiCam(camera_dict)
    else:
        from cv_blocks.cameras.basler import BaslerMultiCam
        camera_dict = dict(left=dict(cam_id='Basler daA1280-54uc (22594447)', pixel_format='RGB8'),
                           right=dict(cam_id='Basler daA1280-54uc (22594466)', pixel_format='RGB8'),
                           back=dict(cam_id='Basler daA1280-54uc (22594463)', pixel_format='YCbCr422_8',
                                     software_trigger=False),
                      )
        # camera_dict = dict(left=dict(cam_id='Basler daA1280-54uc (22594448)', pixel_format='RGB8'),
        #                    right=dict(cam_id='Basler daA1280-54uc (22594469)', pixel_format='YCbCr422_8', software_trigger=False),
        #               )
        cam_source = BaslerMultiCam(camera_dict, debug=args.debug)

    my_reader = BaseImageGrabber(cam_source, target_ms=args.target_ms, debug=args.debug)
    my_reader.start()

    while True:
        t_grab = time.time()
        img_data = my_reader.grab()
        if img_data:
            img_l = img_data['data']['left']['img']
            img_r = img_data['data']['right']['img']
            img_viz = np.hstack((img_l, img_r))
            if 'back' in img_data['data'].keys():
                img_b = img_data['data']['back']['img']
                img_viz = np.hstack((img_viz, img_b))
            ts = img_data['ts']
            cv2.putText(img_viz, '%f' % ts, (10, 30), cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,0), 1)
            cv2.imshow('Image', img_viz)
            key = cv2.waitKey(1) & 0xFF
        loop_time = (time.time() - t_grab) * 1e3
        if loop_time > args.target_ms:
            print('Warning: grab + display took %.2fms' % loop_time)
        else:
            time.sleep((args.target_ms - loop_time) * 1e-3)