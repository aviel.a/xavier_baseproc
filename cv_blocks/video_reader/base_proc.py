import cv2
import numpy as np
import ctypes
from multiprocessing import Process, Lock, RawArray
import time
import os
import argparse
import enum
import Exceptions
from cv_blocks.white_balance.template_matching import TemplateMatching
import cv_blocks.white_balance.white_balance as wb
from config import config
import os
from cv_blocks.white_balance.wb_data import WbData
from cv_blocks.white_balance.monitor_values import MonitoringStatus
from md_blocks.md_messages import MDMessages
from cv_blocks.common.types import BoundingBox2D
import traceback


class SharedMemState(enum.IntEnum):
    Init = 0
    Ready = 1
    Used = 2
    Done = 3
    Failed = 4

class ImageOp(enum.IntEnum):
    NoOp = 0
    DriverWhiteBalance = 1
    ExternalWhiteBalance = 2
    OneTimeExternalWB = 3
    TuneExposure = 4
    OneTimeExpTuning = 5
    MonitorValues = 6
    ShiftSamplePhase = 7
    ToggleStartStop = 8
    SingleStep = 9
    ResetWhiteBalance = 10
    EmptyImageQueue = 11
    CloseStream = 12
    PrintCalibSum = 13
    FindTemplates = 14

class WhiteBalanceStatus(enum.IntEnum):
    Empty = 0
    Success = 1
    Failed = 2

class ExposureStatus(enum.IntEnum):
    Empty = 0
    Success = 1
    Failed = 2

class Lighting(enum.IntEnum):
    Empty = 0
    Dark = 1
    Bright = 2

class GrabberState(enum.IntEnum):
    Running = 0
    Paused = 1
    Stopped = 2

class MonitorWBState(enum.IntEnum):
    NoCalibrationNeeded = 0
    NeedCalibration = 1

class MonitorExposureState(enum.IntEnum):
    NoCalibrationNeeded = 0
    NeedCalibration = 1

class ReplaceCBState(enum.IntEnum):
    ShouldReplace = 0
    ShouldNotReplace = 1

class TemplatesDetectedState(enum.IntEnum):
    Empty = 0
    TemplatesFound = 1
    TemplatesNotFound = 2

class StartStopState(enum.IntEnum):
    Toggle = 0
    Start = 1
    Stop = 2

DEFAULT_IMG_SHAPE = (480, 640, 3)
MAX_IMG_SHAPE = (960, 1280, 3)
VALID_IMG_SHAPE = [(960, 1280, 3), (480, 640, 3)]
DEFAULT_MAX_BUFF_SIZE = 50
class BaseImageGrabber(object):
    '''
    Basic image grabber
    '''
    def __init__(self, stream_type, camera_dict, max_buffer_size=DEFAULT_MAX_BUFF_SIZE, target_ms=40,
                 debug=False, output_barcode_viz=False, output_targets=dict(), cuda_driver=None,
                 profiler=None, time_info=None, wb_info=dict()):
        assert stream_type in ('basler', 'playback')
        self.stream_type = stream_type
        if stream_type=='playback':
            from cv_blocks.cameras.playback import PlaybackMultiCam
            self._movie_name = os.path.basename(camera_dict)
            camera_dict = PlaybackMultiCam.rec_name_to_cam_dict(camera_dict)
        self.time_info = time_info
        self.camera_dict = camera_dict
        self.camera_names = list(camera_dict.keys())
        self.calc_img_size(camera_dict)
        self.target_ms = target_ms
        self.max_buffer_size = max_buffer_size
        self.output_barcode_viz = output_barcode_viz
        self.init_shared_mem()
        self.image_queue = []
        self.last_frame_ts = time.time() * 1000 # time is ms
        self.last_acquisition_ts = 0 # time is ms
        self.keep_running = True
        self.debug = debug
        self.n_buffer_frames = 3
        self.long_cycle_ms = target_ms * 2.
        self.short_cycle_ms = target_ms / 2.
        self.isRealTime = self.stream_type!='playback'
        self.time_anchor = None
        self.time_to_sample_ms = 1.0 # time delta to make acquisition jitter unbiased
        self.grabber_state = GrabberState.Running
        self.init_wb_params(wb_info)
        self.img_buff_full_th = 15
        self.output_targets = output_targets
        self.cuda_driver = cuda_driver
        self.profiler = profiler
    
        self.debug_online_wb = False
        self.print_wb_logs = False
        if self.print_wb_logs:
            self.debug_online_wb = True
    
    def init_wb_params(self, wb_info):
        if 'exposure_range' in wb_info.keys() and isinstance(wb_info['exposure_range'], (list, tuple)) and len(wb_info['exposure_range'])==2:
            self.exposure_range = wb_info['exposure_range']
        else:
            self.exposure_range = (500, 3000)
        if 'exposure_jumps' in wb_info.keys() and isinstance(wb_info['exposure_jumps'], (list, tuple)) and len(wb_info['exposure_jumps'])==3:
            self.exposure_jumps = wb_info['exposure_jumps']
        else:
            self.exposure_jumps = (500, 250, 100)
        if 'gain_range' in wb_info.keys() and isinstance(wb_info['gain_range'], (list, tuple)) and len(wb_info['gain_range'])==2:
            self.gain_range = wb_info['gain_range']
        else:
            self.gain_range = (0, 18)
        if 'gain_anchor' in wb_info.keys():
            self.gain_anchor = wb_info['gain_anchor']
        else:
            self.gain_anchor = 10
        if 'gain_jump' in wb_info.keys():
            self.gain_jump = wb_info['gain_jump']
        else:
            self.gain_jump = 2
        self.allow_n_failures = 3
        if 'templates_mean' in wb_info.keys() and isinstance(wb_info['templates_mean'], dict):
            self.templates_mean = dict(target=wb_info['templates_mean'].get('target', 140), range=wb_info['templates_mean'].get('range', 10))
        else:
            self.templates_mean = dict(target=140, range=10)

    def calc_img_size(self, camera_dict):
        img_sizes = dict()
        img_sizes_post_rotate = dict()
        cam_buffer_size = dict()
        for cam_name, cam_info in camera_dict.items():
            if isinstance(cam_info, dict) and 'height' in cam_info.keys() and 'width' in cam_info.keys() \
                and (cam_info['height'], cam_info['width'], 3) in VALID_IMG_SHAPE:
                img_sizes[cam_name] = (cam_info['height'], cam_info['width'], 3)
                img_sizes_post_rotate[cam_name] = (cam_info['width'], cam_info['height'], 3)
            elif isinstance(cam_info, str):
                try:
                    import imageio
                    vid_size = imageio.get_reader(cam_info).get_meta_data()['size']
                    img_sizes[cam_name] = (int(vid_size[1]), int(vid_size[0]), 3)
                    img_sizes_post_rotate[cam_name] = (int(vid_size[0]), int(vid_size[1]), 3)
                except:
                    vcap = cv2.VideoCapture(cam_info)
                    if vcap.isOpened():
                        img_sizes[cam_name] = (int(vcap.get(cv2.CAP_PROP_FRAME_HEIGHT)), int(vcap.get(cv2.CAP_PROP_FRAME_WIDTH)), 3)
                        img_sizes_post_rotate[cam_name] = (int(vcap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(vcap.get(cv2.CAP_PROP_FRAME_HEIGHT)), 3)
            elif isinstance(cam_info, list): # image list capsules
                raise ImportError 
            else:
                img_sizes[cam_name] = DEFAULT_IMG_SHAPE
            cam_buffer_size[cam_name] = img_sizes[cam_name][0] * img_sizes[cam_name][1] * img_sizes[cam_name][2]
        # Enforce basler values align to required img size
        for cam_name in camera_dict.keys():
            if isinstance(camera_dict[cam_name], dict):
                camera_dict[cam_name]['height'] = img_sizes[cam_name][0]
                camera_dict[cam_name]['width'] = img_sizes[cam_name][1]
                camera_dict[cam_name]['binning_value'] =  MAX_IMG_SHAPE[0] // img_sizes[cam_name][0]
        
        self.img_sizes = img_sizes
        self.cam_buffer_size = cam_buffer_size
        self.img_sizes_post_rotate = img_sizes_post_rotate
    
    def init_shared_mem(self, n_shared_elements=2):
        self.img_buff_size = 0
        for buff_size in self.cam_buffer_size.values(): 
            self.img_buff_size += buff_size
        self.n_shared_elements = n_shared_elements
        self.queue_lock = Lock()
        self.op_lock = Lock()
        self.barcode_viz_lock = Lock()
        self.wb_lock = Lock()
        print('Initializing shared buffer')
        self.shared_mem = dict(img=RawArray(ctypes.c_uint8, np.zeros(n_shared_elements * self.img_buff_size, dtype=np.uint8)), 
                               ts=RawArray(ctypes.c_double, [0] * n_shared_elements),
                               state=RawArray(ctypes.c_uint8, [int(SharedMemState.Init)] * n_shared_elements),
                               img_idx=RawArray(ctypes.c_uint32, [0] * n_shared_elements),
                               queue_size=RawArray(ctypes.c_uint32, [0]),
                               ops=RawArray(ctypes.c_uint32, [int(ImageOp.NoOp), 0, 0, 0]),
                               wb_status=RawArray(ctypes.c_uint8, [int(WhiteBalanceStatus.Empty)]),
                               need_wb=RawArray(ctypes.c_uint8, [int(MonitorWBState.NoCalibrationNeeded), 0, 0]),
                               need_exp_tuning=RawArray(ctypes.c_uint8, [int(MonitorExposureState.NoCalibrationNeeded), 0, 0]),
                               exposure_status=RawArray(ctypes.c_uint8, [int(ExposureStatus.Empty)]),
                               replace_cb_img=RawArray(ctypes.c_uint8, [int(ReplaceCBState.ShouldReplace)]),
                               templates_detected=RawArray(ctypes.c_uint8, [int(TemplatesDetectedState.Empty),  #front
                                                                            int(TemplatesDetectedState.Empty)]),#back
                               expose_barcode_viz=RawArray(ctypes.c_uint8, [0]),)
                               
        if self.output_barcode_viz:
            self.barcode_viz_buff_info = dict(cam='right', h=240, w=320, ch=3, size=240 * 320 * 3)
            self.shared_mem['barcode_viz_img'] = RawArray(ctypes.c_uint8, np.zeros(self.barcode_viz_buff_info['size'], dtype=np.uint8))
            bc_img_h, bc_img_w = (self.img_sizes['right'][0], self.img_sizes['right'][1])
            self.barcode_crop_box = BoundingBox2D([0.25 * bc_img_w, 0., 0.75 * bc_img_w, bc_img_h]) # TODO - align with barcode handler defines
            # grayscale image for barcode scanning
            self.barcode_crop_size = (int(self.barcode_crop_box.h()),int(self.barcode_crop_box.w()))
            self.shared_mem['barcode_crop_img'] = RawArray(ctypes.c_uint8, np.zeros(self.barcode_crop_size[0] * self.barcode_crop_size[1], dtype=np.uint8))
    
    def reset_shared_mem(self):
        state_arr = np.frombuffer(self.shared_mem['state'], dtype=np.uint8)
        for sample_idx in range(self.n_shared_elements):
            state_arr[sample_idx] = SharedMemState.Init

    def set_traget_fps(self, fps):
        self.lock.acquire()
        self.target_ms = 1000 * (1. / fps)
        self.lock.release()
    
    def run(self):
        # Initialize cameras
        if self.stream_type=='basler':
            from cv_blocks.cameras.basler import BaslerMultiCam
            try:
                self.stream = BaslerMultiCam(self.camera_dict, debug=self.debug)
                assert hasattr(self.stream, 'read')
            except:
                MDMessages.log_md_failure(traceback.format_exc(), source='camera_failure')
                raise Exceptions.CameraInitException('Failed loading cameras!')

            try:
                self.run_live()
            except:
                MDMessages.log_md_failure(traceback.format_exc(), source='camera_failure')
                self.mark_failed()
                self.stream.release()
                raise Exceptions.CameraGrabFrameException('Failed during run loop!')
        elif self.stream_type=='playback':
            from cv_blocks.cameras.playback import PlaybackMultiCam
            try:
                self.stream = PlaybackMultiCam(self.camera_dict)
                assert hasattr(self.stream, 'read')
                self.run_playback()
            except:
                self.mark_failed()
                MDMessages.log_md_failure(traceback.format_exc(), source='camera_failure')
                raise Exceptions.CameraInitException('Playback Failure!')

    def run_live(self):
        self.start_counter = 0
        img_idx = 0
        failure_counter = 0
        self.time_anchor = time.time()
        self.last_valid_grab_ts = None
        if config.WHITE_BALANCE_FROM_STICKERS:
            self.template_matching = TemplateMatching(monitor_values=True)
            self.wb_data = WbData(self.time_info, save=config.SAVE_TEMPLATES)
        el_not_pushed_last_cycle = False
        while self.keep_running:
            loop_start_time_ms = time.time() * 1000
            if self.grabber_state==GrabberState.Running:
                if self.debug:
                    time_since_last_grab_ms = loop_start_time_ms - self.last_frame_ts
                # if time_since_last_grab_ms > self.long_cycle_ms:
                #     print('Image USB grab cycle was too long : %.2f' % time_since_last_grab_ms)
                # trigger next image
                # ==================
                self.stream.grab(time_anchor=self.time_anchor)
                # check for popped elements and chop list from left
                n_elements_to_pop, init, shared_buff_state = self.n_elements_used()
                # Pop 1 less element if last cycle didn't push to shared mem
                force_push = False
                if el_not_pushed_last_cycle and n_elements_to_pop>0:
                    force_push = True
                    n_elements_to_pop = n_elements_to_pop - 1
                # update shared memory with next frames to process to fill in popped elements
                for _ in range(n_elements_to_pop):
                    if len(self.image_queue) > 0:
                        self.image_queue.pop(0)

                if self.debug:
                    print('Time diff ms(grabber call): %.2f' % time_since_last_grab_ms, flush=True)
                if self.start_counter < self.n_buffer_frames:
                    self.start_counter += 1
                else:
                    # read next image
                    # ===============
                    ret, image_data = self.stream.read()
                    need_to_push = init or (n_elements_to_pop > 0) or force_push
                    if ret:
                        failure_counter = 0
                        next_queue_item = dict(data=image_data, ts=loop_start_time_ms, img_idx=img_idx)
                        img_idx += 1
                        if 'mean_ts' in image_data:
                            next_queue_item['ts'] = image_data['mean_ts']
                            self.last_valid_grab_ts = image_data['mean_ts']
                        self.image_queue.append(next_queue_item)
                        if len(self.image_queue) >= self.n_shared_elements and need_to_push:
                            self.queue_lock.acquire()
                            self.to_shared_mem(self.image_queue[:self.n_shared_elements])
                            el_not_pushed_last_cycle = False
                            if self.should_update_barcode_img() and len(self.image_queue) > 0:
                                self.update_barcode_img(self.image_queue[-1]) # last image from queue
                            self.queue_lock.release()
                        elif need_to_push:
                            # Most likely an error, mark it for next cycle
                            el_not_pushed_last_cycle = True

                    else:
                        failure_counter += 1
                        ret_status = ['%s:%r' %(k, val['ret']) for k, val in image_data.items() if k!='mean_ts']
                        print('Warning:Failed reading image:', ret_status, 'Failure Counter=%d' % failure_counter, flush=True)
                        print('Buffers State on failure:', self.stream.log_buffer_state(), flush=True)
                        print('Queue size = %d, Shared buffer state:' % (len(self.image_queue)), shared_buff_state, flush=True)
                        if failure_counter > self.allow_n_failures:
                            print('Reached max consequtive failures, Stopping image acquisition:', flush=True)
                            self.mark_failed()
                            break
                        else:
                            # Push next image in image queue into shared mem if needed
                            if len(self.image_queue) >= self.n_shared_elements and need_to_push:
                                self.queue_lock.acquire()
                                self.to_shared_mem(self.image_queue[:self.n_shared_elements])
                                el_not_pushed_last_cycle = False
                                if self.should_update_barcode_img() and len(self.image_queue) > 0:
                                    self.update_barcode_img(self.image_queue[-1]) # last image from queue
                                self.queue_lock.release()
                            elif need_to_push:
                                # Most likely an error, mark it for next cycle
                                el_not_pushed_last_cycle = True
                
            # Perform ops according to external requests(e.g, white balance)
            self.perform_external_ops()
            self.wait_for_complete_cycle()
            self.last_frame_ts = loop_start_time_ms
        
        # Close cameras
        # =============
        self.stream.release()
    
    def run_playback(self):
        self.start_counter = 0
        img_idx = 0
        finished_reading = False
        self.time_anchor = time.time()
        self.last_valid_grab_ts = None

        while self.keep_running:
            loop_start_time_ms = time.time() * 1000
            run_single_step = self.is_single_step()

            if self.grabber_state==GrabberState.Running or run_single_step:
                # trigger next image
                # ==================
                self.stream.grab()
                # read next image
                # ===============
                if (not finished_reading) and len(self.image_queue) < self.max_buffer_size:
                    ret, image_data = self.stream.read()
                    if ret:
                        next_queue_item = dict(data=image_data, ts=loop_start_time_ms, img_idx=img_idx)
                        img_idx += 1
                        if 'mean_ts' in image_data:
                            next_queue_item['ts'] = image_data['mean_ts']
                        self.image_queue.append(next_queue_item)
                    else:
                        ret_status = ['%s:%r' %(k, val['ret']) for k, val in image_data.items()]
                        finished_reading = True
                # if self.debug:
                #     print('Time diff ms(grabber call): %.2f' % time_since_last_grab_ms)
                if self.start_counter < self.n_buffer_frames:
                    self.start_counter += 1
                else:
                    # check for popped elements and chop list from left
                    n_elements_to_pop, init, _ = self.n_elements_used()
                    # update shared memory with next frames to process to fill in popped elements
                    for _ in range(n_elements_to_pop):
                        if len(self.image_queue) > 0:
                            self.image_queue.pop(0)
                    if len(self.image_queue) < 1 and finished_reading:
                        break

                    need_to_push = init or (n_elements_to_pop > 0)
                    if len(self.image_queue) >= self.n_shared_elements and need_to_push:
                        self.queue_lock.acquire()
                        self.to_shared_mem(self.image_queue[:self.n_shared_elements])
                        if self.should_update_barcode_img() and len(self.image_queue) > 0:
                            self.update_barcode_img(self.image_queue[-1]) # last image from queue
                        self.queue_lock.release()

            # Make sure last frame is not stuck in frame by frame mode
            last_frame_in = finished_reading and len(self.image_queue)==1
            if last_frame_in and n_elements_to_pop > 0:
                self.queue_lock.acquire()
                self.to_shared_mem([self.image_queue[0]])
                self.queue_lock.release()
            
            # Perform ops according to external requests(e.g, white balance)
            self.perform_external_ops()

            self.wait_for_complete_cycle()

            self.last_frame_ts = loop_start_time_ms
        
        self.mark_finished()
    
    def mark_finished(self):
        self.queue_lock.acquire()
        state_arr = np.frombuffer(self.shared_mem['state'], dtype=np.uint8)
        state_arr[0] = SharedMemState.Done # mark as ready to use
        self.queue_lock.release()

    def mark_failed(self):
        self.queue_lock.acquire()
        state_arr = np.frombuffer(self.shared_mem['state'], dtype=np.uint8)
        state_arr[0] = SharedMemState.Failed # mark as failed
        self.queue_lock.release()

    def wait_for_complete_cycle(self):
        curr_time = time.time()
        time_in_cycle_ms = ((curr_time - self.time_anchor) * 1e3) % self.target_ms
        time_to_cycle = (self.target_ms - self.time_to_sample_ms - time_in_cycle_ms) * 1e-3
        if time_to_cycle > 0.:
            # Extend time of sample in one cycle to avoid cases of too short cycle
            # We want to make sure at least target ms (usually 50ms) between grab and retrieve 
            if self.last_valid_grab_ts is not None and \
                (curr_time * 1e3 - self.last_valid_grab_ts) < (self.target_ms / self.n_buffer_frames):
                time_to_cycle += (self.target_ms * 1e-3)
                print('Warning: Time to cycle too short, will sleep %.1fms' %(time_to_cycle * 1e3), flush=True)
            time.sleep(time_to_cycle)
    
    def n_elements_used(self):
        self.queue_lock.acquire()
        state = np.frombuffer(self.shared_mem['state'], dtype=np.uint8)
        self.queue_lock.release()
        return (state==SharedMemState.Used).sum(), np.all(state==SharedMemState.Init), state

    def to_shared_mem(self, samples):
        '''
        Push to last shared buffer location
        '''
        # push
        img_arr = np.frombuffer(self.shared_mem['img'], dtype=np.uint8)
        ts_arr = np.frombuffer(self.shared_mem['ts'], dtype=np.double)
        img_idx_arr = np.frombuffer(self.shared_mem['img_idx'], dtype=np.uint32)
        state_arr = np.frombuffer(self.shared_mem['state'], dtype=np.uint8)
        queue_size = np.frombuffer(self.shared_mem['queue_size'], dtype=np.uint32)
        update_shared_mem = not np.all(state_arr==SharedMemState.Ready)
        if update_shared_mem:
            for push_idx, sample in enumerate(samples):
                start_idx = self.img_buff_size * push_idx
                for k in self.camera_names:
                    end_idx = start_idx + self.cam_buffer_size[k]
                    img_arr[start_idx:end_idx] = sample['data'][k]['img'].reshape(self.cam_buffer_size[k])
                    start_idx = end_idx
                ts_arr[push_idx] = sample['ts']
                img_idx_arr[push_idx] = sample['img_idx']
                state_arr[push_idx] = SharedMemState.Ready # mark as ready to use
        # update image queue size
        queue_size[0] = len(self.image_queue)
    
    def update_barcode_img(self, sample):
        '''
        Update barcode UI image with last acquired image
        '''
        barcode_key_name = self.barcode_viz_buff_info['cam']
        # Fill algo crop
        barcode_algo_crop = self.barcode_crop_box.crop(sample['data'][barcode_key_name]['img'])
        barcode_algo_crop = cv2.cvtColor(barcode_algo_crop, cv2.COLOR_BGR2GRAY)
        algo_crop_arr = np.frombuffer(self.shared_mem['barcode_crop_img'], dtype=np.uint8)
        algo_crop_arr[:] = barcode_algo_crop.reshape(self.barcode_crop_size[0] * self.barcode_crop_size[1])
        # Fill viz image
        sample_resized = cv2.resize(sample['data'][barcode_key_name]['img'], (self.barcode_viz_buff_info['w'], self.barcode_viz_buff_info['h']))
        viz_arr = np.frombuffer(self.shared_mem['barcode_viz_img'], dtype=np.uint8)
        viz_arr[:] = sample_resized.reshape(self.barcode_viz_buff_info['size'])


    def update_queue_size(self):
        queue_size = np.frombuffer(self.shared_mem['queue_size'], dtype=np.uint32)
        queue_size[0] = len(self.image_queue)

    def from_shared_mem(self, skip_frame):
        pop_idx = 1 if skip_frame else 0
        start_idx = pop_idx * self.img_buff_size

        #  pop
        if self.cuda_driver is not None:
            img_arr = np.frombuffer(self.shared_mem['img'], dtype=np.uint8)
        else:
            img_arr = np.frombuffer(self.shared_mem['img'], dtype=np.uint8).copy()
        ts_arr = np.frombuffer(self.shared_mem['ts'], dtype=np.double).copy()
        state_arr = np.frombuffer(self.shared_mem['state'], dtype=np.uint8)
        img_idx_arr = np.frombuffer(self.shared_mem['img_idx'], dtype=np.uint32).copy()
        
        # Check stream has not failed
        assert state_arr[0]!=SharedMemState.Failed, 'Stream Error!!'

        queue_item = dict(data=dict(), ts=None, img_idx=None)
        if self.profiler:
            self.profiler.start_block('copy_cam_buffers')
        if self.cuda_driver is not None:
            stream = self.cuda_driver.Stream()
        for k in self.camera_names:
            end_idx = start_idx + self.cam_buffer_size[k]
            valid_output_target = k in self.output_targets
            if valid_output_target:
                if self.cuda_driver is not None:
                        input_img = img_arr[start_idx:end_idx] # Buffer w/o reshaping
                        self.cuda_driver.memcpy_htod_async(self.output_targets[k].base, input_img, stream)
                else:
                    input_img = img_arr[start_idx:end_idx].reshape(self.img_sizes[k])
                    self.output_targets[k][:] = input_img
            else:
                img = img_arr[start_idx:end_idx].reshape(self.img_sizes[k])
                queue_item['data'][k] = dict(img=img)
            start_idx = end_idx
        if self.cuda_driver is not None:
            stream.synchronize()
        if self.profiler:
            self.profiler.stop_block('copy_cam_buffers')
        queue_item['ts'] = ts_arr[pop_idx]
        queue_item['img_idx'] = img_idx_arr[pop_idx]
        state_arr[:pop_idx+1] = SharedMemState.Used # mark as used all elements before the one used, inclusive
        # Update barcode viz image if needed
        if self.should_update_barcode_img():
            barcode_crop_arr = np.frombuffer(self.shared_mem['barcode_crop_img'], dtype=np.uint8).copy()
            queue_item['barcode_crop'] = barcode_crop_arr.reshape(self.barcode_crop_size)
            barcode_viz_arr = np.frombuffer(self.shared_mem['barcode_viz_img'], dtype=np.uint8).copy()
            queue_item['barcode_viz'] = barcode_viz_arr.reshape((self.barcode_viz_buff_info['h'], self.barcode_viz_buff_info['w'], self.barcode_viz_buff_info['ch']))

        return queue_item
    
    def queue_size(self):
        # return queue_size
        self.queue_lock.acquire()
        queue_size = np.frombuffer(self.shared_mem['queue_size'], dtype=np.uint32)[0]
        state_arr = np.frombuffer(self.shared_mem['state'], dtype=np.uint8)
        n_shared_valid = (state_arr==SharedMemState.Ready).sum()
        self.queue_lock.release()

        return queue_size, n_shared_valid
    
    def pop_loop(self, can_skip):
        keep_polling = True
        polling_start = time.time()
        polling_idx = 0
        while keep_polling:
            polling_idx +=1
            time.sleep(1e-3)
            self.queue_lock.acquire()
            queue_size = np.frombuffer(self.shared_mem['queue_size'], dtype=np.uint32)[0]
            state_arr = np.frombuffer(self.shared_mem['state'], dtype=np.uint8)
            if np.any(state_arr==SharedMemState.Done):
                self.queue_lock.release()
                return queue_size, False, dict(data=SharedMemState.Done, ts=0, img_idx=-1)
            elif np.any(state_arr==SharedMemState.Failed):
                self.queue_lock.release()
                return queue_size, False, dict(data=SharedMemState.Failed, ts=0, img_idx=-1)
            n_shared_valid = (state_arr==SharedMemState.Ready).sum()
            if n_shared_valid==self.n_shared_elements:
                skip_frame = can_skip and max(n_shared_valid, queue_size) > self.n_shared_elements
                if self.profiler:
                    self.profiler.start_block('from_shared_mem')
                queue_item = self.from_shared_mem(skip_frame)
                if self.profiler:
                    self.profiler.stop_block('from_shared_mem')
                keep_polling = False
            self.queue_lock.release()
            # Periodic test if we got to a deadlock - fail if we are waiting for more than 10 seconds
            if polling_idx % 100==0:
                polling_time = time.time() - polling_start
                if polling_time > 10:
                    # TODO - initialize state of shared buffer in the future
                    print('Error: polling for image data %.2f seconds' % polling_time, flush=True)
                    print('queue_size=%d, shared mem state: ' % queue_size, state_arr, flush=True)
                    raise Exceptions.CameraGrabFrameException("Failed Due to stream process deadlock")

        
        return queue_size, skip_frame, queue_item

    def grab(self, can_skip=False):
        # Wait until queue is not empty - blocking
        if self.profiler:
            self.profiler.start_block('pop_loop')
        queue_size, skip_frame, queue_item = self.pop_loop(can_skip)
        if self.profiler:
            self.profiler.stop_block('pop_loop')

        if self.debug and skip_frame:
                print('Warning: Skipping image due to long queue: %d' % queue_size)
        acquisition_diff_ms = (queue_item['ts'] - self.last_acquisition_ts)
        short_cycle_ms = 2 * self.short_cycle_ms if skip_frame else self.short_cycle_ms
        long_cycle_ms = 2 * self.long_cycle_ms if skip_frame else self.long_cycle_ms
        if self.stream_type!='playback' and acquisition_diff_ms < short_cycle_ms:
            print('Warning: Acquision cycle is too short:%.2f. ts:[%.2f, %.2f], queue size:%d, skip=%r, img_idx=%d' % \
                (acquisition_diff_ms, self.last_acquisition_ts, queue_item['ts'], queue_size, skip_frame, queue_item['img_idx']), flush=True)
        if self.stream_type!='playback' and acquisition_diff_ms > long_cycle_ms:
            print('Warning: Acquision cycle is too long:%.2f. ts:[%.2f, %.2f], queue size:%d, skip=%r, img_idx=%d' % \
                (acquisition_diff_ms, self.last_acquisition_ts, queue_item['ts'], queue_size, skip_frame, queue_item['img_idx']), flush=True)
        if self.debug:
            print('Time diff ms(throughput): %.2f' % acquisition_diff_ms)
            print('Time diff ms(latency): %.2f' % (time.time() * 1e3 - queue_item['ts']))
        self.last_acquisition_ts = queue_item['ts']

        if queue_item['data']==SharedMemState.Done:
            raise Exceptions.PossiblyMovieFinished("Maybe movie ended")
        elif queue_item['data']==SharedMemState.Failed:
            raise Exceptions.CameraGrabFrameException("Failed Due to stream process failure")
        return queue_item, skip_frame, queue_size
    
    def perform_external_ops(self):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        if op[0]!=ImageOp.SingleStep:
            if op[0]==ImageOp.DriverWhiteBalance:
                self.stream.driver_wb()
            elif op[0]==ImageOp.ResetWhiteBalance:
                self.new_wb_coefficients = dict()
                for camera in self.camera_names:
                    self.new_wb_coefficients[camera] = dict(red=1, green=1, blue=1)
                self.stream.set_new_wb_coefficients(self.new_wb_coefficients)
            elif op[0]==ImageOp.ExternalWhiteBalance:
                if op[3] and config.SAVE_TEMPLATES:
                    self.save_images(frame_num=op[2])
                self.init_wb_calibration(img_idx=-1, frame_num=op[2])
            elif op[0]==ImageOp.OneTimeExternalWB:
                self.init_wb_calibration(img_idx=0, new_calib=op[1], frame_num=op[2])
            elif op[0]==ImageOp.ShiftSamplePhase:
                self.move_sample_phase()
            elif op[0]==ImageOp.ToggleStartStop:
                start_or_stop = op[1]
                self.stop_or_start_grabber(start_or_stop)
            elif op[0]==ImageOp.EmptyImageQueue:
                self.flush_image_queue()
            elif op[0]==ImageOp.CloseStream:
                self.close_cameras()
            elif op[0]==ImageOp.TuneExposure:
                if op[3] and config.SAVE_TEMPLATES:
                    self.save_images(frame_num=op[2])
                self.init_exposure_tuning(new_calib=op[1], frame_num=op[2])
            elif op[0]==ImageOp.OneTimeExpTuning:
                self.init_short_exposure_tuning(new_calib=op[1], frame_num=op[2])
            elif op[0]==ImageOp.MonitorValues:
                self.monitor_templates_values(frame_num=op[2])
            elif op[0]==ImageOp.PrintCalibSum:
                self.wb_data.end_calib(status="success", frame_num=op[2])
            elif op[0]==ImageOp.FindTemplates:
                self.search_templates()
            op[0] = ImageOp.NoOp
            op[1:] = 0
        self.op_lock.release()

    def save_images(self, frame_num, img_idx=-1):
        images = dict()
        for camera in self.camera_names:
            if camera == 'right':
                continue
            images[camera] = self.image_queue[img_idx]['data'][camera]['img']
        self.wb_data.wb_logger.log(images, frame_num)

    def search_templates(self):
        templates_status = dict()
        for camera in self.camera_names:
            if camera == 'right':
                continue
            image = self.image_queue[0]['data'][camera]['img']
            cropped_images, _ = self.template_matching.match(camera=camera, image=image, force_range=True, debug=False)
            templates_status[camera] = len(cropped_images) > 0
        self.write_templates_status(templates_status)

    def find_templates(self):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.FindTemplates
        self.op_lock.release()

    def print_calibration_summary(self, frame_num):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.PrintCalibSum
        op[2] = frame_num
        self.op_lock.release()

    def reset_white_balance(self):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.ResetWhiteBalance
        self.op_lock.release()

    def driver_white_balance(self):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.DriverWhiteBalance
        self.op_lock.release()

    def external_white_balance(self, frame_num, save_images=False):
        # initiated from MD process
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.ExternalWhiteBalance
        self.wb_lock.acquire()
        wb_status = np.frombuffer(self.shared_mem['wb_status'], dtype=np.uint8)
        wb_status[0] = WhiteBalanceStatus.Empty
        self.wb_lock.release()
        op[2] = frame_num
        op[3] = 1 if save_images else 0
        self.op_lock.release()

    def short_wb_cycle(self, new_calib, frame_num):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.OneTimeExternalWB
        self.wb_lock.acquire()
        wb_status = np.frombuffer(self.shared_mem['wb_status'], dtype=np.uint8)
        wb_status[0] = WhiteBalanceStatus.Empty
        self.wb_lock.release()
        op[1] = 1 if new_calib else 0
        op[2] = frame_num
        self.op_lock.release()

    def tune_exposure(self, init_event=False, frame_num=0, save_images=False):
        # initiated from MD process
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.TuneExposure
        exposure_status = np.frombuffer(self.shared_mem['exposure_status'], dtype=np.uint8)
        exposure_status[0] = ExposureStatus.Empty
        op[1] = 1 if init_event else 0
        op[2] = frame_num
        op[3] = 1 if save_images else 0
        self.op_lock.release()

    def short_exp_cycle(self, init_event=False, frame_num=0):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.OneTimeExpTuning
        exposure_status = np.frombuffer(self.shared_mem['exposure_status'], dtype=np.uint8)
        exposure_status[0] = ExposureStatus.Empty
        op[1] = 1 if init_event else 0
        op[2] = frame_num
        self.op_lock.release()

    def init_exposure_tuning(self, new_calib, frame_num):
        if new_calib:
            self.wb_data.new_calib(calib_type='full_wb_cycle', frame_num=frame_num)
        if not hasattr(self, 'during_exp_tuning') or not self.during_exp_tuning:
            self.during_exp_tuning = True
            self.exposure_params = dict()
            for camera in self.camera_names:
                if camera == 'right':
                    continue
                self.exposure_params[camera] = dict(exp_jump_idx=0, finished=False, lighting=Lighting.Empty,
                                                    gain_jump=self.gain_jump)
        self.exposure_tuning_cycle(frame_num=frame_num)

    def init_short_exposure_tuning(self, new_calib, frame_num):
        if new_calib:
            self.wb_data.new_calib(calib_type='online_wb', frame_num=frame_num)
        self.during_exp_tuning = True
        self.exposure_params = dict()
        for camera in self.camera_names:
            if camera == 'right':
                continue
            self.exposure_params[camera] = dict(exp_jump_idx=0, finished=False, lighting=Lighting.Empty,
                                                gain_jump=self.gain_jump)
        self.wb_data.add_monitored_values(self.template_matching.monitor.get_last())
        self.exposure_tuning_cycle(one_cycle=True, frame_num=frame_num)

    def exposure_tuning_cycle(self, one_cycle=False, frame_num=0):
        image_idx = 0 if one_cycle else -1
        status = ExposureStatus.Success
        self.exposure_values = self.stream.read_exposures()
        self.gain_values = self.stream.read_gains()
        data = dict()
        templates_status = dict()
        for camera in self.camera_names:
            if camera == 'right' or self.exposure_params[camera]['finished']:
                continue
            image = self.image_queue[image_idx]['data'][camera]['img']
            force_range = 1 if one_cycle else None
            cropped_images, _ = self.template_matching.match(camera=camera, image=image, force_range=force_range,
                                                             debug=self.print_wb_logs)
            templates_status[camera] = len(cropped_images) > 0
            mean = self.template_matching.calculate_mean(cropped_images=cropped_images, camera=camera, image=image)
            data[camera] = dict(gain=self.gain_values[camera], exp=self.exposure_values[camera], mean=int(mean))
            if self.print_wb_logs:
                print("mean: {} for cam: {}".format(int(mean), camera), flush=True)
            if self.templates_mean['target'] - self.templates_mean['range'] < mean < self.templates_mean['target'] + self.templates_mean['range']:
                data[camera]['state'] = 'in_range'
                self.exposure_params[camera]['finished'] = True
                if self.print_wb_logs:
                    print("\033[35mExposure calibration for camera {} has finished - within range.\033[00m".format(camera), flush=True)
            elif mean > self.templates_mean['target'] + self.templates_mean['range'] and self.exposure_params[camera]['lighting'] in (Lighting.Bright, Lighting.Empty):
                self.exposure_params[camera]['lighting'] = Lighting.Bright
                changed = self.change_exposure_gain(camera)
                if changed:
                    data[camera]['state'] = 'too_bright'
                    status = ExposureStatus.Failed
                else:
                    data[camera]['state'] = 'reached_bottom_limits'

            elif mean < self.templates_mean['target'] - self.templates_mean['range'] and self.exposure_params[camera]['lighting'] in (Lighting.Dark, Lighting.Empty):
                self.exposure_params[camera]['lighting'] = Lighting.Dark
                changed = self.change_exposure_gain(camera)
                if changed:
                    data[camera]['state'] = 'too_dark'
                    status = ExposureStatus.Failed
                else:
                    data[camera]['state'] = 'reached_upper_limits'
            else:
                # mean changed from high->low or low->high, should reduce exposure jump
                self.exposure_params[camera]['exp_jump_idx'] += 1
                self.exposure_params[camera]['gain_jump'] = 1
                if self.exposure_params[camera]['exp_jump_idx'] >= len(self.exposure_jumps):
                    # exposure is at the lower resolution
                    data[camera]['state'] = 'reached_max_resolution'
                    self.exposure_params[camera]['finished'] = True
                    if self.print_wb_logs:
                        print("\033[35mExposure calibration for camera {} has finished - reached max resolution\033[00m".format(camera), flush=True)
                else:
                    if self.print_wb_logs:
                        print("\033[91mChanging exposure jump to {} and gain jump to {} [{}]\033[00m".format(
                            self.exposure_jumps[self.exposure_params[camera]['exp_jump_idx']],
                            self.exposure_params[camera]['gain_jump'], camera), flush=True)
                    if mean < self.templates_mean['target']:
                        self.exposure_params[camera]['lighting'] = Lighting.Dark
                        data[camera]['state'] = 'too_dark'
                        if self.exposure_values[camera] < self.exposure_range[1]:
                            self.exposure_values[camera] = min(self.exposure_range[1],
                                                               self.exposure_values[camera] + self.exposure_jumps[
                                                                   self.exposure_params[camera]['exp_jump_idx']])
                            if self.print_wb_logs:
                                print("\033[91mTemplates are too dark, increasing exposure time to {} [{}]\033[00m".format(
                                        self.exposure_values[camera], camera), flush=True)
                        else:
                            self.gain_values[camera] = min(self.gain_range[1], self.gain_values[camera] + self.exposure_params[camera]['gain_jump'])
                            if self.print_wb_logs:
                                print("\033[91mTemplates are too dark, increasing gain to {} [{}]\033[00m".format(
                                        self.gain_values[camera], camera), flush=True)
                    else:
                        self.exposure_params[camera]['lighting'] = Lighting.Bright
                        data[camera]['state'] = 'too_bright'
                        if self.exposure_values[camera] > self.exposure_range[0]:
                            self.exposure_values[camera] = max(self.exposure_range[0],
                                                               self.exposure_values[camera] - self.exposure_jumps[
                                                                   self.exposure_params[camera]['exp_jump_idx']])
                            if self.print_wb_logs:
                                print("\033[91mTemplates are too bright, reducing exposure time to {} [{}]\033[00m".format(
                                    self.exposure_values[camera], camera), flush=True)
                        else:
                            self.gain_values[camera] = max(self.gain_range[0], self.gain_values[camera] - self.exposure_params[camera]['gain_jump'])
                            if self.print_wb_logs:
                                print("\033[91mTemplates are too bright, decreasing gain to {} [{}]\033[00m".format(
                                    self.gain_values[camera], camera), flush=True)
                    status = ExposureStatus.Failed

        if status == ExposureStatus.Failed:
            self.exposure_values['right'] = self.exposure_values['left']
            self.stream.set_exposures(self.exposure_values)
            self.gain_values['right'] = self.gain_values['left']
            self.stream.set_gains(self.gain_values)
            if one_cycle:
                if self.print_wb_logs:
                    print("\033[35mFinished one cycle of exposure tuning!!\033[00m", flush=True)
                self.during_exp_tuning = False
        else:
            if one_cycle:
                if self.print_wb_logs:
                    print("\033[35mFinished one cycle of exposure tuning - values were not changed!!\033[00m", flush=True)
                self.during_exp_tuning = False
                self.wb_data.end_calib(status='failed_exp_gain', frame_num=frame_num)
                self.should_not_replace_cb()
                self.template_matching.end_calib_in_monitor()
                return
            else:
                if self.print_wb_logs:
                    print("\033[35mFinished exposure tuning!!\033[00m", flush=True)
                self.during_exp_tuning = False

        self.wb_data.add_calibration_step(data)

        self.wb_lock.acquire()
        exposure_status = np.frombuffer(self.shared_mem['exposure_status'], dtype=np.uint8)
        exposure_status[0] = status
        self.wb_lock.release()
        self.write_templates_status(templates_status)

    def change_exposure_gain(self, camera):
        if self.exposure_params[camera]['lighting'] == Lighting.Bright:
            # bright state, should decrease gain / exposure
            if self.exposure_values[camera] > self.exposure_range[0] and self.gain_values[camera] <= self.gain_anchor:  # reduce exposure
                self.exposure_values[camera] = max(self.exposure_range[0],
                                                   self.exposure_values[camera] - self.exposure_jumps[
                                                       self.exposure_params[camera]['exp_jump_idx']])
                if self.print_wb_logs:
                    print("\033[91mTemplates are too bright, reducing exposure time to {} [{}]\033[00m".format(
                        self.exposure_values[camera], camera), flush=True)
            elif self.gain_values[camera] > self.gain_range[0]:  # reduce gain
                self.gain_values[camera] = max(self.gain_range[0], self.gain_values[camera] - self.exposure_params[camera]['gain_jump'])
                if self.print_wb_logs:
                    print("\033[91mTemplates are too bright, decreasing gain to {} [{}]\033[00m".format(
                        self.gain_values[camera], camera), flush=True)
            else:  # reached the range limit - stop tuning
                self.exposure_params[camera]['finished'] = True
                if self.print_wb_logs:
                    print("\033[91mTemplates are too bright, but reached range limits - stopping tuning [{}]\033[00m".format(camera), flush=True)
                    print("gain = {}, exposure = {}".format(self.gain_values[camera], self.exposure_values[camera]), flush=True)
                return False
            return True
        else:
            # dark state, should increase gain / exposure
            if self.exposure_values[camera] < self.exposure_range[1] and self.gain_values[camera] >= self.gain_anchor:  # increase exposure
                self.exposure_values[camera] = min(self.exposure_range[1],
                                                   self.exposure_values[camera] + self.exposure_jumps[
                                                       self.exposure_params[camera]['exp_jump_idx']])
                if self.print_wb_logs:
                    print("\033[91mTemplates are too dark, increasing exposure time to {} [{}]\033[00m".format(
                        self.exposure_values[camera], camera), flush=True)
            elif self.gain_values[camera] < self.gain_range[1]:  # increase gain
                self.gain_values[camera] = min(self.gain_range[1], self.gain_values[camera] + self.exposure_params[camera]['gain_jump'])
                if self.print_wb_logs:
                    print("\033[91mTemplates are too dark, increasing gain to {} [{}]\033[00m".format(
                        self.gain_values[camera], camera), flush=True)
            else:  # reached the range limit - stop tuning
                self.exposure_params[camera]['finished'] = True
                if self.print_wb_logs:
                    print("\033[91mTemplates are too dark, but reached range limits - stopping tuning [{}]\033[00m".format(camera), flush=True)
                    print("gain = {}, exposure = {}".format(self.gain_values[camera], self.exposure_values[camera]), flush=True)
                return False
            return True

    def read_exposure_tuning_status(self):
        self.wb_lock.acquire()
        exposure_status = np.frombuffer(self.shared_mem['exposure_status'], dtype=np.uint8)
        status_to_return = exposure_status[0]
        exposure_status[0] = ExposureStatus.Empty
        self.wb_lock.release()
        return status_to_return

    def should_not_replace_cb(self):
        # camera process
        self.queue_lock.acquire()
        replace_cb_img = np.frombuffer(self.shared_mem['replace_cb_img'], dtype=np.uint8)
        replace_cb_img[0] = ReplaceCBState.ShouldNotReplace
        self.queue_lock.release()

    def read_replace_cb_state(self):
        # MD process
        self.queue_lock.acquire()
        replace_cb_img = np.frombuffer(self.shared_mem['replace_cb_img'], dtype=np.uint8)
        status_to_return = replace_cb_img[0]
        replace_cb_img[0] = ReplaceCBState.ShouldReplace
        self.queue_lock.release()
        return status_to_return

    def monitor_templates_values(self, frame_num):
        failed = False
        templates_status = dict()
        for camera in self.camera_names:
            if camera == 'right':
                continue
            image = self.image_queue[0]['data'][camera]['img']
            cropped_images, _ = self.template_matching.match(camera=camera, image=image, force_range=1, debug=self.debug_online_wb)
            templates_status[camera] = len(cropped_images) > 0
            if len(cropped_images) > 0:
                _ = self.template_matching.calculate_mean(cropped_images=cropped_images, camera=camera, image=image,
                                                          should_monitor=True)
            else:
                if self.print_wb_logs:
                    print("\033[91mCan't monitor camera {} - templates are not found\033[00m".format(camera), flush=True)
                failed = True
        self.write_templates_status(templates_status)
        if failed:
            return
        need_wb, need_exp_tuning, calib_status = self.template_matching.monitoring_status()
        if need_wb:
            self.wb_lock.acquire()
            wb_calibration_status = np.frombuffer(self.shared_mem['need_wb'], dtype=np.uint8)
            wb_calibration_status[0] = MonitorWBState.NeedCalibration
            wb_calibration_status[1] = len(self.image_queue) + self.n_buffer_frames
            wb_calibration_status[2] = calib_status
            self.wb_lock.release()
        elif need_exp_tuning:
            self.wb_lock.acquire()
            exposure_calibration_status = np.frombuffer(self.shared_mem['need_exp_tuning'], dtype=np.uint8)
            exposure_calibration_status[0] = MonitorExposureState.NeedCalibration
            exposure_calibration_status[1] = len(self.image_queue) + self.n_buffer_frames
            exposure_calibration_status[2] = calib_status
            self.wb_lock.release()
        elif calib_status==MonitoringStatus.CalibEnded:
            self.wb_data.end_calib(status='success', frame_num=frame_num)

    def is_wb_calib_needed(self):
        self.wb_lock.acquire()
        wb_calibration_status = np.frombuffer(self.shared_mem['need_wb'], dtype=np.uint8)
        status_to_return = wb_calibration_status[0]
        que_to_return = wb_calibration_status[1]
        new_calib = wb_calibration_status[2]
        wb_calibration_status[0] = MonitorWBState.NoCalibrationNeeded
        self.wb_lock.release()
        return status_to_return, que_to_return, new_calib

    def is_exp_tuning_needed(self):
        self.wb_lock.acquire()
        exposure_calibration_status = np.frombuffer(self.shared_mem['need_exp_tuning'], dtype=np.uint8)
        status_to_return = exposure_calibration_status[0]
        que_to_return = exposure_calibration_status[1]
        new_calib = exposure_calibration_status[2]
        exposure_calibration_status[0] = MonitorExposureState.NoCalibrationNeeded
        self.wb_lock.release()
        return status_to_return, que_to_return, new_calib

    def init_wb_calibration(self, img_idx=-1, new_calib=0, frame_num=0):
        # initiated from acquisition process
        if new_calib:
            self.wb_data.new_calib(calib_type='online_wb', frame_num=frame_num)
        self.wb_calibration_needed = dict()
        for camera in self.camera_names:
            self.wb_calibration_needed[camera] = True
        self.new_wb_coefficients = dict()
        self.old_wb_coefficients = self.stream.read_wb_coefficients()
        if img_idx==0:
            self.wb_data.add_monitored_values(self.template_matching.monitor.get_last())
        self.calibrate_wb_cycle(img_idx=img_idx, frame_num=frame_num)

    def calibrate_wb_cycle(self, img_idx=-1, frame_num=0):
        data = dict()
        success_per_cam = dict()
        templates_status = dict()
        for camera in self.camera_names:
            if camera == 'right':
                continue
            success_per_cam[camera] = True
            image = self.image_queue[img_idx]['data'][camera]['img']
            force_range = 1 if img_idx == 0 else None
            cropped_images, coords = self.template_matching.match(camera=camera, image=image, force_range=force_range,
                                                                  debug=self.print_wb_logs)
            templates_status[camera] = len(cropped_images) > 0
            if len(cropped_images) > 0:
                if self.print_wb_logs:
                    print("Found calibration templates for camera {}".format(camera), flush=True)
                    print("Number of crops catched: {}".format(len(cropped_images)), flush=True)
                self.new_wb_coefficients[camera], rgb_values = wb.grey_world(cropped_images=cropped_images,
                                                                               old_wb_params=self.old_wb_coefficients[camera])
                data[camera] = dict(rgb_values=rgb_values, old_wb_coefficients=self.old_wb_coefficients[camera],
                                    new_wb_coefficients=self.new_wb_coefficients[camera])
                if camera == 'left':
                    self.new_wb_coefficients['right'] = self.new_wb_coefficients['left']
            else:
                success_per_cam[camera] = False
                if self.print_wb_logs:
                    print("\033[91mTemplates for camera {} were not found. WB calibration is aborted\033[00m".format(camera), flush=True)
        self.write_templates_status(templates_status)

        if False in success_per_cam.values():
            failed_cameras = 'front_and_back' if (not success_per_cam['left'] and not success_per_cam['back']) else \
                'front' if not success_per_cam['left'] else 'back'
            self.wb_data.end_calib("failed_{}".format(failed_cameras), frame_num=frame_num)
            self.should_not_replace_cb()
            self.template_matching.monitor.end_calib()
            self.wb_lock.acquire()
            wb_status = np.frombuffer(self.shared_mem['wb_status'], dtype=np.uint8)
            wb_status[0] = WhiteBalanceStatus.Failed
            self.wb_lock.release()
            return

        self.wb_data.add_calibration_step(data)

        self.wb_lock.acquire()
        wb_status = np.frombuffer(self.shared_mem['wb_status'], dtype=np.uint8)
        wb_status[0] = WhiteBalanceStatus.Success
        self.wb_lock.release()
        self.stream.set_new_wb_coefficients(self.new_wb_coefficients)

    def write_templates_status(self, status_dict):
        self.queue_lock.acquire()
        templates_detected_status = np.frombuffer(self.shared_mem['templates_detected'], dtype=np.uint8)
        for camera, found in status_dict.items():
            index = 0 if camera == 'left' else 1
            templates_detected_status[index] = TemplatesDetectedState.TemplatesFound if found else \
                TemplatesDetectedState.TemplatesNotFound
        self.queue_lock.release()

    def check_templates_status(self):
        self.queue_lock.acquire()
        templates_detected_status = np.frombuffer(self.shared_mem['templates_detected'], dtype=np.uint8)
        front_status = templates_detected_status[0]
        templates_detected_status[0] = TemplatesDetectedState.Empty
        back_status = templates_detected_status[1]
        templates_detected_status[1] = TemplatesDetectedState.Empty
        self.queue_lock.release()
        return front_status, back_status

    def read_wb_status(self):
        self.wb_lock.acquire()
        wb_status = np.frombuffer(self.shared_mem['wb_status'], dtype=np.uint8)
        status_to_return = wb_status[0]
        wb_status[0] = WhiteBalanceStatus.Empty
        self.wb_lock.release()
        return status_to_return

    def monitor_values(self, frame_num):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.MonitorValues
        op[2] = frame_num
        self.op_lock.release()

    def shift_sample_phase(self):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.ShiftSamplePhase
        self.op_lock.release()
    
    def toggle_start_stop(self, start=None):
        '''
        start = None -> toggle
        start = True -> start stream
        start = False -> stop stream
        '''
        assert start in (True, False, None)
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.ToggleStartStop
        op[1] = StartStopState.Start if start==True else StartStopState.Stop if start==False else StartStopState.Toggle
        self.op_lock.release()

    def trigger_single_step(self):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.SingleStep
        self.op_lock.release()
    
    def empty_image_queue(self):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.EmptyImageQueue
        self.op_lock.release()
    
    def close(self):
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        op[0] = ImageOp.CloseStream
        self.op_lock.release()

    def is_single_step(self):
        ret = False
        self.op_lock.acquire()
        op = np.frombuffer(self.shared_mem['ops'], dtype=np.uint32)
        if op[0]==ImageOp.SingleStep and self.grabber_state==GrabberState.Paused:
            ret = True
            op[0] = ImageOp.NoOp
        self.op_lock.release()
        return ret

    def move_sample_phase(self, shift_ms=1.0):
        self.time_anchor += (shift_ms * 1e-3)
    
    def stop_or_start_grabber(self, start):
        if start==StartStopState.Toggle:
            next_state = GrabberState.Paused if self.grabber_state==GrabberState.Running \
                else GrabberState.Running
        else:
            next_state = GrabberState.Running if start==StartStopState.Start else GrabberState.Paused
        if next_state==self.grabber_state:
            return
        if next_state==GrabberState.Paused:
            if self.isRealTime:
                self.stream.pause()
                self.image_queue = []
                self.queue_lock.acquire()
                self.reset_shared_mem()
                self.queue_lock.release()
                self.reset_locks()
        elif next_state==GrabberState.Running:
            self.stream.resume()
            if self.isRealTime:
                self.start_counter = 0 # so the queue can be filled again
                self.last_valid_grab_ts = None # reset variable

        print('Image Grabber State transition: %s -> %s' % (self.grabber_state.name, next_state.name), flush=True)
        self.grabber_state = next_state
    
    def flush_image_queue(self):
        print('Flushing images in queue', flush=True)
        self.image_queue = []
        self.queue_lock.acquire()
        self.reset_shared_mem()
        self.queue_lock.release()
        self.reset_locks()
    
    def close_cameras(self):
        print('Closing stream', flush=True)
        self.flush_image_queue()
        self.keep_running = False
    
    def is_stream_ready(self):
        self.queue_lock.acquire()
        state_arr = np.frombuffer(self.shared_mem['state'], dtype=np.uint8)
        stream_ready = state_arr[0]!=SharedMemState.Failed
        self.queue_lock.release()
        return stream_ready

    def set_barcode_viz(self, val):
        if self.output_barcode_viz:
            self.barcode_viz_lock.acquire()
            op = np.frombuffer(self.shared_mem['expose_barcode_viz'], dtype=np.uint8)
            op[0] = val
            self.barcode_viz_lock.release()
            if val==False:
                # Reset crop for algo
                self.queue_lock.acquire()
                algo_crop_arr = np.frombuffer(self.shared_mem['barcode_crop_img'], dtype=np.uint8)
                algo_crop_arr[:] = np.zeros(self.barcode_crop_size[0] * self.barcode_crop_size[1], dtype=np.uint8)
                # Reset visualization image
                viz_arr = np.frombuffer(self.shared_mem['barcode_viz_img'], dtype=np.uint8)
                viz_arr[:] = np.zeros(self.barcode_viz_buff_info['size'], dtype=np.uint8)
                self.queue_lock.release()
    
    def should_update_barcode_img(self):
        if not self.output_barcode_viz:
            return False
        self.barcode_viz_lock.acquire()
        op = np.frombuffer(self.shared_mem['expose_barcode_viz'], dtype=np.uint8)
        ret = bool(op[0])
        self.barcode_viz_lock.release()
        return ret


    def reset_locks(self):
        if self.queue_lock.acquire(block=False):
            self.queue_lock.release()
        if self.op_lock.acquire(block=False):
            self.op_lock.release()
        if self.barcode_viz_lock.acquire(block=False):
            self.barcode_viz_lock.release()
        if self.wb_lock.acquire(block=False):
            self.wb_lock.release()

    # Dummy functions for videoStereoStreams compatability, to be refactored
    def isCamBExist(self):
        return True
    
    def get_max_disparity(self):
        return 10

    def getFullResFactor(self, algo_size):
        full_res_height_post_transpose = self.img_sizes_post_rotate['left']
        return float(algo_size[0]) / full_res_height_post_transpose[0]

    def get_movie_name(self):
        if hasattr(self,'_movie_name'):
            return self._movie_name
        else:
            return ''

    
if __name__=='__main__':
    parser = argparse.ArgumentParser(description='base video reader sanity')
    parser.add_argument('--target-ms', help='take image every X ms', type=float, default=40)
    parser.add_argument('--playback', help='use playback mode', action='store_true')
    parser.add_argument('--debug', help='debug logs', action='store_true')
    args = parser.parse_args()
    if args.playback:
        from cv_blocks.cameras.playback import PlaybackMultiCam
        base_rec_path = '/home/walkout05/Desktop/movies/DS2p39_2019-11-19-19_48_18'
        camera_dict = dict(left=base_rec_path +'_L.avi',
                           right=base_rec_path +'_R.avi',
                           back=base_rec_path +'_B.avi',
                        )
        cam_source = PlaybackMultiCam(camera_dict)
    else:
        camera_dict = dict(left=dict(cam_id='Basler daA1280-54uc (22594447)', pixel_format='RGB8'),
                           right=dict(cam_id='Basler daA1280-54uc (22594466)', pixel_format='RGB8'),
                           back=dict(cam_id='Basler daA1280-54uc (22594463)', pixel_format='YCbCr422_8',
                                     software_trigger=False),
                      )
        camera_dict = dict(left=dict(cam_id='Basler daA1280-54uc (22594448)', pixel_format='RGB8'),
                           right=dict(cam_id='Basler daA1280-54uc (22594469)', pixel_format='YCbCr422_8', software_trigger=False),
                      )
        camera_dict = dict(left=dict(cam_id='Basler daA1280-54uc (22594455)', pixel_format='RGB8'),
                           right=dict(cam_id='Basler daA1280-54uc (22594460)', pixel_format='YCbCr422_8',software_trigger=False),
                           )

    my_reader = BaseImageGrabber('basler', camera_dict, target_ms=args.target_ms, debug=args.debug)
    my_proc = Process(target=my_reader.run)
    my_proc.start()
    while True:
        t_grab = time.time()
        img_data, _, _ = my_reader.grab(can_skip=True)
        if img_data:
            img_l = img_data['data']['left']['img']
            img_r = img_data['data']['right']['img']
            img_viz = np.hstack((img_l, img_r))
            if 'back' in img_data['data'].keys():
                img_b = img_data['data']['back']['img']
                img_viz = np.hstack((img_viz, img_b))
            ts = img_data['ts']
            cv2.putText(img_viz, '%f' % ts, (10, 30), cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,0), 1)
            cv2.imshow('Image', img_viz)
            key = cv2.waitKey(1) & 0xFF

        loop_time = (time.time() - t_grab) * 1e3
        if loop_time > args.target_ms:
            print('Warning: grab + display took %.2fms' % loop_time)
        else:
            time.sleep((args.target_ms - loop_time) * 1e-3)
