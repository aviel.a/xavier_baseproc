import cv2

def choose_representative_image(event, min_size=0.02, max_size=0.5, min_conf=0.5):
    # Phase 1 - try to choose valid image by size criteria
    best_mv_idx = None
    best_size = 0.0
    for mv_idx, mv in enumerate(event['movement_vector']):
        crop_size = mv.coords.area()
        if mv.detector_conf > min_conf and crop_size > min_size and crop_size < max_size:
            if crop_size > best_size:
                best_mv_idx = mv_idx
                best_size = crop_size
    if best_mv_idx is None:
        # Phase 2 - take crop with best yolo score which is large enough
        best_conf = -2.
        best_mv_idx = -1
        for mv_idx, mv in enumerate(event['movement_vector']):
            if mv.detector_conf > best_conf and mv.coords.area() > min_size:
                best_mv_idx = mv_idx
                best_conf = mv.detector_conf
        best_mv_idx = max(best_mv_idx, 0)
    best_mv = event['movement_vector'][best_mv_idx]

    rep_img = best_mv.context_image.copy() if best_mv.context_image is not None else \
        best_mv.cropped_image.copy()
    rep_img = cv2.cvtColor(rep_img, cv2.COLOR_RGB2BGR)
    return rep_img