import numpy as np
import time
from misc import log_classifier_input
from cv_blocks.events.margin_events import MarginEventDetector
from config import config
import os
import json
from cv_blocks.events.mv_manager import MotionVecLogger
from cv_blocks.events.event_utils import choose_representative_image

class EventClassifier(object):
    '''
    A class responsible for classifying mv sequences
    '''
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.min_probability_one_frame_thresh = config.get('min_probability_one_frame_thresh', 0.54)
            self.high_probability_value = config.get('high_probability_value', 0.9)
            self.high_probability_multiplier = config.get('high_probability_multiplier', 2)  # TODO- maybe reduce value a bit
            self.all_events_valid = config.get('all_events_valid', False)

    def __init__(self, md_module, en_trt,loading_params=dict(), event_cls_config=dict(), cam_names=('front', 'back'),
                 model_name=config.LATEST_YAEEN_TRT_MODEL_NAME, sync=True, aligner=None, batch_size=4,
                 use_crop_descriptor=False, logger=None):
        self.logger = logger
        self.params = self.Config(event_cls_config)
        self.classification_during_proccess = False
        self.md_module = md_module
        self.cam_names = cam_names
        self.batch_size = batch_size
        self.use_crop_descriptor = use_crop_descriptor
        if not self.use_crop_descriptor and model_name.endswith("dual_dropout.onnx"):
            self.use_crop_descriptor = True
        if en_trt:
            if self.use_crop_descriptor:
                import tensorrt_lib.Trt_cls_multi_head as TrtAnn
                self.yaaen = TrtAnn.MultiHeadClassifier(model_name=model_name, sync=sync, batch_size=batch_size)
            else:
                import tensorrt_lib.TrtAnn_cpp as TrtAnn
                self.yaaen = TrtAnn.TrtAnn(model_name=model_name, sync=sync, batch_size=batch_size)
        else:
            import myANN
            self.yaaen = myANN.myANN(loading_params['model_path'], loading_params['dataGenPath'], loading_params['classMapPath'])
        # call first time to yaeen in order to avoid long pause of first inference
        self.yaaen.predictSingleImg(np.ndarray((128, 128, 3), dtype='uint8'), prnt=False)
        self.PRODUCT_NUM = len(self.yaaen.indxToProduct)
        self.ProductCount = np.zeros(self.PRODUCT_NUM)
        self.cls_weights = []
        self.initialize_event_parameters()
        self.cast_products = False
        if config.product_casting_file and os.path.exists(config.product_casting_file):
            with open(config.product_casting_file) as f:
                self.product_casting_list = dict(json.load(f))
                self.cast_products = True
        self.margin_event_detector = MarginEventDetector(self.md_module)
        self.aligner = aligner
        self.filtered_events = []
        assert self.aligner is None, 'Debug Feature Only!'
        self.filtered_events = []
    
    def is_class_supported(self, class_name):
        # Check if product is supported by classifier
        if hasattr(self.yaaen, 'confusion_matrix') and self.yaaen.confusion_matrix is not None:
            if class_name in self.yaaen.confusion_matrix and class_name in self.yaaen.confusion_matrix[class_name] and \
                self.yaaen.confusion_matrix[class_name][class_name] > 0:
                return True

        elif class_name in self.yaaen.model_config['class_names']:
            return True
        return False
    
    def is_plu_class(self, class_name):
        if 'plu_classes' in self.yaaen.model_config:
            return class_name in self.yaaen.model_config['plu_classes']
        return False

    def is_barcode_class(self, class_name):
        if 'barcode_classes' in self.yaaen.model_config:
            return class_name in self.yaaen.model_config['barcode_classes']
        return False

    def is_small_class(self, class_name):
        if 'small_classes' in self.yaaen.model_config:
            return class_name in self.yaaen.model_config['small_classes']
        return False
    
    def override_event_logic_params(self, params):
        if 'event_logic_params' in self.yaaen.model_config:
            for param_name, param_val in self.yaaen.model_config['event_logic_params'].items():
                if hasattr(params, param_name):
                    print('Setting %s params: %s' % (param_name, str(param_val)))
                    setattr(params, param_name, param_val)
    
    def get_visual_class(self, product_code):
        '''
        2 options for visual class for a given product code:
        1. product code is a visual class by its name (e.g, some bread)
        2. product code is mapped to a visual class (e.g, bakery products mapped to bakery bag)
        '''
        # Option 1
        if product_code in self.yaaen.model_config['class_names']:
            return [product_code]
        
        # Option 2
        if 'visual_class_mapping' in self.yaaen.model_config and \
            product_code in self.yaaen.model_config['visual_class_mapping']:
            # Make sure class map is a valid category
            visual_cls = self.yaaen.model_config['visual_class_mapping']['product_code']
            if not isinstance(visual_cls, (list, tuple)):
                visual_cls = [visual_cls]
            valid_visual_cls = []
            for vc in visual_cls:
                if vc in self.yaaen.model_config['class_names']:
                    valid_visual_cls.append(vc)
            return valid_visual_cls
        
        return []


    def get_class_confidence_threshold(self, class_name):
        '''
        Test if class requires specific confidence level, and if the condition holds
        '''
        if 'unique_th_per_class' in self.yaaen.model_config:
            if isinstance(self.yaaen.model_config['unique_th_per_class'], dict) and \
                class_name in self.yaaen.model_config['unique_th_per_class']:
                conf_level = self.yaaen.model_config['unique_th_per_class'][class_name]
                if isinstance(conf_level, (float, int)):
                    print('\033[93mLoading unique confidece for class %s->%.1f\033[00m' % (class_name, conf_level), flush=True)
                    return conf_level
        return None

    def initialize_event_parameters(self):
        self.event = None
        self.event_direction = ''
        self.event_frame = None
        self.movement_vector_for_yaeen = []
        self.movement_vector_for_yaeen_len = 0
        self.movement_vector_for_yaeen_idx = -1
        self.classification_prob_mat = np.zeros((1, 1))
        if self.use_crop_descriptor:
            self.sequence_descriptors = np.zeros((1, 1))
            self.jitter_prediction = []
        self.bbox_vec = self.reset_bbox_vec()
        self.movement_vector_for_saving = []
        self.classification_during_proccess = False
        self.activated_by_cart_bottom = False
        self.activated_by_md = False
        self.event_representative_image = None

    def filter_hand_predictions(self, event):
        if config.DETECTOR_V2:
            if hasattr(self.md_module, 'detector_attributes'):
                attributes = self.md_module.detector_attributes
                product_idx = attributes['crop_type'].index('product')
            else:
                product_idx = 0
            event['movement_vector'] = [mv for mv in event['movement_vector'] if mv.detector_conf == -1 or \
                                        mv.source == 'CB' or np.argmax(mv.attributes['crop_type']) == product_idx]
        return event

    def start_new_event(self, pending_event):
        pending_event = self.filter_hand_predictions(pending_event)
        valid_event = True if self.params.all_events_valid else \
            self.margin_event_detector.is_valid_before_classification(pending_event) if self.margin_event_detector \
            else True
        if valid_event:
            self.event = pending_event
            self.event_direction = pending_event['event_direction']
            self.event_frame = pending_event['event_frame']
            self.activated_by_md = pending_event['activated_by_md']
            self.movement_vector_for_yaeen = pending_event['movement_vector']
            self.movement_vector_for_yaeen_len = len(pending_event['movement_vector'])
            self.movement_vector_for_yaeen_idx = -1
            self.activated_by_cart_bottom = pending_event['activated_by_cart_bottom']
            self.event_id = pending_event['event_id']
            self.front_id = pending_event['front_id']
            self.back_id = pending_event['back_id']
            self.event_start = pending_event.get('event_start', None)
            self.review_id = pending_event.get('review_id', None)
            self.multi_cam = pending_event.get('multi_cam', None)
            self.initiating_cam = pending_event.get('initiating_cam', None)
            self.init_coords = pending_event.get('init_coords', None)
            self.both_cams_saw_event = pending_event.get('both_cams_saw_event', False)
            self.no_interaction_if_filtered = pending_event.get('no_interaction_if_filtered', False)
            self.classification_prob_mat = np.zeros(
                (self.movement_vector_for_yaeen_len, len(self.yaaen.productToIndx.keys())))
            if self.use_crop_descriptor:
                self.sequence_descriptors = np.zeros((self.movement_vector_for_yaeen_len, self.yaaen.desc_size))
                self.jitter_prediction = []
            self.bbox_vec = self.reset_bbox_vec()
            self.ProductCount = np.zeros(self.PRODUCT_NUM)
            self.cls_weights = []
            self.movement_vector_for_saving = []
            self.classification_during_proccess = True
            self.marked_during_barcode_scan = pending_event.get('marked_during_barcode_scan', False)
            self.event_representative_image = choose_representative_image(pending_event)
            self.pile_candidate = pending_event.get('pile_candidate', False)
            self.small_item_candidate = pending_event.get('small_item_candidate', False)
            self.thrown_item_candidate = pending_event.get('thrown_item_candidate', False)
            self.early_indication_idx = pending_event.get('early_indication_idx', None)
        else:
            if hasattr(self.md_module, 'algo_logger'):
                if self.logger is not None and self.logger.should_log('event_aggregator_out'):
                    self.logger.log(MotionVecLogger.pack(pending_event), 'event_aggregator_out', frame=pending_event['event_frame'])
            self.filtered_events.append(pending_event)
            print("Event [id: {}, frame:{}] is filtered by pre-classifier logic!".format(pending_event['event_id'], pending_event['event_frame']), flush=True)
            triggered_indication_idx = pending_event.get('early_indication_idx', None)
            if hasattr(self.md_module, 'event_indicator') and triggered_indication_idx is not None:
                self.md_module.event_indicator.set_processing_status(False, triggered_indication_idx, reason='event_cls_reject_event')
        return valid_event
    
    def pack_event(self):
        event_data = dict(event_direction=self.event_direction,
                          event_frame=self.event_frame,
                          event_start=self.event_start,
                          activated_by_md=self.activated_by_md,
                          activated_by_cart_bottom=self.activated_by_cart_bottom,
                          event_id=self.event_id,
                          review_id=self.review_id,
                          front_id=self.front_id,
                          back_id=self.back_id,
                          bbox_vec=self.bbox_vec,
                          classification_prob_mat=self.classification_prob_mat,
                          agg_prob_vec=self.ProductCount,
                          cls_weights=self.cls_weights,
                          movement_vector_for_saving=self.movement_vector_for_saving,
                          multi_cam=self.multi_cam,
                          initiating_cam=self.initiating_cam,
                          marked_during_barcode_scan=self.marked_during_barcode_scan,
                          init_coords=self.init_coords,
                          both_cams_saw_event=self.both_cams_saw_event,
                          event_representative_image=self.event_representative_image,
                          early_indication_idx=self.early_indication_idx,
                          pile_candidate=self.pile_candidate,
                          small_item_candidate=self.small_item_candidate,
                          thrown_item_candidate=self.thrown_item_candidate,
                          no_interaction_if_filtered=self.no_interaction_if_filtered,
                          )
        if self.use_crop_descriptor:
            event_data['sequence_descriptors'] = self.sequence_descriptors
            if len(self.jitter_prediction)>0:
                event_data['jitter_prediction'] = self.jitter_prediction

        return event_data

    def reset_bbox_vec(self):
        bbox_vec = dict()
        for n in self.cam_names:
            bbox_vec[n] = []
        return bbox_vec

    def reset(self):
        self.initialize_event_parameters()

    def classify_sequence(self, seq, verbose=2):
        '''
        Offline classification of full sequence
        '''
        valid_event = self.start_new_event(pending_event = seq)
        if valid_event:
            self.classification_during_proccess = True
            while self.classification_during_proccess:
                # analyze next batch of images & update classification_during_proccess.
                self.update_scores_with_new_batch(verbose, run_all=True)
            # Send sequence when done
            return self.pack_event()

    def determine_item_identity_in_batches(self, verbose):
        if self.classification_during_proccess:
            # print('running yaeen')
            inference_init_time = time.time()
            if verbose > 2:
                if not hasattr(self, 'time_for_print'):
                    self.time_for_print = time.time()
                print('fps:{}'.format(1. / (time.time() - self.time_for_print)))
                self.time_for_print = time.time()
            # analyze next batch of images & update classification_during_proccess.
            self.update_scores_with_new_batch(verbose)
            inference_time_ms = (time.time()-inference_init_time)*1000
            if inference_time_ms > 50 and self.md_module.stream.isRealTime:
                print('inference time longer than expected: %.1f' % inference_time_ms, flush=True)
            # Send sequence if Done
            if self.classification_during_proccess==False:
                self.event['movement_vector'] = self.movement_vector_for_saving
                if self.logger is not None and self.logger.should_log('event_aggregator_out'):
                    self.logger.log(MotionVecLogger.pack(self.event), 'event_aggregator_out', frame=self.event_frame)
                # Log event latency
                if hasattr(self.md_module, 'latency_buffer'):
                    self.md_module.latency_buffer.append('t_classifier_out', time.time(), self.event_frame)
                return self.pack_event()

        elif (self.classification_during_proccess == False) and (
                self.md_module.movement_vector_queue.qsize() != 0):  # if there is item in the queue, classify it as well
            valid_event = self.start_new_event(pending_event = self.md_module.movement_vector_queue.get())
            if valid_event and hasattr(self.md_module, 'gui'):
                avoid_gui_wait = config.GUI_DEMO_IN_OUT_HACK and self.should_avoid_duplicate_indication()
                if not avoid_gui_wait:
                    if self.early_indication_idx is None and hasattr(self.md_module, 'event_indicator'):
                        self.early_indication_idx = self.md_module.event_indicator.alloc_new_idx(early=False)  
                        self.md_module.event_indicator.set_processing_status(True, self.early_indication_idx, reason='event_cls_start_event')

    def should_avoid_duplicate_indication(self, disable_n_frames_after_event=100):
        if len(self.md_module.purchaseList) == 0:
            return False
        # gui hack - TODO - move logic to UI side
        return self.event_frame < self.md_module.purchaseList[-1]['frameNum'] + disable_n_frames_after_event


    def update_scores_with_new_batch(self,verbose, run_all=False):
        idx = 0
        while len(self.movement_vector_for_yaeen) > 0:
            n_elements = min(self.batch_size, len(self.movement_vector_for_yaeen))
            idx += n_elements
            vector_elements = [self.movement_vector_for_yaeen.pop(0) for _ in range(n_elements)]

            # Run prediction
            # ==============
            imgs = [ve.cropped_image for ve in vector_elements]
            if self.aligner is not None:
                imgs = [self.aligner().align(img) for img in imgs]
            if self.use_crop_descriptor:
                batch_probs, batch_desc, batch_jitter_prediction = self.yaaen.predictBatch(imgs, prnt=False)
            else:
                batch_probs = self.yaaen.predictBatch(imgs, prnt=False)

            # Post-process on single samples
            # ==============================
            for n_idx in range(n_elements):
                self.movement_vector_for_yaeen_idx += 1
                self.movement_vector_for_saving.append(vector_elements[n_idx])
                bbox = vector_elements[n_idx].coords
                # Overwrite bbox with extreme value for cart bottom crop
                # This will make sure it will pass the y val test
                # =======================================================
                # if vector_elements[n_idx].source=='CB':
                #     bbox = None
                self.bbox_vec[vector_elements[n_idx].cam_name].append((bbox, self.movement_vector_for_yaeen_idx,
                                                                       vector_elements[n_idx].global_frame_number,
                                                                       vector_elements[n_idx].boxes_summary))
                probabilities = batch_probs[n_idx]
                if self.use_crop_descriptor:
                    descriptor = batch_desc[n_idx]
                    jitter_prediction = batch_jitter_prediction[n_idx] if batch_jitter_prediction is not None else None
                if self.cast_products:
                    for product, casted_to_product in self.product_casting_list.items():
                        if casted_to_product.lower() != "none":
                            probabilities[self.yaaen.getIdxFromProduct(casted_to_product)] += probabilities[self.yaaen.getIdxFromProduct(product)]
                        probabilities[self.yaaen.getIdxFromProduct(product)] = 0

                if verbose > 2:
                    prouct_idx = np.argmax(probabilities)
                    print(('image {}, Product {}. prediction {}  [value={:.3f}]'.format(len(self.movement_vector_for_yaeen),
                                                                                        self.event_direction, self.yaaen.getProductFromIndex(prouct_idx), probabilities[prouct_idx])))
                self.classification_prob_mat[self.movement_vector_for_yaeen_idx] = probabilities
                if self.use_crop_descriptor:
                    self.sequence_descriptors[self.movement_vector_for_yaeen_idx] = descriptor
                    if jitter_prediction is not None:
                        self.jitter_prediction.append(jitter_prediction)
                if config.DEBUG_CLASSIFIER_INPUT:
                    log_classifier_input(vector_elements[n_idx], self.event_frame, probabilities,\
                        self.yaaen.getProductFromIndex, self.md_module.catalog.get_english_name)
                self.cls_weights.append(vector_elements[n_idx].cls_weight)
                probabilities *= vector_elements[n_idx].cls_weight
                # will keep only predictions with high enough probability
                max_idx = np.argmax(probabilities)
                if probabilities[max_idx] > self.params.min_probability_one_frame_thresh:
                    # if probability is high will multiply its value:
                    if probabilities[max_idx] > self.params.high_probability_value / vector_elements[n_idx].cls_weight:
                        probabilities[max_idx] = min(probabilities[max_idx] * self.params.high_probability_multiplier, 3)
                        if verbose > 2:
                            print("will multiply the value since its high")
                    self.ProductCount = self.ProductCount + probabilities
                elif verbose > 2:
                    print("Filtered out last prediction since probability is low...")
            if idx >= config.BATCH_SIZE_FOR_YAAEN and not run_all:
                break
        if len(self.movement_vector_for_yaeen)==0:
            self.classification_during_proccess=False
        return
