
class MvConnectivity(object):
    '''
    An object maintaining connectivity data between motion vectors of different cams
    '''
    def __init__(self, cam_names):
        self.cam_names = cam_names
        self.connect_map = dict()
    
    def add(self, cam1, id1, cam2, id2, frame):
        assert (cam1 in self.cam_names) and (cam2 in self.cam_names)
        key = self.get_key(cam1, id1, cam2, id2)
        if key in self.connect_map.keys():
            self.connect_map[key]['edge_size'] += 1
            self.connect_map[key]['last_edge_frame'] = frame
        else:
            self.connect_map[key] = dict(edge_size=1, last_edge_frame=frame)
    
    def reset(self):
        self.connect_map = dict()
    
    def remove(self, cam_name, id):
        search_pattern = '%s_%d_ID' % (cam_name, id)
        for k in list(self.connect_map.keys()):
            if search_pattern in k:
                del self.connect_map[k]

    def get_edge(self, cam1, id1, cam2, id2):
        key = self.get_key(cam1, id1, cam2, id2)
        return self.connect_map[key]
    
    def reset_edge(self, cam1, id1, cam2, id2):
        key = self.get_key(cam1, id1, cam2, id2)
        if key in self.connect_map.keys():
            self.connect_map[key] = 0

    def get_key(self, cam1, id1, cam2, id2):
        if self.cam_names.index(cam1) < self.cam_names.index(cam2):
            return '%s_%d_ID_T_%s_%d_ID' % (cam1, id1, cam2, id2)
        else:
            return '%s_%d_ID_T_%s_%d_ID' % (cam2, id2, cam1, id1)
    
    def top_k_in_other(self, this_cam, this_id, other_cam, top_k):
        if self.cam_names.index(this_cam) < self.cam_names.index(other_cam):
            search_pattern = '%s_%d_ID_T_' % (this_cam, this_id)
        else:
            search_pattern = '_T_%s_%d_ID' % (this_cam, this_id)
        candidates = []
        for k, v in self.connect_map.items():
            if search_pattern in k:
                candidates.append(dict(id=self.id_in_cam(k, other_cam), count=v['edge_size'], last_frame=v['last_edge_frame']))
        # Get top k 
        candidates.sort(reverse=True, key=lambda x: x['count'])
        return candidates[:top_k]
    
    def get_all_edges(self, this_cam, this_id):
        all_edges = []
        for other_cam in self.cam_names:
            if other_cam==this_cam:
                continue
            all_edges += self.top_k_in_other(this_cam, this_id, other_cam, 100)
        return all_edges
    
    def is_connected(self, this_cam, this_id):
        return len(self.get_all_edges(this_cam, this_id)) > 0

    
    def id_in_cam(self, key, cam_name):
        return int(key.split(cam_name)[1].split('ID')[0].replace('_', ''))

    @staticmethod
    def inverse_key(key):
        return '_T_'.join(reversed(key.split('_T_')))
    