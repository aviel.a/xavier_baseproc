import time
import numpy as np
import itertools
from misc import bcolors as bcolors
from config import config
import enum
import os
from cv_blocks.common.types import BoundingBox2D
from misc import print_formatted_message, get_top_prod_dict
from cv_blocks.events.jitter_pred_logic import is_product_unsupported
from cart_blocks.indications.indication_handler import IndicationEvent, allow_indication
from cart_blocks.user_op_handler import resolveResult

class EventType(enum.Enum):
    HandEvent = 1
    ProductConfidentEventFirstValid = 2
    ProductConfidentEventSecondValid = 3
    ClassifierNotConfidentEvent = 4
    CartBottomOnlyEvent = 5
    FilteredEvent = 6
    LowConfidenceEvent = 7
    NotConfidentEventTriggerBarcode = 8
    ProductConfidentBarcodeId = 9
    NotConfidentEventTriggerPlu = 10
    NotConfidentEventTriggerUnrecognized = 11
    ProductConfidentPluId = 12
    FilteredEventCartBottomOnly = 13
    MultiScanInProgress = 14

class SpecialPrediction(enum.Enum):
    Empty = 0
    OtherProduct = 1
    Background = 2

from cv_blocks.events.unidentified_event import UnidentifiedHandler
class EventLogic(object):
    '''
    A class responsible for deciding on events and sending them to the shopping list
    '''
    class Config(object):
        # TODO - remove un-needed
        def __init__(self, config):
            assert isinstance(config, dict)
            self.verbose = config.get('verbose', 1)
            self.event_logic_debug = config.get('event_logic_debug', False)
            self.log_top_k_products = config.get('log_top_k_products', 5)
            assert self.log_top_k_products > 2, 'log_top_k_products should be greater than 2'
            self.unknown_with_prediction_decision_ratio = config.get('unknown_with_prediction_decision_ratio', 1.5)
            self.similar_unknown_with_prediction_decision_ratio = config.get('similar_unknown_with_prediction_decision_ratio', 5.)
            self.min_norm_probability_for_movement_vector = config.get('min_norm_probability_for_movement_vector', 0.15) # effectively this is 0
            self.unknown_item_text = config.get('unknown_item_text', "UNKNOWN")
            self.unknown_with_predictions_item = config.get('unknown_with_predictions_item', "DECISION")
            self.hand_item_text = config.get('hand_item_text', 'aa_hand')
            self.low_conf_item_text = config.get('low_conf_item_text', 'low_conf_item')
            self.barcode_item_text = config.get('barcode_item_text', 'barcode_item')
            self.plu_item_text = config.get('plu_item_text', 'plu_item')
            self.personal_item_text = config.get('personal_item_text', 'personal_item')
            self.hand_multi_factor = config.get('hand_multi_factor', 4)
            self.frames_diff_for_degeneration = config.get('frames_diff_for_degeneration', 12)
            self.frames_diff_for_degeneration_cb = config.get('frames_diff_for_degeneration_cb', 40)
            self.frames_diff_for_degeneration_barcode = config.get('frames_diff_for_degeneration_barcode', 40)
            self.frame_diff_to_keep_low_conf = config.get('frame_diff_to_keep_low_conf', 100)
            self.frame_diff_to_keep_degenerated = config.get('frame_diff_to_keep_degenerated', 100)
            self.frame_diff_to_keep_cart_bottom = config.get('frame_diff_to_keep_cart_bottom', 30)
            self.frame_diff_to_delay_filtered_in = config.get('frame_diff_to_delay_filtered_in', 50)
            self.send_low_conf_as_unknown = config.get('send_low_conf_as_unknown', False)
            self.store_sent_events = config.get('store_sent_events', False)
            self.allow_duplicate_threshold = config.get('allow_duplicate_threshold', 0.5)
            self.enough_frames_single = config.get('enough_frames_single', 5.0)
            self.very_similar_conf_th_random_split = config.get('very_similar_conf_th', 0.09)
            self.very_similar_conf_th_id_split = config.get('very_similar_conf_th', 0.35)
            self.separate_conf_factor = config.get('separate_conf_factor', 4.0)
            self.barcode_trigger_decision_ratio = config.get('barcode_trigger_decision_ratio', 3.)
            self.similarity_th_for_barcode_trigger = config.get('similarity_th_for_barcode_trigger', 0.05) # TODO - increase value
            self.back_edge_x2_max = config.get('back_edge_x2_max', 0.6)
            self.back_edge_y2_min = config.get('back_edge_y2_min', 0.4)
            self.back_edge_y2_max = config.get('back_edge_y2_max', 0.7)
            self.back_edge_std_th = config.get('back_edge_std_th', 0.05)
            self.small_vertical_diff = config.get('small_vertical_diff', 0.06)
            self.big_vertical_diff = config.get('big_vertical_diff', 0.2)
            self.iou_for_vertical_diff = config.get('iou_for_vertical_diff', 0.05)
            self.filtered_events_to_interaction = config.get('filtered_events_to_interaction', True)
            self.all_events_to_interaction = config.get('all_events_to_interaction', False)
            self.convert_hand_mv_with_cb_to_filtered = config.get('convert_hand_mv_with_cb_to_filtered', False)
            self.ignore_hand_out_n_frames_after_in = config.get('ignore_hand_out_n_frames_after_in', 30)
            self.ignore_cb_n_frames_after_event = config.get('ignore_cb_n_frames_after_event', 60)
            self.ignore_marginal_out_n_frames_after_in = config.get('ignore_marginal_out_n_frames_after_in', None)
            self.ignore_marginal_out_n_frames_after_in_similar_pred = config.get('ignore_marginal_out_n_frames_after_in_similar_pred', 40)
            self.check_unsupported_items = config.get('check_unsupported_items', False)
            self.reject_marginal_in_with_out_n_frames_after = config.get('reject_marginal_in_with_out_n_frames_after', 50)
            self.top_2_bottom_diff = config.get('top_2_bottom_diff', 0.4)
            self.min_unsupported_sequence_size = config.get('min_unsupported_sequence_size', {'in':8, 'out':10})
            self.pile_min_unsupported_sequence_size = config.get('pile_min_unsupported_sequence_size', {'in':8, 'out':8})
            self.min_thrown_sequence_size = config.get('min_thrown_sequence_size', 4)
            self.long_unsupported_sequence_size = config.get('long_unsupported_sequence_size', {'single':12, 'multi':18})
            self.pile_long_unsupported_sequence_size = config.get('pile_long_unsupported_sequence_size', {'single':10, 'multi':14})
            self.test_all_filtered_events = config.get('test_all_filtered_events', True)
            self.force_similar_prod_top_k = config.get('force_similar_prod_top_k', True)
            self.unsupported_score_thresholds = config.get('unsupported_score_thresholds', {'low':0.55, 'high':0.7})
            self.ignore_cb_only_events = config.get('ignore_cb_only_events', False)
            self.filter_only_confident_hands = config.get('filter_only_confident_hands', False)
            self.high_hand_th = config.get('high_hand_th', 0.65)
            self.low_hand_th = config.get('low_hand_th', 0.25)
            self.non_hand_len_th = config.get('non_hand_len_th', 10)
            self.removal_marked_interval = config.get('removal_marked_interval', 15)
            self.min_score_for_other_prediction = config.get('min_score_for_other_prediction', 2.0)
            self.min_norm_score_for_other_prediction = config.get('min_norm_score_for_other_prediction', 0.2)
            self.min_multiplier_for_other_prediction = config.get('min_multiplier_for_other_prediction', 1.5)

    def __init__(self, md_module, idx2product, product2idx, cam_names, logger=None, el_config=dict()):
        self.params = self.Config(el_config)
        self.md_module = md_module
        # Override module params with specific classifier settings
        if hasattr(self.md_module, 'event_classifier'):
            self.md_module.event_classifier.override_event_logic_params(self.params)
        self.idx2product = idx2product
        self.product2idx = product2idx
        self.cam_names = cam_names
        self.pending_low_confidence_events = []
        self.pending_cart_bottom_events = []
        self.degenerated_events = []
        self.pending_hand_events = []
        self.removal_marked = []
        self.special_item_handler = UnidentifiedHandler(md_module, msco=config.MSCO_MODE)
        self.logger = logger
        if logger is not None:
            from cv_blocks.misc.logger import MarkedEventLogger
            self.mel = MarkedEventLogger
        if self.params.store_sent_events:
            self.sent_events = []
            self.low_conf_events = []
    
    def add_classified_seq(self, cls_seq):
        # Apply logic on sequence
        seq_result = self.handle_sequence(cls_seq)
        # Log event info to capsule
        if self.logger is not None and self.logger.should_log('user_marked_event'):
            (event_type, top_products, unsupported_candidate, special_prediction) = seq_result
            event_info_log = dict(prediction=top_products,
                                  unsupported_candidate=unsupported_candidate,
                                  predicted_other=special_prediction.name,
                                  event_type=event_type.name,
                                  )
            self.logger.log(self.mel.pack(cls_seq['event_id'], 'EventCls', metadata=event_info_log), \
                            'user_marked_event', frame=cls_seq['event_frame'])

        # Check if special item expected
        special_item_expected = self.special_item_handler.expected_event(cls_seq, seq_result)

        # Save to dataset if needed
        if hasattr(self.md_module, 'dataset_saver'):
            self.md_module.dataset_saver.save_event_data(cls_seq)

        # 2 events in the same sequence
        if isinstance(seq_result, list):
            for sequence in seq_result:
                self.send_event(cls_seq, sequence, special_item_expected=special_item_expected)
                if self.params.store_sent_events:
                    self.sent_events.append(dict(event=cls_seq, prediction=sequence))
            return True

        # Ignore duplicate events
        if seq_result[0] in (EventType.ProductConfidentEventFirstValid, EventType.ProductConfidentEventSecondValid, EventType.CartBottomOnlyEvent,EventType.ClassifierNotConfidentEvent):
            if self.need_degeneration(cls_seq, seq_result) and cls_seq['review_id'] is None:
                print("Event [id: {}, frame:{}] is degenerated!".format(cls_seq['event_id'], cls_seq['event_frame']), flush=True)
                self.degenerated_events.append(dict(seq=cls_seq, result=seq_result))
                return False

        # Filtered events and hand events
        if seq_result[0] in (EventType.HandEvent, EventType.FilteredEvent):
            if seq_result[3] == SpecialPrediction.Background and not cls_seq['thrown_item_candidate']:
                print("Event [id: {}, frame:{}] was filtered and background is top1 - not sending as interaction".format(
                    cls_seq['event_id'], cls_seq['event_frame']), flush=True)
                return False
            if seq_result[0]==EventType.FilteredEvent and self.need_degeneration(cls_seq, seq_result):
                print("Event [id: {}, frame:{}] is degenerated!".format(cls_seq['event_id'], cls_seq['event_frame']), flush=True)
                self.degenerated_events.append(dict(seq=cls_seq, result=seq_result))
                return False
            elif seq_result[0]==EventType.FilteredEvent and 'no_interaction_if_filtered' in cls_seq.keys() and cls_seq['no_interaction_if_filtered']:
                print("Event [id: {}, frame:{}] is filtered and came after hand event - not allowing interaction!".format(cls_seq['event_id'], cls_seq['event_frame']), flush=True)
                self.pending_low_confidence_events.append(dict(seq=cls_seq, result=seq_result))
                if self.params.store_sent_events:
                    self.low_conf_events.append(dict(event=cls_seq, prediction=seq_result))
                return False
            elif seq_result[0] == EventType.FilteredEvent and self.filtered_out_after_in(cls_seq):
                print("Event [id: {}, frame:{}] is filtered and came after insertion - not allowing interaction!".format(cls_seq['event_id'], cls_seq['event_frame']), flush=True)
                self.pending_low_confidence_events.append(dict(seq=cls_seq, result=seq_result))
                if self.params.store_sent_events:
                    self.low_conf_events.append(dict(event=cls_seq, prediction=seq_result))
                return False
            else:
                unsupported_candidate = seq_result[2]
                has_barcode = hasattr(self.md_module, 'barcode_handler')
                has_plu = hasattr(self.md_module, 'plu_handler')
                # if not expecting a special event(barcode, plu) and hand is top-1, delay it a little
                if special_item_expected is None and self.params.filtered_events_to_interaction and \
                    (has_barcode or has_plu or self.params.test_all_filtered_events) and \
                    seq_result[0]==EventType.FilteredEvent and cls_seq['event_direction']=='in':
                    if seq_result[1][0][0]==self.params.hand_item_text or cls_seq['small_item_candidate']:
                        # The case where top-1 is hand at 'in' event, but event_type is Filtered event should be inspected
                        # Or filtered event at 'in' event, and small item is indicated
                        # Delay sending event for a little, to see if 'out' event occured
                        self.pending_hand_events.append(dict(event=cls_seq, prediction=seq_result))
                    else:
                        # Send event as potential trigger for barcode/plu triggers
                        # self.send_event(cls_seq, seq_result, check_filtered=True)
                        self.pending_low_confidence_events.append(dict(seq=cls_seq, result=seq_result))
                # if filtered event is marked as unsupported - send it anyway
                elif unsupported_candidate and seq_result[0]==EventType.FilteredEvent:
                    self.send_event(cls_seq, seq_result, check_filtered=True, special_item_expected=special_item_expected)
                    return True
                # if barcode mode is expecting an event with the same direction, send the event anyway
                elif has_barcode and special_item_expected=='barcode' and \
                    self.md_module.barcode_handler.is_seq_scanned(cls_seq, seq_result):
                    force_filtered = seq_result[0]==EventType.FilteredEvent
                    self.send_event(cls_seq, seq_result, force_filtered=force_filtered, special_item_expected=special_item_expected)
                    return True
                # Send "IN" filtered events to barcode/plu verification
                elif self.params.filtered_events_to_interaction and (has_barcode or has_plu or self.params.test_all_filtered_events) and \
                    seq_result[0]==EventType.FilteredEvent and cls_seq['event_direction']=='in':
                    if seq_result[1][0][0]==self.params.hand_item_text:
                        # The case where top-1 is hand at 'in' event, but event_type is Filtered event should be inspected
                        # Delay sending event for a little, to see if 'out' event occured
                        self.pending_hand_events.append(dict(event=cls_seq, prediction=seq_result))
                    else:
                        # Send event as potential trigger for msco event=='barcode'
                        self.send_event(cls_seq, seq_result, check_filtered=True, special_item_expected=special_item_expected)
                    return True
                # Send "OUT" filtered events to barcode/plu (unless they are matched)
                elif self.params.filtered_events_to_interaction and (has_barcode or has_plu or self.params.test_all_filtered_events) and \
                    seq_result[0]==EventType.FilteredEvent and cls_seq['event_direction']=='out':
                    # TODO - if hand is top-1, check if a recent insert occured to reject event
                    self.send_event(cls_seq, seq_result, check_filtered=True, special_item_expected=special_item_expected)
                else:
                    self.pending_low_confidence_events.append(dict(seq=cls_seq, result=seq_result))
                    if self.params.store_sent_events:
                        self.low_conf_events.append(dict(event=cls_seq, prediction=seq_result))
                    return False
        # Cart Bottom only events
        elif seq_result[0] in (EventType.CartBottomOnlyEvent, EventType.FilteredEventCartBottomOnly):
            self.handle_cart_bottom_event(cls_seq, seq_result)
        
        # Movement vector Confident Events
        elif seq_result[0] in (EventType.ProductConfidentEventFirstValid, EventType.ProductConfidentEventSecondValid, EventType.ClassifierNotConfidentEvent):
            # Send Event
            self.send_event(cls_seq, seq_result, special_item_expected=special_item_expected)
            if self.params.store_sent_events:
                self.sent_events.append(dict(event=cls_seq, prediction=seq_result))
        
        return True
    
    def add_removal_marker(self, product_code):
        self.removal_marked.append({'markTime': time.time(), 'markProduct': product_code})
        ind_cmd = allow_indication(IndicationEvent.MarkRemoval)
        if ind_cmd is not None:
            if hasattr(self.md_module.cart_content_handler, 'indication_handler'):
                self.md_module.cart_content_handler.indication_handler.generic_indication(ind_cmd)

    def check_marked_removal(self, seq_result):
        now = time.time()
        if self.params.event_logic_debug:
            print("DEBUG event_logic checking if removal_marked time slot applies to any of the lines in removal mark list. list is:\n"+ str(self.removal_marked), flush=True)
        for mark in list(self.removal_marked):
            if self.params.event_logic_debug:
                print("DEBUG event_logic checking if time slot is valid for product with code: "+ mark['markProduct']+" time diff (in seconds) is - "+ str(now - mark['markTime']), flush=True)
            if now - mark['markTime'] < self.params.removal_marked_interval:
                #### TODO: next four lines are for future functionality in vision mode- comparing swiped product with identified removal
                event_type, top_products, unsupported_candidate, special_prediction = seq_result
                newProduct = [mark['markProduct'], 100]
                top_products.insert(0,newProduct)
                seq_result = (EventType.ProductConfidentEventFirstValid, top_products, unsupported_candidate, special_prediction)
                if self.params.event_logic_debug:
                    print("DEBUG event_logic time slot is valid for product with code: ", mark['markProduct'], flush=True)
                self.removal_marked.remove(mark)
                return True, seq_result
            else:
                if self.params.event_logic_debug:
                    print("DEBUG event_logic time slot is NOT valid -removing product from list, the code is: ", mark['markProduct'], flush=True)
                self.removal_marked.remove(mark)
        if self.params.event_logic_debug:
            print("DEBUG event_logic no product in removal list is valid, sending to UI", flush=True)        
        return False, seq_result
        
    def step(self, frame_num, cls_seq):
        event_sent = True
        cls_seq_cb = False
        if cls_seq is not None:
            # Send event or save to low confidence
            event_sent = self.add_classified_seq(cls_seq)

            # Remove events from pending low confidence events if needed
            self.remove_old_low_conf_events(frame_num)

        # Handle delayed cart bottom events
        if len(self.pending_cart_bottom_events) > 0:
            cls_seq_cb = True
            cb_event_ignored = self.handle_delayed_cart_bottom_events(frame_num)

        # Handle delayed filtered 'in' events
        if len(self.pending_low_confidence_events) > 0:
            self.handle_delayed_filtered_in_events(frame_num)

        # Handle MSCO marginal low conf event
        if len(self.pending_hand_events) > 0:
            self.handle_delayed_pending_hand_events(frame_num)

        # when classification is done, regardless of it result, UI should be indicated
        if hasattr(self.md_module, 'event_indicator') and (cls_seq is not None or cls_seq_cb):
            if cls_seq is not None:    
                self.md_module.event_indicator.set_processing_status(False,cls_seq['early_indication_idx'], reason='event_logic_finish_event')
    
    def clean_all_pending_events(self):
        frame_num = self.md_module.global_frame_num
        while len(self.pending_hand_events) > 0 or len(self.pending_cart_bottom_events) > 0:
            # Handle delayed cart bottom events
            if len(self.pending_cart_bottom_events) > 0:
                cb_event_ignored = self.handle_delayed_cart_bottom_events(frame_num)
            # Handle MSCO marginal low conf event
            if len(self.pending_hand_events) > 0:
                self.handle_delayed_pending_hand_events(frame_num)
            frame_num+=1

    
    def reset(self):
        self.pending_low_confidence_events = []
        self.pending_cart_bottom_events = []
        self.degenerated_events = []
        self.pending_hand_events = []
        self.removal_marked = []
        self.special_item_handler.reset()
        if self.params.store_sent_events:
            self.sent_events = []
            self.low_conf_events = []
    
    def remove_old_low_conf_events(self, frame_num):
        self.pending_low_confidence_events = [lce for lce in self.pending_low_confidence_events \
            if frame_num - lce['seq']['event_frame'] < self.params.frame_diff_to_keep_low_conf]
        self.degenerated_events = [lce for lce in self.degenerated_events \
            if frame_num - lce['seq']['event_frame'] < self.params.frame_diff_to_keep_degenerated]

    
    def handle_cart_bottom_event(self, cls_seq, seq_result):
        '''
        Logic handles independent cart bottom event.
        1. if has an mv that was filtered - send it as low confidence event
        2. else if it is attached to a sent mv event - do nothing
        3. else (independent cart bottom event), add to pending cart bottom event,
           send with delay as low confidence event if no other mv events occured
        '''
        event_type, top_products, unsupported_candidate, special_prediction = seq_result
        has_attached_mv_event = cls_seq['review_id'] is not None
        if has_attached_mv_event:
            sent_event = [se for se in self.md_module.purchaseList if se['id']==cls_seq['review_id']]
            filtered_event = [se for se in self.pending_low_confidence_events if se['seq']['event_id']==cls_seq['review_id']]
            degenerated_event = [se for se in self.degenerated_events if se['seq']['event_id']==cls_seq['review_id']]
            filtered_pre_classifier = [se for se in self.md_module.event_classifier.filtered_events if
                                       se['event_id'] == cls_seq['review_id']]
            if len(sent_event)==1 or len(filtered_event)==1 or len(degenerated_event)==1 or \
                    len(filtered_pre_classifier) == 1:
                cart_bottom_product = top_products[0][0]
                if len(sent_event)==1:
                    mv_product = sent_event[0]['name']
                    op_str = 'Updating CB location' if sent_event[0]['direction']=='in' else 'Ignoring'
                    if cart_bottom_product==mv_product:
                        print("\033[93mCart Bottom Review for event_id {}: high confidence event! Both predictions are {}. {}\033[00m".format(cls_seq['review_id'], cart_bottom_product, op_str), flush=True)
                    else:
                        print("\033[93mCart Bottom Review for event_id {}: low confidence event! Review prediction is {} vs the original prediction {}. {}\033[00m".format(cls_seq['review_id'], cart_bottom_product, mv_product, op_str), flush=True)
                    if sent_event[0]['direction']=='in' and hasattr(self.md_module.cart_content_handler, 'location_matching'):
                        # Update cart bottom locations
                        self.md_module.cart_content_handler.location_matching.update_cb_location( \
                            event_id=cls_seq['review_id'], bbox_vec=cls_seq['bbox_vec'])
                elif len(filtered_event)==1:
                    print("\033[91mCart Bottom Review for event_id {}: event was originally filtered, sending it as low confidence event\033[00m".format(cls_seq['review_id']), flush=True)
                    self.add_low_confidence_event(filtered_event[0]['seq'], filtered_event[0]['result'])
                    # Remove from low confidence events
                    self.pending_low_confidence_events = [p for p in self.pending_low_confidence_events if p['seq']['event_id']!=cls_seq['review_id']]
            else:
                print("\033[91mCart Bottom Review for event_id {}: event not found! aborting review process\033[00m".format(
                        cls_seq['review_id']), flush=True)
        else:
            # Independent cart bottom event
            # TODO - send as confident/unknown event if classifier is very confident
            self.pending_cart_bottom_events.append(dict(seq=cls_seq, result=seq_result))

    def cancel_single_cam_cb_event(self, event):
        if len(self.md_module.purchaseList) > 0 and abs(self.md_module.purchaseList[-1]['frameNum'] - event['seq']['event_frame']) < self.params.ignore_cb_n_frames_after_event and \
                self.md_module.purchaseList[-1]['name'] != self.params.hand_item_text:
            if event['seq']['event_direction'] == 'in' and self.md_module.purchaseList[-1]['direction'] == 'in' \
                 and hasattr(self.md_module.cart_content_handler, 'location_matching'):
                self.md_module.cart_content_handler.location_matching.update_cb_location(
                    event_id=self.md_module.purchaseList[-1]['id'], bbox_vec=event['seq']['bbox_vec'])
            return True
        return False
    
    def valid_marginal_removal_event(self, cls_seq):
        if len(self.md_module.purchaseList) > 0 and self.params.ignore_marginal_out_n_frames_after_in is not None:
            last_event_was_in = self.md_module.purchaseList[-1]['direction'] == 'in' and \
                self.md_module.purchaseList[-1]['name'] != self.params.hand_item_text
            last_event_was_near = cls_seq['event_frame'] - self.md_module.purchaseList[-1]['frameNum'] < self.params.ignore_marginal_out_n_frames_after_in
            if last_event_was_in and last_event_was_near:
                print("\033[91mIgnoring marginal event_id {}:Removal event too soon after Insert \033[00m".format(cls_seq['event_id']), flush=True)
                return False
        return True

    def handle_delayed_filtered_in_events(self, frame_num):
        indices_to_remove = []
        for idx, filtered_event in enumerate(self.pending_low_confidence_events):
            if frame_num - filtered_event['seq']['event_frame'] >= self.params.frame_diff_to_delay_filtered_in and \
                    filtered_event['result'][0]==EventType.FilteredEvent and \
                    filtered_event['seq']['event_direction'] == 'in':
                came_before_prod_removal = False
                indices_to_remove.append(idx)
                for purchased_item in reversed(self.md_module.purchaseList):
                    if abs(frame_num - purchased_item['frameNum']) > 300:
                        break
                    if purchased_item['direction'] == 'out' and purchased_item['frameNum'] > filtered_event['seq']['event_frame']:
                        # remove 'in' filtered event that came before product removal
                        print(bcolors.BOLD + "Filtered insertion event [id: {}, frame:{}] is not sent as interaction because it came before removal at frame {}!".format(
                            filtered_event['seq']['event_id'], filtered_event['seq']['event_frame'],
                            self.md_module.purchaseList[-1]['frameNum']) + bcolors.ENDC, flush=True)
                        came_before_prod_removal = True
                        break
                if not came_before_prod_removal:
                    print(bcolors.BOLD + "Casting delayed filtered event [id: {}, frame:{}] to interaction".format(
                        filtered_event['seq']['event_id'], filtered_event['seq']['event_frame']) + bcolors.ENDC, flush=True)
                    self.send_event(filtered_event['seq'], filtered_event['result'], check_filtered=True)
                    break
        self.pending_low_confidence_events = [ev for i, ev in enumerate(self.pending_low_confidence_events) if i not in indices_to_remove]
        return

    def handle_delayed_cart_bottom_events(self, frame_num):
        # Handle single event in a step, the oldest event
        pending_event = self.pending_cart_bottom_events[0]
        # Check if enough frames have passed
        if frame_num - pending_event['seq']['event_frame'] < self.params.frame_diff_to_keep_cart_bottom:
            return False
        event_ignored = False
        # cancel events from single cam if another event was detected around the same frames
        if len(pending_event['seq']['movement_vector_for_saving']) == 1 and self.cancel_single_cam_cb_event(pending_event):
            event_ignored = True
            print("Event [id: {}, frame:{}] is canceled due to another event at {}!".format(
                pending_event['seq']['event_id'], pending_event['seq']['event_frame'], self.md_module.purchaseList[-1]['frameNum']), flush=True)
        else:
            # Check if while event was delayed, an mv event with same prediction was insterted
            # This is used mostly for out events where snapshot can be taken before event ended
            if not self.need_degeneration(pending_event['seq'], pending_event['result']):
                self.add_low_confidence_event(pending_event['seq'], pending_event['result'])
            else:
                event_ignored = True
                print("Event [id: {}, frame:{}] is degenerated!".format(pending_event['seq']['event_id'], pending_event['seq']['event_frame']),flush=True)
        # Remove element from pending cart bottom events
        self.pending_cart_bottom_events.pop(0)
        return event_ignored
    
    def handle_delayed_pending_hand_events(self, frame_num):
        # Handle single event in a step, the oldest event
        pending_event = self.pending_hand_events[0]
        event_info = pending_event['event']
        event_result = pending_event['prediction']
        if event_info['event_direction']=='in' and len(self.md_module.purchaseList) > 0:
            # check if an 'out' event was sent shortly after hand insert event
            last_sent_event = self.md_module.purchaseList[-1]
            if last_sent_event['frameNum'] > event_info['event_frame'] and \
                last_sent_event['frameNum'] - event_info['event_frame'] < self.params.reject_marginal_in_with_out_n_frames_after:
                print("Event [id: {}, frame:{}, dir=in] is ignored due to removal event at {}!".format(
                    event_info['event_id'], event_info['event_frame'], last_sent_event['frameNum']), flush=True)
                # Remove event
                self.pending_hand_events.pop(0)
                return
        
        if frame_num - event_info['event_frame'] > self.params.reject_marginal_in_with_out_n_frames_after:
            # Send event after sufficient delay
            special_item_expected = self.special_item_handler.expected_event(event_info, event_result)
            print("Sending delayed MSCO hand Event [id: {}, frame:{}, dir=in]".format(
                event_info['event_id'], event_info['event_frame']), flush=True)
            self.send_event(event_info, event_result, check_filtered=True, special_item_expected=special_item_expected)
            # Remove event
            self.pending_hand_events.pop(0)

    def handle_sequence(self, cls_seq):
        if self.idx2product(len(cls_seq['agg_prob_vec']) - 1) == "unknown":
            cls_seq['agg_prob_vec'] = cls_seq['agg_prob_vec'][:-1]
        maxIndices = np.argsort(cls_seq['agg_prob_vec'])[::-1][:self.params.log_top_k_products]
        top_products = [(self.idx2product(i), cls_seq['agg_prob_vec'][i]) for i in maxIndices]

        if self.params.verbose>1 or hasattr(self.md_module, 'gui'):
            str ='TOP products(event_id=%d): ' % cls_seq['event_id']
            for product in top_products:
                prod_name = self.md_module.catalog.get_english_name(product[0])
                str+='{}({}):{:.1f}, '.format(product[0], prod_name, product[1])
            print(str[:-2], flush=True)
        winningIndex = maxIndices[0] # the index of the product that is the most probable
        secondIndex =  maxIndices[1] # the index of the product that is the second most probable
        n_images = cls_seq['classification_prob_mat'].shape[0]
        winning_norm_probability = cls_seq['agg_prob_vec'][winningIndex]/n_images #normalize probability to size of vector(not exactly average)
        cart_bottom_only_event = cls_seq['activated_by_cart_bottom'] and not cls_seq['activated_by_md']

        # Check for unsupported product
        # =============================
        unsupported_candidate = False
        if self.params.check_unsupported_items and 'jitter_prediction' in cls_seq and not cart_bottom_only_event:
            unsupported_candidate = is_product_unsupported(maxIndices, cls_seq, top_products, self.md_module.event_classifier.yaaen,
                                                           unsupported_score_thresholds=self.params.unsupported_score_thresholds)
        if "background" in self.md_module.event_classifier.yaaen.productToIndx.keys():
            background_index = self.product2idx("background")
            if winningIndex == background_index:
                return EventType.FilteredEvent, top_products, False, SpecialPrediction.Background
            else:
                cls_seq['agg_prob_vec'][background_index] = 0
                maxIndices = np.argsort(cls_seq['agg_prob_vec'])[::-1][:self.params.log_top_k_products]
                top_products = [(self.idx2product(i), cls_seq['agg_prob_vec'][i]) for i in maxIndices]
                winningIndex = maxIndices[0]
                winning_norm_probability = cls_seq['agg_prob_vec'][winningIndex] / n_images
                secondIndex = maxIndices[1]

        if "other" in self.md_module.event_classifier.yaaen.productToIndx.keys():
            other_index = self.product2idx("other")
            if winningIndex == other_index and cls_seq['agg_prob_vec'][other_index] > self.params.min_score_for_other_prediction and \
                    winning_norm_probability > self.params.min_norm_score_for_other_prediction and \
                    cls_seq['agg_prob_vec'][winningIndex] > self.params.min_multiplier_for_other_prediction * cls_seq['agg_prob_vec'][secondIndex]:# or winningIndex == 0 and secondIndex == other_index:
                unsupported_candidate = True
                top_products = [(tp_name, tp_conf) for (tp_name, tp_conf) in top_products if tp_name!='other']
                return EventType.CartBottomOnlyEvent, top_products, unsupported_candidate, SpecialPrediction.OtherProduct
            else:
                cls_seq['agg_prob_vec'][other_index] = 0.0
                maxIndices = np.argsort(cls_seq['agg_prob_vec'])[::-1][:self.params.log_top_k_products]
                top_products = [(self.idx2product(i), cls_seq['agg_prob_vec'][i]) for i in maxIndices]
                winningIndex = maxIndices[0]
                winning_norm_probability = cls_seq['agg_prob_vec'][winningIndex] / n_images
                secondIndex = maxIndices[1]

        # Cart Bottom Event
        # =================
        if cart_bottom_only_event:
            if winning_norm_probability > self.params.min_norm_probability_for_movement_vector:
                return EventType.CartBottomOnlyEvent, top_products, unsupported_candidate, SpecialPrediction.Empty
            else:
                print("Cart bottom event is filtered due to low probabilities", flush=True)
                return EventType.FilteredEventCartBottomOnly, top_products, unsupported_candidate, SpecialPrediction.Empty
        if winning_norm_probability > self.params.min_norm_probability_for_movement_vector:  # probabilty is good enough, product is known
            # Predict Hand
            # ============
            if self.isWinningIdxisHand(winningIndex) and \
                (self.handIsMuchMoreProbable(cls_seq, second_item_indx=secondIndex) or \
                not self.product_classified_in_enough_frames(cls_seq, secondIndex) or \
                not self.product_seen_in_different_locations(cls_seq, secondIndex)):
                # item is a hand with high confidence
                if not self.product_classified_in_enough_frames(cls_seq, secondIndex):
                    message = "No product is seen for enough frames going {}, that prediction was filtered".format(
                        cls_seq['event_direction'])
                elif not self.product_seen_in_different_locations(cls_seq, secondIndex):
                    message = "Product hasn't move enough in y when going {}, that prediction was filtered".format(
                        cls_seq['event_direction'])
                else:
                    message = "System saw a hand going {}, that prediction was filtered [hand={:.1f} ; {}={:.1f}]".format(
                        cls_seq['event_direction'], cls_seq['agg_prob_vec'][winningIndex], self.idx2product(maxIndices[1]),
                        cls_seq['agg_prob_vec'][maxIndices[1]])
                if self.params.verbose > 1:
                    print(bcolors.BOLD + message + bcolors.ENDC, flush=True)
                elif self.params.verbose > 0:
                    print(message, flush=True)
                
                # Ignore removal hand event right after insert
                if self.params.ignore_hand_out_n_frames_after_in is not None:
                    if cls_seq['event_direction']=='out' and len(self.md_module.purchaseList) > 0:
                        last_registered_event = self.md_module.purchaseList[-1]
                        last_event_is_in = last_registered_event['direction']=='in'
                        valid_frame_diff = cls_seq['event_frame'] - last_registered_event['frameNum'] < self.params.ignore_hand_out_n_frames_after_in
                        if last_event_is_in and valid_frame_diff:
                            print('Ignore Removal event after insert event where Hand is top-1 prediction', flush=True)
                            return EventType.HandEvent, top_products, unsupported_candidate, SpecialPrediction.Empty
                # Convert hand to filtered event
                very_confident_hand = self.handIsMuchMoreProbable(cls_seq, second_item_indx=secondIndex)
                map_hand_to_filtered = not very_confident_hand
                if self.params.convert_hand_mv_with_cb_to_filtered and cls_seq['activated_by_cart_bottom']:
                    map_hand_to_filtered = True
                if map_hand_to_filtered:
                    return EventType.FilteredEvent, top_products, unsupported_candidate, SpecialPrediction.Empty

                # # save for dataset
                # takeHand = np.random.rand() < config.SAVE_FOR_DATASET_EN_HANDS_RATIO
                # if config.SAVE_FOR_DATASET and not config.SAVE_FOR_DATASET_SKIP_HANDS and takeHand:
                #     print('hand taken for dataset' if takeHand else 'hand not taken')
                #     self.saveForDataset(cls_seq, video_capture)
                return EventType.HandEvent, top_products, unsupported_candidate, SpecialPrediction.Empty
            else: # winning is not hand
                # remove hand from products
                top_products = [list(p) for p in top_products if p[0] != self.params.hand_item_text]
                winningIndex = self.product2idx(top_products[0][0])
                secondIndex = self.product2idx(top_products[1][0])

                unknown_candidate = self.is_item_unknown_with_predictions(top_products)
                first_is_valid = self.product_classified_in_enough_frames(cls_seq, winningIndex) and self.product_seen_in_different_locations(cls_seq, winningIndex, top_k=2)
                second_is_valid = self.product_classified_in_enough_frames(cls_seq, secondIndex) and self.product_seen_in_different_locations(cls_seq, secondIndex, top_k=2)
                # force unknown if the top guess has very similar products
                if config.CAST_VERY_SIMILAR_TO_UNKNOWN and (first_is_valid or second_is_valid):
                    valid_prod_idx = 0 if first_is_valid else 1
                    similar_product = self.special_item_handler.very_similar_products(top_products[valid_prod_idx][0])
                    top_prod_ratio = top_products[0][1] / (top_products[1][1] + 1e-6)
                    if similar_product is not None and top_prod_ratio < self.params.similar_unknown_with_prediction_decision_ratio:
                        print("Very similar products: {}, {} - guessing unknown!".format(top_products[valid_prod_idx][0], similar_product), flush=True)
                        all_prod_names = [p[0] for p in top_products]
                        if similar_product in all_prod_names:
                            similar_idx = all_prod_names.index(similar_product)
                            # Similar product is also the other predicted product - equalize scores to ensure unknown
                            if self.params.force_similar_prod_top_k:
                                top_products[similar_idx][1] = top_products[valid_prod_idx][1]
                        else:
                            if self.params.force_similar_prod_top_k:
                                # Similar product is not predicted -> add similar with same prob.
                                top_products.insert(1+valid_prod_idx, [similar_product, top_products[valid_prod_idx][1]])
                                top_products.pop()
                        return EventType.ClassifierNotConfidentEvent, top_products, unsupported_candidate, SpecialPrediction.Empty
                # split event if back and front predict different non similar products with high confidence
                if config.SPLIT_EVENTS and first_is_valid and second_is_valid:
                    top_products_back = self.get_top_products_single_cam(cls_seq=cls_seq, cam='back')
                    top_products_front = self.get_top_products_single_cam(cls_seq=cls_seq, cam='front')
                    top_products_back = [p for p in top_products_back if p[0] != self.params.hand_item_text]
                    top_products_front = [p for p in top_products_front if p[0] != self.params.hand_item_text]
                    separated = self.separate_event_if_needed(top_products_back, top_products_front, cls_seq)
                    if separated:
                        return [(EventType.ProductConfidentEventFirstValid, top_products_back, unsupported_candidate, SpecialPrediction.Empty),
                                (EventType.ProductConfidentEventFirstValid, top_products_front, unsupported_candidate, SpecialPrediction.Empty)]
                cb_saw_first = self.product_seen_in_cb(cls_seq, winningIndex)
                cb_saw_second = self.product_seen_in_cb(cls_seq, secondIndex)

                # No consistent Classifications in enough vertical range
                if unknown_candidate:
                    if (first_is_valid or cb_saw_first) and (second_is_valid or cb_saw_second): # first two indexes are valid - choose unknown
                        # price, productNameToList = self.handle_unknown_item_with_predictions(top_products)
                        return EventType.ClassifierNotConfidentEvent, top_products, unsupported_candidate, SpecialPrediction.Empty
                    elif first_is_valid:
                        return EventType.ProductConfidentEventFirstValid, top_products, unsupported_candidate, SpecialPrediction.Empty
                    elif second_is_valid:
                        return EventType.ProductConfidentEventSecondValid, top_products, unsupported_candidate, SpecialPrediction.Empty
                    else:
                        message1 = "hasn't move enough in y" if not self.product_seen_in_different_locations(cls_seq, winningIndex, top_k=2) else "hasn't been seen for enough frames"
                        message2 = "hasn't move enough in y" if not self.product_seen_in_different_locations(cls_seq, secondIndex, top_k=2) else "hasn't been seen for enough frames"
                        message = "unknown candidate (event direction = {}), but both options are not valid!\nOption #1 {} and option #2 {}-> pending low confidence event".format(cls_seq['event_direction'], message1, message2)
                        print(bcolors.BOLD + message + bcolors.ENDC, flush=True)
                        return EventType.FilteredEvent, top_products, unsupported_candidate, SpecialPrediction.Empty
                else: # classifier confident enough - most probable product will decide if event occured
                    if not first_is_valid:  # event did not occur
                        if not self.product_seen_in_different_locations(cls_seq, winningIndex, top_k=2): # classifier wasn't confident enough about product in both edges of trajectory
                            message = "Product hasn't move enough in y when going {}, -> pending low confidence event".format(
                                cls_seq['event_direction'])
                        else: # most probable product wasn't top selection in enough frames
                            message = "Product hasn't been seen for enough frames going {}, -> pending low confidence event".format(
                                cls_seq['event_direction'])
                        print(bcolors.BOLD + message + bcolors.ENDC, flush=True)
                        return EventType.FilteredEvent, top_products, unsupported_candidate, SpecialPrediction.Empty

        else:  # probability for product is too low, mark as low confidence event
            print(bcolors.BOLD + "Top probability is too low, filtering event" + bcolors.ENDC, flush=True)
            return EventType.FilteredEvent, top_products, unsupported_candidate, SpecialPrediction.Empty

        return EventType.ProductConfidentEventFirstValid, top_products, unsupported_candidate, SpecialPrediction.Empty

    def get_top_products_single_cam(self, cls_seq, cam='back'):
        agg_prob_vec_single_cam = np.zeros_like(cls_seq['agg_prob_vec'])
        for bbox in cls_seq['bbox_vec'][cam]:
            if bbox[0] is None:  # skip CB images
                continue
            element_idx = bbox[1]
            probabilities = cls_seq['classification_prob_mat'][element_idx]
            cls_weight = cls_seq['cls_weights'][element_idx]
            probabilities *= cls_weight
            max_idx = np.argmax(probabilities)
            if probabilities[max_idx] > self.md_module.event_classifier.params.min_probability_one_frame_thresh:
                # if probability is high will multiply its value:
                if probabilities[max_idx] > self.md_module.event_classifier.params.high_probability_value / cls_weight:
                    probabilities[max_idx] = min(probabilities[max_idx] * self.md_module.event_classifier.params.high_probability_multiplier, 3)
            agg_prob_vec_single_cam += probabilities
        maxIndices = np.argsort(agg_prob_vec_single_cam)[::-1][:self.params.log_top_k_products]
        top_products = [(self.idx2product(i), agg_prob_vec_single_cam[i]) for i in maxIndices]
        return top_products

    def separate_event_if_needed(self, top_b, top_f, cls_seq):
        if top_b[0][1] > top_b[1][1] * self.params.separate_conf_factor and top_f[0][1] > top_f[1][1] * 4 and top_b[0][0] != top_f[0][0]:
            similarity_mat = self.md_module.event_classifier.yaaen.similar_products if hasattr(self.md_module.event_classifier.yaaen, 'similar_products') else None
            if similarity_mat is None:
                return False
            back_is_valid = self.product_classified_in_enough_frames(cls_seq, self.product2idx(top_b[0][0]), cam='back') and \
                            self.product_seen_in_different_locations(cls_seq, self.product2idx(top_b[0][0]), top_k=2, cam='back')
            front_is_valid = self.product_classified_in_enough_frames(cls_seq, self.product2idx(top_f[0][0]), cam='front') and \
                            self.product_seen_in_different_locations(cls_seq, self.product2idx(top_f[0][0]), top_k=2, cam='front')
            if not back_is_valid or not front_is_valid:
                return False
            if (top_b[0][0] in similarity_mat.keys() and top_f[0][0] in similarity_mat[top_b[0][0]].keys()) or \
                    (top_f[0][0] in similarity_mat.keys() and top_b[0][0] in similarity_mat[top_f[0][0]].keys()):
                print("Not splitting because similar products have been seen in different cams: {} and {}!!".format(top_b[0][0], top_f[0][0]), flush=True)
                return False  # todo - force unknown?
            print("Splitting to 2 events: {} and {}!!".format(top_b[0][0], top_f[0][0]), flush=True)
            return True
        return False

    def isWinningIdxisHand(self,winningIndex):
        return winningIndex == self.product2idx(self.params.hand_item_text)

    def handIsMuchMoreProbable(self,cls_seq, second_item_indx, high_conf=0.95):
        # Many consequtive confident hand predictions
        hand_prob_vec = cls_seq['classification_prob_mat'][:, self.product2idx('aa_hand')]
        if (hand_prob_vec > high_conf).sum() > 0:
            max_consequtive_confident_hand = np.array([sum( 1 for _ in group ) \
                for key, group in itertools.groupby(hand_prob_vec > high_conf) if key]).max()
            if max_consequtive_confident_hand > max(len(hand_prob_vec) / 2, 7):
                return True
        return cls_seq['agg_prob_vec'][self.product2idx('aa_hand')] > cls_seq['agg_prob_vec'][second_item_indx] * self.params.hand_multi_factor

    def product_classified_in_enough_frames(self, cls_seq, productIndex, top_k=2, min_conf=0.2, cam=None):
        # movement_vec_len = [ 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19]
        # enough_frames    =  [1. 2. 2. 2. 2. 2. 3. 3. 3. 3. 3. 4. 4. 4. 4. 4. 5. 5. 5. 5.]
        n_images = cls_seq['classification_prob_mat'].shape[0] if cam is None else len(cls_seq['bbox_vec'][cam])
        enough_frames = min(np.ceil(n_images/5)+1, 5)
        if cam is not None:
            enough_frames = self.params.enough_frames_single

        top_k_idx = np.argsort(-cls_seq['classification_prob_mat'], axis=1)[:,:top_k]
        product_in_top_k = (top_k_idx==productIndex).sum(axis=1)
        product_confident_enough = cls_seq['classification_prob_mat'][:, productIndex] > min_conf
        product_maybe_seen = np.bitwise_and(product_in_top_k, product_confident_enough)
        cb_count = 0
        all_count = 0
        allowed_cams = self.cam_names if cam is None else [cam]
        for cam_name in allowed_cams:
            for (bbox, idx, _, mv_ids_info) in cls_seq['bbox_vec'][cam_name]:
                if product_maybe_seen[idx]:
                    all_count += 1
                    if mv_ids_info is None:
                        cb_count += 1
        return all_count >= enough_frames or cb_count == 2

    def product_seen_in_cb(self, cls_seq, productIndex, top_k=2, min_conf=0.2):
        top_k_idx = np.argsort(-cls_seq['classification_prob_mat'], axis=1)[:,:top_k]
        product_in_top_k = (top_k_idx==productIndex).sum(axis=1)
        product_confident_enough = cls_seq['classification_prob_mat'][:, productIndex] > min_conf
        product_maybe_seen = np.bitwise_and(product_in_top_k, product_confident_enough)
        for cam_name in self.cam_names:
            for (bbox, idx, _, mv_ids_info) in cls_seq['bbox_vec'][cam_name]:
                if mv_ids_info is None and product_maybe_seen[idx]:
                    return True
        return False

    def back_cam_event_near_edge(self, cls_seq):
        for cam in cls_seq['bbox_vec'].keys():
            if cam=='back' and len(cls_seq['bbox_vec'][cam]) > 0:
                continue
            elif len(cls_seq['bbox_vec'][cam]) == 0:
                continue
            else:
                return False
        cb_boxes = [mv_el.coords for mv_el in cls_seq['movement_vector_for_saving'] if mv_el.source == 'CB']
        md_boxes = [mv_el.coords for mv_el in cls_seq['movement_vector_for_saving'] if mv_el.source != 'CB']
        if len(cb_boxes) == 1:
            bottom_box = cb_boxes[0]
        else:
            bottom_box = md_boxes[0] if cls_seq['event_direction'] == 'out' else md_boxes[-1]
        x_cen_list = []
        for box in md_boxes:
            x_cen_list.append((box.x1 + box.x2) / 2)
        x_cen_list = np.array(x_cen_list)
        x_cen_wo_outliers = x_cen_list[abs(x_cen_list - np.mean(x_cen_list)) < 2 * np.std(x_cen_list)]
        # if x and y location of the bottom bbox are consistent with the fukoshima area and std is small
        # (i.e - vertical removal or insertion that characterizes fukoshima) it is probably indeed fukoshima.
        if bottom_box.x2 <= self.params.back_edge_x2_max and \
                self.params.back_edge_y2_min <= bottom_box.y2 <= self.params.back_edge_y2_max and \
                np.std(x_cen_wo_outliers) <= self.params.back_edge_std_th:
            return True
        return False


    def product_seen_in_different_locations(self, cls_seq, productIndex, top_k=1, min_conf=0.2, cam=None):
        top_k_idx = np.argsort(-cls_seq['classification_prob_mat'], axis=1)[:,:top_k]
        product_in_top_k = (top_k_idx==productIndex).sum(axis=1).astype(np.bool)
        product_confident_enough = cls_seq['classification_prob_mat'][:, productIndex] > min_conf
        product_maybe_seen = np.bitwise_and(product_in_top_k, product_confident_enough)
        allowed_cams = self.cam_names if cam is None else [cam]
        back_cam_event_near_edge = True if 'back' in allowed_cams and self.back_cam_event_near_edge(cls_seq) else False
        for cam_name in allowed_cams:
            chosen_bbox = []
            chosen_frames = []
            for (bbox, idx, frame_idx, mv_ids_info) in cls_seq['bbox_vec'][cam_name]:
                if product_maybe_seen[idx]:
                    if mv_ids_info is None: # Cart bottom
                        return True
                    chosen_bbox.append(bbox)
                    chosen_frames.append(frame_idx)
            if len(chosen_bbox) > 1:
                top_box_idx = np.array([b.centroid()[1] for b in chosen_bbox]).argmin()
                bottom_box_idx = np.array([b.centroid()[1] for b in chosen_bbox]).argmax()
                valid_polarity = chosen_frames[top_box_idx] < chosen_frames[bottom_box_idx] if cls_seq['event_direction']=='in' else \
                                 chosen_frames[top_box_idx] > chosen_frames[bottom_box_idx]
                top_box = chosen_bbox[top_box_idx]
                bottom_box = chosen_bbox[bottom_box_idx]
                vertical_diff = min(bottom_box.y1 - top_box.y1, bottom_box.y2 - top_box.y2)
                top_2_bottom_diff = bottom_box.y2 - top_box.y1
                if valid_polarity and (vertical_diff > self.params.small_vertical_diff and
                                       (BoundingBox2D.iou(top_box, bottom_box) < self.params.iou_for_vertical_diff or
                                        vertical_diff > self.params.big_vertical_diff or back_cam_event_near_edge or
                                        top_2_bottom_diff > self.params.top_2_bottom_diff)):
                    return True
        return False

    def is_item_unknown_with_predictions(self, top_products):
        #top_products = [i for i in top_products if i[0] != config.HAND_ITEM_TEXT] # remove hand tuple if it exists
        return (top_products[0][1] / top_products[1][1]) < self.params.unknown_with_prediction_decision_ratio

    
    def external_algo_trigger(self):
        '''
        Debug Feature Only - Do not use in real-time
        '''
        assert self.params.store_sent_events==True and \
            hasattr(self, 'sent_events'), 'Debug feature - enable params.store_sent_events'
        # Remove last item from purchase list
        if len(self.md_module.purchaseList)>0:
            self.md_module.purchaseList[-1]['name'] = self.params.low_conf_item_text
            # pop last event from event logic sent events
            last_item = self.sent_events.pop()
            self.special_item_handler.trigger_algo_sequence(last_item['event'], last_item['prediction'], 'barcode')

    def get_product_sku(self, event_type, top_products):
        if event_type in (EventType.ProductConfidentEventFirstValid, EventType.ProductConfidentBarcodeId):
            return top_products[0][0]
        elif event_type==EventType.ProductConfidentEventSecondValid:
            return top_products[1][0]
        elif event_type in (EventType.ClassifierNotConfidentEvent, EventType.NotConfidentEventTriggerBarcode, EventType.NotConfidentEventTriggerUnrecognized):
            return self.params.unknown_item_text
        elif event_type in (EventType.LowConfidenceEvent, EventType.CartBottomOnlyEvent):
            return self.params.low_conf_item_text
    
    def is_cb_only(self, cls_seq):
        try:
            for mv in cls_seq['movement_vector_for_saving']:
                if mv.source!='CB':
                    return False
            return True
        except:
            return False

    def send_event(self,cls_seq, seq_result, force_filtered=False, check_filtered=False, special_item_expected=None):
        event_type, top_products, unsupported_candidate, special_prediction = seq_result
        # seq_result = (event_type, top_products)

        include_event = True
        location_idx = None
        # remove marked mvs in barcode mode that are 'out' events
        if cls_seq['initiating_cam'] == 'front' and cls_seq['marked_during_barcode_scan'] and \
                cls_seq['event_direction'] == 'out':
            print("filtering 'out' event with marked mvs (allowing only 'in' for marked mvs)",
                  flush=True)
            return

        # Event was conditionally sent because of barcode mode but isn't similar enough -> ignore
        if event_type==EventType.FilteredEvent and not (force_filtered or check_filtered):
            return
        
        # Ignore CB only events if needed
        is_cb_only_event = self.is_cb_only(cls_seq)
        if self.params.ignore_cb_only_events and is_cb_only_event and special_item_expected is None:
            if not cls_seq['multi_cam']:
                print('Ignoring CB only event(ID %d)-No special event expected' % (cls_seq['event_id']), flush=True)
                return
        
        # Ignore marginal events right after insertion
        # ============================================
        if self.params.ignore_marginal_out_n_frames_after_in is not None:
            if event_type==EventType.FilteredEvent and cls_seq['event_direction']=='out':
                if not self.valid_marginal_removal_event(cls_seq):
                    return


        if hasattr(self.md_module, 'cart_content_handler') and hasattr(self.md_module.cart_content_handler, 'location_matching'):
            if cls_seq['event_direction'] == 'out':
                location_predictions = self.md_module.cart_content_handler.location_matching.match(cls_seq)
                event_type, top_products, location_idx = self.md_module.cart_content_handler.location_matching.compare_location_classifier_predictions(event_type, top_products, location_predictions)
                seq_result = (event_type, top_products, unsupported_candidate, special_prediction)

        # Special items logic
        # ===================
        similar_seq = None
        # Force special item prediction if needed
        event_part_of_verification = dict(barcode=False, plu=False)
        forced_product_type = None
        similar_products = None
        if special_item_expected is not None:
            forced_product_type, similar_seq = self.special_item_handler.force_product_id(cls_seq, seq_result, force_filtered, special_item_expected)
            if forced_product_type is not None:
                event_type, top_products_special = self.special_item_handler.assign_id_to_seq(seq_result, forced_product_type)
                seq_result = (event_type, top_products_special, unsupported_candidate, special_prediction)
            if forced_product_type=='barcode':
                cls_seq['barcode_resolve_event_id'] = self.md_module.barcode_handler.get_resolved_event_id()
                event_part_of_verification['barcode'] = True
        
        # Trigger PLU event
        if special_item_expected is None and forced_product_type is None and self.special_item_handler.should_trigger_request(cls_seq, seq_result, 'plu'):
            # event_type = EventType.NotConfidentEventTriggerPlu
            # self.special_item_handler.trigger_algo_sequence(cls_seq, (event_type, top_products), 'plu')
            event_type = EventType.NotConfidentEventTriggerUnrecognized # map event to unrecognized
            self.special_item_handler.trigger_algo_sequence(cls_seq, (event_type, top_products), 'unrecognized')
            seq_result = (event_type, top_products, unsupported_candidate, special_prediction)
    

        # Trigger unrecognized event
        elif special_item_expected is None and forced_product_type is None and location_idx is None and \
            self.special_item_handler.should_trigger_request(cls_seq, seq_result, 'unrecognized'):
            event_type = EventType.NotConfidentEventTriggerUnrecognized
            self.special_item_handler.trigger_algo_sequence(cls_seq, (event_type, top_products), 'unrecognized')
            seq_result = (event_type, top_products, unsupported_candidate, special_prediction)
            if self.params.store_sent_events:
                self.sent_events.append(dict(event=cls_seq, prediction=seq_result))

        # Trigger barcode event
        elif hasattr(self.md_module, 'barcode_handler') and forced_product_type is None:
            can_trigger_barcode_event = self.special_item_handler.should_trigger_request(cls_seq, seq_result, 'barcode')
            event_similar_to_barcode_event = similar_seq if similar_seq is not None else \
                self.md_module.barcode_handler.is_seq_scanned(cls_seq, seq_result)
            event_part_of_verification['barcode'] = special_item_expected=='barcode' and event_similar_to_barcode_event
            if not event_part_of_verification['barcode'] and can_trigger_barcode_event \
                    and location_idx is None:
                # event_type = EventType.NotConfidentEventTriggerBarcode
                # self.special_item_handler.trigger_algo_sequence(cls_seq, (event_type, top_products), 'barcode')
                event_type = EventType.NotConfidentEventTriggerUnrecognized # map event to unrecognized
                self.special_item_handler.trigger_algo_sequence(cls_seq, (event_type, top_products), 'unrecognized')
                seq_result = (event_type, top_products, unsupported_candidate, special_prediction)
            # Map filtered out event to location based match or barcode scanning
            # TODO - match by descriptors to most probable products
            elif not event_part_of_verification['barcode'] and event_type==EventType.FilteredEvent and check_filtered \
                and can_trigger_barcode_event:
                assert location_idx is None # if location was matched event type should have changed
                # TODO - check if we should trigger barcode event
                # event_type = EventType.NotConfidentEventTriggerBarcode
                # self.special_item_handler.trigger_algo_sequence(cls_seq, (event_type, top_products), 'barcode')
                event_type = EventType.NotConfidentEventTriggerUnrecognized # map event to unrecognized
                self.special_item_handler.trigger_algo_sequence(cls_seq, (event_type, top_products), 'unrecognized')
                seq_result = (event_type, top_products, unsupported_candidate, special_prediction)
            if not event_part_of_verification['barcode'] and not can_trigger_barcode_event and event_type==EventType.FilteredEvent:
                include_event = False
        else:
            event_part_of_verification['barcode'] = False
        
        # Notify PLU event
        # This is not a trigger but only a pointer of the sequence to PLU handler
        if hasattr(self.md_module, 'plu_handler') and forced_product_type=='plu':
            self.special_item_handler.push_seq_to_handler_event_queue(cls_seq, (event_type, top_products), 'plu')
        
        # Make sure no filtered event is not sent
        if event_type==EventType.FilteredEvent:
            if event_part_of_verification['barcode']:
                event_type = EventType.ProductConfidentEventFirstValid
            else:
                include_event = False
        
        # log event info to capsule
        register_event = not event_part_of_verification['barcode'] or event_type in (EventType.ProductConfidentBarcodeId, EventType.NotConfidentEventTriggerBarcode)
        dct_event = event_type in (EventType.NotConfidentEventTriggerUnrecognized, EventType.ClassifierNotConfidentEvent, \
                                EventType.CartBottomOnlyEvent, EventType.LowConfidenceEvent)
        if self.logger is not None and self.logger.should_log('user_marked_event'):
            event_info_log = dict(include_event=include_event,
                                  register_event=register_event,
                                  part_of_bc_flow=event_part_of_verification['barcode'],
                                  prediction=top_products,
                                  unsupported_candidate=unsupported_candidate,
                                  trigger_dct=dct_event and include_event,
                                  event_type=event_type.name,
                                  )
            self.logger.log(self.mel.pack(cls_seq['event_id'], 'EventPred', metadata=event_info_log),\
                            'user_marked_event', frame=cls_seq['event_frame'])

        if include_event:
            # Make sure no confident prediction is sent if all events are mapped to interaction
            if self.params.all_events_to_interaction:
                if special_item_expected is None and not event_part_of_verification['barcode'] and not dct_event \
                    and location_idx is None:
                    event_type = EventType.NotConfidentEventTriggerUnrecognized

            # Make sure non-confident event has a reference event in unrecognized events
            if event_type in (EventType.ClassifierNotConfidentEvent, EventType.CartBottomOnlyEvent, EventType.LowConfidenceEvent):
                self.special_item_handler.push_seq_to_handler_event_queue(cls_seq, (event_type, top_products), 'unrecognized')
            # Update MD event list
            # ====================
            productNameToList = self.get_product_sku(event_type, top_products)
            self.add_md_event(cls_seq, event_type, productNameToList, top_products, event_part_of_verification['barcode'])
            
            # Update cart content
            # ===================
            register_event = not event_part_of_verification['barcode'] or event_type in (EventType.ProductConfidentBarcodeId, EventType.NotConfidentEventTriggerBarcode)
            if register_event and hasattr(self.md_module, 'cart_content_handler'):
                # Don't send UI events matched to marked events
                if special_item_expected=='barcode':
                    cls_seq['skip_gui'] = self.md_module.barcode_handler.is_marker_event_match(cls_seq['event_direction'], cls_seq['event_frame'])
        
                ### add user op
                if hasattr(self.md_module, 'user_op_handler'):
                    multi_scan_res = self.md_module.user_op_handler.resolve(cls_seq['event_id'], cls_seq['event_direction'])
                    if dct_event and (resolveResult.interactionNeeded != multi_scan_res):
                        print("changing event_type to MultiScanInProgress so we do not trigger DCT",flush=True)
                        event_type = event_type.MultiScanInProgress
                        cls_seq['skip_gui'] = True
        
                if cls_seq['event_direction'] == 'out' and len(self.removal_marked) != 0:
                    cls_seq['skip_gui'], seq_result = self.check_marked_removal(seq_result)
                    (event_type, top_products) = seq_result[:2]
                self.md_module.cart_content_handler.register_event(cls_seq, (event_type, top_products), location_idx)
                # Log event latency and summarize
                if hasattr(self.md_module, 'latency_buffer'):
                    self.md_module.latency_buffer.append('t_logic_out', time.time(), cls_seq['event_frame'])
                    event_latency = self.md_module.latency_buffer.get(cls_seq['event_frame'])
                    self.md_module.latency_buffer.reset()
                    try:
                        latency_data = dict(event_type='latency', event_id=cls_seq['event_id'],
                                            event_payload=dict(total_delay='%.3f' % (event_latency['t_logic_out'] - event_latency['t_grab']),
                                                            md_delay='%.3f' % (event_latency['t_logic_out'] - event_latency['t_md']),
                                                            md_t_mv_delay='%.3f' % (event_latency['t_mv_manager_out'] - event_latency['t_md']),
                                                            mv_t_agg_delay='%.3f' % (event_latency['t_aggregator_out'] - event_latency['t_mv_manager_out']),
                                                            agg_t_cls_delay='%.3f' % (event_latency['t_classifier_out'] - event_latency['t_aggregator_out']),
                                                            cls_t_logic_delay='%.3f' % (event_latency['t_logic_out'] - event_latency['t_classifier_out'])))

                        print_formatted_message(latency_data, bold=False)
                    except:
                        pass


    def add_md_event(self,cls_seq, event_type, productNameToList, top_products, part_of_barcode_verification):
        if event_type not in (EventType.HandEvent, EventType.FilteredEvent):
            # Overwrite name if marked for validation
            if 'mark_for_validation' in cls_seq and cls_seq['mark_for_validation']:
                productNameToList = self.params.unknown_item_text

            data = dict(event_type='shopping', event_id=cls_seq['event_id'],
                        event_payload=dict(direction=cls_seq['event_direction'],
                                        prediction=productNameToList,
                                        top_5_products=get_top_prod_dict(top_products, self.md_module.catalog.get_english_name),
                                        frame=cls_seq['event_frame'],
                                        md_event_type=event_type.name,
                                        activated_by_md=cls_seq['activated_by_md'],
                                        activated_by_cart_bottom=cls_seq['activated_by_cart_bottom'],
                                        size=len(cls_seq['movement_vector_for_saving']),
                                        initiating_cam=cls_seq['initiating_cam'],
                                        multi_cam=cls_seq['multi_cam'],
                                        acceptance_test=config.ACCEPTANCE_TEST,
                                        )
                        )
            has_gui = hasattr(self.md_module, 'gui')
            if has_gui:
                from cart_blocks.cart_content_handler import EventApiDict
                data['event_payload']['event_type'] = EventApiDict[event_type.name].value
            print_formatted_message(data, bold=False)

        productDict = {
            'name': productNameToList,
            'direction': cls_seq['event_direction'],
            'frameNum': cls_seq['event_frame'],
            'eventStart': cls_seq['event_start'],
            'topProb': top_products,
            'id': cls_seq['event_id'],
            'front_id': cls_seq['front_id'],
            'back_id': cls_seq['back_id'],
            'event_type': event_type,
            'initiating_cam': cls_seq['initiating_cam'],
            'both_cams_saw_event': cls_seq['both_cams_saw_event'],
        }
        if hasattr(self.md_module, 'barcode_handler'):
            productDict['part_of_barcode_verification'] = part_of_barcode_verification

        self.md_module.purchaseList.append(productDict)

    def add_low_confidence_event(self, cls_seq, seq_result):
        if self.params.send_low_conf_as_unknown:
            top_prod_modified = [p for p in seq_result[1] if p[0] != self.params.hand_item_text]
            seq_result_modified = (EventType.LowConfidenceEvent, top_prod_modified, seq_result[2], seq_result[3])
            # Check if special item expected
            special_item_expected = self.special_item_handler.expected_event(cls_seq, seq_result)
            self.send_event(cls_seq, seq_result_modified, special_item_expected=special_item_expected)
            if self.params.store_sent_events:
                self.sent_events.append(dict(event=cls_seq, prediction=seq_result))
        else:
            productDict = {
                'name': self.params.low_conf_item_text,
                'topProb': seq_result[1],
                'direction': cls_seq['event_direction'],
                'frameNum': cls_seq['event_frame'],
                'id': cls_seq['event_id'],
                'front_id': cls_seq['front_id'],
                'back_id': cls_seq['back_id'],
                'event_type': seq_result[0],
                'initiating_cam': cls_seq['initiating_cam'],
                'both_cams_saw_event': cls_seq['both_cams_saw_event'],
            }
            message = "Adding low confidence event going {}".format(cls_seq['event_direction'])
            print(bcolors.BOLD + message + bcolors.ENDC, flush=True)
            self.md_module.purchaseList.append(productDict)

    def handle_item(self, winningIndex):
        productNameToList = self.idx2product(winningIndex)
        price = self.md_module.catalog.get_price(productNameToList)
        return price, productNameToList

    def handle_unknown_item_without_predictions(self):
        productNameToList = self.params.unknown_item_text
        price = 0.0
        # TODO- make cart led red! unknown product entered the cart!
        return price, productNameToList

    def handle_unknown_item_with_predictions(self, top_products):
        str = 'We have alternatives: '
        for product in top_products:
            str += '{}:{:.1f}, '.format(product[0], product[1])
        print(str[:-1], flush=True)

        price, productNameToList = self.handle_unknown_item_without_predictions()  # TODO - display alternatives in GUI and wait for answer a few sec
        return price, productNameToList

    def filtered_out_after_in(self, cls_seq):
        if not cls_seq['event_direction'] == 'out':
            return False
        event_frame = cls_seq['event_frame']
        event_start = cls_seq['event_start']
        for purchased_item in reversed(self.md_module.purchaseList):
            if abs(event_frame - purchased_item['frameNum']) > max(300, event_frame - event_start):
                break
            if purchased_item['direction'] == 'in' and (abs(event_frame - purchased_item['frameNum']) <= 40 or abs(event_start - purchased_item['frameNum']) <= 10):
                return True
        return False

    def need_degeneration(self, cls_seq, seq_result):
        if not (config.degenerate_duplicates and len(self.md_module.purchaseList) > 0):
            return False
            
        ### if multi scan in progress
        if hasattr(self.md_module, 'user_op_handler'):
            if 1 < self.md_module.user_op_handler.nActiveOps():
                return False
                    
        direction = cls_seq['event_direction']
        product_name = seq_result[1][1][0] if seq_result[0] == EventType.ProductConfidentEventSecondValid else seq_result[1][0][0]
        event_frame = cls_seq['event_frame']
        event_start = cls_seq['event_start']
        special_prediction = seq_result[3]
        degenerate = False
        for purchased_item in reversed(self.md_module.purchaseList):
            if abs(event_frame - purchased_item['frameNum']) > max(300, event_frame - event_start):
                break
            if purchased_item['direction'] == direction and (purchased_item['name'] == product_name or\
                    special_prediction==SpecialPrediction.OtherProduct or self.similar_unknown(purchased_item, seq_result)):
                # Harder constraint on duplicated events after barcode
                if hasattr(self.md_module, 'barcode_handler') and \
                    event_frame - self.md_module.barcode_handler.last_verified_barcode_frame < self.params.frames_diff_for_degeneration_barcode:
                    degenerate = True
                    break
                elif not cls_seq['activated_by_md'] and cls_seq['activated_by_cart_bottom']: # initiated by CB
                    if event_start is not None: # sent as high conf diffs
                        if purchased_item['frameNum'] >= event_start:
                            degenerate = True
                            break
                    if abs(purchased_item['frameNum'] - event_frame) <= self.params.frames_diff_for_degeneration_cb:
                        degenerate = True
                        break
                elif abs(purchased_item['frameNum'] - event_frame) <= self.params.frames_diff_for_degeneration or \
                     abs(purchased_item['eventStart'] - event_start) <= self.params.frames_diff_for_degeneration:
                    degenerate = True
                    break
            # degeneration logic for msco mode
            elif purchased_item['direction'] == direction and hasattr(self.md_module, 'barcode_handler') and \
                    config.MSCO_MODE:
                if (not cls_seq['activated_by_md'] and cls_seq['activated_by_cart_bottom']) and \
                    (abs(purchased_item['frameNum'] - event_frame) <= self.params.frames_diff_for_degeneration_cb or
                     purchased_item['frameNum'] > event_start):
                    degenerate = True
                    break
                elif cls_seq['initiating_cam'] != purchased_item['initiating_cam'] and \
                        not cls_seq['both_cams_saw_event'] and not purchased_item['both_cams_saw_event'] and \
                        abs(purchased_item['frameNum'] - event_frame) < self.params.frames_diff_for_degeneration:
                    degenerate = True
                    break
        if degenerate and not self.allow_duplicate(cls_seq, purchased_item):
            return True
        return False

    def allow_duplicate(self, cls_seq, purchased_item, thresh=None):
        if not config.ALLOW_DUPLICATES:
            return False
        if thresh is None:
            thresh = self.params.allow_duplicate_threshold
        front_count = self.count_multiple_boxes(cls_seq['bbox_vec']['front'], cls_seq['front_id'], purchased_item['front_id'])
        if front_count >= max(5, len(cls_seq['bbox_vec']['front']) * thresh):
            print("Allow duplicate!!! Seen two front ids in {}/{} frames".format(front_count, len(cls_seq['bbox_vec']['front'])))
            return True
        back_count = self.count_multiple_boxes(cls_seq['bbox_vec']['back'], cls_seq['back_id'], purchased_item['back_id'])
        if back_count >= max(5, len(cls_seq['bbox_vec']['back']) * thresh):
            print("Allow duplicate!!! Seen two back ids in {}/{} frames".format(back_count, len(cls_seq['bbox_vec']['back'])))
            return True
        return False

    def count_multiple_boxes(self, bbox_vec, id1, id2):
        if id1 is None or id2 is None:
            return 0
        count = 0
        for elem in bbox_vec:
            found_first, found_second = False, False
            if elem[3] is not None:
                for score_id in elem[3]:
                    if score_id['id'] == id1:
                        found_first = True
                    elif score_id['id'] == id2:
                        found_second = True
                if found_first and found_second:
                    count += 1
        return count


    def similar_unknown(self, purchased_item, seq_result):
        # return True if one of the events is unknown/low confidence and the top guesses are similar
        if purchased_item['name'] in (self.params.unknown_item_text, self.params.low_conf_item_text):
            purchased_item_guesses = [purchased_item['topProb'][0][0], purchased_item['topProb'][1][0]]
            if seq_result[0]==EventType.ProductConfidentEventFirstValid:
                if seq_result[1][0][0] in purchased_item_guesses:
                    return True
            elif seq_result[0]==EventType.ProductConfidentEventSecondValid:
                if seq_result[1][1][0] in purchased_item_guesses:
                    return True
            elif seq_result[0] in (EventType.ClassifierNotConfidentEvent, EventType.CartBottomOnlyEvent):
                if seq_result[1][0][0] in purchased_item_guesses or seq_result[1][1][0] in purchased_item_guesses:
                    return True
        else:
            purchased_item_guess = purchased_item['name']
            if seq_result[0] in (EventType.ClassifierNotConfidentEvent, EventType.CartBottomOnlyEvent, EventType.FilteredEvent):
                if purchased_item_guess in (seq_result[1][0][0], seq_result[1][1][0]):
                    return True
        return False
