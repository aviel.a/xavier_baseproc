from cv_blocks.events.event_logic import EventType
class UnidentifiedHandler(object):
    '''
    A class responsible for handling unidentified items
    '''
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.enable_personal_item = config.get('enable_personal_item', True)
    
    def __init__(self, md_module, uh_config=dict(), msco=False):
        self.params = self.Config(uh_config)
        self.md_module = md_module
        self.pending_verification_events = dict(barcode=[], plu=[], unrecognized=[])
        self.msco_mode = msco
    
    def reset(self):
        self.pending_verification_events = dict(barcode=[], plu=[], unrecognized=[])
    
    def expected_event(self, cls_seq, seq_result):
        spcial_item = None
        filtered_event = seq_result[0] in (EventType.HandEvent, EventType.FilteredEvent)
        if hasattr(self.md_module, 'barcode_handler'):
            # are  we expecting an event in the same direction during barcode verification
            if self.md_module.barcode_handler.is_expecting_event(cls_seq['event_direction'], cls_seq['event_frame'], filtered_event):
                spcial_item = 'barcode'
        if spcial_item is None and hasattr(self.md_module, 'plu_handler'):
            if self.md_module.plu_handler.is_expecting_event(cls_seq['event_direction'], cls_seq['event_frame'], filtered_event):
                spcial_item = 'plu'
        return spcial_item
    
    def force_product_id(self, cls_seq, seq_result, force_filtered, special_item_expected):
        valid_direction = None
        similar_seq = None
        force_prod_id = False
        if special_item_expected=='barcode' and hasattr(self.md_module, 'barcode_handler'):
            # Barcode result is set only for insersion events. Removal events are updated after removal
            valid_direction = cls_seq['event_direction']=='in'
            if not valid_direction:
                return None, similar_seq
            if force_filtered:
                # For filtered event all similarity checks were done before, hence it's valid
                return 'barcode', True
            similar_seq = self.md_module.barcode_handler.is_seq_scanned(cls_seq, seq_result)
            force_prod_id = 'barcode' if valid_direction and similar_seq else None
            return force_prod_id, similar_seq
        elif special_item_expected=='plu' and hasattr(self.md_module, 'plu_handler'):
            # PLU result - insertion event, when special item is "plu"
            # TODO - check classification result to verify/reject it's a PLU item
            valid_direction = cls_seq['event_direction']=='in'
            if valid_direction:
                return 'plu', True
            else:
                return None, similar_seq
        else:
            return None, False

    def assign_id_to_seq(self, seq_result, forced_product_type):
        event_type, top_products = seq_result[:2]
        if forced_product_type=='barcode':
            if not hasattr(self.md_module, 'barcode_handler'):
                return event_type, top_products
            if 'product_code' in self.md_module.barcode_handler.curr_barcode_event:
                barcode_predicition = self.md_module.barcode_handler.curr_barcode_event['product_code']
            else:
                scanned_barcode = self.md_module.barcode_handler.curr_barcode_event['scanned_barcode']
                products_with_barcode = self.md_module.catalog.get_products_with_barcode(scanned_barcode)
                if len(products_with_barcode)==0:
                    # Place holder, should ignore barcodes invalid according to catalog
                    products_with_barcode = [scanned_barcode]
                # Assign product if one of the possible products appear in the original predictions
                # If not, take the first one
                original_pred = [p[0] for p in top_products]
                barcode_predicition = products_with_barcode[0]
                for single_pred in original_pred:
                    if single_pred in products_with_barcode:
                        barcode_predicition = single_pred
                        break
            for idx in range(len(top_products)):
                top_products[idx] = (barcode_predicition, 1.0)
            
            return EventType.ProductConfidentBarcodeId, top_products
        elif forced_product_type=='plu':
            for idx in range(len(top_products)):
                top_products[idx] = ('plu_item', 1.0)
            return EventType.NotConfidentEventTriggerPlu, top_products
    
    def trigger_algo_sequence(self, cls_seq, seq_result, seq_type):
        if seq_type=='barcode' and hasattr(self.md_module, 'barcode_handler'):
            # Put sequence in "pending barcode verification"
            self.pending_verification_events['barcode'].append(dict(seq=cls_seq, result=seq_result))
            # Send message to barcode handler
            internal_trigger = {'cmd':'barcode_event_info', \
                                'payload': {'msg_type': 'new_event_request', 'trigger': 'internal', 'event_ID': cls_seq['event_id']}}
            self.md_module.barcode_handler.write_message(internal_trigger)
        elif seq_type=='plu' and hasattr(self.md_module, 'plu_handler'):
            self.pending_verification_events['plu'].append(dict(seq=cls_seq, result=seq_result))
            if self.md_module.plu_handler.is_idle():
                # Send internal trigger message
                internal_trigger = {'cmd':'plu_event_info', 'payload': {'msg_type': 'new_event_request', 'trigger': 'internal'}}
                self.md_module.plu_handler.push_message(internal_trigger)
        elif seq_type=='unrecognized':
            self.pending_verification_events['unrecognized'].append(dict(seq=cls_seq, result=seq_result))

    def push_seq_to_handler_event_queue(self, cls_seq, seq_result, seq_type):
        if seq_type not in ('barcode', 'plu', 'unrecognized'):
            return
        event_id = cls_seq['event_id']
        n_pending_events_same_id = len([1 for ev in self.pending_verification_events[seq_type] if ev['seq']['event_id']==event_id])
        if n_pending_events_same_id==0:
            self.pending_verification_events[seq_type].append(dict(seq=cls_seq, result=seq_result))

    def redirect(self, event_id, trigger_type):
        '''
        Trigger handler according to shopper resopnse to unidentified item
        '''
        event_idx = [idx for idx,ev in enumerate(self.pending_verification_events['unrecognized']) if event_id==ev['seq']['event_id']]
        if len(event_idx)!=1:
            print('UnidentifiedHandler DEBUG[%d]: Invalid redirect state, ignoring' % (self.md_module.global_frame_num), flush=True) 
            return

        redirected_seq = self.pending_verification_events['unrecognized'].pop(event_idx[0])
        print('UnidentifiedHandler DEBUG[%d]: redirecting to %s verification' % (self.md_module.global_frame_num, trigger_type), flush=True) 
        self.trigger_algo_sequence(redirected_seq['seq'], redirected_seq['result'], trigger_type)
    
    def should_trigger_request(self, cls_seq, seq_result, req_type):
        if req_type not in ('barcode', 'plu', 'unrecognized'):
            return False
        if req_type=='barcode':
            ret = self.should_trigger_barcode_request(cls_seq, seq_result)
        elif req_type=='plu':
            ret = self.should_trigger_plu_request(cls_seq, seq_result)
        elif req_type=='unrecognized':
            ret = self.should_trigger_unrecognized_request(cls_seq, seq_result)
        print('UnidentifiedHandler DEBUG[%d]: checking %s trigger request -> %r' \
            % (self.md_module.global_frame_num, req_type, ret), flush=True) 
        return ret
        

    def should_trigger_unrecognized_request(self, cls_seq, seq_result, filter_only_confident_hands=False, ):
        # TODO - impl. logic - unsupported item, low probs, etc
        # Reject short events that are not low conf
        event_type, top_products, unsupported_candidate, predicted_other = seq_result
        seq_len = len(cls_seq['movement_vector_for_saving'])
        if cls_seq['thrown_item_candidate'] and cls_seq['event_direction']=='in':
            size_th = self.md_module.event_logic.params.min_thrown_sequence_size
        else:
            size_th = self.md_module.event_logic.params.pile_min_unsupported_sequence_size[cls_seq['event_direction']] if cls_seq['pile_candidate'] \
                else self.md_module.event_logic.params.min_unsupported_sequence_size[cls_seq['event_direction']]
        if seq_len < size_th and event_type not in (EventType.CartBottomOnlyEvent, EventType.LowConfidenceEvent) \
            and not cls_seq['small_item_candidate']:
            print('UnidentifiedHandler DEBUG[%d]: Rejected unrecognized request due to short MV' \
                % self.md_module.global_frame_num, flush=True) 
            return False
        # Reject all top-1 hand events
        hand_is_top1 = top_products[0][0]==self.md_module.event_logic.params.hand_item_text
        long_seq_th = self.md_module.event_logic.params.pile_long_unsupported_sequence_size if cls_seq['pile_candidate'] \
            else self.md_module.event_logic.params.long_unsupported_sequence_size
        long_seq_th = long_seq_th['multi'] if cls_seq['multi_cam'] else long_seq_th['single']
        # Check top-1 hand if top-2 is a small product
        if hand_is_top1:
            top2_product = top_products[1][0]
            if cls_seq['small_item_candidate'] or self.md_module.event_classifier.is_small_class(top2_product):
                hand_is_top1 = False
        el_params = self.md_module.event_logic.params
        if (self.msco_mode or el_params.filter_only_confident_hands) and top_products[0][1] / seq_len < el_params.high_hand_th and seq_len >= el_params.non_hand_len_th and \
                (top_products[0][1] / seq_len < el_params.low_hand_th or cls_seq['both_cams_saw_event'] or (cls_seq['multi_cam'] and cls_seq['activated_by_cart_bottom'])):
            hand_is_top1 = False
        if hand_is_top1 and not seq_len > long_seq_th:
            print('UnidentifiedHandler DEBUG[%d]: Rejected unrecognized request due to hand top-1 prediction' \
                % self.md_module.global_frame_num, flush=True) 
            return False
        # Reject removal events right after insert event with similar prediction
        if len(self.md_module.purchaseList) > 0 and self.md_module.event_logic.params.ignore_marginal_out_n_frames_after_in_similar_pred is not None:
            last_event_was_in = self.md_module.purchaseList[-1]['direction'] == 'in' and \
                self.md_module.purchaseList[-1]['name'] != self.md_module.event_logic.params.hand_item_text
            last_event_was_near = cls_seq['event_frame'] - self.md_module.purchaseList[-1]['frameNum'] < self.md_module.event_logic.params.ignore_marginal_out_n_frames_after_in_similar_pred
            similar_pred = seq_result[1][0][0]==self.md_module.purchaseList[-1]['name']
            if (last_event_was_in and last_event_was_near and similar_pred):
                print('UnidentifiedHandler DEBUG[%d]: Rejected unrecognized request due to Filtered out event similar to recent in event' \
                    % self.md_module.global_frame_num, flush=True) 
                return False

        # Send all events to interaction
        if self.md_module.event_logic.params.all_events_to_interaction:
            return True

        # Send all filtered unsupported candidates to unrecognized mode
        if unsupported_candidate:
            return True
        # Send all filtered events to unrecognized mode
        if self.md_module.event_logic.params.filtered_events_to_interaction and event_type==EventType.FilteredEvent:
            return True
        return False

    def should_trigger_plu_request(self, cls_seq, seq_result):
        event_type = seq_result[0]
        if event_type==EventType.ProductConfidentEventFirstValid:
            top1_pred_class_name = seq_result[1][0][0]
            return self.md_module.event_classifier.is_plu_class(top1_pred_class_name)
        elif event_type==EventType.ProductConfidentEventSecondValid:
            top2_pred_class_name = seq_result[1][1][0]
            return self.md_module.event_classifier.is_plu_class(top2_pred_class_name)
        else:
            return False

    def should_trigger_barcode_request(self, cls_seq, seq_result):
        event_type = seq_result[0]
        # Don't trigger event that was just decided by barcode - deadlock
        if event_type==EventType.ProductConfidentBarcodeId:
            return False
        if hasattr(self.md_module, 'barcode_handler'):
            # Make sure barcode handler is allowing a trigger
            if not self.md_module.barcode_handler.is_algo_trigger_allowed(cls_seq, seq_result):
                return False
            if event_type==EventType.ProductConfidentEventFirstValid:
                top1_pred_class_name = seq_result[1][0][0]
                if self.md_module.event_classifier.is_barcode_class(top1_pred_class_name):
                    return True
            elif event_type==EventType.ProductConfidentEventSecondValid:
                top2_pred_class_name = seq_result[1][1][0]
                if self.md_module.event_classifier.is_barcode_class(top2_pred_class_name):
                    return True
            # Send all non-confident classifier events to interaction
            if event_type==EventType.ClassifierNotConfidentEvent:
                return True
            top_products = seq_result[1]
            top1_score = top_products[0][1]
            top2_score = top_products[1][1]
            similar_top1 = self.very_similar_products(top_products[0][0], thresh=self.md_module.event_logic.params.similarity_th_for_barcode_trigger)
            similar_top2 = self.very_similar_products(top_products[1][0], thresh=self.md_module.event_logic.params.similarity_th_for_barcode_trigger)
            similar_product = similar_top1 is not None or similar_top2 is not None
            not_hand = top_products[0][0]!=self.md_module.event_logic.params.hand_item_text
            # Get confidence threshold, unique per class or general
            class_unique_th = self.md_module.event_classifier.get_class_confidence_threshold(top_products[0][0])
            if class_unique_th is not None:
                return top1_score / top2_score < class_unique_th and not_hand
            else:
                classifier_not_sure = top1_score / top2_score < self.md_module.event_logic.params.barcode_trigger_decision_ratio
                return classifier_not_sure and not_hand and similar_product
        else:
            return False

    def very_similar_products(self, product, thresh=None):
        # Returns only single most similar product
        # TODO - expand to multiple products
        if thresh is None:
            thresh = self.md_module.event_logic.params.very_similar_conf_th_id_split if self.md_module.event_classifier.yaaen.split_by_id else \
                self.md_module.event_logic.params.very_similar_conf_th_random_split
        similarity_mat = self.md_module.event_classifier.yaaen.similar_products if hasattr(
            self.md_module.event_classifier.yaaen, 'similar_products') else None
        if similarity_mat is not None and product in similarity_mat.keys():
            for similar_candidate in similarity_mat[product].keys():
                if similarity_mat[product][similar_candidate] > thresh:
                    return similar_candidate
        return None
