from cart_blocks.indications.indication_handler import IndicationEvent, allow_indication
from cv_blocks.events.movement_vector import EventDirection
class EventIndicator(object):
    '''
    A class responsible for indicating early events
    '''
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.min_active_mv_size = config.get('min_active_mv_size', 4)
            self.require_event_detector_signal = config.get('require_event_detector_signal', True)
            self.min_frames_between_indications = config.get('min_frames_between_indications', 50)

    def __init__(self, md_module, ei_config=dict(), debug=False):
        self.params = self.Config(ei_config)
        self.md_module = md_module
        self.last_indication_frame = 0
        self.last_triggering_mv = None
        self.curr_active_process_tirggers = []
        self.indication_running_idx = 1
        self.debug = debug
    
    def reset(self):
        self.last_indication_frame = 0
        self.last_triggering_mv = None
        self.curr_active_process_tirggers = []
        self.indication_running_idx = 1
    
    def alloc_new_idx(self, early=True):
        # Increment running idx
        # Allocate positive values for early indication, negative for late indictation
        ret = self.indication_running_idx if early else -self.indication_running_idx
        self.indication_running_idx +=1
        return ret
    
    def set_processing_status(self, start_stop, indication_idx, reason=None):
        if hasattr(self.md_module, 'gui') and self.md_module.gui is not None:
            if self.debug:
                print('EventIndication Debug[%d, %s]: setting process value:%r. current tirggers before op:' % \
                    (self.md_module.global_frame_num, reason, start_stop), self.curr_active_process_tirggers, flush=True)
            if start_stop:
                if len(self.curr_active_process_tirggers)==0:
                    self.md_module.gui.set_processing_status('process_event', True)
                    if self.debug:
                        print('EventIndication Debug[%d]: setting UI processing: True' % self.md_module.global_frame_num, flush=True)
                if indication_idx not in self.curr_active_process_tirggers:
                    self.curr_active_process_tirggers.append(indication_idx)
            else:
                if indication_idx in self.curr_active_process_tirggers:
                    idx_loc = self.curr_active_process_tirggers.index(indication_idx)
                    self.curr_active_process_tirggers.pop(idx_loc)
                if len(self.curr_active_process_tirggers)==0:
                    self.md_module.gui.set_processing_status('process_event', False)
                    if self.debug:
                        print('EventIndication Debug[%d]: setting UI processing: False' % self.md_module.global_frame_num, flush=True)
            if self.debug:
                print('EventIndication Debug[%d, %s]: current tirggers after op:' % \
                    (self.md_module.global_frame_num, reason), self.curr_active_process_tirggers, flush=True)
    
    def should_send_event_indication(self):
        if not hasattr(self.md_module, 'global_frame_num') or not hasattr(self.md_module, 'mv_manager'):
            return False, None
        if self.params.require_event_detector_signal and not hasattr(self.md_module, 'cart_bottom_detector'):
            return False, None
        # Don't send indications during barcode scanning
        if hasattr(self.md_module, 'barcode_handler') and not self.md_module.barcode_handler.is_idle():
            return False, None
        # Don't send two consequtive indications
        if self.md_module.global_frame_num - self.last_indication_frame < self.params.min_frames_between_indications:
            return False, None
        # Check for valid Movement vector
        valid_mv, cam_name, mv_id = self.md_module.mv_manager.has_active_event()
        if not valid_mv:
            return False, None
        # Ignore if it's still the same mv triggered the last indication
        if self.last_triggering_mv is not None:
            if self.last_triggering_mv['cam_name']==cam_name and self.last_triggering_mv['mv_id']==mv_id:
                return False, None
        # Check event detector
        if self.params.require_event_detector_signal and not self.md_module.cart_bottom_detector.is_during_event():
            return False, None
        # Check valid indication allowed
        event_direction = self.md_module.mv_manager.get_mv_by_id(cam_name, mv_id).event_direction
        event_type = IndicationEvent.EarlyEventIndicationIn if event_direction==EventDirection.In \
            else IndicationEvent.EarlyEventIndicationOut
        if allow_indication(event_type) is None:
            return False, None
        
        # Valid Event
        self.last_indication_frame = self.md_module.global_frame_num
        self.last_triggering_mv = dict(cam_name=cam_name, mv_id=mv_id)
        print('EventIndicator[Frame %d]: Sending Early indication. Event Direction: %s' % (self.md_module.global_frame_num, event_direction), flush=True)
        # Allocate new indication index
        indication_idx = self.alloc_new_idx()
        # Mark mv as indicated to avoid double indication
        self.md_module.mv_manager.set_mv_attr(cam_name, mv_id, 'early_indication_idx', indication_idx)
        self.set_processing_status(True, indication_idx, reason='event_ind_early_event')
        return True, event_direction


        