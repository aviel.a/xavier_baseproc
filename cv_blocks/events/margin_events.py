import numpy as np
from cv_blocks.cart_net_classifier.cart_classifier import CartInOutDecider
from config import config

class MarginEventDetector(object):
    '''
    A class responsible to detect probable made-up events
    '''
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.min_num_detections = config.get('min_num_detections', 3) # dummy
            self.min_mv_size = config.get('min_mv_size', 7)
            self.short_mv_size = config.get('short_mv_size', 8)
            self.short_mv_size_small_item = config.get('short_mv_size_small_item', 6)
            self.short_mv_n_track = config.get('short_mv_n_track', 2)
            self.confident_det_th = config.get('confident_det_th', 0.7)
            self.non_conf_det_th = config.get('non_conf_det_th', 0.5)
            self.conseq_non_conf_det_th = config.get('conseq_non_conf_det_th', 0.52)
            self.small_conseq_non_conf_det_th = config.get('small_conseq_non_conf_det_th', 0.45)
            self.x_edge_th = config.get('x_edge_th', 0.25)

    def __init__(self, md_module, med_config=dict()):
        self.params = self.Config(med_config)
        self.md_module = md_module
        self.use_in_out_decider = config.EN_IN_OUT_CLASSIFIER
        if self.use_in_out_decider:
            self.in_out_decider = CartInOutDecider()

    def set_cart_calib(self, cart_calib):
        self.cart_calib = cart_calib

    def is_valid_before_classification(self, seq):
        # Event detected by cb only - event is valid if at least 1 cb crop is predicted 'in'
        if seq['activated_by_cart_bottom'] and not seq['activated_by_md']:
            cb = [el for el in seq['movement_vector'] if el.source=='CB']
            for cb_el in cb:
                if self.in_out_decider.is_cb_inside_cart([cb_el]):
                    return True
            return False
        # logic for events activated by md by both cams
        elif seq['both_cams_saw_event']:
            assert seq['activated_by_md'], seq
            if not self.use_in_out_decider or seq['activated_by_cart_bottom']:# or not self.should_check_multi_cam(seq):
                return True
            back_mv = [el for el in seq['movement_vector'] if el.source=='MD' and el.cam_name=='back']
            front_mv = [el for el in seq['movement_vector'] if el.source=='MD' and el.cam_name=='front']
            if len(back_mv) > 0:
                back_out_by_cls = len(back_mv) == 0 or not self.in_out_decider.is_inside_cart(back_mv, 'back')
                back_out_by_decision_line = len(back_mv) == 0 or self.bottom_above_decision_line(back_mv, 'back')
                back_outside = back_out_by_cls or back_out_by_decision_line
            else:
                back_out_by_cls = True
                back_outside = True
            if len(front_mv) > 0:
                front_out_by_cls = len(front_mv) == 0 or not self.in_out_decider.is_inside_cart(front_mv, 'front')
                front_out_by_decision_line = len(front_mv) == 0 or self.bottom_above_decision_line(front_mv, 'front')
                front_outside = front_out_by_cls or front_out_by_decision_line
            else:
                front_out_by_cls = True
                front_outside = True
            if not back_outside and not front_outside:
                return True
            if (back_outside and front_outside) and not (not back_out_by_cls and not front_out_by_cls):
                return False
            main_cam = seq['initiating_cam']
            main_mv = [el for el in seq['movement_vector'] if el.source == 'MD' and el.cam_name == main_cam]
            # Handle edge case where main_mv is much shorter than other mv
            if len(main_mv) < len(seq['movement_vector']) // 3:
                main_mv = [el for el in seq['movement_vector'] if el.source == 'MD' and el.cam_name != main_cam]

            seq_size = len(main_mv)
        # logic for events activated by md by 1 camera only
        else:
            # Candidates with 2 cart bottom images are always valid
            cb = [el for el in seq['movement_vector'] if el.source=='CB']
            if len(cb) > 1:
                return True
            elif len(cb) == 1:
                if self.use_in_out_decider:
                    if self.in_out_decider.is_cb_inside_cart(cb):
                        return True
                    elif not seq['activated_by_md']:
                        return False
            main_cam = seq['initiating_cam']
            main_mv = [el for el in seq['movement_vector'] if el.source=='MD' and el.cam_name==main_cam]

            seq_size = len(main_mv)
            if seq_size == 0:
                if config.DETECTOR_V2:
                    other_mv = [el for el in seq['movement_vector'] if el.source == 'MD' and el.cam_name != main_cam]
                    if len(other_mv) > 8:
                        main_mv = other_mv
                        seq_size = len(main_mv)
                    else:
                        return False
                else:
                    return False

            # Check if event is outside of cart
            if self.use_in_out_decider:
                if not self.in_out_decider.is_inside_cart(main_mv, main_cam):
                    return False

        # Skip the rest of the checks if item is small and event candidate exists
        if 'small_item_candidate' in seq.keys() and seq['small_item_candidate'] and hasattr(self.md_module, 'cart_bottom_detector'):
            if self.md_module.cart_bottom_detector.event_candidate_matched(seq['event_frame']):
                return True

        conf_det = [el.detector_conf for el in main_mv]

        # Prune short MVs with low confidence
        short_mv_size_th = self.params.short_mv_size_small_item if ('small_item_candidate' in seq.keys() and seq['small_item_candidate']) or \
            ('thrown_item_candidate' in seq.keys() and seq['thrown_item_candidate']) \
            else self.params.short_mv_size
        conseq_non_conf_det_th = self.params.small_conseq_non_conf_det_th if ('small_item_candidate' in seq.keys() and seq['small_item_candidate']) or \
            ('thrown_item_candidate' in seq.keys() and seq['thrown_item_candidate']) \
            else self.params.conseq_non_conf_det_th
        if seq_size <= short_mv_size_th:
            non_conf_det = [el for el in conf_det if el < self.params.non_conf_det_th]
            n_track = len([1 for el in conf_det if el < 0])
            # More than half is low conf + 2 or more are tracked
            if len(non_conf_det) >= seq_size / 2. and n_track >= self.params.short_mv_n_track:
                return False
            is_low_conf = [el < conseq_non_conf_det_th for el in conf_det]
            idx_pairs = np.where(np.diff(np.hstack(([False],is_low_conf,[False]))))[0].reshape(-1,2)
            if len(idx_pairs) >= 1:
                max_low_conf_seq_size = np.diff(idx_pairs,axis=1).max()
                # Longest consequtive series of low conf. is at least half of the MV
                if max_low_conf_seq_size >= seq_size / 2.:
                    return False

        # Skip the rest of the checks if item is small
        if 'small_item_candidate' in seq.keys() and seq['small_item_candidate'] or 'thrown_item_candidate' in seq.keys() and seq['thrown_item_candidate']:
            return True

        # Detect large jump of non-confident detections
        print("seq size: {}, mv size: {}, both saw event: {}, md event: {}, cb event: {}".format(
            seq_size, len(seq['movement_vector']),seq['both_cams_saw_event'], seq['activated_by_md'], seq['activated_by_cart_bottom']), flush=True)
        if seq_size < 2:
            return False
        seq_start_frame = main_mv[0].global_frame_number
        dts = [el.global_frame_number - main_mv[idx].global_frame_number for idx, el in enumerate(main_mv[1:])]
        h_ratio_vec = [el.coords.h() / main_mv[idx].coords.h() for idx, el in enumerate(main_mv[1:])]
        w_ratio_vec = [el.coords.w() / main_mv[idx].coords.w() for idx, el in enumerate(main_mv[1:])]
        y1 = [el.coords.y1 for el in main_mv]
        y2 = [el.coords.y2 for el in main_mv]
        dy1_vec = [(c2-c1)/dt for dt, c2, c1 in zip(dts, y1[1:], y1[:-1])]
        dy2_vec = [(c2-c1)/dt for dt, c2, c1 in zip(dts, y2[1:], y2[:-1])]
        for dy1, dy2, det1, det2,hr, wr in zip (dy1_vec, dy2_vec, conf_det[1:], conf_det[:-1], h_ratio_vec, w_ratio_vec):
            inconsistent_ratio = max(max(hr, 1./hr), max(wr, 1./wr)) > 2.
            large_vertical_jump = max(abs(dy1), abs(dy2)) > 0.25
            low_conf = max(det1, det2) < 0.55
            if inconsistent_ratio and large_vertical_jump and low_conf:
                return False

        # long MVs and confident detections are valid
        conf_det = [el.detector_conf for el in main_mv if el.detector_conf > self.params.confident_det_th]
        if len(conf_det) > 0 or seq_size >= self.params.min_mv_size:
            return True
        return False

    def bottom_above_decision_line(self, mv, cam):
        y_vals = np.array([e.coords.y1 for e in mv])
        bottom_front_crop = mv[np.argmax(y_vals)]
        return bottom_front_crop.coords.centroid()[1] < self.cart_calib.get_plane_line(cam, bottom_front_crop.depth)
    
    def should_check_multi_cam(self, seq):
        # Check bottom front crop
        mv_front = [e for e in seq['movement_vector'] if e.cam_name=='front' and e.source=='MD']
        mv_back = [e for e in seq['movement_vector'] if e.cam_name=='back' and e.source=='MD']
        if len(mv_front)==0 or len(mv_front)==0:
            return True
        y_vals = np.array([e.coords.y1 for e in mv_front])
        bottom_front_crop = mv_front[np.argmax(y_vals)]
        is_bottom_front_inside = bottom_front_crop.coords.centroid()[1] > self.cart_calib.get_plane_line('front', bottom_front_crop.depth)
        if not is_bottom_front_inside:
            return True
        # Check bottom back crop
        y_vals = np.array([e.coords.y1 for e in mv_back])
        bottom_back_crop = mv_back[np.argmax(y_vals)]
        is_bottom_back_inside = bottom_back_crop.coords.centroid()[1] > self.cart_calib.get_plane_line('back', bottom_back_crop.depth)
        if not is_bottom_back_inside:
            return True
        
        # Test crops that are on the edge of the image in both images
        edge1 = bottom_back_crop.coords.centroid()[0] < self.params.x_edge_th and bottom_front_crop.coords.centroid()[0] > 1 - self.params.x_edge_th
        edge2 = bottom_front_crop.coords.centroid()[0] < self.params.x_edge_th and bottom_back_crop.coords.centroid()[0] > 1 - self.params.x_edge_th
        return edge1 or edge2

    
    def is_valid_after_classification(self):
        pass