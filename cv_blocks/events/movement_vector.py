from config import config
import math
import numpy as np
import enum
from cv_blocks.common.types import BoundingBox2D

class MVState(enum.Enum):
    Empty = -1
    Active = 1
    ActiveConnected = 2
    InactiveConnected = 3 # Don't remove until connected mvs are removed
    Inactive = 4
    Event = 5
    EventConnected = 6
    ForceRemove = 7
    ForceEvent = 8

class MVEvent(enum.Enum):
    Empty = -1
    NotTracked = 1 # not actively tracked anymore
    StaticAfterEvent = 2 # event occured & static
    ActivelyConnected = 3 # initiated connection to different cam mv
    PassivelyConnected = 4 # other cam mv initiated connection
    CutMvTail = 5 # clean history due to static object, actively tracked
    SentByOther = 6 # mv will be sent to event detector because connected mv is an event

class EventDirection(enum.Enum):
    Empty = -1
    In = 1
    Out = 2

class MovementVector:

    def __init__(self, id, pos_coordinates,cam_type, camera_config, depth=None, debug=False, logger=None,
                 load_from_logger=False, detector_class='product', after_hand=False):
        if load_from_logger:
            return
        # coordinates sent: (xmin, ymin, xmax, ymax)
        self.id = id
        self.detector_class = detector_class
        self.init_frame = None
        self.mv_element_list = []
        self.camera_type = cam_type
        self.camera_config = camera_config
        self.decision_line_value = self.determine_decision_line(depth)
        self.init_decision_line = self.decision_line_value
        self.init_location = self.initialize_location(pos_coordinates)
        self.init_coords = pos_coordinates
        self.current_location = self.init_location
        self.event_occurred = False
        self.event_direction = EventDirection.Empty
        self.first_direction = EventDirection.Empty
        self.event_frame = None
        self.state = MVState.Active
        self.remove_counter = None
        self.debug = debug
        self.send_when_done = False
        self.is_idle = False
        self.during_barcode_scan = False
        self.activated_by_cart_bottom = False
        # TODO - get as params
        self.event_frames_before_remove = 10
        self.no_event_frames_before_remove = 20
        self.logger = logger
        self.stall_event_due_to_near_object = False
        self.early_indication_idx = None
        self.frame_history = []
        self.pile_candidate = False
        self.small_item_candidate = False
        self.thrown_item_candidate = False
        self.came_after_hand = after_hand
        self.det_conf = []
        self.class_info = []
        self.prod_class_conf = []
        self.location_info = []
        self.state_info = []

    def __del__(self):
        if hasattr(self, 'logger') and self.logger is not None and self.logger.should_log('movement_vector_delete'):
            # Log movement vectors that are potentially missed events
            if self.state in (MVState.InactiveConnected, MVState.Inactive) and len(self.mv_element_list) > 4:
                self.logger.log(self.pack(), 'movement_vector_delete')
    
    def pack(self):
        ret_dict = dict()
        for k, val in vars(self).items():
            if k in ('logger'): continue
            if k=='mv_element_list':
                ret_dict[k] = [v.pack() for v in val]
            else:
                ret_dict[k] = val

        return ret_dict
    
    @staticmethod
    def unpack(data):
        obj = MovementVector(None, None, None, None, load_from_logger=True)
        for k, val in data.items():
            if k in ('logger'): continue
            if k=='mv_element_list':
                mv_list = [MVElement.unpack(v) for v in val]
                setattr(obj, k, mv_list)
            else:
                setattr(obj, k, val)
        return obj
    
    def get_next_state(self, event):
        '''
        Logic defining transition between states according to messages
        '''
        assert isinstance(event, MVEvent)
        if self.state in (MVState.ForceRemove, MVState.ForceEvent):
            return self.state
        next_state = self.state
        if self.state==MVState.EventConnected:
            if self.event_occurred:
                return self.state
            next_state = MVState.ActiveConnected
        elif event==MVEvent.NotTracked:
            assert self.state in (MVState.Inactive, MVState.Active, MVState.ActiveConnected, MVState.InactiveConnected)
            if self.state==MVState.Active:
                next_state = MVState.Event if self.event_occurred else MVState.Inactive
            elif self.state==MVState.ActiveConnected:
                next_state = MVState.EventConnected if self.event_occurred else MVState.InactiveConnected
        elif event==MVEvent.StaticAfterEvent:
            assert self.state in (MVState.Active, MVState.ActiveConnected)
            if self.state==MVState.Active:
                next_state = MVState.Event
            else:
                next_state = MVState.EventConnected
        elif event==MVEvent.ActivelyConnected:
            assert self.state in (MVState.Active, MVState.ActiveConnected)
            next_state = MVState.ActiveConnected
        elif event==MVEvent.PassivelyConnected:
            assert self.state in (MVState.Active, MVState.ActiveConnected, MVState.InactiveConnected)
            if self.state!=MVState.InactiveConnected:
                next_state = MVState.ActiveConnected
        elif event==MVEvent.CutMvTail:
            assert self.state in (MVState.Active, MVState.ActiveConnected)
            next_state = self.state
        elif event==MVEvent.SentByOther:
            next_state = MVState.EventConnected if self.state==MVState.ActiveConnected and self.event_occurred \
                else self.state
        if self.debug:
            print('MVV DEBUG: MV[%s, %d, Frame:%d, event=%r] state transition: (%s,-> %s) with message %s' \
                % (self.camera_type, self.id, self.tail().global_frame_number, self.event_occurred, self.state, next_state, event))

        return next_state
    
    def update_remove_counter(self, force_wait=False):
        if self.state not in (MVState.Inactive, MVState.InactiveConnected, MVState.EventConnected):
            self.remove_counter = self.event_frames_before_remove if self.event_occurred\
                else self.no_event_frames_before_remove
        else:
            self.remove_counter = 0 if ((self.send_when_done and self.state==MVState.EventConnected) and not force_wait) else self.remove_counter - 1

    def step(self, next_state, message):
        remove_mv = False
        idle_mv = False
        update_tracker = False
        if next_state==MVState.InactiveConnected:
            if message not in (MVEvent.ActivelyConnected, MVEvent.PassivelyConnected):
                self.update_remove_counter()
                remove_mv = self.remove_counter <= 0
        elif next_state==MVState.Inactive:
            self.update_remove_counter(force_wait=True)
            remove_mv = self.remove_counter <= 0
            idle_mv = not remove_mv and (not self.is_idle)
            update_tracker = True
            # remove_mv = True
        elif next_state==MVState.Event:
            remove_mv = True 
            update_tracker = True
        elif next_state==MVState.EventConnected:
            if message not in (MVEvent.ActivelyConnected, MVEvent.PassivelyConnected):
                self.update_remove_counter()
                remove_mv = (self.send_when_done or self.remove_counter==0) and \
                            (self.current_location != 'middle' or message == MVEvent.NotTracked)
                update_tracker = remove_mv
                idle_mv = not remove_mv and (not self.is_idle)
        elif next_state==MVState.Active:
            if message==MVEvent.CutMvTail:
                self.empty_mv_tail()
        elif next_state==MVState.ActiveConnected:
            if message==MVEvent.CutMvTail:
                self.empty_mv_tail()
        elif next_state==MVState.ForceRemove:
            remove_mv = True 
            update_tracker = True

        # Propagate state
        self.state = next_state
        if self.debug and self.remove_counter is not None:
            print('MVV DEBUG: MV[%s, %d, %s]: frames until removal: %d' %\
                (self.camera_type, self.id, self.state, self.remove_counter))
        return remove_mv, idle_mv, update_tracker


    def empty_mv_tail(self, n_keep=3):
        # save frame history for later book-keeping
        self.frame_history += [dict(frame=me.global_frame_number, cam=self.camera_type) \
            for me in self.mv_element_list]
        self.frame_history = self.frame_history[-20:]
        self.mv_element_list = self.mv_element_list[-n_keep:]
    
    def size(self):
        return len(self.mv_element_list)
    
    def force_remove(self):
        self.state = MVState.ForceRemove
    
    def __repr__(self):
        mv_el_str = ', '.join([el.__repr__() for el in self.mv_element_list])
        ret = 'Id(%s)=%d, Size = %d, Is_Event = %r, Elements:\n'\
            % (self.camera_type, self.id, len(self.mv_element_list), self.event_occurred) + mv_el_str
        return ret
    
    def tail(self):
        return self.mv_element_list[-1]
    
    def last_detection(self):
        for mv_el in reversed(self.mv_element_list[-3:]):
            if mv_el.coords.score > 0:
                return mv_el
        
    def head(self):
        return self.mv_element_list[0]
    
    def bottom(self, detection_th=-1, first_n=None, last_n=None):
        ret_el = 0 # dummy, for valid output
        bott_val = -1.
        for el_idx, el in enumerate(self.mv_element_list):
            if first_n is not None and el_idx>=first_n:
                break
            elif last_n is not None and el_idx <= self.size() - last_n:
                continue
            if el.detector_conf < detection_th:
                continue
            val = el.coords.centroid()[1]
            if val > bott_val:
                ret_el = el_idx
                bott_val = val

        return self.mv_element_list[ret_el]

    def evaluate_direction(self):
        if self.event_direction != EventDirection.Empty:
            return 'in' if self.event_direction==EventDirection.In else 'out'
        if len(self.mv_element_list) == 1:
            return None
        delta_y = self.mv_element_list[-1].coords.centroid()[1] - self.init_coords.centroid()[1]
        return 'in' if delta_y > 0.2 else 'out' if delta_y < -0.2 else None

    def determine_decision_line(self, depth, spread_disp_factor=0.2):
        """
        Fixed decision line boundary for back cam, continuous values for front cam.

        Formula for continuous values goes as 1/disp:
        top_dl + (bot_dl - top_dl) * (1 / disp_far_th - 1 / disp) / (1 / disp_far_th - 1/ disp_near_th)

        Disparity thresholds are changed to reflect better the whole cart volume.
        """
        if self.camera_type == 'front':
            if math.isnan(depth):
                return self.camera_config['decision_lines']['top']
            else:
                dl = self.camera_config['decision_lines']
                disp = self.camera_config['decision_disparity']
                disp_far_th = (1 - spread_disp_factor) * disp['far']
                disp_near_th = (1 + spread_disp_factor) * disp['near']
                return dl['top'] + (dl['bottom'] - dl['top']) * \
                    np.clip((1 / disp_far_th - 1 / depth) / (1 / disp_far_th - 1 / disp_near_th), 0, 1)
        else:
            return self.camera_config['decision_lines']['middle']

    def add_element(self, item_content, classify=True, attributes=None):
        self.mv_element_list.append(MVElement(item_content, self.camera_type, classify=classify, attributes=attributes))
        if self.mv_element_list[-1].coords.score != -1:
            self.det_conf.append(self.mv_element_list[-1].detector_conf)
            self.class_info.append(self.mv_element_list[-1].crop_type)
            self.prod_class_conf.append(self.mv_element_list[-1].prod_conf)
            self.location_info.append(self.mv_element_list[-1].crop_location)
            self.state_info.append(self.mv_element_list[-1].crop_location)
        if self.init_frame is None:
            self.init_frame = self.head().global_frame_number
        if 'basler' in config.CAMERA_ID and self.camera_type == 'front': # update decision boundry if camera is basler and front
            if not math.isnan(item_content[5]): # "nan" is where all pixels in the cropped disparity are 0. don't change decision boundry in this case
                self.decision_line_value = self.determine_decision_line(item_content[5])
        self.current_location = self.calculate_current_location(item_content[0])
        cur_dis_from_line = self.mv_element_list[-1].coords.centroid()[1] - self.decision_line_value
        init_dis_from_line = self.init_coords.centroid()[1] - self.init_decision_line
        most_bbox_crossed_line_in = item_content[0].y2 - self.decision_line_value > 0.70 * (
                    item_content[0].y2 - item_content[0].y1)
        most_bbox_crossed_line_out = self.decision_line_value - item_content[0].y1 > 0.70 * (
                    item_content[0].y2 - item_content[0].y1)
        total_y_change = self.mv_element_list[-1].coords.centroid()[1] - self.init_coords.centroid()[1]
        if (self.init_coords.y2 - self.init_decision_line < 0.2 and
            self.decision_line_value - self.mv_element_list[-1].coords.y1 < 0.2) and\
                ((self.init_location == 'up' and most_bbox_crossed_line_in) or\
                 (total_y_change > 0.25 and ((cur_dis_from_line + 0.1 > 0 and self.init_location == 'up') or
                                             (cur_dis_from_line > 0 and init_dis_from_line - 0.1 < 0)))):
            if self.first_direction in (EventDirection.Empty, EventDirection.In):
                self.event_occurred = True
                self.event_direction = EventDirection.In
                if self.first_direction==EventDirection.Empty and self.init_location == 'up':
                    self.first_direction = EventDirection.In
        elif (self.init_coords.y1 - self.init_decision_line > -0.2 and
              self.decision_line_value - self.mv_element_list[-1].coords.y2 > -0.2) and\
                ((self.init_location == 'down' and most_bbox_crossed_line_out) or
                 (total_y_change < -0.25 and ((cur_dis_from_line - 0.1 < 0 and self.init_location == 'down') or
                                              (cur_dis_from_line < 0 and init_dis_from_line + 0.1 > 0)))):
            if self.first_direction in (EventDirection.Empty, EventDirection.Out):
                self.event_occurred = True
                self.event_direction = EventDirection.Out
                if self.first_direction==EventDirection.Empty and self.init_location == 'down':
                    self.first_direction = EventDirection.Out
        else:
            self.event_occurred = False
            self.event_direction = EventDirection.Empty
        if self.event_occurred and (self.event_frame is None):
            event_frame = item_content[3]
            self.event_frame = event_frame
        elif not self.event_occurred:
            self.event_frame = None
        
        if self.debug:
            print('Element added to MV[%s, %d]. init_loc=%s, first_dir=%s, event_dir=%s, is event=%r' % \
                 (self.camera_type, self.id, self.init_location, self.first_direction, self.event_direction, self.event_occurred))

    def can_connect_box(self, bbox, att_info):
        if not config.DETECTOR_V2 or bbox.score == -1:
            return True, None

        # Don't block short events
        if len(self.location_info) < 2:
            return True, None

        if 'crop_location' not in att_info.keys() or 'hand' not in att_info['crop_type']:
            return True, None

        class_info = bbox.get_top_attr('crop_type', att_info)
        location_info = bbox.get_top_attr('crop_location', att_info)
        state_info = bbox.get_top_attr('crop_state', att_info)

        # Reject connections of detection[-1]->tracked[0]->detection[1] if x[-1] type ! x[1] type 
        if len(self.mv_element_list)> 1 and self.mv_element_list[-1].detector_conf==-1:
            if self.mv_element_list[-2].crop_type is not None:
                if class_info['name']=='product' and class_info['conf']>0.95 and \
                    self.mv_element_list[-2].crop_type['name']=='hand' and self.mv_element_list[-2].crop_type['conf']>0.95:
                    return False, True

        # print("id: {}, type: {}, loc: {}, state: {}".format(self.id, class_info, location_info, state_info), flush=True)
        track_going_in = self.location_info[0]['name'] in ('outside-cart', 'on-cart-edge') and \
                         self.location_info[-1]['name'] in ('inside-cart', 'on-cart-bottom')
        cls_names = [ci['name'] for ci in self.class_info]
        is_product_ratio = float(sum([1 for cn in cls_names if cn == 'product'])) / len(cls_names)
        is_hand_ratio = float(sum([1 for cn in cls_names if cn == 'hand'])) / len(cls_names)
        consecutive_hands = 0
        for cn, pcc in zip(cls_names[::-1], self.prod_class_conf[::-1]):
            if 'hand' in cn and pcc < 0.3:
                consecutive_hands += 1
            else:
                break
        track_going_in = self.location_info[0]['name'] in ('outside-cart', 'on-cart-edge') and \
                         (self.location_info[-1]['name'] in ('inside-cart', 'on-cart-bottom') or \
                          self.location_info[-2]['name'] in ('inside-cart', 'on-cart-bottom') or \
                          location_info['name'] in ('inside-cart', 'on-cart-bottom'))
        loc_is_cb = [ci['name'] == 'on-cart-bottom' for ci in self.location_info]
        loc_is_in = [ci['name'] == 'inside-cart' for ci in self.location_info]
        # Disable transition from product going in into hand going out
        product_in_cb_k = 2
        last_k_on_cart_bottom_product = all(loc_is_cb[-product_in_cb_k:])
        if is_product_ratio > 0.7 and track_going_in and last_k_on_cart_bottom_product and \
                class_info['name'] == 'hand' and location_info['name'] in ('on-cart-bottom', 'inside-cart'):
            print('Rejected box connection: product in -> hand')
            return False, False
        mv_was_on_cb = sum(loc_is_cb) >= 1
        mv_was_in = sum(loc_is_in) >= 2
        if is_product_ratio > 0.7 and track_going_in and (mv_was_on_cb or mv_was_in) and \
                class_info['name'] == 'hand' and location_info['name'] in ('on-cart-edge', 'outside-cart'):
            print('Rejected box connection: product in -> hand(already out)')
            return False, False
        # Disable transition from hand going in into product going out,
        #  when hand is detected only in the way out
        confident_hand_out = False
        out_locations = ('outside-cart', 'on-cart-edge') if self.location_info[0]['name'] == 'on-cart-edge' else ('outside-cart',)
        for idx in range(-1, consecutive_hands * (-1) - 1, -1):
            if self.class_info[idx]['name'] == 'hand' and self.class_info[idx]['conf'] > 0.9 and \
                    self.det_conf[idx] > 0.9 and self.location_info[idx]['name'] in out_locations:
                confident_hand_out = True
                break
        if (is_hand_ratio > 0.7 or consecutive_hands >= 3 or confident_hand_out) and track_going_in and \
                (class_info['name'] == 'product' and location_info['name'] in ('inside-cart','on-cart-bottom')):
            print('Rejected box connection: hand in -> product')
            return False, True
        # Disable connection of confident hand to product inside the cart
        confident_hand = class_info['name'] == 'hand' and class_info['conf'] > 0.9 and bbox.score > 0.9
        if cls_names[-1] == 'product' and self.location_info[-1]['name'] in ('inside-cart', 'on-cart-bottom') and \
                confident_hand:
            print('Rejected box connection: product in cart -> confident hand')
            return False, False
        # Disable connection of non-confident hand/hand holding product to large&confident product inside the cart
        confident_product_in_cart = class_info['name'] == 'product' and class_info['conf'] > 0.9 and bbox.score > 0.9 and \
            self.location_info[-1]['name'] != 'outside-cart'
        scale_ratio = self.mv_element_list[-1].coords.area() / (bbox.area() + 1e-6)
        large_scale_diff = min(scale_ratio, 1 / scale_ratio) < 0.25
        if confident_product_in_cart and large_scale_diff and cls_names[-1] in ('hand', 'hand-holding-product'):
            print('Rejected box connection: hand-> product in cart')
            return False, False

        return True, None


    def valid_event(self):
        last_box = self.mv_element_list[-1].coords
        if (self.event_direction == EventDirection.Out and last_box.y2 < self.decision_line_value) or \
                (self.event_direction == EventDirection.In and last_box.y1 > self.decision_line_value):
            return True
        return False

    def initialize_location(self, pos_coordinates):
        if pos_coordinates.centroid()[1] > self.decision_line_value:
            return "down"
        return "up"

    def calculate_current_location(self, pos_coordinates):
        if pos_coordinates.y1 > self.decision_line_value:
            return "down"
        if pos_coordinates.y2 < self.decision_line_value:
            return "up"
        return "middle"

    def visualize(self, path=None, prefix='movement_vector_delete'):
        if path is not None:
            import os
            mv_path = os.path.join(path, prefix+'_%02d_%s' % (self.id, self.camera_type))
            os.makedirs(mv_path, exist_ok=True)
        else:
            mv_path = None
        for img_idx, img_el in enumerate(self.mv_element_list):
            img_path = mv_path + '/img_%02d.png' % img_idx if path is not None else None
            img_el.visualize(path=img_path)

class MVElement:

    def __init__(self, item_content, cam_name, classify=True, source='MD', cls_weight=1.0, load_from_logger=False,
                 attributes=None):
        if load_from_logger:
            return
        coords, cropped_image, frame_since_last_event, global_frame_number, \
            dataset_image, depth, wnoise_ratio, detector_conf, timestamp, boxes_summary, event_detector_score, \
            context_image, global_image = item_content
        self.coords = coords
        self.attributes = coords.attributes
        self.crop_type = coords.get_top_attr('crop_type', attributes)
        self.prod_conf = coords.get_attr_conf('crop_type', attributes).get('product', None)
        self.crop_location = coords.get_top_attr('crop_location', attributes)
        self.crop_state = coords.get_top_attr('crop_state', attributes)
        self.cropped_image = cropped_image
        self.frame_since_last_event = frame_since_last_event
        self.global_frame_number = global_frame_number
        self.dataset_image = dataset_image
        self.depth = depth
        self.wnoise_ratio = wnoise_ratio
        self.detector_conf = detector_conf
        self.cam_name = cam_name
        self.classify = classify
        self.source = source
        self.cls_weight = cls_weight
        self.timestamp = timestamp
        self.boxes_summary = boxes_summary
        self.event_detector_score = event_detector_score
        self.context_image = context_image
        self.global_image = global_image
    
    def __repr__(self):
        return '[Frame:%d(%s) , (%.2f, %.2f, %.2f, %.2f), conf=%.2f, ed_conf=%.2f, classify=%r, type=%s, state=%s, location=%s]'\
             % (self.global_frame_number, self.cam_name, self.coords.x1, self.coords.y1, self.coords.x2,
                self.coords.y2, self.detector_conf, self.event_detector_score, self.classify, self.crop_type,
                self.crop_state, self.crop_location)

    def to_json(self):
        pass

    def pack(self):
        return vars(self)
    
    @staticmethod
    def unpack(data):
        obj = MVElement(None, None, load_from_logger=True)
        for k, val in data.items():
            setattr(obj, k, val)
        return obj
    
    def visualize(self, path=None, extra_txt=None):
        h,w  = self.cropped_image.shape[:2]
        from matplotlib import pyplot as plt
        has_global_img = hasattr(self, 'global_image') and self.global_image is not None
        if has_global_img:
            fig = plt.figure(figsize=(16, 8))
            ax1 = fig.add_axes([0., 0., 0.5, 0.8, ])
            ax2 = fig.add_axes([0.5, 0., 0.5, 0.8, ])
            txt = 'Image from frame % d. ts=%.3f' % (self.global_frame_number, self.timestamp)
            if extra_txt is not None:
                txt += extra_txt
            fig.suptitle(txt)
            ax1.imshow(self.cropped_image)
            ax1.set_title('Cropped Image(%s, (%dx%d)), conf=%.2f, source:%s' %\
                (self.cam_name, h, w, self.detector_conf, self.source))
            ax1.set_axis_off()
            ax2.imshow(self.global_image, cmap='gray')
            ax2.set_axis_off()
        else:
            fig = plt.figure(figsize=(8, 8))
            ax = fig.add_axes([0., 0., 1., 0.8, ])
            txt = 'Image from frame % d. ts=%.3f' % (self.global_frame_number, self.timestamp)
            if extra_txt is not None:
                txt += extra_txt
            fig.suptitle(txt)
            ax.imshow(self.cropped_image)
            ax.set_title('Cropped Image(%s, (%dx%d)), conf=%.2f, source:%s' %\
                (self.cam_name, h, w, self.detector_conf, self.source))
            ax.set_axis_off()
        if path is not None:
            plt.savefig(path)
        else:
            plt.show()
        plt.close()

class QueueForYaaen:
    def __init__(self, frame_number, camera_type):
        self.frame_detected = frame_number
        if camera_type == "back":
            self.back = True
            self.front = False
        else: #front
            self.back = False
            self. front = True
        self.imgs = []

    def add_image(self, image):
        self.imgs.append(image)

    def update_cameras_status(self, camera_type):
        if camera_type == "back":
            self.back = True
        else: #front
            self.front = True

    def check_if_cam_exists(self, camera_type):
        if camera_type == "back":
            return self.back
        return self.front
