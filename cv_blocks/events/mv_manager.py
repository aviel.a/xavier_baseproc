import time
import numpy as np
from .movement_vector import MovementVector, MVState, MVEvent, EventDirection
from config import config # TODO -remove later
from cv_blocks.common.types import BoundingBox2D
from .multi_cam import MvConnectivity
from cv_blocks.cart_content.cart_box_db import CartBoxDB
from md_blocks.barcode.barcode_handler import BarcodeHandlerState

class MotionVecManager(object):
    '''
    A class holding all logic managing motion vectors
    '''
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.rel_y_dist_for_mv_change = config.get('rel_y_dist_for_mv_change', 0.06)
            self.max_multi_cam_frame_diff = config.get('max_multi_cam_frame_diff', 0) # handle front/back miss-alignment
            self.min_multi_cam_box_iou = config.get('min_multi_cam_box_iou', 0.15)
            self.max_multi_cam_box_distance = config.get('max_multi_cam_box_distance', 0.2)
            self.multi_box_iou_ratio = config.get('multi_box_iou_ratio', 0.5)
            self.multi_box_distance_ratio = config.get('multi_box_iou_ratio', 2)
            self.small_bbox_size = config.get('small_bbox_size', 0.03)
            self.min_obs_for_event = config.get('min_obs_for_event', 4)
            self.min_edge_count = config.get('min_edge_count', 3)
            self.min_edge_ratio = config.get('min_edge_ratio', 0.25)
            self.min_edge_to_stall = config.get('min_edge_to_stall', 8)
            self.stall_x_frames = config.get('stall_x_frames', 10)
            self.min_det_after_event = config.get('min_det_after_event', 1)
            self.confident_det_th = config.get('confident_det_th', 0.3)
            self.terminate_event_n_frames_after = config.get('terminate_event_n_frames_after', 10)
            self.ignore_last_tracking_elements = config.get('ignore_last_tracking_elements', True)
            self.activity_frame_margin = config.get('activity_frame_margin', 3)
            self.ignore_static_mv_in_barcode_roi = config.get('ignore_static_mv_in_barcode_roi', True)
            self.min_dist_from_cam_to_stall_event = config.get('min_dist_from_cam_to_stall_event', 0.3)
            self.enable_stall_delay_while_scanning = config.get('enable_stall_delay_while_scanning', 15) 
            self.enable_stall_delay_while_not_scanning = config.get('enable_stall_delay_while_not_scanning', 5) 
            self.old_multi_cam_edge_frame_th = config.get('old_multi_cam_edge_frame_th', 8)
            self.ignore_short_remove_after_scan_for_n_frames = config.get('ignore_short_remove_after_scan_for_n_frames', 30)
            self.ignore_short_insert_after_scan_for_n_frames = config.get('ignore_short_insert_after_scan_for_n_frames', 10)
            self.ignore_short_mv_after_scan_max_len = config.get('ignore_short_mv_after_scan_max_len', 7)
            self.ignore_short_large_mv_after_scan_max_len = config.get('ignore_short_large_mv_after_scan_max_len', 9)
            self.min_frames_passed_for_timeout = config.get('min_frames_passed_for_timeout', 4)
            self.ignore_pile_events = config.get('ignore_pile_events', False)
            self.ignore_small_item_events = config.get('ignore_small_item_events', False)
            self.prod_moving_in_cart_check_n_frames_back = config.get('prod_moving_in_cart_check_n_frames_back', 10)
            self.prod_moving_in_cart_ed_th_dir_in = config.get('prod_moving_in_cart_ed_th_dir_in', 0.97)
            self.prod_moving_in_cart_ed_th_dir_out = config.get('prod_moving_in_cart_ed_th_dir_out', 0.85)
            self.debug = config.get('debug', False)

    def __init__(self, que_names, md_module, mvv_config=dict(), logger=None):
        self.que_names = que_names 
        self.params = self.Config(mvv_config)
        self.vectors = dict()
        self.event_candidate = dict()
        self.event_id = 0
        for n in que_names:
            self.vectors[n] = dict()
            self.event_candidate[n] = []
        self.md_module = md_module
        self.multi_cam_connectivity = MvConnectivity(self.que_names)
        self.cart_box_db = CartBoxDB(que_names)
        self.logger = logger
        self.camera_config = None
        self.stalled_front_ids = dict()
    
    def reset(self):
        self.vectors = dict()
        for n in self.que_names:
            self.vectors[n] = dict()
            self.event_candidate[n] = []
        self.event_id = 0
        self.multi_cam_connectivity.reset()
        self.stalled_front_ids = dict()

    def load_camera_config(self, camera_config):
        self.camera_config = camera_config
    
    def get_mv_by_id(self, cam_name, id):
        assert cam_name in self.que_names
        return self.vectors[cam_name][id]
    
    def get_cam_mvs(self, cam_name):
        assert cam_name in self.que_names
        return self.vectors[cam_name]
    
    def get_event_candidates(self):
        self.event_candidate

    def set_mv_attr(self, cam_name, id, attr, val):
        mv = self.vectors[cam_name][id]
        if attr in dir(mv):
            setattr(mv, attr, val)
    
    def is_active(self):
        for cam_name, cam_mvs in self.vectors.items():
            for mv_id, mv in cam_mvs.items():
                if mv.state in (MVState.Active,
                                MVState.ActiveConnected,
                                MVState.Event,
                                MVState.EventConnected):
                    return True
        return False
    
    def is_near_cart(self, el, margin=0.1):
        '''
        Test if bottom part of the box is still near or below plane
        '''
        if el.depth is None or el.depth < self.camera_config['front']['decision_disparity']['far']:
            plane_line = self.camera_config['front']['decision_lines']['top']
        elif el.depth < self.camera_config['front']['decision_disparity']['near']:
            plane_line = self.camera_config['front']['decision_lines']['middle']
        else:
            plane_line = self.camera_config['front']['decision_lines']['bottom']
        y_bottom = el.coords.y2  # lowest box part
        is_near_plane = y_bottom + margin > plane_line 
        return is_near_plane

    def is_active_near_cart(self):
        for cam_name, cam_mvs in self.vectors.items():
            for mv_id, mv in cam_mvs.items():
                last_el = mv.tail()
                if self.is_near_cart(last_el) and \
                    self.md_module.global_frame_num - self.params.activity_frame_margin < last_el.global_frame_number and \
                    mv.state in (MVState.Active,
                                MVState.ActiveConnected,
                                MVState.Event,
                                MVState.EventConnected): 
                    return True
        return False

    def has_thrown_product(self, conf_th=0.9):
        attributes = self.md_module.detector.attributes
        if 'crop_state' not in attributes.keys() or 'thrown' not in attributes['crop_state']:
            return False
        for cam_name, cam_mvs in self.vectors.items():
            for mv_id, mv in cam_mvs.items():
                last_el = mv.last_detection()
                if last_el is not None and last_el.coords.attributes is not None and \
                    self.md_module.global_frame_num - self.params.activity_frame_margin < last_el.global_frame_number:
                        prob_thrown = last_el.coords.attributes['crop_state'][attributes['crop_state'].index('thrown')]
                        prob_product = last_el.coords.attributes['crop_type'][attributes['crop_type'].index('product')]
                        if prob_thrown > conf_th and prob_product > conf_th:
                            return True
        return False

    def has_active_event(self, min_mv_size=4):
        for cam_name, cam_mvs in self.vectors.items():
            for mv_id, mv in cam_mvs.items():
                last_el = mv.tail()
                active_event = mv.state in (MVState.Active, MVState.ActiveConnected) and mv.event_occurred
                recent_event = mv.state in (MVState.Event, MVState.EventConnected) and \
                    self.md_module.global_frame_num - self.params.activity_frame_margin < last_el.global_frame_number
                if (active_event or recent_event) and mv.size()>=min_mv_size:
                    if config.DETECTOR_V2:
                        last_det = mv.last_detection()
                        if last_det is not None and last_det.crop_type is not None:
                            if last_det.crop_type['name']!='product':
                                continue
                    return True, cam_name, mv_id
        return False, None, None

    def add_to_movement_vectors(self, items_for_mv, cam_name, attributes=None):
        assert cam_name in self.que_names
        mv = self.vectors[cam_name]
        for (item_id, item_content) in items_for_mv:
            if config.DETECTOR_V2 and 'crop_type' in self.md_module.detector.attributes.keys() and \
                    item_content[0].attributes is not None:
                detector_class = self.md_module.detector.attributes['crop_type'][
                    np.argmax(item_content[0].attributes['crop_type'])]
            else:
                detector_class = 'product'
            if not self.check_if_id_exists_in_movement_vectors(item_id, cam_name):
                if self.params.debug:
                    print('MVV DEBUG[Frame %d]: Initializing new MVV[%s, %d], (x,y)=(%.2f, %.2f)' % \
                    (self.md_module.global_frame_num, cam_name, item_id, item_content[0].centroid()[0], item_content[0].centroid()[1]))
                mv[item_id] = MovementVector(id=item_id, pos_coordinates=item_content[0], cam_type= cam_name,
                                             camera_config=self.camera_config[cam_name], depth=item_content[5],
                                             debug=self.params.debug, logger=self.logger, detector_class=detector_class) # make movement vector for the new id
            else:
                can_connect, now_tracking_product = mv[item_id].can_connect_box(item_content[0], attributes)
                if not can_connect:
                    # start new mv and remove / send the other
                    id_to_replace = item_id
                    item_id = self.md_module.object_tracker[cam_name].replace_id(item_id)
                    # make a movement vector for the new id
                    mv[item_id] = MovementVector(id=item_id, pos_coordinates=item_content[0], cam_type= cam_name,
                                                 camera_config=self.camera_config[cam_name], depth=item_content[5],
                                                 debug=self.params.debug, logger=self.logger,
                                                 detector_class=detector_class, after_hand=now_tracking_product)
                    if now_tracking_product:
                        # remove the previous hand mv
                        mv[id_to_replace].state = MVState.ForceRemove
            if mv[item_id].state not in (MVState.InactiveConnected, MVState.ForceRemove):
                 # Don't add elements to event after it's connected to avoid pile confusion
                # classify = not (mv[item_id].state == MVState.EventConnected and mv[item_id].event_direction==EventDirection.In)
                classify = True
                mv[item_id].add_element(item_content, classify=classify, attributes=attributes) #add element to movement vector
                if hasattr(self.md_module, 'barcode_handler'):
                    if mv[item_id].event_occurred and item_id in self.md_module.object_tracker[cam_name].ids_to_force_tracking and \
                        ((self.md_module.barcode_handler.state == BarcodeHandlerState.InsertionPending and mv[item_id].event_direction == EventDirection.In) or \
                        (self.md_module.barcode_handler.state == BarcodeHandlerState.RemovalPending and mv[item_id].event_direction == EventDirection.Out)):
                        self.md_module.object_tracker[cam_name].ids_to_force_tracking.remove(item_id)
                if self.params.debug:
                    print('MVV DEBUG[Frame %d]: Adding new element MVV[%s, %d], (x,y)=(%.2f, %.2f), size=%d' % \
                    (self.md_module.global_frame_num, cam_name, item_id, item_content[0].centroid()[0], item_content[0].centroid()[1], mv[item_id].size()))
                # TODO - move somewhere
                if config.DISPLAY_ALGO_DETAIL or cam_name == 'back':
                    self.md_module.draw_decision_boundry(item_id, mv[item_id].decision_line_value, cam_name,
                                                         detector_class=detector_class)

    def connect_multi_cam_detections(self):
        for cam_name in self.que_names:
            mv = self.vectors[cam_name]
            for (item_id, item_content) in mv.items():
                if item_content.tail().global_frame_number != self.md_module.global_frame_num:
                    continue # handle only recently added frames
                self.calc_mv_connectivity_multi_cam(mv[item_id])

    def check_if_id_exists_in_movement_vectors(self, id, cam_name):
            return id in self.vectors[cam_name].keys()

    def movement_exists_in_movement_vectors(self, cam_name, frame_delta, th):
        assert cam_name in self.que_names
        fh, fw = frame_delta.shape[:2]
        for key, mv in self.vectors[cam_name].items():
            coords = mv.mv_element_list[-1].coords
            coords_nn = coords.renormalize(fh, fw)
            x = int(coords_nn.x1)
            y = int(coords_nn.y1)
            w = int(coords_nn.w())
            h = int(coords_nn.h())
            rect_delta = frame_delta[y:y + h, x:x + w]
            mean_rect_delta = np.mean(rect_delta)
            if mean_rect_delta > th:
                return True
        return False

    def analyze_movement_vectors(self, cam_name, tracker, during_barcode_scan=False):
        assert cam_name in self.que_names
        mv = self.vectors[cam_name]

        update_tracker = False
        ids_to_remove = []
        ids_to_idle = []
        for id_in_mv in mv.keys():
            id_found = id_in_mv in [tr.id for tr in tracker.actively_tracked_objects]
            min_delay_ok = self.md_module.global_frame_num - mv[id_in_mv].init_frame > self.params.min_frames_passed_for_timeout \
                if mv[id_in_mv].init_frame is not None else False
            if not id_found:
                message = MVEvent.NotTracked
                if self.params.debug:
                    print('MVV DEBUG[Frame %d]: Trying to remove MVV[%s, %d]. Reason: Inactive Tracking' % (self.md_module.global_frame_num, cam_name, id_in_mv))
            elif min_delay_ok and mv[id_in_mv].event_occurred and mv[id_in_mv].current_location != "middle" and mv[id_in_mv].valid_event() and \
                    ((self.check_if_bbox_is_static(id_in_mv, mv) and self.valid_event(mv[id_in_mv])) or self.is_event_timeout(id_in_mv, mv)): # static bbox of after timeout - conclude event
                message = MVEvent.StaticAfterEvent
                # tracker.remove_id_from_tracker(id_in_mv)
                if self.params.debug:
                    print('MVV DEBUG[Frame %d]: Trying to remove MVV[%s, %d]. Reason: Event occured & Static Object' % (self.md_module.global_frame_num, cam_name, id_in_mv))
            elif self.check_if_bbox_is_static(id_in_mv, mv) and mv[id_in_mv].current_location != "middle":
                message = MVEvent.CutMvTail
                if self.params.debug:
                    print('MVV DEBUG[Frame %d]: Trimming MVV[%s, %d] tail. Reason: Static Object' % (self.md_module.global_frame_num, cam_name, id_in_mv))
            else:
                message = MVEvent.Empty

            # Keep marginal MV alive
            if message!=MVEvent.Empty and self.is_marginal_mv(mv[id_in_mv]):
                message = MVEvent.Empty

            # Get next state & act according to it
            next_state = mv[id_in_mv].get_next_state(message)
            remove_mv, idle_mv, update_tracker_mv = mv[id_in_mv].step(next_state, message)

            if mv[id_in_mv].state in (MVState.InactiveConnected, MVState.EventConnected):
                other_cam_mv = self.get_other_cam_mv(cam_name, id_in_mv)
                if len(other_cam_mv) > 0:
                    if other_cam_mv[0].state in (MVState.InactiveConnected, MVState.EventConnected):
                        if mv[id_in_mv].state == MVState.EventConnected:
                            mv[id_in_mv].send_when_done = True
                        else:
                            other_cam_mv[0].send_when_done = True
                else:
                    mv[id_in_mv].send_when_done = True

            # Add forced events (e.g, from cart bottom) to clean list
            if mv[id_in_mv].state==MVState.ForceEvent:
                remove_mv = True
                update_tracker = True

            update_tracker = update_tracker or update_tracker_mv
            if remove_mv:
                ids_to_remove.append(id_in_mv)
            elif idle_mv:
                ids_to_idle.append(id_in_mv)


        if self.params.debug and len(ids_to_remove) > 0:
            print('MVV DEBUG[Frame %d, cam %s]: Removing IDs' % (self.md_module.global_frame_num, cam_name), ids_to_remove)
        if self.params.debug and len(ids_to_idle) > 0:
            print('MVV DEBUG[Frame %d, cam %s]: Moving to Idle IDs' % (self.md_module.global_frame_num, cam_name), ids_to_idle)

        for id_in_mv in ids_to_remove:
            tracker.remove_id_from_tracker(id_in_mv)
        if update_tracker:
            tracker.update_multi_tracker()
            tracker.countdown_to_apply_detector = 0
        self.clean_mv_by_id(mv, ids_to_remove, cam_name)
        self.idle_mv_by_id(mv, ids_to_idle, cam_name)

    def clean_all_pending_mvs(self):
        for cam_name, cam_mvs in self.vectors.items():
            all_ids = list(cam_mvs.keys())
            self.clean_mv_by_id(cam_mvs, all_ids, cam_name)

    def check_if_bbox_is_static(self, id, mv, last_n=5, dist=None):
        # TODO - replace with better heuristic 
        if mv[id].size() < last_n:
            return False
        y = mv[id].mv_element_list[-last_n].coords.centroid()[1]
        miny, maxy = y, y
        height_vec = []
        for mv_element in mv[id].mv_element_list[(1-last_n):]:
            y = mv_element.coords.centroid()[1]
            height_vec.append(mv_element.coords.h())
            maxy = max(maxy, y)
            miny = min(miny, y)
        if dist==None:
            dist = self.params.rel_y_dist_for_mv_change
        ave_height = np.array(height_vec).mean() # relative box size of small boxes
        if (maxy - miny) <= min(dist, 0.5 * ave_height):
            return True
        return False
    
    def is_marginal_mv(self, mv_candidate):
        last_el = mv_candidate.tail()
        cam_name = mv_candidate.camera_type
        wait_n_frames_from_last_el = self.params.enable_stall_delay_while_scanning if hasattr(self.md_module, 'barcode_handler') \
            and self.md_module.barcode_handler.is_during_scan() else self.params.enable_stall_delay_while_not_scanning
        if last_el.global_frame_number + wait_n_frames_from_last_el < self.md_module.global_frame_num:
            return False
        cart_calib = self.md_module.cart_calib
        if config.BARCODE_HW_SUPPORT and self.params.min_dist_from_cam_to_stall_event is not None and self.md_module.during_barcode_scan:
            if last_el.depth is not None:
                disp_scale = cart_calib.conf_data['disparity']['scale']
                mv_cam = cart_calib.sensors[cam_name]
                uv = last_el.coords.renormalize(320, 240).centroid()
                xyz = mv_cam.uvdisp2xyz(uv, last_el.depth, disp_scale=disp_scale)
                dist_from_cam = np.linalg.norm(xyz)
                stall_event_due_to_near_object = dist_from_cam < self.params.min_dist_from_cam_to_stall_event
                # Mark MV as near object
                mv_candidate.stall_event_due_to_near_object = stall_event_due_to_near_object
                if mv_candidate.state in (MVState.ActiveConnected, MVState.EventConnected):
                    other_cam_mv = self.get_other_cam_mv(cam_name,mv_candidate.id)
                    if len(other_cam_mv) > 0:
                        other_cam_mv[0].stall_event_due_to_near_object = stall_event_due_to_near_object
                    elif self.stall_front_connected_in_barcode_mode(cam_name, mv_candidate.id):
                        if self.params.debug:
                            print('MVV DEBUG[Frame %d, cam %s]: Ignoring message due to stalling front cam in barcode mode' % \
                                  (last_el.global_frame_number, cam_name))
                        return True
                if stall_event_due_to_near_object:
                    if self.params.debug:
                        print('MVV DEBUG[Frame %d, cam %s]: Ingoring message due to item near cam: %.2fm' % \
                            (last_el.global_frame_number, cam_name, dist_from_cam))
                    return True
        return False
    
    def is_event_timeout(self, id, mv):
        if mv[id].event_frame is None:
            return False
        return self.md_module.global_frame_num - mv[id].event_frame > self.params.terminate_event_n_frames_after

    def clean_mv_by_id(self, mv, ids_to_remove, cam_name):
        if len(ids_to_remove)==0:
            return
        #cleans id from the movement vector, and add it to queue for classifier if event was detected
        event_que = self.event_candidate[cam_name]

        for id_to_remove in ids_to_remove:
            if self.is_event(mv, id_to_remove): # event occured
                direction = mv[id_to_remove].event_direction
                initiating_cam = cam_name
                # Join most similar motion vector from other cam/s to event
                mv_elements = mv[id_to_remove].mv_element_list.copy()
                frame_history = mv[id_to_remove].frame_history.copy()
                if self.params.ignore_last_tracking_elements:
                    self.ignore_last_mv_tracker_boxes(mv_elements)
                other_cam_mvs = self.get_other_cam_mv(cam_name, id_to_remove, multiple=True)
                both_cams_saw_event = False
                after_hand_by_other = True
                # Aggregate early indication
                ea_ind = [mv[id_to_remove].early_indication_idx] if mv[id_to_remove].early_indication_idx is not None else []
                activated_by_md_other_mv = False
                if len(other_cam_mvs) > 0:
                    for other_cam_mv in other_cam_mvs:
                        # Move other cam to finished mode
                        next_state = other_cam_mv.get_next_state(MVEvent.SentByOther)
                        other_cam_mv.step(next_state, MVEvent.SentByOther)
                        # Override direction with a valid mv event, in the following cases:
                        # 1. Cleaned MV is not initiating the event
                        # 2. Both MVs are events in opposite directions (take the longer MV direction)
                        if other_cam_mv.state==MVState.EventConnected:
                            mv_is_event = mv[id_to_remove].state==MVState.EventConnected
                            if mv_is_event:
                                both_cams_saw_event = True
                                after_hand_by_other = after_hand_by_other and other_cam_mv.came_after_hand
                            other_mv_is_longer = len(other_cam_mv.mv_element_list) > len(mv_elements)
                            different_directions = other_cam_mv.event_direction!=mv[id_to_remove].event_direction
                            take_direction_from_other_mv = (not mv_is_event or \
                                (mv_is_event and different_directions and other_mv_is_longer)) and \
                                other_cam_mv.event_direction in [EventDirection.In, EventDirection.Out]
                            if take_direction_from_other_mv:
                                direction = other_cam_mv.event_direction
                                initiating_cam = other_cam_mv.camera_type
                            if self.params.ignore_last_tracking_elements:
                                self.ignore_last_mv_tracker_boxes(other_cam_mv.mv_element_list)
                        mv_elements = self.merge_mvs(mv_elements, other_cam_mv.mv_element_list.copy())
                        frame_history += other_cam_mv.frame_history.copy()
                        if other_cam_mv.early_indication_idx is not None:
                            ea_ind.append(other_cam_mv.early_indication_idx)
                            other_cam_mv.early_indication_idx = None
                        self.multi_cam_connectivity.remove(other_cam_mv.camera_type, other_cam_mv.id)
                        other_cam_mv.force_remove()
                        activated_by_md_other_mv = activated_by_md_other_mv or other_cam_mv.event_occurred
                # Filter Hand-events - Product Detector V2
                no_interaction_if_filtered = False
                if config.DETECTOR_V2:
                    attributes = self.md_module.detector.attributes
                    if self.is_hand_event(mv_elements, attributes):
                        print('\033[93mRejecting MV [%d,%s,%s]: Hand-Event\033[00m' % \
                                (id_to_remove, direction, initiating_cam), flush=True)
                        # Perform removal sequence
                        if hasattr(self.md_module, 'event_indicator') and mv[id_to_remove].early_indication_idx is not None:
                            self.md_module.event_indicator.set_processing_status(False, mv[id_to_remove].early_indication_idx, reason='mv_manager_reject_event')
                        if hasattr(self.md_module, 'barcode_handler'):
                            self.pass_barcode_marker(mv[id_to_remove])
                        # Remove motion vector from database
                        self.multi_cam_connectivity.remove(cam_name, id_to_remove)
                        del mv[id_to_remove]
                        if cam_name and id_to_remove in self.stalled_front_ids:
                            del self.stalled_front_ids[id_to_remove]
                        continue
                    if mv[id_to_remove].came_after_hand and (not both_cams_saw_event or after_hand_by_other):
                        no_interaction_if_filtered = True
                event = {}
                event['event_frame'] = mv[id_to_remove].tail().global_frame_number
                event['event_start'] = mv[id_to_remove].head().global_frame_number
                event['event_end'] = mv[id_to_remove].tail().global_frame_number
                event['movement_vector'] = mv_elements
                event['frame_history'] = frame_history
                event['connected_mvs'] = self.get_top_connected(cam_name, id_to_remove)
                event['event_direction'] = "in" if direction in [EventDirection.In] else "out"
                event['multi_cam'] = len(other_cam_mvs) > 0
                event['both_cams_saw_event'] = both_cams_saw_event
                event['initiating_cam'] = initiating_cam
                event['activated_by_cart_bottom'] = mv[id_to_remove].activated_by_cart_bottom or \
                                                    (len(other_cam_mvs) > 0 and other_cam_mvs[0].activated_by_cart_bottom) or \
                                                    (len(other_cam_mvs) > 1 and other_cam_mvs[1].activated_by_cart_bottom)
                event['activated_by_md'] = mv[id_to_remove].event_occurred or activated_by_md_other_mv
                event['event_id'] = self.event_id
                event['mv_id'] = id_to_remove
                event['front_id'] = id_to_remove if cam_name == 'front' else other_cam_mvs[0].id if len(other_cam_mvs) > 0 else None
                event['back_id'] = id_to_remove if cam_name == 'back' else other_cam_mvs[0].id if len(other_cam_mvs) > 0 else None
                event['marked_during_barcode_scan'] = mv[id_to_remove].during_barcode_scan or \
                                                      (event['multi_cam'] and other_cam_mvs[0].during_barcode_scan) or \
                                                      (len(other_cam_mvs) > 1 and other_cam_mvs[1].during_barcode_scan)
                event['init_coords'] = mv[id_to_remove].init_coords
                event['pile_candidate'] = mv[id_to_remove].pile_candidate
                event['no_interaction_if_filtered'] = no_interaction_if_filtered
                event['small_item_candidate'] = mv[id_to_remove].small_item_candidate
                event['thrown_item_candidate'] = mv[id_to_remove].thrown_item_candidate
                # Aggregate early indication
                event['early_indication_idx'] = ea_ind[0] if len(ea_ind) > 0 else None
                if len(ea_ind) > 1 and hasattr(self.md_module, 'event_indicator'):
                    for ea in ea_ind[1:]:
                        self.md_module.event_indicator.set_processing_status(False, ea, reason='mv_manager_remove_multi_event')
                # Log event latency
                if hasattr(self.md_module, 'latency_buffer'):
                    self.md_module.latency_buffer.append('t_mv_manager_out', time.time(), event['event_frame'])

                self.event_id += 1
                event_que.append(event)
                if self.logger is not None and self.logger.should_log('mv_manager_out'):
                    self.logger.log(MotionVecLogger.pack(event), 'mv_manager_out')
                other_id = [other_cam_mv.id for other_cam_mv in other_cam_mvs] if len(other_cam_mvs) > 0 else [-1]
                other_cam_name = other_cam_mvs[0].camera_type if len(other_cam_mvs) > 0 else 'None'
                avg_det_score = np.array([el.detector_conf for el in event['movement_vector'] if el.detector_conf > 0]).mean()
                # Summarize track info
                track_types = dict(tracked=0, product=0, hand=0, hhp=0)
                for el in event['movement_vector']:
                    crop_type = 'tracked' if el.detector_conf < 0 else 'product' if el.crop_type is None or el.crop_type['name']=='product' \
                        else 'hand' if el.crop_type['name']=='hand' else 'hhp'
                    track_types[crop_type] +=1
                print('Sending mv [ID %d, %s, size=%d, dir=%s, frame=%d, detection score=%.2f] to event aggregation, multi-cam: mv ID [%s, %s]'\
                    % (id_to_remove, cam_name, len(event['movement_vector']), event['event_direction'], event['event_frame'], avg_det_score, \
                        ', '.join(map(str, other_id)), other_cam_name) + '. mv types:%s' % str(track_types), flush=True)
            else:
                if hasattr(self.md_module, 'event_indicator') and mv[id_to_remove].early_indication_idx is not None:
                    self.md_module.event_indicator.set_processing_status(False, mv[id_to_remove].early_indication_idx, reason='mv_manager_reject_event')
                if hasattr(self.md_module, 'barcode_handler'):
                    self.pass_barcode_marker(mv[id_to_remove])
            # Remove motion vector from database
            self.multi_cam_connectivity.remove(cam_name, id_to_remove)
            del mv[id_to_remove]
            if cam_name and id_to_remove in self.stalled_front_ids:
                del self.stalled_front_ids[id_to_remove]

    def idle_mv_by_id(self, mv, ids_to_idle, cam_name):
        if len(ids_to_idle)==0:
            return

        for id_to_idle in ids_to_idle:
            mv[id_to_idle].is_idle = True
            other_cam_mv = self.get_other_cam_mv(cam_name, id_to_idle)
            if len(other_cam_mv) > 0:
                # Make sure other cam is connected enough to current cam:
                # 1. edge is big enough to be considered connected
                # 2. other cam is also in connected state
                # 3. cross-check is ok
                # This is meant to deal with scenarios where connected mv was already removed
                edge = self.multi_cam_connectivity.get_edge(mv[id_to_idle].camera_type, id_to_idle, other_cam_mv[0].camera_type, other_cam_mv[0].id)
                other_in_connected_state = other_cam_mv[0].state in (MVState.ActiveConnected, MVState.InactiveConnected, MVState.EventConnected)
                reverse_candidates = self.multi_cam_connectivity.top_k_in_other(other_cam_mv[0].camera_type, other_cam_mv[0].id, cam_name, top_k=1)
                cross_check = reverse_candidates[0]['id']==id_to_idle
                if edge['edge_size'] >= self.params.min_edge_count and other_in_connected_state and cross_check:
                    other_cam_mv[0].send_when_done = True
                else:
                    mv[id_to_idle].send_when_done = True
            else: # connected mv from other cam was just removed, remove mv without waiting
                mv[id_to_idle].send_when_done = True

    def merge_mvs(self, el1, el2):
        return el1 + el2

    def ignore_last_mv_tracker_boxes(self, mv):
        for el_idx in reversed(range(len(mv))):
            if mv[el_idx].detector_conf==-1:
                mv[el_idx].classify = False
            else:
                break

    def get_top_connected(self, cam_name, cam_id):
        best_candidates = []
        for other_cam_name in self.que_names:
            if other_cam_name==cam_name:
                continue
            best_candidates += self.multi_cam_connectivity.top_k_in_other(cam_name, cam_id, other_cam_name, top_k=3)
        return best_candidates

    def get_other_cam_mv(self, cam_name, cam_id, multiple=False):
        other_mv = []
        best_candidates = []
        for other_cam_name in self.que_names:
            if other_cam_name==cam_name:
                continue
            best_candidates += self.multi_cam_connectivity.top_k_in_other(cam_name, cam_id, other_cam_name, top_k=3)
            # TODO - currently supporting first other cam
            if len(best_candidates) > 0:
                allowed_mvs_to_return = 2 if multiple and len(best_candidates) > 1 else 1
                for i in range(allowed_mvs_to_return):
                    # Cross-check connection
                    reverse_candidates = self.multi_cam_connectivity.top_k_in_other(other_cam_name, best_candidates[i]['id'], cam_name, top_k=2)
                    cross_check = reverse_candidates[0]['id']==cam_id
                    # Check for strong old connection that should be ignored
                    if not cross_check and len(reverse_candidates)>1:
                        valid_top2_candidate = reverse_candidates[1]['id']==cam_id
                        new_top2_candidate = self.md_module.global_frame_num - reverse_candidates[1]['last_frame'] < self.params.old_multi_cam_edge_frame_th / 2
                        old_top1_candidate = self.md_module.global_frame_num - reverse_candidates[0]['last_frame'] > self.params.old_multi_cam_edge_frame_th
                        if valid_top2_candidate and new_top2_candidate and old_top1_candidate:
                            cross_check = True

                    if cross_check and (best_candidates[i]['count'] >= self.params.min_edge_count or\
                       best_candidates[i]['count'] >= self.params.min_edge_ratio * self.vectors[cam_name][cam_id].size()):
                        other_cam_id = best_candidates[i]['id']
                        other_mv.append(self.vectors[other_cam_name][other_cam_id])
        return other_mv

    def stall_front_connected_in_barcode_mode(self, cam_name, cam_id):
        if cam_name != 'front' or not hasattr(self.md_module, 'barcode_handler'):
            return False
        for other_cam_name in self.que_names:
            if other_cam_name==cam_name:
                continue
            best_candidates = self.multi_cam_connectivity.top_k_in_other(cam_name, cam_id, other_cam_name, top_k=3)
            if len(best_candidates) > 0 and best_candidates[0]['count'] >= self.params.min_edge_to_stall:
                if cam_id not in self.stalled_front_ids.keys():
                    self.stalled_front_ids[cam_id] = 1
                    return True
                elif self.stalled_front_ids[cam_id] <= self.params.stall_x_frames:
                    self.stalled_front_ids[cam_id] += 1
                    return True
        return False
    
    def is_hand_event(self, mv_elements, attributes):
        # TODO - move to params
        if 'hand' not in attributes['crop_type']:
            return False
        for cam_name in self.que_names:
            # Collect all detector boxes from cam
            cam_mv = [mv for mv in mv_elements if mv.cam_name==cam_name  and mv.source=='MD' and mv.attributes is not None]
            if len(cam_mv)==0:
                continue
            # Compute hand probabilities
            hand_probs = np.array([cmv.attributes['crop_type'][attributes['crop_type'].index('hand')] for cmv in cam_mv])
            # TODO - also look at hand holding product probabilities
            # hhp_probs = np.array([cmv.attributes['crop_type'][attributes['crop_type'].index('hand-holding-product')] for cmv in cam_mv])
            # Filter crops on cart-bottom
            cb_prob = np.array([cmv.attributes['crop_location'][attributes['crop_location'].index('on-cart-bottom')] for cmv in cam_mv])
            hand_probs = hand_probs[cb_prob < 0.7]
            if len(hand_probs)==0:
                continue

            if np.median(hand_probs) < 0.5:
                return False

        return True

    def is_product_thrown_event(self, mv, conf_th=0.9, n_thrown_th=2, n_not_cb_th=1, valid_dist_between_crops=0.08):
        if mv.event_direction==EventDirection.Out:
            return False
        attributes = self.md_module.detector.attributes
        n_thrown = 0
        n_not_cb = 0
        thrown_el = []
        for mv_el in mv.mv_element_list:
            if mv_el.coords.attributes is not None and 'crop_state' in mv_el.coords.attributes.keys() and \
                    'thrown' in attributes['crop_state']:
                prob_thrown = mv_el.coords.attributes['crop_state'][attributes['crop_state'].index('thrown')]
                prob_product = mv_el.coords.attributes['crop_type'][attributes['crop_type'].index('product')]
                if prob_thrown > conf_th and prob_product > conf_th:
                    n_thrown +=1
                    prob_inside = mv_el.coords.attributes['crop_location'][attributes['crop_location'].index('inside-cart')]
                    prob_edge = mv_el.coords.attributes['crop_location'][attributes['crop_location'].index('on-cart-edge')]
                    thrown_el.append(mv_el.coords)
                    if prob_inside + prob_edge > conf_th:
                        n_not_cb +=1
        if n_thrown >=n_thrown_th and n_not_cb>=n_not_cb_th:
            # Filter false detection of thrown
            el_dists = np.array([BoundingBox2D.dist(thrown_el[el_idx], thrown_el[el_idx+1]) \
                for el_idx in range(len(thrown_el) - 1)])
            if el_dists.mean() < valid_dist_between_crops:
                return False
            return True
        else:
            return False
    
    def is_event_on_pile(self, mv):
        if self.params.ignore_pile_events:
            return False
        if not hasattr(self.md_module, 'cart_content_handler'):
            return False
        if mv.size() < self.params.min_obs_for_event:
            return False
        # if mv.init_location==mv.current_location:
        #     return False
        event_direction = mv.evaluate_direction()
        if event_direction is None:
            return False
        if not mv.activated_by_cart_bottom:
            if event_direction=='in' and mv.init_location!='up':
                return False
            if event_direction=='out' and mv.current_location!='up':
                return False
        pos_v = np.array([el.coords.centroid()[1] for el in mv.mv_element_list])
        dynamic_mask = np.hstack(([True], np.abs(pos_v[1:] - pos_v[:-1])>0.01))
        pos_v = pos_v[dynamic_mask]
        ed_score = np.array([el.event_detector_score for el in mv.mv_element_list])[dynamic_mask]
        event_pos_v_correlation = np.corrcoef(pos_v, ed_score)[0,1]
        test_box = mv.bottom(detection_th=0, last_n=5).coords if event_direction=='in' else \
                   mv.bottom(detection_th=0, first_n=5).coords

        high_corr = event_pos_v_correlation > 0.6
        high_ed_score = ed_score.mean() > 0.9
        medium_corr_high_ed_score = event_pos_v_correlation > 0.2 and ed_score.mean() > 0.8
        valid_ed_signal =  high_corr or high_ed_score or medium_corr_high_ed_score

        pile_mode = 'overlap' if mv.activated_by_cart_bottom else 'adjacent'
        if hasattr(self.md_module.cart_content_handler, 'location_matching'):
            pile_items = self.md_module.cart_content_handler.location_matching.get_pile_locations(mv.camera_type, test_box, mode=pile_mode)
        else:
            pile_items = []
        if self.params.debug:
            print('\033[93mEvent detector correlation:', event_pos_v_correlation, '\033[00m', flush=True)
            print('\033[93mEvent detector mean:', ed_score.mean(), '\033[00m', flush=True)
            print('\033[93mEvent detector signal:', ed_score, '\033[00m', flush=True)
            print('\033[93mNum pile items:', len(pile_items), '\033[00m', flush=True)

        if len(pile_items) > 0 and (valid_ed_signal or mv.activated_by_cart_bottom):
            print('\033[93mIdentified MV [%d,%s] as possible pile event\033[00m' % \
                    (mv.id, event_direction), flush=True)
            # Make sure direction is set
            mv.event_direction = EventDirection.In if event_direction=='in' else EventDirection.Out
            return True
        return False
    
    def is_event_small_product(self, mv):
        if self.params.ignore_small_item_events:
            return False
        # Find smallest crop
        obj_sizes = np.array([el.coords.area() for el in mv.mv_element_list])
        # Reject too large boxes TODO set thresholds
        if obj_sizes.max() > 0.03 or obj_sizes.mean() > 0.01:
            return False
        super_small_item = obj_sizes.mean() < 7e-3 # most likely small item in the edge
        # print('\033[93mobj sizes', obj_sizes,'\033[00m', flush=True)
        event_direction = mv.evaluate_direction()
        if self.params.debug:
            print('\033[93mPass size test. Super-small=%r' % super_small_item,'\033[00m', flush=True)
        # Reject removal events
        if event_direction!='in':
            # Allow small items to pass even if the horizontal diff is small
            delta_y = mv.tail().coords.centroid()[1] - mv.init_coords.centroid()[1]
            if delta_y < mv.tail().coords.h() * 2:
                if self.params.debug:
                    print('\033[93mdelta_y=%.2f, det h=%.2f' % (delta_y, mv.tail().coords.h()),'\033[00m', flush=True)
                return False
            else:
                mv.event_direction = EventDirection.In
        if self.params.debug:
            print('\033[93mPass direction test','\033[00m', flush=True)
        # Reject event too short
        if len(obj_sizes) < 3:
            return False
        # Get Event Detector scores around the event
        n_pre_event = 10
        n_post_event = 10
        start_frame = max(mv.mv_element_list[0].global_frame_number - n_pre_event, 0)
        end_frame = max(mv.mv_element_list[-1].global_frame_number + n_post_event, 0)
        ed_scores = self.md_module.cart_bottom_detector.event_detector_scores.get_range(start_frame, end_frame)
        pre_event_scores = np.array([es[mv.camera_type] for es in ed_scores[:n_pre_event] if es is not None])
        event_scores = np.array([es[mv.camera_type] for es in ed_scores[n_pre_event:-n_post_event] if es is not None])
        post_event_scores = np.array([es[mv.camera_type] for es in ed_scores[-n_post_event:] if es is not None])
        n_pre_event = len(pre_event_scores) # mainly for capsules
        n_post_event = len(post_event_scores) # mainly for capsules

        pre_event_low = pre_event_scores.mean() < 0.2
        margin = 0.25
        event_size = event_scores.shape[0]
        pre_event_pos_slope = pre_event_scores[:n_pre_event // 2].mean() < pre_event_scores[n_pre_event // 2:].mean()
        event_pos_slope = event_scores[:event_size // 2].mean() + margin < event_scores[event_size // 2:].mean()
        active_event = event_scores.mean() > 0.9 or event_scores[event_size//2:].mean() > 0.95
        active_event_super_small = super_small_item and event_scores.mean() > 0.5 and (event_pos_slope or pre_event_low)
        # TODO - add condition on post-event
        if self.params.debug:
            print('pre-event:', pre_event_scores, flush=True)
            print('during-event:', event_scores, flush=True)
            print('pre-event low=%r, pre-event++:%r, active event:%r, , active & small event:%r' \
                % (pre_event_low, pre_event_pos_slope, active_event, active_event_super_small), flush=True)

        if (pre_event_low and (event_pos_slope or active_event)) or \
            (pre_event_pos_slope and active_event) or \
            (active_event_super_small):
            print('\033[93mIdentified MV [%d,%s] as possible small item event\033[00m' % \
                    (mv.id, event_direction), flush=True)
            # Make sure direction is set
            return True
        return False
    
    def is_moving_inside_cart(self, mv):
        try:
            first_box_dist_from_line = mv.mv_element_list[0].coords.y2 - mv.init_decision_line
            last_mv_el = [el for el in mv.mv_element_list if el.source=='MD' and el.cam_name==mv.camera_type][-1]
            last_box_dist_from_line = last_mv_el.coords.y2 - mv.decision_line_value
            first_box_inside_ratio = first_box_dist_from_line / mv.mv_element_list[0].coords.h()
            last_box_inside_ratio = last_box_dist_from_line / last_mv_el.coords.h()

            if (mv.event_direction==EventDirection.In or mv.evaluate_direction()=='in') and \
                (mv.init_location=='down' or (mv.init_location=='middle' and first_box_inside_ratio > 0.7)):
                # Get scores from event detector
                end_frame = mv.mv_element_list[0].global_frame_number
                start_frame = max(end_frame - self.params.prod_moving_in_cart_check_n_frames_back, 0)
                ed_scores = self.md_module.cart_bottom_detector.event_detector_scores.get_range(start_frame, end_frame)
                ed_scores = np.array([es[mv.camera_type] for es in ed_scores if es is not None])
                if ed_scores.mean() > self.params.prod_moving_in_cart_ed_th_dir_in:
                    print('\033[93mIdentified MV [%s,%d, in] as product moving in cart, Ignoring\033[00m' % \
                        (mv.camera_type, mv.id), flush=True)
                    return True
            elif (mv.event_direction==EventDirection.Out or mv.evaluate_direction()=='out') and \
                mv.init_location=='down' and (mv.current_location=='down' or \
                    (mv.current_location=='middle' and last_box_inside_ratio > 0.7)):
                # TODO - find solution to event finished in middle
                # Get scores from event detector
                # start_frame = mv.event_frame
                start_frame = mv.event_frame if mv.event_frame is not None else last_mv_el.global_frame_number
                check_n_frames_after_out = 3 if mv.current_location=='middle' else 5
                end_frame = start_frame + check_n_frames_after_out
                ed_scores = self.md_module.cart_bottom_detector.event_detector_scores.get_range(start_frame, end_frame)
                ed_scores = np.array([es[mv.camera_type] for es in ed_scores if es is not None])
                if ed_scores.mean() > self.params.prod_moving_in_cart_ed_th_dir_out:
                    print('\033[93mIdentified MV [%s,%d, out] as product moving in cart, Ignoring\033[00m' % \
                        (mv.camera_type, mv.id), flush=True)
                    return True
            return False
        except:
            return False

    def is_event(self, mv, id):
        if mv[id].state==MVState.ForceRemove:
            return False
        elif mv[id].state==MVState.ForceEvent:
            return True
        event_occurred = (mv[id].state in (MVState.Event, MVState.EventConnected) and \
             mv[id].event_occurred) and self.valid_event(mv[id])
        
        # Check for pile candidate
        mv[id].pile_candidate = self.is_event_on_pile(mv[id])
        # Check for small product candidate
        mv[id].small_item_candidate = self.is_event_small_product(mv[id])
        # Check for thrown product candidate
        if config.DETECTOR_V2:
            mv[id].thrown_item_candidate = self.is_product_thrown_event(mv[id])
            if mv[id].thrown_item_candidate:
                print('\033[93mSending thrown item mv:[cam: %s, ID %d, size %d]\033[00m' % (mv[id].camera_type, mv[id].id, mv[id].size()), flush=True)
                mv[id].event_direction = EventDirection.In
                return True

        # Filter events suspected as motion inside the cart
        if self.is_moving_inside_cart(mv[id]):
            return False

        if not event_occurred:
            # Test if event is possible a pile event
            mv[id].event_occurred = mv[id].pile_candidate or mv[id].small_item_candidate
            event_occurred = mv[id].event_occurred
        valid_event = (event_occurred) and (mv[id].small_item_candidate or mv[id].thrown_item_candidate or mv[id].size() >= self.params.min_obs_for_event)
        if event_occurred and not valid_event:
            # TODO - handle case of connected event that has enought mv elements if you consider also connected mv
            # Maybe check in this case cart bottom to decide
            print('Ignoring mv [ID %d, %s, size=%d, dir=%s] Due to short size'\
                % (id, mv[id].camera_type, len(mv[id].mv_element_list), mv[id].event_direction), flush=True)

        if valid_event and hasattr(self.md_module, 'barcode_handler'):
            cam_name = mv[id].camera_type
            last_mv_box = mv[id].mv_element_list[-1].coords
            check_coverage = cam_name!='front'
            last_box_in_barcode_roi = False if not config.BARCODE_HW_SUPPORT else \
                 self.md_module.barcode_handler.box_in_barcode_roi(last_mv_box, cam_name, check_coverage=check_coverage)
            # Ignore MV generated during scan from box in Barcode ROI
            cam_stat_th = self.md_module.barcode_handler.auto_detect_trigger.params.cam_static_th \
                if hasattr(self.md_module.barcode_handler, 'auto_detect_trigger') else dict(front=0.03, back=0.01)
            static_event_in_bc_roi = self.params.ignore_static_mv_in_barcode_roi and self.md_module.during_barcode_scan and \
                self.check_if_bbox_is_static(id, mv, last_n=3, dist=cam_stat_th[cam_name]) and last_box_in_barcode_roi
            
            large_front_mv = np.array([el.coords.area() for el in mv[id].mv_element_list]).min() > 0.2 and cam_name=='front'
            last_mv_frame = mv[id].mv_element_list[-1].global_frame_number
            if mv[id].size() <= self.params.ignore_short_mv_after_scan_max_len or \
                (large_front_mv and mv[id].size() <=self.params.ignore_short_large_mv_after_scan_max_len):
                # Ignore short removal event started shortly after barcode scan and are originating in barcode scan area
                if mv[id].event_direction==EventDirection.Out:
                    first_mv_box = mv[id].mv_element_list[0].coords
                    first_mv_frame = mv[id].mv_element_list[0].global_frame_number
                    short_time_after_scan = first_mv_frame - self.md_module.barcode_handler.last_scanned_barcode_frame < self.params.ignore_short_remove_after_scan_for_n_frames
                    first_box_in_barcode_roi = self.md_module.barcode_handler.box_in_barcode_roi(first_mv_box, cam_name, check_coverage=check_coverage)
                    large_box = cam_name=='front' and first_mv_box.area() > 0.1
                    if short_time_after_scan and (large_box or first_box_in_barcode_roi):
                        print('Ignoring mv [ID %d, %s, size=%d, dir=%s] Due to short time after last barcode scan: short remove event near scanner'\
                            % (id, cam_name, len(mv[id].mv_element_list), mv[id].event_direction), flush=True)
                        return False
            
                # Ignore short insert event from front cam shortly after barcode during barcode scan where boxes are large
                if mv[id].event_direction==EventDirection.In and cam_name=='front':
                    short_time_after_scan = last_mv_frame - self.md_module.barcode_handler.last_scanned_barcode_frame < self.params.ignore_short_insert_after_scan_for_n_frames
                    large_box = last_mv_box.area() > 0.1
                    if short_time_after_scan and large_box:
                        print('Ignoring mv [ID %d, %s, size=%d, dir=%s] Due to short time after last barcode scan: short insert event near scanner'\
                            % (id, cam_name, len(mv[id].mv_element_list), mv[id].event_direction), flush=True)
                        return False
            
            if self.params.min_dist_from_cam_to_stall_event is not None and self.md_module.during_barcode_scan:
                mv_sent_during_scan = last_mv_frame < self.md_module.barcode_handler.last_scanned_barcode_frame or \
                    self.md_module.barcode_handler.is_pending_scan()
                mv_insert_large_and_middle_loc = last_mv_box.area() > 0.1 and mv[id].event_direction==EventDirection.In and mv[id].current_location=='middle'
                if hasattr(mv[id], 'stall_event_due_to_near_object') and mv[id].stall_event_due_to_near_object and \
                        (static_event_in_bc_roi or mv_sent_during_scan or mv_insert_large_and_middle_loc):
                    print('Ignoring mv [ID %d, %s, size=%d, dir=%s] Due to near distance from camera'\
                        % (id, cam_name, len(mv[id].mv_element_list), mv[id].event_direction), flush=True)
                    return False
            if static_event_in_bc_roi:
                print('Ignoring mv [ID %d, %s, size=%d, dir=%s] Due to Static event in Barcode ROI'\
                    % (id, cam_name, len(mv[id].mv_element_list), mv[id].event_direction), flush=True)
                return False
            # Ignore MV in barcode roi during scan
            if not self.md_module.barcode_handler.is_idle():
                if last_box_in_barcode_roi and static_event_in_bc_roi:
                    print('Ignoring mv [ID %d, %s, size=%d, dir=%s] Due to Active BC mode + box still in ROI'\
                        % (id, cam_name, len(mv[id].mv_element_list), mv[id].event_direction), flush=True)
                    return False
        return valid_event


    def valid_event(self, mv):
        el_before_event = [m for m in mv.mv_element_list if m.global_frame_number <= mv.event_frame]
        el_after_event = [m for m in mv.mv_element_list if m.global_frame_number >= mv.event_frame]
        confident_detections_before_event = [el for el in el_before_event if el.detector_conf>self.params.confident_det_th]
        confident_detections_after_event = [el for el in el_after_event if el.detector_conf>self.params.confident_det_th]
        if config.DETECTOR_V2:
            attributes = self.md_module.detector.attributes
            if mv.event_direction in (EventDirection.In, EventDirection.Out):
                els_inside_cart = el_after_event if mv.event_direction==EventDirection.In else el_before_event
                inside_cart_idx = [el_idx for el_idx, el in enumerate(els_inside_cart) if hasattr(el.coords, 'attributes') and \
                    el.coords.attributes is not None and \
                    el.coords.attributes['crop_location'][attributes['crop_location'].index('outside-cart')] < 0.1]
                event_in_idx = [idx for idx in inside_cart_idx if els_inside_cart[idx].event_detector_score > 0.5]
                if len(inside_cart_idx)==0 or len(event_in_idx)==0:
                    cb_crop_th = 0.1 if mv.size() < 6 else 0.5
                    thrown_element_idx = [el_idx for el_idx, el in enumerate(els_inside_cart) if hasattr(el.coords, 'attributes') and \
                        el.coords.attributes is not None and \
                        el.coords.attributes['crop_state'][attributes['crop_state'].index('thrown')] > 0.9 and \
                        el.coords.attributes['crop_location'][attributes['crop_location'].index('on-cart-bottom')] < cb_crop_th]
                    if len(thrown_element_idx)==0 or mv.event_direction==EventDirection.Out:
                        print('Ignoring mv [ID %d, %s, size=%d, dir=%s] Due to not confident detection inside cart'\
                            % (mv.id, mv.camera_type, len(mv.mv_element_list), mv.event_direction), flush=True)
                        return False
        return len(confident_detections_before_event) > 0 and  len(confident_detections_after_event) > 0

    def calc_mv_connectivity_multi_cam(self, mv):
        '''
        Calculate connections between detections in different cameras
        We always compare the last element in mv againts N last elements in other cameras
        If the reference element is from a stereo camera, we will project its box once for each other camera
        If the reference element is from a mono camera, we will compare it only vs stereo camera(s)
        '''
        best_match = dict(id=None, iou = 0., distance = 100, box=None)
        second_match = dict(id=None, iou = 0., distance = 100)
        cart_calib = self.md_module.cart_calib
        can_project = cart_calib.sensors[mv.camera_type].has_stereo()
        for other_cam_name, other_cam_vec in self.vectors.items():
            if other_cam_name==mv.camera_type:
                continue
            # for each vec, iterate back from the end until frame difference is too large
            for other_mv_id, other_mv in other_cam_vec.items():
                # Project reference box to other camera
                if can_project:
                    ref_box, valid_projection = self.project_by_disparity(mv.tail(), mv.camera_type, other_cam_name)
                else:
                    ref_box = mv.tail().coords
                # Run over other cam boxes and compare to box
                for other_el in reversed(other_mv.mv_element_list):
                    frame_diff = mv.tail().global_frame_number - other_el.global_frame_number
                    if frame_diff > self.params.max_multi_cam_frame_diff:
                        break
                    if can_project:
                        test_box = other_el.coords
                    else:
                        test_box, valid_projection = self.project_by_disparity(other_el, other_cam_name, mv.camera_type)
                    if not valid_projection:
                        continue
                    # Calculate IOU
                    iou = BoundingBox2D.iou(ref_box, test_box)
                    distance = BoundingBox2D.dist(ref_box, test_box)
                    size = min(ref_box.area(), test_box.area())
                    iou_condition = iou > max(self.params.min_multi_cam_box_iou, best_match['iou'])
                    distance_condition = best_match['iou'] == 0 and size <= self.params.small_bbox_size and \
                                         distance <= min(self.params.max_multi_cam_box_distance, best_match['distance'])
                    if iou_condition or distance_condition:
                        if best_match['id']!=other_mv_id:
                            second_match = best_match.copy()
                        if not iou_condition and distance_condition:
                            best_match['distance'] = distance
                            # print("satisfying distance condition but not iou")
                        best_match['iou'] = iou
                        best_match['id'] = other_mv_id
                        best_match['frame'] = mv.tail().global_frame_number if can_project else other_el.global_frame_number
                        best_match['box'] = ref_box if can_project else test_box
            # Register connectivity
            if (best_match['id'] is not None and (best_match['iou'] * self.params.multi_box_iou_ratio > second_match['iou']) or
                    (best_match['iou'] == 0 and best_match['distance'] * self.params.multi_box_distance_ratio < second_match['distance'])):
                # Ignore matches of idle mvs
                if other_cam_vec[best_match['id']].is_idle:
                    return
                self.multi_cam_connectivity.add(mv.camera_type, mv.id, other_cam_name, best_match['id'], self.md_module.global_frame_num)
                edge = self.multi_cam_connectivity.get_edge(mv.camera_type, mv.id, other_cam_name, best_match['id'])
                # Transition to connected state if needed
                if edge['edge_size'] >= self.params.min_edge_count:
                    next_state = mv.get_next_state(MVEvent.ActivelyConnected)
                    mv.step(next_state, MVEvent.ActivelyConnected)
                    next_state = other_cam_vec[best_match['id']].get_next_state(MVEvent.PassivelyConnected)
                    other_cam_vec[best_match['id']].step(next_state, MVEvent.PassivelyConnected)
                if self.params.debug:
                    edge = self.multi_cam_connectivity.get_edge(mv.camera_type, mv.id, other_cam_name, best_match['id'])
                    print('MVV DEBUG: [Frame:%d] MV[%s, %d, Frame:%d] connected with MV[ID:%s, %d, Frame:%d], iou:%.2f, edge=%d'\
                         % (self.md_module.global_frame_num, mv.camera_type, mv.id, mv.tail().global_frame_number,\
                           other_cam_name, best_match['id'], best_match['frame'], best_match['iou'], edge['edge_size']))
                    if best_match['frame']==self.md_module.global_frame_num:
                        self.md_module.viz_frameB_resized = self.draw_boxes(self.md_module.viz_frameB_resized, [best_match['box']])


    
    def draw_boxes(self, image, boxes):
        import cv2
        image_h, image_w, _ = image.shape

        color_list = [
                        (0,   0, 0),  #cyan
                    ]
        for idx,box in enumerate(boxes):
            color = color_list[0 % len(color_list)]
            xmin = int(box.x1*image_w)
            ymin = int(box.y1*image_h)
            xmax = int(box.x2*image_w)
            ymax = int(box.y2*image_h)

            cv2.rectangle(image, (xmin,ymin), (xmax,ymax), color, 1)
            
        return image          

    def project_by_disparity(self, el, cam_name, other_cam_name):
        cart_calib = self.md_module.cart_calib
        disp_scale = cart_calib.conf_data['disparity']['scale']
        # cam_name = mv.camera_type
        disparity = np.copy(self.md_module.disparity_frame)
        assert cam_name=='front' and other_cam_name=='back', 'Currently supporting only 1 type of projection'
        cam_this = cart_calib.sensors[cam_name]
        cam_other = cart_calib.sensors[other_cam_name]
        h, w = disparity.shape[:2]
        last_mv_el = el.coords.renormalize(h, w) if hasattr(el, 'coords') else el.renormalize(h, w)

        disp_crop = last_mv_el.crop(disparity)
        ave_disparity = np.percentile(disp_crop, 90) ## get foreground depth
        uv_this = last_mv_el.box()
        neig = 1
        for p in uv_this: # tweak value to bbox will not get the background depth
            disparity[int(p[1]) - neig:int(p[1]) + neig,
                      int(p[0]) - neig:int(p[0]) + neig] = ave_disparity
        xyz_this = cam_this.disp2xyz(disparity, uv_this, disp_scale=disp_scale)
        uv_other, is_visible = cam_this.project_xyz_other_cam(cam_other, xyz_this, with_distortion=False)
        if is_visible.sum() < 3:
            return None, False
        uv_other[:,0] = np.clip(uv_other[:,0], 0, w-1)
        uv_other[:,1] = np.clip(uv_other[:,1], 0, h-1)
        # Create rectange from projected coordinates (currently polygon)
        box_other = BoundingBox2D.block_with_box(uv_other[is_visible]).normalize(h, w)
        return box_other, True

    def update_cart_box_db_state(self, mv):
        if mv.event_direction==EventDirection.In:
            box_to_add = mv.mv_element_list[-1].coords
            self.cart_box_db.add(mv.camera_type, box_to_add)
        elif mv.event_direction==EventDirection.Out:
            box_to_remove = mv.mv_element_list[0].coords
            rm_dict = self.cart_box_db.get_similar_box(mv.camera_type, box_to_remove)
            if rm_dict['exist']:
                self.cart_box_db.remove(mv.camera_type, rm_dict['id'])
        elif  mv.current_location=='down': # update item location
            box_to_add = mv.mv_element_list[-1].coords
            self.cart_box_db.add(mv.camera_type, box_to_add, force=True)
    
    def match_mv_to_roi(self, cam_name, roi):
        '''
        Return mv with best IOU to a given roi
        '''
        if len(self.vectors[cam_name])==0:
            return None, 0.
        id_vec = []
        iou_vec = []
        box_vec = []
        for mv_id, mv in self.vectors[cam_name].items():
            if mv.state in (MVState.InactiveConnected, MVState.Inactive):
                continue
            id_vec.append(mv_id)
            iou_vec.append(BoundingBox2D.iou(roi, mv.mv_element_list[-1].coords))
            box_vec.append(mv.mv_element_list[-1].coords)

        if len(id_vec)==0:
            return None, 0.
        best_iou_idx = np.array(iou_vec).argmax()
        best_mv_id = id_vec[best_iou_idx]
        best_box = box_vec[best_iou_idx]
        
        return best_mv_id, best_box
    
    def mark_as_barcode_scanned(self, cam_name, barcode_ids):
        for mv_id in self.vectors[cam_name].keys():
            during_barcode_scan =  mv_id in barcode_ids
            self.vectors[cam_name][mv_id].during_barcode_scan = during_barcode_scan
    
    def pass_barcode_marker(self, mv_cleaned):
        if mv_cleaned.during_barcode_scan and \
            self.md_module.barcode_handler.is_during_insert_event():
            # Try to pass barcode marked to a later, similar,active mv 
            cam_name = mv_cleaned.camera_type
            cam_mvs = self.get_cam_mvs(cam_name)
            cam_mvs = [mv for id, mv in cam_mvs.items() if id!=mv_cleaned.id and mv.state in (MVState.Active, MVState.ActiveConnected)]
            # Find mv created shortly after cleaned mv was inactive, and has valid iou
            for mv_candidate in cam_mvs:
                created_shortly_after = mv_candidate.head().global_frame_number - mv_cleaned.tail().global_frame_number < 5
                high_iou = BoundingBox2D.iou(mv_cleaned.tail().coords, mv_candidate.head().coords) > 0.5
                if created_shortly_after and high_iou:
                    self.set_mv_attr(cam_name, mv_candidate.id, 'during_barcode_scan', True)
                    break


class MotionVecLogger(object):  
    def __init__(self, data=None):
        self.data = data

    @staticmethod
    def pack(event):
        ret_dict = dict()
        for k, val in event.items():
            if k=='movement_vector':
                ret_dict[k] = [v.pack() for v in val]
            else:
                ret_dict[k] = val

        return ret_dict

    @staticmethod
    def unpack(data):
        from cv_blocks.events.movement_vector import MVElement
        ret_dict = dict()
        for k, val in data.items():
            if k=='movement_vector':
                ret_dict[k] = [MVElement.unpack(v) for v in val]
            else:
                ret_dict[k] = val

        return MotionVecLogger(data=ret_dict)

    def visualize(self, path=None, prefix='mv_manager_out', timestamp=None):
        if path is not None:
            import os
            mv_path = os.path.join(path, prefix+'_id_%03d_frame_%05d_%s_dir_%s_ts_%s' %\
                 (self.data['event_id'], self.data['event_frame'], self.data['initiating_cam'], \
                  self.data['event_direction'], timestamp))
            os.makedirs(mv_path, exist_ok=True)
        else:
            mv_path = None
        for img_idx, img_el in enumerate(self.data['movement_vector']):
            img_path = mv_path + '/img_%02d.png' % img_idx if path is not None else None
            img_el.visualize(path=img_path)
