import numpy as np
from config import config

def calculate_crop_quality(mv, max_area=0.2, max_conf=0.9):
    if config.DETECTOR_V2:
        max_conf = 1
    box_area = []; box_conf=[]
    hand_type_idx = 1 # TODO read hand index from config
    for mv_el in mv:
        if hasattr(mv_el.coords, 'attributes') and mv_el.coords.attributes is not None \
            and len(mv_el.coords.attributes['crop_type']) > hand_type_idx and mv_el.coords.attributes['crop_type'][hand_type_idx]>0.5:
                # Overwrite hand predictions to have low crop quality score
                box_area.append(0.)
                box_conf.append(0.)
        else:
            box_area.append(mv_el.coords.area())
            box_conf.append(mv_el.coords.score if mv_el.coords.score > 0 else 0.3)
    area_score = np.minimum(np.array(box_area), max_area) / max_area
    conf_score = np.minimum(np.array(box_conf), max_conf) / max_conf
    box_quality = (area_score + conf_score) / 2
    return box_quality

def compute_score(jitter_std, jitter_conf, jitter_preds, jitter_preds_not_in_topk, hand_idx,
                  std_w=0.5, conf_w=0.25, err_w=0.25, debug=True):
        avg_std = np.average(jitter_std)
        avg_conf = np.average(jitter_conf)
        hands_predictions = len([1 for pred in jitter_preds if pred == hand_idx])
        err_frac = sum(jitter_preds_not_in_topk) / (len(jitter_preds_not_in_topk) - hands_predictions + 1e-8)
        std_score = min(1, avg_std / 0.1)
        conf_score = min(max((0.85 - avg_conf) / 0.5, 0), 1)
        err_score = min(1, err_frac / 0.5)
        total_unsupported_score = std_w * std_score + conf_w * conf_score + err_w * err_score
        if debug:
            print("\033[92mmetrics: std: {:.3f}, conf: {:.2f}, predictions not in top3: {:.2f} - {} images"
                    "\033[00m".format(avg_std, avg_conf, err_frac, len(jitter_std)), flush=True)
            print("\033[92mscores: std: {:.3f}, conf: {:.2f}, predictions not in top3: {:.2f}"
                    "\033[00m".format(std_score, conf_score, err_score), flush=True)
            print("\033[92mUnsupported score: {}\033[00m".format(total_unsupported_score), flush=True)

        return total_unsupported_score

def is_product_unsupported(max_indices, cls_seq, top_products, md_classifier,
                           top_k=2, ignore_classes=('aa_hand', 'other', 'background'),
                           unsupported_score_thresholds={'low':0.38, 'high':0.55}):
    ignore_indices = [md_classifier.productToIndx[cn] for cn in ignore_classes if cn in md_classifier.productToIndx.keys()]
    hand_idx = md_classifier.productToIndx['aa_hand']
    indices_wo_ignored = [idx for idx in max_indices if idx not in ignore_indices]
    scores_wo_ignored = [tp_score for tp_name, tp_score in top_products if tp_name not in ignore_classes]
    jitter_prediction = cls_seq['jitter_prediction']
    top_k = min(len(jitter_prediction[0]['base']) - 1, top_k)
    topk_indices = indices_wo_ignored[:top_k]
    #  Check top-1 and near neighbors
    top_prod = md_classifier.indxToProduct[indices_wo_ignored[0]]
    if top_prod not in md_classifier.confusion_matrix:
        return False
    top_prod = [prod_name for prod_name, prod_score in md_classifier.confusion_matrix[top_prod].items() if prod_score > 0.02 and prod_name not in ignore_classes]
    # Check top-2 prob and add if needed
    for next_idx in indices_wo_ignored[1:top_k]:
        prod = md_classifier.indxToProduct[next_idx]
        if prod not in prod in top_prod:
            top_prod.append(prod)
        if len(top_prod) > top_k:
            break
    topk_indices = [md_classifier.productToIndx[pn] for pn in top_prod]
    base_conf, jitter_conf, jitter_std, jitter_preds, jitter_preds_not_in_topk = [], [], [], [], []
    # Gather image info
    box_quality = calculate_crop_quality(cls_seq['movement_vector_for_saving'])
    for image_scores in jitter_prediction:
        img_topk_idx = image_scores['cls_idx']
        image_base_topk_conf = np.array([image_scores['base'][np.argwhere(img_topk_idx==k).ravel()[0]] \
            if k in img_topk_idx else 0. for k in topk_indices]).sum()
        base_conf.append(image_base_topk_conf)
        img_jitter_conf = []
        for single_jitter_score in image_scores['dropouts']:
            image_jitter_iter_topk_conf = np.array([single_jitter_score[np.argwhere(img_topk_idx==k).ravel()[0]] \
                if k in img_topk_idx else 0. for k in topk_indices]).sum()
            img_jitter_conf.append(image_jitter_iter_topk_conf)
        
        img_jitter_pred = image_scores['cls_idx'][np.argmax(image_scores['dropouts'].sum(axis=0))]
        jitter_preds.append(img_jitter_pred)
        jitter_preds_not_in_topk.append(img_jitter_pred not in topk_indices + ignore_indices)
        jitter_conf.append(np.average(img_jitter_conf))
        jitter_std.append(np.std(img_jitter_conf))

    # penalize images with low confidence
    low_conf_elements = [i for i, (conf_el, pred_el) in enumerate(zip(jitter_conf, jitter_preds))
                        if (conf_el <= 0.1 and pred_el not in ignore_indices)]
    penalty_std = np.percentile(jitter_std, 90)
    for i in low_conf_elements:
        jitter_std[i] = penalty_std

    # Try to mask out non-confident & low quality boxes
    box_quality_th = 0.6 if config.DETECTOR_V2 else 0.5
    cls_conf_th = 0.95
    min_conf_size = 8
    inlier_box = np.bitwise_or(box_quality > box_quality_th, np.array(base_conf) > cls_conf_th)
    if inlier_box.sum() > min_conf_size:
        calc_mask = inlier_box
        print("\033[93mUsing %d/%d classifier crops for jitter computation\033[00m" % (inlier_box.sum(), inlier_box.shape[0]), flush=True)
    else:
        calc_mask = np.ones_like(box_quality, dtype=np.bool)
    
    jitter_std = np.array(jitter_std)[calc_mask]
    jitter_conf = np.array(jitter_conf)[calc_mask]
    jitter_preds = np.array(jitter_preds)[calc_mask]
    jitter_preds_not_in_topk = np.array(jitter_preds_not_in_topk)[calc_mask]

    # Compute unsupported score
    # =========================
    total_unsupported_score = compute_score(jitter_std, jitter_conf, jitter_preds, jitter_preds_not_in_topk, hand_idx)

    top_product_score = scores_wo_ignored[0]
    threshold = unsupported_score_thresholds['high'] if top_product_score / len(jitter_prediction) > 0.8 else \
        unsupported_score_thresholds['low']
    if total_unsupported_score >= threshold:
        print("\033[91mUnsupported product candidate - %s Threshold\033[00m" % \
            ('High' if threshold==unsupported_score_thresholds['high'] else 'Low'))
        return True
    return False