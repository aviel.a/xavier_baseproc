import numpy as np
from scipy.spatial.distance import pdist, cdist

def event_vs_event_predictions_similar(ev_pred1, ev_pred2, input_type='pred_vs_pred', debug=True):
    pred_intersection = list(set(ev_pred1) & set(ev_pred2))
    retval = len(pred_intersection) > 0
    if debug:
        print('Prediction Similarity Debug(%s): similar : %r' % (input_type, retval), flush=True)
        print('Prediction #1 list: ', ev_pred1 , flush=True)
        print('Prediction #2 list: ', ev_pred2 , flush=True)
    return retval

def calc_desc_dists(ev_desc1, ev_desc2, debug=False):
    seq12_distances = cdist(ev_desc1, ev_desc2)
    ui = np.triu_indices(seq12_distances.shape[0], m=seq12_distances.shape[1]) 
    seq12_distances = seq12_distances[ui]
    seq12_dist = seq12_distances.mean()
    if debug:
        print('Descriptors Similarity Debug: seq1(%d el), seq2(%d el), seq1=>2 dist: %.2f' % \
            (len(ev_desc1), len(ev_desc2), seq12_dist), flush=True)
    return seq12_dist

def similar_by_descriptor_dist(ev_desc1, ev_desc2, max_th=1.2, max_ratio_to_internal=1.1, high_dist_th=1.35, debug=True):
    # Default threshold if size of one of the descriptors is 1
    seq1_distances = pdist(ev_desc1) if len(ev_desc1) > 1 else np.array([max_th])
    seq2_distances = pdist(ev_desc2) if len(ev_desc2) > 1 else np.array([max_th])
    seq12_distances = cdist(ev_desc1, ev_desc2)
    ui = np.triu_indices(seq12_distances.shape[0], m=seq12_distances.shape[1]) 
    seq12_distances = seq12_distances[ui]
    seq1_dist_internal = seq1_distances.mean()
    seq2_dist_internal = seq2_distances.mean()
    seq12_dist = seq12_distances.mean()
    single_target_dist = max(seq1_dist_internal, seq2_dist_internal) * max_ratio_to_internal
    single_target_dist = min(single_target_dist, high_dist_th)
    valid_relative_dist = single_target_dist > seq12_dist
    retval = valid_relative_dist or seq12_dist < max_th
    if debug:
        print('Descriptors Similarity Debug: seq1(%d el) dist: %.2f, seq2(%d el) dist: %.2f, seq1=>2 dist: %.2f, similar : %r' % \
            (len(ev_desc1), seq1_dist_internal, len(ev_desc2), seq2_dist_internal, seq12_dist, retval), flush=True)
    return retval

def event_vs_event_descriptors_similar(ev_desc1, ev_desc2, max_th=1.2, max_ratio_to_internal=1.1, per_cam=False, ev1_cams=[], ev2_cams=[]):
    if not per_cam:
        if len(ev_desc1)==0 or len(ev_desc2)==0:
            print("\033[91mInvalid Usage(Full): Trying to compare empty descriptor list!\033[00m", flush=True)
            return False
        return similar_by_descriptor_dist(ev_desc1, ev_desc2, max_th=max_th, max_ratio_to_internal=max_ratio_to_internal)

    if len(ev1_cams)==0 or len(ev2_cams)==0:
        print("\033[91mInvalid Usage(Per-Cam): Trying to compare empty descriptor list!\033[00m", flush=True)
        return False
    single_cam_tested = False
    if len(ev1_cams) > 1 and len(ev2_cams) > 1:
        ev1_unique_cams = list(set(ev1_cams))
        ev2_unique_cams = list(set(ev2_cams))
        for ev1_single_cam in ev1_unique_cams:
            ev1_cam_desc = [d for d, cam in zip(ev_desc1, ev1_cams) if cam==ev1_single_cam]
            if len(ev1_cam_desc) > 1:
                for ev2_single_cam in ev2_unique_cams:
                    if ev1_single_cam!=ev2_single_cam:
                        continue
                    ev2_cam_desc = [d for d, cam in zip(ev_desc2, ev2_cams) if cam==ev2_single_cam]
                    if len(ev2_cam_desc) > 1:
                        single_cam_tested = True
                        print('Computing descriptor dist per cam [%s(%d)->%s(%d)]' % (ev1_single_cam, len(ev1_cam_desc), ev2_single_cam, len(ev2_cam_desc)), flush=True)
                        valid = similar_by_descriptor_dist(ev1_cam_desc, ev2_cam_desc, max_th=max_th, max_ratio_to_internal=max_ratio_to_internal)
                        if valid:
                            return True
        if not single_cam_tested:
            return similar_by_descriptor_dist(ev_desc1, ev_desc2, max_th=max_th, max_ratio_to_internal=max_ratio_to_internal)
        else:
            return False
    print("\033[91mInvalid Usage(Per-Cam): No comparison was made!\033[00m", flush=True)
    return False