import numpy as np
import cv2

class ClassifierSampleRanker(object):
    def __init__(self):
        self.svm_params = \
            dict(w=np.array([-0.11173547, 2.52148335, 2.36560864,0.236144, -0.41246127, 0.90339028,\
                             -0.47836905, 0.18384657, 0.2618406]), 
                 b=-2.3572461)
        from scipy.stats import entropy
        self.entropy = entropy

    def extract_features(self, sample):
        area = sample.coords.area()
        detector_conf = sample.detector_conf
        entropy = self.entropy(cv2.cvtColor(sample.cropped_image, cv2.COLOR_RGB2GRAY).flatten())

        f = np.array([entropy, detector_conf, area, \
            entropy * detector_conf, entropy * area, detector_conf * area, \
            entropy<8., area<0.01, area>0.6], dtype=np.float)
        return f
    
    def predict(self, sample):
        # neutral score for tracker 
        if sample.detector_conf < 0:
            return 0
        f = self.extract_features(sample)
        score = self.svm_params['w'].dot(f) + self.svm_params['b']
        return score
