import time
import queue
import numpy as np
from config import config
from cv_blocks.common.types import BoundingBox2D
from cv_blocks.events.mv_manager import MotionVecLogger
import copy

class EventAggregator(object):
    '''
    A class responsible for sending sequences to classifier
    It can merge, trim & prune MVs
    '''
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.n_frames_aggregate_mv_single = config.get('n_frames_aggregate_mv_single', 15)
            self.n_frames_aggregate_mv_multi = config.get('n_frames_aggregate_mv_multi', 10)
            self.n_frames_delay_before_send = config.get('n_frames_delay_before_send', 20)
            self.long_event_th = config.get('long_event_th', 20)
            self.max_frames_delay = config.get('max_frames_delay', 60)
            self.different_cams_frame_diff = config.get('different_cams_frame_diff', 5)
            self.mv_trim_method = config.get('mv_trim_method', 'uniform')
            self.mv_min_len = config.get('mv_min_len', 4)
            self.small_mv_min_len = config.get('small_mv_min_len', 3)
            self.mv_max_length = config.get('mv_max_length', 20)
            self.max_wnoise_ratio = config.get('max_wnoise_ratio', 0.75)
            self.same_traj_min_iou = config.get('same_traj_min_iou', 0.15)
            self.same_traj_min_dist = config.get('same_traj_min_dist', 0.15)
            self.same_box_min_iou = config.get('same_box_min_iou', 0.40)
            self.keep_event_n_frames = config.get('keep_event_n_frames', 50)
            self.merge_small_mv_conncetions = config.get('merge_small_mv_conncetions', True)


    def __init__(self, cam_names, md_module, event_config=dict()):
        self.cam_names = cam_names
        self.params = self.Config(event_config)
        self.pending_events = []
        self.sent_events = []
        self.event_queue = queue.Queue()
        self.md_module = md_module

    def reset(self):
        self.pending_events = []
        self.sent_events = []
        self.event_queue.queue.clear()
    
    def empty_sent_events(self):
        if len(self.sent_events)==0:
            return
        self.sent_events = [se for se in self.sent_events if \
            self.md_module.global_frame_num <= se['event_end'] + self.params.keep_event_n_frames]

    def is_event_done(self, event):
        # TODO - add all heuristics here
        frame_th = self.params.n_frames_aggregate_mv_multi if event['multi_cam'] else\
            self.params.n_frames_aggregate_mv_single
        # No delay for long events
        if len(event['movement_vector'])>=self.params.long_event_th:
            return True
        frame_diff = self.md_module.global_frame_num - event['event_frame']
        if (frame_diff >= max(frame_th, self.params.n_frames_delay_before_send) and not self.md_module.in_dynamic_state) or \
                frame_diff >= self.params.max_frames_delay:
            return True
        else:
            return False

    def assign_event_candidate(self, event_cand, cam_name, pending=True):
        test_events = self.pending_events if pending else self.sent_events
        if len(test_events)==0:
            return None
        # if event_cand should be assigned to existing_event, return idx
        for idx, existing_event in enumerate(test_events):
            if existing_event['event_direction']!=event_cand['event_direction']:
                continue
            both_single = not(existing_event['multi_cam'] | event_cand['multi_cam'])
            multi_single = existing_event['multi_cam'] ^ event_cand['multi_cam']
            same_cam = existing_event['initiating_cam']==cam_name
            # Both single-cam & different cam:
            # Check for connected MVs that were sent in different times 
            # And therefore were not connected
            if both_single and not same_cam and existing_event['activated_by_md'] and event_cand['activated_by_md']:
                a_connected_to_b = len([1 for cmv in event_cand['connected_mvs'] if cmv['id']==existing_event['mv_id']]) > 0
                b_connected_to_a = len([1 for cmv in existing_event['connected_mvs'] if cmv['id']==event_cand['mv_id']]) > 0
                if a_connected_to_b or b_connected_to_a:
                    return idx
            # Multi-cam & Single-cam or both singles from the same cam
            if multi_single or (both_single and same_cam):
                if self.similar_trajectory(existing_event, event_cand):
                    return idx
            # TODO
            # Single-cam & different cam - prune by event frame similarity
            elif (both_single and not same_cam):
                frame_diff_s = abs(existing_event['event_start'] - event_cand['event_start'])
                frame_diff_e = abs(existing_event['event_end'] - event_cand['event_end'])
                frame_diff = min(frame_diff_e, frame_diff_s)
                if frame_diff <= self.params.different_cams_frame_diff:
                    return idx

        return None
    
    def similar_trajectory(self, ev1, ev2):
        cam_name = ev1['initiating_cam'] if not ev1['multi_cam'] else ev2['initiating_cam']
        mv1 = [mv for mv in ev1['movement_vector'] if mv.cam_name==cam_name]
        mv2 = [mv for mv in ev2['movement_vector'] if mv.cam_name==cam_name]
        # Option 1: the end of one mv is the beginning of the other
        # Take into account also past frames that were removed
        if 'frame_history' in ev1:
            fnum1 = [fh['frame'] for fh in ev1['frame_history'] if fh['cam']==cam_name]
        else:
            fnum1 = []
        if 'frame_history' in ev2:
            fnum2 = [fh['frame'] for fh in ev2['frame_history'] if fh['cam']==cam_name]
        else:
            fnum2 = []

        fnum1 += [mv_el.global_frame_number for mv_el in mv1]
        fnum2 += [mv_el.global_frame_number for mv_el in mv2]
        if len(fnum1) == 0 or len(fnum2) == 0:
            print("Warning: illegal input in similar trajectory - ignored")
            return False
        continuous = False
        frame_sequence = False
        # TODO - also check no spatial overlap between 2 sequences
        if np.max(fnum1) <= np.min(fnum2):
            frame_sequence = len(fnum1)>1 and fnum1[-1]-fnum1[-2] in (1,2,3) and fnum2[0]-fnum1[-1] in (0, 1,2)
            continuous = BoundingBox2D.iou(mv1[-1].coords, mv2[0].coords) > self.params.same_box_min_iou
        elif np.max(fnum2) <= np.min(fnum1):
            frame_sequence = len(fnum2)>1 and fnum2[-1]-fnum2[-2] in (1,2,3) and fnum1[0]-fnum2[-1] in (0, 1,2)
            continuous = BoundingBox2D.iou(mv2[-1].coords, mv1[0].coords) > self.params.same_box_min_iou 
        if continuous or frame_sequence:
            return True
        # Option 3: connected mvs and very similar event time
        if self.params.merge_small_mv_conncetions:
            connected_ev = ev1 if ev1['multi_cam'] else ev2
            single_ev = ev1 if not ev1['multi_cam'] else ev2
            if connected_ev['initiating_cam']!=single_ev['initiating_cam'] \
                and connected_ev['activated_by_md'] and single_ev['activated_by_md']:
                c_to_s = len([1 for cmv in connected_ev['connected_mvs'] if cmv['id']==single_ev['mv_id']]) > 0
                s_to_c = len([1 for cmv in single_ev['connected_mvs'] if cmv['id']==connected_ev['mv_id']]) > 0
                if (c_to_s or s_to_c) and abs(ev1['event_frame'] - ev2['event_frame']) < 5:
                    return True
        
        # Option 3: double boxes of the same event
        # calculate centroid distances in matching frames
        distances = []
        ious = []
        fnum1 = [mv_el.global_frame_number for mv_el in mv1]
        fnum2 = [mv_el.global_frame_number for mv_el in mv2]
        for mv_el1 in mv1:
            if mv_el1.global_frame_number in fnum2:
                mv2_idx = fnum2.index(mv_el1.global_frame_number)
                distances.append(BoundingBox2D.dist(mv_el1.coords, mv2[mv2_idx].coords))
                ious.append(BoundingBox2D.iou(mv_el1.coords, mv2[mv2_idx].coords))
        if len(ious) == 0:
            return False
        mean_iou = np.array(ious).mean()
        mean_dist = np.array(distances).mean()

        return mean_iou > self.params.same_traj_min_iou or \
            mean_dist < self.params.same_traj_min_dist

            
    
    def step(self, event_candidate, stream_closed=False):
        # Empty sent events list if needed
        # ================================
        self.empty_sent_events()
        # Check if a sent event was a match,
        # in this case ignore the event
        # =====================================
        for cam_name in list(event_candidate.keys()):
            for event_cand in event_candidate[cam_name]:
                # for each candidate - decide if new (None) or existing(id)
                event_idx = self.assign_event_candidate(event_cand, cam_name, pending=False)
                if event_idx is not None:
                    print("Event Aggregator: ignoring event %d due to previously sent matched event" % event_cand['event_id'], flush=True)
                    if hasattr(self.md_module, 'event_indicator') and event_cand.get('early_indication_idx', None) is not None:
                        self.md_module.event_indicator.set_processing_status(False, event_cand['early_indication_idx'], reason='event_agg_reject_event')
                    event_candidate[cam_name] = []
                    return None
        # assign event candidates from all cams
        # =====================================
        for cam_name in list(event_candidate.keys()):
            for event_cand in event_candidate[cam_name]:
                # for each candidate - decide if new (None) or existing(id)
                event_idx = self.assign_event_candidate(event_cand, cam_name)
                if event_idx is not None:
                    print("Event Aggregator: combining events", flush=True)
                    self.add_to_event(event_cand, event_idx)
                else:
                    self.create_new_event(event_cand, cam_name)
            event_candidate[cam_name] = []
        # Send Event to que if ready (not more than one)
        # ==============================================
        ret_val = None
        idx_to_delete = None
        send_event = False
        for event_idx, event in enumerate(self.pending_events):
            if 'wait_before_sending_event' in event.keys() and event['wait_before_sending_event'] > 0:
                event['wait_before_sending_event'] -= 1
                continue
            if self.is_event_done(event) or stream_closed: #promote event to be done when video is over
                event['movement_vector'] = self.trim_mv_if_too_long(event['movement_vector'])
                idx_to_delete = event_idx
                small_item_candidate = event.get('small_item_candidate', False)
                thrown_item_candidate = event.get('thrown_item_candidate', False)
                min_len_mv_th = self.params.small_mv_min_len if small_item_candidate or thrown_item_candidate \
                    else self.params.mv_min_len
                if event['activated_by_cart_bottom'] or len(event['movement_vector']) >= min_len_mv_th:
                    self.event_queue.put(event)
                    self.sent_events.append(copy.deepcopy(event))
                    send_event = True
                    # Log event latency
                    if hasattr(self.md_module, 'latency_buffer'):
                        self.md_module.latency_buffer.append('t_aggregator_out', time.time(), event['event_frame'])
                else:
                    if hasattr(self.md_module, 'event_indicator') and event.get('early_indication_idx', None) is not None:
                        self.md_module.event_indicator.set_processing_status(False, event['early_indication_idx'], reason='event_agg_reject_event')
                break
        if send_event:
            ret_val = self.pending_events[idx_to_delete].copy()
        if idx_to_delete is not None:
            del self.pending_events[idx_to_delete]
        
        return ret_val


    def create_new_event(self, event_cand, cam_name):
        new_event = event_cand.copy()
        self.pending_events.append(new_event)

    def add_to_event(self, event_cand, event_idx):
        self.pending_events[event_idx]['movement_vector'] += event_cand['movement_vector']
        self.pending_events[event_idx]['activated_by_md'] = \
            self.pending_events[event_idx]['activated_by_md'] or event_cand['activated_by_md']
        self.pending_events[event_idx]['activated_by_cart_bottom'] = \
            self.pending_events[event_idx]['activated_by_cart_bottom'] or event_cand['activated_by_cart_bottom']
        if self.pending_events[event_idx]['initiating_cam'] != event_cand['initiating_cam']:
            self.pending_events[event_idx]['multi_cam'] = True
        if 'early_indication_idx' in event_cand:
            # Aggregate both indictations
            all_ind = []
            if hasattr(self.md_module, 'event_indicator') and event_cand['early_indication_idx'] is not None:
                all_ind.append(event_cand['early_indication_idx'])
            if hasattr(self.md_module, 'event_indicator') and self.pending_events[event_idx]['early_indication_idx'] is not None:
                all_ind.append(self.pending_events[event_idx]['early_indication_idx'])
            if len(all_ind)==0:
                self.pending_events[event_idx]['early_indication_idx'] = None
            elif len(all_ind)>0:
                self.pending_events[event_idx]['early_indication_idx'] = all_ind[0]
                if len(all_ind)>1:
                    self.md_module.event_indicator.set_processing_status(False, all_ind[1], reason='event_agg_add_to_event')


    def trim_mv_if_too_long(self, curr_mv):
        # Remove ignored items
        # Remove front images with large wnoise mask ratio or marked as ignore
        chosen_movement_vector = [mv for mv in curr_mv if (mv.wnoise_ratio is None or\
            mv.wnoise_ratio < self.params.max_wnoise_ratio) and mv.classify]

        vector_length = len(chosen_movement_vector)
        # First remove hands if mv is too long
        if config.DETECTOR_V2 and vector_length > self.params.mv_max_length:
            chosen_movement_vector = [mv for mv in chosen_movement_vector if mv.detector_conf == -1 or
                                      mv.source == 'CB' or mv.crop_type['name'] == 'product']
            vector_length = len(chosen_movement_vector)
        # Remove tracking boxes first
        if vector_length > self.params.mv_max_length:
            tracking_boxes = [mv for mv in chosen_movement_vector if mv.detector_conf == -1]
            chosen_movement_vector = [mv for mv in chosen_movement_vector if mv.detector_conf != -1]
            if len(chosen_movement_vector) < self.params.mv_max_length:
                missing_boxes = self.params.mv_max_length - len(chosen_movement_vector)
                chosen_tracking_vector = [tracking_boxes[index] for index in \
                     np.linspace(0, len(tracking_boxes), endpoint=False, num=missing_boxes, dtype=int)]
                chosen_movement_vector += chosen_tracking_vector
            vector_length = len(chosen_movement_vector)
        if vector_length > self.params.mv_max_length:
            if self.params.mv_trim_method=='uniform':
                orig_movement_vector = chosen_movement_vector.copy()
                chosen_movement_vector = [chosen_movement_vector[index] for index in \
                     np.linspace(0, vector_length, endpoint=False, num=self.params.mv_max_length, dtype=int)]
                for element in orig_movement_vector:
                    if element.source == 'CB' and element not in chosen_movement_vector:
                        chosen_movement_vector.append(element)
            elif self.params.mv_trim_method=='confidence':
                # Sort by confidence
                conf_idx = np.argsort(np.array([-mv.detector_conf for mv in chosen_movement_vector]))[:self.params.mv_max_length]
                chosen_movement_vector = [mv for i, mv in enumerate(chosen_movement_vector) if i in conf_idx]
            elif self.params.mv_trim_method=='detector_first':
                # prioritize detector 
                det_mv = [mv for mv in chosen_movement_vector if mv.detector_conf>0] 
                if len(det_mv) < self.params.mv_max_length:
                    track_n = self.params.mv_max_length - len(det_mv)
                    track_mv = [mv for mv in chosen_movement_vector if mv.detector_conf<0]
                    track_mv = [track_mv[index] for index in \
                        np.linspace(0, len(track_mv), endpoint=False, num=track_n, dtype=int)]
                    chosen_movement_vector = det_mv + track_mv
                else:
                    chosen_movement_vector = [det_mv[index] for index in \
                        np.linspace(0, len(det_mv), endpoint=False, num=self.params.mv_max_length, dtype=int)]


        return chosen_movement_vector 
    
