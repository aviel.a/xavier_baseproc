import cv2
from collections import deque
import os
import glob
import numpy as np

class PlaybackSingleCam(object):
    def __init__(self, stream_name, stream_path, sample_size_ms=40):
        self.stream_name = stream_name
        self.stream_path = stream_path
        self.sample_size_ms = sample_size_ms
        self.img_idx = 0
        self.is_img_list = isinstance(stream_path, (list, tuple))
        if not self.is_img_list:
            try:
                import imageio
                self.camera = imageio.get_reader(self.stream_path, 'ffmpeg')
                self.opencv_reader = False
            except:
                self.camera = cv2.VideoCapture(self.stream_path)
                self.opencv_reader = True


    def grab(self):
        grab_time_ms = self.sample_size_ms * self.img_idx
        self.img_idx += 1
        return grab_time_ms

    def retrieve(self):
        if self.is_img_list:
            if len(self.stream_path) > 0:
                img = cv2.imread(self.stream_path.pop(0))
                success = True
            else:
                img = None
                success = False
        else:
            if self.opencv_reader:
                success, img = self.camera.read()
            else:
                try:
                    img = cv2.cvtColor(self.camera.get_next_data(), cv2.COLOR_RGB2BGR)
                    success = True
                except:
                    img = None
                    success = False
        return success, img

    def read(self):
        self.grab()
        return self.retrieve()

    def open(self, noUse=None):
        self.camera.isOpened()

    def isOpened(self):
        return True

    def release(self):
        self.camera.release()

class PlaybackMultiCam(object):
    def __init__(self, camera_dict):
        print('Initializing playback Multi-cam')
        if not isinstance(camera_dict, dict):
            # Assign with default configuration
            camera_dict = PlaybackMultiCam.rec_name_to_cam_dict(camera_dict)
        self.is_real_time = False
        self.cameras = dict()
        self.cam_names = camera_dict.keys()
        for cam_name, cam_path in camera_dict.items():
            self.cameras[cam_name] = PlaybackSingleCam(cam_name,cam_path)
        self.grab_ts_queue = deque()
    
    @staticmethod
    def rec_name_to_cam_dict(rec_name):
        if os.path.isfile(os.path.join(rec_name, os.path.basename(rec_name) + '_L.avi')):
            # Capsule mode - video
            rec_name_no_suffix = os.path.join(rec_name, os.path.basename(rec_name))
            camera_dict = dict(left=rec_name_no_suffix +'_L.avi',
                                right=rec_name_no_suffix +'_R.avi',
                                back=rec_name_no_suffix +'_B.avi',
                            )
        elif os.path.isdir(rec_name):
            # Capsule mode - images
            camera_dict = dict(left=sorted(glob.glob(os.path.join(rec_name, 'img_left_*'))), 
                               right=sorted(glob.glob(os.path.join(rec_name, 'img_right_*'))),
                               back=sorted(glob.glob(os.path.join(rec_name, 'img_back_*'))),
            )
        else:
            # Regular video mode
            camera_dict = dict(left=rec_name +'_L.avi',
                                right=rec_name +'_R.avi',
                                back=rec_name +'_B.avi',
                            )
        return camera_dict
    
    def grab(self):
        grab_times = []
        for cam_name in self.cam_names:
            gtm = self.cameras[cam_name].grab()
            grab_times.append(gtm)
        self.grab_ts_queue.append(np.mean(grab_times))
        
    
    def read(self):
        valid = True
        all_ts = []
        cam_result = dict()
        for cam_name in self.cam_names:
            ret, img = self.cameras[cam_name].retrieve()
            cam_result[cam_name] = dict(ret=ret, img=img)
    
        if self.is_valid_frame(cam_result):
            cam_result['mean_ts'] = self.grab_ts_queue.popleft()
            return True, cam_result
        else:
            return False, cam_result
    
    def is_valid_frame(self, cam_result):
        valid = True
        for _, cam_data in cam_result.items():
            if not cam_data['ret']:
                return False
        return valid
    
    def pause(self):
        pass

    def resume(self):
        pass

if __name__ == '__main__':
    # Test playback configuration
    base_rec_path = '/home/walkout05/Desktop/movies/DS2p39_2019-11-19-19_48_18'
    camera_dict = dict(left=base_rec_path +'_L.avi',
                       right=base_rec_path +'_R.avi',
                       back=base_rec_path +'_B.avi',
                      )
    playback = PlaybackMultiCam(camera_dict)

    for i in range(1,100):
        t0 = time.time()
        ret, frame = playback.read()
        bas.grab()
        time.sleep(0.035)
        # cv2.imshow("f", frame)
        # cv2.waitKey(1)
        t1 = time.time()
        print("fps={:.3f}".format(1/((t1-t0))))