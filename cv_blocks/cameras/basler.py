import os
import time
import yaml
import cv2
import pypylon.pylon as py
from collections import deque
import numpy as np

from config import config
if config.NVIDIA_VISUAL_PROFILER:
    import cupy

"""
This class represents a single Basler camera object to use in the videoStereoStream object  
"""

tlFactory = py.TlFactory.GetInstance()

class ImageEventPrinter(py.ImageEventHandler):
    def OnImagesSkipped(self, camera, countOfSkippedImages):
        print("OnImagesSkipped event for device ", camera.GetDeviceInfo().GetModelName(), flush=True)
        print(countOfSkippedImages, " images have been skipped.", flush=True)
        print()

    def OnImageGrabbed(self, camera, grabResult):
        print("OnImageGrabbed event for device ", camera.GetDeviceInfo().GetModelName(), flush=True)

        # Image grabbed successfully?
        if grabResult.GrabSucceeded():
            print("SizeX: ", grabResult.GetWidth(), flush=True)
            print("SizeY: ", grabResult.GetHeight(), flush=True)
            img = grabResult.GetArray()
            print("Gray values of first row: ", img[0], flush=True)
            print()
        else:
            print("Error: ", grabResult.GetErrorCode(), grabResult.GetErrorDescription(), flush=True)

class BaslerSingleCam(object):
    def __init__(self, cam_id,
                 binning_value=2,
                 pixel_format='RGB8',
                 color_space='sRGB',
                 exposure_ns=1500,
                 software_trigger=True,
                 height=480,
                 width=640,
                 debug=False,
                 gain_once=False,
                 gain_db=10,
                 max_fps=25,
                 cam_cache_dir='/tmp/basler_cache'):
        self.cam_cache_dir = cam_cache_dir
        self.cam_id = cam_id
        self.software_trigger = software_trigger
        self.camera = BaslerSingleCam.get_cam_by_name(cam_id)
        # TODO - check influence on back cam FPS
        if self.software_trigger:
            self.camera.RegisterConfiguration(py.SoftwareTriggerConfiguration(),\
                py.RegistrationMode_ReplaceAll,py.Cleanup_Delete)
        self.camera.Open()
        self.camera.ExposureAuto.SetValue("Off")
        self.camera.ExposureTime.SetValue(exposure_ns)
        self.exposure_ns = exposure_ns
        print("exposure_ns = {}".format(self.exposure_ns), flush=True)
        self.camera.BslColorSpaceMode.SetValue(color_space)
        self.camera.BalanceWhiteAuto.SetValue("Off")
        self.need_wb_calibration = True
        self.max_wb_value = 7.98
        self.LoadWhiteBalance()
        self.gain_once = gain_once
        if self.gain_once:
            self.camera.GainAuto.SetValue('Once')
            self.gain = self.camera.Gain.GetValue()
        else:
            self.camera.GainAuto.SetValue('Off')
            self.camera.Gain.SetValue(gain_db)
            self.gain = gain_db
        self.camera.BinningHorizontal.ImposeMax(binning_value)
        self.camera.BinningVertical.ImposeMax(binning_value)
        self.camera.BinningHorizontal.SetValue(binning_value)
        self.camera.BinningVertical.SetValue(binning_value)
        self.camera.Height.SetValue(height)
        self.camera.Width.SetValue(width)
        # Set max Throughput limit for camera (byte/sec) 
        # relevant to non-sw trigger activation - currently disabled
        # max_throughput = int(height * width * 3 * max_fps)
        # self.camera.DeviceLinkThroughputLimitMode.SetValue('On')
        # self.camera.DeviceLinkThroughputLimit.SetValue(max_throughput)
        self.debug = debug
        self.max_ms_retrieve_wait = 0
        assert pixel_format in ('RGB8', 'YCbCr422_8')
        self.camera.PixelFormat = pixel_format
        if pixel_format=='RGB8':
            self.CAM2BGR_COLOR_CONVERT = cv2.COLOR_RGB2BGR
        elif pixel_format=='YCbCr422_8':
            self.CAM2BGR_COLOR_CONVERT = cv2.COLOR_YUV2BGR_YUY2
        else:
            raise NameError

        if self.debug:
            # Event printer
            self.camera.RegisterImageEventHandler(ImageEventPrinter(), py.RegistrationMode_Append, py.Cleanup_Delete)

        print("Basler camera started: {}".format(self.camera.GetDeviceInfo()), flush=True)
        if self.software_trigger:
            self.camera.StartGrabbing(py.GrabStrategy_OneByOne)
            # self.camera.StartGrabbing(py.GrabStrategy_OneByOne, py.GrabLoop_ProvidedByInstantCamera)
            # self.camera.StartGrabbing(py.GrabStrategy_LatestImages)
        else:
            self.camera.StartGrabbing(py.GrabStrategy_LatestImages)
    
    def resume(self):
        if self.software_trigger:
            self.camera.StartGrabbing(py.GrabStrategy_OneByOne)
        else:
            self.camera.StartGrabbing(py.GrabStrategy_LatestImages)
    
    def pause(self):
        self.camera.StopGrabbing()
        

    @staticmethod
    def get_cam_by_name(cam_id):
        devices = tlFactory.EnumerateDevices()
        cams = [py.InstantCamera(tlFactory.CreateDevice(d))\
             for d in devices if d.GetFriendlyName()==cam_id]
        assert len(cams)==1, 'Invalid or non-unique camera name: %s' % cam_id
        return cams[0]


    def grab(self):
        # if config.NVIDIA_VISUAL_PROFILER:
        #     cupy.cuda.nvtx.RangePush("inside-grab {}".format(self.cam_id), 3)
        if self.software_trigger:
            self.camera.ExecuteSoftwareTrigger()
        grab_time_ms = time.time() * 1000
        # if config.NVIDIA_VISUAL_PROFILER:
        #     cupy.cuda.nvtx.RangePop()
        return grab_time_ms

    def retrieve(self):
        img = None
        success = False
        if self.debug:
            print('Cam %s retrieve' % self.cam_id, flush=True)
            print('ready: ', self.camera.NumReadyBuffers.Value, flush=True)
        img_ready = self.camera.NumReadyBuffers and self.camera.NumReadyBuffers.Value >= 1
        wait_ms = 0
        while wait_ms < self.max_ms_retrieve_wait and not img_ready:
            time.sleep(1e-3)
            wait_ms += 1
            img_ready = self.camera.NumReadyBuffers and self.camera.NumReadyBuffers.Value >= 1
        if img_ready:
            if config.NVIDIA_VISUAL_PROFILER:
                cupy.cuda.nvtx.RangePush("inside-wait {}".format(self.cam_id), 9)
            if config.NVIDIA_VISUAL_PROFILER:
                cupy.cuda.nvtx.RangePop()
                cupy.cuda.nvtx.RangePush("inside-Retrieve {}, RBuf={}, QBuf={}".format(self.cam_id, self.camera.NumReadyBuffers.Value, self.camera.NumQueuedBuffers.Value), 1)
            camera_result = self.camera.RetrieveResult(1)
            if config.NVIDIA_VISUAL_PROFILER:
                cupy.cuda.nvtx.RangePop()
            if self.debug:
                print('Success: ', camera_result.GrabSucceeded(), flush=True) 
            if camera_result:
                try:
                    if camera_result.GrabSucceeded():
                        success = True
                        if config.NVIDIA_VISUAL_PROFILER:
                            cupy.cuda.nvtx.RangePush("inside-array {}".format(self.cam_id), 8)
                        img = camera_result.Array
                        if config.NVIDIA_VISUAL_PROFILER:
                            cupy.cuda.nvtx.RangePop()
                            cupy.cuda.nvtx.RangePush("inside-color {}".format(self.cam_id), 6)
                        img = cv2.cvtColor(img, self.CAM2BGR_COLOR_CONVERT)
                        if config.NVIDIA_VISUAL_PROFILER:
                            cupy.cuda.nvtx.RangePop()
                finally:
                    camera_result.Release()
        return success, img

    def read(self):
        self.grab()
        return self.retrieve()

    def open(self, noUse=None):
        return self.camera.IsOpen()  # TODO- TBD maybe need to open the camera only here

    def isOpened(self):
        return self.camera.IsOpen()

    def release(self):
        self.camera.StopGrabbing()
        self.camera.Close()

    def getNumReadyBuffers(self):
        return self.camera.NumReadyBuffers.Value

    # curr_imgs[1] = cv2.flip(curr_imgs[1], +1)


    def oneTimeWhiteBalanceAndSave(self):
        time.sleep(0.1)
        self.camera.BalanceWhiteAuto.SetValue("Once")
        #one call for grab & retrieve
        time.sleep(0.1)
        self.read()
        time.sleep(0.1)
        self.saveWhiteBalanceParams()

    def saveWhiteBalanceParams(self):
        '''Write current white balance params to file'''
        config_path = os.path.join(self.cam_cache_dir, '%s.yaml' % self.cam_id)
        self.wb_coefficients = dict()
        for color in ['Green', 'Blue', 'Red']:
            self.camera.BalanceRatioSelector.SetValue(color)
            self.wb_coefficients[color.lower()] = self.camera.BalanceRatio.GetValue()
        yaml.dump(self.wb_coefficients, open(config_path, 'w'))
        print('Saving White Balance params to %s' % config_path, flush=True)
        print(self.wb_coefficients, flush=True)

    def LoadWhiteBalance(self):
        if config.LOAD_WHITE_BALANCE_FROM_FILE == False:
            self.initialize_wb_parameters()
        else:
            '''read current white balance params from file or set it automatically if not found'''
            config_path = os.path.join(self.cam_cache_dir, '%s.yaml' % self.cam_id)
            if os.path.exists(config_path):
                wb_ratio = yaml.safe_load(open(config_path, 'r'))
                if wb_ratio is not None:
                    print('Loading White Balance params from %s' % config_path, flush=True)
                    print(wb_ratio, flush=True)
                    for color in ['Green', 'Blue', 'Red']:
                        self.camera.BalanceRatioSelector.SetValue(color)
                        self.camera.BalanceRatio.SetValue(wb_ratio[color.lower()])
                    self.wb_coefficients = wb_ratio
                    self.need_wb_calibration = False
                else:
                    self.initialize_wb_parameters()
                    self.saveWhiteBalanceParams()
            else:
                # set all parameters to 1 if no file exists
                self.initialize_wb_parameters()
                self.saveWhiteBalanceParams()

    def set_wb_params(self, wb_coefficients):
        for color in ['Green', 'Blue', 'Red']:
            self.camera.BalanceRatioSelector.SetValue(color)
            if wb_coefficients[color.lower()] > self.max_wb_value:
                print("\033[92mWB value of {:.2f} ({}) is above max ({:.2f}) ==> rounding down\033[00m".format(
                    wb_coefficients[color.lower()], color, self.max_wb_value), flush=True)
                wb_coefficients[color.lower()] = self.max_wb_value
            self.camera.BalanceRatio.SetValue(wb_coefficients[color.lower()])
        self.wb_coefficients = wb_coefficients
        self.saveWhiteBalanceParams()

    def read_wb_params(self):
        wb_coefficients = dict()
        for color in ['Green', 'Blue', 'Red']:
            self.camera.BalanceRatioSelector.SetValue(color)
            wb_coefficients[color.lower()] = self.camera.BalanceRatio.GetValue()
        return wb_coefficients

    def initialize_wb_parameters(self):
        for color in ['Green', 'Blue', 'Red']:
            self.camera.BalanceRatioSelector.SetValue(color)
            self.camera.BalanceRatio.SetValue(1)
        self.wb_coefficients = dict(green=1, blue=1, red=1)

    def read_exposure_time(self):
        return self.camera.ExposureTime.GetValue()

    def set_exposure_time(self, exposure_time):
        if exposure_time != self.exposure_ns:
            self.camera.ExposureTime.SetValue(exposure_time)
            self.exposure_ns = exposure_time

    def apply_gain_once(self):
        self.camera.GainAuto.SetValue('Once')
        self.gain = self.camera.Gain.GetValue()

    def read_gain(self):
        return self.gain

    def set_gain(self, gain):
        if gain != self.gain:
            self.camera.Gain.SetValue(gain)
            self.gain = gain


class BaslerMultiCam(object):
    def __init__(self, camera_dict,
                 max_camera_diff_ms=1.,
                 debug=False):
        print('Initializing balser Multi-cam', flush=True)
        self.is_real_time = True
        self.max_camera_diff_ms = max_camera_diff_ms
        self.cameras = dict()
        curr_dir = os.path.dirname(os.path.abspath(__file__))
        self.cam_cache_dir = os.path.join(curr_dir, 'cam_cache')
        os.makedirs(self.cam_cache_dir, exist_ok=True)
        self.cam_names = camera_dict.keys()
        for cam_name, cam_details in camera_dict.items():
            self.cameras[cam_name] = BaslerSingleCam(**cam_details, debug=debug, cam_cache_dir=self.cam_cache_dir)
        if 'right' in self.cam_names and 'left' in self.cam_names:
            self.equalize_gain_front()
        self.print_gain_value()
        self.grab_ts_queue = deque()
        self.all_triggers = [cam.camera.ExecuteSoftwareTrigger for _, cam in self.cameras.items() if cam.software_trigger]
    
    def release(self):
        for cam_name in self.cam_names:
            try:
                print('Shuting down camera %s: %s' % (cam_name, self.cameras[cam_name].cam_id), flush=True)
                self.cameras[cam_name].release()
            except:
                print('Shut Down failed!', flush=True)
    
    def grab_seq(self):
        grab_times = []
        for cam_name in self.cam_names:
            gtm = self.cameras[cam_name].grab()
            grab_times.append(gtm)
        self.grab_ts_queue.append(np.mean(grab_times))
    
    def grab(self, time_anchor=None):
        for trigger in self.all_triggers:
            trigger()
        grab_time_ms = time.time() * 1000
        # time_in_cycle_ms = (grab_time_ms - (time_anchor * 1e3)) % 50
        # jitter = min(time_in_cycle_ms, 50-time_in_cycle_ms)
        # if time_in_cycle_ms > 25: jitter = -jitter
        # if abs(jitter) > 1.0:
        #     print('Cameras grab jitter: %.2f' % jitter, flush=True)
        self.grab_ts_queue.append(grab_time_ms)
    
    def log_buffer_state(self):
        buff_state = dict()
        for cam_name in self.cam_names:
            try:
                buff_state[cam_name] = self.cameras[cam_name].getNumReadyBuffers()
            except:
                buff_state[cam_name] = None
        return buff_state
    
    def read(self):
        valid = True
        all_ts = []
        cam_result = dict()
        for cam_name in self.cam_names:
            ret, img = self.cameras[cam_name].retrieve()
            cam_result[cam_name] = dict(ret=ret, img=img)
    
        if self.is_valid_frame(cam_result):
            cam_result['mean_ts'] = self.grab_ts_queue.popleft()
            return True, cam_result
        else:
            return False, cam_result
    
    def is_valid_frame(self, cam_result):
        valid = True
        for _, cam_data in cam_result.items():
            if not cam_data['ret']:
                return False
        return valid

    def driver_wb(self):
        for cam_name in self.cam_names:
            self.cameras[cam_name].oneTimeWhiteBalanceAndSave()

    def read_wb_coefficients(self):
        coefficients = dict()
        for cam_name in self.cam_names:
            coefficients[cam_name] = self.cameras[cam_name].read_wb_params()
        return coefficients

    def set_new_wb_coefficients(self, wb_coefficients):
        for cam_name in self.cam_names:
            self.cameras[cam_name].set_wb_params(wb_coefficients[cam_name])

    def read_exposures(self):
        exposures = dict()
        for cam_name in self.cam_names:
            exposures[cam_name] = self.cameras[cam_name].read_exposure_time()
        return exposures

    def set_exposures(self, exposures):
        for cam_name in self.cam_names:
            self.cameras[cam_name].set_exposure_time(exposures[cam_name])

    def read_gains(self):
        gains = dict()
        for cam_name in self.cam_names:
            gains[cam_name] = self.cameras[cam_name].read_gain()
        return gains

    def set_gains(self, gains):
        for cam_name in self.cam_names:
            self.cameras[cam_name].set_gain(gains[cam_name])

    def apply_gain_once(self):
        for cam_name in self.cam_names:
            if cam_name == 'right' and 'left' in self.cam_names:
                continue
            if self.cameras[cam_name].gain_once:
                self.cameras[cam_name].apply_gain_once()
        if 'right' in self.cam_names and 'left' in self.cam_names:
            self.equalize_gain_front()
        self.print_gain_value()

    def equalize_gain_front(self):
        self.cameras['right'].set_gain(self.cameras['left'].read_gain())

    def print_gain_value(self):
        for cam_name in self.cam_names:
            print("gain for camera: {} is {}".format(cam_name, self.cameras[cam_name].read_gain()), flush=True)

    def pause(self):
        for cam_name in self.cam_names:
            self.cameras[cam_name].pause()

    def resume(self):
        for cam_name in self.cam_names:
            self.cameras[cam_name].resume()

if __name__ == '__main__':
    # Test basler configuration
    camera_dict = dict(left=dict(name='Basler daA1280-54uc (22594448)', pixel_format='RGB8'),
                       right=dict(name='Basler daA1280-54uc (22594469)', pixel_format='RGB8')
                    #    back=dict(name='TODO', pixel_format='YCbCr422_8')
                      )
    bas = BaslerMultiCam(camera_dict)
    bas.grab()
    time.sleep(0.2)

    for i in range(1,100):
        t0 = time.time()
        ret, frame = bas.read()
        bas.grab()
        time.sleep(0.035)
        # cv2.imshow("f", frame)
        # cv2.waitKey(1)
        t1 = time.time()
        print("fps={:.3f}".format(1/((t1-t0))))
