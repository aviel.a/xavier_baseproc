import pickle
from scipy.spatial.distance import cdist

class SimilarityValidator(object):
    def __init__(self, repr_info_file, debug=True, valid_th=0.5):
        self.repr_info_file = repr_info_file
        print('Opening product validation from %s' % repr_info_file, flush=True)
        self.repr_info = pickle.load(open(repr_info_file, 'rb'))
        self.generator = self.repr_info['generator']
        self.product_info = self.repr_info['product_info']
        self.debug = debug
        self.valid_th = valid_th
    
    def is_valid(self, score, product_code):
        # TODO - score per product code
        return score > self.valid_th
    @staticmethod
    def get_distances(v_a, v_b, agg_method='min'):
        assert v_a.shape[1]==v_b.shape[1]
        dists = cdist(v_a, v_b)
        if agg_method=='min':
            return dists.min(axis=0)
        else:
            raise NotImplementedError
    
    def get_product_info(self, product_name):
        if product_name not in self.product_info:
            return None
        else:
            return self.product_info[product_name]
    
    def get_product_names(self):
        return list(self.product_info.keys())
    
    def calc_validation_score(self, distances, product_info):
        '''
        Generate validation score
        Vanilla method - weighted average on simple features
        '''
        r_below_same_q50 = (distances < product_info['same_info']['q50'])
        r_below_same_q95 = (distances < product_info['same_info']['q95'])
        same_score = 0.5 * r_below_same_q50 + 0.5 * r_below_same_q95

        r_above_other_q5 = (distances > product_info['other_info']['q5'])
        r_above_other_q50 = (distances > product_info['other_info']['q50'])
        other_score = 0.5 * r_above_other_q5 + 0.5 * r_above_other_q50

        score_per_img = 0.5 * same_score + 0.5 * (1 - other_score)
        total_score = score_per_img.mean()
        if self.debug:
            print('SimValDebug: r<same q50: %.2f, r<same q95: %.2f, r>other q5: %.2f, r>other q50: %.2f' % \
            (r_below_same_q50.mean(), r_below_same_q95.mean(), r_above_other_q5.mean(), r_above_other_q50.mean()), flush=True)
            print('SimValDebug: same score: %.2f, other score: %.2f, total score: %.2f' % \
            (same_score.mean(), other_score.mean(), total_score), flush=True)
        return total_score, score_per_img

    def validate_event(self, event, product_name):
        '''
        Check if event is similar enough to product feature representatives
        Event ranking range is (0.-1.)
        1: high validation score, 0: low validation score
        event - classifier output for an event
        '''
        product_info = self.get_product_info(product_name)
        if product_info is None or 'sequence_descriptors' not in event:
            # TODO - unrecognized product - add event to dataset
            return None, None
        
        product_vecs = product_info['repr_desc']
        event_vecs = event['sequence_descriptors']

        # Calculate validation score
        print('SimValDebug: Validating is event is product: %s' % product_name, flush=True)
        distances = SimilarityValidator.get_distances(product_vecs, event_vecs)
        if self.debug:
            print('SimValDebug: Descriptor Distances: ', distances, flush=True)
        val_score, score_per_img = self.calc_validation_score(distances, product_info)

        return val_score, dict(distances=distances, scores=score_per_img)


if __name__=='__main__':
    similarity_info_file = '/home/walkout05/feature_space_experiments/cls_guardian_poc/output_poc/demo1750_01_circle_00082/product_repr.info'
    my_sim = SimilarityValidator(similarity_info_file)

    sample_product = '7290113302365'
    # Define classifier
    from cv_blocks.benchmarks.product_classifier.utils import MotionDetectLight
    from cv_blocks.events.event_classifier import EventClassifier
    md_module = MotionDetectLight()
    cls_model = 'demo1750_01_circle_00082_dual_dropout.onnx'
    event_classifier = EventClassifier(md_module, en_trt=True, model_name=cls_model, use_crop_descriptor=True)
    events_file = '/home/walkout05/cart_data/regression_pickles/2021-02-28_jumbo/events_info/shop_mode_log_shopping_journey_2021-01-15-11_25_35.pkl'
    from cv_blocks.misc.logger import BaseLogger
    logger = BaseLogger(True)
    el_list = logger.from_data(events_file)
    # Remove non-classified items
    el_list = [el['element'].data for el in el_list if el['type'] in ('event_aggregator_out')]

    # Validate single event
    for el in el_list:
        try:
            event_result = event_classifier.classify_sequence(el)
            event_score = my_sim.validate_event(event_result, sample_product)
            print('Event Score: %.3f' % event_score)
        except:
            pass