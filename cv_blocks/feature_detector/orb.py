import cv2

class OrbFeatureDetector(object):
    def __init__(self, n_levels=8,
                       scale_factor=1.2,
                       patch_size=31,
                       fast_th=20,
                       max_features=2000,
                       ):
        print('Initializing ORB Feature detector')
        self.can_extract = True
        self.feature_extractor = cv2.ORB_create()
        self.feature_extractor.setNLevels(n_levels)
        self.feature_extractor.setScaleFactor(scale_factor)
        self.feature_extractor.setPatchSize(patch_size)
        self.feature_extractor.setFastThreshold(fast_th) 
        self.feature_extractor.setMaxFeatures(max_features) 
    
    def detect(self, img, mask=None):
        kps = self.feature_extractor.detect(img, mask)
        return kps

    def extract(self, img, kps=None, mask=None):
        assert self.can_extract
        if kps==None:
            kps = self.detect(img,mask=mask)
        kps, desc = self.feature_extractor.compute(img, kps)
        return kps, desc