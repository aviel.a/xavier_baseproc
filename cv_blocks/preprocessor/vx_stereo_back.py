from streams.StreamTypes import StreamTypes
import numpy as np
'''
Camera configuration assuming stereo pair + back camera
'''
VX_PROCESSING_VERSION = "1.01"
DEFAULT_FRAME_SIZE = (480, 640)
class VXStereoBackPreprocessor(object):
    def __init__(self, stream, fs=DEFAULT_FRAME_SIZE):
        self.output_names = ['disparity', 'left_gray_resize', 'left_resize', 'back_resize', 'disparity_mask','left_remap']
        self.stream = stream
        self.fs = fs
        self.init_vx()
    
    def init_vx(self):
        import streams.stereo.vx._main_stereo_matching as msm
        if msm.get_version() != VX_PROCESSING_VERSION:
            raise NameError("VX version is {}, whereas version {} is expected. "
                            "please recompile !!".format(msm.get_version(), VX_PROCESSING_VERSION))
        cam_dir = self.stream.get_vx_cal_dir()
        self.vx_prcss = msm.init(self.fs[1], self.fs[0], self.fs[1], self.fs[0],cam_dir)
        self.msm = msm

    def capture(self, spatial_th):
        # TODO: handle spatial th inside module?
        frameL,frameR,frameB = self.stream.read()
        
        # Run VisionWorks
        # ===============
        vx_outputs = self.msm.process_stream(self.vx_prcss,False, frameL, frameR, frameB, spatial_th)
        vx_dict = {k:v for k, v in zip(self.output_names, vx_outputs)}

        return vx_dict

if __name__=='__main__':
    import re
    from streams.stereo.videoStreamStereo import videoStreamStereo
    import cv2
    realtime = False # you need to connect cameras for that
    if realtime:
        movie_name_wo_suffix = None
    else:
        video_capture = '/home/walkout05/walkout_dataset/test_videos/DS2p24/DS2p24_2019-09-17-09_32_14_L.avi'
        movie_name_wo_suffix = re.sub('_.\.avi$', '', video_capture)
    stream = videoStreamStereo(movie_name_wo_suffix, None)
    my_preproc = VXStereoBackPreprocessor(stream)
    spatial_th = np.zeros((320, 240), dtype=np.uint8)
    while True:
        out = my_preproc.capture(spatial_th)
        lb_vis = np.hstack((out['left_resize'], out['back_resize']))
        cv2.imshow('Left-Back', lb_vis)
        cv2.waitKey()