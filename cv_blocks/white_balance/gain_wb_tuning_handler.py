from cv_blocks.video_reader.base_proc import WhiteBalanceStatus, ExposureStatus, MonitorExposureState, MonitorWBState, ReplaceCBState
from config import config
from cv_blocks.white_balance.monitor_values import MonitoringStatus
try:
    from cart_blocks.cart_content_handler import ErrorType
except:
    pass


class TuningHandler:
    def __init__(self, md_module):
        self.last_wb_calib_frame = -1000
        self.last_gain_exp_calib_frame = -1000
        self.last_frame_monitored = -1000
        self.wb_status = WhiteBalanceStatus.Empty
        self.during_exp_tuning = False
        self.md_module = md_module
        self.full_calibration = False
        self.during_exposure_tuning = False
        self.during_wb_tuning = False
        self.monitor_every = 30
        self.start_monitoring_at_frame = 50
        self.extra_frames_to_wait_wb = 5
        self.is_wb_calib_needed = False
        self.is_exp_tuning_needed = False
        self.fd_th = 1.5
        self.total_que = 0
        self.hardware_que = self.md_module.stream.n_buffer_frames
        self.another_cycle_countdown = 0
        self.last_frame_number = self.md_module.global_frame_num
        self.replace_cb_image = False
        self.force_ref_change = False
        self.order_sent_to_cam_proc = False

    def init_full_calibration(self):
        self.full_calibration = True
        self.during_exposure_tuning = True
        self.during_wb_tuning = False
        self.times_wb = 0
        self.another_cycle_countdown = self.hardware_que
        self.wb_status = WhiteBalanceStatus.Empty
        self.md_module.stream.tune_exposure(init_event=True, frame_num=self.md_module.global_frame_num, save_images=True)
        self.order_sent_to_cam_proc = True

    def end_calibration(self):
        print("\033[35mFinished full wb cycle\033[00m", flush=True)
        self.full_calibration = False
        self.during_exposure_tuning = False
        self.during_wb_tuning = False
        self.replace_cb_image = True

    def exposure_gain_calibration(self):
        if self.another_cycle_countdown <= 0:
            self.another_cycle_countdown = self.hardware_que  # wait 5 frames and then try again
            print("\033[91mTuning exposure again [frame: {}]!\033[00m".format(self.md_module.global_frame_num), flush=True)
            self.md_module.stream.tune_exposure(frame_num=self.md_module.global_frame_num)
            self.order_sent_to_cam_proc = True
        else:
            exposure_status = self.md_module.stream.read_exposure_tuning_status()
            if exposure_status == ExposureStatus.Success:
                self.last_gain_exp_calib_frame = self.md_module.global_frame_num
                if self.full_calibration and self.times_wb == 0:
                    self.during_exposure_tuning = False
                    self.during_wb_tuning = True
                    self.wb_status = WhiteBalanceStatus.Empty
                    self.another_cycle_countdown = 0
                else:
                    self.md_module.stream.print_calibration_summary(frame_num=self.md_module.global_frame_num)
                    self.order_sent_to_cam_proc = True
                    self.force_ref_change = True
                    self.end_calibration()
            elif exposure_status == ExposureStatus.Failed:
                self.another_cycle_countdown = self.hardware_que - 1

    def wb_calibration(self):
        if self.another_cycle_countdown <= 0:
            print("\033[91mWhite balance calibration cycle #{} [frame: {}]!\033[00m".format(
                self.times_wb + 1, self.md_module.global_frame_num), flush=True)
            self.wb_status = WhiteBalanceStatus.Empty
            self.another_cycle_countdown = self.hardware_que + self.extra_frames_to_wait_wb
            self.md_module.stream.external_white_balance(frame_num=self.md_module.global_frame_num,
                                                         save_images=not self.times_wb)
            self.order_sent_to_cam_proc = True
        else:
            self.wb_status = self.md_module.stream.read_wb_status()
            if self.wb_status == WhiteBalanceStatus.Failed:
                if hasattr(self.md_module, 'gui'):
                    if hasattr(self.md_module, 'cart_content_handler'):
                        self.md_module.cart_content_handler.add_error(ErrorType.WhiteBalanceFailure)
                    else:
                        self.md_module.gui.make_cart_led_red(ErrorType.WhiteBalanceFailure.name)
                self.end_calibration()
            elif self.wb_status == WhiteBalanceStatus.Success:
                self.last_wb_calib_frame = self.md_module.global_frame_num
                self.times_wb += 1
                if self.times_wb >= 3:
                    # do one more gain/exposure calibration
                    self.during_exposure_tuning = True
                    self.another_cycle_countdown = self.hardware_que
                    # clean wb status in MD and stream
                    self.wb_status = WhiteBalanceStatus.Empty
                    self.md_module.stream.read_wb_status()
                    if hasattr(self.md_module, 'cart_content_handler'):
                        self.md_module.cart_content_handler.remove_error(ErrorType.WhiteBalanceFailure)

    def handle_full_calibration(self):
        if self.during_exposure_tuning:
            self.exposure_gain_calibration()
        if self.during_wb_tuning:
            self.wb_calibration()

    def need_full_calibration(self):
        if (config.WB_AT_START and self.md_module.global_frame_num >= 20 and self.last_gain_exp_calib_frame < 0) or \
                (config.AUTO_WB_EVERY is not None and self.md_module.global_frame_num % config.AUTO_WB_EVERY <= 1):
            if self.md_module.global_frame_num - self.last_gain_exp_calib_frame > self.monitor_every:
                return True
        return False

    def need_monitoring(self):
        if self.another_cycle_countdown > 0:
            return False
        reference_frame = max(self.last_frame_monitored, self.last_wb_calib_frame, self.last_gain_exp_calib_frame)
        if reference_frame > 0 and self.md_module.global_frame_num - reference_frame >= self.monitor_every and \
                self.not_event_frame():
            return True
        return False

    def not_event_frame(self):
        if self.md_module.maybe_during_event or self.md_module.frameDelta.mean() > self.fd_th or \
                self.md_module.frameDeltaBack.mean() > self.fd_th:
            return False
        return True

    def short_calibration_cycle(self):
        if self.not_event_frame() and self.another_cycle_countdown <= 0:
            if self.is_wb_calib_needed:
                # if self.new_online_calib:
                #     print("\033[35mStarting online wb cycle [frame: {}]!\033[00m".format(self.md_module.global_frame_num),
                #           flush=True)
                self.another_cycle_countdown = self.total_que + self.extra_frames_to_wait_wb
                self.last_wb_calib_frame = self.md_module.global_frame_num
                self.is_wb_calib_needed = False
                self.replace_cb_image = True
                self.md_module.stream.short_wb_cycle(self.new_online_calib, frame_num=self.md_module.global_frame_num)
                self.new_online_calib = False
                self.order_sent_to_cam_proc = True
            else:
                # if self.new_online_calib:
                #     print("\033[35mStarting online wb cycle [frame: {}]!\033[00m".format(self.md_module.global_frame_num),
                #           flush=True)
                self.another_cycle_countdown = self.total_que
                self.last_gain_exp_calib_frame = self.md_module.global_frame_num
                self.is_exp_tuning_needed = False
                self.md_module.stream.short_exp_cycle(self.new_online_calib, frame_num=self.md_module.global_frame_num)
                self.new_online_calib = False
                self.order_sent_to_cam_proc = True

    def step(self):
        self.order_sent_to_cam_proc = False
        self.another_cycle_countdown -= (self.md_module.global_frame_num - self.last_frame_number)
        self.last_frame_number = self.md_module.global_frame_num
        if self.full_calibration:
            self.handle_full_calibration()
        elif self.need_full_calibration():
            print("\033[35mStarting full wb cycle [frame: {}]!\033[00m".format(self.md_module.global_frame_num),
                  flush=True)
            self.init_full_calibration()
        elif hasattr(self.md_module, 'vx_prcss') and hasattr(self.md_module, 'cart_bottom_detector'):  # monitor values - not in data mode!
            if not self.is_wb_calib_needed and not self.is_exp_tuning_needed and self.need_monitoring():
                # print("\033[91mMonitoring templates values at frame: {}!\033[00m".format(self.md_module.global_frame_num),
                #       flush=True)
                self.last_frame_monitored = self.md_module.global_frame_num
                self.md_module.stream.monitor_values(self.md_module.global_frame_num)
                self.order_sent_to_cam_proc = True
            elif self.md_module.global_frame_num >= self.start_monitoring_at_frame:
                if self.is_wb_calib_needed or self.is_exp_tuning_needed:
                    self.short_calibration_cycle()
                else:
                    wb_calib = self.md_module.stream.is_wb_calib_needed()
                    self.is_wb_calib_needed, self.total_que = wb_calib[0] == MonitorWBState.NeedCalibration,  wb_calib[1]
                    if self.is_wb_calib_needed:
                        self.new_online_calib = wb_calib[2] == MonitoringStatus.NewCalib
                        # print("\033[91mInitiating 1 time WB triggered by monitored templates values!\033[00m", flush=True)
                        self.short_calibration_cycle()
                    else:
                        exp_tuning = self.md_module.stream.is_exp_tuning_needed()
                        self.is_exp_tuning_needed, self.total_que = exp_tuning[0] == MonitorExposureState.NeedCalibration, exp_tuning[1]
                        if self.is_exp_tuning_needed:
                            self.new_online_calib = exp_tuning[2] == MonitoringStatus.NewCalib
                            # print("\033[91mInitiating 1 time exp triggered by monitored templates values!\033[00m", flush=True)
                            self.short_calibration_cycle()
        if self.replace_cb_image and self.another_cycle_countdown <= 0 and not self.md_module.maybe_during_event and \
                self.md_module.stream.read_replace_cb_state()==ReplaceCBState.ShouldReplace:
            print("\033[92mreplacing cb reference image at frame {}\033[00m".format(self.md_module.global_frame_num))
            self.replace_cb_image = False
            force_ref_change = self.force_ref_change
            self.force_ref_change = False
            return force_ref_change, True, self.order_sent_to_cam_proc
        return False, False, self.order_sent_to_cam_proc

