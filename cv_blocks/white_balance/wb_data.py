from misc import print_formatted_message
import os
import pickle
from config import config


class WbLogger:
    def __init__(self, file_name):
        self.file_name = file_name
        self.open()

    def reset(self, new_file_name):
        self.file_name = new_file_name
        self.close()
        self.open()

    def open(self):
        os.makedirs(os.path.dirname(self.file_name), exist_ok=True)
        print('\033[93m', 'Opening WB Logger', '\033[0m', flush=True)
        self.logger = open(self.file_name, 'wb')

    def close(self):
        print('\033[93m','Closing WB Logger','\033[0m', flush=True)
        if hasattr(self, 'logger'):
            print('\033[93m','Wrote capsule to %s' % self.file_name,'\033[0m', flush=True)
            self.logger.close()

    def log(self, data, frame=None):
        print('\033[93m','Writing image to WB Logger: frame=%s' % (str(frame)),'\033[0m')
        capsule = dict(data=data, frame=frame)
        pickle.dump(capsule, self.logger, protocol=pickle.HIGHEST_PROTOCOL)


class WbData:
    def __init__(self, time_info=None, save=False):
        self.print_failure = True
        if save:
            file_name = os.path.join(config.MOVIES_PATH, time_info['date'],
                                     'templates_' + time_info['timestamp'] + '.pkl')
            self.wb_logger = WbLogger(file_name)

    def new_calib(self, calib_type, frame_num):
        self.steps = dict()
        self.calib_type = calib_type
        self.monitored_values = dict()
        self.starting_frame = frame_num

    def add_monitored_values(self, data):
        self.monitored_values[len(self.monitored_values)+1] = data

    def add_calibration_step(self, data):
        for camera in data.keys():
            if 'rgb_values' in data[camera].keys():
                for key in data[camera].keys():
                    for color in data[camera][key].keys():
                        data[camera][key][color] = round(data[camera][key][color], 2)
        self.steps[len(self.steps)+1] = data

    def clear(self):
        self.steps = dict()
        self.monitored_values = dict()

    def end_calib(self, status, frame_num):
        if status == 'success' and len(self.steps.keys()) == 0:
            return
        if "failed" in status and self.calib_type == 'online_wb':
            if not self.print_failure:
                self.clear()
                return
            self.print_failure = False
        else:
            self.print_failure = True
        frames = int(frame_num - self.starting_frame)
        data = dict(event_type='wb_calibration', event_id=-1,
                    event_payload=dict(status=status, calib_type=self.calib_type, total_steps=len(self.steps),
                                       total_frames=frames, steps=self.steps, monitored_values=self.monitored_values))
        print_formatted_message(data)
        self.clear()

if __name__ == '__main__':
    # unpack pickle file containing images
    import cv2
    data_dir = "/home/walkout03/Desktop/input/"
    output_dir = "/home/walkout03/Desktop/output_templates"
    os.makedirs(output_dir, exist_ok=True)
    for file in os.listdir(data_dir):
        el_list = []
        data_file = os.path.join(data_dir, file)
        with open(data_file, 'rb') as fr:
            try:
                while True:
                    obj = pickle.load(fr)
                    el_list.append(dict(images=obj['data'], frame=obj['frame']))
            except EOFError:
                pass
        for i, el in enumerate(el_list):
            for key, val in el['images'].items():
                image_name = file.split('.pkl')[0] + "_img_" + str(i) + "_frame_{}_".format(el['frame']) + key + ".jpg"
                cv2.imwrite(os.path.join(output_dir, image_name), val)
