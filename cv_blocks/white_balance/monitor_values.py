import matplotlib.pyplot as plt
import numpy as np
import enum


class MonitoringStatus(enum.IntEnum):
    NoCalib = 0
    OngoingCalib = 1
    NewCalib = 2
    CalibEnded = 3


class MonitorValues:
    def __init__(self):
        self.mean_values = dict(left=[], back=[])
        self.mean_channels = dict(left=dict(r=[], g=[], b=[]),
                                  right=dict(r=[], g=[], b=[]),
                                  back=dict(r=[], g=[], b=[]))
        self.color_MSE = dict(left=[], back=[])
        self.color_th = 0.05
        self.means_ME = dict(left=[], back=[])
        self.means_th = 0.12
        self.means_target = 140
        self.min_length = 2
        self.max_diff = 0.2  # diff bigger than this between max and min elements will not trigger new calibration
        self.debug = False
        self.since_last_calib = self.min_length

    def step(self, mean, camera, cropped_images):
        self.mean_values[camera].append(mean)
        self.means_ME[camera].append(abs((mean - self.means_target) / self.means_target))
        mean_b, mean_g, mean_r = 0, 0, 0
        for cropped_image in cropped_images:
            mean_b += np.average(cropped_image[:, :, 0])
            mean_g += np.average(cropped_image[:, :, 1])
            mean_r += np.average(cropped_image[:, :, 2])
        mean_b /= len(cropped_images)
        mean_g /= len(cropped_images)
        mean_r /= len(cropped_images)
        self.mean_channels[camera]['b'].append(mean_b)
        self.mean_channels[camera]['g'].append(mean_g)
        self.mean_channels[camera]['r'].append(mean_r)
        self.color_MSE[camera].append(np.sqrt(pow((mean_b - mean) / mean, 2) +
                                              pow((mean_g - mean) / mean, 2) +
                                              pow((mean_r - mean) / mean, 2)))
        if self.debug:
            print("monitoring brief:\ncam: {}\nmean: {}, ME: {}\nr: {}, g: {}, b: {}, MSE: {}".format(
                camera, mean, self.means_ME[camera][-1], self.mean_channels[camera]['r'][-1],
                self.mean_channels[camera]['g'][-1], self.mean_channels[camera]['b'][-1], self.color_MSE[camera][-1]), flush=True)

    def status(self):
        for camera in self.color_MSE.keys():
            if len(self.color_MSE[camera]) < self.min_length:
                return False, False, MonitoringStatus.NoCalib
        need_wb, need_exp_tuning = False, False
        for camera in self.color_MSE.keys():
            above_threshold = [k for k in self.color_MSE[camera][-self.min_length:] if k > self.color_th]
            if len(above_threshold) == self.min_length:
                diff = max(above_threshold) / min(above_threshold) - 1
                if self.debug:
                    print("High color MSE for cam {}, values: {:.3f} and {:.3f}".format(camera,
                                                                                        self.color_MSE[camera][-2],
                                                                                        self.color_MSE[camera][-1]),
                      flush=True)
                if diff < self.max_diff:
                    need_wb = True
                elif self.debug:
                    print("Big diff between high color MSE of {}%".format(int(diff*100)), flush=True)
        for camera in self.means_ME.keys():
            above_threshold = [k for k in self.means_ME[camera][-self.min_length:] if k > self.means_th]
            if len(above_threshold) == self.min_length:
                diff = max(above_threshold) / min(above_threshold) - 1
                if self.debug:
                    print("High means ME for cam {}, values: {:.3f} and {:.3f}".format(camera,
                                                                                       self.means_ME[camera][-2],
                                                                                       self.means_ME[camera][-1]),
                      flush=True)
                if diff < self.max_diff:
                    need_exp_tuning = True
                elif self.debug:
                    print("Big diff between means ME of {}%".format(int(diff*100)), flush=True)
        if need_wb or need_exp_tuning:
            if self.since_last_calib > self.min_length:
                calib_status = MonitoringStatus.NewCalib
            else:
                calib_status = MonitoringStatus.OngoingCalib
            self.since_last_calib = 0
        else:
            self.since_last_calib += 1
            if self.since_last_calib == self.min_length + 1:
                calib_status = MonitoringStatus.CalibEnded
            else:
                calib_status = MonitoringStatus.NoCalib
        return need_wb, need_exp_tuning, calib_status

    def get_last(self):
        values = dict()
        for camera in self.mean_values.keys():
            values[camera] = dict()
            if len(self.mean_values[camera]) >= self.min_length:
                values[camera]['means_ME'] = round(float(np.mean(self.means_ME[camera][-self.min_length:])), 3)
                values[camera]['color_MSE'] = round(float(np.mean(self.color_MSE[camera][-self.min_length:])), 3)
            else:
                values[camera]['means_ME'] = None
                values[camera]['color_MSE'] = None
        return values

    def end_calib(self):
        self.since_last_calib = self.min_length + 1

    def visualize(self, plot1=None, plot2=None, step=10, max_points=100):
        fig1, (ax1_left, ax1_back) = plt.subplots(nrows=2, ncols=1) if plot1 is None else plot1
        fig2, (ax2_left, ax2_back) = plt.subplots(nrows=2, ncols=1) if plot2 is None else plot2
        ax1_left.cla(), ax1_back.cla(), ax2_left.cla(), ax2_back.cla()
        fig1.suptitle("Means MSE", fontsize=16)
        fig2.suptitle("Colors MSE", fontsize=16)
        x = np.arange(max(1, len(self.means_ME['left']) - max_points + 1) * step, len(self.means_ME['left']) * step + 1, step)
        ax1_left.plot(x, self.means_ME['left'][-max_points:], color='green', marker='o')
        ax1_back.plot(x, self.means_ME['back'][-max_points:], color='red', marker='o')
        ax1_left.set_title("left"), ax1_back.set_title("back")
        ax2_left.plot(x, self.color_MSE['left'][-max_points:], color='green', marker='o')
        ax2_back.plot(x, self.color_MSE['back'][-max_points:], color='red', marker='o')
        ax2_left.set_title("left"), ax2_back.set_title("back")
        print("\033[92mMeans MSE left: {}\033[00m".format(self.means_ME['left'][-1]))
        print("\033[92mMeans MSE back: {}\033[00m".format(self.means_ME['back'][-1]))
        print("\033[92mColors MSE left: {}\033[00m".format(self.color_MSE['left'][-1]))
        print("\033[92mColors MSE back: {}\033[00m".format(self.color_MSE['back'][-1]))
        plt.pause(0.001)
        return (fig1, (ax1_left, ax1_back)), (fig2, (ax2_left, ax2_back))