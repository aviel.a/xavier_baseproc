import numpy as np
import cv2


def grey_world(cropped_images, old_wb_params, debug=False):
    bgr = dict(blue=0, green=0, red=0)
    total_pixels = 0
    for cropped_image in cropped_images:
        image, pixels_num = mask_irelevant_pixels(image=cropped_image)
        total_pixels += pixels_num
        image = image.transpose(2, 0, 1).astype(np.uint32)
        bgr['blue'] += np.sum(image[0])
        bgr['green'] += np.sum(image[1])
        bgr['red'] += np.sum(image[2])
    if total_pixels < 100:
        print("calibrating on only {} pixels!!!".format(total_pixels))
    colors_means = dict()
    for key, value in bgr.items():
        bgr[key] = bgr[key] / total_pixels
        colors_means[key] = bgr[key]
        bgr[key] /= old_wb_params[key]
    highest_color = max(bgr.values())
    for key in bgr.keys():
        bgr[key] = highest_color / bgr[key]
    if debug:
        return bgr, colors_means, total_pixels
    return bgr, colors_means


def mask_irelevant_pixels(image, th_low=75, th_high=245):
    mask_saturated = (image[:,:,0] < th_high) * (image[:,:,1] < th_high) * (image[:,:,2] < th_high)# remove saturated pixels
    mask_unexposed = (image[:,:,0] > th_low) * (image[:,:,1] > th_low) * (image[:,:,2] > th_low) # remove low intensity pixels
    while np.sum(mask_unexposed) < 50: # reduce threshold if intensity is really low
        th_low -= 10
        print("reducing threshold to {}".format(th_low))
        mask_unexposed = (image[:, :, 0] > th_low) * (image[:, :, 1] > th_low) * (image[:, :, 2] > th_low)
    mask = (mask_unexposed * mask_saturated * 255).astype(np.uint8)
    masked_image = cv2.bitwise_and(image, image, mask=mask)
    relevant_pixels_count = np.count_nonzero(mask)
    return masked_image, relevant_pixels_count

if __name__ == '__main__':
    import os
    import pandas as pd
    dir = "/home/walkout03/Desktop/output_templates2/back_3rd_movie"
    cropped_images = []
    for i in range(1, 6):
        image_name = os.path.join(dir, "crop_{}.jpg".format(i))
        if os.path.exists(image_name):
            cropped_images.append(cv2.imread(image_name))
    old_wb_params = {'blue': 1.0, 'green': 1.0, 'red': 1.0}
    gt = {'green': 1.0, 'blue': 1.125, 'red': 1.125}

    report = pd.DataFrame(columns=('crop_num', 'valid_pix', 'tot_pix', 'r_mean', 'g_mean', 'b_mean',
                                   'r_wb', 'g_wb', 'b_wb'))
    for cropped_image in cropped_images:
        new_wb_params, color_means, valid_pixels = grey_world([cropped_image], old_wb_params, debug=True)
        report = report.append(pd.Series(), ignore_index=True)
        index = report.shape[0] - 1
        report['crop_num'][index] = index
        report['valid_pix'][index] = valid_pixels
        report['tot_pix'][index] = int(cropped_image.size / 3)
        report['r_mean'][index] = round(color_means['red'], 0)
        report['g_mean'][index] = round(color_means['green'], 0)
        report['b_mean'][index] = round(color_means['blue'], 0)
        report['r_wb'][index] = round(new_wb_params['red'], 3)
        report['g_wb'][index] = round(new_wb_params['green'], 3)
        report['b_wb'][index] = round(new_wb_params['blue'], 3)
    report = report.append(pd.Series(), ignore_index=True)
    index = report.shape[0] - 1
    mean = report.mean()
    for key in report.mean().keys():
        if key == 'crop_num':
            report[key][index] = 'average'
        elif key in ('r_wb', 'g_wb', 'b_wb'):
            report[key][index] = round(mean[key], 3)
        else:
            report[key][index] = round(mean[key], 0)
    report = report.append(pd.Series(), ignore_index=True)
    index = report.shape[0] - 1
    report['crop_num'][index] = 'gt'
    report['r_wb'][index] = round(gt['red'], 3)
    report['g_wb'][index] = round(gt['green'], 3)
    report['b_wb'][index] = round(gt['blue'], 3)
    report.to_csv(os.path.join(dir, 'report.csv'), index=False)