import cv2
import os
import numpy as np
from config import config
import misc
import yaml


class TemplateMatching:
    def __init__(self, camera_id=config.CAMERA_ID, monitor_values=False):
        self.camera_id = camera_id
        self.templates_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'templates')
        self.get_templates()
        self.load_params_from_config()
        self.comparison_method = cv2.TM_CCOEFF_NORMED
        if monitor_values:
            from cv_blocks.white_balance.monitor_values import MonitorValues
            self.monitor = MonitorValues()

    def match(self, camera, image, couples=True, force_range=None, debug=True):
        if tuple(image.shape[:2])!=tuple(self.img_shape[::-1]):
            image = cv2.resize(image, self.img_shape)
        matched_images = []
        matched_coords = []
        crops = self.crops_coordinates[camera]
        templates = self.templates[camera]
        threshold = self.thresholds[camera]
        shapes = self.shapes[camera]
        if couples:
            couples_data = dict()
        for loc, coords in crops.items():
            is_first_calib = self.is_first_calibration[camera][loc]
            search_range = self.init_search_range if is_first_calib else self.search_range
            if force_range is not None:
                search_range = force_range
            cropped_image = image[coords['ymin'] - search_range: coords['ymin'] + shapes[loc][0] + search_range,
                                  coords['xmin'] - search_range: coords['xmin'] + shapes[loc][1] + search_range, :]

            res = cv2.matchTemplate(cropped_image, templates[loc], self.comparison_method)
            if couples:
                couples_data[loc] = dict(confidences=res, search_range=search_range, shapes=shapes[loc],
                                         is_first_calib=is_first_calib, coords=coords)
                continue
            max_conf = np.max(res)
            if debug:
                print("camera: {}, score: {}".format(camera, max_conf))
            if max_conf >= threshold:
                loc_pt = np.where(res == max_conf)
                for pt in zip(*loc_pt[::-1]):
                    coord = (pt[1] + coords['ymin'] - search_range,
                             pt[1] + coords['ymin'] - search_range + shapes[loc][0],
                             pt[0] + coords['xmin'] - search_range,
                             pt[0] + coords['xmin'] - search_range + shapes[loc][1])
                    if is_first_calib:
                        self.is_first_calibration[camera][loc] = False
                        self.crops_coordinates[camera][loc]['ymin'] = coord[0]
                        self.crops_coordinates[camera][loc]['xmin'] = coord[2]
                    matched_images.append(image[coord[0]:coord[1], coord[2]:coord[3], :])
                    matched_coords.append(coord)
                    break
        if couples:
            distance = (crops['up']['xmin'] - crops['down']['xmin'], crops['up']['ymin'] - crops['down']['ymin'])
            coords = self.choose_right_distanced_crops(couples_data, distance, threshold, camera, debug=debug)
            for loc, coord in coords.items():
                if self.is_first_calibration[camera][loc]:
                    self.is_first_calibration[camera][loc] = False
                    self.crops_coordinates[camera][loc]['ymin'] = coord[0]
                    self.crops_coordinates[camera][loc]['xmin'] = coord[2]
                matched_images.append(image[coord[0]:coord[1], coord[2]:coord[3], :])
                matched_coords.append(coord)

        return matched_images, matched_coords

    def choose_right_distanced_crops(self, couples_data, distance, threshold, camera, debug=True):
        diff_allowed = 5
        sorted_args_up = np.argsort(couples_data['up']['confidences'].flatten())[::-1]
        shape_up = couples_data['up']['search_range'] * 2 + 1
        sorted_args_down = np.argsort(couples_data['down']['confidences'].flatten())[::-1]
        shape_down = couples_data['down']['search_range'] * 2 + 1
        best_conf = None
        best_crops = dict()
        scores = dict()
        for i in range(len(sorted_args_up)):
            up_row, up_col = np.unravel_index(sorted_args_up[i], (shape_up, shape_up))
            conf_up = couples_data['up']['confidences'][up_row, up_col]
            if conf_up < threshold - 0.05:
                break
            for j in range(len(sorted_args_down)):
                down_row, down_col = np.unravel_index(sorted_args_down[j], (shape_down, shape_down))
                conf_down = couples_data['down']['confidences'][down_row, down_col]
                if (conf_down < threshold and not (conf_up + conf_down >= threshold * 2 + 0.05 and conf_down >= threshold - 0.05 and conf_up >= threshold - 0.05)) or \
                        (best_conf is not None and conf_up + conf_down < best_conf):
                    break
                x_up = couples_data['up']['coords']['xmin'] - couples_data['up']['search_range'] + up_col
                y_up = couples_data['up']['coords']['ymin'] - couples_data['up']['search_range'] + up_row
                x_down = couples_data['down']['coords']['xmin'] - couples_data['down']['search_range'] + down_col
                y_down = couples_data['down']['coords']['ymin'] - couples_data['down']['search_range'] + down_row
                if abs(x_up - x_down - distance[0]) <= diff_allowed and abs(y_up - y_down - distance[1]) <= diff_allowed:
                    best_conf = conf_up + conf_down
                    best_crops = dict(up=(y_up, y_up + couples_data['up']['shapes'][0],
                                          x_up, x_up + couples_data['up']['shapes'][1]),
                                      down=(y_down, y_down + couples_data['down']['shapes'][0],
                                            x_down, x_down + couples_data['down']['shapes'][1]))
                    scores = dict(up=conf_up, down=conf_down)
                    break
            if j == 0:
                break
        if scores:  # if coupled templates are found
            if debug:
                print("Coupled templates for camera {} were found!".format(camera), flush=True)
                print("score for up template: {}".format(scores['up']), flush=True)
                print("score for down template: {}".format(scores['down']), flush=True)
        else:  # else choose very confident templates not far from origin
            if debug:
                print("\033[91mCoupled templates for camera {} were not found!\033[00m".format(camera))
                print("Best score for up template: {}".format(np.max(couples_data['up']['confidences'])), flush=True)
            for i in range(len(sorted_args_up)):
                best_up_row, best_up_col = np.unravel_index(sorted_args_up[i], (shape_up, shape_up))
                if couples_data['up']['confidences'][best_up_row, best_up_col] < threshold:
                    break
                if abs(best_up_col - couples_data['up']['search_range']) <= 5 and \
                        abs(best_up_row - couples_data['up']['search_range']) <= 5:
                    if debug:
                        print("Choosing up template due to high confidence and proximity to initial coordinates", flush=True)
                    x_up = couples_data['up']['coords']['xmin'] - couples_data['up']['search_range'] + best_up_col
                    y_up = couples_data['up']['coords']['ymin'] - couples_data['up']['search_range'] + best_up_row
                    best_crops['up'] = (y_up, y_up + couples_data['up']['shapes'][0],
                                        x_up, x_up + couples_data['up']['shapes'][1])
                    break
            if debug:
                print("Best score for down template: {}".format(np.max(couples_data['down']['confidences'])), flush=True)
            for j in range(len(sorted_args_down)):
                best_down_row, best_down_col = np.unravel_index(sorted_args_down[j], (shape_down, shape_down))
                if couples_data['down']['confidences'][best_down_row, best_down_col] < threshold:
                    break
                if abs(best_down_col - couples_data['down']['search_range']) <= 5 and \
                        abs(best_down_row - couples_data['down']['search_range']) <= 5:
                    if debug:
                        print("Choosing down template due to high confidence and proximity to initial coordinates", flush=True)
                    x_down = couples_data['down']['coords']['xmin'] - couples_data['down']['search_range'] + best_down_col
                    y_down = couples_data['down']['coords']['ymin'] - couples_data['down']['search_range'] + best_down_row
                    best_crops['down'] = (y_down, y_down + couples_data['down']['shapes'][0],
                                          x_down, x_down + couples_data['down']['shapes'][1])
                    break
        return best_crops

    def print_coords(self, image, coords):
        # coords is a list of (ymin, ymax, xmin, xmax) regarding the full size not rotated image
        for coord in coords:
            ymin, ymax, xmin, xmax = coord
            cv2.rectangle(image, (xmin, ymin), (xmax, ymax), (0, 0, 255), 2)

    def get_templates(self):
        local_addr = os.path.join(os.path.dirname(__file__).replace('white_balance', 'calibration'), 'models', self.camera_id, 'templates')
        files_list = {'back': {'down': "cam_b_d.jpg", 'up': "cam_b_u.jpg"},
                      'left': {'down': "cam_l_d.jpg", 'up': "cam_l_u.jpg"}}
        self.templates, self.shapes = dict(), dict()
        for camera in files_list.keys():
            self.templates[camera], self.shapes[camera] = dict(), dict()
            for loc, file_name in files_list[camera].items():
                assert os.path.exists(os.path.join(local_addr, file_name)), "No templates are found in calibration model dir!"
                template = cv2.imread(os.path.join(local_addr, file_name))
                self.templates[camera][loc] = template
                self.shapes[camera][loc] = template.shape

    def load_params_from_config(self):
        config_path = os.path.join(os.path.dirname(__file__).replace('white_balance', 'calibration'), 'models', self.camera_id, 'cart_config.yaml')
        conf = yaml.safe_load(open(config_path))
        self.thresholds = conf['templates']['thresholds']
        self.init_search_range = conf['templates'].get('init_search_range', 20)
        self.search_range = conf['templates'].get('search_range', 5)
        self.img_shape = conf['templates'].get('img_shape', (640, 480))
        self.crops_coordinates = dict()
        self.is_first_calibration = dict()
        for camera in conf['templates']['coordinates'].keys():
            self.crops_coordinates[camera] = dict()
            self.is_first_calibration[camera] = dict()
            for template_loc in conf['templates']['coordinates'][camera].keys():
                self.crops_coordinates[camera][template_loc] = {'xmin': conf['templates']['coordinates'][camera][template_loc]['xmin'],
                                                                'ymin': conf['templates']['coordinates'][camera][template_loc]['ymin']}
                self.is_first_calibration[camera][template_loc] = True

    def calculate_mean(self, cropped_images, camera, image, should_monitor=False):
        if len(cropped_images) == 0:  # will be problematic if using wb every x frames
            init_coords = self.crops_coordinates[camera]
            shapes = self.shapes[camera]
            for loc, coord in init_coords.items():
                cropped_images.append(image[coord['ymin']:coord['ymin'] + shapes[loc][0] + 1,
                                      coord['xmin']:coord['xmin'] + shapes[loc][1] + 1, :])
        mean = 0
        for cropped_image in cropped_images:
            mean += np.average(cropped_image)
        mean /= len(cropped_images)
        if hasattr(self, 'monitor') and should_monitor:
            self.monitor.step(mean, camera, cropped_images)

        return mean

    def monitoring_status(self):
        return self.monitor.status()

    def end_calib_in_monitor(self):
        self.monitor.end_calib()

if __name__=='__main__':
    import argparse
    import getpass
    import pandas as pd
    import json

    def locate_movie(movie_name, root_dir):
        optional_dirs = [root_dir]
        for optional_dir in optional_dirs:
            for root, dirs, files in os.walk(optional_dir):
                path = os.path.join(root, movie_name)
                if os.path.isfile(path) or os.path.isdir(path):
                    return root
        return None

    DEFAULT_CSV = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               '../../ourFirstCNN/recording_list/default.csv')
    DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
    parser = argparse.ArgumentParser(description='generate event trigger images')
    parser.add_argument('--movies-file', help='input video',type=str, default=DEFAULT_CSV)
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--camera-id', help='calibration model name',type=str, default='gen2_basler_sys8.0_barcode')
    args = parser.parse_args()
    movie_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()
    for single_movie in movie_list:
        movie_rec = locate_movie(single_movie, args.root_dir)
        if movie_rec is None:
            raise NameError('movie {} was not found!'.format(single_movie))
        if not single_movie.endswith('.jpg'):
            full_rec_path = os.path.join(movie_rec, single_movie)
            movie_name_wo_suffix = full_rec_path.split('_L.avi')[0]

            cap_L = cv2.VideoCapture(full_rec_path)
            cap_B = cv2.VideoCapture(full_rec_path.replace('_L.avi', '_B.avi'))
            prop_file = movie_name_wo_suffix + '_prop.json'
            with open(prop_file) as f:
                camera_id = json.load(f)['camera_id']
            template_match = TemplateMatching(camera_id=camera_id, monitor_values=True)
            frame_num = 0
            plot1, plot2 = None, None
            while (cap_L.isOpened() and cap_B.isOpened()):
                frame_num += 1
                print(frame_num)
                retL, frame_L = cap_L.read()
                frame_L = cv2.resize(frame_L, (640,480))
                retB, frame_B = cap_B.read()
                if frame_num < 20:
                    continue
                if retL and retB and retL:
                    cropped_L, coords_L = template_match.match(camera='left', image=frame_L)
                    cropped_B, coords_B = template_match.match(camera='back', image=frame_B)
                    should_monitor = False#frame_num % 10 == 0
                    mean_L = template_match.calculate_mean(cropped_L, camera='left', image=frame_L, should_monitor=should_monitor)
                    mean_B = template_match.calculate_mean(cropped_B, camera='back', image=frame_B, should_monitor=should_monitor)
                    print("mean: {} for cam: {}".format(int(mean_L), 'left'))
                    print("mean: {} for cam: {}".format(int(mean_B), 'back'))
                    if hasattr(template_match, 'monitor') and should_monitor:
                        plot1, plot2 = template_match.monitor.visualize(plot1, plot2, step=10)
                        need_wb, need_exp_tuning, _ = template_match.monitoring_status()
                        if need_wb:
                            print("Need WB calibration!")
                            h, w = frame_L.shape[:2]
                            cv2.rectangle(frame_L, (0, 0), (w, h), (255, 0, 0), 10)
                            cv2.rectangle(frame_B, (0, 0), (w, h), (255, 0, 0), 10)
                        if need_exp_tuning:
                            print("Need gain/exposure tuning!")
                            h, w = frame_L.shape[:2]
                            cv2.rectangle(frame_L, (10, 10), (w - 10, h - 10), (0, 0, 255), 5)
                            cv2.rectangle(frame_B, (10, 10), (w - 10, h - 10), (0, 0, 255), 5)
                    template_match.print_coords(image=frame_L, coords=coords_L)
                    template_match.print_coords(image=frame_B, coords=coords_B)
                    cv2.imshow("Frame L", frame_L)
                    cv2.imshow("Frame B", frame_B)
                    if frame_num % 10 == 0:
                        key = cv2.waitKey() & 0xFF
                        if key == ord('f'):
                            continue
                        elif key == ord('q'):
                            break
                    else:
                        cv2.waitKey(1)
                else:
                    break
        else:
            print(single_movie)
            frame_L_name = single_movie if 'left' in single_movie else single_movie.replace('back', 'left')
            frame_B_name = single_movie if 'back' in single_movie else single_movie.replace('left', 'back')
            frame_L = cv2.imread(os.path.join(movie_rec, frame_L_name))
            frame_L = cv2.resize(frame_L, (640, 480))
            frame_B = cv2.imread(os.path.join(movie_rec, frame_B_name))
            template_match = TemplateMatching(camera_id=args.camera_id)
            cropped_L, coords_L = template_match.match(camera='left', image=frame_L)
            cropped_B, coords_B = template_match.match(camera='back', image=frame_B)
            template_match.print_coords(image=frame_L, coords=coords_L)
            template_match.print_coords(image=frame_B, coords=coords_B)
            cv2.imshow("Frame L", frame_L)
            cv2.imshow("Frame B", frame_B)
            key = cv2.waitKey() & 0xFF
            if key == ord('f'):
                continue
            elif key == ord('q'):
                break
