import cv2
import sys
import numpy as np
import time
from cv_blocks.common.types import BoundingBox2D
from detector.yolo.utils import draw_boxes as detector_draw_boxes
from config import config

tracker_types = ['BOOSTING', 'MIL', 'KCF', 'TLD', 'MEDIANFLOW', 'GOTURN', 'MOSSE', 'CSRT']

tracking_details = {
    "optimal_frames_to_track"          :      2,    # amount of frames between taking the ground truth box
    "frames_to_track_near_borders"     :      2,    # amount of frames between taking the ground truth box when near the edges
    "frames_to_wait_without_detection" :      1,    # amount of frames without performing yolo or tracking
    "tracker_type"                     :      4,    # index of tracker to use from the list above
    "image_reduction"                  :      1,
    "gray_image"                       :      True,
    "iou_threshold"                    :      0.3,  # relevent only for statistics when gt is given
    "confidence_threshold"             :      0.7,  # track for a long time bboxes that are above this threshold and for a short period when below
    "confidence_to_start_tracking"     :      0.5, # new bboxes below this threshold will not be tracked
    "low_confidence_detection"         :      0.7, # Consider detection below this threshold to low-conf detectio
    "frames_not_seen_to_remove_id"     :      3,    # number of times no yolo bbox for a specific id before removal
    "iou_threshold_for_tracking"       :      dict(high=0.3,low=0.2),  # minimum iou between yolo and tracker bboxes to consider them as the same id
    "distance_threshold_for_tracking"  :      0.3,  # maximum distance between yolo and tracker bboxes's centers to consider them as the same id
    "min_valid_tracked_bbox"           :      0.004, # min size of bbox width and height to be considered good bbox
    "max_distance_tracked"             :      0.3,  # maximum distance to consider success tracking
    "max_patch_desc_dist"              :      0.2,  # maximum distance between patch descriptors
    "frame_delta_detection_min_th"     :      4.0,  # th to prune dynamic detector bboxes by frame delta
    "frame_delta_dynamic_obj_q90_th"   :      15.0,  # th to prune dynamic detector bboxes by frame delta
    "disparity_detection_min_th"       :      0.5,  # th to prune dynamic detector bboxes by disparity
    "min_box_scale_ratio"              :      0.2,  # max allowed scale between boxes areas to be considered same track
    "dark_product_mean_th"             :      80,  # if mean values of cropped image is below this value - reduce fd th
}

def createTrackerByName(trackerType):
    # Create a tracker based on tracker name
    if trackerType == tracker_types[0]:
        tracker = cv2.TrackerBoosting_create()
    elif trackerType == tracker_types[1]:
        tracker = cv2.TrackerMIL_create()
    elif trackerType == tracker_types[2]:
        tracker = cv2.TrackerKCF_create()
    elif trackerType == tracker_types[3]:
        tracker = cv2.TrackerTLD_create()
    elif trackerType == tracker_types[4]:
        tracker = cv2.TrackerMedianFlow_create()
    elif trackerType == tracker_types[5]:
        tracker = cv2.TrackerGOTURN_create()
    elif trackerType == tracker_types[6]:
        tracker = cv2.TrackerMOSSE_create()
    elif trackerType == tracker_types[7]:
        tracker = cv2.TrackerCSRT_create()
    else:
        tracker = None
        print('Incorrect tracker name')
        print('Available trackers are:')
        for t in tracker_types:
            print(t)
        sys.exit(1)
    return tracker

def create_patch_descriptor(box, img):
    # Naive patch descriptor, currently color histogram
    # TODO: find better lean representation to replace it
    def _color_hist(patch):
        _ps = cv2.resize(patch, (32,32))
        hist = []
        for ch_idx in range(3):
            ch_hist = np.histogram(_ps[:,:,ch_idx], bins=np.arange(0,257,16))[0]
            hist = np.hstack((hist, ch_hist))
        hist /= hist.sum()
        return hist

    h,w = img.shape[:2]
    xs = int(round(box.x1 * w))
    ys = int(round(box.y1 * h))
    xe = int(round(box.x2 * w))
    ye = int(round(box.y2 * h))
    if xs==xe:
        xs = max(xs-2, 0); xe = min(xe+1, w-1)
    if ys==ye:
        ys = max(ys-2, 0); ye = min(ye+1, h-1)
    patch = img[ys:ye, xs:xe]
    hist = _color_hist(patch)

    return hist, patch

    # from matplotlib import pyplot as plt
    # plt.figure()
    # plt.subplot(121)
    # plt.imshow(patch)
    # plt.subplot(122)
    # plt.stem(np.arange(45), hist)
    # plt.show()
class ObjectIdentity:

    def __init__(self, bbox, id, img, frame_number, att_info=None):
        self.x1 = bbox.x1
        self.y1 = bbox.y1
        self.x2 = bbox.x2
        self.y2 = bbox.y2
        self.id = id
        self.last_observation_frame = frame_number
        self.frames_not_seen = 0
        self.patch_desc, self.patch = create_patch_descriptor(self, img)
        self.direction = np.array([0.,0.])
        if att_info is not None and isinstance(bbox, BoundingBox2D):
            self.class_info = [bbox.get_top_attr('crop_type', att_info)]
            self.location_info = [bbox.get_top_attr('crop_location', att_info)]
            self.state_info = [bbox.get_top_attr('crop_state', att_info)]

    def update_bbox(self, bbox, img, frame_number, att_info=None):
        frames_since_last_obs = frame_number - self.last_observation_frame
        self.direction = self.get_direction(BoundingBox2D((self.x1, self.y1, self.x2, self.y2)), bbox) / float(frames_since_last_obs)
        self.x1 = bbox.x1
        self.y1 = bbox.y1
        self.x2 = bbox.x2
        self.y2 = bbox.y2
        self.last_observation_frame = frame_number
        self.patch_desc, self.patch = create_patch_descriptor(self, img)
        if isinstance(bbox, BoundingBox2D):
            # Update class & loc info
            class_info = bbox.get_top_attr('crop_type', att_info)
            location_info = bbox.get_top_attr('crop_location', att_info)
            state_info = bbox.get_top_attr('crop_state', att_info)
            if class_info is not None:
                self.class_info.append(class_info)
            if location_info is not None:
                self.location_info.append(location_info)
            if state_info is not None:
                self.state_info.append(state_info)

    def get_direction(self, old_bbox, new_bbox):
        direction = new_bbox.centroid() - old_bbox.centroid()
        return direction


class ObjectTracker:

    def __init__(self, prune_boxes_below=None, debug=False, barcode_roi_params=None, detector_attributes=dict(crop_type=['product'])):
        self.frames_between_yolo_predictions_tracking_on = tracking_details["optimal_frames_to_track"]
        self.frames_between_yolo_predictions_tracking_off = tracking_details["frames_to_track_near_borders"]
        self.frames_to_wait_without_detection_and_tracking = tracking_details["frames_to_wait_without_detection"]
        self.tracker_type = tracker_types[tracking_details["tracker_type"]]
        self.resize_factor = tracking_details["image_reduction"]
        self.is_gray = tracking_details["gray_image"]
        self.frames_not_seen_to_remove_id = tracking_details["frames_not_seen_to_remove_id"]
        self.frames_not_seen_with_force = self.frames_not_seen_to_remove_id
        self.iou_threshold_for_tracking = tracking_details["iou_threshold_for_tracking"]
        self.min_box_scale_ratio = tracking_details["min_box_scale_ratio"]
        self.distance_threshold_for_tracking = tracking_details["distance_threshold_for_tracking"]
        self.multi_tracker = self.init_multi_tracker()
        self.confidence_threshold = tracking_details["confidence_threshold"]
        self.confidence_to_start_tracking = tracking_details["confidence_to_start_tracking"] if not config.SAVE_ALL_DETECTIONS else 0.3
        self.low_confidence_detection = tracking_details["low_confidence_detection"]
        self.max_distance_tracked = tracking_details['max_distance_tracked']
        self.max_patch_desc_dist = tracking_details['max_patch_desc_dist']
        self.frame_delta_detection_min_th = tracking_details['frame_delta_detection_min_th']
        self.frame_delta_dynamic_obj_q90_th = tracking_details['frame_delta_dynamic_obj_q90_th']
        self.disparity_detection_min_th = tracking_details['disparity_detection_min_th']
        self.min_valid_tracked_bbox = tracking_details['min_valid_tracked_bbox']
        self.dark_product_mean_th = tracking_details['dark_product_mean_th']
        self.fast_motion_params = dict(big_size=0.1, med_size=0.07, high_fd=5, med_fd=3, conf=0.7)
        self.skip_tracker_bbox_size_th = 0.8 # skip tracking of large bounding boxes in order to reduce latency
        self.countdown_to_apply_detector = 0
        self.previous_predicted_bbox = []
        self.skip_yolo_and_tracking = False
        self.actively_tracked_objects = []
        self.frame_for_tracking = None
        self.current_id = -1
        self.prune_boxes_below = prune_boxes_below
        self.debug = debug
        self.ids_to_force_tracking = []
        self.barcode_roi_params = barcode_roi_params
        self.detector_attributes = detector_attributes

    def has_actively_tracked_objects(self):
        return len(self.actively_tracked_objects) > 0

    def actively_tracked_objects_length(self):
        return len(self.actively_tracked_objects)

    def remove_id_from_tracker(self, id_to_remove):
        for idx in range(len(self.actively_tracked_objects)):
            if self.actively_tracked_objects[idx].id == id_to_remove:
                del self.actively_tracked_objects[idx]
                break

    def init_multi_tracker(self):
        return cv2.MultiTracker_create()

    def add_bbox_to_track(self, bbox, frame):
        # a = time.time()
        self.multi_tracker = self.init_multi_tracker()
        self.multi_tracker.add(createTrackerByName(self.tracker_type), frame, bbox)
        # print("time = {}".format((time.time() - a) * 1000))

    def update_multi_tracker(self):
        if len(self.actively_tracked_objects)==0:
            return
        shape = self.frame_for_tracking.shape
        self.multi_tracker = self.init_multi_tracker()
        for object_to_track in self.actively_tracked_objects:
            obj_size = (object_to_track.x2 - object_to_track.x1) * (object_to_track.y2-object_to_track.y1)
            # Fail object on pupose when it's too large (a hacky way to skip it)
            if obj_size>self.skip_tracker_bbox_size_th:
                bbox = (0., 0., 0., 0.)
            else:
                bbox = (object_to_track.x1 * shape[1], object_to_track.y1 * shape[0],
                        (object_to_track.x2 - object_to_track.x1) * shape[1],
                        (object_to_track.y2 - object_to_track.y1) * shape[0])
            self.multi_tracker.add(createTrackerByName(self.tracker_type), self.frame_for_tracking, bbox)
        if self.debug:
            boxes = []
            for object_to_track in self.actively_tracked_objects:
                bbox = (object_to_track.x1, 
                        object_to_track.y1,
                        object_to_track.x2,
                        object_to_track.y2)
                boxes.append(BoundingBox2D(bbox))
                self.last_frame_info = dict(img=self.frame_for_tracking.copy(), boxes=boxes)


    def small_or_near_borders(self, bboxes):
        near_borders, small_box = False, False
        for bbox in bboxes:
            near_borders = near_borders or (bbox.x1 < 0.03 or bbox.y1 < 0.03 or bbox.x2 > 0.97 or bbox.y2 > 0.97)
            small_box = small_box or (bbox.h() < 0.07 or bbox.w() < 0.07)
        return near_borders, small_box

    def find_bbox_with_highest_confidence(self, bboxes):
        best_confidence = -1
        best_bbox = bboxes[0]
        for bbox in bboxes:
            if bbox.c > best_confidence:
                best_bbox = bbox
        return best_bbox

    def process_yolo_bboxes(self, bboxes_and_ids, shape):
        # from xmin, ymin, xmax, ymax in [0,1] to xmin, ymin, w, h in [shape[1], shape[0]]
        new_bboxes = []
        for bbox, id in bboxes_and_ids:
            new_bboxes.append((bbox.x1, bbox.y1, bbox.x2 - bbox.x1, bbox.y2 - bbox.y1))
        return new_bboxes

    def check_if_there_are_objects_to_remove(self):
        indices_to_keep = []
        for i, tracked_object in enumerate(self.actively_tracked_objects):
            if tracked_object.frames_not_seen < self.frames_not_seen_to_remove_id or \
                (tracked_object.id in self.ids_to_force_tracking and tracked_object.frames_not_seen < self.frames_not_seen_with_force):
                indices_to_keep.append(i)
        if len(indices_to_keep) != len(self.actively_tracked_objects):
            self.actively_tracked_objects = [self.actively_tracked_objects[i] for i in indices_to_keep]
            return True
        return False

    def is_tracking_failed(self, tracked_bboxes):
        tracking_failed = False
        per_track = []

        for i, tracked_bbox in enumerate(tracked_bboxes):
            success = True
            # Check if tracker jumps too much
            prv_obj = self.actively_tracked_objects[i]
            prv_box = BoundingBox2D([prv_obj.x1, prv_obj.y1, prv_obj.x2, prv_obj.y2])
            if BoundingBox2D.dist(tracked_bbox, prv_box) > self.max_distance_tracked:
                self.actively_tracked_objects[i].frames_not_seen +=1
                tracking_failed = True
                success = False
            # Check if tracker didn't change position
            elif np.all(np.isclose(prv_box.asarray(), tracked_bbox.asarray())):
                self.actively_tracked_objects[i].frames_not_seen +=1
                tracking_failed = True
                success = False
            # Check if box has invalid size
            elif tracked_bbox.x1 >=1 or tracked_bbox.y1 >=1 or tracked_bbox.w()<=self.min_valid_tracked_bbox or \
                    tracked_bbox.h()<=self.min_valid_tracked_bbox:
                self.actively_tracked_objects[i].frames_not_seen +=1
                tracking_failed = True
                success = False
            # Check if box has invalid scale
            if BoundingBox2D.area_ratio(tracked_bbox, prv_box) < self.min_box_scale_ratio:
                self.actively_tracked_objects[i].frames_not_seen +=1
                tracking_failed = True
                success = False
            per_track.append(success)
        return tracking_failed, per_track

    def normalize(self, boxes):
        shape = self.frame_for_tracking.shape
        normalized_boxes = []
        for box in boxes:
            normalized_boxes.append((box[0] / shape[1], box[1] / shape[0], box[2] / shape[1], box[3] / shape[0]))
        return normalized_boxes

    def fit_bboxes_between_0_and_1(self, bboxes, type):
        if type == "yolo":
            for bbox in bboxes:
                bbox.x1 = max(0, bbox.x1)
                bbox.y1 = max(0, bbox.y1)
                bbox.x2 = min(1, bbox.x2)
                bbox.y2 = min(1, bbox.y2)
        else:
            new_bboxes = []
            for bbox in bboxes:
                xmin = max(0, bbox[0])
                ymin = max(0, bbox[1])
                w = min(1 - bbox[0], bbox[2])
                h = min(1 - bbox[1], bbox[3])
                new_bboxes.append((xmin,ymin,w,h))
            return new_bboxes

    def prune_box_by_map(self, boxes, map_img, map_th, stat='mean', full_pruning_below=None, gray_image=None, during_scan=False):
        map_h, map_w = map_img.shape[:2]
        boxes_out = []
        for b in boxes:
            # boxes inside barcode roi are not pruned in barcode mode
            if self.is_in_barcode_roi(b, during_scan):
                boxes_out.append(b)
                continue
            # allow less motion above certain y value
            if full_pruning_below is not None and b.centroid()[1] < full_pruning_below:
                bx_th = map_th / 2
            else:
                bx_th = map_th
            x = int(b.x1 * map_w)
            y = int(b.y1 * map_h)
            w = int((b.x2 - b.x1) * map_w)
            h = int((b.y2 - b.y1) * map_h)
            if stat == 'mean':
                val = map_img[y:y + h, x:x + w].mean()
            elif stat == 'q90':
                val = np.percentile(map_img[y:y + h, x:x + w], 90)
                if gray_image is not None:
                    mean = np.mean(gray_image[y:y + h, x:x + w])
                    if mean < self.dark_product_mean_th:
                        bx_th = map_th / 2
            else:
                raise NameError('Invalid statistic: %s' % stat)
            if val >= bx_th:
                boxes_out.append(b)
        return boxes_out

    def is_in_barcode_roi(self, box, during_scan):
        if self.barcode_roi_params is None or not during_scan:
            return False
        iou_condition = BoundingBox2D.iou(self.barcode_roi_params['barcode_roi'], box) > \
                        self.barcode_roi_params['iou_th_to_consider_during_scan']
        boxes_intersection = BoundingBox2D.intersection(self.barcode_roi_params['barcode_roi'], box)
        coverage_condition = boxes_intersection > self.barcode_roi_params[
            'item_coverage_th_to_consider_during_scan'] * box.area() or \
            boxes_intersection > self.barcode_roi_params['roi_coverage_th_to_consider_during_scan'] * \
            self.barcode_roi_params['barcode_roi'].area()
        if self.barcode_roi_params['check_coverage']:
            return iou_condition or coverage_condition
        else:  # in front cam demand big boxes instead of coverage
            return (iou_condition or coverage_condition) and box.area() > 0.15

    def assign_boxes_to_mv(self, yolo_bboxes, tracked_bboxes, curr_frame, success_per_track, frame_number, frame_delta):
        check_if_needs_to_remove = False
        bboxes_and_ids_for_MD = []
        iou_mat = np.zeros((len(yolo_bboxes), len(tracked_bboxes)))
        dist_mat = np.inf * np.ones((len(yolo_bboxes), len(tracked_bboxes)))
        scale_mat = np.zeros((len(yolo_bboxes), len(tracked_bboxes)))

        assigned_yolo = [False] * len(yolo_bboxes)
        assigned_yolo_map = [-1] * len(yolo_bboxes)
        assigned_tracked = [False] * len(tracked_bboxes)

        for i, tracked_bbox in enumerate(tracked_bboxes):
            if not success_per_track[i]:
                prv_obj = self.actively_tracked_objects[i]
                # project the location of the next bbox, based on the last bbox and its movement direction
                frame_diff = frame_number - prv_obj.last_observation_frame
                dx, dy = prv_obj.direction * frame_diff
                projected_bbox = BoundingBox2D(np.clip([prv_obj.x1 + dx, prv_obj.y1 + dy, prv_obj.x2 + dx, prv_obj.y2 + dy], 0 , 1))
                tracked_bboxes[i] = projected_bbox

        # When tracking & current detection agree, assign detector box to that track
        if len(yolo_bboxes) > 0 or len(tracked_bboxes) > 0:
            if len(yolo_bboxes) > 0 and len(tracked_bboxes) > 0:
                # Check new boxes assignment to existing tracked boxes
                # ====================================================
                for i in range(len(yolo_bboxes)):
                    # 1. Compute IOUs, distances and scales between all detector and tracked boxes
                    for j in range(len(tracked_bboxes)):
                        iou_mat[i,j] = BoundingBox2D.iou(tracked_bboxes[j], yolo_bboxes[i])
                        dist_mat[i,j] = BoundingBox2D.dist(tracked_bboxes[j], yolo_bboxes[i])
                        scale_mat[i,j] = BoundingBox2D.area_ratio(tracked_bboxes[j], yolo_bboxes[i])

                # Assign by highest IOU - bi-directional
                for i in range(len(yolo_bboxes)):
                    best_iou = iou_mat[i].max()
                    if best_iou > self.iou_threshold_for_tracking['high']:
                        # Test if bi-directional
                        best_track_idx = np.argmax(iou_mat[i]).ravel()[0]
                        best_det_idx_bi = np.argmax(iou_mat[:, best_track_idx]).ravel()[0]
                        # Check if detector box can be connected to track
                        if best_det_idx_bi==i:
                            assigned_yolo[i] = True
                            assigned_tracked[best_track_idx] = True 
                            assigned_yolo_map[i] = best_track_idx
                            # invalidate track and detector box to other matches
                            iou_mat[:, best_track_idx] = 0; iou_mat[i, :] = 0
                            dist_mat[:, best_track_idx] = np.inf; dist_mat[i, :] = np.inf

                # If there are multiple candidates meeting lower criteria, select by patch similarity
                for i in range(len(yolo_bboxes)):
                    if assigned_yolo[i]:
                        continue
                    is_valid_candidate = np.bitwise_or(dist_mat[i] < self.distance_threshold_for_tracking, 
                                                       iou_mat[i]>self.iou_threshold_for_tracking['low']).ravel()
                    is_valid_scale = (scale_mat[i] > self.min_box_scale_ratio).ravel()
                    is_valid_candidate = np.bitwise_and(is_valid_candidate, is_valid_scale)
                    if is_valid_candidate.sum() > 0:
                        # Check patch similarity
                        candidate_idx = np.argwhere(is_valid_candidate).ravel()
                        if len(candidate_idx) > 1:
                            yolo_desc, yolo_patch = create_patch_descriptor(yolo_bboxes[i], curr_frame)
                            desc_candidates = [self.actively_tracked_objects[ci].patch_desc for ci in candidate_idx]
                            dist_desc = [np.linalg.norm(yolo_desc - dc) for dc in desc_candidates]
                        else:
                            dist_desc = [0.] # skip patch descriptor when only 1 candidate

                        best_dist_idx = np.argmin(dist_desc).ravel()[0]
                        # Check bi-directionality - if the chosen tracked box is most similar to current yolo box
                        if len([1 for ay in assigned_yolo if not ay]) > 1:
                            best_det_iou_idx_bi = np.argmax(iou_mat[:, best_dist_idx]).ravel()[0]
                            best_det_dist_idx_bi = np.argmin(dist_mat[:, best_dist_idx]).ravel()[0]
                            if iou_mat[best_det_iou_idx_bi, best_dist_idx]==0 or assigned_yolo[best_det_iou_idx_bi]:
                                best_det_iou_idx_bi = i
                            if assigned_yolo[best_det_dist_idx_bi]:
                                best_det_dist_idx_bi = i
                            valid_bi_dir = best_det_iou_idx_bi==i and best_det_dist_idx_bi==i
                            if not valid_bi_dir:
                                continue

                        
                        # Check if detector box can be connected to track
                        if dist_desc[best_dist_idx] < self.max_patch_desc_dist:
                            best_track_idx = candidate_idx[best_dist_idx]
                            assigned_yolo[i] = True
                            assigned_tracked[best_track_idx] = True 
                            assigned_yolo_map[i] = best_track_idx
                            # invalidate track and detector box to other matches
                            iou_mat[:, best_track_idx] = 0; iou_mat[i, :] = 0
                            dist_mat[:, best_track_idx] = np.inf; dist_mat[i, :] = np.inf
                if len(yolo_bboxes) == 1 and len(tracked_bboxes) == 1 and not assigned_yolo[0]:
                    mean_fd = frame_delta.mean()
                    bbox_size = yolo_bboxes[0].area()
                    bbox_conf = yolo_bboxes[0].score
                    if bbox_conf > self.fast_motion_params['conf'] and \
                            ((bbox_size > self.fast_motion_params['big_size'] and mean_fd > self.fast_motion_params['med_fd']) or
                             (bbox_size > self.fast_motion_params['med_size'] and mean_fd > self.fast_motion_params['high_fd'])):
                        assigned_yolo[0] = True
                        assigned_tracked[0] = True
                        assigned_yolo_map[0] = 0

                # Assign boxes to existing tracks
                # ===============================
                for i in range(len(yolo_bboxes)):
                    if assigned_yolo[i]:
                        track_idx = assigned_yolo_map[i]
                        self.actively_tracked_objects[track_idx].frames_not_seen = 0
                        self.actively_tracked_objects[track_idx].update_bbox(yolo_bboxes[i], curr_frame, frame_number, self.detector_attributes)
                        bboxes_and_ids_for_MD.append((yolo_bboxes[i], self.actively_tracked_objects[track_idx].id))

            # Update tracks not observed
            # ==========================
            for i in range(len(tracked_bboxes)):
                if not assigned_tracked[i]:
                    self.actively_tracked_objects[i].frames_not_seen += 1
                    self.actively_tracked_objects[i].update_bbox(tracked_bboxes[i], curr_frame, frame_number)
                    # bboxes_and_ids_for_MD.append(self.change_to_yolo_format_bbox(tracked_bboxes[i])) #todo - add option to send tracked bbox
                    check_if_needs_to_remove = True
            
            if check_if_needs_to_remove:
                self.check_if_there_are_objects_to_remove()
            # Initialize new tracks
            # =====================
            if len(yolo_bboxes) > 0: # add new bboxes to track
                for yolo_bbox, assigned in zip(yolo_bboxes, assigned_yolo):
                    if not assigned and yolo_bbox.score >= self.confidence_to_start_tracking:
                        self.current_id += 1
                        att_info = self.detector_attributes if config.DETECTOR_V2 else None
                        self.actively_tracked_objects.append(ObjectIdentity(yolo_bbox, self.current_id, curr_frame, frame_number, att_info=att_info))
                        bboxes_and_ids_for_MD.append((yolo_bbox, self.current_id))

        return bboxes_and_ids_for_MD

    def replace_id(self, id_to_replace):
        self.current_id += 1
        found = False
        for actively_tracked in self.actively_tracked_objects:
            if actively_tracked.id == id_to_replace:
                actively_tracked.id = self.current_id
                found = True
                break
        assert found, "id: {} was not found!".format(id_to_replace)
        print("replaced id: {} with {}".format(id_to_replace, self.current_id))
        return self.current_id

    def track(self, frame):
        ch_num = np.atleast_3d(frame).shape[-1]
        assert ch_num in (1, 3, 4), 'Only RGB or Gray or RGB+fd allowed into tracker'
        self.countdown_to_apply_detector = max(0, self.countdown_to_apply_detector - 1)
        if ch_num == 4:
            frame = frame[:,:,:3]
        if self.resize_factor != 1:
            resized_frame = cv2.resize(frame, (int(frame.shape[1] * self.resize_factor), int(frame.shape[0] * self.resize_factor)))
        else:
            resized_frame = frame
        if self.is_gray:
            gray_frame = cv2.cvtColor(resized_frame, cv2.COLOR_BGR2GRAY)
            self.frame_for_tracking = gray_frame
        else:
            self.frame_for_tracking = resized_frame
        if len(self.actively_tracked_objects)==0:
            predicted_boxes_normalized = []
            need_detection = self.countdown_to_apply_detector <= 0
            success_per_track = []
        else:
            # Run tracking
            # ============
            (fh, fw) = self.frame_for_tracking.shape[:2]
            success, predicted_boxes = self.multi_tracker.update(self.frame_for_tracking)
            predicted_boxes = BoundingBox2D.xywh_to_xyxy(predicted_boxes)
            predicted_boxes_normalized = [BoundingBox2D(b).normalize(fh, fw) for b in predicted_boxes]
            tracking_failed, success_per_track = self.is_tracking_failed(predicted_boxes_normalized)
            need_detection = self.countdown_to_apply_detector <= 0 or tracking_failed 
            if need_detection:
                self.countdown_to_apply_detector = 0

            if self.debug:
                curr_dict = dict(boxes=predicted_boxes_normalized, img=self.frame_for_tracking.copy())
                self.visualize_tracker_result(self.last_frame_info, curr_dict)

        return dict(frame=resized_frame, boxes=predicted_boxes_normalized, need_detection=need_detection, success_per_track=success_per_track)

    def post_tracking(self, track_data, yolo_bboxes, frame_delta, frame_number, disparity=None, gray_image=None,
                      during_scan=False, attributes=None):
        if yolo_bboxes is None:
            ran_detector = False
            yolo_bboxes = []
        else:
            ran_detector = True
        bboxes_and_ids_for_MD = []
        resized_frame = track_data['frame']
        predicted_boxes_normalized = track_data['boxes']
        success_per_track = track_data['success_per_track']
        # Prune yolo boxes using frame delta
        self.yolo_boxes_before_prune = np.copy(yolo_bboxes)
        yolo_bboxes = self.prune_box_by_map(yolo_bboxes, frame_delta, self.frame_delta_dynamic_obj_q90_th, stat='q90',\
            full_pruning_below=self.prune_boxes_below, gray_image=gray_image, during_scan=during_scan)
        # # # Prune boxes by overlap with disparity
        if disparity is not None:
            yolo_bboxes = self.prune_box_by_map(yolo_bboxes, disparity>0, self.disparity_detection_min_th, during_scan=during_scan)

        if len(yolo_bboxes) == 0: # no predictions
            if len(self.actively_tracked_objects) > 0:
                if not ran_detector: # this was a tracking frame
                    for i, (predicted_box, is_valid) in enumerate(zip(predicted_boxes_normalized, success_per_track)):
                        if is_valid:
                            self.actively_tracked_objects[i].update_bbox(predicted_box, resized_frame, frame_number)
                            bboxes_and_ids_for_MD.append((predicted_box, self.actively_tracked_objects[i].id))
                        else:
                            self.actively_tracked_objects[i].frames_not_seen += 1
                else: # this was a detection frame
                    for i, tracked_object in enumerate(self.actively_tracked_objects):
                        self.actively_tracked_objects[i].frames_not_seen += 1
        else:
            bboxes_and_ids_for_MD = self.assign_boxes_to_mv(yolo_bboxes, predicted_boxes_normalized, resized_frame, success_per_track, frame_number, frame_delta)
            bx_for_md = [bx for bx, id in bboxes_and_ids_for_MD]
            near_borders, small_box = self.small_or_near_borders(bx_for_md)
            if config.DETECTOR_V2:
                low_conf_detector = len(bx_for_md)>0 and \
                    np.array([1.] + [bx.score for bx in bx_for_md if bx.get_attr_conf('crop_location', attributes)['outside-cart'] < 0.5]).min() < self.low_confidence_detection
                if attributes is not None:
                    p_thrown = np.array([bx.get_attr_conf('crop_state', attributes)['thrown'] for bx in bx_for_md])
                    thrown_item = p_thrown.max() > 0.7
                    low_conf_detector = low_conf_detector or thrown_item

            else:
                low_conf_detector = len(bx_for_md)>0 and np.array([bx.score for bx in bx_for_md]).min() < self.confidence_to_start_tracking
            # If cases of low conf. detection, detection near boarderds or small object, run detector again next frame
            if near_borders or small_box or low_conf_detector:
                self.countdown_to_apply_detector = 0

        check_for_removal = self.check_if_there_are_objects_to_remove()
        self.update_multi_tracker()

        return bboxes_and_ids_for_MD

    def visualize_tracker_result(self, prv_dict, cur_dict):
        prv_frame = cv2.cvtColor(prv_dict['img'], cv2.COLOR_GRAY2BGR)
        prv_frame = detector_draw_boxes(prv_frame, prv_dict['boxes'],
                                        ['o'], ids = np.arange(len(prv_dict['boxes'])))
        cur_frame = cv2.cvtColor(cur_dict['img'], cv2.COLOR_GRAY2BGR)
        cur_frame = detector_draw_boxes(cur_frame, cur_dict['boxes'],
                                        ['o'], ids = np.arange(len(cur_dict['boxes'])))
        # Run tracker again for verification
        multi_tracker = self.init_multi_tracker()
        shape = cur_dict['img'].shape[:2]
        for bx in prv_dict['boxes']:
            bbox = (bx.x1 * shape[1], bx.y1 * shape[0],
                    (bx.x2 - bx.x1) * shape[1],
                    (bx.y2 - bx.y1) * shape[0])
            multi_tracker.add(createTrackerByName(self.tracker_type), prv_dict['img'], bbox)
        success, predicted_boxes = self.multi_tracker.update(cur_dict['img'])
        predicted_boxes = BoundingBox2D.xywh_to_xyxy(predicted_boxes)
        predicted_boxes_norm = [BoundingBox2D(b).normalize(shape[0], shape[1]) for b in predicted_boxes]
        cur_frame_test = cv2.cvtColor(cur_dict['img'], cv2.COLOR_GRAY2BGR)
        cur_frame_test = detector_draw_boxes(cur_frame_test, predicted_boxes_norm,
                                        ['o'], ids = np.arange(len(predicted_boxes_norm)))
        
        viz_frame = np.hstack((prv_frame, cur_frame, cur_frame_test))
        cv2.imshow('Tracking Result:Prv, result, result recovery', viz_frame)
        cv2.waitKey(0)