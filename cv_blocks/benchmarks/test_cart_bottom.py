import argparse
import os
import getpass
import re
import pandas as pd
import numpy as np
from cv_blocks.cart_bottom.base_diff_detector import BaseDiffDetector
import cv2
import yaml
from tqdm import tqdm
import movie_parser_time_and_dir as mParser
from cart_blocks import catalog as Catalog
from misc import bcolors as bcolors
from external_libs.tabulate import tabulate
from cv_blocks.metrics.shopping_metrics import shopping_accuracy, summarize_recording_metrics
from cv_blocks.visualizers.cart_bottom_visualizer import CartBottomVisualizer
from cv_blocks.common.types import BoundingBox2D

DEFAULT_CSV = os.path.join(os.path.dirname(os.path.realpath(__file__)),'../../ourFirstCNN/recording_list', 'cart_bottom.csv')
DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
DEFAULT_SNAPSHOT_DIR = '/home/%s/walkout_dataset/cart_bottom_benchmark' % (getpass.getuser())
# TODO - encapsulate in diff detector module
DEFAULT_WEIGHTS = '/home/walkout05/git_repo/walkout/cv_blocks/cart_bottom/diff_yolo/models/diff_detector/diff_detector.pth'
DEFAULT_CLS_WEIGHTS = '/home/walkout05/git_repo/walkout/ourFirstCNN/tensorrt_lib/models/classifier/demo35_NRF_00137/demo35_NRF_00137.pth'
CAM_POSTFIX_MAP = dict(front='_qvga.',back='_b_qvga.')
DEFAULT_ROI_FRONT = BoundingBox2D((0,60,240,320))
DEFAULT_ROI_BACK = BoundingBox2D((0,100,240,320))

def get_snapshots(movie_name, snapshot_dir, cam_name):
    cam_postfix = CAM_POSTFIX_MAP[cam_name]
    pattern =  '[a-zA-z]+_[0-9]+%s*' % cam_postfix 
    search_dir = os.path.join(snapshot_dir, movie_name)
    list_files = sorted([f for f in os.listdir(search_dir) if re.search(pattern, f)])
    list_files = [os.path.join(search_dir, f) for f in list_files]
    return list_files

def collect_gt_diffs(gt, t1, t2, last_event_t):
    '''
    Collect All diffs occured between t1 and t2
    '''
    gt_events = [g for g in gt if int(g['frameNum'])>max(t1, last_event_t) and int(g['frameNum'])<t2+5]
    return gt_events

def identify_failure(diff, gt_events):
    '''
    Mark cart bottom failures: wrong detection/classification 
    '''
    diff_products = [dict((k, d[k]) for k in ('name', 'direction'))\
         for d in diff if d['name']!='aa_hand']
    gt_products = [dict((k, d[k]) for k in ('name', 'direction'))\
         for d in gt_events if d['name']!='aa_hand']
    diff_sorted = sorted(diff_products, key=lambda x: (x['name'], x['direction']))
    gt_sorted = sorted(gt_products, key=lambda x: (x['name'], x['direction']))
    
    return gt_sorted!=diff_sorted

def naive_product_prediction(prob, cls_config):
    top2_classes = np.argsort(-prob)[:2]
    top2_probs = prob[top2_classes]
    top2_products = [cls_config['class_names'][c] for c in top2_classes]
    top2_alias = [cls_config['class_alias'][c] for c in top2_classes]
    # print('Debug probs:', top2_alias, top2_probs)
    return top2_classes[0]

def is_event_visible(gt_event, cam_name):
    vis_key = '%sVis' % cam_name
    if vis_key in gt_event.keys():
        return gt_event[vis_key]
    else:
        return True

def run_single_test(cam_name, snapshot_list, diff_detector, classifier, cls_config, gt, log_dir, masked_images=False):
    results = []
    one_2_one_mapping = len(snapshot_list) -1 == len(gt) 
    last_event_t = 0
    visuzalizer = CartBottomVisualizer(['O'], output_dir=log_dir)

    valid_gt_events = []
    for snap_idx, snapshot_path in enumerate(tqdm(snapshot_list)):
        curr_valid_gt = []
        frame_idx = int(re.findall(r"\D(\d{5})\D", os.path.basename(snapshot_path))[0])
        snapshot = cv2.imread(snapshot_path, -1)
        assert snapshot is not None
        # Apply diff detector
        # ===================
        diff = diff_detector.step(snapshot, None, frame_idx, force_snapshot=True)

        if diff is not None:
            t1, t2 = diff_detector.get_cart_timestamps()
            gt_events = [gt[snap_idx-1]] if one_2_one_mapping \
                else collect_gt_diffs(gt, t1, t2, last_event_t)
            for g in gt_events:
                event_visible = is_event_visible(g, cam_name)
                if not event_visible:
                    continue
                gt_class = cls_config['class_names'].index(g['name'])
                g['alias'] = cls_config['class_alias'][gt_class]
                curr_valid_gt.append(g)
            if len(gt_events)!=1:
                print('Warning:GT has %d events, not supported yet.skipping' % (len(gt_events)))
                continue
            # Classify diffs to produce product prediction
            for diff_idx in range(len(diff)):
                if masked_images:
                    curr_prob = classifier.predict(diff[diff_idx]['img_masked'][:,:,::-1])
                else:
                    curr_prob = classifier.predict(diff[diff_idx]['img'][:,:,::-1])
                predicted_cls = naive_product_prediction(curr_prob, cls_config)
                predicted_product = cls_config['class_names'][predicted_cls]
                predicted_alias = cls_config['class_alias'][predicted_cls]
                diff[diff_idx]['name'] = predicted_product
                diff[diff_idx]['alias'] = predicted_alias
                diff[diff_idx]['frameNum'] = gt_events[0]['frameNum'] # TODO-handle multiple diffs
            
            last_event_t = int(gt_events[-1]['frameNum'])
            
            # Visualize Failures
            is_failure = identify_failure(diff, curr_valid_gt)
            if is_failure:
                img1, img2 = diff_detector.get_cart_images()
                viz_name = 'diff_%05d_%05d.jpg' % (t1, t2) if log_dir else 'Diff Visualization' 
                visuzalizer.visualize(img1, img2, diff, gt=curr_valid_gt, name=viz_name)

            
            # Remove hands from prediction
            diff = [d for d in diff if d['name']!='aa_hand']
            # Keep only metadata
            diff_result = [dict((k, d[k]) for k in ('name', 'frameNum', 'direction')) for d in diff]
            
            results += diff_result
            valid_gt_events += curr_valid_gt
    
    return results, valid_gt_events


def update_total_stats(total_stats, result_dict, result_table, name):
    if result_dict['clsTested'] !=0 and result_dict['MDTested']!=0:
        result_table.append([name,
                     "{:.1f}% [{}/{}]".format(result_dict['clsSuccess']/result_dict['clsTested']*100, result_dict['clsSuccess'], result_dict['clsTested']),
                     "{:.1f} % [{}/{}]".format(result_dict['MDSuccess']/result_dict['MDTested']*100, result_dict['MDSuccess'], result_dict['MDTested']),
                     result_dict['unknownItems']])
    # total_stats = dict(cls_succ=0, cls_test=0, cb_succ=0, cb_test=0  unknowns=0, err=0)
    total_stats['cls_succ'] += result_dict['clsSuccess']
    total_stats['cls_test'] += result_dict['clsTested']
    total_stats['cb_succ'] += result_dict['MDSuccess']
    total_stats['cb_test'] += result_dict['MDTested']
    total_stats['unknowns'] += result_dict['unknownItems']
    total_stats['err'] += result_dict['errorCount']

def log_total_results(total_stats, result_table):
    cls_succ_rate = total_stats['cls_succ']/(total_stats['cls_test'] + 1e-6)*100
    cb_succ_rate    = total_stats['cb_succ']/(total_stats['cb_test'] + 1e-6)*100
    print(tabulate(result_table, headers="firstrow")) #printing a summary table
    print(bcolors.BOLD + "**************************************************" + bcolors.ENDC)
    print(bcolors.BOLD + "Total results summary for {} movies :".format(len(result_table)-1) + bcolors.ENDC)
    print(bcolors.BOLD + "Classifier  Success Rate :{:.1f}% [{}/{}]".format(cls_succ_rate, total_stats['cls_succ'], total_stats['cls_test']) + bcolors.ENDC)
    print(bcolors.BOLD + "Cart Bottom Success Rate :{:.1f}% [{}/{}]".format(cb_succ_rate, total_stats['cb_succ'], total_stats['cb_test']) + bcolors.ENDC)
    print(bcolors.BOLD + "Unknown items            :{}".format(total_stats['unknowns']) + bcolors.ENDC)
    print(bcolors.BOLD + "**************************************************" + bcolors.ENDC)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='object detector benchmark')
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--movies-file', help='path to csv file containing files to run',type=str, default=DEFAULT_CSV)
    parser.add_argument('--snapshot-dir', help='path to directory containing snapshot images',type=str, default=DEFAULT_SNAPSHOT_DIR)
    parser.add_argument('--output-dir', help='directory to output logs and visualizations',type=str, default=None)
    parser.add_argument('--test-cams', help='postfix for valid image type',type=str, default='front')
    parser.add_argument('--weights', help='weights for diff detector module',type=str, default=DEFAULT_WEIGHTS)
    parser.add_argument('--cls-weights', help='weights for classifier module',type=str, default=DEFAULT_CLS_WEIGHTS)
    parser.add_argument('--wnoise-mask', help='use wnoise mask of static pixels', action='store_true')
    args = parser.parse_args()

    # Load diff detector
    # ==================
    diff_detector = BaseDiffDetector(args.weights, 'multi_cam')

    # Load Classifier & product mapping
    # =================================
    catalog = Catalog.Catalog()
    cls_config = yaml.safe_load(open(args.cls_weights.replace('.pth', '_config.yaml'), 'r'))
    from cv_blocks.classifier.resnet18.torch_impl.classifier import ResNet18Classifier
    classifier = ResNet18Classifier(args.cls_weights, n_class=len(cls_config['class_names']))

    # Run Benchmark
    # =============
    assert os.path.exists(args.movies_file)
    args.test_cams = args.test_cams.split(',')
    movie_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()

    total_stats = dict(cls_succ=0, cls_test=0, cb_succ=0, cb_test=0, unknowns=0, err=0)

    result_table = [['Movie Name', 'Classifier Success', 'Cart Bottom Success', 'Unknown']]

    for movie_name in movie_list:
        for cam_name in args.test_cams:
            snapshot_list = get_snapshots(movie_name, args.snapshot_dir, cam_name)
            cam_roi = DEFAULT_ROI_FRONT if cam_name=='front' else DEFAULT_ROI_BACK
            diff_detector.params.roi = cam_roi
            # Single Test
            # ===========
            gt_events = mParser.getPurchaseList(movie_name+'_L.avi', args.root_dir)
            cart_bottom_events, valid_gt_events = run_single_test(cam_name, snapshot_list, diff_detector,
                classifier, cls_config, gt_events, args.output_dir, args.wnoise_mask)
            diff_detector.reset()
            # Compare to GT
            # =============
            analyzed_events = shopping_accuracy(cart_bottom_events, valid_gt_events)
            result_dict = summarize_recording_metrics(analyzed_events, catalog)
            run_name = movie_name + '(%s)' % cam_name
            mParser.printMovieScoreDict(result_dict, run_name)
            update_total_stats(total_stats, result_dict, result_table, run_name)
    
    log_total_results(total_stats, result_table)