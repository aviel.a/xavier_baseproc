import argparse
import os
import getpass
import tensorrt_lib.Trt_cls_multi_head as SimilarityModel
from cv_blocks.misc.aws_download import find_and_download
from cv_blocks.misc.logger import BaseLogger
import pandas as pd
from tqdm import tqdm
import numpy as np
from scipy.spatial.distance import pdist, cdist
from matplotlib import pyplot as plt
from scipy.stats import wasserstein_distance

def visualize_same_not_same_dists(data_vec, output_dir=None, exp_name='all', metric='euclidean'):
    n_dist = len(data_vec)
    # assert len(data_vec)==len(labels)
    if metric=='cosine':
        hist_bins = np.linspace(-1, 1 , 100)
    else:
        hist_bins = np.linspace(0, 2 , 100)
    dx = hist_bins[1] - hist_bins[0]
    # Collect same/not same data
    distributions = []
    labels = []
    for dist_name, data_sample in data_vec.items():
        distributions.append(np.histogram(np.array(data_sample), bins=hist_bins, normed=True)[0])
        labels.append(dist_name)

    # Show same/not same distributions
    _, ax = plt.subplots(n_dist)
    for dist_idx, single_dist in enumerate(distributions):
        ax[dist_idx].bar(hist_bins[1:], single_dist, alpha=0.7, width = dx, label=labels[dist_idx])
        if labels[dist_idx]!='diff event':
            hist_dist = wasserstein_distance(single_dist, distributions[labels.index('diff event')])
            ax[dist_idx].text(0.1, 0.9, 'dist to diff: %.2f' % hist_dist)

        # ax[dist_idx].set_xlim((0.5,1.75))
        if metric=='cosine':
            ax[dist_idx].set_xlim((-1.,1))
        else:
            ax[dist_idx].set_xlim((0.,1.75))
        ax[dist_idx].set_ylim((0,single_dist.max()))
        ax[dist_idx].legend(loc='upper right')
    if output_dir is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, '{}_same_not_same_hist.png'.format(exp_name)))

def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            path = os.path.join(root, movie_name)
            if os.path.isfile(path) or os.path.isdir(path):
                return root

    print('no movies found in next paths :')
    for optional_dir in optional_dirs:
        print(os.path.join(optional_dir, movie_name))

def load_recording_cache(cache_file, root_dir):
    cache_dir = locate_movie(cache_file, root_dir)
    if cache_dir is None:
        cache_dir = find_and_download(cache_file, 'regression_data', root_dir)
    cache_full_path = os.path.join(cache_dir, cache_file)
    # Parse cache file
    # ================
    logger = BaseLogger(True)
    el_list = logger.from_data(cache_full_path)
    # Remove non-classified items
    el_list = [el['element'].data for el in el_list if el['type'] in ('event_aggregator_out')]
    
    return el_list, cache_dir

def extract_descriptors(model, event):
    mv_idx = 0
    frames = []; cams = []; sources = []; descs = np.zeros((0, 64), dtype=np.float32)
    while mv_idx < len(event['movement_vector']):
        next_batch = event['movement_vector'][mv_idx:mv_idx+model.batch_size]
        mv_idx += model.batch_size
        images = [be.cropped_image for be in next_batch]
        cams += [be.cam_name for be in next_batch]
        frames += [be.global_frame_number for be in next_batch] 
        sources += [be.source for be in next_batch] 
        _, batch_desc, _ = model.predictBatch(images, prnt=False)
        descs = np.vstack((descs, batch_desc))
    return dict(descs=descs, frames=frames, cams=cams, sources=sources)

def calc_same_event_similarity(event_info, metric='cosine'):
    ev_desc = event_info['descs']
    all_distances = pdist(ev_desc, metric=metric)
    front_cam_desc = event_info['descs'][np.array([idx for idx,cn in enumerate(event_info['cams']) if cn=='front'], dtype=np.int)]
    back_cam_desc = event_info['descs'][np.array([idx for idx,cn in enumerate(event_info['cams']) if cn=='back'], dtype=np.int)]
    front_dist = np.inf
    back_dist = np.inf
    same_cam_dist = []
    if len(front_cam_desc) > 1:
        front_distances = pdist(front_cam_desc, metric=metric)
        front_dist = front_distances.mean()
        same_cam_dist.append(front_dist)
    if len(back_cam_desc) > 1:
        back_distances = pdist(back_cam_desc, metric=metric)
        back_dist = back_distances.mean()
        same_cam_dist.append(back_dist)
    same_cam_dist = np.array(same_cam_dist)
    same_cam_dist = same_cam_dist.mean() if same_cam_dist.shape[0] > 0 else None

    seq_dist_internal = all_distances.mean()
    # print('Descriptors Similarity Debug: event dist: %.2f, front %.2f, back %.2f' % (seq_dist_internal, front_dist, back_dist))
    if metric=='cosine':
        return 1 - seq_dist_internal, 1 - same_cam_dist
    else:
        return seq_dist_internal, same_cam_dist

def calc_diff_events_similarity(ev1, ev2, metric='cosine'):
    ev_desc1 = ev1['descs']
    ev_desc2 = ev2['descs']
    seq12_distances = cdist(ev_desc1, ev_desc2, metric=metric)
    ui = np.triu_indices(seq12_distances.shape[0], m=seq12_distances.shape[1]) 
    seq12_distances = seq12_distances[ui]
    seq12_dist = seq12_distances.mean()
    if metric=='cosine':
        return 1 - seq12_dist
    else:
        return seq12_dist
    


DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='product similarity benchmark')
    parser.add_argument('--model-name', help='product classifier model to test',type=str, required=True)
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--movies-file', help='path to csv file containing files to run',type=str, required=True)
    parser.add_argument('--metric', help='type of metric to check',type=str, default='euclidean')

    args = parser.parse_args()
    assert args.metric in ('euclidean', 'cosine')

    # Load similarity network
    # =======================
    model = SimilarityModel.MultiHeadClassifier(model_name=args.model_name,
                                                sync=False, batch_size=4)
    # Run over cache files
    # ====================
    same_event_dist = []
    same_event_and_cam_dist = []
    diff_event_dist = []
    diff_event_probably_same_prod = [] # assume most similar event is the same prod again
    rec_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()
    for rec_file in tqdm(rec_list):
        rec_events, rec_dir = load_recording_cache(rec_file, args.root_dir)
        single_rec_events = []
        for event in rec_events:
            # Extract descriptors
            event_info = extract_descriptors(model, event)
            if len(event_info['descs']) < 3:
                continue
            single_rec_events.append(event_info)
        # Measure similarity distributions
        for ev1_idx, ev1 in enumerate(single_rec_events):
            # Calculate same event distances
            event_dist, cam_event_dist = calc_same_event_similarity(ev1, metric=args.metric)
            same_event_dist.append(event_dist)
            if cam_event_dist is not None:
                same_event_and_cam_dist.append(cam_event_dist)
            ev1_dists = []
            for _, ev2 in enumerate(single_rec_events[ev1_idx+1:]):
                # Calculate distance between different events
                ev1ev2_dist = calc_diff_events_similarity(ev1, ev2, metric=args.metric)
                ev1_dists.append(ev1ev2_dist)
            # For the first half of events, take out the most similar event as it's probably the same product
            if ev1_idx < len(single_rec_events)/2:
                ev1_dists = sorted(ev1_dists)
                if args.metric=='cosine':
                    most_similar = ev1_dists.pop(-1)
                else:
                    most_similar = ev1_dists.pop(0)
                diff_event_probably_same_prod.append(most_similar)
            diff_event_dist += ev1_dists
    
    # Visualize results
    # =================
    # dist_vec = [same_event_dist, same_event_and_cam_dist, diff_event_probably_same_prod, diff_event_dist]
    labels = ['same event', 'same event&cam', 'same prod', 'diff event']
    dist_vecs = {'same_event': same_event_dist, 
                 'same event&cam': same_event_and_cam_dist,
                 'same prod': diff_event_probably_same_prod,
                 'diff event': diff_event_dist}
    visualize_same_not_same_dists(dist_vecs, metric=args.metric)