import argparse
import os
import numpy as np
from tqdm import tqdm
import cv2
import pandas as pd
from cv_blocks.misc.aws_download import find_and_download
from cv_blocks.misc.logger import BaseLogger
import xml.etree.ElementTree as ET
import pickle


ALLOWED_SUBDIRS = ['cart_bottom_rgb_crop', 'front_wnoise_crop', 'back_rgb_crop']

def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            path = os.path.join(root, movie_name)
            if os.path.isfile(path) or os.path.isdir(path):
                return root

    print('no movies found in next paths :')
    for optional_dir in optional_dirs:
        print(os.path.join(optional_dir, movie_name))


def load_recording_cache(cache_file, root_dir):
    assert cache_file.endswith('.pkl'), "file name: {} does not end in .pkl!".format(cache_file)
    cache_dir = locate_movie(cache_file, root_dir)
    if cache_dir is None:
        cache_dir = find_and_download(cache_file, 'regression_data', root_dir)
    cache_full_path = os.path.join(cache_dir, cache_file)
    # Parse cache file
    # ================
    logger = BaseLogger(True)
    el_list = logger.from_data(cache_full_path)
    # Remove non-classified items
    el_list = [el['element'].data for el in el_list if el['type'] in ('event_aggregator_out')]

    return el_list, cache_dir


def get_images(root_dir, input_dirs, allowed_subdirs, class_names, use_gt):
    images = []
    if allowed_subdirs is None:
        allowed_subdirs = ALLOWED_SUBDIRS
    for dataset_dir in input_dirs.split(','):
        root_dir = os.path.join(root_dir, dataset_dir, 'product_classifier')
        if not use_gt:
            for product in os.listdir(root_dir):
                for subdir in allowed_subdirs:
                    for image_name in os.listdir(os.path.join(root_dir, product, subdir)):
                        full_image_path = os.path.join(root_dir, product, subdir, image_name)
                        images.append(dict(img=full_image_path, gt=None))
        else:
            xml_paths = [os.path.join(root_dir, file_name) for file_name in os.listdir(root_dir) if
                         "multi_classifier_annotations" in file_name]
            if len(xml_paths) > 0:
                for xml_path in xml_paths:
                    images = decode_cvat_xml(xml_path, images, class_names)
            else:
                for product in os.listdir(root_dir):
                    if not os.path.isdir(os.path.join(root_dir, product)):
                        continue
                    xml_path = os.path.join(root_dir, product, 'annotations.xml')
                    if os.path.exists(xml_path):
                        images = decode_cvat_xml(xml_path, images, class_names, singles=True)
    return images


def decode_cvat_xml(path, images, class_names, singles=False):
    annots_dict = list()
    dir = os.path.dirname(path)
    tree = ET.parse(path).getroot()
    missing_labels = 0
    if singles:  # no multi annotations - using regular classifier annotations
        valid_labels = ('valid',)
    else:
        valid_labels = ('single item', '2 items', '3 items', 'no item', 'multiple items')
    for elem in tree:
        if elem.tag == 'image':
            attributes = elem.attrib
            image_name = attributes['name'].split('/')[-1]
            label = elem[0][0].text
            if label not in valid_labels:
                if label is None:
                    missing_labels += 1
                continue
            subdir = 'cart_bottom_rgb_crop' if 'cart_bottom' in attributes['name'] else 'front_wnoise_crop' if \
                'front' in attributes['name'] else 'back_rgb_crop'
            if not singles:
                subdir = os.path.join(attributes['name'].split('/')[-3], subdir)
            full_addr = os.path.join(dir, subdir, image_name)
            full_addr = full_addr.replace('front_rgb_crop', 'front_wnoise_crop')
            if not os.path.exists(full_addr):
                print("image {} not found!!!".format(full_addr))
                continue
            if singles:
                gt = 'single item'
            else:
                if label in ('2 items', '3 items') and label not in class_names and 'multiple items' in class_names:
                    gt = 'multiple items'
                elif label not in class_names:
                    continue
                else:
                    gt = label
            images.append(dict(img=full_addr, gt=gt))
    if missing_labels > 10:
        print("missing labels for {}:\n{}".format(path, missing_labels))
    return images


def valid_event(event, analyzed_events):
    for ae in analyzed_events:
        if event['event_direction'] == ae['direction'] and event['event_frame'] == ae['pred_frame']:
            return True
    return False


def choose_images_for_stack_classifier(images, n_images=6, method='spread_uniform'):
    if len(images) <= n_images:
        return [img.cropped_image for img in images]
    if method == 'spread_uniform':
        indices = list(np.linspace(0, len(images), endpoint=False, num=n_images, dtype=int))
    elif method == 'spread_det_conf':
        # split to n_images parts and choose the image highest det_conf from each
        indices = []
        scores = None
        el_in_part = len(images) / n_images
        for i in range(n_images):
            i_s = int(i * el_in_part)
            i_f = int((i + 1) * el_in_part)
            det_conf = []
            for image in images[i_s:i_f]:
                if image.source == 'CB':
                    det_conf.append(-2)
                else:
                    det_conf.append(image.detector_conf)
            indices.append(i_s + int(np.argmax(det_conf)))
    else:
        scores = []
        for image in images:
            if image.source == 'CB':
                scores.append(-1)
            elif image.detector_conf == -1:
                scores.append(0)
            else:
                detector_conf = image.detector_conf
                eve_det_score = image.event_detector_score
                area = image.coords.area()
                area_score = area * 10 if area <= 0.1 else (0.15 - area / 2) * 10 if area <= 0.3 else 0
                score = max(detector_conf, 0) * 0.5 + area_score * 0.5
                scores.append(score)
        indices = np.argsort(scores)[::-1][:n_images]
    # viz_scores(images, scores, indices)
    return [images[idx].cropped_image for idx in indices]


def viz_scores(images, scores, indices):
    all_images = []
    for i, img in enumerate(images):
        image = img.cropped_image.copy()
        image = cv2.resize(image, (64, 64))
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        if scores is not None:
            cv2.putText(image, 'S: {:.2f}'.format(scores[i]),
                        (5, 10), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.5, (255, 0, 0))
        all_images.append(image)
    cv2.imshow("All images", np.hstack(all_images))
    chosen_images = [all_images[idx] for idx in indices]
    cv2.imshow("Chosen images", np.hstack(chosen_images))
    cv2.waitKey(0)
    return


def get_multiple_indices(class_names):
    multiple_indices = []
    for i, class_name in enumerate(class_names):
        if class_name in ("2 items", "3 items", "multiple items"):
            multiple_indices.append(i)
    return multiple_indices


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='object detector benchmark')
    parser.add_argument('--backend', help='backend to use for CNN[trt|torch]',type=str, default='trt')
    parser.add_argument('--mode', help='images / capsules', type=str, default='capsules')
    parser.add_argument('--root-dir', help='Path for images + annotations or capsules root directory',type=str, required=True)
    parser.add_argument('--input-dirs', help='Dataset directories to load from (for mode: images)',type=str, default=None)
    parser.add_argument('--allowed-subdirs', help='Allowed subdirs to load images',type=str, default=None)
    parser.add_argument('--movies-file', help='Csv containing capsules (for mode: capsules)',type=str, default=None)
    parser.add_argument('--output-dir', help='Path for results',type=str, default=None)
    parser.add_argument('--conf-file', help='Path to config file',type=str, default=None)
    parser.add_argument('--weights', help='Detector weight file',type=str, required=True)
    parser.add_argument('--viz-all', help='visualize all detections', action='store_true')
    parser.add_argument('--viz-failure', help='visualize detector failure', action='store_true')
    parser.add_argument('--no-gt', help='visualize detector failure', action='store_true')
    args = parser.parse_args()

    assert args.mode in ['images', 'capsules']
    assert args.backend in ['torch', 'trt']
    assert (not args.viz_all and not args.viz_failure) or args.output_dir, \
        "for visualization please provide output directory"

    # load model
    if args.backend == 'torch':
        print("torch backend is not supported at the moment")
        exit(0)
        # assert os.path.exists(args.backend.endswith('.pth'))
        # assert os.path.exists(args.conf_file)
        # from cv_blocks.object_detector.yolo_detector.torch_impl.multi_att_yolo import MultiAttYoloObjectDetector as YOLO
    else:  # args.backend == 'trt'
        assert os.path.exists(args.backend.endswith('.onnx'))
        from cv_blocks.stacked_classifier.stacked_classifier import StackedClassifier
        model = StackedClassifier(weights=args.weights, model_config=args.conf_file)

    if args.mode == 'images':
        assert args.input_dirs is not None
        dataset = get_images(root_dir=args.root_dir, input_dirs=args.input_dirs, allowed_subdirs=args.allowed_subdirs,
                             class_names=model.class_names, use_gt=not args.no_gt)
        confusion_matrix = np.zeros((len(model.class_names), len(model.class_names)))
        for d in tqdm(dataset):
            image = cv2.imread(d['img'])
            gt = d['gt']
            probs = model.predict(image[:, :, ::-1])
            top_prob = model.class_names[np.argmax(probs)]
            viz_image = model.visualize_input(image, probs, gt)
            if not args.no_gt:
                confusion_matrix[model.class_names.index(gt), model.class_names.index(top_prob)] += 1
                if args.viz_failure and gt != top_prob:
                    output_path = os.path.join(args.output_dir, 'failures', 'gt_{}'.format(gt),
                                               'pred_{}'.format(top_prob), os.path.basename(d['img']))
                    os.makedirs(os.path.dirname(output_path), exist_ok=True)
                    cv2.imwrite(output_path, viz_image)
                if args.viz_all:
                    output_path = os.path.join(args.output_dir, 'all', 'gt_{}'.format(gt),
                                               'pred_{}'.format(top_prob), os.path.basename(d['img']))
                    os.makedirs(os.path.dirname(output_path), exist_ok=True)
                    cv2.imwrite(output_path, viz_image)
            elif args.viz_all:
                output_path = os.path.join(args.output_dir, 'all', 'pred_{}'.format(top_prob),
                                           os.path.basename(d['img']))
                os.makedirs(os.path.dirname(output_path), exist_ok=True)
                cv2.imwrite(output_path, viz_image)
        if not args.no_gt:
            print("Confusion matrix:\n", confusion_matrix)
            n_images = np.sum(confusion_matrix, axis=1)
            sorted_pred_ind = np.argsort(confusion_matrix, axis=1)
            for i in range(len(model.class_names)):
                print("Class: {}\n\tTotal images: {}".format(model.class_names[i], int(n_images[i])))
                pred_ind = list(sorted_pred_ind[i])[::-1]
                for j in pred_ind:
                    print("\tPredicted - {}: {:.3f}".format(model.class_names[j], confusion_matrix[i,j] / n_images[i]))
            print("Accuracy: {}".format(np.sum(np.diag(confusion_matrix)) / np.sum(confusion_matrix)))
    else:  # args.mode == 'capsules'
        assert args.movies_file
        rec_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)[
            'movie'].tolist()
        multiple_indices = get_multiple_indices(model.class_names)
        for rec_file in tqdm(rec_list):
            rec_events, rec_dir = load_recording_cache(rec_file, args.root_dir)
            results_file = os.path.join(rec_dir, rec_file.replace('.pkl', '_L.results').split('shop_mode_log_')[1])
            if os.path.exists(results_file):
                res, analyzed_events, eve_det = pickle.load(open(results_file, 'rb'))
            else:
                print("Results file were not found for {}. Skipping.".format(rec_file))
                continue
            for event in rec_events:
                if valid_event(event, analyzed_events):
                    images = event['movement_vector']
                    chosen_images = choose_images_for_stack_classifier(images)
                    multiple_scores = []
                    if args.viz_all:
                        output_dir = os.path.join(args.output_dir, str(event['event_id']))
                        os.makedirs(os.path.join(output_dir, 'all_images'), exist_ok=True)
                        os.makedirs(os.path.join(output_dir, 'chosen_images'), exist_ok=True)
                        for i, image in enumerate(images):
                            cv2.imwrite(os.path.join(output_dir, 'all_images', 'img_{}.jpg'.format(i)), image.cropped_image[:, :, ::-1])
                    for i, image in enumerate(chosen_images):
                        if args.viz_all:
                            cv2.imwrite(os.path.join(output_dir, 'chosen_images', 'img_{}.jpg'.format(i)), image[:, :, ::-1])
                        prediction = model.predict(image)[0]
                        multiple_score = sum([prediction[idx] for idx in multiple_indices])
                        multiple_scores.append(multiple_score)
                    # print("multiple scores: ", multiple_scores)
                    print("sum: {:.2f}, above 0.5: {}, above 0.9: {}".format(
                        sum(multiple_scores), sum([score > 0.5 for score in multiple_scores]),
                        sum([score > 0.95 for score in multiple_scores])))
                    if sum(multiple_scores) > 4.5 or sum([score > 0.5 for score in multiple_scores]) >= 5 or sum([score > 0.95 for score in multiple_scores]) >= 4:
                        print("Event id: {}, frame: {}, direction: {}, predicted multiple!!!".format(event['event_id'], event['event_frame'], event['event_direction']))
                    else:
                        print("Event id: {}, frame: {}, direction: {}, predicted single".format(event['event_id'], event['event_frame'], event['event_direction']))
                else:
                    print("Skipping event id: {}, frame: {}, direction: {}".format(event['event_id'], event['event_frame'], event['event_direction']))
