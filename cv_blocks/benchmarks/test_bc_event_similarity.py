import argparse
import os
import getpass
import cv2
import tensorrt_lib.Trt_cls_multi_head as SimilarityModel
from cv_blocks.misc.aws_download import find_and_download
from cv_blocks.misc.logger import BaseLogger
import pandas as pd
from tqdm import tqdm
import numpy as np
from scipy.spatial.distance import pdist, cdist
from matplotlib import pyplot as plt

def visualize_dists(data_vec, path=None, labels=[]):
    n_dist = len(data_vec)
    assert len(data_vec)==len(labels)
    hist_bins = np.linspace(0, 2 , 100)
    dx = hist_bins[1] - hist_bins[0]
    # Collect same/not same data
    distributions = []
    for data_sample in data_vec:
        distributions.append(np.histogram(np.array(data_sample), bins=hist_bins)[0])

    # Show same/not same distributions
    _, ax = plt.subplots(n_dist, figsize=(16,16))
    if n_dist==1:
        ax = [ax]
    for dist_idx, single_dist in enumerate(distributions):
        ax[dist_idx].bar(hist_bins[1:], single_dist, alpha=0.7, width = dx, label=labels[dist_idx])
        ax[dist_idx].set_xlim((0.5,1.75))
        ax[dist_idx].set_ylim((0,single_dist.max()))
        ax[dist_idx].set_title('ave dist(%d matches): %.2f' % (len(data_vec[dist_idx]), data_vec[dist_idx].mean()))
        ax[dist_idx].legend(loc='upper right')
    if path is None:
        plt.show()
    else:
        plt.savefig(path)

def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            path = os.path.join(root, movie_name)
            if os.path.isfile(path) or os.path.isdir(path):
                return root

    print('no movies found in next paths :')
    for optional_dir in optional_dirs:
        print(os.path.join(optional_dir, movie_name))

def load_recording_cache(cache_file, root_dir):
    cache_dir = locate_movie(cache_file, root_dir)
    if cache_dir is None:
        cache_dir = find_and_download(cache_file, 'regression_data', root_dir)
    cache_full_path = os.path.join(cache_dir, cache_file)
    # Parse cache file
    # ================
    logger = BaseLogger(True, include_barocde=True)
    el_list = logger.from_data(cache_full_path)
    # Find pairs of barcode event + insert event
    barcode_events = [el['element'].data for el in el_list if el['type']=='barcode_event_img']
    insert_events = [el['element'].data for el in el_list if el['type'] in ('event_aggregator_out') \
        and el['element'].data['event_direction']=='in']
    
    bc_event_pairs = []
    for bc_event in barcode_events:
        bc_event_frame = bc_event[0].global_frame_number
        best_frame_diff = np.inf
        best_in_ev_match = None
        for in_ev in insert_events:
            frame_diff = in_ev['event_start'] - bc_event_frame
            if 0 < frame_diff < best_frame_diff:
                best_in_ev_match = in_ev
                best_frame_diff = frame_diff
        if best_in_ev_match is not None:
            bc_event_pairs.append(dict(bc=bc_event, insert_event=best_in_ev_match))
    
    print('Found %d bc-event pairs' % (len(bc_event_pairs)))
    return bc_event_pairs

def extract_descriptors(model, event):
    mv_idx = 0
    frames = []; cams = []; sources = []; descs = np.zeros((0, 64), dtype=np.float32)
    while mv_idx < len(event['movement_vector']):
        next_batch = event['movement_vector'][mv_idx:mv_idx+model.batch_size]
        mv_idx += model.batch_size
        images = [be.cropped_image for be in next_batch]
        cams += [be.cam_name for be in next_batch]
        frames += [be.global_frame_number for be in next_batch] 
        sources += [be.source for be in next_batch] 
        _, batch_desc = model.predictBatch(images, prnt=False)
        descs = np.vstack((descs, batch_desc))
    return dict(descs=descs, frames=frames, cams=cams, sources=sources)

def calc_same_event_similarity(event_info):
    ev_desc = event_info['descs']
    all_distances = pdist(ev_desc)
    front_cam_desc = event_info['descs'][np.array([idx for idx,cn in enumerate(event_info['cams']) if cn=='front'], dtype=np.int)]
    back_cam_desc = event_info['descs'][np.array([idx for idx,cn in enumerate(event_info['cams']) if cn=='back'], dtype=np.int)]
    front_dist = np.inf
    back_dist = np.inf
    same_cam_dist = []
    if len(front_cam_desc) > 1:
        front_distances = pdist(front_cam_desc)
        front_dist = front_distances.mean()
        same_cam_dist.append(front_dist)
    if len(back_cam_desc) > 1:
        back_distances = pdist(back_cam_desc)
        back_dist = back_distances.mean()
        same_cam_dist.append(back_dist)
    same_cam_dist = np.array(same_cam_dist)
    same_cam_dist = same_cam_dist.mean() if same_cam_dist.shape[0] > 0 else None

    seq_dist_internal = all_distances.mean()
    # print('Descriptors Similarity Debug: event dist: %.2f, front %.2f, back %.2f' % (seq_dist_internal, front_dist, back_dist))
    return seq_dist_internal, same_cam_dist

def calc_diff_events_similarity(ev1, ev2):
    ev_desc1 = ev1['descs']
    ev_desc2 = ev2['descs']
    seq12_distances = cdist(ev_desc1, ev_desc2)
    ui = np.triu_indices(seq12_distances.shape[0], m=seq12_distances.shape[1]) 
    seq12_distances = seq12_distances[ui]
    seq12_dist = seq12_distances.mean()
    return seq12_dist

def viz_desc_dist(bc_data, ev_data, dist, path=None):
    fig = plt.figure(figsize=(16, 8))
    ax_bc = fig.add_axes([0., 0., 0.5, 0.8, ])
    ax_ev = fig.add_axes([0.5, 0., 0.5, 0.8, ])
    txt = 'Descriptor dist = %.3f' % dist
    fig.suptitle(txt)
    ax_bc.imshow(bc_data['img'], aspect='auto')
    ax_bc.set_axis_off()
    ax_bc.set_title('Barcode[%s] crop(%dx%d)' % (bc_data['cam'], bc_data['img'].shape[0], bc_data['img'].shape[1]))
    ax_ev.imshow(ev_data['img'], aspect='auto')
    ax_ev.set_axis_off()
    ax_ev.set_title('Event[%s] crop(%dx%d)' % (ev_data['cam'], ev_data['img'].shape[0], ev_data['img'].shape[1]))
    if path is not None:
        plt.savefig(path)
    else:
        plt.show()
    plt.close()

def calc_distances(ev1, ev2):
    ev_desc1 = ev1['descs']
    ev_desc2 = ev2['descs']
    seq12_distances = cdist(ev_desc1, ev_desc2)
    return seq12_distances

def analyze_pair(bacrode, insert_event, output_dir, viz=False):
    # Analyze all vs. all
    all_dists = calc_distances(bacrode, insert_event)
    # Cam vs. Cam distances
    dist_per_cam = dict()
    for bc_cam in ('front', 'back'):
        bc_cam_idx = [idx for idx, el in enumerate(bacrode['cams']) if el==bc_cam]
        if len(bc_cam_idx)==0:
            continue
        for ev_cam in ('front', 'back'):
            ev_cam_idx = [idx for idx, el in enumerate(insert_event['cams']) if el==ev_cam]
            if len(ev_cam_idx)==0:
                continue
            per_cam_dist = np.array([all_dists[i,j] for i in bc_cam_idx for j in ev_cam_idx])
            dist_per_cam['%s,%s' % (bc_cam, ev_cam)] = per_cam_dist
    # Plot per cam distributions
    # ==========================
    data_vec = []; labels = []
    for k, v in dist_per_cam.items():
        labels.append(k)
        data_vec.append(v)
    path = os.path.join(output_dir, 'per_cam_distributions.png')
    visualize_dists(data_vec, path=path, labels=labels)

    # Visualize pairs
    if viz==True:
        for bc_idx in range(len(bacrode['imgs'])):
            bc_data = dict(img=bacrode['imgs'][bc_idx], cam=bacrode['cams'][bc_idx])
            for ev_idx in range(len(insert_event['imgs'])):
                ev_data = dict(img=insert_event['imgs'][ev_idx], cam=insert_event['cams'][ev_idx])
                dist = all_dists[bc_idx, ev_idx]
                path = os.path.join(output_dir, 'bc_%s_idx_%03d_ev_%s_idx_%03d.png' % (bc_data['cam'], bc_idx, ev_data['cam'], ev_idx))
                viz_desc_dist(bc_data, ev_data, dist, path=path)


DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='product similarity benchmark')
    parser.add_argument('--model-name', help='product classifier model to test',type=str, required=True)
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--capsules-file', help='path to csv file containing files to run',type=str, required=True)
    parser.add_argument('--output-dir', help='path to write results to',type=str, default='/tmp/bc_ev_similarity')
    parser.add_argument('--min-crop-area', help='minimum relative crop area',type=float, default=0.01)
    parser.add_argument('--rotate', help='make sure larger dim is height', action='store_true')
    parser.add_argument('--viz', help='make sure larger dim is height', action='store_true')

    args = parser.parse_args()

    # Load similarity network
    # =======================
    model = SimilarityModel.MultiHeadClassifier(model_name=args.model_name,
                                                sync=False, batch_size=4)
    # Run over cache files
    # ====================
    same_event_dist = []
    same_event_and_cam_dist = []
    diff_event_dist = []
    diff_event_probably_same_prod = [] # assume most similar event is the same prod again
    capsule_list = pd.read_csv(args.capsules_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()
    for capsule_file in tqdm(capsule_list):
        bc_events_pairs = load_recording_cache(capsule_file, args.root_dir)
        for pair_idx, single_pair in enumerate(bc_events_pairs):
            results_dir = os.path.join(args.output_dir, os.path.basename(capsule_file), 'event_%03d' % pair_idx)
            os.makedirs(results_dir, exist_ok=True)
            # Compute image descriptors
            # =========================
            bc_idx = [idx for idx, bc_el in enumerate(single_pair['bc']) if bc_el.coords.area() > args.min_crop_area]
            if args.rotate:
                bc_imgs = [bc_el.cropped_image.copy() if bc_el.coords.h() > bc_el.coords.w() else \
                    cv2.rotate(bc_el.cropped_image, cv2.ROTATE_90_COUNTERCLOCKWISE) \
                    for idx, bc_el in enumerate(single_pair['bc']) if idx in bc_idx]
            else:
                bc_imgs = [bc_el.cropped_image.copy()for idx, bc_el in enumerate(single_pair['bc']) if idx in bc_idx]
            bc_cams = [bc_el.cam_name for idx, bc_el in enumerate(single_pair['bc']) if idx in bc_idx]

            ev_idx = [idx for idx, ev_el in enumerate(single_pair['insert_event']['movement_vector']) if ev_el.coords.area() > args.min_crop_area]
            if args.rotate:
                ev_imgs = [ev_el.cropped_image.copy() for idx, ev_el in enumerate(single_pair['insert_event']['movement_vector']) if idx in ev_idx]
                ev_imgs = [ev_el.cropped_image.copy() if ev_el.coords.h() > ev_el.coords.w() else \
                    cv2.rotate(ev_el.cropped_image, cv2.ROTATE_90_COUNTERCLOCKWISE) \
                    for idx, ev_el in enumerate(single_pair['insert_event']['movement_vector']) if idx in ev_idx]
            else:
                ev_imgs = [ev_el.cropped_image.copy() for idx, ev_el in enumerate(single_pair['insert_event']['movement_vector']) if idx in ev_idx]
            ev_cams = [ev_el.cam_name for idx, ev_el in enumerate(single_pair['insert_event']['movement_vector']) if idx in ev_idx]

            bc_desc = model.extract_descriptors(bc_imgs)
            ev_desc = model.extract_descriptors(ev_imgs)
            bacrode = dict(imgs=bc_imgs, cams=bc_cams, descs=bc_desc)
            insert_event = dict(imgs=ev_imgs, cams=ev_cams, descs=ev_desc)
            # Analyze results
            # ===============
            analyze_pair(bacrode, insert_event, results_dir, viz=args.viz)