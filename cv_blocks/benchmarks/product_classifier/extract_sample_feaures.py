import numpy as np
from cart_blocks import catalog as Catalog
from config import config
import pandas as pd
import cv2


class ClassifierSampleFeatureExtractor(object):
    '''
    Extract sample features for classifier
    '''
    def __init__(self, idx2product):
        self.catalog = Catalog.Catalog(def_language=config.GUI_LANGUAGE)
        self.include_events = ('EventDetected', 'ProductDetectedInTopK', 'ProductDetected')
        self.idx2product = idx2product
        self.feature_df = None
        from scipy.stats import entropy
        self.entropy = entropy

    def extract_from_sequence(self, seq):
        if seq['result']['state'].name not in self.include_events:
            return
        pred_idx = seq['event']['classification_prob_mat'].argmax(axis=-1)
        pred_sku = [self.idx2product(i) for i in pred_idx]
        # Features
        feat_dict = dict(
            cam_name = [m.cam_name for m in seq['event']['movement_vector_for_saving']],
            img_source = [m.source for m in seq['event']['movement_vector_for_saving']],
            height = [m.coords.h() for m in seq['event']['movement_vector_for_saving']],
            width = [m.coords.w() for m in seq['event']['movement_vector_for_saving']],
            area = [m.coords.area() for m in seq['event']['movement_vector_for_saving']],
            detector_conf = [m.detector_conf for m in seq['event']['movement_vector_for_saving']],
            entropy = [self.entropy(cv2.cvtColor(m.cropped_image, cv2.COLOR_RGB2GRAY).flatten()) for m in seq['event']['movement_vector_for_saving']],
        )
        # Result
        feat_dict['correct_pred'] = [p==seq['result']['name'] for p in pred_sku]
        feat_dict['gt_sku'] = seq['result']['name']
        if self.feature_df is None:
            self.feature_df = pd.DataFrame(feat_dict)
        else:
            self.feature_df = pd.concat([self.feature_df, pd.DataFrame(feat_dict)], ignore_index=True)
        
    def save_results(self, path):
        self.feature_df.to_csv(path)
    
    def accuracy(self):
        return self.feature_df['correct_pred'].mean()