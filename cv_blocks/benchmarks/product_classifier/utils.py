import numpy as np
import os
import MotionDetect
from cart_blocks import catalog as Catalog
from config import config
from cv_blocks.metrics.shopping_metrics import shopping_accuracy,shopping_accuracy_msco, summarize_recording_metrics, ItemStatus
from external_libs.tabulate import tabulate
from WalkoutExecutor import result_line

class MotionDetectLight(MotionDetect.MotionDetect):
    def __init__(self):
        self.purchaseList = []
        self.catalog = Catalog.Catalog(def_language=config.GUI_LANGUAGE)
        self.gui = None
        self.global_frame_num = 0 # dummy
        self.eval_mode = True
    
    def init_md(self, model_path='default', dataGenPath=None, classMapPath=None, eval_mode=False):
        pass

    def reset(self):
        self.purchaseList = []

def compare_to_gt(md_module, gt, movie_file, location_predictions=None, classifier=None, msco=False):
    walkout_prediction = md_module.purchaseList
    non_supported_products = []
    if classifier is not None:
        non_supported_products = [gt_ev['name'] for gt_ev in gt if not classifier.is_class_supported(gt_ev['name'])]
    if msco:
        analyzed_events = shopping_accuracy_msco(walkout_prediction, gt)
        result_dict = summarize_recording_metrics(analyzed_events, md_module.catalog, log_errors=True, log_interactions=False)
    else:
        analyzed_events = shopping_accuracy(walkout_prediction, gt, location_predictions=location_predictions)
        result_dict = summarize_recording_metrics(analyzed_events, md_module.catalog, log_errors=True, ignore_products=non_supported_products)
    resultsTable = [['Movie Name', 'Cls Success', 'MD Success']]
    resultsTable.append(result_line(movie_file.split('.avi')[0], result_dict, bold=True))
    print(tabulate(resultsTable, headers="firstrow", tablefmt="pretty")) #printing a summary table
    return result_dict, analyzed_events

def compare_location_to_MD(analyzed_events, movie_file, location_predictions):
    loc_results = dict(total=0, top1_success=0, top3_success=0, errors=0, ignored=0, no_prediction=0, all_scores=[])
    missed_products_in = []
    for md_event in analyzed_events:
        if md_event['state'] == ItemStatus.EventMissed and md_event['direction'] == 'in':
            missed_products_in.append(md_event['name'])
    for loc_pred in location_predictions:
        for md_out_event in analyzed_events:
            if loc_pred['event_out_frame'] == md_out_event['pred_frame']:
                gt_product = md_out_event['name']
                if md_out_event['state'] not in (ItemStatus.ProductDetected, ItemStatus.ProductDetectedInTopK,
                                                 ItemStatus.EventDetected):
                    loc_results = update_event_score(loc_results, 'ignored')
                    print("Ignoring event at frame {}. State ignored: {}".format(loc_pred['event_out_frame'],
                                                                                 md_out_event['state']))
                    break
                if gt_product in missed_products_in:
                    loc_results = update_event_score(loc_results, 'ignored')
                    print("Ignoring event at frame {} because in event was missed".format(loc_pred['event_out_frame']))
                    break
                if loc_pred['prod_ids'] is None:
                    loc_results = update_event_score(loc_results, 'no_prediction')
                    break

                found = False
                for md_in_event in analyzed_events:
                    if md_in_event['pred_frame'] in loc_pred['event_in_frames'] and md_in_event['name'] == gt_product:
                        pred_choice_num = loc_pred['event_in_frames'].index(md_in_event['pred_frame'])
                        loc_status = 'top1_success' if pred_choice_num == 0 else 'top3_success'
                        found = True
                        loc_results = update_event_score(loc_results, loc_status, loc_pred['scores'], pred_choice_num)
                        break
                if not found:
                    loc_results = update_event_score(loc_results, 'errors', loc_pred['scores'], None)
                break

    print("loc results for {}: {}".format(movie_file, {key: value for key, value in loc_results.items() if key != 'all_scores'}))

    return loc_results


def update_event_score(loc_results, loc_status, scores=None, choice=None):
    loc_results[loc_status] += 1
    if loc_status == 'ignored':
        return loc_results
    loc_results['total'] += 1
    if scores is not None:
        ratio = scores[0] / scores[1] if len(scores) > 1 else None
        if choice is None:  # error
            loc_results['all_scores'].append(dict(status='error', top1=scores[0], predicted_score=None, ratio=ratio))
        else:  # top1 / top3
            status = 'top1' if choice == 0 else 'top3'
            loc_results['all_scores'].append(dict(status=status, top1=scores[0], predicted_score=scores[choice], ratio=ratio))
    return loc_results


def scores_statistics(keys, scores):
    for key in keys:
        elements = [el for el in scores if el['status'] == key]
        if len(elements) == 0:
            continue
        top1_score = [el['top1'] for el in elements]
        ratio = [el['ratio'] for el in elements if el['ratio'] is not None]
        predicted_score = [el['predicted_score'] for el in elements if el['predicted_score'] is not None]
        print("\nStatistics for {}:".format(key))
        print("top guess score: avg - {:.2f}, median - {:.2f}, std: {:.3f}".format(
            np.average(top1_score), np.median(top1_score), np.std(top1_score)))
        if len(predicted_score) > 0:
            print("correct guess score: avg - {:.2f}, median - {:.2f}, std: {:.3f}".format(
                np.average(predicted_score), np.median(predicted_score), np.std(predicted_score)))
        print("1st / 2nd ratio: avg - {:.2f}, median - {:.2f}, std: {:.3f}".format(
            np.average(ratio), np.median(ratio), np.std(ratio)))


def match_results_to_events(results, sent_events, input_events):
    output = []
    for ev in sent_events:
        if results is not None:
            try:
                ev_result = [r for r in results if r['pred_frame']==ev['event']['event_frame']][0]
            except:
                continue
        else:
            ev_result = None
        output.append(dict(event=ev['event'], prediction=ev['prediction'], result=ev_result))
    return output

def match_event_to_gt_pre_cls(gt, events):
    '''
    Match gt to events only by direction and frame proximity
    '''
    matched_gt = [None] * len(events)
    gt_used = [False] * len(gt)
    for event_idx, event in enumerate(events):
        # Ignore indep. CB events
        event_size = len(event['movement_vector'])
        if event_size < 4:
            continue

        # Check if gt has event is same direction & near frame
        gt_candidates = [(gt_v, gt_i) for gt_i, gt_v in enumerate(gt) if gt_v['direction']==event['event_direction'] \
            and abs(gt_v['frameNum'] - event['event_frame']) < 150 and not gt_used[gt_i]]
        if len(gt_candidates)==1:
            gt_ev, gt_idx = gt_candidates[0]
            diff_curr = abs(gt_ev['frameNum'] - event['event_frame'])
            # Check next event is not also a match
            if event_idx < len(events) - 1:
                next_ev = events[event_idx+1]
                next_event_size = len(event['movement_vector'])
                if next_event_size >= 4:
                    diff_next = abs(gt_ev['frameNum'] - next_ev['event_frame'])
                    if gt_ev['direction']==next_ev['event_direction'] and diff_next<diff_curr:
                        # Next event will be matched
                        continue
            # Valid flow - match single gt to single event
            # if gt_used[gt_idx]:
            #     continue
            matched_gt[event_idx] = gt_ev['name']
            gt_used[gt_idx] = True
        elif len(gt_candidates)==0:
            # Probably made-up or gt too early/too late
            continue
        elif len(gt_candidates)==2:
            diff1 = gt_candidates[0][0]['frameNum'] - event['event_frame']
            diff2 = gt_candidates[1][0]['frameNum'] - event['event_frame']
            # Option 0 - exact frmae (probably fixed gt)
            if diff1==0 or diff2==0:
                gt_ev, gt_idx = gt_candidates[0] if diff1==0 else gt_candidates[1]
                matched_gt[event_idx] = gt_ev['name']
                gt_used[gt_idx] = True
            # Option 1 - modified gt - event & gt are almost at the same frame
            elif abs(diff1) < 50 and abs(float(diff2) / float(diff1)) > 3:
                gt_ev, gt_idx = gt_candidates[0]
                matched_gt[event_idx] = gt_ev['name']
                gt_used[gt_idx] = True
            elif abs(diff2) < 50 and abs(float(diff1) / float(diff2)) > 3:
                gt_ev, gt_idx = gt_candidates[1]
                matched_gt[event_idx] = gt_ev['name']
                gt_used[gt_idx] = True
            else:

                # Option 2 - (gt1, event, gt2):pick gt before event for insert, gt after event for remove
                if diff1 < 0 and diff2 > 0:
                    gt_ev, gt_idx = gt_candidates[0] if event['event_direction']=='in' else gt_candidates[1]
                    matched_gt[event_idx] = gt_ev['name']
                    gt_used[gt_idx] = True
                else:
                    # TODO-see more options
                    continue
        else:
            # More than 2 matching gt events, ignore
            continue
        
    return matched_gt

def collect_event_stats(mr, movie_name=''):
    front_mv = [mv for mv in mr['event']['movement_vector_for_saving'] if mv.cam_name=='front' and mv.source=='MD']
    back_mv = [mv for mv in mr['event']['movement_vector_for_saving'] if mv.cam_name=='back' and mv.source=='MD']
    front_n_tracked = len([1 for mv in front_mv if mv.detector_conf==-1])
    back_n_tracked = len([1 for mv in front_mv if mv.detector_conf==-1])
    front_det = [mv.detector_conf for mv in front_mv if mv.detector_conf>0.]
    back_det = [mv.detector_conf for mv in back_mv if mv.detector_conf>0.]
    front_avg_conf = np.array(front_det).mean()
    back_avg_conf = np.array(back_det).mean()
    front_avg_box = np.array([mv.coords.area() for mv in front_mv]).mean()
    back_avg_box = np.array([mv.coords.area() for mv in back_mv]).mean()
    cb_mv = [mv for mv in mr['event']['movement_vector_for_saving'] if mv.source=='CB']
    stats = dict(prediction_type=mr['prediction'][0].name,
                 top1_conf=mr['prediction'][1][0][1],
                 top2_conf=mr['prediction'][1][1][1],
                 top1_pred=mr['prediction'][1][0][0],
                 top2_pred=mr['prediction'][1][1][0],
                 movie_name=movie_name,
                 event_direction=mr['result']['direction'],
                 gt_product=mr['result']['name'],
                 predicted_product=mr['result']['pred_name'],
                 front_mv_size=len(front_mv),
                 back_mv_size=len(back_mv),
                 cart_bottom_images=len(cb_mv),
                 front_n_tracked=front_n_tracked,
                 back_n_tracked=back_n_tracked,
                 front_avg_conf=front_avg_conf,
                 back_avg_conf=back_avg_conf,
                 front_avg_box=front_avg_box,
                 back_avg_box=back_avg_box,
                 result=mr['result']['state'].name
                 )
    return stats

def plot_precision_recall_graph(x, y, output_dir=None):
    from sklearn.metrics import precision_recall_curve, average_precision_score
    from matplotlib import pyplot as plt
    precision, recall, th = precision_recall_curve(y, x)
    ap = average_precision_score(y, x)
    prec_at_recall99 = precision[np.where(recall>=0.99)[0].max()]
    th_at_recall99 = th[np.where(recall>=0.99)[0].max()]
    plt.figure()
    plt.plot(th, recall[:-1], '-r', label='Recall')
    plt.plot(th, precision[:-1], '-b', label='Precision')
    plt.legend()
    plt.title('Precision & Recall\nPrecision@Recall99=%.3f, Th=%.3f' % (prec_at_recall99, th_at_recall99))
    plt.vlines(th_at_recall99, ymin=0, ymax=1)
    plt.xlabel('Validation threshold[0-1]')
    if output_dir is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, 'precision_recall_validation.png'))
    return th_at_recall99

def plot_accuracy_graph(event_data, output_dir=None):
    from matplotlib import pyplot as plt
    import pandas as pd
    plt.figure()
    pd.DataFrame(event_data[['top1_acc_per_img', 'top2_acc_per_img']]).boxplot()
    plt.title('Seq acc: %.3f, img: top1-acc: %.3f, top2-acc:%.3f' % \
        (event_data['success'].mean(), event_data['top1_acc_per_img'].mean(), event_data['top2_acc_per_img'].mean()))
    if output_dir is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, 'accuracy_plot.png'))
    plt.close()
    # Repeat for failure
    event_data_failure = event_data[event_data['success']==False]
    plt.figure()
    pd.DataFrame(event_data_failure[['top1_acc_per_img', 'top2_acc_per_img']]).boxplot()
    plt.title('Seq acc: %.3f, img: top1-acc: %.3f, top2-acc:%.3f' % \
        (event_data_failure['success'].mean(), event_data_failure['top1_acc_per_img'].mean(), event_data_failure['top2_acc_per_img'].mean()))
    if output_dir is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, 'accuracy_plot_failure.png'))
    plt.close()

def plot_event_validation_viz(event_result, event_data, metadata, output_dir=None):
    from matplotlib import pyplot as plt
    joint_txt = 'Event: %s. ' % event_data['event_type']
    if event_data['event_type']=='valid':
        joint_txt += 'GT prod: %s' %  event_data['gt_product']
    else:
        joint_txt += 'GT: %s, scanned: %s' %  (event_data['gt_product'], event_data['scanned_product'])
    joint_txt +='\nTot Score:%.3f,' % event_data['validation_score']
    if output_dir is not None:
        rec_name = os.path.splitext(event_data['rec_file'])[0]
        dir_name = os.path.join(rec_name, 'event_id_%03d' % event_data['event_id'], '%s_%s' % (event_data['event_type'], event_data['scanned_product']))
        os.makedirs(os.path.join(output_dir, dir_name), exist_ok=True)
    for img_idx, (img_dist, img_score) in enumerate(zip(metadata['distances'], metadata['scores'])):
        mv_img = event_result['movement_vector_for_saving'][img_idx].cropped_image
        plt.figure()
        plt.imshow(mv_img)
        plt.title(joint_txt + 'dist=%.3f, score=%.3f' % (img_dist, img_score))
        plt.axis('off')
        if output_dir is None:
            plt.show()
        else:
            img_path = 'img_%02d.png' % img_idx
            plt.savefig(os.path.join(output_dir, dir_name, img_path))
        plt.close()

def plot_product_reduction_viz(event_result, event_data, metadata, output_dir=None):
    from matplotlib import pyplot as plt
    joint_txt = 'Res: %s. ' % event_data['success']
    joint_txt += 'GT: %s,c_ids:(%d,%d),' %  (event_data['gt_product'], metadata['top1_cluster'], metadata['top2_cluster'])
    joint_txt +='probs:(%.2f, %.2f),' % (metadata['top1_prob'], metadata['top2_prob'])
    if output_dir is not None:
        rec_name = os.path.splitext(event_data['rec_file'])[0]
        dir_name = os.path.join(rec_name, 'event_id_%03d_%s' % (event_data['event_id'], event_data['gt_product']))
        os.makedirs(os.path.join(output_dir, dir_name), exist_ok=True)
    for img_idx, (top1_img_acc, top2_img_acc, c_id) in enumerate(zip(metadata['top1_acc_per_img'], metadata['top2_acc_per_img'], metadata['cluster_per_img'])):
        mv_img = event_result['movement_vector_for_saving'][img_idx].cropped_image
        plt.figure()
        plt.imshow(mv_img)
        plt.title(joint_txt + '\nimg:top1:(%r,%d), top2:(%r,%d)' % (top1_img_acc, c_id[0], top2_img_acc, c_id[1]))
        plt.axis('off')
        if output_dir is None:
            plt.show()
        else:
            img_path = 'img_%02d.png' % img_idx
            plt.savefig(os.path.join(output_dir, dir_name, img_path))
        plt.close()

class EvenLogger(object):  
    def __init__(self, data=None):
        self.data = data

    @staticmethod
    def pack(event):
        ret_dict = dict()
        for k, val in event.items():
            if k=='movement_vector_for_saving':
                ret_dict[k] = [v.pack() for v in val]
            else:
                ret_dict[k] = val

        return ret_dict

    @staticmethod
    def unpack(data):
        from cv_blocks.events.movement_vector import MVElement
        ret_dict = dict()
        for k, val in data.items():
            if k=='movement_vector_for_saving':
                ret_dict[k] = [MVElement.unpack(v) for v in val]
            else:
                ret_dict[k] = val

        return EvenLogger(data=ret_dict)

    def visualize(self, path=None, prefix='event_logic_out', prod_from_idx=None):
        prediction = self.data['prediction'][1][0][0]
        if path is not None:
            import os
            if self.data['result'] is not None: 
                prefix = self.data['result']['state'].name
            mv_path = os.path.join(path, prefix+'_id_%03d_frame_%05d_dir_%s_pred_%s' %\
                 (self.data['event']['event_id'], self.data['event']['event_frame'],\
                  self.data['event']['event_direction'], prediction))
            os.makedirs(mv_path, exist_ok=True)
        else:
            mv_path = None
        if self.data['result'] is not None: 
            extra_txt = '\nGT SKU: %s, pred SKU: %s' % (self.data['result']['name'], self.data['result']['pred_name'])
        else:
            extra_txt = '\npred SKU: %s' % (prediction)
        if len(self.data['prediction'])>1:
            pred_txt = 'Top3:'
            for sku, pred in self.data['prediction'][1][:3]:
                pred_txt += '[%s:%.1f],' % (sku, pred)
            extra_txt += '\n%s' % (pred_txt[:-1])
        for img_idx, img_el in enumerate(self.data['event']['movement_vector_for_saving']):
            if prod_from_idx is not None:
                img_sku_idx = self.data['event']['classification_prob_mat'][img_idx].argmax()
                img_prob = self.data['event']['classification_prob_mat'][img_idx][img_sku_idx]
                img_txt = '\nimg predicted sku: %s, prob: %.2f' % (prod_from_idx(img_sku_idx), img_prob)
            else:
                img_txt = ''
            img_path = mv_path + '/img_%02d.png' % img_idx if path is not None else None
            img_el.visualize(path=img_path, extra_txt=extra_txt + img_txt)