import argparse
import getpass
import os
import numpy as np
import pandas as pd
from config import config
import random
from cv_blocks.benchmarks.product_classifier.utils import MotionDetectLight, \
    match_event_to_gt_pre_cls, plot_precision_recall_graph, plot_event_validation_viz
from cv_blocks.events.event_classifier import EventClassifier
from cv_blocks.misc.logger import BaseLogger
import movie_parser_time_and_dir as mParser
from cv_blocks.misc.aws_download import movie_download, find_and_download
from cv_blocks.metrics.shopping_metrics import ItemStatus, get_only_classifier_supported_events, \
    summarize_recording_metrics, accuracy_per_sku
from tqdm import tqdm
from WalkoutExecutor import result_line, kpi_line
from external_libs.tabulate import tabulate
from cv_blocks.calibration.cart_calib import CartCalib
from cart_blocks import catalog
from matplotlib import pyplot as plt

def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            path = os.path.join(root, movie_name)
            if os.path.isfile(path) or os.path.isdir(path):
                return root

    print('no movies found in next paths :')
    for optional_dir in optional_dirs:
        print(os.path.join(optional_dir, movie_name))

def load_recording_cache(cache_file, root_dir):
    cache_dir = locate_movie(cache_file, root_dir)
    if cache_dir is None:
        cache_dir = find_and_download(cache_file, 'regression_data', root_dir)
    cache_full_path = os.path.join(cache_dir, cache_file)
    # Parse cache file
    # ================
    logger = BaseLogger(True)
    el_list = logger.from_data(cache_full_path)
    # Remove non-classified items
    el_list = [el['element'].data for el in el_list if el['type'] in ('event_aggregator_out')]
    
    return el_list, cache_dir


DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='product classifier benchmark')
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--movies-file', help='path to csv file containing files to run',type=str, required=True)
    parser.add_argument('--model', help='product classifier model to test',type=str, default=config.LATEST_YAEEN_TRT_MODEL_NAME)
    parser.add_argument('--params', help='path to yaml overwriting default params',type=str, default=None)
    parser.add_argument('--neg-per-event', help='number of negative gt to test per event',type=int, default=1)
    parser.add_argument('--output-dir', help='path to write visualizations',type=str, default=None)
    parser.add_argument('--skip-download', help='assume all files are found locally', action='store_true')
    parser.add_argument('--vis-all', help='visualize all sequences', action='store_true')
    parser.add_argument('--vis-failure', help='visualize failure sequences', action='store_true')
    parser.add_argument('--all-event-save-path', help='path to save all event results',type=str, default=None)
    parser.add_argument('--calib-model', help='calibration model for the capsule',type=str, default='gen2_basler_sys0.11_sh_1')

    args = parser.parse_args()
    random.seed()

    # Overwrite default params
    # ========================
    if args.params is not None:
        config.override_default_config(args.params)
    
    # Build partial system
    # ====================
    md_module = MotionDetectLight()
    # TODO - Add calibration model info to capsule
    cart_calib = CartCalib(config_id=args.calib_model, sync=False) 
    event_classifier = EventClassifier(md_module, en_trt=True, model_name=args.model, use_crop_descriptor=True, sync=config.SYNC_WITH_AWS)
    assert hasattr(event_classifier.yaaen, 'product_validator')
    md_module.event_classifier = event_classifier
    event_classifier.margin_event_detector.set_cart_calib(cart_calib)
    prod_catalog = catalog.Catalog(def_language=config.GUI_LANGUAGE)

    # Load list of recordings to evaluate
    rec_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()

    all_events = []
    os.makedirs(args.output_dir, exist_ok=True)
    result_file_path = os.path.join(args.output_dir, 'all_event_data.csv')
    if not os.path.exists(result_file_path):
        for rec_file in tqdm(rec_list):
            rec_events, rec_dir = load_recording_cache(rec_file, args.root_dir)
            md_module.reset()
            event_classifier.reset()
            # find gt file
            movie_file = rec_file.replace('.pkl', '_L.avi').replace('shop_mode_log_', '')
            movie_name = movie_file.split('_L.avi')[0]
            if not args.skip_download:
                if movie_download(movie_file, rec_dir, include=('_L.csv', 'script.json')) is None:
                    print('GT file is not found, skipping to next file')
            try:
                manual_gt_file = os.path.join(rec_dir, 'gt', rec_file.replace('.pkl', '.csv'))
                if os.path.exists(manual_gt_file):
                    gt = mParser.getPurchaseList(movie_file, rec_dir, force_file=manual_gt_file)
                else:
                    gt = mParser.getPurchaseList(movie_file, rec_dir)
            except:
                print('Failed to load GT for %s. Skipping' % movie_file)
                continue
            # Classify events sequentially
            # ============================
            last_event_frame = None
            # Match event to gt event (frame & direction has to match)
            matched_gt = match_event_to_gt_pre_cls(gt, rec_events)
            for event, event_gt in zip(rec_events, matched_gt):
                if event_gt is None:
                    continue
                # Run classification
                event_result = event_classifier.classify_sequence(event)
                if event_result is None:
                    continue

                # Check validation score w.r.t gt product
                validation_score, metadata = event_classifier.yaaen.product_validator.validate_event(event_result, event_gt)
                if validation_score is None:
                    continue

                all_events.append(dict(event_type='valid',
                                    rec_file=rec_file,
                                    event_id=event['event_id'],
                                    event_size=len(event_result['movement_vector_for_saving']),
                                    gt_product=event_gt,
                                    scanned_product=event_gt,
                                    validation_score=validation_score))
                if args.vis_all:
                    viz_dir = os.path.join(args.output_dir, 'event_viz')
                    plot_event_validation_viz(event_result, all_events[-1], metadata, output_dir=viz_dir)
                
                # Check validation score w.r.t N other products
                all_product_names = event_classifier.yaaen.product_validator.get_product_names()
                neg_products = random.choices(all_product_names, k=args.neg_per_event + 1) # protect choosing same product
                neg_products = [n for n in neg_products if n!=event_gt][:args.neg_per_event]
                for false_event_gt in neg_products:
                    validation_score, metadata = event_classifier.yaaen.product_validator.validate_event(event_result, false_event_gt)
                    all_events.append(dict(event_type='invalid',
                                        rec_file=rec_file,
                                        event_id=event['event_id'],
                                        event_size=len(event_result['movement_vector_for_saving']),
                                        gt_product=event_gt,
                                        scanned_product=false_event_gt,
                                        validation_score=validation_score))
                    if args.vis_all:
                        viz_dir = os.path.join(args.output_dir, 'event_viz')
                        plot_event_validation_viz(event_result, all_events[-1], metadata, output_dir=viz_dir)
        all_events = pd.DataFrame(all_events)
        all_events.to_csv(result_file_path)
    else:
        print('Loading result data from %s' % result_file_path)
        all_events = pd.read_csv(result_file_path)

    all_events['gt_product'] = all_events['gt_product'].astype(str)
    all_events['scanned_product'] = all_events['scanned_product'].astype(str)


    # Analyze events
    # ==============
    # Precision-Recall curve
    x =  all_events['validation_score']
    y =  (all_events['event_type']=='valid').astype(np.float)
    chosen_th = plot_precision_recall_graph(x, y)

    # Analyze failures
    if args.vis_failure:
        # False Negatives
        viz_dir = os.path.join(args.output_dir, 'event_viz_failure', 'FalseNegatives')
        false_neg = all_events[all_events['event_type']=='valid'][all_events['validation_score'] < chosen_th]
        curr_rec = None
        for _, false_neg_event in tqdm(false_neg.iterrows()):
            if curr_rec is None or curr_rec!=false_neg_event.rec_file:
                rec_events, rec_dir = load_recording_cache(false_neg_event.rec_file, args.root_dir)
                curr_rec = false_neg_event.rec_file
            # Run Event again
            event = [ev for ev in rec_events if ev['event_id']==false_neg_event.event_id][0]
            event_result = event_classifier.classify_sequence(event)
            validation_score, metadata = event_classifier.yaaen.product_validator.validate_event(event_result, false_neg_event.gt_product)
            assert np.isclose(false_neg_event['validation_score'], validation_score)
            plot_event_validation_viz(event_result, false_neg_event, metadata, output_dir=viz_dir)
        # False Positives
        viz_dir = os.path.join(args.output_dir, 'event_viz_failure', 'FalsePositives')
        false_pos = all_events[all_events['event_type']=='invalid'][all_events['validation_score'] > chosen_th]
        curr_rec = None
        for _, false_pos_event in tqdm(false_pos.iterrows()):
            if curr_rec is None or curr_rec!=false_pos_event.rec_file:
                rec_events, rec_dir = load_recording_cache(false_pos_event.rec_file, args.root_dir)
                curr_rec = false_pos_event.rec_file
            # Run Event again
            event = [ev for ev in rec_events if ev['event_id']==false_pos_event.event_id][0]
            event_result = event_classifier.classify_sequence(event)
            validation_score, metadata = event_classifier.yaaen.product_validator.validate_event(event_result, false_pos_event.scanned_product)
            assert np.isclose(false_pos_event['validation_score'], validation_score)
            plot_event_validation_viz(event_result, false_pos_event, metadata, output_dir=viz_dir)