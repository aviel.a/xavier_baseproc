import argparse
import getpass
import os
import pandas as pd
from config import config
from cv_blocks.benchmarks.product_classifier.utils import MotionDetectLight, compare_to_gt, \
    match_results_to_events, EvenLogger, collect_event_stats
from cv_blocks.events.event_classifier import EventClassifier
from cv_blocks.events.event_logic import EventLogic
from cv_blocks.misc.logger import BaseLogger
import movie_parser_time_and_dir as mParser
from cv_blocks.misc.aws_download import movie_download, find_and_download
from cv_blocks.metrics.shopping_metrics import ItemStatus
from tqdm import tqdm
from WalkoutExecutor import result_line, kpi_line
from external_libs.tabulate import tabulate

FAILURE_STATES = (ItemStatus.EventDetected, ItemStatus.ProductDetectedInTopK, ItemStatus.EventMadeUp)
FAILURE_STRINGS = [s.name for s in FAILURE_STATES]

def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            path = os.path.join(root, movie_name)
            if os.path.isfile(path) or os.path.isdir(path):
                return root

    print('no movies found in next paths :')
    for optional_dir in optional_dirs:
        print(os.path.join(optional_dir, movie_name))

def load_recording_cache(cache_file, root_dir):
    cache_dir = locate_movie(cache_file, root_dir)
    if cache_dir is None:
        cache_dir = find_and_download(cache_file, 'regression_data', root_dir)
    cache_full_path = os.path.join(cache_dir, cache_file)
    # Parse cache file
    # ================
    logger = BaseLogger(True)
    el_list = logger.from_data(cache_full_path)
    # Remove non-classified items
    el_list = [el['element'].data for el in el_list if el['type'] in ('event_aggregator_out')]
    
    return el_list, cache_dir


DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='product classifier benchmark')
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--movies-file', help='path to csv file containing files to run',type=str, required=True)
    parser.add_argument('--model', help='product classifier model to test',type=str, default=config.LATEST_YAEEN_TRT_MODEL_NAME)
    parser.add_argument('--params', help='path to yaml overwriting default params',type=str, default=None)
    parser.add_argument('--vis-dir', help='path to write visualizations',type=str, default=None)
    parser.add_argument('--skip-download', help='assume all files are found locally', action='store_true')
    parser.add_argument('--stat-path', help='path to statistics',type=str, default='/tmp')
    parser.add_argument('--vis-all', help='visualize all sequences', action='store_true')
    args = parser.parse_args()

    # Overwrite default params
    # ========================
    if args.params is not None:
        config.override_default_config(args.params)

    # Build partial system
    # ====================
    md_module = MotionDetectLight()
    event_classifier = EventClassifier(md_module, en_trt=True, model_name=args.model)
    event_logic = EventLogic(md_module, event_classifier.yaaen.getProductFromIndex, \
        event_classifier.yaaen.getIdxFromProduct, ['front', 'back'], \
            el_config=dict(send_low_conf_as_unknown=config.LOW_CONF_AS_UNKNOWN,
                           store_sent_events=True, verbose=0))
    
    # Classifier samples feature extractor
    # ====================================
    from cv_blocks.benchmarks.product_classifier.extract_sample_feaures import ClassifierSampleFeatureExtractor
    sfe = ClassifierSampleFeatureExtractor(event_classifier.yaaen.getProductFromIndex)

    # Load list of recordings to evaluate
    rec_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()

    all_events = []
    tot_res = dict(clsSuccess=0, clsTested=0, MDSuccess=0, MDTested=0, unknownItems=0, n_missed=0, n_madeup=0, n_false_low_conf=0)
    resultsTable = [['Movie Name', 'Cls Success', 'MD Success']]
    event_stats = []
    for rec_file in tqdm(rec_list):
        rec_events, rec_dir = load_recording_cache(rec_file, args.root_dir)
        md_module.reset()
        event_logic.reset()
        # find gt file
        movie_file = rec_file.replace('.pkl', '_L.avi').replace('shop_mode_log_', '')
        movie_name = movie_file.split('_L.avi')[0]
        if not args.skip_download:
            if movie_download(movie_file, rec_dir, include=('_L.csv', 'script.json')) is None:
                print('GT file is not found, skipping to next file')
        try:
            gt = mParser.getPurchaseList(movie_file, rec_dir)
        except:
            print('Failed to load GT for %s. skipping' % movie_file)
            continue
        # Classify events sequentially
        # ============================
        last_event_frame = None
        for event in rec_events:
            if last_event_frame is not None:
                # Emulate event logic operation between events
                for ignore_frame in range(last_event_frame, event['event_frame']):
                    event_logic.step(ignore_frame, None, False, None)
            # Handle new event
            event_result = event_classifier.classify_sequence(event)
            event_logic.step(event_result['event_frame'], event_result, False, None)
            last_event_frame = event_result['event_frame']

        # Compare to GT
        result_dict, analyzed_events = compare_to_gt(md_module, gt, movie_file)
        # Aggregate results
        all_events.append(analyzed_events)
        for k in tot_res.keys():
            tot_res[k] += result_dict[k]
        resultsTable.append(result_line(movie_name, result_dict))

        # Match results with stored events
        matched_results = match_results_to_events(analyzed_events, event_logic.sent_events, rec_events)

        # Extract single prediction data
        for mr in matched_results:
            sfe.extract_from_sequence(mr)

        
    # Save event statistics
    sfe.save_results(os.path.join(args.stat_path, 'all_cls_stats.csv'))

    single_cls_acc = sfe.accuracy()
    print('Finished running Single classification benchmark. Accuracy = %.2f' % single_cls_acc)