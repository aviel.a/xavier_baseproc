import argparse
import getpass
import os
import pandas as pd
from config import config
from cv_blocks.benchmarks.product_classifier.utils import MotionDetectLight, compare_to_gt, \
    match_results_to_events, EvenLogger, collect_event_stats
from cv_blocks.events.event_classifier import EventClassifier
from cv_blocks.events.event_logic import EventLogic
from cv_blocks.misc.logger import BaseLogger
import movie_parser_time_and_dir as mParser
from cv_blocks.misc.aws_download import movie_download as movie_download_aws, find_and_download
from cv_blocks.misc.az_download import movie_download as movie_download_az
from cv_blocks.metrics.shopping_metrics import ItemStatus, get_only_classifier_supported_events, \
    summarize_recording_metrics, accuracy_per_sku
from tqdm import tqdm
from WalkoutExecutor import result_line, kpi_line
from external_libs.tabulate import tabulate
from cv_blocks.calibration.cart_calib import CartCalib
from cart_blocks.cart_content_handler import CartContentHandler
from cart_blocks import catalog
from matplotlib import pyplot as plt

FAILURE_STATES_VISION = (ItemStatus.EventDetected, ItemStatus.ProductDetectedInTopK, ItemStatus.EventMadeUp,
                  ItemStatus.EventMissed, ItemStatus.EventDetectedLowConf, ItemStatus.EventFalseLowConf)
FAILURE_STATES_MSCO = (ItemStatus.EventFalseLowConf, ItemStatus.EventMissed,ItemStatus.EventMadeUp)
FAILURE_STRINGS_VISION = [s.name for s in FAILURE_STATES_VISION]
FAILURE_STRINGS_MSCO = [s.name for s in FAILURE_STATES_MSCO]

def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            path = os.path.join(root, movie_name)
            if os.path.isfile(path) or os.path.isdir(path):
                return root

    print('no movies found in next paths :')
    for optional_dir in optional_dirs:
        print(os.path.join(optional_dir, movie_name))

def load_recording_cache(cache_file, root_dir):
    assert cache_file.endswith('.pkl'), "file name: {} does not end in .pkl!".format(cache_file)
    cache_dir = locate_movie(cache_file, root_dir)
    if cache_dir is None:
        cache_dir = find_and_download(cache_file, 'regression_data', root_dir)
    cache_full_path = os.path.join(cache_dir, cache_file)
    # Parse cache file
    # ================
    logger = BaseLogger(True)
    el_list = logger.from_data(cache_full_path)
    # Remove non-classified items
    el_list = [el['element'].data for el in el_list if el['type'] in ('event_aggregator_out')]
    
    return el_list, cache_dir


DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='product classifier benchmark')
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--movies-file', help='path to csv file containing files to run',type=str, required=True)
    parser.add_argument('--model', help='product classifier model to test',type=str, default=config.LATEST_YAEEN_TRT_MODEL_NAME)
    parser.add_argument('--params', help='path to yaml overwriting default params',type=str, default=None)
    parser.add_argument('--vis-dir', help='path to write visualizations',type=str, default=None)
    parser.add_argument('--skip-download', help='assume all files are found locally', action='store_true')
    parser.add_argument('--stat-path', help='path to statistics',type=str, default='/tmp')
    parser.add_argument('--skip-gt', help='run without GT', action='store_true')
    parser.add_argument('--vis-all', help='visualize all sequences', action='store_true')
    parser.add_argument('--failure-save-path', help='path to save failure data',type=str, default=None)
    parser.add_argument('--all-event-save-path', help='path to save all event results',type=str, default=None)
    parser.add_argument('--use-original-pred', help='dont change predictions with heuristics', action='store_true')
    parser.add_argument('--calib-model', help='calibration model for the capsule',type=str, default='gen2_basler_sys0.11_sh_1')
    parser.add_argument('--unsupported', help='enable usupported items feature', action='store_true')
    parser.add_argument('--barcode', help='enable barcode trigger', action='store_true')
    parser.add_argument('--verbose', help='verbosity level',type=int, default=0)
    parser.add_argument('--output-pred', help='output predictions to csv file', action='store_true')
    parser.add_argument('--msco-mode', help='approximate msco mode guardian', action='store_true')
    parser.add_argument('--plot', help='plot a graph of #err vs #trained images', action='store_true')
    parser.add_argument('--azure', help='download gt from azure', action='store_true')

    args = parser.parse_args()

    # Overwrite default params
    # ========================
    if args.params is not None:
        config.override_default_config(args.params)
    
    FAILURE_STATES = FAILURE_STATES_MSCO if args.msco_mode else FAILURE_STATES_VISION
    FAILURE_STRINGS = FAILURE_STRINGS_MSCO if args.msco_mode else FAILURE_STRINGS_VISION

    # Build partial system
    # ====================
    md_module = MotionDetectLight()
    # TODO - Add calibration model info to capsule
    cart_calib = CartCalib(config_id=args.calib_model, sync=False) 
    event_classifier = EventClassifier(md_module, en_trt=True, model_name=args.model)
    md_module.event_classifier = event_classifier
    el_config=dict(send_low_conf_as_unknown=config.LOW_CONF_AS_UNKNOWN,
                    store_sent_events=True, verbose=args.verbose,check_unsupported_items=args.unsupported,
                    force_similar_prod_top_k=not args.use_original_pred)
    if args.msco_mode:
        config.MSCO_MODE = True
        el_config['all_events_to_interaction'] = True
        el_config['convert_hand_mv_with_cb_to_filtered'] = True
        el_config['hand_multi_factor'] = 4
        el_config['ignore_marginal_out_n_frames_after_in'] = 40
        el_config['ignore_cb_only_events'] = True
    event_logic = EventLogic(md_module, event_classifier.yaaen.getProductFromIndex, \
        event_classifier.yaaen.getIdxFromProduct, ['front', 'back'], \
            el_config=el_config)
    event_classifier.margin_event_detector.set_cart_calib(cart_calib)
    prod_catalog = catalog.Catalog(def_language=config.GUI_LANGUAGE)
    md_module.cart_content_handler = CartContentHandler(product_catalog=prod_catalog, gui=None, md_module=md_module)
    md_module.event_logic = event_logic
    if args.barcode:
        config.EN_BARCODE_SCANNER = True
    if config.EN_BARCODE_SCANNER:
        from md_blocks.barcode.barcode_handler import BarcodeHandler
        bh_config = dict()
        md_module.barcode_handler = BarcodeHandler(md_module, bh_config=bh_config, use_cam_scanner=False)

    # Load list of recordings to evaluate
    rec_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()

    all_events = []
    tot_res = dict(clsSuccess=0, clsTested=0, MDSuccess=0, MDTested=0, unknownItems=0, n_missed=0, n_madeup=0, n_false_low_conf=0)
    resultsTable = [['Movie Name', 'Cls Success', 'MD Success']]
    results_per_sku = dict()
    event_stats = []
    if args.failure_save_path is not None:
        failure_vec = []
    for rec_file in tqdm(rec_list):
        rec_events, rec_dir = load_recording_cache(rec_file, args.root_dir)
        md_module.reset()
        event_logic.reset()
        event_classifier.reset()
        md_module.cart_content_handler.reset()
        # find gt file
        movie_file = rec_file.replace('.pkl', '_L.avi').replace('shop_mode_log_', '')
        movie_name = movie_file.split('_L.avi')[0]
        if not args.skip_gt:
            if not args.skip_download:
                if args.azure:
                    if movie_download_az(movie_file, rec_dir, include=('_L.csv', 'script.json')) is None:
                        print('GT file is not found, skipping to next file')
                else:
                    if movie_download_aws(movie_file, rec_dir, include=('_L.csv', 'script.json')) is None:
                        print('GT file is not found, skipping to next file')
            try:
                manual_gt_file = os.path.join(rec_dir, 'gt', rec_file.replace('.pkl', '.csv'))
                if os.path.exists(manual_gt_file):
                    gt = mParser.getPurchaseList(movie_file, rec_dir, force_file=manual_gt_file)
                else:
                    gt = mParser.getPurchaseList(movie_file, rec_dir)
            except:
                print('Failed to load GT for %s. skipping' % movie_file)
                continue
        # Classify events sequentially
        # ============================
        last_event_frame = None
        for event in rec_events:
            print("Sending event at frame {} for classification (size: {}, direction: {}, back = {}, front = {})".format(
                event['event_frame'], len(event['movement_vector']), event['event_direction'], event['back_id'] is not None,
                event['front_id'] is not None))
            if last_event_frame is not None:
                # Emulate event logic operation between events
                for ignore_frame in range(last_event_frame, event['event_frame']):
                    event_logic.step(ignore_frame, None)
            # Handle new event
            event_result = event_classifier.classify_sequence(event)
            if event_result is not None:
                event_logic.step(event_result['event_frame'], event_result)
                last_event_frame = event_result['event_frame']
            if event is rec_events[-1]:
                # in case last event is triggered by CB only, emulate event logic operation for another 50 frames to handle event
                for ignore_frame in range(event['event_frame'], event['event_frame'] + 50):
                    event_logic.step(ignore_frame, None)
        
        if args.output_pred:
            prediction_dir = os.path.join(rec_dir, 'predictions_%s' % args.model)
            os.makedirs(prediction_dir, exist_ok=True)
            predicted_csv = os.path.join(prediction_dir, rec_file.replace('.pkl', '.csv'))
            from WalkoutExecutor import savePurchaseListToCSV
            savePurchaseListToCSV(predicted_csv, md_module.purchaseList)
        if not args.skip_gt:
            # Compare to GT
            mParser.printPurchaseList('walkout purchase list', md_module.purchaseList)
            mParser.printPurchaseList('ground Truth purchase', gt)
            confident_location_predictions = md_module.cart_content_handler.location_matching.confident_predictions
            result_dict, analyzed_events = compare_to_gt(md_module, gt, movie_file, confident_location_predictions,
                                                         classifier=event_classifier, msco=args.msco_mode)
            # Aggregate results
            all_events.append(analyzed_events)
            for k in tot_res.keys():
                tot_res[k] += result_dict[k]
            results_per_sku = accuracy_per_sku(results_per_sku, analyzed_events)
            resultsTable.append(result_line(movie_name, result_dict))

            # Match results with stored events
            matched_results = match_results_to_events(analyzed_events, event_logic.sent_events, rec_events)

            # Log Failures
            if args.vis_dir is not None:
                movie_log_dir = os.path.join(args.vis_dir, movie_name) 
                os.makedirs(movie_log_dir, exist_ok=True)
                for mr in matched_results:
                    if mr['result']['state'] in FAILURE_STATES or args.vis_all:
                        mvl = EvenLogger(data=mr)
                        mvl.visualize(path=movie_log_dir, prod_from_idx=event_classifier.yaaen.getProductFromIndex)
            
            # Save Failures
            if args.failure_save_path is not None:
                for mr in matched_results:
                    if mr['result']['state'] in FAILURE_STATES:
                        failure_vec.append(mr)


            # Gather event statistics
            for mr in matched_results:
                event_stats.append(collect_event_stats(mr, movie_name))
        else:
            print("{} {} {} ".format("product".ljust(20), "direction".ljust(20), "frame".ljust(20)))
            for i in range(len(md_module.purchaseList)):
                print("{} {} {}".format(str(md_module.purchaseList[i]["name"]).ljust(20),
                                        str(md_module.purchaseList[i]["direction"]).ljust(20),
                                        str(md_module.purchaseList[i]["frameNum"]).ljust(20)))
            matched_results = match_results_to_events(None, event_logic.sent_events + event_logic.low_conf_events, rec_events)
            if args.vis_dir is not None:
                movie_log_dir = os.path.join(args.vis_dir, movie_name) 
                os.makedirs(movie_log_dir, exist_ok=True)
                for mr in matched_results:
                    mvl = EvenLogger(data=mr)
                    mvl.visualize(path=movie_log_dir, prod_from_idx=event_classifier.yaaen.getProductFromIndex)
        
    # Save event statistics
    if not args.skip_gt:
        # save results per sku analysis
        pd.DataFrame.from_dict(results_per_sku, orient='index').to_csv(os.path.join(args.stat_path, 'results_per_sku.csv'))

        event_stats = pd.DataFrame(event_stats)
        event_stats.to_csv(os.path.join(args.stat_path, 'all_event_stats.csv'))
        fail_mask = event_stats['result'].isin(FAILURE_STRINGS)
        event_stats.loc[fail_mask].to_csv(os.path.join(args.stat_path, 'fail_event_stats.csv'))

        # Log output results
        resultsTable.append(['---------'])
        resultsTable.append(result_line('Total', tot_res, full=True, bold=True))
        print('Results by Movie:')
        print(tabulate(resultsTable, headers="firstrow", tablefmt="pretty")) #printing a summary table

        from cart_blocks import catalog as Catalog
        catalog = Catalog.Catalog(def_language=config.GUI_LANGUAGE)
        # Print KPI results
        # =================
        print('KPI report:')
        KpiTable = [['Total Events', 'Errors', 'Interactions', 'Err:Wrong Product', 'Err:Missed Event', 'Err:Made-up Event']]
        KpiTable.append(kpi_line(tot_res, bold=True))
        print(tabulate(KpiTable, headers="firstrow", tablefmt="pretty"))

        # Report KPI only for supported products if needed
        # ================================================
        all_supported_events, all_supported = get_only_classifier_supported_events(all_events, event_classifier)
        if not all_supported:
            tot_res_supported = []
            tot_res_supported = dict(clsSuccess=0, clsTested=0, MDSuccess=0, MDTested=0, unknownItems=0, n_missed=0, n_madeup=0, n_false_low_conf=0)
            for ev in all_supported_events:
                result_dict = summarize_recording_metrics(ev, catalog, log_errors=False)
                for k in tot_res_supported.keys():
                    tot_res_supported[k] += result_dict[k]
            print('KPI report, Classifier Supported Products only:')
            KpiTable = [['Total Events', 'Errors', 'Interactions', 'Err:Wrong Product', 'Err:Missed Event', 'Err:Made-up Event']]
            KpiTable.append(kpi_line(tot_res_supported, bold=True))
            print(tabulate(KpiTable, headers="firstrow", tablefmt="pretty"))

        # Save Failures
        if args.failure_save_path is not None:
            import pickle
            pickle.dump(failure_vec, open(args.failure_save_path, 'wb'))

        # Save Event results
        if args.all_event_save_path is not None:
            import pickle
            pickle.dump(all_events, open(args.all_event_save_path, 'wb'))

        if args.plot:
            model_name_wo_suffix = args.model[:-5]
            images_per_sku_path = os.path.join(config.SOURCEPATH, 'ourFirstCNN', 'tensorrt_lib', 'models', 'classifier',
                                               os.path.splitext(args.model)[0], "images_per_SKU.csv")
            if os.path.exists(images_per_sku_path):
                images_per_sku = pd.read_csv(images_per_sku_path, dtype='object')
                errors = dict()
                made_ups = dict()
                misses = dict()
                cls_errors = dict()
                false_low_conf = 0
                dct = dict()
                total_per_product = dict()
                for movie_events in all_events:
                    for event in movie_events:
                        state = event['state']
                        if state==ItemStatus.Ignored:
                            continue
                        product = event['pred_name'] if state==ItemStatus.EventMadeUp else event['name']
                        total_per_product[product] = 1 if product not in total_per_product.keys() else total_per_product[product] + 1
                        if event['state'] in [ItemStatus.EventDetectedLowConf, ItemStatus.ProductDetectedInTopK]:
                            dct[product] = 1 if product not in dct.keys() else dct[product] + 1
                        elif event['state']==ItemStatus.EventMissed:
                            misses[product] = 1 if product not in misses.keys() else misses[product] + 1
                            errors[product] = 1 if product not in errors.keys() else errors[product] + 1
                        elif event['state']==ItemStatus.EventMadeUp:
                            made_ups[product] = 1 if product not in made_ups.keys() else made_ups[product] + 1
                            errors[product] = 1 if product not in errors.keys() else errors[product] + 1
                        elif event['state']==ItemStatus.EventDetected:
                            cls_errors[product] = 1 if product not in cls_errors.keys() else cls_errors[product] + 1
                            errors[product] = 1 if product not in errors.keys() else errors[product] + 1
                        elif event['state'] == ItemStatus.EventFalseLowConf:
                            false_low_conf += 1
                save_path = args.vis_dir if args.vis_dir is not None else "/tmp/"
                total_events = sum(total_per_product.values())
                average_images = int(images_per_sku.loc[images_per_sku.ix[:, 0]!='aa_hand'].loc[images_per_sku.ix[:, 0]!='other']['train'].astype(int).mean())
                # DCT graph
                x, y, label = [], [], []
                for product, count in dct.items():
                    images = int(images_per_sku.loc[images_per_sku.ix[:, 0] == product]['train']) if \
                        product in images_per_sku.ix[:, 0].to_list() else 0
                    x.append(images)
                    y.append(count / total_per_product[product])
                    label.append(product)
                plt.figure(figsize=(16, 10))
                plt.plot(x, y, 'ro', label='dct per sku')
                avg_y = 0 if len(dct.values()) == 0 else sum(dct.values()) / float(total_events)
                plt.plot(average_images, avg_y, 'bo', label='avg dct')
                plt.legend()
                plt.title("Interactions per Product", fontsize=25)
                plt.xlim([min(min(x), average_images) * 0.95, max(max(x), average_images) * 1.05])
                plt.xlabel('Training Images', fontsize=18)
                plt.ylim([0, 1.05])
                plt.ylabel('Normalized DCT Rate', fontsize=18)
                plt.text(min(min(x), average_images) * 0.99, 0.9, "TP DCT : {} ({}%)\nFP DCT : {} ({}%)".format(
                    sum(dct.values()), round(sum(dct.values()) / float(total_events) * 100, 2), false_low_conf, round(false_low_conf / float(total_events) * 100, 2)), fontsize=14)
                ax = plt.gca()
                for i, txt in enumerate(label):
                    ax.annotate(txt, (x[i], y[i]+0.01), ha='center')
                plt.savefig(os.path.join(save_path, "dct_per_product.jpg"))
                plt.close()

                # errors graph
                x, y, label = [], [], []
                for product, count in errors.items():
                    images = int(images_per_sku.loc[images_per_sku.ix[:, 0] == product]['train']) if \
                        product in images_per_sku.ix[:, 0].to_list() else 0
                    x.append(images)
                    y.append(count / total_per_product[product])
                    label.append(product)
                plt.figure(figsize=(16, 10))
                plt.plot(x, y, 'ro', label='errors per sku')
                avg_y = 0 if len(errors.values()) == 0 else sum(errors.values()) / float(sum(total_per_product.values()))
                plt.plot(average_images, avg_y, 'bo', label='avg err')
                plt.legend()
                plt.title("Errors per Product", fontsize=25)
                plt.xlim([min(min(x), average_images) * 0.95, max(max(x), average_images) * 1.05])
                plt.xlabel('Training Images', fontsize=18)
                plt.ylim([0, 1.05])
                plt.ylabel('Normalized Error Rate', fontsize=18)
                plt.text(min(min(x), average_images) * 0.99, 0.9, "Total errors : {} ({}%)\nMisses : {} ({}%)\nMade ups : {} ({}%)\nCls errors : {} ({}%)".format(
                    sum(errors.values()), round(sum(errors.values()) / float(total_events) * 100, 2),
                    sum(misses.values()), round(sum(misses.values()) / float(total_events) * 100, 2),
                    sum(made_ups.values()), round(sum(made_ups.values()) / float(total_events) * 100, 2),
                    sum(cls_errors.values()), round(sum(cls_errors.values()) / float(total_events) * 100, 2)), fontsize=14)
                ax = plt.gca()
                for i, txt in enumerate(label):
                    ax.annotate(txt, (x[i], y[i]+0.01), ha='center')
                plt.savefig(os.path.join(save_path, "err_per_product.jpg"))
                plt.close()
                print("cls errors: {}".format(cls_errors))
                print("misses: {}".format(misses))
                print("made ups: {}".format(made_ups))
                print("dcts: {}".format(dct))
