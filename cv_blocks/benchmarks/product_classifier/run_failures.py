import argparse
import os
import pandas as pd
from config import config
from cv_blocks.benchmarks.product_classifier.utils import MotionDetectLight, EvenLogger, collect_event_stats
from cv_blocks.events.event_classifier import EventClassifier
from cv_blocks.events.event_logic import EventLogic
from cv_blocks.metrics.shopping_metrics import shopping_accuracy
from cv_blocks.sequence_aligner.image_aligner import ManualImageAligner
import pickle
import copy


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='product classifier benchmark')
    parser.add_argument('--model', help='product classifier model to test',type=str, default=config.LATEST_YAEEN_TRT_MODEL_NAME)
    parser.add_argument('--params', help='path to yaml overwriting default params',type=str, default=None)
    parser.add_argument('--vis-dir', help='path to write visualizations',type=str, default=None)
    parser.add_argument('--failure-save-path', help='path to save failure data',type=str, required=True)
    args = parser.parse_args()

    # Overwrite default params
    # ========================
    if args.params is not None:
        config.override_default_config(args.params)

    # Initialize image aligner
    # ========================
    aligner = ManualImageAligner
    # aligner = None

    # Build partial system
    # ====================
    md_module = MotionDetectLight()
    event_classifier = EventClassifier(md_module, en_trt=True, model_name=args.model, aligner=aligner)
    event_logic = EventLogic(md_module, event_classifier.yaaen.getProductFromIndex, \
        event_classifier.yaaen.getIdxFromProduct, ['front', 'back'], \
            el_config=dict(send_low_conf_as_unknown=config.LOW_CONF_AS_UNKNOWN,
                           store_sent_events=True, verbose=0))

    event_stats = []
    # Load Events
    # ===========
    events = pickle.load(open(args.failure_save_path, 'rb'))

    for event in events:
        # Handle new event
        event['event']['movement_vector'] = event['event']['movement_vector_for_saving']
        event_result = event_classifier.classify_sequence(event['event'])
        event_logic.step(event_result['event_frame'], event_result, False, None)
        # modify prediction
        event['prediction'] = copy.deepcopy(event_logic.sent_events[-1]['prediction'])
        event['event'] = copy.deepcopy(event_logic.sent_events[-1]['event'])

        # Modify result
        gt = [dict(frameNum=event['result']['frameNum'], direction=event['result']['direction'], name=event['result']['name'])]
        walkout_prediction = md_module.purchaseList
        analyzed_event = shopping_accuracy(walkout_prediction, gt)[0]
        event['result'] = analyzed_event
        md_module.purchaseList = []
        
        # Visualize prediction
        mvl = EvenLogger(data=event)
        mvl.visualize(path=args.vis_dir, prod_from_idx=event_classifier.yaaen.getProductFromIndex)

        # Gather event statistics
        event_stats.append(collect_event_stats(event))

    # Log mv statistics
    event_stats = pd.DataFrame(event_stats)
    event_stats.to_csv(os.path.join(args.vis_dir, 'all_event_stats.csv'))