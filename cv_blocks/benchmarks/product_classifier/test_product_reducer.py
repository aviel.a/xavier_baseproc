import argparse
import getpass
import os
import numpy as np
import pandas as pd
from config import config
import random
from cv_blocks.benchmarks.product_classifier.utils import MotionDetectLight, \
    match_event_to_gt_pre_cls, plot_precision_recall_graph, plot_product_reduction_viz, plot_accuracy_graph
from cv_blocks.events.event_classifier import EventClassifier
from cv_blocks.product_reducer.cluster_reducer import ClusterReducer
from cv_blocks.misc.logger import BaseLogger
import movie_parser_time_and_dir as mParser
from cv_blocks.misc.aws_download import movie_download, find_and_download
from tqdm import tqdm
from cv_blocks.calibration.cart_calib import CartCalib
from cart_blocks import catalog
from matplotlib import pyplot as plt

def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            path = os.path.join(root, movie_name)
            if os.path.isfile(path) or os.path.isdir(path):
                return root

    print('no movies found in next paths :')
    for optional_dir in optional_dirs:
        print(os.path.join(optional_dir, movie_name))

def load_recording_cache(cache_file, root_dir):
    cache_dir = locate_movie(cache_file, root_dir)
    if cache_dir is None:
        cache_dir = find_and_download(cache_file, 'regression_data', root_dir)
    cache_full_path = os.path.join(cache_dir, cache_file)
    # Parse cache file
    # ================
    logger = BaseLogger(True)
    el_list = logger.from_data(cache_full_path)
    # Remove non-classified items
    el_list = [el['element'].data for el in el_list if el['type'] in ('event_aggregator_out')]
    
    return el_list, cache_dir


DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='product classifier benchmark')
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--movies-file', help='path to csv file containing files to run',type=str, required=True)
    parser.add_argument('--model', help='product classifier model to test',type=str, default=config.LATEST_YAEEN_TRT_MODEL_NAME)
    parser.add_argument('--params', help='path to yaml overwriting default params',type=str, default=None)
    parser.add_argument('--neg-per-event', help='number of negative gt to test per event',type=int, default=1)
    parser.add_argument('--output-dir', help='path to write visualizations',type=str, required=True)
    parser.add_argument('--skip-download', help='assume all files are found locally', action='store_true')
    parser.add_argument('--vis-all', help='visualize all sequences', action='store_true')
    parser.add_argument('--vis-failure', help='visualize failure sequences', action='store_true')
    parser.add_argument('--all-event-save-path', help='path to save all event results',type=str, default=None)
    parser.add_argument('--calib-model', help='calibration model for the capsule',type=str, default='gen2_basler_sys0.11_sh_1')
    parser.add_argument('--reducer-info-path', help='information about product reducer network', type=str, default=None)

    args = parser.parse_args()
    random.seed()

    # Overwrite default params
    # ========================
    if args.params is not None:
        config.override_default_config(args.params)
    
    # Build partial system
    # ====================
    md_module = MotionDetectLight()
    # TODO - Add calibration model info to capsule
    cart_calib = CartCalib(config_id=args.calib_model, sync=False) 
    event_classifier = EventClassifier(md_module, en_trt=True, model_name=args.model, use_crop_descriptor=True, sync=config.SYNC_WITH_AWS)
    assert args.reducer_info_path is not None
    product_reducer = ClusterReducer(args.reducer_info_path)
    md_module.event_classifier = event_classifier
    event_classifier.margin_event_detector.set_cart_calib(cart_calib)
    prod_catalog = catalog.Catalog(def_language=config.GUI_LANGUAGE)

    # Load list of recordings to evaluate
    rec_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()

    all_events = []
    repr_config_name = os.path.splitext(os.path.basename(args.reducer_info_path))[0]
    args.output_dir = os.path.join(args.output_dir, repr_config_name)
    os.makedirs(args.output_dir, exist_ok=True)
    result_file_path = os.path.join(args.output_dir, 'all_event_data.csv')
    if not os.path.exists(result_file_path):
        for rec_file in tqdm(rec_list):
            rec_events, rec_dir = load_recording_cache(rec_file, args.root_dir)
            md_module.reset()
            event_classifier.reset()
            # find gt file
            movie_file = rec_file.replace('.pkl', '_L.avi').replace('shop_mode_log_', '')
            movie_name = movie_file.split('_L.avi')[0]
            if not args.skip_download:
                if movie_download(movie_file, rec_dir, include=('_L.csv', 'script.json')) is None:
                    print('GT file is not found, skipping to next file')
            try:
                manual_gt_file = os.path.join(rec_dir, 'gt', rec_file.replace('.pkl', '.csv'))
                if os.path.exists(manual_gt_file):
                    gt = mParser.getPurchaseList(movie_file, rec_dir, force_file=manual_gt_file)
                else:
                    gt = mParser.getPurchaseList(movie_file, rec_dir)
            except:
                print('Failed to load GT for %s. Skipping' % movie_file)
                continue
            # Classify events sequentially
            # ============================
            last_event_frame = None
            # Match event to gt event (frame & direction has to match)
            matched_gt = match_event_to_gt_pre_cls(gt, rec_events)
            for event, event_gt in zip(rec_events, matched_gt):
                if event_gt is None:
                    continue
                # Run classification
                event_result = event_classifier.classify_sequence(event)
                if event_result is None:
                    continue

                # Reduce event to list of possible products
                # =========================================
                if not product_reducer.is_valid_gt(event_gt):
                    continue
                chosen_cluster, product_candidate_list, metadata = product_reducer.reduce_event(event_result)
                if product_candidate_list is None:
                    continue
                success = event_gt in product_candidate_list
                top1_acc_per_img = [event_gt in product_reducer.get_cluster_products(c_id) for c_id in metadata['cluster_per_img'][:,0].flatten()]
                top2_acc_per_img = [event_gt in product_reducer.get_cluster_products(c_id) for c_id in metadata['cluster_per_img'][:,1].flatten()]
                top2_acc_per_img = [top1 or top2 for top1, top2 in zip(top1_acc_per_img, top2_acc_per_img)]
                metadata['top1_acc_per_img'] = top1_acc_per_img
                metadata['top2_acc_per_img'] = top2_acc_per_img

                all_events.append(dict(event_type='valid',
                                    rec_file=rec_file,
                                    event_id=event['event_id'],
                                    event_size=len(event_result['movement_vector_for_saving']),
                                    gt_product=event_gt,
                                    success=success,
                                    top1_acc_per_img=np.array(top1_acc_per_img).mean(),
                                    top2_acc_per_img=np.array(top2_acc_per_img).mean(),
                                    chosen_cluster=chosen_cluster,
                                    validation_score=metadata['top1_prob']))
                if args.vis_all:
                    viz_dir = os.path.join(args.output_dir, 'event_viz')
                    plot_product_reduction_viz(event_result, all_events[-1], metadata, output_dir=viz_dir)
                
        all_events = pd.DataFrame(all_events)
        all_events.to_csv(result_file_path)
    else:
        print('Loading result data from %s' % result_file_path)
        all_events = pd.read_csv(result_file_path)

    all_events['gt_product'] = all_events['gt_product'].astype(str)


    # Analyze events
    # ==============
    # Accuracy
    plot_accuracy_graph(all_events, output_dir=args.output_dir)
    # Precision-Recall curve
    x =  all_events['validation_score']
    y =  (all_events['success']).astype(np.float)
    plot_precision_recall_graph(x, y, output_dir=args.output_dir)

    # Analyze failures
    if args.vis_failure:
        viz_dir = os.path.join(args.output_dir, 'event_viz_failure')
        false_neg = all_events[all_events['success']==False]
        curr_rec = None
        for _, false_neg_event in tqdm(false_neg.iterrows()):
            if curr_rec is None or curr_rec!=false_neg_event.rec_file:
                rec_events, rec_dir = load_recording_cache(false_neg_event.rec_file, args.root_dir)
                curr_rec = false_neg_event.rec_file
            # Run Event again
            event = [ev for ev in rec_events if ev['event_id']==false_neg_event.event_id][0]
            event_result = event_classifier.classify_sequence(event)
            chosen_cluster, product_candidate_list, metadata = product_reducer.reduce_event(event_result)
            top1_acc_per_img = [false_neg_event['gt_product'] in product_reducer.get_cluster_products(c_id) for c_id in metadata['cluster_per_img'][:,0].flatten()]
            top2_acc_per_img = [false_neg_event['gt_product'] in product_reducer.get_cluster_products(c_id) for c_id in metadata['cluster_per_img'][:,1].flatten()]
            top2_acc_per_img = [top1 or top2 for top1, top2 in zip(top1_acc_per_img, top2_acc_per_img)]
            metadata['chosen_cluster'] = chosen_cluster
            metadata['top1_acc_per_img'] = top1_acc_per_img
            metadata['top2_acc_per_img'] = top2_acc_per_img
            assert np.isclose(false_neg_event['validation_score'], metadata['top1_prob'])
            plot_product_reduction_viz(event_result, false_neg_event, metadata, output_dir=viz_dir)