import os
import argparse
import re
import numpy as np
import cv2
import json
import getpass
from cv_blocks.video_reader.base_proc import BaseImageGrabber
from multiprocessing import Process
from cv_blocks.calibration.cart_calib import CartCalib
import pandas as pd
import streams.stereo.vx._main_stereo_matching as msm
import Exceptions
from cv_blocks.cart_bottom.multi_cam_diff_detector import MultiCamDiffDetector
from config import config


def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            path = os.path.join(root, movie_name)
            if os.path.isfile(path) or os.path.isdir(path):
                return root


class EventDetectorBenchmark:
    def __init__(self):
        self.first_snapshot = True
        self.event_started = None
        self.events_predicted = []

    def step(self, snapshot_taken, dynamic, frame_num):
        if dynamic and self.event_started is None:
            self.event_started = frame_num
        if snapshot_taken:
            if self.first_snapshot:
                self.first_snapshot = False
            else:
                self.events_predicted.append((self.event_started, frame_num))
            self.event_started = None

    @staticmethod
    def calculate_performance(predicted, gt):
        print("\nEvent detector errors:")
        total_true_events = len(gt)
        total_predicted_events = len(predicted)
        made_ups = 0
        misses = 0
        wrong_timing = 0  # todo - is there a way to evaluate it?
        gt_idx, pred_idx = 0,0
        while True:
            if gt_idx < total_true_events and pred_idx < total_predicted_events:
                frame_num, direction = EventDetectorBenchmark.read_gt(gt, gt_idx)
                if EventDetectorBenchmark.is_event_in_range(predicted[pred_idx], frame_num, direction):
                    # if the next event also in range - the current one is considered a miss
                    if gt_idx + 1 < total_true_events:
                        frame_next, direction_next = EventDetectorBenchmark.read_gt(gt, gt_idx + 1)
                        if EventDetectorBenchmark.is_event_in_range(predicted[pred_idx], frame_next, direction_next):
                            misses += 1
                            print("Event at frame {} direction: {} is missed".format(frame_num, direction))
                        else:
                            pred_idx += 1
                    else:
                        pred_idx += 1
                    gt_idx += 1
                elif EventDetectorBenchmark.early_event_missed(predicted[pred_idx], frame_num, direction):
                    misses += 1
                    print("Event at frame {} direction: {} is missed".format(frame_num, direction))
                    gt_idx += 1
                else:  # if not in range - the current prediction is made up
                    made_ups += 1
                    print("Snapshot at frame {} is made up".format(predicted[pred_idx][1]))
                    pred_idx += 1
            else:
                break

        # the remaining predicted events are made ups
        while pred_idx < total_predicted_events:
            made_ups += 1
            print("Snapshot at frame {} is made up".format(predicted[pred_idx][1]))
            pred_idx += 1
        # the remaining gt events are misses
        while gt_idx < total_true_events:
            misses += 1
            print("Event at frame {} direction: {} is missed".format(gt[gt_idx]['frameNum'], gt[gt_idx]['direction']))
            gt_idx += 1

        return dict(misses=misses, made_ups=made_ups, total=total_true_events)

    @staticmethod
    def read_gt(gt, idx):
        if isinstance(gt, list):
            return int(gt[idx]['frameNum']), gt[idx]['direction']
        return gt['frame'][idx], gt['direction'][idx]

    @staticmethod
    def is_event_in_range(predicted, gt_frame, gt_direction, tol_in=(-10, 10), tol_out=(0, 30)):
        # in case the gt event frame comes slightly before / after the snapshot we add tolerance
        tolerance = tol_in if gt_direction == 'in' else tol_out
        if predicted[0] + tolerance[0] < gt_frame < predicted[1] + tolerance[1]:
            return True
        return False

    @staticmethod
    def early_event_missed(predicted, gt_frame, gt_direction, tol_in=(-10, 10), tol_out=(0, 30)):
        tolerance = tol_in if gt_direction == 'in' else tol_out
        if gt_frame < predicted[0] + tolerance[0]:
            return True
        return False


    @staticmethod
    def print_results(score):
        total_denom = score['total'] + 1e-6
        print("{:.1f}% [{}/{}], misses: {:.1f}% [{}], made-ups {:.1f}% [{}]".format(
            ((score['total'] - score['misses']) / total_denom * 100), score['total'] - score['misses'], score['total'],
            score['misses'] / total_denom * 100, score['misses'],score['made_ups'] / total_denom * 100,score['made_ups']))

    @staticmethod
    def sum_up_results(all_movies_results):
        print("\n\n*************************************")
        print("Event detector Results by movie:")
        misses = 0
        made_ups = 0
        total = 0
        for movie_results in all_movies_results:
            print(movie_results['movie_name'], end='\t')
            if movie_results['results'] is None:
                print("no gt")
                continue
            EventDetectorBenchmark.print_results(movie_results['results'])
            misses += movie_results['results']['misses']
            made_ups += movie_results['results']['made_ups']
            total += movie_results['results']['total']
        summed_results =  dict(misses=misses, made_ups=made_ups, total=total)
        print('\nTotal\t', end='\t')
        EventDetectorBenchmark.print_results(summed_results)
        print("*************************************")


DEFAULT_CSV = os.path.join(os.path.dirname(os.path.realpath(__file__)),'../../ourFirstCNN/recording_list/default.csv')
DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
if __name__=='__main__':
    parser = argparse.ArgumentParser(description='generate event trigger images')
    parser.add_argument('--movies-file', help='input video',type=str, default=DEFAULT_CSV)
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)

    args = parser.parse_args()
    all_results = []

    movie_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()
    for single_movie in movie_list:
        movie_rec = locate_movie(single_movie, args.root_dir)
        full_rec_path = os.path.join(movie_rec, single_movie)
        movie_name_wo_suffix = full_rec_path.split('_L.avi')[0]
        gt_csv = pd.read_csv(movie_name_wo_suffix + '_L.csv')
        eve_det_bench = EventDetectorBenchmark()
        wait_time = 10
        frame_idx = 0
        try:
            stream = BaseImageGrabber('playback', movie_name_wo_suffix, target_ms=wait_time)
            proc = Process(target=stream.run)
            proc.start()

            prop_file = movie_name_wo_suffix + '_prop.json'
            with open(prop_file) as f:
                camera_id = json.load(f)['camera_id']
            # Load config data
            cart_calib = CartCalib(camera_id)
            cart_bottom_detector = MultiCamDiffDetector(md_module=None, weights=config.DEFAULT_DIFF_DETECTOR_WEIGHTS, cam_names=['front', 'back'], camera_config=cart_calib.conf_data)
            vx_prcss = msm.init(640, 480, 640, 480, cart_calib.calib_dir,False)
            spatial_th = np.zeros((320, 240),dtype=np.uint8)

            while True:
                img_data, _, _ = stream.grab(can_skip=False)
                frame_L = img_data['data']['left']['img'].copy()
                frame_R = img_data['data']['right']['img'].copy()
                frame_B = img_data['data']['back']['img'].copy()
                disparity_image, grayL_resized, frameL_resized, frameB_resized, cart_disp_mask, left_remap, \
                    frameDelta, frameDeltaBack = msm.process_stream(vx_prcss, 0, frame_L, frame_R, frame_B, spatial_th)[:8]

                cb_images = dict(front=frameL_resized, back=frameB_resized)
                cb_images_high_res = dict(front=left_remap, back=frame_B)
                cb_frame_delta = dict(front=frameDelta, back=frameDeltaBack)
                snapshot_taken, _, dynamic = cart_bottom_detector.step(cb_images, cb_frame_delta, frame_idx, img_high_res=cb_images_high_res)

                eve_det_bench.step(snapshot_taken, dynamic, frame_idx)
                frame_idx += 1

        except:
            proc.terminate()
            pass # movie finished
        finally:
            pass
            # proc.terminate()
        results = EventDetectorBenchmark.calculate_performance(eve_det_bench.events_predicted, gt_csv)
        print("\nResults for movie {}:".format(os.path.basename(movie_name_wo_suffix)))
        EventDetectorBenchmark.print_results(results)
        all_results.append(dict(movie_name=os.path.basename(movie_name_wo_suffix), results=results))

    EventDetectorBenchmark.sum_up_results(all_results)

