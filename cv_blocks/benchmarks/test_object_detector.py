import os
import argparse
import yaml
import numpy as np
import cv2
from tqdm import tqdm
from cv_blocks.common.types import BoundingBox2D, RotatedBoundingBox2D
from cv_blocks.metrics.detector_metrics import bbox_iou, compute_detector_metric
from cv_blocks.visualizers.detector_visualizer import DetectorVisualizer
import xml.etree.ElementTree as ET
import pickle
import json

def get_detector_v1_bboxes_from_att(gt_bboxes):
    bboxes = []
    # gt_bboxes = json.loads(gt_bboxes)  # transforms a string containing list information to list
    for gt_bbox in gt_bboxes:
        poly_points = np.array(gt_bbox)
        xmin, ymin = poly_points.min(axis=0)
        xmax, ymax = poly_points.max(axis=0)
        bboxes.append(BoundingBox2D((xmin, ymin, xmax, ymax)))
    return bboxes

def decode_cvat_xml(path, img_type='frame_delta'):
    annots_dict = list()
    tree = ET.parse(path).getroot()
    base_dir = os.path.dirname(path)
    for elem in tree:
        if elem.tag == 'image':
            skip_image = False
            bboxes = []
            if len(elem) == 0:
                continue
            for sub_elem in elem:
                if 'label' in sub_elem.attrib and sub_elem.attrib['label'] == 'crop':
                    # Polygon with attributes
                    try:
                        poly_point = [[float(xy_str.split(',')[0]), float(xy_str.split(',')[1])] \
                                      for xy_str in (sub_elem.attrib['points']).split(';')]
                        for att_elem in sub_elem:
                            if att_elem.attrib['name'] == 'crop type' and att_elem.text == 'product':
                                bboxes.append(poly_point)
                    except:
                        skip_image = True
            # Check if image should be skipped - skip flag or no event annotation
            if skip_image:
                print('Warning - Corrupted polygon for {}, ignoring image'.format(elem.attrib['name']))
                continue
            image_name = os.path.basename(elem.attrib['name'])
            image_name = image_name.replace('_viz_', '_full_')  # annotation image is only for visualization
            product_name = image_name.split('cls_')[-1].split('.')[0]
            camera = 'back' if 'back' in image_name else 'front'
            img_path = os.path.join(base_dir, product_name, camera + '_full', image_name)
            if not os.path.exists(img_path):
                print("image {}\ndoes not exist!".format(img_path))
                continue
            if img_type == 'frame_delta':
                fd_path = img_path.replace('full', 'frame_delta')
                if os.path.exists(fd_path):
                    annots_dict.append(dict(img=img_path, img_fd=fd_path, gt=bboxes, gt_type='att_boxes'))
                else:
                    print("fd {}\ndoes not exist!".format(fd_path))
                    continue
            else:
                dataset.append(dict(img=img_path, gt=bboxes, gt_type='att_boxes'))
    return annots_dict

def generate_image_gt_pairs_from_xml(xml_path, img_dirs='full'):
    dataset = decode_cvat_xml(xml_path)
    print("loaded {} images from {}".format(len(dataset), xml_path))
    return dataset

JSON_IMAGE_COUPLING = dict(wnoise='full', frame_delta='full', full='full', multi_frames='full')
def generate_image_gt_pairs(input_dir, img_dirs='full', no_gt=False, rotated=False):
    '''
    Return list of pairs: {image, gt annotations}
    '''
    img_dirs = img_dirs.split(',')
    dataset = []
    print("input dir:", input_dir)
    for camera in ('front', 'back'):
        for img_type in img_dirs:
            img_dir = '/' + camera + '_' + img_type + '/'
            for root, dirs, files in os.walk(input_dir):
                if img_dir not in root + '/':
                    continue
                for file in files:
                    suffix = os.path.splitext(file)[-1].lower()
                    if suffix in ['.jpg', '.jpeg', '.png']:
                        img_path = os.path.join(root, file)
                        gt_path = img_path.replace(suffix, '.json').replace(img_dir, '/annotations/').replace(img_type, JSON_IMAGE_COUPLING[img_type])
                        if not os.path.exists(gt_path):
                            print("No gt for image: {}".format(img_path))
                        else:
                            if img_type == 'frame_delta':
                                rgb_path = img_path.replace(img_type, 'full')
                                if not os.path.exists(rgb_path):
                                    print("No rgb image at: {}".format(rgb_path))
                                else:
                                    dataset.append(dict(img=rgb_path, img_fd=img_path, gt=gt_path, gt_type='old'))
                            else:
                                dataset.append(dict(img=img_path, gt=gt_path, gt_type='old'))
    print('Loaded %d images with annotations' % len(dataset))
    return dataset

def remove_images_with_masked_bboxes(data, rotated=False):
    threshold = 0.5
    filtered_data = []
    for d in data:
        remove = False
        if "wnoise" in d['img']:
            gt_boxes = get_gt_bboxes(d['gt'], rotated=rotated)
            masked_image = cv2.imread(d['img'].replace("wnoise", "mask"), 0)
            for bbox in gt_boxes:
                cropped_bbox = masked_image[bbox.y1: bbox.y2, bbox.x1: bbox.x2].copy()
                num_pixels = cropped_bbox.size
                black_pixels = np.sum(cropped_bbox <= 50) # sometimes the masked pixels have values greater than 0
                black_pixels_percentage = black_pixels / num_pixels
                if black_pixels_percentage >= threshold:
                    remove = True
                    break
        if not remove:
            filtered_data.append(d)
    print("removed {} images with masked bboxes".format(len(data) - len(filtered_data)))
    return filtered_data

def get_gt_bboxes(gt_path, obj_list=('object',), rotated=False):
    bboxes = []
    if gt_path is None:
        return bboxes
    with open(gt_path, 'r') as fp:
        img_ann = json.load(fp)
        for r in img_ann['regions']:
            if rotated:
                if not r['type'] == 'POLYGON' or r['tags'][0].lower() not in obj_list:
                    continue
                bboxes.append(RotatedBoundingBox2D.from_poly(r['points']))
            else:
                if not r['type'] in ('POLYGON', 'RECTANGLE') or r['tags'][0].lower() not in obj_list:
                    continue
                bbox = dict(xmin=round(r['boundingBox']['left']),
                            ymin=round(r['boundingBox']['top']),
                            xmax=round(r['boundingBox']['left']+r['boundingBox']['width']),
                            ymax=round(r['boundingBox']['top']+r['boundingBox']['height']),
                            obj_id=obj_list.index(r['tags'][0]),
                            obj_name=r['tags'][0],
                )
                bboxes.append(BoundingBox2D((bbox['xmin'], bbox['ymin'], bbox['xmax'], bbox['ymax'])))
    return bboxes

def order_det_by_gt(boxes, gt_boxes):
    '''
    Re-order detections so they align with GT:
    '''
    new_boxes = []
    best_ious = []
    if len(boxes)==0 or len(gt_boxes)==0:
        return boxes, best_ious, [], gt_boxes
    is_rotated = isinstance((gt_boxes + boxes)[0], RotatedBoundingBox2D)
    iou_f = RotatedBoundingBox2D.iou if is_rotated else BoundingBox2D.iou
    matched_gt, unmatched_gt = [], []
    for gt_b in gt_boxes:
        ious = np.array([iou_f(gt_b, b) for b in boxes])
        best_idx = np.argmax(ious)
        if ious[best_idx] > 0:
            matched_gt.append(gt_b)
            new_boxes.append(boxes[best_idx])
            best_ious.append(ious[best_idx])
            del boxes[best_idx]
            if len(boxes)==0:
                break
        else:
            unmatched_gt.append(gt_b)
    new_boxes = new_boxes + boxes

    return new_boxes, best_ious, matched_gt, unmatched_gt

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='object detector benchmark')
    parser.add_argument('--detector', help='Detector type to use',type=str, default='yolo')
    parser.add_argument('--backend', help='backend to use for CNN[trt|torch]',type=str, default='trt')
    parser.add_argument('--input-root-dir', help='Path for images and annotations root directory',type=str, default=None)
    parser.add_argument('--input-dirs', help='DS numbers to load from',type=str, default='demo35_test_pile,demo35_test_single')
    parser.add_argument('--output-dir', help='Path for results',type=str, default=None)
    parser.add_argument('--conf-file', help='Detector config file',type=str, default=None)
    parser.add_argument('--weights', help='Detector weight file',type=str, default=None)
    parser.add_argument('--failure-th', help='detection threshold below that is failure',type=float, default=0.5)
    parser.add_argument('--viz-all', help='visualize all detections', action='store_true')
    parser.add_argument('--viz-failure', help='visualize detector failure', action='store_true')
    parser.add_argument('--conf-th', help='detector threshold',type=float, default=0.3)
    parser.add_argument('--nms-th', help='NMS threshold',type=float, default=0.3)
    parser.add_argument('--subdir', help='image type to load: from full,wnoise,frame_delta',type=str, default='frame_delta')
    parser.add_argument('--channels', help='input number of channels',type=int, default=4)
    parser.add_argument('--no-gt', help='visualize detector failure', action='store_true')
    parser.add_argument('--version', help='Detector arch version',type=str, default='v1')
    parser.add_argument('--rm-masked-bboxes', help='remove masked bboxes', action='store_true')
    parser.add_argument('--rotated', help='rotated bboxes', action='store_true')
    args = parser.parse_args()

    assert args.version in ('v1', 'v2')
    # Load images dataset
    # ===================
    dataset = []
    img_dirs = args.input_dirs.split(',')
    for img_dir in img_dirs:
        input_dir = os.path.join(args.input_root_dir, img_dir)
        xml_path = os.path.join(input_dir, 'product_detector', 'product_detector_v2_annotations.xml')
        if os.path.exists(xml_path):
            dataset += generate_image_gt_pairs_from_xml(xml_path, img_dirs=args.subdir)
        else:
            input_dir = os.path.join(input_dir, 'detector')
            if not os.path.isdir(input_dir):
                continue
            dataset += generate_image_gt_pairs(input_dir, img_dirs=args.subdir, no_gt=args.no_gt)

    if "wnoise" in args.subdir and args.rm_masked_bboxes:
        dataset = remove_images_with_masked_bboxes(dataset)

    # Load detector
    # =============
    if args.detector=='yolo':
        if args.backend=='torch':
            # TODO - support config when no file is provided
            assert os.path.exists(args.conf_file)
            if args.version=='v1':
                from cv_blocks.object_detector.yolo_detector.torch_impl.yolo import YoloObjectDetector as YOLO
            elif args.version=='v2':
                from cv_blocks.object_detector.yolo_detector.torch_impl.multi_att_yolo import MultiAttYoloObjectDetector as YOLO
        elif args.backend == 'trt':
            if args.conf_file is None:
                args.conf_file = args.weights.replace('.onnx','_config.yaml')
            if args.version=='v1':
                from tensorrt_lib.TrtYolo_cpp import YOLO
            elif args.version=='v2':
                from cv_blocks.object_detector.yolo_detector.trt_impl.multi_att_yolo import MultiAttYoloObjectDetector as YOLO
        else:
            raise NotImplementedError
    # Load config
    # ===========
    assert os.path.isfile(args.conf_file) or args.backend != 'trt', 'Invalid config file %s' % args.conf_file
    with open(args.conf_file) as config_f:
        config = yaml.safe_load(config_f)

    box_type = config['box_type'] if 'box_type' in config else 'axis_box'
    if args.version=='v1':
        obj_det = YOLO(config['backend'],
                    input_size=config['input_size'],
                    labels=config['labels'],
                    anchors=config['anchors'],
                    max_box_per_image=config['max_box_per_image'],
                    model_path=args.weights,
                    confidence_th=args.conf_th,
                    nms_th=args.nms_th,
                    channels=config['input_channels'],
                    box_type=box_type)
    if args.version=='v2':
        config['backend'] = 'YoloMobilenetV2MultiAtts' # DEBUG
        obj_det = YOLO(config['backend'],
                    input_size=config['input_size'],
                    attributes=config['attributes'],
                    anchors=config['anchors'],
                    weights_path=args.weights,
                    confidence_th=args.conf_th,
                    nms_th=args.nms_th,
                    channels=config['input_channels'])

    # Load visualizer
    # ===============
    visualizer = DetectorVisualizer(config['labels'], output_dir=os.path.join(args.output_dir, 'images'))
    
    # Run detector on images
    # ======================
    detection_data = []
    print('Running on images...')
    for d in tqdm(dataset):
        if config['input_channels'] == 4 and 'frame_delta' in args.subdir:
            img_bgr = cv2.imread(d['img'])
            img_fd = cv2.imread(d['img_fd'], 0)
            img = np.concatenate((np.expand_dims(img_fd, axis=2), img_bgr), axis=2)
        elif 'frames_stacked' in config.keys() and config['frames_stacked'] > 1:
            is_gray = config['input_channels'] / config['frames_stacked'] == 1
            img = cv2.imread(d['img'])
            shape = img.shape
            split_images = [img[:, int(shape[1]/3) * 2:, :],
                            img[:, int(shape[1]/3):int(shape[1]/3) * 2, :],
                            img[:, :int(shape[1] / 3), :]]
            img_bgr = split_images[0].copy()
            if config['input_channels'] not in (3, 4):
                for idx in range(len(split_images)):
                    split_images[idx] = cv2.resize(split_images[idx], (256,256), interpolation=cv2.INTER_LINEAR)
            if config['frames_stacked'] < len(split_images):
                split_images = split_images[:config['frames_stacked']]
            if is_gray and len(shape) > 2:
                for idx, image in enumerate(split_images):
                    split_images[idx] = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            img = np.dstack(split_images)
        else:
            img = cv2.imread(d['img'])
            img_bgr = img
        boxes = obj_det.predict([img, img])[0]
        if args.version=='v2':
            crop_type_categories = obj_det.attributes[0]['crop_type']
            # Filter boxes that are not classified as 'product' 
            min_prod_prob = 0.5
            boxes = [bx for bx in boxes if crop_type_categories[np.argmax(bx.attributes['crop_type'])]=='product' \
                and bx.attributes['crop_type'].max() > min_prod_prob]
        for i in range(len(boxes)):
            boxes[i] = boxes[i].renormalize(img_bgr.shape[0], img_bgr.shape[1])
        if d['gt_type'] == 'old':
            gt_boxes = get_gt_bboxes(d['gt'], rotated=args.rotated)
        else:#gt_type=='att_boxes'
            gt_boxes = get_detector_v1_bboxes_from_att(d['gt'])
        # Order detections so it will be aligned with gt 
        boxes, ious, matched_gt, unmatched_gt = order_det_by_gt(boxes, gt_boxes)
        if args.viz_all:
            visualizer.visualize(img_bgr, boxes, gt_boxes, ious, name=d['img'])
        image_data = dict(pred=boxes, matched_gt=matched_gt, unmatched_gt=unmatched_gt, iou=ious, img_name=d['img'])
        detection_data.append(image_data)
    
    if not args.no_gt:
        # Calculate metrics
        # =================
        print("Calculating detector metrics...")
        results = compute_detector_metric(detection_data, args.output_dir, args.weights)

        # Visualize failures
        # ==================
        if args.viz_failure:
            print("Saving failure images...")
            failure_imgs = list(results[results['iou'] < args.failure_th]['img_name'].unique())
            dataset_failure = [d for d in detection_data if d['img_name'] in failure_imgs]
            for d in tqdm(dataset_failure):
                img = cv2.imread(d['img_name'])
                if 'frames_stacked' in config.keys() and config['frames_stacked'] > 1:
                    img = img[:, int(img.shape[1] / 3) * 2:, :]
                boxes = d['pred']
                gt_boxes = d['matched_gt'] + d['unmatched_gt']
                ious = d['iou']
                visualizer.visualize(img, boxes, gt_boxes, ious, name='faliure_%s' % d['img_name'])