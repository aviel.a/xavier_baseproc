import os
import argparse
import numpy as np
import cv2
import pickle
from cv_blocks.localization.place_recognition.dbow3 import DBoWPlaceRecognizer
from cv_blocks.localization.place_recognition.utils import viz_similar_images

def collect_rec_db(rec_path, place_recongizer, max_img, img_every):
    cache_path = os.path.join(os.path.dirname(rec_path), 'loc_cache', rec_path + '.cache')
    if not os.path.exists(cache_path):
        os.makedirs(os.path.dirname(cache_path), exist_ok=True)
        camera = cv2.VideoCapture(rec_path)
        rec_db = []
        k = 0
        while True:
            ret, img = camera.read()
            k += 1
            if k % img_every != 0:
                continue
            if not ret: 
                break
            img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
            bow_vec = place_recongizer.calc_descriptor(img)
            rec_db.append(dict(img=img, bow_vec=bow_vec))
            if max_img is not None and len(rec_db) > max_img:
                break
        pickle.dump(rec_db, open(cache_path, 'wb'))
    else:
        rec_db = pickle.load(open(cache_path, 'rb'))
    return rec_db

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Place Recognition benchmark')
    parser.add_argument('--recognizer', help='Place Recognizer type to use',type=str, default='dbow')
    parser.add_argument('--rec1', help='reference movie', type=str, required=True)
    parser.add_argument('--rec2', help='reference movie', type=str, required=True)
    parser.add_argument('--img-every-n', help='how many frames between 2 images', type=int, default=50)
    parser.add_argument('--max-img', help='max number of images', type=int, default=None)
    parser.add_argument('--top-k', help='take top k images', type=int, default=3)
    parser.add_argument('--video', help='store visualization video', action='store_true')
    args = parser.parse_args()

    # Initialize place recognizer
    assert args.recognizer=='dbow'
    assert os.path.exists(args.rec1) and os.path.exists(args.rec2)
    if args.recognizer=='dbow':
        place_recongizer = DBoWPlaceRecognizer()
    
    # Collect imgs from recordings
    rec1_db = collect_rec_db(args.rec1, place_recongizer, args.max_img, args.img_every_n)
    rec2_db = collect_rec_db(args.rec2, place_recongizer, args.max_img, args.img_every_n)
    if args.video:
        video_path = os.path.join(os.path.dirname(args.rec1), 'viz.mp4')
        video_writer = cv2.VideoWriter(video_path,cv2.VideoWriter_fourcc('M','J','P','G'), 5, (240 * (1+args.top_k),320))

    # Calculate pair-wise distances 
    db1_size = len(rec1_db)
    db2_size = len(rec2_db)
    dist_mat = np.zeros((db1_size, db2_size))
    for i in range(db1_size):
        for j in range(db2_size):
            similarity_score = place_recongizer.calc_similarity(rec1_db[i]['bow_vec'], rec2_db[j]['bow_vec'])
            dist_mat[i,j] = similarity_score

    # Calc most similar images per each img (1->2)
    for i in range(db1_size):
        similarity_scores = dist_mat[i]
        most_similar_idx = np.argsort(-similarity_scores)[:args.top_k].ravel()
        similar_images = [rec2_db[idx]['img'] for idx in most_similar_idx]
        similar_scores = [similarity_scores[idx] for idx in most_similar_idx]
        viz_img = viz_similar_images(rec1_db[i]['img'], similar_images, similar_scores)
        if args.video:
            video_writer.write(viz_img)
    
    if args.video:
        video_writer.release()