import os
import argparse
import yaml
import numpy as np
import cv2
from tqdm import tqdm
from cv_blocks.common.types import BoundingBox2D, RotatedBoundingBox2D
from cv_blocks.metrics.detector_metrics import bbox_iou, compute_detector_metric
from cv_blocks.visualizers.detector_visualizer import DetectorWithAttVisualizer
import xml.etree.ElementTree as ET
import random
import pickle
import json
import subprocess

DEFAULT_PRODUCT_TYPE = ['product', 'hand', 'hand-holding-product']
def decode_cvat_xml(path, product_type, img_type='fd'):
    annots_dict = list()
    dir = os.path.dirname(path)
    tree = ET.parse(path).getroot()
    base_dir = os.path.dirname(path)
    for elem in tree:
        if elem.tag == 'image':
            skip_image = False
            image_ann = dict(polygons=[])
            if len(elem)==0:
                continue
            for sub_elem in elem:
                if 'label' in sub_elem.attrib and sub_elem.attrib['label']=='crop':
                    # Polygon with attributes
                    try:
                        poly_point = [[float(xy_str.split(',')[0]) , float(xy_str.split(',')[1])] \
                            for xy_str in (sub_elem.attrib['points']).split(';')]
                        poly_att = dict()
                        for att_elem in sub_elem:
                            poly_att[att_elem.attrib['name']] = att_elem.text
                        polygon = dict(points=poly_point, atts=poly_att)
                        if poly_att['crop type'] in product_type:
                            image_ann['polygons'].append(polygon)
                    except:
                        skip_image = True
                elif 'label' in sub_elem.attrib and sub_elem.attrib['label']=='event':
                    # Event attribute
                    image_ann['event type'] = sub_elem[0].text

            # Check if image should be skipped - skip flag or no event annotation
            if skip_image or 'event type' not in image_ann or image_ann['event type'] is None:
                print('Warning - Corrupted polygon, ignoring image')
                continue

            image_name = os.path.basename(elem.attrib['name'])
            image_name = image_name.replace('_viz_', '_full_') # annotation image is only for visualization
            product_name = image_name.split('cls_')[-1].split('.')[0]
            camera = 'back' if 'back' in image_name else 'front'
            img_path = os.path.join(base_dir, product_name, camera + '_full', image_name)
            if not os.path.exists(img_path):
                print("image {}\ndoes not exist!".format(img_path))
                continue
            if img_type == 'fd':
                fd_path = img_path.replace('full', 'frame_delta')
                if os.path.exists(fd_path):
                    annots_dict.append(dict(img=img_path, fd=fd_path, gt=image_ann, gt_type='att_boxes'))
                else:
                    print("fd {}\ndoes not exist!".format(fd_path))
                    continue

    return annots_dict

def generate_image_gt_pairs(local_dir, dataset_dir, product_type, img_dirs='full'):
    if not os.path.exists(local_dir):
        cmd = "aws s3 sync s3://walkout-main/visual_db_storage/{}/product_detector/ {}".format(dataset_dir, local_dir)
        print(cmd)
        subprocess.run(cmd,
                       stdout=subprocess.PIPE,
                       shell=True)
    xml_path = os.path.join(local_dir, 'product_detector_v2_annotations.xml')
    if os.path.exists(xml_path):
        dataset = decode_cvat_xml(xml_path, product_type=product_type)
        print("loaded {} images from {}".format(len(dataset), dataset_dir))
    else:
        print("no annotation file for {}".format(local_dir))
    return dataset

def get_gt_bboxes(img_gt, obj_att_dict):
    bboxes = []
    if 'polygons' in img_gt:
        for gt_poly in img_gt['polygons']:
            poly_points = np.array(gt_poly['points'])
            xmin, ymin = poly_points.min(axis=0)
            xmax, ymax = poly_points.max(axis=0)
            gt_atts = dict()
            for att in obj_att_dict:
                att_name = list(att.keys())[0]
                att_categories = att[att_name]
                gt_att_name = att_name.replace('_', ' ')
                gt_atts[att_name] = gt_poly['atts'][gt_att_name]
                
            bboxes.append(BoundingBox2D((xmin, ymin, xmax, ymax), attributes=gt_atts))
    return bboxes

# def get_gt_bboxes(gt_polygons, obj_list=('object',), rotated=False):
#     bboxes = []
#     for r in gt_polygons:
#         import ipdb; ipdb.set_trace(context=11)
#         if rotated:
#             if not r['type'] == 'POLYGON' or r['tags'][0].lower() not in obj_list:
#                 continue
#             bboxes.append(RotatedBoundingBox2D.from_poly(r['points']))
#         else:
#             if not r['type'] in ('POLYGON', 'RECTANGLE') or r['tags'][0].lower() not in obj_list:
#                 continue
#             bbox = dict(xmin=round(r['boundingBox']['left']),
#                         ymin=round(r['boundingBox']['top']),
#                         xmax=round(r['boundingBox']['left']+r['boundingBox']['width']),
#                         ymax=round(r['boundingBox']['top']+r['boundingBox']['height']),
#                         obj_id=obj_list.index(r['tags'][0]),
#                         obj_name=r['tags'][0],
#             )
#             bboxes.append(BoundingBox2D((bbox['xmin'], bbox['ymin'], bbox['xmax'], bbox['ymax'])))
#     return bboxes

def order_det_by_gt(boxes, gt_boxes, product_type):
    '''
    Re-order detections so they align with GT:
    '''
    new_boxes = []
    best_ious = []
    if len(boxes)==0 or len(gt_boxes)==0:
        return boxes, best_ious, [], gt_boxes
    is_rotated = isinstance((gt_boxes + boxes)[0], RotatedBoundingBox2D)
    iou_f = RotatedBoundingBox2D.iou if is_rotated else BoundingBox2D.iou
    unmatched_gt = []
    matched_gt = []
    # First run - match only to the same class
    for gt_b in gt_boxes:
        ious = np.array([iou_f(gt_b, b) if np.argmax(b.attributes['crop_type']) == product_type.index(gt_b.attributes['crop_type']) else 0 for b in boxes])
        best_idx = np.argmax(ious)
        if np.max(ious) > 0:
            matched_gt.append(gt_b)
            new_boxes.append(boxes[best_idx])
            best_ious.append(ious[best_idx])
            del boxes[best_idx]
            if len(boxes)==0:
                break
        else:
            unmatched_gt.append(gt_b)
    # Second run - consider wrong classification
    unmatched_gt_final = []
    for gt_b in unmatched_gt:
        if len(boxes) == 0:
            unmatched_gt_final.append(gt_b)
            continue
        ious = np.array([iou_f(gt_b, b) for b in boxes])
        best_idx = np.argmax(ious)
        if np.max(ious) > 0:
            matched_gt.append(gt_b)
            new_boxes.append(boxes[best_idx])
            best_ious.append(ious[best_idx])
            del boxes[best_idx]
            if len(boxes)==0:
                break
        else:
            unmatched_gt_final.append(gt_b)

    new_boxes = new_boxes + boxes
    if len(matched_gt) > len(best_ious):
        pass
    return new_boxes, best_ious, matched_gt, unmatched_gt_final

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='object detector benchmark')
    parser.add_argument('--detector', help='Detector type to use',type=str, default='yolo')
    parser.add_argument('--backend', help='backend to use for CNN[trt|torch]',type=str, default='trt')
    parser.add_argument('--input-root-dir', help='Path for images and annotations root directory',type=str, default=None)
    parser.add_argument('--input-dirs', help='Dataset directories to load from',type=str, default='prod_det_v2_batch_2')
    parser.add_argument('--output-dir', help='Path for results',type=str, default=None)
    parser.add_argument('--conf-file', help='Detector config file',type=str, default=None)
    parser.add_argument('--weights', help='Detector weight file',type=str, default=None)
    parser.add_argument('--failure-th', help='detection threshold below that is failure',type=float, default=0.5)
    parser.add_argument('--viz-all', help='visualize all detections', action='store_true')
    parser.add_argument('--viz-failure', help='visualize detector failure', action='store_true')
    parser.add_argument('--conf-th', help='detector threshold',type=float, default=0.3)
    parser.add_argument('--nms-th', help='NMS threshold',type=float, default=0.3)
    parser.add_argument('--subdir', help='image type to load: from full,wnoise,frame_delta',type=str, default='frame_delta')
    parser.add_argument('--channels', help='input number of channels',type=int, default=3)
    parser.add_argument('--no-gt', help='visualize detector failure', action='store_true')
    parser.add_argument('--rm-masked-bboxes', help='remove masked bboxes', action='store_true')
    parser.add_argument('--rotated', help='rotated bboxes', action='store_true')
    parser.add_argument('--products-only', help='test only on products bounding boxes', action='store_true')
    args = parser.parse_args()

    # Load detector
    # =============
    if args.detector=='yolo':
        if args.backend=='torch':
            assert os.path.exists(args.conf_file)
            from cv_blocks.object_detector.yolo_detector.torch_impl.multi_att_yolo import MultiAttYoloObjectDetector as YOLO
        elif args.backend == 'trt':
            if args.conf_file is None:
                args.conf_file = args.weights.replace('.onnx','_config.yaml')
            from cv_blocks.object_detector.yolo_detector.trt_impl.multi_att_yolo import MultiAttYoloObjectDetector as YOLO
        else:
            raise NotImplementedError
    # Load config
    # ===========
    assert os.path.isfile(args.conf_file) or args.backend != 'trt', 'Invalid config file %s' % args.conf_file
    with open(args.conf_file) as config_f:
        config = yaml.safe_load(config_f)

    # Load images dataset
    # ===================
    dataset = []
    img_dirs = args.input_dirs.split(',')
    attr_dict = dict()
    for attr in config['attributes']:
        task_name = list(attr.keys())[0]
        attr_dict[task_name] = attr[task_name]
    product_type = attr_dict['crop_type'] if not args.products_only else ['product']
    for dataset_dir in img_dirs:
        local_dir = os.path.join(args.input_root_dir, dataset_dir, 'product_detector')
        dataset += generate_image_gt_pairs(local_dir, dataset_dir, product_type=product_type, img_dirs=args.subdir)

    random.shuffle(dataset)

    config['backend'] = 'YoloMobilenetV2MultiAtts' # DEBUG
    obj_det = YOLO(config['backend'],
                   input_size=config['input_size'],
                   attributes=config['attributes'],
                   anchors=config['anchors'],
                   weights_path=args.weights,
                   confidence_th=args.conf_th,
                   nms_th=args.nms_th,
                   channels=config['input_channels'],
                   predict_products_only=args.products_only)

    # Load visualizer
    # ===============
    visualizer = DetectorWithAttVisualizer(obj_det.attributes, \
        output_dir=os.path.join(args.output_dir, 'images') if args.output_dir is not None else None)
    
    # Run detector on images
    # ======================
    detection_data = []
    print('Running on images...')
    for d in tqdm(dataset):
        if config['input_channels'] == 4 and 'frame_delta' in args.subdir:
            img_bgr = cv2.imread(d['img'])
            img_fd = cv2.imread(d['fd'], 0)
            img = np.concatenate((np.expand_dims(img_fd, axis=2), img_bgr), axis=2)
        else:
            img = cv2.imread(d['img'])
            img_bgr = img
        if args.backend=='torch':
            boxes = obj_det.predict(img)
        elif args.backend=='trt':
            boxes = obj_det.predict([img, img])[0]
        for i in range(len(boxes)):
            boxes[i] = boxes[i].renormalize(img_bgr.shape[0], img_bgr.shape[1])
        gt_boxes = get_gt_bboxes(d['gt'], config['attributes'])
        # Order detections so it will be aligned with gt 
        boxes, ious, matched_gt, unmatched_gt = order_det_by_gt(boxes, gt_boxes, attr_dict['crop_type'])
        if args.viz_all:
            visualizer.visualize(img_bgr, boxes, gt_boxes, ious, name=d['img'])
        image_data = dict(pred=boxes, matched_gt=matched_gt, unmatched_gt=unmatched_gt, iou=ious, img_name=d['img'])
        detection_data.append(image_data)
    
    if not args.no_gt:
        # Calculate metrics
        # =================
        print("Calculating detector metrics...")
        results = compute_detector_metric(detection_data, args.output_dir, args.weights, attributes=attr_dict,
                                          visualizer=visualizer)

        # Visualize failures
        # ==================
        if args.viz_failure:
            print("Saving failure images...")
            failure_imgs = list(results[results['iou'] < args.failure_th]['img_name'].unique())
            dataset_failure = [d for d in detection_data if d['img_name'] in failure_imgs]
            for d in tqdm(dataset_failure):
                img = cv2.imread(d['img_name'])
                if 'frames_stacked' in config.keys() and config['frames_stacked'] > 1:
                    img = img[:, int(img.shape[1] / 3) * 2:, :]
                boxes = d['pred']
                gt_boxes = d['matched_gt'] + d['unmatched_gt']
                ious = d['iou']
                visualizer.visualize(img, boxes, gt_boxes, ious, name='faliure_%s' % d['img_name'])