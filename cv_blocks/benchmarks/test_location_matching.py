import argparse
import getpass
import os
import pandas as pd
from config import config
from cv_blocks.benchmarks.product_classifier.utils import MotionDetectLight, compare_to_gt, \
    compare_location_to_MD, scores_statistics
from cv_blocks.events.event_classifier import EventClassifier
from cv_blocks.events.event_logic import EventLogic
from cv_blocks.misc.logger import BaseLogger
import movie_parser_time_and_dir as mParser
from cv_blocks.misc.aws_download import movie_download, find_and_download
from cv_blocks.metrics.shopping_metrics import ItemStatus
from tqdm import tqdm
from cv_blocks.calibration.cart_calib import CartCalib
from cart_blocks.cart_content_handler import CartContentHandler
from cart_blocks import catalog
from WalkoutExecutor import result_line, kpi_line
from external_libs.tabulate import tabulate
from misc import bcolors as bcolors

FAILURE_STATES = (ItemStatus.EventDetected, ItemStatus.ProductDetectedInTopK, ItemStatus.EventMadeUp,
                  ItemStatus.EventMissed, ItemStatus.EventDetectedLowConf, ItemStatus.EventFalseLowConf)
FAILURE_STRINGS = [s.name for s in FAILURE_STATES]


def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            path = os.path.join(root, movie_name)
            if os.path.isfile(path) or os.path.isdir(path):
                return root

    print('no movies found in next paths :')
    for optional_dir in optional_dirs:
        print(os.path.join(optional_dir, movie_name))


def load_recording_cache(cache_file, root_dir):
    cache_dir = locate_movie(cache_file, root_dir)
    if cache_dir is None:
        cache_dir = find_and_download(cache_file, 'regression_data', root_dir)
    cache_full_path = os.path.join(cache_dir, cache_file)
    # Parse cache file
    # ================
    logger = BaseLogger(True)
    el_list = logger.from_data(cache_full_path)
    # Remove non-classified items
    el_list = [el['element'].data for el in el_list if el['type'] in ('event_aggregator_out')]

    return el_list, cache_dir


def result_line_loc(name, res, bold=False):
    total = res['total']
    top1_pr = res['top1_success'] / total * 100 if total > 0 else 0
    top3_pr = res['top3_success'] / total * 100 if total > 0 else 0
    success_pr = (res['top1_success'] + res['top3_success']) / total * 100 if total > 0 else 0
    err_pr = res['errors'] / total * 100 if total > 0 else 0
    no_pred_pr = res['no_prediction'] / total * 100 if total > 0 else 0
    ret_val = [name,
               "{:.1f}% [{}/{}], top1:{:.1f}%[{}], top3:{:.1f}%[{}]".format(
                   success_pr, res['top1_success'] + res['top3_success'], total, top1_pr, res['top1_success'],
                   top3_pr, res['top3_success']),
               "errors: {:.1f}% [{}/{}]".format(err_pr, res['errors'], total),
               "no predictions: {:.1f}% [{}/{}]".format(no_pred_pr, res['no_prediction'], total),
               "ignored: [{}]".format(res['ignored']),
               ]
    if bold:
        ret_val = [bcolors.BOLD + r + bcolors.ENDC for r in ret_val]

    return ret_val


DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='product classifier benchmark')
    parser.add_argument('--root-dir', help='path to all recordings root directory', type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--movies-file', help='path to csv file containing files to run', type=str, required=True)
    parser.add_argument('--model', help='product classifier model to test', type=str,
                        default=config.LATEST_YAEEN_TRT_MODEL_NAME)
    parser.add_argument('--params', help='path to yaml overwriting default params', type=str, default=None)
    parser.add_argument('--vis-dir', help='path to write visualizations', type=str, default=None)
    parser.add_argument('--skip-download', help='assume all files are found locally', action='store_true')
    parser.add_argument('--stat-path', help='path to statistics', type=str, default='/tmp')
    parser.add_argument('--vis-all', help='visualize all sequences', action='store_true')
    parser.add_argument('--failure-save-path', help='path to save failure data', type=str, default=None)
    parser.add_argument('--calib-model', help='calibration model for the capsule', type=str,
                        default='gen2_basler_sys0.11_sh_1')

    args = parser.parse_args()

    # Overwrite default params
    # ========================
    if args.params is not None:
        config.override_default_config(args.params)

    # Build partial system
    # ====================
    md_module = MotionDetectLight()
    # TODO - Add calibration model info to capsule
    cart_calib = CartCalib(config_id=args.calib_model, sync=False)
    event_classifier = EventClassifier(md_module, en_trt=True, model_name=args.model)
    md_module.event_classifier = event_classifier
    event_logic = EventLogic(md_module, event_classifier.yaaen.getProductFromIndex,
                             event_classifier.yaaen.getIdxFromProduct, ['front', 'back'],
                             el_config=dict(send_low_conf_as_unknown=config.LOW_CONF_AS_UNKNOWN,
                                            store_sent_events=True, verbose=0))
    event_classifier.margin_event_detector.set_cart_calib(cart_calib)
    catalog = catalog.Catalog(def_language=config.GUI_LANGUAGE)
    md_module.cart_content_handler = CartContentHandler(product_catalog=catalog, gui=None, md_module=None)

    # Load list of recordings to evaluate
    rec_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)[
        'movie'].tolist()

    all_events = []

    tot_res_MD = dict(clsSuccess=0, clsTested=0, MDSuccess=0, MDTested=0, unknownItems=0, n_missed=0, n_madeup=0, n_false_low_conf=0)
    resultsTable_MD = [['Movie Name', 'Cls Success', 'MD Success']]
    resultsTable_loc = [['Movie Name', 'Success', 'Errors', 'No Predictions', 'Ignored']]
    tot_loc_res = dict(total=0, top1_success=0, top3_success=0, errors=0, ignored=0, no_prediction=0, all_scores=[])

    event_stats = []
    if args.failure_save_path is not None:
        failure_vec = []
    for rec_file in tqdm(rec_list):
        rec_events, rec_dir = load_recording_cache(rec_file, args.root_dir)
        md_module.reset()
        event_logic.reset()
        event_classifier.reset()
        md_module.cart_content_handler.reset()
        # find gt file
        movie_file = rec_file.replace('.pkl', '_L.avi').replace('shop_mode_log_', '')
        movie_name = movie_file.split('_L.avi')[0]
        if not args.skip_download:
            if movie_download(movie_file, rec_dir, include=('_L.csv', 'script.json')) is None:
                print('GT file is not found, skipping to next file')
        try:
            gt = mParser.getPurchaseList(movie_file, rec_dir)
        except:
            print('Failed to load GT for %s. skipping' % movie_file)
            continue
        # Classify events sequentially
        # ============================
        last_event_frame = None
        for event in rec_events:
            if last_event_frame is not None:
                # Emulate event logic operation between events
                for ignore_frame in range(last_event_frame, event['event_frame']):
                    event_logic.step(ignore_frame, None)
            # Handle new event
            event_result = event_classifier.classify_sequence(event)
            if event_result is not None:
                event_logic.step(event_result['event_frame'], event_result)
                last_event_frame = event_result['event_frame']
        # Compare to GT
        mParser.printPurchaseList('walkout purchase list', md_module.purchaseList)
        mParser.printPurchaseList('ground Truth purchase', gt)
        all_location_predictions = md_module.cart_content_handler.location_matching.predictions_history
        confident_location_predictions = md_module.cart_content_handler.location_matching.confident_predictions
        results_dict_MD, analyzed_events = compare_to_gt(md_module, gt, movie_file, confident_location_predictions)
        loc_results = compare_location_to_MD(analyzed_events, movie_file, all_location_predictions)
        # Aggregate results
        for k in tot_res_MD.keys():
            tot_res_MD[k] += results_dict_MD[k]
        resultsTable_MD.append(result_line(movie_name, results_dict_MD))
        for k in tot_loc_res.keys():
            tot_loc_res[k] += loc_results[k]
        resultsTable_loc.append(result_line_loc(movie_name, loc_results))


    # results summary MD
    resultsTable_MD.append(['---------'])
    resultsTable_MD.append(result_line('Total', tot_res_MD, full=True, bold=True))
    print('\nResults by Movie:')
    print(tabulate(resultsTable_MD, headers="firstrow", tablefmt="pretty"))  # printing a summary table

    # Print KPI results
    # =================
    print('\nKPI report:')
    KpiTable = [['Total Events', 'Errors', 'Interactions', 'Err:Wrong Product', 'Err:Missed Event', 'Err:Made-up Event']]
    KpiTable.append(kpi_line(tot_res_MD, bold=True))
    print(tabulate(KpiTable, headers="firstrow", tablefmt="pretty"))

    # results summary location matching
    # print("loc: {}".format({key: value for key, value in tot_loc_res.items() if key != 'all_scores'}))
    resultsTable_loc.append(['---------'])
    resultsTable_loc.append(result_line_loc('Total', tot_loc_res, bold=True))
    print('\nLocation results by Movie:')
    print(tabulate(resultsTable_loc, headers="firstrow", tablefmt="pretty"))  # printing a summary table

    # statistics analysis
    scores_statistics(keys=('top1', 'top3', 'error'), scores=tot_loc_res['all_scores'])
