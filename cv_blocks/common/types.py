import numpy as np
import cv2

class BoundingBox2D(object):
    '''
    class implementing Bounding box 2d:
    (x1, y1)
           -------
           |     |
           |     |
           -------
                  (x2,y2)
    '''
    def __init__(self, box, classes=None, attributes=None):
        self.score = -1
        if isinstance(box, dict):
            self.x1 = box['xmin']
            self.x2 = box['xmax']
            self.y1 = box['ymin']
            self.y2 = box['ymax']
        elif all(hasattr(box, attr) for attr in ['xmin', 'xmax', 'ymin', 'ymax', 'score']):
            # TODO - remove when utils.BoundBox is removed
            self.x1 = box.xmin
            self.x2 = box.xmax
            self.y1 = box.ymin
            self.y2 = box.ymax
            self.score = box.score
        elif isinstance(box, (list, tuple, np.ndarray)): 
            self.x1, self.y1, self.x2, self.y2 = box[:4]
            if len(box)==5:
                self.score = box[4]
        else:
            raise NameError
            all(hasattr(box, attr) for attr in ['xmin', 'xmax', 'ymin', 'ymax', 'score'])
            import ipdb; ipdb.set_trace(context=11)
        self.classes = classes
        self.attributes = attributes
        self.connected_boxes = []
    
    def __repr__(self):
        if hasattr(self, 'attributes') and self.attributes is not None:
            return 'BoundingBox2D: (x1=%.2f, y1=%.2f, x2=%.2f, y2=%.2f, score=%.2f). attributes: %s'\
                % (self.x1, self.y1, self.x2, self.y2, self.score, str(self.attributes))
        else:
            return 'BoundingBox2D: (x1=%.2f, y1=%.2f, x2=%.2f, y2=%.2f, score=%.2f)'\
                % (self.x1, self.y1, self.x2, self.y2, self.score)
    
    def to_dict(self):
        ret_dict = dict()
        for k, v in vars(self).items():
            if isinstance(v, np.ndarray):
                ret_dict[k] = v.tolist()
            elif k=='attributes' and v is not None:
                ret_dict[k] = {att_k: att_v.tolist() for att_k, att_v in v.items()}
            elif k=='connected_boxes':
                ret_dict[k] = [] # ommit to avoid recursive flow
            elif type(v) in (np.float32, np.float64):
                ret_dict[k] = float(v)
            else:
                ret_dict[k] = v
        return ret_dict

    
    @staticmethod
    def xywh_to_xyxy(boxes):
        assert isinstance(boxes, np.ndarray)
        boxes = np.atleast_2d(boxes)
        boxes[:, 2] += boxes[:,0]
        boxes[:, 3] += boxes[:,1]

        return boxes

    @staticmethod
    def xyxy_to_xyhw(boxes):
        assert isinstance(boxes, np.ndarray)
        boxes = np.atleast_2d(boxes)
        boxes[:, 2] -= boxes[:, 0]
        boxes[:, 3] -= boxes[:, 1]

        return boxes

    @staticmethod
    def scale_similarity(box1, box2):
        '''
        Similarity [0-1] between w-h ratios of 2 boxes
        '''
        ratio1 = max(box1.h() / box1.w(), box1.w() / box1.h())
        ratio2 = max(box2.h() / box2.w(), box2.w() / box2.h())
        return min(ratio1/ratio2, ratio2/ratio1)

    @staticmethod
    def dist(box1, box2, norm_by=None):
        '''
        Distance between 2 boxes centroids
        '''
        c1 = box1.centroid(norm_by=norm_by)
        c2 = box2.centroid(norm_by=norm_by)
        return np.linalg.norm(c1 - c2)

    @staticmethod
    def area_ratio(box1, box2):
        '''
        Ratio [0-1] between the small and large box area
        '''
        ratio = (box1.area() + 1e-6) / (box2.area() + 1e-6)
        return min(ratio, 1./ratio)

    @staticmethod
    def iou(box1, box2):
        dx = min(box1.x2, box2.x2) - max(box1.x1, box2.x1)
        dy = min(box1.y2, box2.y2) - max(box1.y1, box2.y1)
        intersection = max(dx, 0) * max(dy, 0)
        union = box1.w() * box1.h() + (box2.x2 - box2.x1) * (box2.y2 - box2.y1) - intersection
        return intersection / max(union, 1e-6)

    @staticmethod
    def intersection(box1, box2):
        dx = min(box1.x2, box2.x2) - max(box1.x1, box2.x1)
        dy = min(box1.y2, box2.y2) - max(box1.y1, box2.y1)
        intersect = max(dx, 0) * max(dy, 0)
        return intersect

    def asarray(self):
        return np.array([self.x1, self.y1, self.x2, self.y2], dtype=np.float32)

    def get_label(self):
        if self.attributes is not None and 'crop_type' in self.attributes:
            return np.argmax(self.attributes['crop_type'])
        else:
            return 0

    def get_score(self):
        return self.score
    
    def w(self):
        return max(self.x2 - self.x1, 0)

    def h(self):
        return max(self.y2 - self.y1, 0)

    def area(self, norm_by=None):
        w = max(self.x2 - self.x1, 0)
        h = max(self.y2 - self.y1, 0)
        area = h * w
        if norm_by:
            h_norm, w_norm = norm_by
            area /= (h_norm * w_norm)
        return area

    def box(self):
        return np.asarray([[self.x1, self.y1], [self.x1 , self.y2],
                           [self.x2, self.y1], [self.x2 , self.y2]])

    @staticmethod
    def block_with_box(xys):
        '''Block array of xys with box'''
        bx = (xys[:,0].min(), xys[:,1].min(), xys[:,0].max(), xys[:,1].max())
        return BoundingBox2D(bx)
            
    def centroid(self, norm_by=None):
        xm = (self.x2 + self.x1) / 2
        ym = (self.y2 + self.y1) / 2
        centroid = np.array([xm ,ym])
        if norm_by:
            h_norm, w_norm = norm_by
            centroid /= np.array([w_norm, h_norm])
        return centroid

    def renormalize(self, h, w):
        x1 = max(0, self.x1 * w)
        x2 = min(w-1, self.x2 * w)
        y1 = max(0, self.y1 * h)
        y2 = min(h-1, self.y2 * h)
        return BoundingBox2D((x1, y1, x2, y2, self.score), classes=self.classes, attributes=self.attributes)

    def scale(self, s):
        x1 = self.x1 * s
        x2 = self.x2 * s
        y1 = self.y1 * s
        y2 = self.y2 * s
        return BoundingBox2D((x1, y1, x2, y2, self.score), classes=self.classes, attributes=self.attributes)

    def normalize(self, h, w):
        x1 = np.clip(self.x1 / w, 0, 1)
        x2 = np.clip(self.x2 / w, 0, 1)
        y1 = np.clip(self.y1 / h, 0, 1)
        y2 = np.clip(self.y2 / h, 0, 1)
        return BoundingBox2D((x1, y1, x2, y2, self.score), classes=self.classes, attributes=self.attributes)
    
    def crop(self, img, rotated=False):
        if rotated:
            h, w  = img.shape[:2]
            x1 = max(int(np.round(self.y1)), 0)
            x2 = int(np.round(self.y2))
            y1 = max(int(np.round(h - self.x2)), 0)
            y2 = int(np.round(h - self.x1))
            return cv2.rotate(img[y1:y2, x1:x2],cv2.ROTATE_90_CLOCKWISE)
        else:
            x1 = max(int(np.round(self.x1)), 0)
            x2 = int(np.round(self.x2))
            y1 = max(int(np.round(self.y1)), 0)
            y2 = int(np.round(self.y2))
            return img[y1:y2, x1:x2]
    
    def get_top_attr(self, attr, attr_dict):
        if self.attributes is not None and attr in self.attributes:
            ret_info = dict(name=attr_dict[attr][np.argmax(self.attributes[attr])],
                            conf = float(self.attributes[attr].max()))
            return ret_info
        else:
            return None

    def get_attr_conf(self, attr, attr_dict):
        if self.attributes is not None and attr in self.attributes:
            attr_ret = dict()
            for attr_idx, attr_name in enumerate(attr_dict[attr]):
                attr_conf = float(self.attributes[attr][attr_idx])
                attr_ret[attr_name] = attr_conf
            return attr_ret
        else:
            return dict()
    
    def add_connected_box(self, box):
        self.connected_boxes.append(box)

class RotatedBoundingBox2D(object):
    def __init__(self, box, classes=None, attributes=None):
        '''
        Rotated bounding box paramertrized by:
        (xc, yc) - center of rectangle
        (height, width) - height & width of rectangle
        angle - rotation [Rad] of rectangle w.r.t axis aligned (-pi/4,-pi/4)
        '''
        self.score = -1
        if isinstance(box, dict):
            self.xc = box['xc']
            self.yc = box['yc']
            self.width = box['width']
            self.height = box['height']
            self.angle = box['angle']
        elif isinstance(box, (list, tuple, np.ndarray)): 
            self.xc, self.yc, self.width, self.height , self.angle = box[:5]
            if len(box)==6:
                self.score = box[5]
        else:
            raise NameError
        self.classes = classes
        self.attributes = attributes
        if abs(self.angle) > 45:
            tmp = self.width; self.width = self.height; self.height = tmp
            self.angle = -(90. - abs(self.angle)) * np.sign(self.angle)
        # Backward compatability with BoundingBox2D
        axis_box = self.to_axis_box()
        self.x1 = axis_box.x1
        self.y1 = axis_box.y1
        self.connected_boxes = []
    
    def __repr__(self):
        return 'RotatedBoundingBox2D: (xc=%.2f, yc=%.2f, w=%.2f, h=%.2f, angle[deg]=%.2f score=%.2f)'\
             % (self.xc, self.yc, self.height, self.width, self.angle * (180./np.pi), self.score)

    def get_label(self):
        if self.attributes is not None and 'crop_type' in self.attributes:
            return np.argmax(self.attributes['crop_type'])
        else:
            return 0

    def get_score(self):
        return self.score
    
    def w(self):
        return max(self.width, 0)

    def h(self):
        return max(self.height, 0)
    
    def points(self):
        s = np.sin(self.angle)
        c = np.cos(self.angle)
        xs = np.array([-self.width/2, self.width/2, self.width/2, -self.width/2])
        ys = np.array([-self.height/2, -self.height/2, self.height/2, self.height/2])
        xr = xs * c - ys * s
        yr = xs * s + ys * c
        points = np.stack((xr + self.xc, yr + self.yc)).T
        return points

    def renormalize(self, h, w):
        # TODO - test if valid
        xc = self.xc * w
        yc = self.yc * h
        s = np.sin(self.angle)
        c = np.cos(self.angle)
        width = np.linalg.norm([w * self.width * c, h * self.width * s])
        height = np.linalg.norm([w * self.height * s, h * self.height * c])
        return RotatedBoundingBox2D((xc, yc, width, height, self.angle, self.score), classes=self.classes, attributes=self.attributes)
    
    def to_axis_box(self):
        box_points = self.points()
        x1, y1 = box_points.min(axis=0)
        x2, y2 = box_points.max(axis=0)
        return BoundingBox2D(np.array([x1, y1, x2, y2]))

    def warp(self, img, keep_ratio=True):
        src_pts = np.array(self.points(), dtype="float32")
        h, w = img.shape[:2]
        if keep_ratio:
            l1 = np.linalg.norm(src_pts[0] - src_pts[1])
            l2 = np.linalg.norm(src_pts[1] - src_pts[2])
            l3 = np.linalg.norm(src_pts[2] - src_pts[3])
            l4 = np.linalg.norm(src_pts[3] - src_pts[0])
            d1 = (l1 + l3)/2
            d2 = (l2 + l4)/2
            if d1 < d2:
                w = int(d2)
            else:
                h = int(d1)
        dst_pts = np.array([[0, 0],
                            [w-1, 0],
                            [w-1, h-1],
                            [0, h-1]], dtype="float32")

        # the perspective transformation matrix
        M = cv2.getPerspectiveTransform(src_pts, dst_pts)

        # directly warp the rotated rectangle to get the straightened rectangle
        warped = cv2.warpPerspective(img, M, (w, h))
        return warped

    @staticmethod
    def iou(box1, box2):
        # Approximate RIou metric: https://arxiv.org/pdf/1711.09405.pdf
        bx1_aligned = BoundingBox2D((box1.xc - box1.width/2, box1.yc - box1.height/2, box1.xc + box1.width/2, box1.yc + box1.height/2))
        bx2_aligned = BoundingBox2D((box2.xc - box2.width/2, box2.yc - box2.height/2, box2.xc + box2.width/2, box2.yc + box2.height/2))
        iou_aligned = BoundingBox2D.iou(bx1_aligned, bx2_aligned)
        angle_cos_diff = np.cos(box1.angle - box2.angle)
        return  iou_aligned * angle_cos_diff

    @staticmethod
    def iou_exact(box1, box2):
        import shapely
        from shapely.geometry import Polygon
        p1 = Polygon(box1.points())
        p2 = Polygon(box2.points())
        return p1.intersection(p2).area / (p1.union(p2).area + 1e-9)

    @staticmethod
    def from_poly(poly):
        if isinstance(poly[0], dict):
            poly = np.array([[p['x'], p['y']] for p in poly], dtype=np.float32)
        rect = cv2.minAreaRect(poly)
        (xc, yc), (w, h), angle = rect
        # treat rect as transposed and rotated (90-angle) to the opposite direction
        if abs(angle) > 45:
            tmp = w; w = h; h = tmp
            angle = -(90. - abs(angle)) * np.sign(angle)
        angle_rad = angle * (np.pi / 180.)
        return RotatedBoundingBox2D([xc, yc, w, h, angle_rad])

    def add_connected_box(self, box):
        self.connected_boxes.append(box)