class CircularBuffer(object):
    def __init__(self, history_size, debug=False):
        self.history_size = history_size
        self.buffer = [None] * history_size
        self.idx_map = dict()
        self.next_idx = 0
        self.debug = debug
    
    def push(self, data, frame):
        # Remove last value from mapping
        if self.buffer[self.next_idx] is not None:
            removed_frame = self.buffer[self.next_idx]['frame']
            if removed_frame in self.idx_map:
                del self.idx_map[removed_frame]
        self.buffer[self.next_idx] = dict(frame=frame, data=data)
        self.idx_map[frame] = self.next_idx
        # Increment next idx
        self.next_idx = (self.next_idx + 1) % self.history_size
        if self.debug:
            self.log()

    def append(self, key, val, frame):
        if frame not in self.idx_map:
            return
        query_idx = self.idx_map[frame]
        query = self.buffer[query_idx]
        assert query['frame']==frame
        query['data'][key] = val

    def get(self, frame):
        if frame not in self.idx_map:
            return None
        query_idx = self.idx_map[frame]
        query = self.buffer[query_idx]
        assert query['frame']==frame
        return query['data']
    
    def size(self):
        return len(self.idx_map)
    
    def reset(self):
        self.buffer = [None] * self.history_size
        self.idx_map = dict()
        self.next_idx = 0
    
    def get_last(self):
        last_inserted_idx = self.next_idx - 1
        if last_inserted_idx < 0:
            last_inserted_idx = self.history_size - 1
        if self.buffer[last_inserted_idx] is not None:
            last_frame = self.buffer[last_inserted_idx]['frame']
            if last_frame in self.idx_map:
                return self.buffer[last_inserted_idx]['data']
        return None
    
    def get_range(self, first, last):
        ret_list = []
        for frame_idx in range(first, last+1):
            if frame_idx in self.idx_map:
                query_idx = self.idx_map[frame_idx]
                info = self.buffer[query_idx]
                assert info['frame']==frame_idx
                ret_list.append(info['data'])
            else:
                ret_list.append(None)
        return ret_list
    
    def log(self):
        print('Next idx=%d' % self.next_idx, flush=True)
        print('Buffer=', self.buffer, flush=True)