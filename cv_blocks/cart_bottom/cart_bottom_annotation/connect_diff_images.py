import os
import argparse
import re
import numpy as np
import cv2
from tqdm import tqdm
import xml.etree.ElementTree as ET
from cv_blocks.common.types import BoundingBox2D
from cv_blocks.visualizers.cart_bottom_visualizer import draw_boxes
import shutil

def merge_image(img1, img2):
    (h, wx2) = img1.shape[:2]
    w = wx2 // 2
    img_l = img1[:,:w]
    img_r = img2[:,w:]
    return np.hstack((img_l, img_r))

def is_valid_pair(img1, img2, th=2):
    (h, wx2) = img1.shape[:2]
    w = wx2 // 2
    img1_r = img1[:,w:]
    img2_l = img2[:,:w]
    valid = np.allclose(img1_r.astype(np.float32), img2_l.astype(np.float32), rtol=1e-2, atol=2)
    dist = np.percentile(np.abs(img1_r.astype(np.float32) - img2_l.astype(np.float32)), 99)
    # viz = np.hstack((img1_r, img2_l))
    # cv2.putText(viz,
    #         'dist = %.2f, valid=%d' % (dist, valid),
    #         (10, 30),
    #         cv2.FONT_HERSHEY_SIMPLEX,
    #         1,
    #         (255,0,0), 1)
    # cv2.imshow('dd',viz)
    # cv2.waitKey()
    return dist<th

def merge_xmls(xml1_path, xml2_path, xml_out_path):
    tree1 = ET.parse(xml1_path)
    tree2 = ET.parse(xml2_path)

    obj1_ptr = tree1.find('object')
    obj2_ptr = tree2.find('object')
    if obj1_ptr and obj2_ptr:
        obj2_ptr.extend(obj1_ptr)
        # tree2.getroot().insert(-1,obj1_ptr)
        tree2.write(open(xml_out_path, 'wb'))
    elif obj1_ptr:
        tree1.write(open(xml_out_path, 'wb'))
    else:
        tree2.write(open(xml_out_path, 'wb'))

def get_gt_bboxes(gt_path):
    bboxes = []
    if gt_path is None:
        return bboxes
    tree = ET.parse(gt_path)
    for e in tree.iter():
        if e.tag=='annotation':
            for ee in e.iter():
                if ee.tag=='object':
                    for eee in ee:
                        if 'bndbox' in eee.tag:
                            bbox = dict()
                            for dim in list(eee):
                                if 'xmin' in dim.tag:
                                    bbox['xmin'] = int(round(float(dim.text)))
                                if 'ymin' in dim.tag:
                                    bbox['ymin'] = int(round(float(dim.text)))
                                if 'xmax' in dim.tag:
                                    bbox['xmax'] = int(round(float(dim.text)))
                                if 'ymax' in dim.tag:
                                    bbox['ymax'] = int(round(float(dim.text)))
                            bboxes.append(BoundingBox2D((bbox['xmin'], bbox['ymin'], bbox['xmax'], bbox['ymax'])))
    return bboxes

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='generate cart bottom images for annotation from dump images')
    parser.add_argument('--input-dir', help='input directory of dump images',type=str, required=True)
    parser.add_argument('--output-dir', help='input directory of dump images',type=str, default=None)
    parser.add_argument('--valid-postfix', help='postfix for valid image type',type=str, default='_qvga.,_b_qvga.')
    args = parser.parse_args()
    valid_postfix_list = args.valid_postfix.split(',')

    idx_dist = 1
    # Load images list
    tmp_xml_file = '/tmp/tmp_annotation.xml'
    images_dir = os.path.join(args.input_dir, 'Images')
    annotations_dir = os.path.join(args.input_dir, 'Annotations')
    output_images_dir = os.path.join(args.output_dir, 'Images')
    output_annotations_dir = os.path.join(args.output_dir, 'Annotations')
    os.makedirs(output_images_dir, exist_ok=True)
    os.makedirs(output_annotations_dir, exist_ok=True)
    assert os.path.exists(images_dir) and os.path.exists(annotations_dir)
    for postfix in valid_postfix_list:
        output_idx = 0
        pattern =  '[a-zA-z]+_[0-9]+%s*' % postfix 
        images_list = sorted([f for f in os.listdir(images_dir) if re.search(pattern, f)])
        # images_list = sorted(os.listdir(images_dir))

        for img_idx in tqdm(range(len(images_list[:-idx_dist]))):
            img1_path = os.path.join(images_dir, images_list[img_idx])
            img2_path = os.path.join(images_dir, images_list[img_idx+1])
            img1_fid = int(re.findall(r"\D(\d{5})\D", os.path.basename(img1_path))[0])
            img2_fid = int(re.findall(r"\D(\d{5})\D", os.path.basename(img2_path))[0])
            print('Pairing %s, %s' %(img1_path, img2_path))
            ref_image = cv2.imread(img1_path)
            dst_image = cv2.imread(img2_path)
            if (img2_fid - img1_fid == 1) and is_valid_pair(ref_image, dst_image):
                xml_out_path = os.path.join(output_annotations_dir, 'img_%05d%sxml' % (output_idx, postfix))
                img_out_path = os.path.join(output_images_dir, 'img_%05d%sjpg' % (output_idx, postfix))
                xml1_path = img1_path.replace('Images/', 'Annotations/').replace('jpg', 'xml')
                xml2_path = img2_path.replace('Images/', 'Annotations/').replace('jpg', 'xml')
                assert os.path.exists(xml1_path) and os.path.exists(xml2_path)
                merge_xmls(xml1_path, xml2_path, tmp_xml_file)
                merged_boxes = get_gt_bboxes(tmp_xml_file)
                # Visualize new sample
                merged_image = merge_image(ref_image, dst_image)
                img_vis = draw_boxes(merged_image.copy(), merged_boxes, 
                                    ids=['o'], 
                                    override_ids=range(len(merged_boxes)))
                viz_h = img_vis.shape[0] * 2
                viz_w = img_vis.shape[1] * 2
                img_vis = cv2.resize(img_vis, (viz_w, viz_h))
                cv2.imshow('visualize', img_vis)
                key = cv2.waitKey() & 0xFF
                if key==ord('y'):
                    print('Saving new sample: %s' % img_out_path)
                    cv2.imwrite(img_out_path, merged_image)
                    shutil.copy(tmp_xml_file, xml_out_path)
                    output_idx +=1
                elif key==ord('n'):
                    print('Skipping sample')


