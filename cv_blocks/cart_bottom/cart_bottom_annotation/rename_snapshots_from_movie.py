import os
import shutil
import argparse

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='rename cart bottom images from movies so they have a unique name')
    parser.add_argument('--input-dir', help='input directory of dump images',type=str, required=True)
    args = parser.parse_args()

    for root, dirs, files in os.walk(args.input_dir):
        prefix = os.path.basename(root)
        for file in files:
            if '.jpg' in file.lower():
                new_file_name = prefix + file
                src_path = os.path.join(root, file)
                dst_path = os.path.join(root, new_file_name)
                shutil.move(src_path, dst_path)