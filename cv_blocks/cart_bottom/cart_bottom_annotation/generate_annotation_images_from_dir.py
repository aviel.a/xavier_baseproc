import os
import argparse
import re
import numpy as np
import cv2

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='generate cart bottom images for annotation from dump images')
    parser.add_argument('--input-dir', help='input directory of dump images',type=str, required=True)
    parser.add_argument('--output-dir', help='input directory of dump images',type=str, default=None)
    parser.add_argument('--valid-postfix', help='postfix for valid image type',type=str, default='_qvga.,_b_qvga.')

    args = parser.parse_args()
    valid_postfix_list = args.valid_postfix.split(',')
    assert os.path.isdir(args.input_dir)
    if args.output_dir is None:
        args.output_dir = os.path.join(args.input_dir, 'imgs_for_annotation')
    list_subdirs = [d for d in os.listdir(args.input_dir) if os.path.isdir(os.path.join(args.input_dir, d)) \
        and d!='imgs_for_annotation']
    os.makedirs(args.output_dir, exist_ok=True)

    for subdir in list_subdirs:
        list_files = os.listdir(os.path.join(args.input_dir, subdir))
        for postfix in valid_postfix_list:
            pattern =  '[a-zA-z]+_[0-9]+%s*' % postfix 
            list_subtype_files = sorted([f for f in list_files if re.search(pattern, f)])
            print('Generating data for %s, %s: %d images' % (subdir, postfix, len(list_subtype_files)))
            postfix_no_point = postfix[:-1]
            # Genreate samples
            for img_idx in range(len(list_subtype_files) - 1):
                img_before_path = os.path.join(args.input_dir, subdir, list_subtype_files[img_idx])
                img_after_path = os.path.join(args.input_dir, subdir, list_subtype_files[img_idx+1])
                img_before = cv2.imread(img_before_path, -1)
                img_after = cv2.imread(img_after_path, -1)
                stacked_img = np.hstack((img_before, img_after))
                out_img_path = os.path.join(args.output_dir, '%s_img_%05d%s.jpg' % (subdir, img_idx, postfix_no_point))
                cv2.imwrite(out_img_path, stacked_img)