import os
import argparse
import re
import numpy as np
import cv2
import json
import getpass
from cv_blocks.video_reader.base_proc import BaseImageGrabber
from multiprocessing import Process
from cv_blocks.calibration.cart_calib import CartCalib
import pandas as pd
import streams.stereo.vx._main_stereo_matching as msm
import Exceptions

def locate_movie(movie_name, root_dir):
    optional_dirs = [root_dir]
    for optional_dir in optional_dirs:
        for root, dirs, files in os.walk(optional_dir):
            for file in sorted(files):
                path = os.path.join(root, movie_name)
                if os.path.isfile(path) or os.path.isdir(path):
                    return root

DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
if __name__=='__main__':
    parser = argparse.ArgumentParser(description='generate event trigger images')
    parser.add_argument('--movies-file', help='input video',type=str, required=True)
    parser.add_argument('--output-dir', help='input directory of dump images',type=str, required=True)
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    parser.add_argument('--weights', help='path to classifier weights',type=str, default=None)
    parser.add_argument('--step', help='step between images',type=int, default=4)
    parser.add_argument('--grayscale', help='use grayscale images', action='store_true')
    parser.add_argument('--frames-stacked', help='number of frames per training image', type=int, default=1)
    parser.add_argument('--skip-prediction', help='skip prediction part', action='store_true')
    parser.add_argument('--not-sure-only', help='skip prediction part', action='store_true')

    args = parser.parse_args()

    use_classifier = args.weights is not None
    if use_classifier:
        from cv_blocks.cart_bottom.event_detector.event_detector import EventDetector
        my_cls = EventDetector(weights=args.weights)
    predict = not args.skip_prediction

    frames_stacked = args.frames_stacked
    is_gray = args.grayscale
    not_sure_only = args.not_sure_only
    movie_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()
    for single_movie in movie_list:
        movie_rec = locate_movie(single_movie, args.root_dir)
        full_rec_path = os.path.join(movie_rec, single_movie)
        movie_name_wo_suffix = full_rec_path.split('_L.avi')[0]
        movie_alias = os.path.basename(movie_name_wo_suffix)
        full_output_dir = os.path.join(args.output_dir, movie_alias)
        if args.skip_prediction:
            os.makedirs(os.path.join(full_output_dir, 'all'), exist_ok=True)
        elif use_classifier:
            os.makedirs(os.path.join(full_output_dir, 'PredictedEvent'), exist_ok=True)
            os.makedirs(os.path.join(full_output_dir, 'PredictedNoEvent'), exist_ok=True)
            os.makedirs(os.path.join(full_output_dir, 'PredictedNotSure'), exist_ok=True)
        else:
            os.makedirs(os.path.join(full_output_dir, 'Event'), exist_ok=True)
            os.makedirs(os.path.join(full_output_dir, 'NoEvent'), exist_ok=True)
        wait_time = 10
        try:
            stream = BaseImageGrabber('playback', movie_name_wo_suffix, target_ms=wait_time)
            proc = Process(target=stream.run)
            proc.start()

            prop_file = movie_name_wo_suffix + '_prop.json'
            frame_idx = 0
            with open(prop_file) as f:
                camera_id = json.load(f)['camera_id']
            # Load config data
            cart_calib = CartCalib(camera_id)
            vx_prcss = msm.init(640, 480, 640, 480, cart_calib.calib_dir, False)
            spatial_th = np.zeros((320, 240),dtype=np.uint8)
            past_frames = dict(left=[], back=[])

            while True:
                img_data, _, _ = stream.grab(can_skip=False)
                if frames_stacked==1 and frame_idx % args.step != 0:
                    frame_idx += 1
                    continue
                frame_L = img_data['data']['left']['img'].copy()
                frame_R = img_data['data']['right']['img'].copy()
                frame_B = img_data['data']['back']['img'].copy()
                disparity_image, grayL_resized, frameL_resized, frameB_resized, cart_disp_mask, left_remap = msm.process_stream(
                        vx_prcss, 0, frame_L, frame_R, frame_B, spatial_th)[:6]

                if len(past_frames['left']) >= frames_stacked:
                    past_frames['left'].pop(0)
                    past_frames['back'].pop(0)
                if is_gray:
                    past_frames['left'].append(cv2.cvtColor(frameL_resized, cv2.COLOR_RGB2GRAY))
                    past_frames['back'].append(cv2.cvtColor(frameB_resized, cv2.COLOR_RGB2GRAY))
                else:
                    past_frames['left'].append(frameL_resized.copy())
                    past_frames['back'].append(frameB_resized.copy())

                if frame_idx % args.step != 0 or frame_idx < frames_stacked - 1:
                    frame_idx += 1
                    continue

                image_L = past_frames['left'][-1]
                image_B = past_frames['back'][-1]
                if frames_stacked>1:
                    for i in range(2,frames_stacked+1):
                        image_L = np.hstack((image_L, past_frames['left'][-i]))
                        image_B = np.hstack((image_B, past_frames['back'][-i]))

                # Display images
                # ==============
                viz_img = np.hstack((image_L, image_B))
                if predict:
                    if not use_classifier:
                        cv2.imshow('visualize', viz_img)
                        key = cv2.waitKey() & 0xFF
                        if key==ord('p'):
                            print('Saving positive sample')
                            img_path = os.path.join(full_output_dir, 'Event', '%s_img_front_%05d.jpg' % (movie_alias, frame_idx))
                            cv2.imwrite(img_path, image_L)
                            img_path = os.path.join(full_output_dir, 'Event', '%s_img_back_%05d.jpg' % (movie_alias, frame_idx))
                            cv2.imwrite(img_path, image_B)
                        elif key==ord('n'):
                            print('Saving negative sample')
                            img_path = os.path.join(full_output_dir, 'NoEvent', '%s_img_front_%05d.jpg' % (movie_alias, frame_idx))
                            cv2.imwrite(img_path, image_L)
                            img_path = os.path.join(full_output_dir, 'NoEvent', '%s_img_back_%05d.jpg' % (movie_alias, frame_idx))
                            cv2.imwrite(img_path, image_B)
                        elif key==ord('s'):
                            print('Skipping sample')
                        elif key==ord('q'):
                            break
                    else:
                        # front img prediction
                        p_event = my_cls.predict(np.dstack(past_frames['left']))
                        pred_dir = 'PredictedEvent' if p_event > 0.8 else 'PredictedNoEvent' if p_event < 0.2 else 'PredictedNotSure'
                        if not not_sure_only or pred_dir == 'PredictedNotSure':
                            img_path = os.path.join(full_output_dir, pred_dir, '%s_img_front_%05d.jpg' % (movie_alias, frame_idx))
                            cv2.imwrite(img_path, image_L)
                        # back img prediction
                        p_event = my_cls.predict(np.dstack(past_frames['back']))
                        pred_dir = 'PredictedEvent' if p_event > 0.8 else 'PredictedNoEvent' if p_event < 0.2 else 'PredictedNotSure'
                        if not not_sure_only or pred_dir == 'PredictedNotSure':
                            img_path = os.path.join(full_output_dir, pred_dir, '%s_img_back_%05d.jpg' % (movie_alias, frame_idx))
                            cv2.imwrite(img_path, image_B)
                else:
                    pred_dir = 'all'
                    img_path = os.path.join(full_output_dir, pred_dir,
                                            '%s_img_front_%05d.jpg' % (movie_alias, frame_idx))
                    cv2.imwrite(img_path, image_L)
                    # back img prediction
                    pred_dir = 'all'
                    img_path = os.path.join(full_output_dir, pred_dir,
                                            '%s_img_back_%05d.jpg' % (movie_alias, frame_idx))
                    cv2.imwrite(img_path, image_B)
                frame_idx +=1

        except:
            proc.terminate()
            pass # movie finished
        finally:
            pass
            # proc.terminate()