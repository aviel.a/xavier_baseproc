import os
import shutil

new_images_dir = '/home/walkout03/Desktop/event_cls_dataset'
reference_dir = '/media/walkout03/DATA/dataset/event_cls_dataset'


def locate_file(search_dir, file_name):
    for root, dirs, files in os.walk(search_dir):
        if file_name in files:
            return root
    return None

count = dict(not_used=0, event=0, no_event=0)
sub_dirs_list = sorted(os.listdir(reference_dir))
for sub_dir in sub_dirs_list:
    print('subdir processed: ', sub_dir)
    for event_type in os.listdir(os.path.join(reference_dir,sub_dir)):
        for file_name in os.listdir(os.path.join(reference_dir,sub_dir, event_type)):
            movie_name = file_name.split('_img_')[0]
            if os.path.exists(os.path.join(new_images_dir, movie_name)):
                found_location = locate_file(os.path.join(new_images_dir, movie_name), file_name)
            else:
                found_location = None
            if found_location is None:
                count['not_used'] += 1
                continue
            save_dir = os.path.join(new_images_dir, sub_dir, event_type)
            os.makedirs(save_dir, exist_ok=True)
            shutil.move(os.path.join(found_location, file_name), os.path.join(save_dir, file_name))
            if event_type=='Event':
                count['event'] += 1
            else:
                count['no_event'] += 1

#clean unused images
for sub_dir in os.listdir(new_images_dir):
    if sub_dir not in sub_dirs_list:
        shutil.rmtree(os.path.join(new_images_dir, sub_dir))

print("Total event images: {}".format(count['event']))
print("Total no_event images: {}".format(count['no_event']))
print("Total not_used images: {}".format(count['not_used']))