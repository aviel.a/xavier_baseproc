import numpy as np
import cv2
import enum
from cv_blocks.cart_bottom.static_snapshot_detector import SDState
import os
import yaml
from cv_blocks.misc import aws_download
from config import config
try:
    cmd = 'import tensorrt_lib.bin._swig_yaeen as swig_cls'
    exec(cmd)
except:
    raise NameError('missing swig_yaeen compilation for given arch,FP,training data')

class EventDetectorLogic(object):
    def __init__(self):
        self.th_low = 0.2
        self.th_med = 0.5
        self.th_high = 0.8
        self.th_very_high = 0.95 # fast trigger
        self.same_pred_req_start = 2
        self.same_pred_req_end = 5
        self.during_event_counter = 0
        self.not_during_event_counter = 0
        self.state = SDState.StaticNoTrigger
        self.fd_th_high_motion = 8
        self.minimum_frames_in_event = 15
        self.frames_since_event_started = 0

    def is_static(self, p_event, fd_metric):
        self.state = self.step(p_event, fd_metric)
        return self.state

    def step(self, p_event, fd_metric):
        next_state = self.state
        if self.state==SDState.StaticNoTrigger:
            self.frames_since_event_started = 0
            next_state = SDState.StaticNoTrigger if p_event < self.th_high else SDState.DynamicTrigger if p_event > self.th_very_high else SDState.DynamicNoTrigger
            if next_state==SDState.DynamicNoTrigger:
                self.during_event_counter = 1 # already 1 time high prob
            if next_state==SDState.DynamicTrigger:
                self.not_during_event_counter = 0
        elif self.state==SDState.DynamicNoTrigger:
            # TODO: what happens if only one high prediction was seen?
            if p_event > self.th_high:
                self.during_event_counter += 1
                next_state = SDState.DynamicNoTrigger if self.during_event_counter < self.same_pred_req_start else SDState.DynamicTrigger
            else:
                next_state = SDState.StaticNoTrigger
            if next_state==SDState.DynamicTrigger:
                self.not_during_event_counter = 0
        elif self.state==SDState.DynamicTrigger:
            high_motion_in_cart = fd_metric > self.fd_th_high_motion
            enough_frames_past = self.frames_since_event_started - self.minimum_frames_in_event >= 0
            if p_event < self.th_med:
                self.not_during_event_counter += 1
            else:
                self.not_during_event_counter = 0
            next_state = SDState.DynamicTrigger if self.not_during_event_counter < self.same_pred_req_end else \
                SDState.StaticPendingTrigger if (high_motion_in_cart or not enough_frames_past) else SDState.StaticTrigger
        elif self.state==SDState.StaticPendingTrigger:
            high_motion_in_cart = fd_metric > self.fd_th_high_motion
            enough_frames_past = self.frames_since_event_started - self.minimum_frames_in_event >= 0
            next_state = SDState.DynamicTrigger if p_event > self.th_high else SDState.StaticPendingTrigger if \
                (high_motion_in_cart or not enough_frames_past) else SDState.StaticTrigger
            if next_state==SDState.DynamicTrigger:
                self.not_during_event_counter = 0
        elif self.state==SDState.StaticTrigger:
            next_state = SDState.DynamicNoTrigger if p_event > self.th_high else SDState.StaticNoTrigger
            if next_state==SDState.DynamicTrigger:
                self.not_during_event_counter = 0
        self.frames_since_event_started += 1
        return next_state
    
    def reset(self):
        self.state = SDState.StaticNoTrigger

class ECState(enum.Enum):
    DuringEvent = 0
    NoEvent = 1

class EventDetector(object):
    '''
    A class implementing event/no event classifier
    '''
    def __init__(self, weights=None, img_size=(64,64), use_fp16=True, n_classes=2, batch_size=2, debug=False, sync=True):
        if weights is None:
            weights = config.DEFAULT_EVENT_DETECTOR_WEIGHTS
        self.weights = weights
        print('initializing Event Classifier')
        self.use_fp16 = use_fp16
        self.batch_size = batch_size
        self.img_size = img_size
        self.load_detector(weights, sync=sync)
        self.n_classes = n_classes
        self.debug = debug

    def load_detector(self, weights, sync):
        weights, conf_file = self.get_model(weights, sync=sync)
        with open(conf_file) as config_f:
            config = yaml.safe_load(config_f)
        if 'img_size' in config:
            self.img_size = tuple(config['img_size'])
        if 'class_names' in config:
            self.n_classes = len(config['class_names'])
        if 'hist_eq' in config:
            self.hist_eq = config['hist_eq']
        else:
            self.hist_eq = False
        self.input_channels = config.get('input_channels', 3)
        self.frames_stacked = config.get('frames_stacked', 1)
        self.is_gray = self.input_channels / self.frames_stacked == 1

        print('initializing Event Classifier')
        self.engine = swig_cls.loadModelAndCreateEngine(weights, self.use_fp16, self.batch_size)
        print('loading context')
        self.context = swig_cls.create_context(self.engine)
        print('finished loading context')

    def get_model(self, weights, sync):
        model_root_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'models')
        model_name, model_ext = os.path.splitext(weights)
        assert model_ext in ('.pth', '.onnx')
        full_weights_path = os.path.join(model_root_dir, model_name, weights)
        full_conf_path = full_weights_path.replace(model_ext, '_config.yaml')
        if not os.path.exists(full_weights_path) and sync:
            full_model_dir = os.path.dirname(full_weights_path)
            os.makedirs(full_model_dir, exist_ok=True)
            # Get model & config file
            aws_download.download("models/event_detector", model_name, full_model_dir)
        
        assert os.path.exists(full_weights_path) and os.path.exists(full_conf_path)
        return full_weights_path, full_conf_path
    
    def equalize_hist(self, image):
        image[:,:,0] = cv2.equalizeHist(image[:,:,0])
        image[:,:,1] = cv2.equalizeHist(image[:,:,1])
        image[:,:,2] = cv2.equalizeHist(image[:,:,2])
        return image

    def predict(self,img):
        if not isinstance(img, (list, tuple)):
            img = [img]
        bs = len(img)
        img_re = [cv2.resize(i, self.img_size) if i.shape!=self.img_size else i for i in img]
        if self.hist_eq:
            img_re = [self.equalize_hist(i) for i in img_re]
        assert bs <= self.batch_size
        img = np.stack(img_re)
        img = img.astype(np.float32) / 255.
        img = np.transpose(img[...,::-1], (0, 3, 1, 2))
        data = img.ravel()
        probs = np.zeros(self.n_classes * bs, np.float32)
        swig_cls.execute(self.engine,self.context,data,probs, self.batch_size)
        probs = np.reshape(probs, (bs, -1))
        # print(probs[:, ECState.DuringEvent.value])
        p_event = probs[:, ECState.DuringEvent.value]
        if self.debug:
            self.visualize_input(img_re, p_event)
        return p_event
    
    def visualize_input(self, img_re, p_event):
        if self.frames_stacked > 1:
            input_viz = []
            n_images = img_re[0].shape[-1]
            for cam_input, prob in zip(img_re, p_event):
                single_cam_viz = np.hstack([cv2.resize(cam_input[:,:,img_idx], (256,256)) for img_idx in range(n_images)])
                single_cam_viz = cv2.cvtColor(single_cam_viz, cv2.COLOR_GRAY2BGR)
                cv2.putText(single_cam_viz, 'p=%.2f' % prob, (5, 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255,0,0))
                input_viz.append(single_cam_viz)
                
            input_viz = np.vstack(input_viz)
        else:
            raise NotImplementedError
        cv2.imshow("Event Detector input", input_viz)

if __name__=='__main__':
    weights = '/home/walkout05/git_repo/walkout/cv_blocks/cart_bottom/event_cls/models/sanity_model/event_trigger_00007.onnx'
    img_path = '/home/walkout05/walkout_dataset/event_cls_data/RG3p05_2020-01-01-13_28_49/NoEvent/RG3p05_2020-01-01-13_28_49_img_back_00000.jpg'
    img = cv2.imread(img_path)
    my_event_cls = EventDetector(weights=weights)
    my_event_cls.predict(img)