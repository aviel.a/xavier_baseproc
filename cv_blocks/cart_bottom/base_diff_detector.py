import cv2
import numpy as np
import enum
import os
import yaml
from detector.yolo.utils import  draw_boxes
from cv_blocks.common.types import BoundingBox2D
from .static_snapshot_detector import StaticSnapshotDetector, SDState
from .event_detector.event_detector import EventDetectorLogic
from config import config

PROD_IN_CLS = 0
PROD_OUT_CLS = 1


class BaseDiffDetector(object):
    '''
    This class implements single cam cart bottom module
    '''
    class Config(object):
        def __init__(self, config, cam_name):
            assert isinstance(config, dict)
            self.take_snapshot_every_n_frames = config.get('take_snapshot_every_n_frames', 15)
            self.min_roi_th = config.get('min_roi_th', 0.8)
            self.min_roi_th_pile = config.get('min_roi_th_pile', 0.6)
            self.min_diff_ratio = config.get('min_diff_ratio', 0.25)
            self.max_num_diffs = config.get('max_num_diffs', 1)
            self.detector_conf_th = config.get('detector_conf_th', 0.4)
            self.fd_box_th = config.get('fd_box_th', 1.5)
            self.direction_conf_ratio = config.get('direction_conf_ratio', 1000.0)
            self.enable_diff_mask = config.get('enable_diff_mask', False)
            self.event_detector_dim = config.get('event_detector_dim', 128)
            self.debug = config.get('debug', False)

    def set_default_roi(self, cam_name, camera_config):
        if cam_name == 'front':
            ymin = camera_config['decision_lines']['top']
            return BoundingBox2D((0, int(ymin * 320), 240, 320))
        ymin = camera_config['decision_lines']['middle']
        return BoundingBox2D((0, int(ymin * 320), 240, 320))

    def __init__(self, weights, cam_name, snapshot_method='event_detector', method='img_diff', bfd_config=dict()):
        self.cam_name = cam_name
        self.ref_img = None
        self.cur_img = None
        wnoise_img_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'data/images/wnoise_img.png')
        self.wnoise_img = cv2.imread(wnoise_img_path)
        self.params = self.Config(bfd_config, self.cam_name)
        if weights is not None:
            self.detector = self.load_detector(weights)
        self.method = method
        if method == 'bg_sub':
            self.bg_sub = cv2.createBackgroundSubtractorMOG2()
        assert snapshot_method in ('fd_based', 'event_detector')
        if snapshot_method=='fd_based':
            self.ssd = StaticSnapshotDetector(cam_name)
        elif snapshot_method=='event_detector':
            self.ssd = EventDetectorLogic()
        self.snapshot_method = snapshot_method
    
    def load_camera_config(self, camera_config):
        self.params.roi = self.set_default_roi(self.cam_name, camera_config)

    def load_detector(self, weights):
        conf_file = os.path.splitext(weights)[0] + '_config.yaml'
        assert os.path.exists(conf_file)
        with open(conf_file) as config_f:
            config = yaml.safe_load(config_f)
        if weights.endswith('.pth'):
            from cv_blocks.object_detector.yolo_detector.torch_impl.yolo import YoloObjectDetector
        elif weights.endswith('.onnx'):
            from cv_blocks.object_detector.yolo_detector.trt_impl.yolo import YoloObjectDetector
        obj_det = YoloObjectDetector(config['backend'],
                    input_size=config['input_size'],
                    labels=config['labels'],
                    anchors=config['anchors'],
                    weights_path=weights,
                    nb_class = 2, # in & out
                    confidence_th=self.params.detector_conf_th,
                    # nms_th=args.nms_th,
                    channels=config['input_channels'])
        return obj_det
    
    def is_in_roi(self, box, pile_pred):
        if self.params.roi is None:
            return True
        ratio_in_roi = BoundingBox2D.intersection(box['box'], self.params.roi) / box['box'].area()
        if pile_pred:
            return ratio_in_roi >= self.params.min_roi_th_pile
        else:
            return ratio_in_roi >= self.params.min_roi_th
    
    def valid_diff_mask(self, box, diff_mask):
        return  box['box'].crop(diff_mask).mean() > self.params.min_diff_ratio

    def valid_frame_delta(self, box):
        if self.cur_img['fd'] is None: 
            return True
        ref_crp = box['box'].crop(self.ref_img['fd'])
        cur_crp = box['box'].crop(self.cur_img['fd'])
        metric = cur_crp.mean() if box['direction']=='in' else ref_crp.mean()
        return metric < self.params.fd_box_th

    def reset(self):
        self.ref_img = None
        self.cur_img = None
    
    def step(self, img, fd, frame_num, force_snapshot=False):
        diff = None
        if force_snapshot or self.should_take_snapshot(fd, frame_num):
            if self.params.debug:
                print('Cart Bottom Debug: [Cam: %s]:Taking snapshot in frame %d' % (self.cam_name, frame_num))
            self.take_cart_snapshot(img, frame_num, fd=fd)
            if self.is_comparison_valid():
                diff = self.compare_cart_state()
        return diff
    
    def should_take_snapshot(self, fd, img, frame_num, metric=None):
        # Check if enough time passed since last snapshot
        last_snapshot_frame_num = self.get_cart_timestamps()[-1]
        if last_snapshot_frame_num < 0 and self.cur_img is None:
            return SDState.StaticTrigger
        enough_frames_passed = frame_num - last_snapshot_frame_num >= self.params.take_snapshot_every_n_frames
        # Check if enough time cart is static
        if metric is None:
            # metric = np.percentile(self.params.roi.crop(fd), 95)
            metric = dict(fd=self.params.roi.crop(fd).mean(),
                        img=self.img_diff(img))
        if self.snapshot_method=='fd_based':
            static_scene = self.ssd.is_static(metric)
        elif self.snapshot_method=='event_detector':
            fd_metric = np.percentile(self.params.roi.crop(fd), 97)
            static_scene = self.ssd.is_static(metric, fd_metric)


        return static_scene if enough_frames_passed else SDState.StaticNoTrigger

    def img_diff(self, img):
        cur_gray = cv2.cvtColor(self.cur_img['img'], cv2.COLOR_BGR2GRAY)
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        return cv2.absdiff(cur_gray, img_gray).mean()

    def is_comparison_valid(self):
        return self.ref_img is not None and self.cur_img is not None
    
    def get_cart_images(self):
        return self.ref_img['img'], self.cur_img['img']

    def get_cart_images_high_res(self):
        return self.ref_img['img_high_res'], self.cur_img['img_high_res']
    
    def get_cart_timestamps(self):
        ref_img_ts = self.ref_img['frame_num'] if self.ref_img is not None else \
             -self.params.take_snapshot_every_n_frames
        cur_img_ts = self.cur_img['frame_num'] if self.cur_img is not None else \
             -self.params.take_snapshot_every_n_frames
        return ref_img_ts, cur_img_ts

    @staticmethod
    def diff_type(box):
        box_classes = box.classes
        op_type = 'in' if box_classes[PROD_IN_CLS] > box_classes[PROD_OUT_CLS] else 'out'
        return op_type

    def predict(self, img1, img2):
        img_h, img_w = img1.shape[:2]
        tensor_in = np.concatenate((img1[:,:,::-1], img2[:,:,::-1]), axis=-1)
        boxes = self.detector.predict(tensor_in, reverse_channels=False)
        # Output diffs
        diffs_out = []
        for bx in boxes:
            direction_conf = bx.classes.max() / (bx.classes.min() + 1e-6)
            diff_type = BaseDiffDetector.diff_type(bx)
            diff = dict(box=bx.renormalize(img_h, img_w), direction=diff_type, direction_conf=direction_conf)
            diffs_out.append(diff)
        return diffs_out

    def filter_diffs(self, diff_boxes, diff_mask, pile_pred=None):
        # Remove boxes out of ROI
        if pile_pred is None:
            pile_pred = [False] * len(diff_boxes)
        diff_boxes = [bx for bx, pp in zip(diff_boxes, pile_pred) if self.is_in_roi(bx, pp)]

        # Remove boxes with small diff overlap
        # if diff_mask is not None:
        #     diff_boxes = [bx for bx in diff_boxes if self.valid_diff_mask(bx, diff_mask)]

        # Remove Boxes with high frame delta
        # diff_boxes = [bx for bx in diff_boxes if self.valid_frame_delta(bx)]

        # Remove boxes with low direction confidence
        diff_boxes = [bx for bx in diff_boxes if bx['direction_conf'] > self.params.direction_conf_ratio]

        return diff_boxes
    
    def compare_cart_state(self):
        ref_img, cur_img = self.get_cart_images()
        # Run diff yolo diff detector
        # ===========================
        diff_boxes = self.predict(ref_img, cur_img)
        diff_mask = self.calc_diff_mask(ref_img, cur_img)

        # Filter outputs
        # ==============
        diff_boxes = self.filter_diffs(diff_boxes, diff_mask)

        if self.params.debug:
            BaseDiffDetector.visualize_diff_mask(ref_img, cur_img, diff_mask, diff_boxes=diff_boxes)
        # TODO - decide what format to return 
        diffs = self.pack_diffs(ref_img, cur_img, diff_mask, diff_boxes)
        return diffs
    
    def pack_diffs(self, ref_img, cur_img, diff_mask, diff_boxes):
        diffs = []
        h, w = ref_img.shape[:2]
        event_detector_size = self.params.event_detector_dim * self.params.event_detector_dim
        ref_high_res, cur_high_res = self.get_cart_images_high_res()
        for bx in diff_boxes:
            if ref_high_res is not None and bx['box'].area() < event_detector_size:
                img_to_crop = cur_high_res if bx['direction']=='in' else ref_high_res
                rotated = self.cam_name=='back' # TODO- move to params
                img_crop = cv2.cvtColor(bx['box'].scale(2).crop(img_to_crop, rotated=rotated), cv2.COLOR_BGR2RGB)
            else:
                img_to_crop = cur_img if bx['direction']=='in' else ref_img
                img_crop = cv2.cvtColor(bx['box'].crop(img_to_crop), cv2.COLOR_BGR2RGB)
            if diff_mask is not None:
                mask_crop = bx['box'].crop(diff_mask)
                img_masked_crop = img_crop.copy()
                wnoise_crop = bx['box'].crop(self.wnoise_img)
                img_masked_crop[mask_crop==0] = wnoise_crop[mask_crop==0]
            else:
                mask_crop = None
                img_masked_crop = None
            diffs.append(dict(direction=bx['direction'], box=bx['box'], img=img_crop, mask=mask_crop, img_masked=img_masked_crop,
                frames=self.get_cart_timestamps()))
        return diffs
    
    @staticmethod
    def visualize_diff_mask(ref_img, cur_img, diff_mask, diff_boxes=None, mask_w = 0.0, ret=False):
        if diff_mask is not None:
            mask_rgb = cv2.cvtColor(diff_mask.astype(np.uint8) * 255, cv2.COLOR_GRAY2BGR)
        else:
            mask_rgb = np.zeros_like(ref_img)
        ref_img_masked = cv2.addWeighted(ref_img, (1-mask_w), mask_rgb, mask_w, 0.0)
        cur_img_masked = cv2.addWeighted(cur_img, (1-mask_w), mask_rgb, mask_w, 0.0)
        if diff_boxes is not None:
            h,w  = ref_img.shape[:2]
            boxes_in = [bx['box'].normalize(h,w) for bx in diff_boxes if bx['direction']=='in'] 
            boxes_out = [bx['box'].normalize(h,w) for bx in diff_boxes if bx['direction']=='out'] 

        cur_img_masked = draw_boxes(cur_img_masked, boxes_in,['obj_in'], ids=np.arange(len(boxes_in)))
        ref_img_masked = draw_boxes(ref_img_masked, boxes_out,['obj_out'], ids=np.arange(len(boxes_out)))
        viz_img = np.hstack((ref_img_masked, cur_img_masked))
        if ret:
            return viz_img
        cv2.imshow('Diff Images Mask', viz_img)
        cv2.waitKey()
    
    def calc_diff_mask(self, ref_img, cur_img):
        if not self.params.enable_diff_mask:
            return None
        if self.method=='img_diff':
            intensity_diff = cv2.absdiff(ref_img, cur_img)
            intensity_diff = intensity_diff.mean(axis=-1)
            return intensity_diff > 20
        elif self.method=='ssim':
            from skimage.measure import compare_ssim
            (_, diff) = compare_ssim(ref_img, cur_img, full=True, multichannel=True)
            diff = diff.min(axis=-1)
            mask = diff < 0.7
        elif self.method=='bg_sub':
            mask = self.bg_sub.apply(new_img['rgb'])

        # Morphologies
        mask = cv2.dilate(mask.astype(np.uint8) * 255, np.ones((3, 3)))

        return mask > 0

    
    def take_cart_snapshot(self, img, frame_num, fd=None, img_high_res=None):
        self.ref_img = self.cur_img
        img_cp = img.copy() if img is not None else None
        fd_cp = fd.copy() if fd is not None else None
        img_high_res_cp = img_high_res.copy() if img_high_res is not None else None
        self.cur_img = dict(img=img_cp, frame_num=frame_num, fd=fd_cp, img_high_res=img_high_res_cp)

        if self.method=='bg_sub':
            self.bg_sub.apply(img) # TODO - check

    def replace_reference_image(self, img, frame_num, fd=None, img_high_res=None):
        img_cp = img.copy() if img is not None else None
        fd_cp = fd.copy() if fd is not None else None
        img_high_res_cp = img_high_res.copy() if img_high_res is not None else None
        self.ref_img = dict(img=img_cp, frame_num=frame_num, fd=fd_cp, img_high_res=img_high_res_cp)

    def get_state(self, save_imgs):
        state_dict = dict()
        for k, val in vars(self).items():
            if k in ('wnoise_img') or (k in ('ref_img', 'cur_img') and not save_imgs):
                continue
            elif k=='ssd':
                state_dict[k] = vars(self.ssd)
            else:
                state_dict[k] = val

        return state_dict

    def set_state(self, state):
        for k, val in state.items():
            if k=='ssd':
                for ssd_k, ssd_val in val.items():
                    setattr(self.ssd, ssd_k, ssd_val)
            elif k in ('params', 'debug'):
                continue
            else:
                setattr(self, k, val)

if __name__=='__main__':
    import os, re
    import argparse
    parser = argparse.ArgumentParser(description='generate cart bottom images for annotation from dump images')
    parser.add_argument('--input-dir', help='input directory of dump images',type=str, required=True)
    parser.add_argument('--valid-postfix', help='postfix for valid image type',type=str, default='_qvga.')
    args = parser.parse_args()
    valid_postfix_list = args.valid_postfix.split(',')

    # Load Diff detector
    weights = 'diff_yolo/models/diff_detector_00191.pth'
    list_files = [f for f in os.listdir(os.path.join(args.input_dir)) if '.jpg' in f.lower()]
    for postfix in valid_postfix_list:
        pattern =  '[a-zA-z]+_[0-9]+%s*' % postfix 
        list_subtype_files = sorted([f for f in list_files if re.search(pattern, f)])
        my_diff_detector = BaseDiffDetector(weights, 'tset', method='ssim')
        for frame_idx, img_name in enumerate(list_subtype_files):
            img = cv2.imread(os.path.join(args.input_dir, img_name))
            diff = my_diff_detector.step(img, None, frame_idx, force_snapshot=True)