import cv2
import numpy as np
import enum
import os
import yaml
from cv_blocks.common.types import BoundingBox2D
from .base_diff_detector import BaseDiffDetector
from cv_blocks.events.movement_vector import MVState, MovementVector, EventDirection, MVElement
from .static_snapshot_detector import SDState
from .event_detector.event_detector import EventDetector
from cv_blocks.misc import aws_download
from cv_blocks.events.mv_manager import MotionVecLogger
from cv_blocks.common.buffers import CircularBuffer
from config import config

class MultiCamDiffDetector(object):
    '''
    This class implements single cam cart bottom module
    '''
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.maybe_during_event_th = config.get('maybe_during_event_th', 0.3)
            self.detector_conf_th = config.get('detector_conf_th', 0.35)
            self.connected_diffs_iou_th = config.get('connected_diffs_iou_th', 0.1)
            self.connected_diffs_dist_th = config.get('connected_diffs_dist_th', 0.1)
            self.connected_diffs_dist_th_small_products = config.get('connected_diffs_dist_th', 0.2)
            self.very_high_conf_diff_th = config.get('very_high_conf_diff_th', 0.9)
            self.high_conf_diff_th = config.get('high_conf_diff_th', 0.6)
            self.high_conf_accumulate_value = config.get('high_conf_accumulate_value', 1.3)
            self.high_conf_single = config.get('high_conf_single', 0.7)
            self.debug = config.get('debug', False)
            self.debug_reverse_dir = config.get('debug_reverse_dir', False) # DO NOT USE IN PRODUCTION
            self.frames_to_save = 4  # in data mode
            assert not self.debug_reverse_dir, 'Debug Feature Only!'
            self.save_snapshots = config.get('save_snapshots', False)
            self.initiate_events = config.get('initiate_events', True)
            self.cls_snapshot = config.get('cls_snapshot', True)
            self.pre_snapshot_frames_include_mv = config.get('pre_snapshot_frames_include_mv', 5)
            self.wait_before_sending_event = config.get('wait_before_sending_event', 10)
            self.event_detector_history_len = config.get('event_detector_history_len', 100)

    def __init__(self, md_module, weights, cam_names, method='img_diff', bfd_config=dict(), sync=True):
        self.cam_names = cam_names
        wnoise_img_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'data/images/wnoise_img.png')
        self.wnoise_img = cv2.imread(wnoise_img_path)
        self.params = self.Config(bfd_config)
        self.detector = self.load_detector(weights, sync)
        self.md_module = md_module
        # reduce confidence threshold if running on dataset mode
        if hasattr(self.md_module, 'dataset_saver'):
            self.params.detector_conf_th = 0.2
        self.method = method
        if method == 'bg_sub':
            self.bg_sub = cv2.createBackgroundSubtractorMOG2()
        # Load single cam diff detectors
        self.single_diff_detector = dict()
        for k in cam_names:
            self.single_diff_detector[k] = BaseDiffDetector(None, k,  bfd_config=dict())
        if self.params.save_snapshots:
            self.snapshot_log_dir = self.get_snapshot_dir()
            self.log_snapshot_idx = 0
        if self.params.cls_snapshot:
            self.event_detector = EventDetector(sync=sync)
        self.past_frames = dict()
        self.event_detector_scores = CircularBuffer(self.params.event_detector_history_len)
        self.event_detector_state = CircularBuffer(self.params.event_detector_history_len)
        self.event_candidate_range = []

        for cam in self.cam_names:
            self.past_frames[cam] = []
        self.num_snapshots_taken = 0
        self.has_trigger = False
        self.in_dynamic_state = False
        self.maybe_during_event = False

    def load_camera_config(self, camera_config):
        self.camera_config = camera_config
        for k in self.cam_names:
            self.single_diff_detector[k].load_camera_config(camera_config['camera_config'][k])

    def get_snapshot_dir(self):
        from datetime import datetime
        home_dir = os.path.expanduser("~")
        self.ts_id = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")
        snapshot_log_dir = os.path.join(home_dir, 'cart_bottom_snapshots', self.ts_id)
        os.makedirs(snapshot_log_dir, exist_ok=True)
        return snapshot_log_dir

    def get_model(self, weights, sync):
        model_root_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'diff_yolo/models')
        model_name, model_ext = os.path.splitext(weights)
        assert model_ext in ('.pth', '.onnx')
        full_weights_path = os.path.join(model_root_dir, model_name, weights)
        full_conf_path = full_weights_path.replace(model_ext, '_config.yaml')
        if not os.path.exists(full_weights_path) and sync:
            full_model_dir = os.path.dirname(full_weights_path)
            os.makedirs(full_model_dir, exist_ok=True)
            # Get model & config file
            aws_download.download("models/diff_detector", model_name, full_model_dir)
        
        assert os.path.exists(full_weights_path) and os.path.exists(full_conf_path)
        return full_weights_path, full_conf_path


    def load_detector(self, weights, sync):
        weights, conf_file = self.get_model(weights, sync)
        with open(conf_file) as config_f:
            config = yaml.safe_load(config_f)
        if weights.endswith('.pth'):
            from cv_blocks.object_detector.yolo_detector.torch_impl.yolo import YoloObjectDetector
        elif weights.endswith('.onnx'):
            from cv_blocks.object_detector.yolo_detector.trt_impl.yolo import YoloObjectDetector
        obj_det = YoloObjectDetector(config['backend'],
                    input_size=config['input_size'],
                    labels=config['labels'],
                    anchors=config['anchors'],
                    weights_path=weights,
                    nb_class = 2, # in & out
                    confidence_th=self.params.detector_conf_th,
                    # nms_th=args.nms_th,
                    channels=config['input_channels'])
        return obj_det
    
    def reset(self):
        for k in self.cam_names:
            self.single_diff_detector[k].reset()
        self.event_detector_scores.reset()
        self.event_detector_state.reset()
        self.num_snapshots_taken = 0
        self.has_trigger = False
        self.in_dynamic_state = False
        self.event_candidate_range = []
    
    def is_during_event(self):
        return self.in_dynamic_state
    
    def is_event_done(self):
        return self.has_trigger and not self.in_dynamic_state
    
    def event_candidate_matched(self, frame):
        if len(self.event_candidate_range) == 0:
            return False
        last_event = self.event_candidate_range[-1]
        return last_event['start_frame'] <= frame <= last_event['end_frame']
    
    def get_last_ed_image(self, cam_name):
        if cam_name in self.past_frames.keys():
            if len(self.past_frames[cam_name]) > 0:
                return self.past_frames[cam_name][-1]
        return None
    
    def should_take_snapshot(self, fd, img, frame_num, gray_img):
        ssd_state = []
        frames_stacked = self.params.frames_to_save if hasattr(self.md_module, 'dataset_saver') else \
            self.event_detector.frames_stacked
        if frames_stacked > 1:
            for cam in self.cam_names:
                if self.event_detector.is_gray:
                    gray_image = cv2.cvtColor(img[cam], cv2.COLOR_RGB2GRAY) if gray_img is None else gray_img[cam]
                    if hasattr(self.md_module, 'dataset_saver'):
                        self.past_frames[cam].append(gray_image.copy())
                    else:
                        self.past_frames[cam].append(cv2.resize(gray_image, self.event_detector.img_size))
                else:
                    if hasattr(self.md_module, 'dataset_saver'):
                        self.past_frames[cam].append(img[cam].copy())
                    else:
                        self.past_frames[cam].append(cv2.resize(img[cam], self.event_detector.img_size))
            if len(self.past_frames[self.cam_names[0]]) < frames_stacked:
                return False
            image = dict()
            for cam in self.cam_names:
                if len(self.past_frames[cam]) > frames_stacked:
                    self.past_frames[cam].pop(0)
                image[cam] = np.dstack(self.past_frames[cam])
            img = image
        if self.params.cls_snapshot:
            if hasattr(self.md_module, 'dataset_saver') and self.event_detector.frames_stacked != frames_stacked:
                prediction_images = [img[k][:, :, -self.event_detector.frames_stacked:] for k in self.cam_names]
            else:
                prediction_images = [img[k] for k in self.cam_names]
            
            # Event Detector Prediction
            # =========================
            self.md_module.profiler.start_block('event_detector')
            metric = self.event_detector.predict(prediction_images)
            self.md_module.profiler.stop_block('event_detector')

            metric = {self.cam_names[i] : metric[i] for i in range(len(metric))}
        for k in self.cam_names:
            cam_metric = metric[k] if self.params.cls_snapshot else None
            self.md_module.profiler.start_block('per_cam_logic')
            ssd_state.append(self.single_diff_detector[k].should_take_snapshot(fd[k], img[k], frame_num, metric=cam_metric))
            self.md_module.profiler.stop_block('per_cam_logic')
            # Save for dataset
            if hasattr(self.md_module, 'dataset_saver'):
                # Image convention is currently last frame on the right
                img_viz = np.hstack([x for x in img[k].transpose((2,0,1))])
                if not self.event_detector.is_gray:
                    img_viz = cv2.cvtColor(img_viz, cv2.COLOR_BGR2RGB)
                event_detector_data = dict(cam_name=k, img=img_viz, score=cam_metric, frame=frame_num)
                self.md_module.dataset_saver.save_event_detector_data(event_detector_data)

        if self.params.debug:
            for state, k in zip(ssd_state, self.cam_names):
                print('Cart Bottom Debug: [Frame %d] camera %s state: %s' % (frame_num, k, state))
        
        # Save static/dynamic state
        ssd_state_dict = {k:v for k,v in zip(self.cam_names, ssd_state)}
        self.event_detector_state.push(ssd_state_dict, self.md_module.global_frame_num)

        self.has_trigger = SDState.StaticTrigger in ssd_state
        self.in_dynamic_state = len([s for s in ssd_state if s in \
            (SDState.DynamicTrigger, SDState.DynamicNoTrigger, SDState.DynamicQuickTrigger, SDState.StaticPendingTrigger)])>0
        self.maybe_during_event = len([m for m in metric.values() if m > self.params.maybe_during_event_th]) > 0
        self.event_detector_scores.push(metric, self.md_module.global_frame_num)
        return self.has_trigger and not self.in_dynamic_state
    
    def get_event_detector_scrore(self, cam):
        last_sample = self.event_detector_scores.get_last()
        if last_sample is not None:
            return last_sample[cam]
        else:
            return 0.
        
    def update_event_candidate(self):
        if self.event_detector_state.size() < 20:
            return
        max_event_size = self.params.event_detector_history_len
        first_frame = self.md_module.global_frame_num - max_event_size
        event_last_frame = self.md_module.global_frame_num
        event_first_frame = None
        ed_state = self.event_detector_state.get_range(first_frame, event_last_frame)
        # Iterate back and find the last "silent" sequence (several staic frames)
        n_slient_frames = 0
        for state, frame_idx in zip(reversed(ed_state), range(event_last_frame, first_frame, -1)):
            if state is None:
                continue
            # Check if we have several "silent" consequtive frames
            if [SDState.StaticNoTrigger] * len(state)==list(state.values()):
                n_slient_frames += 1
                if n_slient_frames==10:
                    event_first_frame = frame_idx
                    break
            else:
                n_silent_frames = 0

        if event_first_frame is not None:
            # TODO - add conditions for valid size
            print('\033[93mIdentified Possible event candidate [%d,%s]\033[00m' % \
                (event_first_frame, event_last_frame), flush=True)
            self.event_candidate_range.append(dict(start_frame=event_first_frame, end_frame=event_last_frame))
    
    def step(self, img, fd, frame_num, force_snapshot=False, img_high_res=None, gray_img=None):
        diff = None
        self.md_module.profiler.start_block('should_take_snapshot')
        should_take_snapshot = self.should_take_snapshot(fd, img, frame_num, gray_img)
        self.md_module.profiler.stop_block('should_take_snapshot')
        if should_take_snapshot:
            self.update_event_candidate()
            for k in self.cam_names:
                self.single_diff_detector[k].ssd.reset()

        snapshot_taken = force_snapshot or should_take_snapshot
        if hasattr(self.md_module, 'barcode_handler'):
            # Don't take snapshots during barcode scanning
            if self.md_module.barcode_handler.is_during_scan():
                snapshot_taken = False

        if snapshot_taken:
            self.num_snapshots_taken += 1
            if self.params.debug:
                print('Cart Bottom Debug:Taking snapshot in frame %d' % (frame_num))
            for k in self.cam_names:
                self.single_diff_detector[k].take_cart_snapshot(img[k], frame_num, fd=fd[k], img_high_res=img_high_res[k])
            if self.is_comparison_valid():
                ref_frame = self.single_diff_detector[self.cam_names[0]].ref_img['frame_num']
                cur_frame = self.single_diff_detector[self.cam_names[0]].cur_img['frame_num']
                print('\033[94mCart Bottom:Comparing snapshots in frames [%d,%d]\033[00m' % (ref_frame, cur_frame), flush=True)
                diff = self.compare_cart_state()
        return snapshot_taken, diff, self.in_dynamic_state, self.maybe_during_event

    def replace_reference_image(self, img, fd, frame_num, img_high_res=None):
        for k in self.cam_names:
            self.single_diff_detector[k].replace_reference_image(img[k], frame_num, fd=fd[k], img_high_res=img_high_res[k])

    def is_comparison_valid(self):
        return min([self.single_diff_detector[k].is_comparison_valid() for k in self.cam_names])

    def get_cart_images(self):
        cart_images = dict()
        for k in self.cam_names:
            cart_images[k] = self.single_diff_detector[k].get_cart_images()
            if self.params.debug_reverse_dir:
                cart_images[k] = cart_images[k][::-1]
        return  cart_images   
    
    def predict(self, images):
        img_h, img_w = images[self.cam_names[0]][0].shape[:2]
        detector_in = []
        for k in self.cam_names:
            v = images[k]
            tensor_in = np.concatenate((v[0][:,:,::-1], v[1][:,:,::-1]), axis=-1)
            detector_in.append(tensor_in)
        boxes = self.detector.predict(detector_in, reverse_channels=False)
        # Output diffs
        diffs_out = dict()
        for i, k in enumerate(self.cam_names):
            diffs_out[k] = []
            for bx in boxes[i]:
                direction_conf = bx.classes.max() / (bx.classes.min() + 1e-6)
                diff_type = BaseDiffDetector.diff_type(bx)
                diff = dict(box=bx.renormalize(img_h, img_w), direction=diff_type, direction_conf=direction_conf)
                diffs_out[k].append(diff)
        return diffs_out

    def compare_cart_state(self):
        cart_images = self.get_cart_images()
        # Run diff yolo diff detector
        # ===========================
        self.md_module.profiler.start_block('diff_detector')
        diff_boxes = self.predict(cart_images)
        self.md_module.profiler.stop_block('diff_detector')
        diff_mask = dict()
        h, w = cart_images['front'][0].shape[:2]
        if hasattr(self.md_module.cart_content_handler, 'location_matching'):
            pile_predictor = self.md_module.cart_content_handler.location_matching.get_pile_locations
        for k in self.cam_names:
            diff_mask[k] = self.single_diff_detector[k].calc_diff_mask(cart_images[k][0], cart_images[k][1])
            # Filter outputs
            # ==============
            norm_boxes = [bbox['box'].normalize(h, w) for bbox in diff_boxes[k]]
            pile_modes = ['adjacent' if bbox['direction']=='in' else 'overlap' for bbox in diff_boxes[k]]
            if hasattr(self.md_module.cart_content_handler, 'location_matching'):
                pile_pred = [len(pile_predictor(k, bbox, mode=mode)) > 0 for bbox, mode in zip(norm_boxes, pile_modes)]
                diff_boxes[k] = self.single_diff_detector[k].filter_diffs(diff_boxes[k], diff_mask[k], pile_pred=pile_pred)
            else:
                diff_boxes[k] = self.single_diff_detector[k].filter_diffs(diff_boxes[k], diff_mask[k])

        if self.params.debug:
            viz_img = []
            for k in self.cam_names:
                viz_img.append(BaseDiffDetector.visualize_diff_mask(cart_images[k][0], cart_images[k][1], \
                                                                    diff_mask[k], diff_boxes=diff_boxes[k], ret=True))
            cv2.imshow('Cart Bottom Diffs', np.hstack(viz_img))
            cv2.waitKey(2)
        if self.params.save_snapshots: # TODO - check if deprecated
            for k in self.cam_names:
                postfix = 'qvga' if k=='front' else 'b_qvga'
                cam_sanpshot = np.hstack((cart_images[k][0], cart_images[k][1]))
                snapshot_file = os.path.join(self.snapshot_log_dir, '%s_cbt_%05d_%s.jpg' % (self.ts_id, self.log_snapshot_idx, postfix))
                cv2.imwrite(snapshot_file, cam_sanpshot)
            self.log_snapshot_idx += 1
        if hasattr(self.md_module, 'dataset_saver'):
            for cam_name in self.cam_names:
                ref_frame = self.single_diff_detector[cam_name].ref_img['frame_num']
                cur_frame = self.single_diff_detector[cam_name].cur_img['frame_num']
                cam_sanpshot = np.hstack((cart_images[cam_name][0], cart_images[cam_name][1]))
                diff_data = dict(cam_name=cam_name, img=cam_sanpshot, diffs=diff_boxes[cam_name], ref_frame=ref_frame, cur_frame=cur_frame)
                self.md_module.dataset_saver.save_diff_snapshot_data(diff_data)


        diffs = dict()
        for k in self.cam_names:
            diffs[k] = self.single_diff_detector[k].pack_diffs(cart_images[k][0], cart_images[k][1], diff_mask[k], diff_boxes[k])
        return diffs
    
    def diffs_connected(self, box1, box2, best, idx):
        iou = BoundingBox2D.iou(box1, box2)
        dist = BoundingBox2D.dist(box1, box2)
        dist_th = self.params.connected_diffs_dist_th_small_products if (box1.area() < 0.03 or box2.area() < 0.03) else self.params.connected_diffs_dist_th
        valid_connection = iou > self.params.connected_diffs_iou_th or dist < dist_th
        best_connection = iou > best['best_iou'] or (iou==best['best_iou'] and dist < best['best_dist'])
        if valid_connection and best_connection:
            best['best_idx'] = idx


    def find_high_conf_diffs(self, mv_manager, diffs, project_from=['front']):
        already_assigned = dict()
        for cam_name in self.cam_names:
            already_assigned[cam_name] = []
        # Find diffs with valid projections on other cam
        # ==============================================
        connected_diffs = []
        h, w = self.single_diff_detector[self.cam_names[0]].cur_img['img'].shape[:2]
        for proj_cam_name in project_from:
            for this_idx, diff in enumerate(diffs[proj_cam_name]):
                if this_idx in already_assigned[proj_cam_name]:
                    continue
                orig_box = diff['box'].normalize(h,w)
                this_dir = diff['direction']
                for other_cam_name in self.cam_names:
                    best_diff = dict(best_idx=None, best_iou=0., best_dist=1.)
                    if other_cam_name==proj_cam_name:
                        continue
                    proj_box, valid = mv_manager.project_by_disparity(orig_box, proj_cam_name, other_cam_name)
                    if not valid:
                        continue
                    for other_idx, other_cam_diff in enumerate(diffs[other_cam_name]):
                        if other_cam_diff['direction']!=this_dir or other_idx in already_assigned[other_cam_name]:
                            continue
                        other_cam_box = other_cam_diff['box'].normalize(h,w)
                        self.diffs_connected(proj_box, other_cam_box, best_diff, other_idx)
                    if best_diff['best_idx'] is not None:
                        connected_diffs.append(dict(cams=(proj_cam_name, other_cam_name), idx=(this_idx, best_diff['best_idx'])))
                        already_assigned[proj_cam_name].append(this_idx)
                        already_assigned[other_cam_name].append(best_diff['best_idx'])
        
        # Take high confidence diffs
        # ==========================
        high_conf_diffs = []
        for cam_name in self.cam_names:
            for diff_idx, cam_diff in enumerate(diffs[cam_name]):
                if cam_diff['box'].score > self.params.high_conf_diff_th:
                    conf_level = 'very_high' if cam_diff['box'].score > self.params.very_high_conf_diff_th else 'high'
                    high_conf_diffs.append(dict(cam=cam_name, idx=diff_idx, confidence=cam_diff['box'].score, level=conf_level))

        return connected_diffs, high_conf_diffs

    def merge_cart_bottom_diffs(self, cart_bottom_diffs, mv_manager, event_aggregator, event_classifier):
        '''
        Detect partial MVs that can be assigned to cart bottom changes
        '''
        if cart_bottom_diffs is None or \
            np.array([len(v) for v in cart_bottom_diffs.values()]).max()==0:
            return

        # Identify high confidence diffs
        connected_diffs, high_conf_diffs = self.find_high_conf_diffs(mv_manager, cart_bottom_diffs)

        assigned_diffs = dict()
        for cam_name in self.cam_names:
            assigned_diffs[cam_name] = []
            for diff in cart_bottom_diffs[cam_name]:
                diff['box'] = diff['box'].normalize(320,240)

        valid_initiate_event = self.valid_initiate_event(cart_bottom_diffs)
        # Connect cart bottom diffs to exising MVs and pending events
        # ===========================================================
        for cam_name in mv_manager.que_names:
            if len(cart_bottom_diffs[cam_name])==0:
                continue
            # Collect all partial events not sent to event detector
            inactive_events_pending = []
            live_event_mvs = []
            cam_mvs = mv_manager.get_cam_mvs(cam_name)
            for mv_id, mv in cam_mvs.items():
                if mv.state in (MVState.Inactive, MVState.InactiveConnected):
                    inactive_events_pending.append(mv)
                elif mv.state in (MVState.Active, MVState.ActiveConnected, MVState.Event, MVState.EventConnected):
                    live_event_mvs.append(mv)
            for diff_idx, cam_diff in enumerate(cart_bottom_diffs[cam_name]):
                if diff_idx in assigned_diffs[cam_name]:
                    continue
                # Check if a matching event was already sent to event detector
                # In that case, cart bottom diff was already handled by Motion Detect
                pre_event_offset = self.params.pre_snapshot_frames_include_mv
                pending_event, state = MultiCamDiffDetector.similar_event_sent(cam_diff, event_aggregator, event_classifier, cam_name, \
                    pre_event_offset)
                if pending_event is not None:
                    assigned_diffs[cam_name].append(diff_idx)
                    connected_diff = MultiCamDiffDetector.get_connected_diff(cart_bottom_diffs, diff_idx, cam_name,
                                                                             connected_diffs, assigned_diffs)
                    if connected_diff is not None:
                        connected_subset = [cd for cd in connected_diffs if
                                            cam_name in cd['cams'] and diff_idx == cd['idx'][
                                                cd['cams'].index(cam_name)]]
                        other_cam_name = [cam for cam in connected_subset[0]['cams'] if cam != cam_name][0]
                        assigned_diffs[cam_name].append(diff_idx)
                        if state == 'Pending':
                            # check if event can be extended also with connected diff
                            MultiCamDiffDetector.connect_pending_event(pending_event, connected_diff, other_cam_name)
                            continue
                        elif state == 'sent_and_not_done':
                            MultiCamDiffDetector.extend_sent_event(pending_event['event_id'], connected_diff, other_cam_name, event_classifier)
                            print('\033[92mCart Bottom[%s] connected diff is also added to event in classification above!\033[00m' % other_cam_name, flush=True)
                        else: # state=='sent_and_done'
                            MultiCamDiffDetector.create_new_event(dict(diff=cam_diff, camera=cam_name),
                                                                  dict(diff=connected_diff, camera=other_cam_name),
                                                                  mv_manager, event_aggregator, review=pending_event['event_id'])
                    elif state=='sent_and_done':
                        print('\033[93mCart Bottom[{}] found matching diff, matched to a sent event (event_id={}) that finished classifying ==> CB image not used\033[00m'.format(cam_name, pending_event['event_id']), flush=True)
                    continue
                if len(live_event_mvs) > 0:
                    enabled_event = MultiCamDiffDetector.find_event_mv(cam_diff, live_event_mvs, cam_name)
                    if enabled_event is not None:
                        assigned_diffs[cam_name].append(diff_idx)
                        MultiCamDiffDetector.connect_partial_mv(enabled_event, cart_bottom_diffs, diff_idx, cam_name, connected_diffs, assigned_diffs)
                        mv_manager.vectors[cam_name][enabled_event.id] = enabled_event
                if len(inactive_events_pending) > 0 and valid_initiate_event:
                    enabled_event = MultiCamDiffDetector.find_partial_mv(mv_manager, cam_diff, inactive_events_pending, cam_name)
                    if enabled_event is not None:
                        assigned_diffs[cam_name].append(diff_idx)
                        MultiCamDiffDetector.connect_partial_mv(enabled_event, cart_bottom_diffs, diff_idx, cam_name, connected_diffs, assigned_diffs)
                        # Force Event
                        mv_manager.vectors[cam_name][enabled_event.id] = enabled_event

        # Send remaining connected and high conf. diffs as events
        # =======================================================
        if self.params.initiate_events and valid_initiate_event:
            # go here only if there are only 3 or less bboxes overall (large amount has higher probability to cause errors)
            self.send_high_conf_diffs(cart_bottom_diffs, connected_diffs, high_conf_diffs, assigned_diffs, event_aggregator, event_classifier, mv_manager)

    def valid_initiate_event(self, diffs):
        # 3 or less bboxes overall (large amount has higher probability to cause errors)
        if len(diffs['back']) + len(diffs['front']) > 3:
            return False
        # all bboxes from both cameras have same direction
        directions_saw = []
        for camera in diffs.keys():
            for diff in diffs[camera]:
                if len(directions_saw) > 0 and diff['direction'] not in directions_saw:
                    return False
                directions_saw.append(diff['direction'])
        
        # Ignore images with diffs in barcode roi that were seen during process
        if hasattr(self.md_module, 'barcode_handler') and not self.md_module.barcode_handler.is_idle():
            for camera in diffs.keys():
                for diff in diffs[camera]:
                    check_coverage = camera!='front'
                    if self.md_module.barcode_handler.box_in_barcode_roi(diff['box'], camera, check_coverage=check_coverage):
                        return False
        
        # Avoid diffs that can be a result of snapshots during barcode scanning:
        # Diffs above highest decision line during or shortly after barcode scanning
        if hasattr(self.md_module, 'barcode_handler') and not self.md_module.barcode_handler.is_idle():
            for camera in diffs.keys():
                decision_line = self.md_module.cart_config['camera_config'][camera]['decision_lines']['top'] \
                    if camera=='front' else self.md_module.cart_config['camera_config'][camera]['decision_lines']['middle']
                for diff in diffs[camera]:
                    if diff['box'].y1 < decision_line:
                        return False
        return True

    @staticmethod
    def find_partial_mv(mv_manager, cam_diff, events_pending, cam_name):
        cb_dir = cam_diff['direction']
        only_one_option = len(events_pending) == 1
        for partial_event in events_pending:
            event_dir_eval = partial_event.evaluate_direction()
            valid_none_direction_event = event_dir_eval is None and \
                ((cb_dir=='in' and partial_event.init_location=='up') or (cb_dir=='out' and partial_event.current_location=='up'))
            if partial_event.event_occurred or len(partial_event.mv_element_list)<3 or \
                    (cb_dir != event_dir_eval and (not valid_none_direction_event)):
                continue
            valid_time = partial_event.mv_element_list[0].global_frame_number>=cam_diff['frames'][0] and \
                         partial_event.mv_element_list[-1].global_frame_number<=cam_diff['frames'][1]
            if not valid_time:
                continue
            valid_box = MultiCamDiffDetector.check_valid_box(cam_diff['box'], partial_event, cam_name, only_one_option)
            if valid_box:
                is_pile_event = mv_manager.is_event_on_pile(partial_event)
                is_small_item = mv_manager.is_event_small_product(partial_event)
                print('\033[92mCart Bottom[%s] attaching diff(%s) to partial MV [%s, %s]\033[00m' % (cam_name, cb_dir, partial_event.camera_type, partial_event.id), flush=True)
                # Connect to diff & force Event
                enabled_event = MultiCamDiffDetector.enable_mv(partial_event, cam_diff, cam_name, \
                    pile_event=is_pile_event, small_item=is_small_item)
                return enabled_event
        return None

    @staticmethod
    def find_event_mv(cam_diff, events_pending, cam_name):
        cb_dir = cam_diff['direction']
        only_one_option = len(events_pending) == 1
        for live_event in events_pending:
            if cb_dir != live_event.evaluate_direction():
                continue
            valid_time = live_event.mv_element_list[0].global_frame_number>=cam_diff['frames'][0] and \
                         live_event.mv_element_list[-1].global_frame_number<=cam_diff['frames'][1]
            if not valid_time:
                continue
            valid_box = MultiCamDiffDetector.check_valid_box(cam_diff['box'], live_event, cam_name, only_one_option)
            if valid_box:
                print('\033[92mCart Bottom[%s] attaching diff(%s) to event MV [%s, %s]\033[00m' % (cam_name, cb_dir, live_event.camera_type, live_event.id), flush=True)
                # Connect to diff & force Event
                enabled_event = MultiCamDiffDetector.enable_mv(live_event, cam_diff, cam_name, partial=False)
                return enabled_event
        return None
    
    @staticmethod
    def similar_event_sent(cam_diff, event_aggregator, event_classifier, cam_name, pre_event_offset):
        mv_state = ['Sent'] * len(event_aggregator.sent_events) + ['Pending'] * len(event_aggregator.pending_events)
        for pending_event, state in zip(event_aggregator.sent_events + event_aggregator.pending_events, mv_state):
            valid_cam = pending_event['multi_cam']==True or pending_event['initiating_cam']==cam_name
            valid_dir = pending_event['event_direction']==cam_diff['direction']
            if not (valid_cam and valid_dir):
                continue
            valid_time = pending_event['event_start'] + pre_event_offset >=cam_diff['frames'][0] and \
                         pending_event['event_end']<=cam_diff['frames'][1]
            valid_box = MultiCamDiffDetector.check_valid_box(cam_diff['box'], pending_event, cam_name)

            if valid_time and valid_box:
                if state=='Pending':
                    MultiCamDiffDetector.extend_pending_event(pending_event, cam_diff, cam_name)
                    print('\033[92mCart Bottom[{}] found matching diff, added to a pending event (event_id={})\033[00m'.format(cam_name, pending_event['event_id']), flush=True)
                    return pending_event, state
                else:
                    ret = MultiCamDiffDetector.extend_sent_event(pending_event['event_id'], cam_diff, cam_name, event_classifier)
                    if ret:
                        print('\033[93mCart Bottom[{}] found matching diff, added to a sent event (event_id={}) during classification\033[00m'.format(cam_name, pending_event['event_id']), flush=True)
                        return pending_event, 'sent_and_not_done'
                    else:
                        return pending_event, 'sent_and_done'
        
        return None, None
    
    @staticmethod
    def check_valid_box(cb_box_norm, event, cam_name, only_one_option = False):
        if isinstance(event, MovementVector):
            cam_boxes = [cb.coords for cb in event.mv_element_list]
            bottom_box = cam_boxes[np.argmax([coords.centroid()[1] for coords in cam_boxes])]
            edge_boxes = [bottom_box]
            if event.evaluate_direction()=="in" and cam_boxes[-1] != bottom_box:
                edge_boxes.append(cam_boxes[-1])
            elif event.evaluate_direction()=="out":
                if cam_boxes[0] != bottom_box:
                    edge_boxes.append(cam_boxes[0])
                if cam_name=='front':
                    edge_boxes.append(BoundingBox2D((edge_boxes[0].x1, edge_boxes[0].y2, edge_boxes[0].x2, 2*edge_boxes[0].y2-edge_boxes[0].y1)))
        else:
            cam_boxes = [mv.coords for mv in event['movement_vector'] if mv.cam_name==cam_name and not mv.source=='CB']
            if len(cam_boxes) == 0:
                return False
            bottom_box = cam_boxes[np.argmax([coords.centroid()[1] for coords in cam_boxes])]
            edge_boxes = [bottom_box]
            if event['event_direction']=="in" and cam_boxes[-1] != bottom_box:
                edge_boxes.append(cam_boxes[-1])
            elif event['event_direction']=="out":
                if cam_boxes[0] != bottom_box:
                    edge_boxes.append(cam_boxes[0])
                if cam_name=='front':
                    edge_boxes.append(BoundingBox2D((edge_boxes[0].x1, edge_boxes[0].y2, edge_boxes[0].x2, 2*edge_boxes[0].y2-edge_boxes[0].y1)))

        for ebx in edge_boxes:
            iou = BoundingBox2D.iou(cb_box_norm, ebx)
            dist = BoundingBox2D.dist(cb_box_norm, ebx)
            # print('Iou:%.2f, Dist:%.2f' % (iou, dist))
            if (iou>0.15 or dist<0.1): # TODO -read from params
                return True
            if only_one_option and dist<0.3:
                return True
        return False

    @staticmethod
    def enable_mv(event, diff, cam_name, cb_ratio=1.0, partial=True, pile_event=False, small_item=False):
        cls_weight = min(cb_ratio * len(event.mv_element_list), 3)
        cb_el = MultiCamDiffDetector.diff_to_mv_el(diff, cam_name, cls_weight=cls_weight)
        
        if partial:
            event.state = MVState.ForceEvent
            event.event_direction = EventDirection.In if diff['direction']=='in' else EventDirection.Out
            event.event_frame = cb_el.global_frame_number if event.event_direction==EventDirection.Out else cb_el.global_frame_number - 5
            event.mv_element_list += [cb_el]
        else:
            # If mv direction is not clear, decide with CB direction
            event.mv_element_list += [cb_el]
            curr_direction = event.evaluate_direction()
            if curr_direction is None:
                event.event_direction = EventDirection.In if diff['direction']=='in' else EventDirection.Out
        event.activated_by_cart_bottom = True
        if pile_event:
            event.event_occurred = True
            event.pile_event = True
        if small_item:
            event.small_item_candidate = True
        return event

    @staticmethod
    def connect_partial_mv(enabled_event, cart_bottom_diffs, diff_idx, cam_name, connected_diffs, assigned_diffs):
        # Find connected diff from other cam if exists
        connected_subset = [cd for cd in connected_diffs if cam_name in cd['cams'] and diff_idx==cd['idx'][cd['cams'].index(cam_name)]]
        if len(connected_subset) > 0:
            assert len(connected_subset)==1
            other_cam_name = [cam for cam in connected_subset[0]['cams'] if cam!=cam_name][0]
            other_idx = connected_subset[0]['idx'][connected_subset[0]['cams'].index(other_cam_name)]
            if other_idx not in assigned_diffs[other_cam_name]:
                connected_diff = cart_bottom_diffs[other_cam_name][other_idx]

                cls_weight = max([el.cls_weight for el in enabled_event.mv_element_list])
                cb_el = MultiCamDiffDetector.diff_to_mv_el(connected_diff, other_cam_name, cls_weight=cls_weight)
            
                # Add also connected diff to same mv vector
                enabled_event.mv_element_list += [cb_el]
                enabled_event.activated_by_cart_bottom = True

                # remove conencted diff from list
                assigned_diffs[other_cam_name].append(other_idx)

                print('\033[92mCart Bottom[%s] connected diff(%s) to MV[%s,%d]!\033[00m' % \
                    (other_cam_name, connected_diff['direction'], enabled_event.camera_type, enabled_event.id), flush=True)

    @staticmethod
    def extend_pending_event(event, diff, cam_name, cb_ratio=0.5):
        cls_weight = min(cb_ratio * len(event['movement_vector']), 3)
        cb_el = MultiCamDiffDetector.diff_to_mv_el(diff, cam_name, cls_weight=cls_weight)
        event['movement_vector'] += [cb_el]
        event['activated_by_cart_bottom'] = True

    @staticmethod
    def extend_sent_event(event_id, diff, cam_name, event_classifier, cb_ratio=0.5):
        # add to event that is now in classiffication
        if event_classifier.classification_during_proccess and event_classifier.event_id == event_id:
            cls_weight = min(cb_ratio * event_classifier.movement_vector_for_yaeen_len, 3)
            cb_el = MultiCamDiffDetector.diff_to_mv_el(diff, cam_name, cls_weight=cls_weight)
            event_classifier.movement_vector_for_yaeen += [cb_el]
            event_classifier.activated_by_cart_bottom = True
            event_classifier.event['activated_by_cart_bottom'] = True
            event_classifier.movement_vector_for_yaeen_len += 1
            event_classifier.classification_prob_mat = np.vstack((event_classifier.classification_prob_mat, np.zeros((1, event_classifier.classification_prob_mat.shape[1]))))
            if event_classifier.use_crop_descriptor:
                event_classifier.sequence_descriptors = np.vstack((event_classifier.sequence_descriptors, np.zeros((1, event_classifier.sequence_descriptors.shape[1]))))
            return True
        return False

    @staticmethod
    def get_connected_diff(cart_bottom_diffs, diff_idx, cam_name, connected_diffs, assigned_diffs):
        connected_subset = [cd for cd in connected_diffs if
                            cam_name in cd['cams'] and diff_idx == cd['idx'][cd['cams'].index(cam_name)]]
        if len(connected_subset) > 0:
            assert len(connected_subset) == 1
            other_cam_name = [cam for cam in connected_subset[0]['cams'] if cam != cam_name][0]
            other_idx = connected_subset[0]['idx'][connected_subset[0]['cams'].index(other_cam_name)]
            if other_idx not in assigned_diffs[other_cam_name]:
                connected_diff = cart_bottom_diffs[other_cam_name][other_idx]
                # remove conencted diff from list
                assigned_diffs[other_cam_name].append(other_idx)
                return connected_diff
        return None

    @staticmethod
    def connect_pending_event(pending_event, connected_diff, cam_name):
        cls_weight = max([el.cls_weight for el in pending_event['movement_vector']])
        cb_el = MultiCamDiffDetector.diff_to_mv_el(connected_diff, cam_name, cls_weight=cls_weight)

        # Add also connected diff to same mv vector
        pending_event['movement_vector'] += [cb_el] # TODO - add with classification weight
        pending_event['activated_by_cart_bottom'] = True

        print('\033[92mCart Bottom[%s] connected diff is also added to event above!\033[00m' % cam_name, flush=True)

    def send_high_conf_diffs(self, cart_bottom_diffs, connected_diffs, high_conf_diffs, assigned_diffs, event_aggregator, event_classifier, mv_manager):
        # Send connected diffs which at least 1 of them has high confidence
        for cam_name in cart_bottom_diffs.keys():
            cam_diffs = cart_bottom_diffs[cam_name]
            for this_idx, this_diff in enumerate(cam_diffs):
                if this_idx in assigned_diffs[cam_name]:
                    continue
                is_high_conf = len([cd for cd in high_conf_diffs if cam_name == cd['cam'] and this_idx == cd['idx']]) > 0
                if not is_high_conf:
                    continue
                connected_subset = [cd for cd in connected_diffs if cam_name in cd['cams'] and this_idx==cd['idx'][cd['cams'].index(cam_name)]]
                is_connected = len(connected_subset) > 0
                if is_connected:
                    assert len(connected_subset)==1
                    other_cam_name = [cam for cam in connected_subset[0]['cams'] if cam!=cam_name][0]
                    other_idx = connected_subset[0]['idx'][connected_subset[0]['cams'].index(other_cam_name)]
                    if other_idx in assigned_diffs[other_cam_name]:
                        continue
                    # other_high_conf = len([cd for cd in high_conf_diffs if other_cam_name == cd['cam'] and other_idx == cd['idx']]) > 0
                    both_conf_enough = cart_bottom_diffs[cam_name][this_idx]['box'].score + cart_bottom_diffs[other_cam_name][other_idx]['box'].score > self.params.high_conf_accumulate_value
                    this_very_high = len([cd for cd in high_conf_diffs if cam_name == cd['cam'] and this_idx == cd['idx'] and cd['level'] == 'very_high']) > 0
                    # demand very high conf from the box that wants to initiate event, or that its connected friend has also high confidence
                    if not this_very_high and not both_conf_enough:
                        continue
                    other_diff = cart_bottom_diffs[other_cam_name][other_idx]
                    # Create Event
                    MultiCamDiffDetector.create_new_event(dict(diff=this_diff, camera=cam_name), dict(diff=other_diff, camera=other_cam_name),
                                                          mv_manager, event_aggregator, wait=self.params.wait_before_sending_event)
                    # remove conencted diff from list
                    assigned_diffs[cam_name].append(this_idx)
                    assigned_diffs[other_cam_name].append(other_idx)
                elif config.CB_HIGH_CONF_SINGLE and cart_bottom_diffs[cam_name][this_idx]['box'].score > self.params.high_conf_single:
                    MultiCamDiffDetector.create_new_event(dict(diff=this_diff, camera=cam_name), None, mv_manager,
                                                          event_aggregator, wait=self.params.wait_before_sending_event)
                    assigned_diffs[cam_name].append(this_idx)
            
    @staticmethod
    def create_new_event(el_1, el_2, mv_manager, event_aggregator, review=None, wait=0):
        event = dict()
        this_el = MultiCamDiffDetector.diff_to_mv_el(el_1['diff'], el_1['camera'])
        if el_2 is not None:
            other_el = MultiCamDiffDetector.diff_to_mv_el(el_2['diff'], el_2['camera'])
            event['movement_vector'] = [this_el, other_el]
        else:
            event['movement_vector'] = [this_el]
        event['event_direction'] = el_1['diff']['direction']
        event['activated_by_cart_bottom'] = True
        event['activated_by_md'] = False
        event['initiating_cam'] = el_1['camera']
        event['multi_cam'] = el_2 is not None
        event['both_cams_saw_event'] = el_2 is not None
        event['event_frame'] = el_1['diff']['frames'][1] - 5 if event['event_direction']=='in' else el_1['diff']['frames'][1]
        event['event_start'] = el_1['diff']['frames'][0]
        event['event_end'] = el_1['diff']['frames'][1]
        event['event_id'] = mv_manager.event_id
        event['front_id'] = None
        event['back_id'] = None
        event['init_coords'] = None
        event['frame_history'] = []
        event['connected_mvs'] = []
        event['mv_id'] = -1
        event['early_indication_idx'] = None
        event['wait_before_sending_event'] = wait
        if review is not None:
            event['review_id'] = review
            review_message = ', to review event {}'.format(review)
        else:
            review_message = ''
        mv_manager.event_id += 1
        # event_aggregator.event_queue.put(event)
        event_aggregator.pending_events.append(event)
        if el_2 is not None:
            print(
                '\033[93mCart Bottom({}, {}): sending connected diffs without MV @ frame {}, direction {}{}! (event_id = {})\033[00m'.format(
                    el_1['camera'], el_2['camera'], event['event_frame'], event['event_direction'], review_message, event['event_id']), flush=True)
        else:
            print(
                '\033[93mCart Bottom({}): sending high conf diff from single camera without MV @ frame {}, direction {}{}! (event_id = {})\033[00m'.format(
                    el_1['camera'], event['event_frame'], event['event_direction'], review_message, event['event_id']), flush=True)

    @staticmethod
    def diff_to_mv_el(diff, cam_name, cls_weight=1.):
        # diff_frame = (diff['frames'][0] + diff['frames'][1]) / 2
        diff_frame = diff['frames'][1] - 5
        item_content = [diff['box'], diff['img'], 0, diff_frame,
                        dict(), None, None, diff['box'].score, 0.0, None, -1, None, None]
        cb_el = MVElement(item_content, cam_name, source='CB', cls_weight=cls_weight)
        return cb_el

    def get_state(self, save_imgs=False):
        state = dict()
        for k in self.cam_names:
            state[k] = self.single_diff_detector[k].get_state(save_imgs=save_imgs)
        return state
    
    def set_state(self, state):
        for k in self.cam_names:
            self.single_diff_detector[k].set_state(state[k])