import torch
import numpy as np
import cv2
from .mobilenet import mobilenet_v2
from detector.yolo.utils import decode_netout_vec

DEFAULT_ANCHORS = [1.13, 0.71, 1.58, 1.43, 2.74, 2.01, 3.24, 1.09, 4.61, 2.74]
DEFAULT_GRID = (8, 8)

PROD_IN_CLS = 0
PROD_OUT_CLS = 1
class Conv2d(torch.nn.Module):
    def __init__(self, n_in, n_out, kernel_size=3, stride=1,
                 padding=None, use_bn=True, activation='relu', groups=1):
        super(Conv2d, self).__init__()
        if padding is None:
            padding = kernel_size // 2
        self.conv = torch.nn.Conv2d(n_in, n_out, kernel_size=kernel_size, stride=stride,
                              padding=padding, groups=groups)
        assert activation in ['relu', 'linear', 'relu6', 'leaky_relu']
        # Activation
        if activation=='relu':
            self.activation = torch.nn.ReLU(inplace=True)
        elif activation=='relu6':
            self.activation = torch.nn.ReLU6(inplace=True)
        elif activation=='leaky_relu':
            self.activation = torch.nn.LeakyReLU(inplace=True)
        elif activation=='linear':
            self.activation = lambda x: x
        # Batch Norm
        if use_bn:
            self.bn = torch.nn.BatchNorm2d(n_out)
        else:
            self.bn = lambda x: x

    def forward(self, x):
        out = self.activation(self.bn(self.conv(x)))
        return out

class DiffYoloMobilenetV2(torch.nn.Module):
    def __init__(self, weights, n_class=2, n_box=5, 
                 anchors=DEFAULT_ANCHORS, grid_size=DEFAULT_GRID,
                 input_channels=6, img_size=(256,256),
                 nms_th=0.2,
                 confidence_th=0.3):
        torch.nn.Module.__init__(self)

        self.n_box = n_box
        self.anchors = np.array(anchors)
        self.n_class = n_class
        self.grid_size = grid_size
        self.img_size = img_size
        self.nms_th = nms_th
        self.confidence_th = confidence_th
        self.input_channels = input_channels
        self.model = mobilenet_v2(channels=input_channels)

        # YOLO Head
        mobilenet_feat_dim = self.model.features[-1][0].weight.shape[0]
        n_feat_out = self.n_box * (4 + 1 + n_class) # note we assume single object
        self.yolo = Conv2d(mobilenet_feat_dim, n_feat_out, kernel_size=1, activation='linear')

        # Load weights
        print('Diff Yolo: Loading weights from %s' % weights)
        checkpoint = torch.load(weights)
        self.load_state_dict(checkpoint['model_state_dict'])
        self.eval()

    def forward(self, p_in):
        x = self.model.features(p_in)
        out = self.yolo(x)

        return out
    
    def preproc(self, im):
        im = cv2.resize(im, self.img_size)
        im = im[:, :, ::-1]  # BGR 2 RGB
        im = np.array(im).astype(np.float32) / 255.
        return torch.from_numpy(im)

    def diff_type(self, box):
        box_classes = box.classes
        op_type = 'in' if box_classes[PROD_IN_CLS] > box_classes[PROD_OUT_CLS] else 'out'
        return op_type

    def predict(self, img1, img2):
        img_h, img_w = img1.shape[:2]
        img1 = self.preproc(img1)
        img2 = self.preproc(img2)
        tensor_in = torch.cat((img1, img2), dim=-1).view(1, self.img_size[0], self.img_size[1], self.input_channels)
        tensor_in = tensor_in.transpose(1,3).transpose(2,3)

        tensor_out = self.forward(tensor_in).cpu().detach().numpy()
        gh, gw = self.grid_size
        tensor_out = tensor_out.reshape(self.n_box, -1, gh, gw).transpose(2,3,0,1)

        boxes = decode_netout_vec(tensor_out, self.anchors, self.n_class, \
                obj_threshold=self.confidence_th, nms_threshold=self.nms_th)
        
        # Output diffs
        diffs_out = []
        for bx in boxes:
            diff_type = self.diff_type(bx)
            diff = dict(box=bx.renormalize(img_h, img_w), direction=diff_type)
            diffs_out.append(diff)

        return diffs_out