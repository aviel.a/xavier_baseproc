import numpy as np
import enum

class SDState(enum.Enum):
    StaticTrigger = 0
    StaticNoTrigger = 1
    StaticPendingTrigger = 2
    DynamicTrigger = 3
    DynamicNoTrigger = 4
    DynamicQuickTrigger = 5

class StaticSnapshotDetector(object):
    def __init__(self, cam_name, debug=False):
        self.cam_name = cam_name
        # params
        self.th_very_low = 0.7# 3.0
        self.th_low = 1.5# 3.0
        self.th_high = 2.5#7.0
        self.th_img_diff = 10
        self.min_hist_len = 10
        self.high_img_diff = 15
        self.max_frames_in_pending = 3
        self.metric_vector = dict(fd=[], img=[])
        self.state = SDState.StaticNoTrigger
        self.debug = debug
        self.ref_metric = None
        self.pending_counter = 0
    
    def plot_metrics(self):
        from matplotlib import pyplot as plt
        plt.figure()
        plt.subplot(121)
        plt.title('Frame Delta metric')
        plt.plot(np.arange(len(self.metric_vector['fd'])), self.metric_vector['fd'])
        plt.subplot(122)
        plt.title('Img diff metric')
        plt.plot(np.arange(len(self.metric_vector['img'])), self.metric_vector['img'])
        plt.show()
    
    def is_static(self, metric):
        self.state = self.step(metric)
        if self.state==SDState.StaticTrigger:
            # reset stuff
            # self.plot_metrics()
            self.metric_vector = dict(fd=[], img=[])
        return self.state


    def step(self, metric):
        next_state = self.state
        if self.state==SDState.StaticNoTrigger:
            next_state = SDState.DynamicNoTrigger if metric['fd'] > self.th_high else SDState.StaticNoTrigger
        elif self.state==SDState.DynamicNoTrigger:
            if metric['fd'] > self.metric_vector['fd'][-1]: # motion still increasing, don't change state
                assert metric['fd'] > self.th_high 
                next_state = SDState.DynamicNoTrigger
            else: # motion starts decreasing, move to dynamic, trigger
                # assert metric['fd'] > self.th_low # TODO - maybe this is not true!
                next_state = SDState.DynamicTrigger
        elif self.state==SDState.DynamicTrigger:
            # Hand in cart will result high image diff w.r.t to last frame
            # motion_in_cart = metric['img'] > self.th_img_diff and metric['fd'] > self.th_very_low
            motion_in_cart = metric['fd'] > self.th_low
            # Cart in motion for move the basic image diff to high value. if it is steady for some time 
            # and frame delta is low, snapshot can be taken
            steady_high_img_diff = len(self.metric_vector['img']) > self.min_hist_len  and \
                metric['img'] - np.array(self.metric_vector['img'][-self.min_hist_len:]).mean() < 0 and \
                metric['img'] > self.high_img_diff
            next_state = SDState.DynamicTrigger if metric['fd'] > self.th_low or motion_in_cart and not steady_high_img_diff\
                else SDState.StaticPendingTrigger
            if next_state==SDState.StaticPendingTrigger:
                self.ref_metric = metric['fd']
                self.pending_counter = 0
        elif self.state==SDState.StaticPendingTrigger:
            # as long as metric['fd'] is decreasing/same, stay in this state, otherwise:
            # if another dynamic peak occured right after the first one, go to second dynamic state, else go to trigger event 
            metric_not_up = metric['fd'] <= self.ref_metric
            pending_time_frame = self.pending_counter < self.max_frames_in_pending
            if metric_not_up:
                next_state = SDState.StaticPendingTrigger if pending_time_frame else SDState.StaticTrigger
            else:
                next_state = SDState.DynamicQuickTrigger if pending_time_frame else SDState.StaticTrigger
            self.pending_counter += 1
        elif self.state==SDState.StaticTrigger:
            # next_state = SDState.StaticNoTrigger
            next_state = SDState.StaticTrigger # Keep here until released

        elif self.state==SDState.DynamicQuickTrigger:
            next_state = SDState.DynamicQuickTrigger if metric['fd'] > self.th_low else SDState.StaticTrigger
        if self.debug:
            print('SSD Debug[cam %s]: fd metric=%.2f, img metric=%.2f, state = %s' % (self.cam_name, metric['fd'], metric['img'], next_state))
        self.metric_vector['fd'].append(metric['fd'])
        self.metric_vector['img'].append(metric['img'])
        return next_state
    
    def reset(self):
        self.metric_vector = dict(fd=[], img=[])
        self.state = SDState.StaticNoTrigger