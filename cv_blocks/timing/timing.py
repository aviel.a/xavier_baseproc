from time import time


class TimesDict:
    def __init__(self):
        self.times = dict()
        self.names = []

    def start(self, name):
        self.times[name] = dict(start=time())
        self.names.append(name)

    def end(self):
        if len(self.names) > 0:
            self.times[self.names[-1]]['end'] = time()

    def start_and_close_previous(self, name):
        self.end()
        self.start(name)

    def clear(self):
        self.times = dict()
        self.names = []

    def print_times(self, clear_after_print=True):
        sum = 0
        for element in self.names:
            time = self.times[element]['end'] - self.times[element]['start']
            sum += time
            if time >= 0.1 :
                print("\033[31mTime for {:35s} \t{:.2f} sec\033[00m".format(element, time), flush=True)
            else:
                time = time * 1000
                print("\033[93mTime for {:35s} \t{:.2f} ms\033[00m".format(element, time), flush=True)

        print("\n\033[31mTotal {:38s} \t{:.2f} sec\n\033[00m".format('', sum), flush=True)
        if clear_after_print:
            self.clear()
