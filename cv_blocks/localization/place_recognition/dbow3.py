import os
import cv2
import numpy as np
import pyDBoW3
from cv_blocks.localization.map.slam_map import SlamMap
from cv_blocks.feature_detector.orb import OrbFeatureDetector
from cv_blocks.geometry.pose import Pose

class DBoWPlaceRecognizer(object):
    def __init__(self, vocabulary_file='vocabulary/ORBvoc.txt',
                       feature_extractor=OrbFeatureDetector(),
                       anchor_map=None,
                       img_mask=None,
                       di_levels=5,
                       direct_index=True, 
                       debug=True,
                       num_candidates=3):
        if anchor_map is not None:
            self.anchor_map = anchor_map
        else:
            self.anchor_map = None
        self.vocabulary_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), vocabulary_file)
        if not os.path.exists(self.vocabulary_file):
            raise FileNotFoundError('Vocalbulary file not found: %s. please download first' % self.vocabulary_file)
        self.feature_extractor=feature_extractor
        self.di_levels = di_levels
        self.direct_index = direct_index
        self.voc = self.load_voc()
        if self.anchor_map is not None:
            self.anchor_map.calc_kf_bows(self.desc2bow)
        self.img_mask = img_mask
        if self.img_mask is not None:
           assert len(self.img_mask.shape)==2
        self.debug = debug
        self.num_candidates = num_candidates
    
    def load_voc(self):
        print('DBoW: Loading vocabulary from %s' % self.vocabulary_file)
        voc = pyDBoW3.Vocabulary()
        voc.load(self.vocabulary_file)
        return voc
    
    def create_database(self, voc):
        self.db = pyDBoW3.Database()
        self.db.setVocabulary(voc, self.direct_index, self.di_levels)
    
    def calc_descriptor(self, img):
        # Extract image features
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        _, desc = self.feature_extractor.extract(img, mask=self.img_mask)
        # Create BoW representation
        bow_vec = self.desc2bow(desc)
        return bow_vec
    
    def desc2bow(self, img_descriptors):
        bow_vec = self.voc.transform(np.expand_dims(img_descriptors, axis=1))
        return bow_vec.get()


    def calc_similarity(self, rep1, rep2):
        bw1 = pyDBoW3.BowVector()
        bw1.set(rep1)
        bw2 = pyDBoW3.BowVector()
        bw2.set(rep2)
        return self.voc.score(bw1, bw2)

    def find_similar_kfs(self, frame):
        assert self.anchor_map is not None
        ref_bow = self.calc_descriptor(frame)
        # 1. Find neareset neighbor KF using bow representation
        kf_ids = []
        kf_scores = []
        for kf_id, kf_data in self.anchor_map.kf_iter():
            similarity_score = self.calc_similarity(ref_bow, kf_data['bow'])
            kf_scores.append(similarity_score)
            kf_ids.append(kf_id)
        similar_kf_idx = np.argsort(-np.array(kf_scores))[:self.num_candidates]
        similar_kf_scores = np.array(kf_scores)[similar_kf_idx]
        similar_kf_ids = np.array(kf_ids)[similar_kf_idx]

        # Temporary - return best candidate location
        best_kf = self.anchor_map.get_kf(similar_kf_ids[0])
        second_best_kf = self.anchor_map.get_kf(similar_kf_ids[1])
        third_best_kf = self.anchor_map.get_kf(similar_kf_ids[2])
        if self.anchor_map.is_near_kfs(best_kf, second_best_kf) or self.anchor_map.is_near_kfs(best_kf, third_best_kf):
            estimated_pose = Pose((best_kf['rot_cw'], best_kf['trans_cw']))
        elif similar_kf_scores[0] > similar_kf_scores[1] * 2:
            estimated_pose = Pose((best_kf['rot_cw'], best_kf['trans_cw']))
        else:
            estimated_pose = None
        
        if self.debug:
            print(['KF_id:%d, Score:%.3f' % (kf_id, kf_sc) for (kf_sc, kf_id) in zip(similar_kf_scores, similar_kf_ids)])
        # similar_frame_idx =  [best_kf['src_frm_id'], second_best_kf['src_frm_id'], third_best_kf['src_frm_id']]
        similar_frame_idx =  [self.anchor_map.get_kf(kf_id)['src_frm_id'] for kf_id in similar_kf_ids]
        similar_frame_info = [dict(score=sc, frame_id=fid, keyframe_id=kfid) \
            for sc, fid, kfid in zip(similar_kf_scores, similar_frame_idx, similar_kf_ids)]
        
        return similar_frame_info, estimated_pose

if __name__=='__main__':
    from matplotlib import pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    from cv_blocks.localization.place_recognition.utils import viz_similar_images, get_sparse_frames_from_video
    video_path = '/home/walkout05/place_rec_data/walkout/video3.avi'
    map_video_path = '/home/walkout05/place_rec_data/walkout/video2.avi'
    map_path = '/home/walkout05/place_rec_data/walkout/map2.pb'
    img_mask = cv2.imread('/home/walkout05/place_rec_data/walkout/mask.png', -1)
    rotate = False
    anchor_map = SlamMap(map_path)
    place_recongizer = DBoWPlaceRecognizer(anchor_map=anchor_map, img_mask=img_mask)
    camera = cv2.VideoCapture(video_path)
    _, map_frames_idx = place_recongizer.anchor_map.get_kf_frame_ids()
    map_frames = get_sparse_frames_from_video(map_video_path, map_frames_idx)
    img_db = []
    n_imgs = 500
    traj_3d = dict(x=[], y=[], z=[])
    img_every = 20
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlim(-0.1, 0.1)
    ax.set_ylim(-0.1, 0.1)
    ax.set_zlim(-0.1, 0.1)
    for k in range(n_imgs * img_every):
        ret, img = camera.read()
        if k % img_every != 0:
            continue
        if not ret: 
            break
        if rotate:
            img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
        bow_vec = place_recongizer.calc_descriptor(img)
        img_db.append(dict(img=img, bow_vec=bow_vec))
        similar_frame_info, est_pose  = place_recongizer.find_similar_kfs(img)
        similar_images = [map_frames[f['frame_id']] for f in similar_frame_info]
        similar_scores = [f['score'] for f in similar_frame_info]
        viz_similar_images(img, similar_images, similar_scores, viz=True)
        if est_pose is not None:
            loc = est_pose.t()
            traj_3d['x'].append(loc[0])
            traj_3d['y'].append(loc[1])
            traj_3d['z'].append(loc[2])
        
        # Visualize trajectory
        ax.plot(traj_3d['x'], traj_3d['y'], traj_3d['z'], color='blue')
        plt.pause(0.001)

    # Calculate pair-wise distances 
    dist_mat = np.zeros((n_imgs, n_imgs))
    for i in range(n_imgs):
        for j in range(i, n_imgs):
            if i==j:
                continue
            similarity_score = place_recongizer.calc_similarity(img_db[i]['bow_vec'], img_db[j]['bow_vec'])
            dist_mat[i,j] = similarity_score
            dist_mat[j,i] = similarity_score

    # Calc most similar images per each img
    for i in range(n_imgs):
        similarity_scores = dist_mat[i]
        most_similar_idx = np.argsort(-similarity_scores)[:3].ravel()
        similar_images = [img_db[idx]['img'] for idx in most_similar_idx]
        similar_scores = [similarity_scores[idx] for idx in most_similar_idx]
        viz_similar_images(img_db[i]['img'], similar_images, similar_scores, viz=True)