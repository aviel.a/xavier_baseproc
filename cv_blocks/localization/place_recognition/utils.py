import cv2
import numpy as np

def viz_similar_images(ref, imgs, scores, viz_size=(240,320), viz=False):
    viz_all = cv2.resize(ref, viz_size)
    cv2.putText(viz_all, 'ref', (5, 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255,0,0))
    for img, score in zip(imgs, scores):
        img_viz = cv2.resize(img, viz_size)
        cv2.putText(img_viz, 'sc:%.2f' % score, (5, 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        viz_all = np.hstack((viz_all, img_viz))
    if viz:
        cv2.imshow('similar_imgs', viz_all)
        cv2.waitKey()
    return viz_all

def get_sparse_frames_from_video(video_path, frames_idx):
    frames_idx = np.array(frames_idx)
    idx = 0
    vid_reader = cv2.VideoCapture(video_path)
    sparse_frames = dict()
    while True:
        ret, img = vid_reader.read()
        if not ret or idx > frames_idx.max(): break
        if idx in frames_idx:
            sparse_frames[idx] = img
        idx += 1
    print('Extracted %d frames from %s' % (len(sparse_frames), video_path))
    return sparse_frames

def viz_image_matches(img1, img2, feat1, feat2, viz=False):
    img_viz1 = img1.copy()
    img_viz2 = img2.copy()
    n_inliers = len(feat1)
    cv2.putText(img_viz1, 'ref, inliers:%d' % (n_inliers), (5, 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255,0,0))
    cv2.putText(img_viz2, 'map', (5, 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
    dummy_match = [cv2.DMatch(i, i, 0.) for i in range(n_inliers)]
    final_viz = cv2.drawMatches(img_viz1, feat1,  img_viz2, feat2, dummy_match, None)
    if viz:
        cv2.imshow('Match to map', final_viz)
        cv2.waitKey()
    return final_viz