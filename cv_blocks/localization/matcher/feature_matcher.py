import numpy as np
import cv2

class FeatureMatcher(object):
    def __init__(self, norm=cv2.NORM_HAMMING, cross_check=True):
        self.norm = norm
        self.cross_check = cross_check

    def match_brute_force(self, desc1, desc2):
        '''
        Brute force feature matching
        '''
        matcher = cv2.BFMatcher(self.norm, crossCheck=self.cross_check)
        matches = matcher.match(desc1,desc2)
        return matches
    
    def match_with_radius(self):
        '''
        Feature matching with location prior
        '''
        # TODO - implement

