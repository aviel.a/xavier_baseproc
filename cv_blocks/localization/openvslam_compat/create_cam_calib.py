import argparse
import os
import pickle
import yaml

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='create OpenVSLAM calib file')
    parser.add_argument('--calib-name', help='input calibration model',type=str, required=True)
    parser.add_argument('--output-dir', help='output directory',type=str, required=True)
    args = parser.parse_args()

    curr_dir = os.path.dirname(os.path.realpath(__file__))
    # Load template file
    # ==================
    calib_left_out = yaml.safe_load(open(os.path.join(curr_dir, 'template_mono.yaml')))
    walkout_calib_dir = os.path.join(curr_dir, '..', '..', 'calibration', 'models')
    input_calib_dir = os.path.join(walkout_calib_dir, args.calib_name)
    input_calib_file = os.path.join(input_calib_dir, 'calib.pkl')
    assert os.path.isfile(input_calib_file) 
    calib_in = pickle.load(open(input_calib_file, 'rb'))
    # Left Mono cam calib
    # ===================
    calib_left_int = calib_in['intrinsics']['left']
    calib_left_out['Camera.name']='WalkOut %s monocular' % args.calib_name

    calib_left_out['Camera.fx']=float(calib_left_int['K'][0][0])
    calib_left_out['Camera.fy']=float(calib_left_int['K'][1][1])
    calib_left_out['Camera.cx']=float(calib_left_int['K'][0][2])
    calib_left_out['Camera.cy']=float(calib_left_int['K'][1][2])

    calib_left_out['Camera.k1']=float(calib_left_int['dist_coeffs'][0][0])
    calib_left_out['Camera.k2']=float(calib_left_int['dist_coeffs'][0][1])
    calib_left_out['Camera.p1']=float(calib_left_int['dist_coeffs'][0][2])
    calib_left_out['Camera.p2']=float(calib_left_int['dist_coeffs'][0][3])
    calib_left_out['Camera.k3']=float(calib_left_int['dist_coeffs'][0][4])

    output_name = os.path.join(args.output_dir, 'walkout_%s_mono.yaml' % args.calib_name)
    yaml.dump(calib_left_out, open(output_name, 'w'))

    # Sterero cam calib
    # =================
    calib_right_int = calib_in['intrinsics']['right']
    calib_stereo_out = yaml.safe_load(open(os.path.join(curr_dir, 'template_stereo.yaml')))
    calib_stereo = calib_in['stereo']['img_640x480']
    calib_stereo_out['Camera.name']='WalkOut %s stereo' % args.calib_name
    calib_stereo_out['Initializer.num_min_triangulated_pts']=20

    calib_stereo_out['Camera.focal_x_baseline']=float(-calib_stereo['pr'][0,3])

    calib_stereo_out['Camera.fx']=float(calib_stereo['pl'][0][0])
    calib_stereo_out['Camera.fy']=float(calib_stereo['pl'][1][1])
    calib_stereo_out['Camera.cx']=float(calib_stereo['pl'][0][2])
    calib_stereo_out['Camera.cy']=float(calib_stereo['pl'][1][2])

    calib_stereo_out['StereoRectifier.K_left']=calib_left_int['K'].flatten().tolist()
    calib_stereo_out['StereoRectifier.D_left']=calib_left_int['dist_coeffs'].flatten().tolist()
    calib_stereo_out['StereoRectifier.R_left']=calib_stereo['rl'].flatten().tolist()

    calib_stereo_out['StereoRectifier.K_right']=calib_right_int['K'].flatten().tolist()
    calib_stereo_out['StereoRectifier.D_right']=calib_right_int['dist_coeffs'].flatten().tolist()
    calib_stereo_out['StereoRectifier.R_right']=calib_stereo['rr'].flatten().tolist()

    output_name = os.path.join(args.output_dir, 'walkout_%s_stereo.yaml' % args.calib_name)
    yaml.dump(calib_stereo_out, open(output_name, 'w'))
