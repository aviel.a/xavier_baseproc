import numpy as np
import cv2
from cv_blocks.localization.map.slam_map import SlamMap
from cv_blocks.localization.place_recognition.dbow3 import DBoWPlaceRecognizer
from cv_blocks.localization.matcher.feature_matcher import FeatureMatcher
from cv_blocks.geometry.pose import Pose

class MapLocalizer(object):
    def __init__(self, map_file, img_mask,
                 num_kf_candidates=3,
                 init_pose_est_inlier_th=15):
        self.anchor_map = SlamMap(map_file)
        self.place_recognizer = DBoWPlaceRecognizer(anchor_map=self.anchor_map,
                                                    img_mask=img_mask,
                                                    num_candidates=num_kf_candidates)
        self.img_mask = img_mask
        self.matcher = FeatureMatcher()
        self.num_kf_candidates = num_kf_candidates
        self.init_pose_est_inlier_th = init_pose_est_inlier_th
    
    def localize_frame(self, img, cam_params):
        # Find candidate keyframes from map
        # =================================
        kf_candidates, _  = self.place_recognizer.find_similar_kfs(img)

        # Try to verify each candidate vs. map
        # ====================================
        img_T_w = None
        inlier_info = dict()
        ref_kp, ref_desc = self.place_recognizer.feature_extractor.extract(img, mask=self.img_mask)
        for candidate_idx, candidate in enumerate(kf_candidates):
            # Get KF accosicated 3d points and their descriptors
            kf_T_w_ps = self.anchor_map.get_kf_pose(candidate['keyframe_id'])
            kf_pts_3d, feat_idx = self.anchor_map.get_kf_pts3d(candidate['keyframe_id'])
            candidate_desc = self.anchor_map.get_kf_descriptors(candidate['keyframe_id'], subset_idx=feat_idx)
            candidate_feats = self.anchor_map.get_kf_features(candidate['keyframe_id'], subset_idx=feat_idx)
            # Step 1: Brute-force matching
            matches = self.matcher.match_brute_force(ref_desc, candidate_desc)
            # Step 2: Create 3d-2d correspondences
            pts_3d = []; ref_pts_2d = []; map_pts_2d = []
            for m in matches:
                ref_pts_2d.append(ref_kp[m.queryIdx])
                pts_3d.append(kf_pts_3d[m.trainIdx])
                map_pts_2d.append(candidate_feats[m.trainIdx])
            pts_3d = np.expand_dims(np.array(pts_3d), axis=1).astype(np.float32)
            ref_pts_2d_opt = np.expand_dims(np.array([f.pt for f in ref_pts_2d]), axis=1).astype(np.float32)
            # Step 3: Find transform using RANSAC
            success, rvecs, tvecs, inliers = cv2.solvePnPRansac(pts_3d, ref_pts_2d_opt, cam_params['K'], cam_params['dist_coeffs'])
            if inliers is None:
                inliers = []
            if success and len(inliers) > self.init_pose_est_inlier_th:
                print('\033[92mFound relative pose with KF %d: %d inliers\033[00m' % (candidate['keyframe_id'], len(inliers)))
                img_T_w = Pose((rvecs, tvecs)).inv()

                inlier_info = dict(map_feats=[pt for idx, pt in enumerate(map_pts_2d) if idx in inliers],
                                   img_feats=[pt for idx, pt in enumerate(ref_pts_2d) if idx in inliers],
                                   kf_idx=candidate['keyframe_id'],
                                   kf_pose=kf_T_w_ps)
                break
            else:
                print('\033[91mFailed on KF %d: %d inliers\033[00m' % (candidate['keyframe_id'], len(inliers)))
                if candidate_idx==0: # save most probable candidate
                    inlier_info = dict(map_feats=[pt for idx, pt in enumerate(map_pts_2d) if idx in inliers],
                                    img_feats=[pt for idx, pt in enumerate(ref_pts_2d) if idx in inliers],
                                    kf_idx=candidate['keyframe_id'],
                                    kf_pose=kf_T_w_ps)

        # Step 4:refine pose with feature matches with prior 
        # (project all relevant map points onto reference frame to find more matches and find better solution)
        # TODO

        # Return final localization result
        # ================================
        return img_T_w, inlier_info



if __name__=='__main__':
    # from matplotlib import pyplot as plt
    # from mpl_toolkits.mplot3d import Axes3D
    # from cv_blocks.localization.place_recognition.utils import viz_similar_images, get_sparse_frames_from_video
    from cv_blocks.localization.place_recognition.utils import viz_image_matches, get_sparse_frames_from_video
    import pickle
    cam_config_file = '/home/walkout05/git_repo/walkout/cv_blocks/calibration/models/gen2_basler_sys0.11_sh_1/calib.pkl'
    conf_data = pickle.load(open(cam_config_file, 'rb'))
    cam_conf = dict(K=conf_data['intrinsics']['left']['K'],
                    dist_coeffs=conf_data['intrinsics']['left']['dist_coeffs'])

    video_path = '/home/walkout05/place_rec_data/walkout/video3.avi'
    map_video_path = '/home/walkout05/place_rec_data/walkout/video2.avi'
    map_path = '/home/walkout05/place_rec_data/walkout/map2.pb'
    img_mask = cv2.imread('/home/walkout05/place_rec_data/walkout/mask.png', -1)
    rotate = False
    localizer = MapLocalizer(map_path, img_mask)
    camera = cv2.VideoCapture(video_path)
    map_kf_idx, map_frames_idx = localizer.anchor_map.get_kf_frame_ids()
    map_frames = get_sparse_frames_from_video(map_video_path, map_frames_idx)
    video_writer = cv2.VideoWriter('/tmp/map_loc_viz.mp4',cv2.VideoWriter_fourcc('M','J','P','G'), 5, (960,640))
    n_imgs = 500
    img_every = 20
    for k in range(n_imgs * img_every):
        ret, img = camera.read()
        if k % img_every != 0:
            continue
        if not ret: 
            break
        if rotate:
            img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
        est_pose, inlier_info = localizer.localize_frame(img, cam_conf)
        # Visualize matches
        # =================
        t = est_pose.t().flatten(); r = est_pose.r().flatten()
        cv2.putText(img, 'frame 6DOF:', (100, 200), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        cv2.putText(img, 'pos:(%.2f, %.2f, %.2f)' % (t[0], t[1], t[2]), (100, 220), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        cv2.putText(img, 'rot:(%.2f, %.2f, %.2f)' % (r[0], r[1], r[2]), (100, 240), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        kf_img = map_frames[map_frames_idx[map_kf_idx.index(inlier_info['kf_idx'])]].copy()
        t = inlier_info['kf_pose'].t().flatten(); r = inlier_info['kf_pose'].r().flatten()
        cv2.putText(kf_img, 'map 6DOF:', (100, 200), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        cv2.putText(kf_img, 'pos:(%.2f, %.2f, %.2f)' % (t[0], t[1], t[2]), (100, 220), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        cv2.putText(kf_img, 'rot:(%.2f, %.2f, %.2f)' % (r[0], r[1], r[2]), (100, 240), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        img_viz = viz_image_matches(img, kf_img, inlier_info['img_feats'], inlier_info['map_feats'], viz=False)
        video_writer.write(img_viz)