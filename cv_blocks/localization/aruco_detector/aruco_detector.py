import cv2
import numpy as np
import os
from multiprocessing import Process
from config import config
import time

class ArucoDetector(object):
    def __init__(self, aruco_type=cv2.aruco.DICT_4X4_1000):
        self.aruco_type = aruco_type
        self.dict = cv2.aruco.Dictionary_get(aruco_type)
        self.params =  cv2.aruco.DetectorParameters_create()

    def detect(self, img):
        markerCorners, markerIds, rejectedCandidates = \
            cv2.aruco.detectMarkers(img, self.dict, parameters=self.params)
        return dict(corners=markerCorners, ids=markerIds)
    
    def draw(self, img, data, scale=1.):
        if len(data['corners']) > 0:
            if scale!=1:
                for d_idx, d in enumerate(data['corners']):
                    data['corners'][d_idx] = d * scale
            cv2.aruco.drawDetectedMarkers(img, data['corners'], data['ids'])
    
    def show_arucos(self, img, data):
        for c, id in zip(data['corners'][0], data['ids'][0]):
            w1 = int(c[:,0].min())
            w2 = int(c[:,0].max())
            h1 = int(c[:,1].min())
            h2 = int(c[:,1].max())
            crop = img[h1:h2, w1:w2]
            cv2.imshow('Aruco Crop', crop)
            cv2.waitKey()


if __name__=='__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Aruco marker detection experiment')
    parser.add_argument('--target-ms', help='take image every X ms', type=float, default=50)
    parser.add_argument('--video-name', help='path to video', type=str, default=None)
    parser.add_argument('--basler-id', help='basler cam id', type=str, default='22594467')
    parser.add_argument('--aruco-size', help='aruco size [4,5,6]', type=int, default=-1)
    parser.add_argument('--sys-id', help='system id', type=str, default='gen2_basler_sys0.11_sh_1')
    parser.add_argument('--debug', help='debug detected markers', action='store_true')
    args = parser.parse_args()

    assert args.aruco_size in (-1,4,5,6)
    aruco_type = cv2.aruco.DICT_4X4_1000 if args.aruco_size==4 else \
                 cv2.aruco.DICT_5X5_1000 if args.aruco_size==5 else \
                 cv2.aruco.DICT_6X6_1000 if args.aruco_size==5 else \
                 cv2.aruco.DICT_ARUCO_ORIGINAL
    aruco_detector = ArucoDetector(aruco_type = aruco_type)
    # config.WHITE_BALANCE_FROM_STICKERS = False
    config.CAMERA_ID = args.sys_id
    print(config.CAMERA_ID)
    args.playback = args.video_name is not None
    from cv_blocks.video_reader.base_proc import BaseImageGrabber
    if args.playback:
        base_rec_path = args.video_name.split('_L.avi')[0]
        my_reader = BaseImageGrabber('playback', base_rec_path, target_ms=args.target_ms)
    else:
        camera_dict = dict(left=dict(cam_id='Basler daA1280-54uc (%s)' % args.basler_id, pixel_format='RGB8',
                           binning_value=2, height=480, width=640))
        my_reader = BaseImageGrabber('basler', camera_dict, target_ms=args.target_ms, img_buff_shape=(480, 640, 3))
    # video_writer = VideoWriter('barcode',video_path)

    my_proc = Process(target=my_reader.run)
    my_proc.start()
    freeze = False
    while True:
        t_grab = time.time()
        if not freeze:
            img_data, _, _ = my_reader.grab(can_skip=True)
        else:
            img_data = None
        if img_data:
            img_l = img_data['data']['left']['img']
            img_l = cv2.rotate(img_l, cv2.ROTATE_90_CLOCKWISE)
            # Detect Aruco
            # ============
            detected_aruco = aruco_detector.detect(img_l)

            # Draw markers
            # ============
            img_viz = cv2.resize(img_l.copy(), (480,640))
            scale = img_viz.shape[0] / float(img_l.shape[0])
            aruco_detector.draw(img_viz, detected_aruco, scale=scale)

            cv2.imshow('Image', img_viz)
            if args.playback and args.debug and len(detected_aruco['corners']) > 0:
                aruco_detector.show_arucos(img_l, detected_aruco)
        key = cv2.waitKey(1) & 0xFF
        # video_writer._outputMovieL.writeFrame(img_l)
        if key == ord("q"):
            cv2.destroyAllWindows()
            break
        if key == ord("w"):
            freeze = not freeze

        loop_time = (time.time() - t_grab) * 1e3
        if loop_time > args.target_ms:
            print('Warning: grab + display took %.2fms' % loop_time)
        else:
            time.sleep((args.target_ms - loop_time) * 1e-3)
    my_proc.terminate()
    
