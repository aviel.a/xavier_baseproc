import argparse
import os
from cv_blocks.localization.localizer.map_localizer import MapLocalizer
from cv_blocks.localization.place_recognition.utils import viz_image_matches, get_sparse_frames_from_video
import pickle
import cv2

DEFAULT_MASK = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'default_mask.png')
DEFAULT_VIZ = '/tmp/map_loc_viz.mp4'

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Localize Video to SLAM map')
    parser.add_argument('--calib-name', help='input calibration model',type=str, required=True)
    parser.add_argument('--video-in', help='path to video file to be localized',type=str, required=True)
    parser.add_argument('--slam-map', help='path to pre-generated SLAM map',type=str, required=True)
    parser.add_argument('--video-map', help='path to video file from which map was generated',type=str, required=True)
    parser.add_argument('--mask', help='path to image feature mask',type=str, default=DEFAULT_MASK)
    parser.add_argument('--viz-out', help='path to visualization',type=str, default=DEFAULT_VIZ)
    parser.add_argument('--img-every', help='localize every X frames',type=int, default=20)
    parser.add_argument('--max-imgs', help='max images to localize',type=int, default=500)
    parser.add_argument('--rotate', help='transpose video in', action='store_true')
    args = parser.parse_args()

    # Open camera config
    # ==================
    curr_dir = os.path.dirname(os.path.realpath(__file__))
    walkout_calib_dir = os.path.join(curr_dir, '..', '..', 'calibration', 'models')
    input_calib_dir = os.path.join(walkout_calib_dir, args.calib_name)
    input_calib_file = os.path.join(input_calib_dir, 'calib.pkl')
    assert os.path.isfile(input_calib_file) 
    conf_data = pickle.load(open(input_calib_file, 'rb'))
    cam_conf = dict(K=conf_data['intrinsics']['left']['K'],
                    dist_coeffs=conf_data['intrinsics']['left']['dist_coeffs'])

    img_mask = cv2.imread(args.mask, -1)
    localizer = MapLocalizer(args.slam_map, img_mask)
    camera = cv2.VideoCapture(args.video_in)
    map_kf_idx, map_frames_idx = localizer.anchor_map.get_kf_frame_ids()
    map_frames = get_sparse_frames_from_video(args.video_map, map_frames_idx)
    video_writer = cv2.VideoWriter(args.viz_out, cv2.VideoWriter_fourcc('M','J','P','G'), 5, (960,640))
    for k in range(args.max_imgs * args.img_every):
        ret, img = camera.read()
        if k % args.img_every != 0:
            continue
        if not ret: 
            break
        if args.rotate:
            img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
        est_pose, inlier_info = localizer.localize_frame(img, cam_conf)
        # Visualize matches
        # =================
        if est_pose is not None:
            t = est_pose.t().flatten(); r = est_pose.r().flatten()
            cv2.putText(img, 'frame 6DOF:', (100, 200), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
            cv2.putText(img, 'pos:(%.2f, %.2f, %.2f)' % (t[0], t[1], t[2]), (100, 220), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
            cv2.putText(img, 'rot:(%.2f, %.2f, %.2f)' % (r[0], r[1], r[2]), (100, 240), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        else:
            cv2.putText(img, 'frame Loc. FAILED:', (100, 200), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))

        kf_img = map_frames[map_frames_idx[map_kf_idx.index(inlier_info['kf_idx'])]].copy()
        t = inlier_info['kf_pose'].t().flatten(); r = inlier_info['kf_pose'].r().flatten()
        cv2.putText(kf_img, 'map 6DOF:', (100, 200), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        cv2.putText(kf_img, 'pos:(%.2f, %.2f, %.2f)' % (t[0], t[1], t[2]), (100, 220), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        cv2.putText(kf_img, 'rot:(%.2f, %.2f, %.2f)' % (r[0], r[1], r[2]), (100, 240), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0,0,255))
        img_viz = viz_image_matches(img, kf_img, inlier_info['img_feats'], inlier_info['map_feats'], viz=False)
        video_writer.write(img_viz)