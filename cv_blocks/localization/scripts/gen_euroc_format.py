import argparse
from genericpath import exists
import os
import cv2
import pandas as pd
from collections import OrderedDict


DEFAULT_MASK = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'default_mask.png')
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Prepare data to euroc format')
    parser.add_argument('--video-in', help='path to video file to be localized',type=str, required=True)
    parser.add_argument('--output-dir', help='path to video file to be localized',type=str, required=True)
    parser.add_argument('--mask-left', help='path to mask of left frame', type=str, default=DEFAULT_MASK)
    parser.add_argument('--mask-right', help='path to mask of right frame', type=str, default=DEFAULT_MASK)
    parser.add_argument('--rotate', help='transpose video in', action='store_true')
    parser.add_argument('--gray', help='turn to gray images', action='store_true')
    parser.add_argument('--max-frames', help='max # of frames to run', type=int, default=None)
    args = parser.parse_args()

    video_left = args.video_in
    video_right = video_left.replace('_L.avi', '_R.avi')
    assert os.path.exists(video_left) and os.path.exists(video_right)
    os.makedirs(os.path.join(args.output_dir, 'cam1', 'data'), exist_ok=True)

    # Create left video
    # =================
    left_frames = []
    left_dir = os.path.join(args.output_dir, 'cam0', 'data')
    os.makedirs(left_dir, exist_ok=True)
    left_reader = cv2.VideoCapture(video_left)
    mask_left = cv2.imread(args.mask_left)
    img_idx = 0
    start_ts = 1413393212255760000 # Euroc hack
    while True:
        img_ts = img_idx + start_ts
        img_name = '%019d.png' % img_ts
        success, img = left_reader.read()
        if not success:
            break
        # Rotate
        if args.rotate:
            img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
        # Resize
        if img.shape[0]!=640:
            fx = fy = 640 / float(img.shape[0])
            img = cv2.resize(img, (0, 0), fx=fx, fy=fy)
        # Apply mask
        img[mask_left==0] = 0
        if args.gray:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(os.path.join(left_dir, img_name), img)
        left_frames.append(OrderedDict(img_ts='%019d' % img_ts, filename=img_name))
        img_idx+=1
        if args.max_frames is not None and img_idx >= args.max_frames:
            break
    pd.DataFrame(left_frames).to_csv(os.path.join(left_dir, '..', 'data.csv'), index=False, columns=('img_ts', 'filename'))

    # Create right video
    # =================
    right_frames = []
    right_dir = os.path.join(args.output_dir, 'cam1', 'data')
    os.makedirs(right_dir, exist_ok=True)
    right_reader = cv2.VideoCapture(video_right)
    mask_right = cv2.imread(args.mask_right)
    img_idx = 0
    while True:
        img_ts = img_idx + start_ts
        img_name = '%019d.png' % img_ts
        success, img = right_reader.read()
        if not success:
            break
        # Rotate
        if args.rotate:
            img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
        # Resize
        if img.shape[0]!=640:
            fx = fy = 640 / float(img.shape[0])
            img = cv2.resize(img, (0, 0), fx=fx, fy=fy)
        # Apply mask
        img[mask_right==0] = 0
        if args.gray:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(os.path.join(right_dir, img_name), img)
        right_frames.append(OrderedDict(img_ts='%019d' % img_ts, filename=img_name))
        img_idx+=1
        if args.max_frames is not None and img_idx >= args.max_frames:
            break
    pd.DataFrame(right_frames).to_csv(os.path.join(right_dir, '..', 'data.csv'), index=False, columns=('img_ts', 'filename'))