import enum
from cart_blocks.cart_content_handler import ErrorType
from cv_blocks.pile_indicator.pile_disp_indicator import PileDisparityIndicator

class PileState(enum.Enum):
    NoPile = 1
    MaybePile = 2
    MaybeNoPile = 3
    PileDetected = 4

VALID_METHODS = ('disparity_indication')
class PileIndicator(object):
    '''
    Indicate pile events inside cart
    '''
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.method = config.get('method', 'disparity_indication')
            self.debug = config.get('debug', False)
            self.offline_viz = config.get('offline_viz', False)
            assert self.offline_viz==False, 'offline feature only'
            self.check_every_n_on_pile = config.get('check_every_n_on_pile', 1)
            self.check_every_n_on_maybe_pile = config.get('check_every_n_on_maybe_pile', 1)
            self.check_every_n_on_no_pile = config.get('check_every_n_on_no_pile', 10)
            self.n_detections_to_consider_pile = config.get('n_detections_to_consider_pile', 3)
    
    def __init__(self, md_module, pi_config=dict()):
        print('Initializing Pile Indication Handler', flush=True)
        self.params = self.Config(pi_config)
        assert self.params.method in VALID_METHODS
        self.md_module = md_module
        assert hasattr(md_module, 'cart_config') and 'edge_calib' in md_module.cart_config
        self.edge_calib = md_module.cart_config['edge_calib']
        self.n_times_pile_detected = 0
        self.n_times_no_pile_detected = 0
        self.pile_indication = False
        
        if self.params.method=='disparity_indication':
            self.metric = PileDisparityIndicator(self.edge_calib['front'])
        assert hasattr(self.metric, 'calculate_pile_metric')
        self.state = PileState.NoPile
        if self.params.offline_viz:
            self.viz_plot = None
            self.viz_frames = []
            self.viz_metric = []
    
    def visualize(self, frame, is_pile, pile_score):
        from matplotlib import pyplot as plt 
        self.viz_frames.append(frame)
        self.viz_metric.append(pile_score)
        fig1, (ax1) = plt.subplots(nrows=1, ncols=1) if self.viz_plot is None else self.viz_plot
        ax1.cla()
        max_samples = 500
        fig1.suptitle("Pile Detected: %r" % is_pile, fontsize=16)
        ax1.plot(self.viz_frames[-max_samples:], self.viz_metric[-max_samples:], color='green', marker='o')
        ax1.set_xlabel('Frame #')
        ax1.set_ylabel('Pile Metric')
        ax1.set_ylim((0.5, 1.5))
        plt.pause(0.001)
        self.viz_plot = fig1, (ax1)

    def reset(self):
        self.state = PileState.NoPile
        self.n_times_pile_detected = 0
        self.n_times_no_pile_detected = 0
        self.pile_indication = False
    
    def reload_calib(self):
        self.edge_calib = self.md_module.cart_config['edge_calib']
        if self.params.method=='disparity_indication':
            self.metric.edge_calib = self.edge_calib['front']
    
    def should_check_pile(self):
        if not hasattr(self.md_module, 'global_frame_num'):
            return False
        
        # Don't check if during event
        if self.md_module.cart_bottom_detector.is_during_event():
            return False
        
        # Don't check if occlusion is active
        if hasattr(self.md_module, 'occlusion_detector'):
            if hasattr(self.md_module.occlusion_detector, 'times_failed'):
                tf_buffer = self.md_module.occlusion_detector.times_failed
                if 'front' in  tf_buffer and tf_buffer['front'] > 0:
                    return False

        check_every_x_frames = self.params.check_every_n_on_pile \
            if self.state==PileState.PileDetected else self.params.check_every_n_on_maybe_pile \
            if self.state in (PileState.MaybePile, PileState.MaybeNoPile) else self.params.check_every_n_on_no_pile
        
        return self.md_module.global_frame_num % check_every_x_frames == 0
    
    def is_pile_in_cart(self):
        return self.pile_indication

    def step(self):
        if not self.should_check_pile():
            return

        if self.params.method=='disparity_indication':
            disp_image = self.md_module.buffer_manager.get('disparity')
            is_pile, pile_score = self.metric.calculate_pile_metric(disp_image)
        
        if self.params.offline_viz:
            self.visualize(self.md_module.global_frame_num, is_pile, pile_score)
        
        next_state = self.state
        if self.state==PileState.NoPile:
            if is_pile:
                self.n_times_pile_detected += 1
                next_state = PileState.MaybePile
        elif self.state==PileState.MaybePile:
            if is_pile:
                self.n_times_pile_detected += 1
                if self.n_times_pile_detected > self.params.n_detections_to_consider_pile:
                    next_state = PileState.PileDetected
            else:
                self.n_times_pile_detected = 0
                next_state = PileState.NoPile
        elif self.state==PileState.PileDetected:
            if not is_pile:
                self.n_times_no_pile_detected += 1
                next_state = PileState.MaybeNoPile
        elif self.state==PileState.MaybeNoPile:
            if not is_pile:
                self.n_times_no_pile_detected += 1
                if self.n_times_no_pile_detected > self.params.n_detections_to_consider_pile:
                    next_state = PileState.NoPile
            else:
                self.n_times_no_pile_detected = 0
                next_state = PileState.PileDetected

        state_transition = (self.state==PileState.MaybePile and next_state==PileState.PileDetected) or \
            (self.state==PileState.MaybeNoPile and next_state==PileState.NoPile)
        
        if self.params.debug and next_state!=self.state:
            print('PileIndicationHandler: curr_state: %s, next_state:%s, n_pile: %d, n_no_pile: %d' % \
                (self.state.name, next_state.name, self.n_times_pile_detected, self.n_times_no_pile_detected), flush=True)
        self.state = next_state
        if state_transition:
            self.n_times_no_pile_detected = 0
            self.n_times_pile_detected = 0
            print('PileIndicationHandler: Cart state is %s' % (self.state.name), flush=True)
            # Add indication if moved to pile state
            if self.state==PileState.PileDetected:
                self.pile_indication = True
                if hasattr(self.md_module, 'cart_content_handler'):
                    self.md_module.cart_content_handler.add_error(ErrorType.CartIsFull)
            elif self.state==PileState.NoPile:
                self.pile_indication = False
                if hasattr(self.md_module, 'cart_content_handler'):
                    self.md_module.cart_content_handler.remove_error(ErrorType.CartIsFull)