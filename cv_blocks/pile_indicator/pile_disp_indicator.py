import numpy as np
import cv2

class PileDisparityIndicator(object):
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.valid_th = config.get('valid_th', 1.1)
            self.use_initial_edge_values = config.get('use_initial_edge_values', True)
    '''
    This class implements pile detection using disparity image
    '''
    def __init__(self, edge_calib, pdi_config=dict()):
        self.params = self.Config(pdi_config)
        assert isinstance(edge_calib, dict) and \
            'left_edge' in edge_calib and 'right_edge' in edge_calib and 'far_edge' in edge_calib
        self.edge_calib = edge_calib
        self.edge_frac = 0.5
        self.profile_vals = self.generate_xy_profile(self.edge_frac)
        self.empty_cart_values = None
    
    def generate_xy_profile(self, edge_frac):
        dx_left = self.edge_calib['left_edge']['p2'][0] - self.edge_calib['left_edge']['p1'][0]
        dy_left = self.edge_calib['left_edge']['p2'][1] - self.edge_calib['left_edge']['p1'][1]
        x_start = self.edge_calib['left_edge']['p1'][0] + (dx_left * edge_frac)
        y_val = self.edge_calib['left_edge']['p1'][1] + (dy_left * edge_frac)
        # compute final point
        # y0 + dy_right * frac_right = y_val => (y_val - y0) / dy_right = frac_right
        dy_right = self.edge_calib['right_edge']['p2'][1] - self.edge_calib['right_edge']['p1'][1]
        frac_right = (y_val - self.edge_calib['right_edge']['p1'][1]) / dy_right
        dx_right = self.edge_calib['right_edge']['p2'][0] - self.edge_calib['right_edge']['p1'][0]
        x_end = self.edge_calib['right_edge']['p1'][0] + dx_right * frac_right

        return dict(x_start=x_start, x_end=x_end, y=y_val)

    def generate_disparity_profile(self, disp_image, edge_frac):
        if edge_frac!=self.edge_frac:
            self.profile_vals = self.generate_xy_profile(self.edge_frac)

        h, w = disp_image.shape[:2]
        profile = []
        x_start = int(self.profile_vals['x_start'] * w)
        x_end = int(self.profile_vals['x_end'] * w)
        y_val = int(self.profile_vals['y'] * h)
        for x_val in range(x_start, x_end):
            profile.append(disp_image[y_val, x_val])
        profile = np.array(profile)
        ret_dict = dict(x=np.arange(x_start, x_end), disp=profile)
        return ret_dict
    
    def calculate_pile_metric(self, disparity):
        default_edge_frac = 0.5
        disp_profile = self.generate_disparity_profile(disparity, default_edge_frac)
        # Calculate ratio between edge values and center values
        profile_size = disp_profile['x'].shape[0]
        disp_edge_size = int(profile_size * 0.1)
        disp_edge_l = disp_profile['disp'][:disp_edge_size]
        disp_edge_r = disp_profile['disp'][-disp_edge_size:]
        disp_mid = disp_profile['disp'][disp_edge_size:-disp_edge_size]
        edge_l = disp_edge_l.mean()
        edge_r = disp_edge_r.mean()
        # score_mid = disp_mid.mean()
        score_mid = np.percentile(disp_mid, 60)
        if self.empty_cart_values is None:
            # Assume empty cart on first calculation
            self.empty_cart_values = dict(edge_l=edge_l, edge_r=edge_r, score_mid=score_mid)
        else:
            if self.params.use_initial_edge_values:
                # Use initial edge values to prevent case where pile is on the edges 
                edge_l = self.empty_cart_values['edge_l']
                edge_r = self.empty_cart_values['edge_r']
        score_edge = (edge_l + edge_r) / 2
        is_pile_score = score_mid / score_edge
        is_pile_event = is_pile_score > self.params.valid_th

        return is_pile_event, is_pile_score
    