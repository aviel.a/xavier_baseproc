import misc
from skvideo import io
import os
import time
import json
from config import config
import cv2
from misc import print_formatted_message

class VideoWriter(object):

    def __init__(self, camera_id, camera_info, movie_path=None):
        self.camera_id = camera_id
        if movie_path is None:
            movie_path = self.gen_movie_name()
        self.movie_path = movie_path
        dir = os.path.dirname(self.movie_path)
        os.makedirs(dir, exist_ok=True)
        self.save_movie_properties_file(self.movie_path)
        self.video_frame_number = 0
        self.set_pause_policy(self.movie_path)

        movie_data = dict(event_type='saving_video', event_id=-1,
                            event_payload=dict(local_path=os.path.dirname(self.movie_path),
                                               video_name=os.path.basename(self.movie_path)))
        print_formatted_message(movie_data, bold=False)

        dict_in = dict()
        for camera_name, ci in camera_info.items():
            dict_in[camera_name] = {'-r': str(30), '-s': '{}x{}'.format(ci['width'], ci['height']), '-pix_fmt': 'bgr24'}
        dict_out = {'-r': str(30), '-c:v': 'libx264', '-crf': str(10), '-preset': 'ultrafast'}
        # Define the codec and create VideoWriter object
        self._outputMovieL = io.FFmpegWriter('{}_L.avi'.format(self.movie_path), inputdict=dict_in['left'], outputdict=dict_out)
        self._outputMovieR = io.FFmpegWriter('{}_R.avi'.format(self.movie_path), inputdict=dict_in['right'], outputdict=dict_out)
        self._outputMovieB = io.FFmpegWriter('{}_B.avi'.format(self.movie_path), inputdict=dict_in['back'], outputdict=dict_out)

    def write_images(self,frameL,frameR,frameB):
        # Skip frame in case of "pause"
        if self.pause_video:
            return
        # Write frames
        self._outputMovieL.writeFrame(frameL)
        self._outputMovieR.writeFrame(frameR)
        self._outputMovieB.writeFrame(frameB)
        # Propagate video frame number
        self.video_frame_number += 1
    
    def set_pause_policy(self, movie_path):
        # Pause/Resume op is currently enabled only for onboarding videos
        movie_name = os.path.basename(movie_path)
        self.enable_pause_resume = movie_name.startswith('onboarding')
        print('VideoWriter: Enable Pause/Resume Policy: %r' % self.enable_pause_resume, flush=True)

        if self.enable_pause_resume:
            self.pause_video = True # start movie as pause, until first barcode scan/first indication
        else:
            self.pause_video = False


    def set_pause(self, val):
        if not self.enable_pause_resume:
            return
        assert val in (False, True)
        if val!=self.pause_video:
            print('VideoWriter[Video Frame %d]: Setting pause value=%r. curr value=%r' % (self.video_frame_number, val, self.pause_video), flush=True)
            self.pause_video = val
    
    def frame(self):
        return self.video_frame_number

    def write_images_dict(self,dict):
        self.write_images(dict['left']['img'], dict['right']['img'], dict['back']['img'])

    def save_movie_properties_file(self, mdName):
            movie_properties = {}
            movie_properties['camera_id'] = self.camera_id
            movie_properties['img_reduction'] = 0.5
            movie_properties['movie_format'] = 'compressed'
            movie_properties['git_branch'] = misc.get_git_branch_name()
            movie_properties['git_version'] = misc.get_git_version_name()

            path = mdName + '_prop.json'
            os.makedirs(os.path.dirname(path),exist_ok=True)
            with open(path, 'w') as outfile:
                json.dump(movie_properties, outfile)

    def __del__(self):
        # print('\033[93m','Closing VideoWriter','\033[0m')
        self._outputMovieL.close()
        self._outputMovieR.close()
        self._outputMovieB.close()


    def get_movie_path(self):
        return self.movie_path

    def gen_movie_name(self):
        mdName = config.SAVE_REALTIME_MOVIE_PREFIX + misc.getTimeAndDate()
        year = str(time.gmtime().tm_year)
        mon = str(time.gmtime().tm_mon).zfill(2)
        day = str(time.gmtime().tm_mday).zfill(2)
        date = '{}-{}-{}'.format(year, mon, day)
        dir = os.path.join(config.MOVIES_PATH, date)
        full_path = os.path.join(dir, mdName)
        return full_path
    
class OpenCVGsteamerVideoWriter(VideoWriter):
    def __init__(self, camera_id, camera_info, movie_path=None, quality_fraction=1., time_info=None):
        print('\033[93m','Opening OpenCVGsteamerVideoWriter','\033[0m')
        self.camera_id = camera_id
        if movie_path is None:
            if time_info is not None:
                movie_path = os.path.join(config.MOVIES_PATH, time_info['date'],
                                          config.SAVE_REALTIME_MOVIE_PREFIX + time_info['timestamp'])
            else:
                movie_path = self.gen_movie_name()
        self.movie_path = movie_path
        dir = os.path.dirname(self.movie_path)
        os.makedirs(dir, exist_ok=True)
        self.save_movie_properties_file(self.movie_path)
        self.baseline_bitrate = 6500000
        self.effective_bitrate = int(self.baseline_bitrate * quality_fraction)
        self.video_frame_number = 0
        self.set_pause_policy(self.movie_path)

        movie_data = dict(event_type='saving_video', event_id=-1,
                            event_payload=dict(local_path=os.path.dirname(self.movie_path),
                                               video_name=os.path.basename(self.movie_path)))
        print_formatted_message(movie_data, bold=False)

        # TODO - bitrate can result different quality when image size changes, should check if we want to change image size
        gst_str_L = ('appsrc ! videoconvert ! omxh264enc profile=high bitrate={} !' 'video/x-h264,quality-level=3' '! avimux !  filesink location={}').format(self.effective_bitrate, self.movie_path + '_L.avi')
        gst_str_R = ('appsrc ! videoconvert ! omxh264enc profile=high bitrate={} !' 'video/x-h264,quality-level=3' '! avimux !  filesink location={}').format(self.effective_bitrate, self.movie_path + '_R.avi')
        gst_str_B = ('appsrc ! videoconvert ! omxh264enc profile=high bitrate={} !' 'video/x-h264,quality-level=3' '! avimux !  filesink location={}').format(self.effective_bitrate, self.movie_path + '_B.avi')

        # Define the codec and create VideoWriter object
        fps = 30
        self._outputMovieL = cv2.VideoWriter(gst_str_L, cv2.CAP_GSTREAMER, 0, fps,(camera_info['left']['width'], camera_info['left']['height']))
        self._outputMovieR = cv2.VideoWriter(gst_str_R, cv2.CAP_GSTREAMER, 0, fps,(camera_info['right']['width'], camera_info['right']['height']))
        self._outputMovieB = cv2.VideoWriter(gst_str_B, cv2.CAP_GSTREAMER, 0, fps,(camera_info['back']['width'], camera_info['back']['height']))

    def write_images(self,frameL,frameR,frameB):
        # Skip frame in case of "pause"
        if self.pause_video:
            return
        # Write frames
        self._outputMovieL.write(frameL)
        self._outputMovieR.write(frameR)
        self._outputMovieB.write(frameB)
        # Propagate video frame number
        self.video_frame_number += 1
    
    def __del__(self):
        print('\033[93m','Closing OpenCVGsteamerVideoWriter','\033[0m')
        self._outputMovieL.release()
        self._outputMovieR.release()
        self._outputMovieB.release()


class ExactVideoWriter(VideoWriter): #tf - to remove this
    def __init__(self, camera_id, movie_path=None):
        print('\033[93m','Opening ExactVideoWriter','\033[0m')
        self.camera_id = camera_id
        if movie_path is None:
            movie_path = self.gen_movie_name()
        self.movie_path = movie_path
        dir = os.path.dirname(self.movie_path)
        os.makedirs(dir, exist_ok=True)
        self.save_movie_properties_file(self.movie_path)
        movie_data = dict(event_type='saving_video', event_id=-1,
                            event_payload=dict(local_path=os.path.dirname(self.movie_path),
                                               video_name=os.path.basename(self.movie_path)))
        print_formatted_message(movie_data, bold=False)
        self.frame_idx = 0

    def write_images(self,frameL,frameR,frameB):

            movie_name = os.path.dirname(self.movie_path)
            cv2.imwrite(os.path.join(movie_name, "img_left_%03d.png" % self.frame_idx), frameL)
            cv2.imwrite(os.path.join(movie_name, "img_right_%03d.png" % self.frame_idx), frameR)
            cv2.imwrite(os.path.join(movie_name, "img_back_%03d.png" % self.frame_idx), frameB)
            self.frame_idx += 1

    def __del__(self):
        pass