import numpy as np
import enum
from cv_blocks.video_reader.base_proc import TemplatesDetectedState
import cv2
from cart_blocks.cart_content_handler import ErrorType


class OcclusionState(enum.IntEnum):
    NoOcclusion = 0
    MaybeOcclusion = 1
    Occlusion = 2


class DetectOcclusion:
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.skip_cams = config.get('skip_cams', ())

    def __init__(self, md_module=None, de_config=dict()):
        self.params = self.Config(de_config)
        self.md_module = md_module
        self.cams = ['front', 'back']
        if md_module is not None:
            self.db_lines = dict(front=md_module.mv_manager.camera_config['front']['decision_lines']['top'],
                                 back=md_module.mv_manager.camera_config['back']['decision_lines']['middle'])
        else:
            self.db_lines = dict(front=0.29, back=0.32)
        self.bins = 16
        self.ref_hist = dict(front=np.zeros((self.bins,)), back=np.zeros((self.bins,)))
        self.ref_hist_initiated = False
        self.last_frame_evaluated = 50
        self.times_failed = dict(front=0, back=0)
        self.should_replace_reference = False
        self.evaluate_every = 10
        self.derivatives_th = 8.5
        self.debug = False
        self.read_status_for_occlusion_detection = False
        self.read_status_for_hist_replacement = False
        self.show_occlusion_error_after_x_failures = 3

    def step(self, images_dict, replace_ref, allow_cam_commands, force_replace_ref, allow_skip):
        if self.read_status_for_occlusion_detection or self.read_status_for_hist_replacement:
            front_status, back_status = self.md_module.stream.check_templates_status()
            if front_status != TemplatesDetectedState.Empty:
                if allow_skip:
                    if 'front' in self.params.skip_cams:
                        front_status = TemplatesDetectedState.TemplatesFound
                    if 'back' in self.params.skip_cams:
                        back_status = TemplatesDetectedState.TemplatesFound
                if self.debug:
                    print("Debug Occlusion Detector: templates status: front = {}, back = {}, frame: {}".format(
                        front_status, back_status, self.md_module.global_frame_num), flush=True)
                if self.read_status_for_occlusion_detection:
                    if self.debug:
                        print("Debug Occlusion Detector: checking if there is an occlusion, frame: {}".format(
                            self.md_module.global_frame_num), flush=True)
                    self.detect_occlusion(templates_states=dict(front=front_status, back=back_status), images=images_dict)
                if self.read_status_for_hist_replacement:
                    if self.debug:
                        print("Debug Occlusion Detector: checking if it is possible to replace reference", flush=True)
                    if front_status == TemplatesDetectedState.TemplatesFound and \
                            back_status == TemplatesDetectedState.TemplatesFound and \
                            (not self.ref_hist_initiated or
                             (self.is_occluded(image=images_dict['front'], camera='front') != OcclusionState.Occlusion and
                              self.is_occluded(image=images_dict['back'], camera='back') != OcclusionState.Occlusion)):
                        self.replace_reference(images_dict)
                    else:
                        print("\033[92mOcclusion Detector: Not Replacing reference histogram (occlusion candidate)\033[00m", flush=True)
                self.read_status_for_occlusion_detection = False
                self.read_status_for_hist_replacement = False
        if force_replace_ref:
            if self.debug:
                print("Debug Occlusion Detector: forcing replacement of reference at frame {}.".format(
                    self.md_module.global_frame_num), flush=True)
            self.replace_reference(images_dict)
        elif replace_ref or self.should_replace_reference:
            if self.debug:
                print("Debug Occlusion Detector: trying to replace reference at frame {}. allow_cam_command = {}".format(
                    self.md_module.global_frame_num, allow_cam_commands), flush=True)
            self.should_replace_reference = True
            if allow_cam_commands:
                self.md_module.stream.find_templates()
                self.should_replace_reference = False
                self.read_status_for_hist_replacement = True
        elif self.should_check_for_occlusion():
            if self.debug:
                print("Debug Occlusion Detector: searching templates at frame {}, allow_cam_commands = {}".format(
                    self.md_module.global_frame_num, allow_cam_commands), flush=True)
            if allow_cam_commands:
                self.last_frame_evaluated = self.md_module.global_frame_num
                self.md_module.stream.find_templates()
                self.read_status_for_occlusion_detection = True

    def replace_reference(self, images):
        print("\033[92mOcclusion Detector: Replacing reference histogram\033[00m", flush=True)
        self.ref_hist_initiated = True
        self.replace_reference_histograms(images)
        for cam, num_failed in self.times_failed.items():
            if num_failed >= self.show_occlusion_error_after_x_failures:
                print("\033[91mOcclusion Detector: {} cam is not blocked anymore\033[00m".format(cam), flush=True)
        self.times_failed = dict(front=0, back=0)
        if hasattr(self.md_module, 'cart_content_handler'):
            self.md_module.cart_content_handler.remove_error(ErrorType.CamerasOcclusion)

    def detect_occlusion(self, templates_states, images):
        for cam, state in templates_states.items():
            if state == TemplatesDetectedState.TemplatesNotFound:
                hist_state = self.is_occluded(image=images[cam], camera=cam)
                if self.debug:
                    print("Debug Occlusion Detector: templates not found for camera {}, occlusion status by hist: {}".format(cam, hist_state), flush=True)
                if hist_state == OcclusionState.Occlusion:
                    self.times_failed[cam] += 1
                    if self.times_failed[cam] == self.show_occlusion_error_after_x_failures:
                        print("\033[91mOcclusion Detector: {} cam is blocked!!!!!\033[00m".format(cam), flush=True)
                        if hasattr(self.md_module, 'cart_content_handler'):
                            send_indication = True
                            if hasattr(self.md_module, 'barcode_handler'):
                                # Ignore occlusion indication when barcode scanning is active
                                send_indication = self.md_module.barcode_handler.is_idle() or cam != 'front'
                            if send_indication:
                                self.md_module.cart_content_handler.add_error(ErrorType.CamerasOcclusion)
                elif hist_state == OcclusionState.NoOcclusion:
                    if self.times_failed[cam] >= self.show_occlusion_error_after_x_failures:
                        print("\033[91mOcclusion Detector: {} cam is not blocked anymore\033[00m".format(cam),
                              flush=True)
                    self.times_failed[cam] = 0
            elif self.times_failed[cam] > 0:
                hist_state = self.is_occluded(image=images[cam], camera=cam)
                if hist_state != OcclusionState.Occlusion:
                    print("\033[91mOcclusion Detector: {} cam is not blocked anymore\033[00m".format(cam), flush=True)
                    self.times_failed[cam] = 0
        if sum([failures for failures in self.times_failed.values()]) == 0:
            if hasattr(self.md_module, 'cart_content_handler'):
                self.md_module.cart_content_handler.remove_error(ErrorType.CamerasOcclusion)

    def should_check_for_occlusion(self):
        if self.md_module.global_frame_num - self.last_frame_evaluated >= self.evaluate_every and self.ref_hist_initiated:
            return True
        return False

    def replace_reference_histograms(self, images):
        for camera in self.cams:
            self.ref_hist[camera] = self.calculate_histogram(images[camera], camera)

    def predict_occlusion_by_hist(self, max_el, l2_diff):
        # first element in each bracket is the minimum portion of the bin with the highest diff, from the overall diff.
        # second element in each bracket is the minimum overall l2 diff
        # Both condition should be fulfilled to consider occlusion (for strong conditions) or maybe occlusion (weak conditions)
        weak_conditions = [(0.3, 0.01), (0.4, 0.007)]
        strong_conditions = [(0.4, 0.025), (0.3, 0.03), (0, 0.05)]
        for condition in strong_conditions:
            if max_el >= condition[0] and l2_diff >= condition[1]:
                return OcclusionState.Occlusion
        for condition in weak_conditions:
            if max_el >= condition[0] and l2_diff >= condition[1]:
                return OcclusionState.MaybeOcclusion
        return OcclusionState.NoOcclusion

    def is_occluded(self, image, camera):
        hist = self.calculate_histogram(image=image, camera=camera)
        max_el_percentage, total_diff = self.histogram_diff(hist, camera)
        state = self.predict_occlusion_by_hist(max_el_percentage, total_diff)
        if state in [OcclusionState.Occlusion, OcclusionState.MaybeOcclusion]:
            occlusion_by_laplacian = self.validate_occlusion_by_laplacian(image, camera)
            if not occlusion_by_laplacian:
                if state == OcclusionState.Occlusion:
                    if self.debug:
                        print("Debug Occlusion Detector: changed camera {} status from Occlusion to".format(camera) +
                              " MaybeOcclusion because of laplacian high values", flush=True)
                    state = OcclusionState.MaybeOcclusion
                else:
                    if self.debug:
                        print("Debug Occlusion Detector: changed camera {} status from MaybeOcclusion to".format(camera) +
                              " NoOcclusion because of laplacian high values", flush=True)
                    state = OcclusionState.NoOcclusion
        if self.debug and state in [OcclusionState.Occlusion, OcclusionState.MaybeOcclusion]:
            print("Debug Occlusion Detector: camera: {}".format(camera), flush=True)
            print("Debug Occlusion Detector: max element percentage: {}".format(round(max_el_percentage, 2)), flush=True)
            print("Debug Occlusion Detector: total diff: {}".format(round(total_diff, 3)), flush=True)
        return state

    def histogram_diff(self, hist, camera):
        dist = np.power((self.ref_hist[camera]-hist), 2)
        return dist.max() / (dist.sum() + 1e-8), dist.sum()

    def calculate_histogram(self, image, camera):
        hist = np.histogram(image[int(image.shape[0]*self.db_lines[camera]):, :], self.bins)[0]
        hist = np.around(hist / hist.sum(), 2)
        return hist

    def validate_occlusion_by_laplacian(self, image, camera):
        lap = cv2.Laplacian(image[int(image.shape[0]*self.db_lines[camera]):, :], cv2.CV_64F, ksize=1)
        lap_avg = np.mean(np.abs(lap))
        if self.debug:
            print("laplacian value: {} for camera {}".format(lap_avg, camera))
        if lap_avg < self.derivatives_th:
            return True
        return False


if __name__ == '__main__':
    import argparse
    import getpass
    import pandas as pd
    import json
    import os
    from cv_blocks.white_balance.template_matching import TemplateMatching

    def locate_movie(movie_name, root_dir):
        optional_dirs = [root_dir]
        for optional_dir in optional_dirs:
            for root, dirs, files in os.walk(optional_dir):
                path = os.path.join(root, movie_name)
                if os.path.isfile(path) or os.path.isdir(path):
                    return root
        return None

    # mode = 'histogram'
    mode = 'gradients'
    DEFAULT_CSV = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               '../../ourFirstCNN/recording_list/default.csv')
    DEFAULT_ROOT_DIR = '/home/%s/' % (getpass.getuser())
    parser = argparse.ArgumentParser(description='generate event trigger images')
    parser.add_argument('--movies-file', help='input video',type=str, default=DEFAULT_CSV)
    parser.add_argument('--root-dir', help='path to all recordings root directory',type=str, default=DEFAULT_ROOT_DIR)
    args = parser.parse_args()
    movie_list = pd.read_csv(args.movies_file, comment='#', names=['movie'], index_col=False, skip_blank_lines=True)['movie'].tolist()
    for single_movie in movie_list:
        movie_rec = locate_movie(single_movie, args.root_dir)
        if movie_rec is None:
            raise NameError('movie {} was not found!'.format(single_movie))
        full_rec_path = os.path.join(movie_rec, single_movie)
        movie_name_wo_suffix = full_rec_path.split('_L.avi')[0]

        cap_L = cv2.VideoCapture(full_rec_path)
        cap_B = cv2.VideoCapture(full_rec_path.replace('_L.avi', '_B.avi'))
        prop_file = movie_name_wo_suffix + '_prop.json'
        with open(prop_file) as f:
            camera_id = json.load(f)['camera_id']
        template_match = TemplateMatching(camera_id=camera_id, monitor_values=True)
        frame_num = 0
        occlusion_detector = DetectOcclusion()
        while (cap_L.isOpened() and cap_B.isOpened()):
            frame_num += 1
            print(frame_num)
            retL, frame_L = cap_L.read()
            retB, frame_B = cap_B.read()
            if retL and retB:
                frame_L = cv2.rotate(frame_L, cv2.ROTATE_90_CLOCKWISE)
                frame_B = cv2.rotate(frame_B, cv2.ROTATE_90_CLOCKWISE)
                if frame_num >= 50:
                    gray_L = cv2.cvtColor(frame_L, cv2.COLOR_BGR2GRAY)
                    gray_B = cv2.cvtColor(frame_B, cv2.COLOR_BGR2GRAY)
                    gray_images = dict(front=gray_L, back=gray_B)
                    if frame_num >= 50:
                        if mode == 'histogram':
                            if frame_num == 50:
                                occlusion_detector.replace_reference_histograms(gray_images)
                                continue
                            else:
                                states = dict()
                                for cam in ['front', 'back']:
                                    states[cam] = occlusion_detector.is_occluded(gray_images, cam)
                                h, w = frame_L.shape[:2]
                                if states['front'] == OcclusionState.Occlusion:
                                    cv2.rectangle(frame_L, (0, 0), (w, h), (0, 0, 255), 10)
                                elif states['front'] == OcclusionState.MaybeOcclusion:
                                    cv2.rectangle(frame_L, (0, 0), (w, h), (0, 165, 255), 10)
                                if states['back'] == OcclusionState.Occlusion:
                                    cv2.rectangle(frame_B, (0, 0), (w, h), (0, 0, 255), 10)
                                elif states['back'] == OcclusionState.MaybeOcclusion:
                                    cv2.rectangle(frame_B, (0, 0), (w, h), (0, 165, 255), 10)
                        elif mode == 'gradients':
                            for cam, image in gray_images.items():
                                h, w = image.shape[:2]
                                occluded = occlusion_detector.validate_occlusion_by_laplacian(image, cam)
                                if occluded:
                                    relevent_frame = frame_B if cam == 'back' else frame_L
                                    cv2.rectangle(relevent_frame, (0, 0), (w, h), (0, 0, 255), 10)
                        cv2.imshow("Frame L", frame_L[int(frame_L.shape[0]*0.29):,:,:])
                        cv2.imshow("Frame B", frame_B[int(frame_B.shape[0]*0.32):,:,:])
                        key = cv2.waitKey() & 0xFF
                        if key == ord('f'):
                            continue
                        elif key == ord('q'):
                            break
            else:
                break
