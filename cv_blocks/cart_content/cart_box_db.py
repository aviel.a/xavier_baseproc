import numpy as np
from cv_blocks.common.types import BoundingBox2D

class CartBoxDB(object):
    '''
    This class implements a database of static object inside the cart.
    It supports push/pop operations as well as indicating whether an object
    is likely to be in the cart
    '''
    class Config(object):
        def __init__(self, config):
            assert isinstance(config, dict)
            self.same_box_iou_th = config.get('same_box_iou_th', 0.4)
            self.debug = config.get('debug', True)

    def __init__(self, camera_names, cbdb_config=dict()):
        self.params = self.Config(cbdb_config)
        self.camera_names = camera_names 
        self.box_db = dict()
        for cam in self.camera_names:
            self.box_db[cam] = []
    
    def add(self, camera_name, box, force=False):
        assert isinstance(box, BoundingBox2D)
        similar_box_dict = self.get_similar_box(camera_name, box)
        if not similar_box_dict['exist']:
            self.box_db[camera_name].append(box)
            if self.params.debug:
                print('Box DB DEBUG: Adding box to [%s] cam: %s' % (camera_name, box))
        else:
            if force:
                self.box_db[camera_name][similar_box_dict['id']] = box
                if self.params.debug:
                    print('Box DB DEBUG: Overwriting box [%d] to [%s] cam: %s' % \
                        (similar_box_dict['id'], camera_name, box))
            else:
                if self.params.debug:
                    print('Box DB DEBUG: Failed Adding box to [%s] cam: %s. Similar box exists!' % (camera_name, box))


    def remove(self, camera_name, idx):
        if self.params.debug:
            print('Box DB DEBUG: Removing box from [%s] cam: %s' % (camera_name, self.box_db[camera_name][idx]))
        del self.box_db[camera_name][idx]
    
    def get_similar_box(self,camera_name, box):
        ious = np.array([BoundingBox2D.iou(box, cur_bx) for cur_bx in self.box_db[camera_name]])
        if len(ious)==0:
            return dict(exist=False, id=-1)
        best_iou = ious.max()
        if self.params.debug:
            print('Box DB DEBUG: similar box in [%s] cam: best match was IOU=%.2f' % (camera_name, best_iou))
        if best_iou >= self.params.same_box_iou_th:
            return dict(exist=True, id=np.argmax(ious))
        else:
            return dict(exist=False, id=-1)