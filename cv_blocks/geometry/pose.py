import numpy as np
import cv2

class Pose(object):
    '''
    This class implements 6dof camera pose, i.e rotation and translation
    '''
    def __init__(self,input=np.eye(4)):
        if isinstance(input, np.ndarray) and input.shape==(4,4):
            self.Rt = input
        elif isinstance(input, (list, tuple)) and len(input)==2:
            if len(input[0])==3 and len(input[1])==3:
                r = np.reshape(input[0], (3,1)).astype(np.float)
                t = input[1].astype(np.float)
                self.Rt = Pose.from_rt_vecs(r, t)
            if len(input[0])==4 and len(input[1])==3:
                q = np.array(input[0]).astype(np.float)
                t = np.array(input[1]).astype(np.float)
                self.Rt = Pose.from_qt_vecs(q, t)


    @staticmethod
    def from_rt_vecs(r, t):
        '''
        Create pose from rotation and translation vectors
        '''
        ps = np.eye(4, dtype=np.float32)
        R = cv2.Rodrigues(r)[0]
        ps[:3,:3] = R; ps[:3,-1] = t.ravel()
        return ps

    @staticmethod
    def from_qt_vecs(q, t):
        '''
        Create pose from quaternion and translation vector
        '''
        r = Pose.quat2axis(q)
        ps = np.eye(4, dtype=np.float32)
        R = cv2.Rodrigues(r)[0]
        ps[:3,:3] = R; ps[:3,-1] = t.ravel()
        return ps
    
    def __mul__(self, other):
        '''
        2 poses concatenation
        '''
        return Pose(self.Rt.dot(other.Rt))

    def trans_to(self, other):
        '''
        Compute relative pose between 2 poses
        '''
        R12 = other.Rt[:3,:3].T.dot(self.Rt[:3,:3])
        t12 = other.Rt[:3,:3].T.dot(self.Rt[:3,-1] - other.Rt[:3,-1])
        ps = np.eye(4, dtype=np.float32)
        ps[:3,:3] = R12; ps[:3,-1] = t12.ravel()
        return Pose(ps)
    
    def project(self, xyz):
        '''
        Project xyz points to current pose coordinates
        '''
        assert xyz.shape[-1]==3
        xyz_proj = (self.R().dot(np.atleast_2d(xyz).T) + self.t().reshape(3,-1)).T
        assert xyz.shape==xyz_proj.shape
        return xyz_proj

    def inv(self):
        '''
        Return inverse pose
        '''
        R_inv = self.Rt[:3,:3].T
        t = self.Rt[:3,-1]
        t_inv = -R_inv.dot(t)
        ps = np.eye(4, dtype=np.float32)
        ps[:3,:3] = R_inv; ps[:3,-1] = t_inv.ravel()
        return Pose(ps)
    
    def t(self):
        '''
        Return tanslation vector
        '''
        return self.Rt[:3,-1].ravel()

    def R(self):
        '''
        Return rotation matrix
        '''
        return self.Rt[:3,:3]
    
    def r(self):
        '''
        Return rotation vector
        '''
        return cv2.Rodrigues(self.Rt[:3,:3])[0]

    @staticmethod
    def quat2axis(q):
        '''
        Quaternion notation [x,y,z,w]
        taken from http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToAngle/
        '''
        q = np.array(q)
        if q[-1] > 1:
            q /= np.linalg.norm(q)
        [qx , qy, qz, qw] = q
        # angle = 2 * np.acos(qw)
        s = np.sqrt(1-qw * qw); # assuming quaternion normalised then w is less than 1, so term always positive.
        if s < 0.001: # test to avoid divide by zero, s is always positive due to sqrt
            # if s close to zero then direction of axis not important
             # if it is important that axis is normalised then replace with x=1; y=z=0;
            x = qx
            y = qy
            z = qz
        else:
            # Normalise axis
            x = qx / s
            y = qy / s
            z = qz / s
        return np.array([x, y, z])

    def __repr__(self):
        r = cv2.Rodrigues(self.Rt[:3,:3])[0].ravel()
        t = self.Rt[:3,-1].ravel()
        ret = 'Rotation: %s, translation: %s' % (r, t) 
        return ret

if __name__=='__main__':
    ps1 = Pose(np.eye(4))
    ps2 = Pose(np.eye(4))
    ps3 = ps1.inv() * ps2.inv()
    print(ps3)
