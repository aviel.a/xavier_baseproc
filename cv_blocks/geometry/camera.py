import numpy as np
import cv2

class Camera(object):
    '''
    This class implements basic pinhole camera
    '''
    def __init__(self, ps, K ,D=None, Q=None):
        '''
        ps - input camera 6DOF pose
        K - camera matrix
        D - camera distortion coeefiecients
        '''
        self.ps = ps
        self.K = K
        self.D = D
        self.Q = Q
    
    def pix2ray(self, pix):
        '''
        Convert pixel coordinate to ray (x,y) in camera coordinates
        Ray is to be generated by adding 1s column
        '''
        ray_xy = cv2.undistortPoints(pix, self.K, self.D).squeeze()
        return ray_xy
    
    def disp2xyz(self, disparity, uv=None, disp_scale=1./16):
        '''
        Convert disparity map pixels to xyz in camera coordinated
        If uv is None, it will convert the whole pixel grid
        '''
        assert self.Q is not None, 'Q transfromation is missing'
        if uv is not None:
            assert uv.shape[-1]==2
            disp_pts = np.asarray([np.array([p[0],
                                             p[1],
                                             disp_scale * disparity[int(p[1]), int(p[0])]]) for p in np.atleast_2d(uv)])
            xyz = cv2.perspectiveTransform(np.array([disp_pts]), self.Q).squeeze()
        else:
            xyz = cv2.reprojectImageTo3D((disp_scale * disparity).astype(np.float32), self.Q)

        return np.atleast_2d(xyz)
    
    def uvdisp2xyz(self, uv, disp, disp_scale=1./16):
        assert self.Q is not None, 'Q transfromation is missing'
        disp_pts = np.array([uv[0], uv[1], disp_scale * disp])
        xyz = cv2.perspectiveTransform(disp_pts.reshape(-1,1,3), self.Q).squeeze()
        return xyz
    
    def xyz2uv(self, xyz, with_distortion=True):
        '''
        Project xyz in camera coordinates to pixel coordinates
        '''
        D = self.D if with_distortion else np.zeros_like(self.D)
        uv = cv2.projectPoints(xyz, np.zeros((3,1)), np.zeros((3,1)), self.K , D)[0].squeeze()
        # handle points behind camera
        is_visible = xyz[:,-1]>0 # TODO - add points out of frustum

        return uv, is_visible
    
    def project_xyz_other_cam(self, other, xyz_curr, with_distortion=True):
        '''
        Given xyz points in current cam, project it to another camera
        '''
        assert isinstance(other, Camera)
        # Compute relative pose
        curr_T_other = other.ps.trans_to(self.ps)
        # transform xyz to other camera coordinates
        xyz_other = curr_T_other.project(xyz_curr)
        # project onto other camera
        uv_other, is_visible = other.xyz2uv(xyz_other, with_distortion)
        return uv_other, is_visible
    
    def has_stereo(self):
        return self.Q is not None

        