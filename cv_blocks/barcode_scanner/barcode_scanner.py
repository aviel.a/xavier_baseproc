import scanditsdk as sc
import tempfile
import cv2
from cv_blocks.common.types import BoundingBox2D
import os
import yaml
import numpy as np
from config import config


class BarcodeScanner:
    def __init__(self, config_path=None, full_image_shape=(1280, 960), is_gray=True,
                 bbox=None, rotation_op=None, debug=False):
        self.config = self.load_config(config_path)
        assert (config.PRODUCTION_CART and 'password_production' in self.config.keys()) or\
               (not config.PRODUCTION_CART and 'password_rnd' in self.config.keys()), "No scandit password in config file!"
        self.shape_full = full_image_shape
        self.is_gray = is_gray
        self.rotation_op = rotation_op
        self.debug = debug
        assert not self.debug, 'Debug Feature Only!'
        self.bbox = bbox if isinstance(bbox, BoundingBox2D) else \
            BoundingBox2D(dict(xmin=bbox[0], ymin=bbox[1], xmax=bbox[2], ymax=bbox[3])) if bbox is not None else \
            BoundingBox2D(dict(xmin=0, ymin=0, xmax=full_image_shape[1], ymax=full_image_shape[0]))
        self.shape_crop = (self.bbox.h(), self.bbox.w())
        if self.debug:
            self.debug_rec_writer = dict(raw=cv2.VideoWriter('/tmp/barcode_rec_raw.avi', cv2.VideoWriter_fourcc('M','J','P','G'), 20, self.shape_crop[::-1]),
                                         debug=cv2.VideoWriter('/tmp/barcode_rec_debug.avi', cv2.VideoWriter_fourcc('M','J','P','G'), 20, self.shape_crop[::-1]))

        self.initialize_scandit()
        # First scan - verify class is running
        if rotation_op is not None:
            self.scan(np.zeros((self.shape_full[1], self.shape_full[0], 3), dtype=np.uint8))
        else:
            self.scan(np.zeros((self.shape_full[0], self.shape_full[1], 3), dtype=np.uint8))

    def load_config(self, path):
        if path is None:
            path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'scanner_config.yaml')
        return yaml.load(open(path, 'r'))

    def initialize_scandit(self):
        self.descr = sc.ImageDescription()
        self.descr.width = self.shape_crop[1]
        self.descr.height = self.shape_crop[0]
        channels = 1 if self.is_gray else 3
        self.descr.first_plane_row_bytes = self.shape_crop[1] * channels
        self.descr.layout = sc.IMAGE_LAYOUT_RGB_8U if channels == 3 else sc.IMAGE_LAYOUT_GRAY_8U
        self.descr.memory_size = self.shape_crop[0] * self.shape_crop[1] * channels * 8
        self.channels = channels

        # Create a recognition context. Files created by the recognition context and the
        # attached scanner will be written to a temporary directory. In production environment,
        # it should be replaced with writable path which does not get removed between reboots.
        try:
            context_dir = config.BARCODE_LICENSE_DIR
            os.makedirs(context_dir, exist_ok=True)
        except:
            context_dir = os.path.dirname(os.path.abspath(__file__))
            os.makedirs(context_dir, exist_ok=True)
        print('Initializing Scandit context in %s' % context_dir, flush=True)
        password = self.config['password_production'] if config.PRODUCTION_CART else self.config['password_rnd']
        context = sc.RecognitionContext(password, context_dir)

        # Use the single frame preset.
        settings = sc.BarcodeScannerSettings(preset=sc.PRESET_ENABLE_SINGLE_FRAME_MODE)

        # We assume the worst camera system.
        settings.focus_mode = sc.SC_CAMERA_FOCUS_MODE_FIXED

        # We want to scan at most one code per frame.
        # Should be set to a higher number if false positives are likely.
        settings.max_number_of_codes_per_frame = 1

        # Search in the full image and do not check the code location area.
        settings.code_location_constraint_1d = sc.SC_CODE_LOCATION_IGNORE
        # settings.code_location_constraint_2d = sc.SC_CODE_LOCATION_IGNORE

        # No assumptions about the code direction are made.
        settings.code_direction_hint = sc.SC_CODE_DIRECTION_NONE

        settings.enable_symbology(sc.SYMBOLOGY_EAN13, True)
        settings.enable_symbology(sc.SYMBOLOGY_EAN8, True)
        settings.enable_symbology(sc.SYMBOLOGY_UPCA, True)
        # settings.enable_symbology(sc.SYMBOLOGY_QR, True)
        # settings.enable_symbology(sc.SYMBOLOGY_CODE128, True)

        # Change the EAN13 symbology settings to enable scanning of color-inverted barcodes.
        # symbology_settings = settings.symbologies[sc.SYMBOLOGY_EAN13]
        # symbology_settings.color_inverted_enabled = True

        self.scanner = sc.BarcodeScanner(context, settings)
        self.scanner.wait_for_setup_completed()

        self.frame_seq = context.start_new_frame_sequence()

    def scan(self, image, img_viz=None, scan_raw=False):
        rotated = self.rotation_op is not None
        if scan_raw:
            cropped_image = image
            pass
        else:
            cropped_image = self.bbox.crop(image, rotated=rotated).copy()
            if self.is_gray and len(cropped_image.shape) > 2:
                cropped_image = cv2.cvtColor(cropped_image, cv2.COLOR_RGB2GRAY).copy()
        assert cropped_image.shape[:2] == self.shape_crop[:2], "cropped shape: {}, scandit shape: {}".format(cropped_image.shape, self.shape_crop)
        pointer, read_only_flag = cropped_image.__array_interface__['data']
        status = self.frame_seq.process_frame(self.descr, pointer)
        if status.status != sc.RECOGNITION_CONTEXT_STATUS_SUCCESS:
            print("Processing frame failed with code {}: {}".format(
                status.status, status.get_status_flag_message()), flush=True)
            return []
        output_codes = self.scanner.session.newly_recognized_codes
        for code in output_codes:
            print("Barcode found at location ", code.location, ": ", code.data,
                  " (", code.symbology_string, ")", sep="", flush=True)
            if img_viz is not None:
                if self.rotation_op is not None:
                    img_viz = cv2.rotate(img_viz, self.rotation_op)
                ratio_x = self.shape_full[1] / img_viz.shape[1]
                ratio_y = self.shape_full[0] / img_viz.shape[0]
                top_left = (int((self.bbox.x1 + code.location.top_left.x) / ratio_x),
                            int((self.bbox.y1 + code.location.top_left.y) / ratio_y))
                bottom_right = (int((self.bbox.x1 + code.location.bottom_right.x) / ratio_x),
                                int((self.bbox.y1 + code.location.bottom_right.y) / ratio_y))
                cv2.rectangle(img_viz, top_left, bottom_right, (0, 255, 0), 1)
                cv2.putText(img_viz, code.data, top_left, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1)
        if self.debug:
            cropped_viz = cropped_image.copy()
            if self.is_gray:
                cropped_viz = cv2.cvtColor(cropped_viz, cv2.COLOR_GRAY2BGR)
            self.debug_rec_writer['raw'].write(cropped_viz)
            # Localized barcodes
            for code in self.scanner.session.newly_localized_codes:
                top_left = (code.location.top_left.x, code.location.top_left.y)
                bottom_right = (code.location.bottom_right.x, code.location.bottom_right.y)
                cv2.rectangle(cropped_viz, top_left, bottom_right, (255, 0, 0), 1)
            # Recognized barcodes
            for code in output_codes:
                top_left = (code.location.top_left.x, code.location.top_left.y)
                bottom_right = (code.location.bottom_right.x, code.location.bottom_right.y)
                cv2.rectangle(cropped_viz, top_left, bottom_right, (0, 255, 0), 1)
                cv2.putText(cropped_viz, code.data, top_left, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1)
            cv2.imshow('Barcode Scanned Image', cropped_viz)
            self.debug_rec_writer['debug'].write(cropped_viz)

        return output_codes

    def end(self):
        self.frame_seq.end()
