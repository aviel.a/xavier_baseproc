import cv2
import numpy as np

FINAL_LINE_COLOR = (255, 0, 0)
WORKING_LINE_COLOR = (127, 127, 127)

class ManualImageAligner(object):
    def __init__(self, window_name='original', max_points=5, img_size=(128,128), border=32):
        self.window_name = window_name # Name for our window
        self.max_points = max_points
        self.done = False # Flag signalling we're done
        self.current = (0, 0) # Current position, so we can draw the line-in-progress
        self.points = [] # List of points defining our polygon
        self.img_size = img_size
        self.border = border


    def on_mouse(self, event, x, y, buttons, user_param):
        # Mouse callback that gets called for every mouse event (i.e. moving, clicking, etc.)

        if self.done: # Nothing more to do
            return

        if event == cv2.EVENT_MOUSEMOVE:
            # We want to be able to draw the line-in-progress, so update current mouse position
            self.current = (x, y)
        elif event == cv2.EVENT_LBUTTONDOWN:
            # Left click means adding a point at current position to the list of points
            # print("Adding point #%d with position(%d,%d)" % (len(self.points), x, y))
            self.points.append((x, y))


    def mark_polygon(self, image):
        self.points = [] # List of points defining our polygon
        # Let's create our working window and set a mouse callback to handle events
        cv2.namedWindow(self.window_name, flags=cv2.WINDOW_AUTOSIZE)
        cv2.imshow(self.window_name, image)
        cv2.waitKey(1)
        cv2.setMouseCallback(self.window_name, self.on_mouse)

        while(not self.done):
            # This is our drawing loop, we just continuously draw new images
            # and show them in the named window
            if (len(self.points) > 0):
                # Draw all the current polygon segments
                cv2.polylines(image, np.array([self.points]), False, FINAL_LINE_COLOR, 1)
                # # And  also show what the current segment would look like
            # Update the window
            cv2.imshow(self.window_name, image)
            # And wait 50ms before next iteration (this will pump window messages meanwhile)
            if cv2.waitKey(50) == 27: # ESC hit
                self.done = True
            elif len(self.points)==self.max_points:
                self.done = True

        cv2.destroyWindow(self.window_name)
        return self.points[:-1]
    
    def warp(self, img, src_pts, keep_ratio):
        src_pts = np.array(src_pts, dtype="float32")
        h, w = self.img_size
        if keep_ratio:
            l1 = np.linalg.norm(src_pts[0] - src_pts[1])
            l2 = np.linalg.norm(src_pts[1] - src_pts[2])
            l3 = np.linalg.norm(src_pts[2] - src_pts[3])
            l4 = np.linalg.norm(src_pts[3] - src_pts[0])
            d1 = (l1 + l3)/2
            d2 = (l2 + l4)/2
            if d1 > d2:
                w = int(d2)
            else:
                h = int(d1)
        dst_pts = np.array([[0, h-1],
                                [0, 0],
                                [w-1, 0],
                                [w-1, h-1]], dtype="float32")

        # the perspective transformation matrix
        M = cv2.getPerspectiveTransform(src_pts, dst_pts)

        # directly warp the rotated rectangle to get the straightened rectangle
        warped = cv2.warpPerspective(img, M, (w, h))

        # Visualize Result image
        cv2.imshow('Warped', cv2.cvtColor(warped.copy(), cv2.COLOR_RGB2BGR))
        cv2.waitKey()
        return warped
    
    def align(self, img, keep_ratio=True):
        b = self.border
        img_with_border = np.random.uniform(0,255,(img.shape[0]+2*b,img.shape[1]+2*b,3)).round().astype(np.uint8)
        img_with_border[b:b+img.shape[0], b:b+img.shape[1], :] = img

        points = self.mark_polygon(cv2.cvtColor(img_with_border.copy(), cv2.COLOR_RGB2BGR))
        # Polygon -> rect
        rect = cv2.minAreaRect(np.array(points))
        box_points = cv2.boxPoints(rect)
        # Warp
        aligned_image = self.warp(img_with_border, box_points, keep_ratio)
        return aligned_image


if __name__=='__main__':
    my_aligner = ManualImageAligner()
    img = '/home/walkout05/Downloads/data_from_02_06_dataset_job_2020-06-03_21_01_52/dataset_job_2020-06-03_21_01_52/product_classifier/8000380007257/product_classifier_back_rgb_crop_onboarding_2020-06-02-08_41_56_event_id_182_frame_6226_cam_back_source_MD_cls_8000380007257.jpg'
    img = cv2.imread(img, -1)
    aligned_image = my_aligner.align(img, keep_ratio=True)

    cv2.imshow('Original image', img)
    cv2.imshow('warped image', aligned_image)
    cv2.waitKey(0)