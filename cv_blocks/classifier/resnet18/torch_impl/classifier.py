import torch
from torchvision.models import resnet18
import numpy as np
# from models.resnet import resnet18
import cv2

class ResNet18Classifier(torch.nn.Module):
    def __init__(self, weights, n_class, softmax=True,
                 img_size=(128, 128),
                 debug=False):
        torch.nn.Module.__init__(self)

        self.n_class = n_class
        self.img_size = img_size
        self.debug = debug
        self.model = resnet18(pretrained=False, num_classes=n_class)
        self.softmax = softmax
        if softmax:
            self.add_softmax()
        print('ResNet18 classifier: Loading weights from %s' % weights)
        checkpoint = torch.load(weights)
        self.load_state_dict(checkpoint['model_state_dict'])
        self.eval()

    def add_softmax(self):
        self.sm_layer = torch.nn.Softmax(dim=1)
        self.softmax = True

    def forward(self, p_in):
        out = self.model(p_in)
        if self.softmax:
            out = self.sm_layer(out)
        return out
    
    def preproc(self, im):
        im = cv2.resize(im, self.img_size)
        if self.debug:
            cv2.imshow('Classifier Input', im)
            cv2.waitKey()
        im = im[:, :, ::-1]  # BGR 2 RGB
        im = np.array(im).astype(np.float32) / 255.
        im = torch.from_numpy(im)
        im = im.transpose(0,2).transpose(1,2).contiguous().view(1, 3, self.img_size[0], self.img_size[1])
        return im

    def predict(self, img):
        img = self.preproc(img)
        probs = self.forward(img)
        return probs.cpu().detach().numpy().squeeze()
