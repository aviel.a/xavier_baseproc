
import pickle
import numpy as np
from scipy.spatial.distance import cdist

class ClusterReducer(object):
    '''
    Class implementing reduction of multiple products to sub-group of products
    It is done by comparison of descriptor distances to pre-computed cluster centroids
    '''
    def __init__(self, repr_info_file, debug=True,):
        self.repr_info_file = repr_info_file
        print('Opening product reducer from %s' % repr_info_file, flush=True)
        self.repr_info = pickle.load(open(repr_info_file, 'rb'))
        self.generator = self.repr_info['generator']
        self.product_repr = self.repr_info['product_repr']
        self.prod_to_cluster_mapping = self.repr_info['prod_to_cluster_mapping']
        self.cluster_to_prod_mapping = self.repr_info['cluster_to_prod_mapping']
        self.cluster_centroids = self.repr_info['centroids'].astype(np.float32)
        self.desc_dim = self.cluster_centroids.shape[1]
        self.debug = debug
        import faiss
        self.index = faiss.IndexFlatL2(self.desc_dim)
        self.index.add(self.cluster_centroids)
    
    def near_cluster(self, seq_descriptors, top_k=2):
        seq_dists, cluster_id = self.index.search(seq_descriptors.astype(np.float32), top_k)

        return dict(dist=seq_dists, cluster_ids=cluster_id)

    def is_valid_gt(self, product_name):
        return product_name in self.product_repr

    def get_cluster_products(self, cluster_idx):
        return self.cluster_to_prod_mapping[cluster_idx]

    def reduce_event(self, event):
        '''
        Reduce event to list of possible products
        '''
        if 'sequence_descriptors' not in event:
            return None, None, None
        
        # Find top-2 nearest cluster per image
        # ====================================
        top_k = 2
        near_cluster_info = self.near_cluster(event['sequence_descriptors'], top_k=top_k)
        score_candidates = 1
        dist_for_score = near_cluster_info['dist'][:, :score_candidates].flatten()
        cluster_ids_for_score = near_cluster_info['cluster_ids'][:, :score_candidates].flatten()

        # Convert cluster to probability
        # ==============================
        temp_factor = 2.0
        w = np.exp(-dist_for_score * temp_factor).flatten()
        valid_idx = np.unique(cluster_ids_for_score)
        agg_w = np.zeros(len(valid_idx))
        for w_i, c_id in zip(w, cluster_ids_for_score):
            agg_w[np.argwhere(valid_idx==c_id).ravel()[0]] += w_i
        
        prob = agg_w / w.sum()
        if len(prob)==1:
            top1_idx = 0
            top2_idx = 0
        else:
            top1_idx, top2_idx = prob.argsort()[::-1][:2]
        top1_cluster = valid_idx[top1_idx]
        top2_cluster = valid_idx[top2_idx]
        top1_prob = prob[top1_idx]
        top2_prob = prob[top2_idx]
        best_cluster = valid_idx[np.argmax(prob)]
        metadata = dict(top1_prob=top1_prob, probs=prob, cluster_per_img=near_cluster_info['cluster_ids'],
                        top2_prob=top2_prob, top1_cluster=top1_cluster, top2_cluster=top2_cluster)

        # Return list of product mapped to the cluster id
        return best_cluster, self.cluster_to_prod_mapping[best_cluster], metadata

        # TODO - add validation of distance to specific products


if __name__=='__main__':
    repr_info_file = '/home/guyr/mini_proj/extreme_cls_experiments/results/cls_3k_02_00025/repr_structure_cls_3k_02_00025_2675_max_cls_prod_100_4_FaissKNN_n_cluster_100_max_iter_300_n_init_10.pkl'
    my_cls_reducer = ClusterReducer(repr_info_file)