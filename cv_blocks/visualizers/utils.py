import cv2
import numpy as np

def drawline(img,pt1,pt2,color,thickness=1,style='default',gap=20):
    if style=='default':
        cv2.line(img,tuple(pt1),tuple(pt2),color,thickness)
        return
    dist =((pt1[0]-pt2[0])**2+(pt1[1]-pt2[1])**2)**.5
    pts= []
    for i in  np.arange(0,dist,gap):
        r=i/dist
        x=int((pt1[0]*(1-r)+pt2[0]*r)+.5)
        y=int((pt1[1]*(1-r)+pt2[1]*r)+.5)
        p = (x,y)
        pts.append(p)

    if style=='dotted':
        for p in pts:
            cv2.circle(img,p,thickness,color,-1)
    elif style=='dashed':
        s=pts[0]
        e=pts[0]
        i=0
        for p in pts:
            s=e
            e=p
            if i%2==1:
                cv2.line(img,s,e,color,thickness)
            i+=1

def drawpoly(img,pts,color,thickness=1,style='dashed'):
    s=pts[0]
    e=pts[0]
    pts.append(pts.pop(0))
    for p in pts:
        s=e
        e=p
        drawline(img,s,e,color,thickness,style)

def drawrect(img,pt1,pt2,color,thickness=1,style='default'):
    if style=='default':
        cv2.rectangle(img, pt1, pt2, color, thickness)
    else:
        pts = [pt1,(pt2[0],pt1[1]),pt2,(pt1[0],pt2[1])] 
        drawpoly(img,pts,color,thickness,style)

def drawrect_rotated(img,rect,color,thickness=1,style='default'):
    if style=='default':
        drawpoly(img,rect.points().astype(int).tolist(),color,thickness,style)
    else:
        drawpoly(img,rect.points().tolist(),color,thickness,style)

def drawborder(img, border_size=10, border_color=(0,255,0)):
    h,w = img.shape[0:2]
    base_size=h,w,3
    cv2.rectangle(img,(0,0),(w,h),border_color,border_size) # really thick white rectangle