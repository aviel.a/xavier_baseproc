import argparse
import os
from cv_blocks.misc.logger import BaseLogger
from tqdm import tqdm
import pandas as pd

def gen_session_summary(el_list, structured_events=True):
    # Sort by timestamp
    events = []
    for el in el_list:
        ev_txt = ''
        row = dict(timestamp=el['timestamp'])
        if el['type'] == 'event_aggregator_out':
            row['frame'] = el['frame']
            row['event_id'] = el['element'].data['event_id']
            row['event_type'] = 'VisualEvent'
            if structured_events:
                event_metadata = {k: v for k, v in el['element'].data.items() if k!='movement_vector'}
                event_metadata['event_size'] = len(el['element'].data['movement_vector'])
                ev_txt ='Info:%s' % (str(event_metadata))
            else:
                ev_size = len(el['element'].data['movement_vector'])
                ev_dir = el['element'].data['event_direction']
                ev_multi = el['element'].data['multi_cam']
                ev_cb = el['element'].data['activated_by_cart_bottom']
                ev_txt = 'Event going %s. size=%d, multi-cam=%r, CB=%r' % (ev_dir, ev_size, ev_multi, ev_cb)
        elif el['type'] == 'user_marked_event':
            row['frame'] = el['frame']
            row['event_id'] = el['element'].event_id
            row['event_type'] = el['element'].event_type
            if el['element'].event_type=='QuantityChange':
                ev_txt ='qunatity=%d' % el['element'].metadata['quantity']
            elif el['element'].event_type=='EventRedirected':
                ev_txt ='redirected to %s' % el['element'].metadata['resolution']
            elif el['element'].event_type=='EventResolved':
                ev_txt ='resolved to %s' % el['element'].metadata['resolution']
            elif el['element'].event_type=='EventPred':
                ev_txt ='Info:%s' % (str(el['element'].metadata))
            elif el['element'].event_type=='EventCls':
                ev_txt ='Info:%s' % (str(el['element'].metadata))
            elif el['element'].event_type=='BarcodeEventOp':
                ev_txt ='Info:%s' % (str(el['element'].metadata))
            elif el['element'].event_type=='PluEventOp':
                ev_txt ='Info:%s' % (str(el['element'].metadata))
            elif el['element'].event_type=='ProductValidation':
                ev_txt ='Info:%s' % (str(el['element'].metadata))
            elif el['element'].event_type in ('SessionStart', 'SessionEnd'):
                ev_txt ='Info:%s' % (str(el['element'].metadata))
        
        row['event'] = ev_txt
        events.append(row)
            
    return pd.DataFrame(events, columns=['timestamp', 'frame', 'event_id', 'event_type' ,'event'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='WalkOut Logger Visualizer')
    parser.add_argument('--log-file', help='path to all recordings root directory',type=str, required=True)
    parser.add_argument('--output-dir', help='path to directory to write output',type=str, default='/tmp/mv_log')
    parser.add_argument('--marked-only', help='output only marked events', action='store_true')
    parser.add_argument('--multi-item-only', help='output only marked as multiple items events', action='store_true')
    parser.add_argument('--start-frame', help='event from frame', type=int, default=None)
    parser.add_argument('--end-frame', help='event until frame', type=int, default=None)
    args = parser.parse_args()
    assert os.path.exists(args.log_file)
    assert not (args.marked_only and args.multi_item_only)

    my_logger = BaseLogger(True, include_barocde=True)

    el_list = my_logger.from_data(args.log_file)

    # Get session summary
    # ===================
    os.makedirs(args.output_dir, exist_ok=True)
    summary = gen_session_summary(el_list)
    summary.to_csv(os.path.join(args.output_dir, 'session_summary.csv'))

    # Extract marked event ids
    if args.marked_only:
        events_to_log = []
        for el in el_list:
            if el['type'] == 'user_marked_event':
                 events_to_log.append(el['element'].event_id)
    elif args.multi_item_only:
        events_to_log = []
        for el in el_list:
            if el['type'] == 'user_marked_event' and el['element'].quantity > 1:
                 events_to_log.append(el['element'].event_id)

    for el in tqdm(el_list, desc='Parsing log file'):
        if args.start_frame is not None and args.end_frame is not None and 'frame' in el.keys():
            if not (args.start_frame < el['frame'] < args.end_frame):
                continue
        if not hasattr(el['element'], 'visualize'):
            continue
        if args.marked_only or args.multi_item_only:
            if el['type']!='event_aggregator_out' or \
                el['element'].data['event_id'] not in events_to_log:
                continue
        el['element'].visualize(path=args.output_dir, prefix=el['type'], timestamp=el['timestamp'])