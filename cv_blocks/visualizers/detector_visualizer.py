import os
import cv2
import numpy as np
from .utils import drawrect, drawrect_rotated
from cv_blocks.common.types import BoundingBox2D, RotatedBoundingBox2D

def draw_boxes(image, boxes, gt_boxes=None, ious=None,
               labels=None,skip_indices=[],override_ids=None,
               ids = None):
    image_h, image_w, _ = image.shape

    color_list = [
                    (0,   255, 255),  #cyan
                    (255, 0  , 255),  # magenta
                    (255, 0, 0),  # red
                    (0, 255, 0),  # lime (sort of green)
                    (0, 0, 255),  # blue
                    (255, 255, 0),  # yellow
                  ]
    for idx,box in enumerate(boxes):
        if box.get_label() in skip_indices:
            continue
        color = color_list[box.get_label() % len(color_list)]
        if override_ids is not None:
            label = 'ID {}'.format(override_ids[idx])
            color = color_list[override_ids[idx] % len(color_list)]
        else:
            label = labels[box.get_label()]
            color = color_list[ids[idx] % len(color_list)]

        if isinstance(box, BoundingBox2D):
            drawrect(image, (int(box.x1),int(box.y1)), (int(box.x2),int(box.y2)), color, 1)
        else:
            drawrect_rotated(image, box, color, 1, style='default')
        cv2.putText(image,
                    '%s  c:%.2f' % (label,box.get_score()),
                    (int(box.x1), int(box.y1) + 15),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1e-3 * image_h,
                    color, 1)
        if ious is not None and len(ious)>idx:
            cv2.putText(image,
                        'IOU:%.2f' % ious[idx],
                        (int(box.x1), int(box.y1) + 30),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1e-3 * image_h,
                        color, 1)

    # Draw GT boxes
    if gt_boxes:
        for idx,box in enumerate(gt_boxes):
            if box.get_label() in skip_indices:
                continue
            color = color_list[box.get_label() % len(color_list)]
            if override_ids is not None:
                # label = 'ID_{}'.format(override_ids[idx])
                color = color_list[override_ids[idx] % len(color_list)]
            else:
                # label = labels[box.get_label()]
                color = color_list[ids[idx] % len(color_list)]

            if isinstance(box, BoundingBox2D):
                drawrect(image, (int(box.x1),int(box.y1)), (int(box.x2),int(box.y2)), color, 1, style='dashed')
            else:
                drawrect_rotated(image, box, color, 1, style='dashed')
        
    return image          

class DetectorVisualizer(object):
    '''
    Class visualizing object detections
    '''
    def __init__(self, labels, output_dir=None):
        self.labels = labels
        self.output_dir = output_dir
        self.plot_result = output_dir is not None
        if self.output_dir is not None and not os.path.exists(output_dir):
            os.makedirs(output_dir)
    
    def visualize(self, img, boxes, gt_boxes=None, ious=None, name='visualize'):
        name = os.path.basename(name)
        override_ids = list(range(max(len(boxes), len(gt_boxes))))
        img_vis = draw_boxes(img, boxes, 
                             gt_boxes=gt_boxes,
                             ious=ious,
                             ids=self.labels, 
                             override_ids=override_ids)
        if self.output_dir is not None:
            cv2.imwrite(os.path.join(self.output_dir, name), img_vis)
        else:
            cv2.imshow('visualize', img_vis)
            cv2.waitKey()

def draw_att_boxes(image, boxes, ious=None,
                   attributes=None, box_type='default'):
    image_h, image_w, _ = image.shape

    color_list = [
                    (0,   255, 255),  #cyan
                    (255, 0  , 255),  # magenta
                    (255, 0, 0),  # red
                    (0, 255, 0),  # lime (sort of green)
                    (0, 0, 255),  # blue
                    (255, 255, 0),  # yellow
                  ]
    v_diff = int(image_h * 0.02)
    for idx,box in enumerate(boxes):
        box_atts = []
        if box_type=='default':
            color = color_list[box.get_label() % len(color_list)]
            for att_name, att_vals in attributes.items():
                att_pred = np.argmax(box.attributes[att_name])
                att_score = box.attributes[att_name][att_pred]
                box_atts.append(dict(name=att_name, pred=att_vals[att_pred], score=att_score))
            box_label = ['c: %.2f' % box.score] + ['%s: %.2f' % (ba['pred'], ba['score']) for ba in box_atts]
        elif box_type=='gt':
            color = color_list[attributes['crop_type'].index(box.attributes['crop_type']) % len(color_list)] \
                if box.attributes['crop_type']!='not-sure' else color_list[0]
            for att_name in attributes.keys():
                att_pred = box.attributes[att_name]
                box_atts.append(dict(name=att_name, pred=att_pred))
            box_label = ['%s' % (ba['pred']) for ba in box_atts]

        if isinstance(box, BoundingBox2D):
            drawrect(image, (int(box.x1),int(box.y1)), (int(box.x2),int(box.y2)), color, 1)
        else:
            drawrect_rotated(image, box, color, 1, style='default')
        for line_idx, label_line in enumerate(box_label):
            cv2.putText(image,
                        label_line,
                        (int(box.x1), int(box.y1) + 15 + line_idx * v_diff),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL,
                        1e-3 * image_h,
                        color, 1)
        # if ious is not None and len(ious)>idx:
        #     cv2.putText(image,
        #                 'IOU:%.2f' % ious[idx],
        #                 (int(box.x1), int(box.y1) + 30),
        #                 cv2.FONT_HERSHEY_SIMPLEX,
        #                 1e-3 * image_h,
        #                 color, 1)

        
    return image          
class DetectorWithAttVisualizer(object):
    '''
    Class visualizing object detections
    '''
    def __init__(self, attributes, output_dir=None):
        self.attributes = attributes
        self.output_dir = output_dir
        self.plot_result = output_dir is not None
        if self.output_dir is not None and not os.path.exists(output_dir):
            os.makedirs(output_dir)
    
    def visualize(self, img, boxes, gt_boxes=None, ious=None, name='visualize', viz_scale=2., path=None):
        name = os.path.basename(name)
        override_ids = list(range(max(len(boxes), len(gt_boxes))))
        boxes_scale = [bx.scale(viz_scale) for bx in boxes]
        gt_boxes_scale = [bx.scale(viz_scale) for bx in gt_boxes]
        img_scale = cv2.resize(img, (0, 0), fx=viz_scale, fy=viz_scale)
        # Draw predicted boxes
        img_vis_boxes_gt = draw_att_boxes(img_scale.copy(), gt_boxes_scale, 
                             attributes=self.attributes, box_type='gt')
        cv2.putText(img_vis_boxes_gt, 'GT', (10, 20),
            cv2.FONT_HERSHEY_COMPLEX_SMALL,
            2e-3 * img_vis_boxes_gt.shape[0],
            (0,255,0), 1)
        img_vis_boxes = draw_att_boxes(img_scale.copy(), boxes_scale, 
                             ious=ious,
                             attributes=self.attributes)
        cv2.putText(img_vis_boxes, 'PRED', (10, 20),
            cv2.FONT_HERSHEY_COMPLEX_SMALL,
            2e-3 * img_vis_boxes.shape[0],
            (0,255,0), 1)
        img_vis = np.hstack((img_vis_boxes, img_vis_boxes_gt))
        if name is not None:
            cv2.putText(img_vis, name, (10, 10),
            cv2.FONT_HERSHEY_COMPLEX_SMALL,
            1e-3 * img_vis.shape[0],
            (0,0,255), 1)
        if self.output_dir is not None or path is not None:
            output_dir = path if path is not None else self.output_dir
            cv2.imwrite(os.path.join(output_dir, name), img_vis)
        else:
            cv2.imshow('visualize', img_vis)
            while cv2.waitKey(33)!=32:
                continue
            