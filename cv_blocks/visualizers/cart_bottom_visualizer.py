import cv2
import numpy as np
from .utils import drawrect
import os

def draw_boxes(image, boxes, gt_boxes=None, ious=None,
               labels=None,skip_indices=[],override_ids=None,
               ids = None):
    image_h, image_w, _ = image.shape

    color_list = [
                    (0,   255, 255),  #cyan
                    (255, 0  , 255),  # magenta
                    (255, 0, 0),  # red
                    (0, 255, 0),  # lime (sort of green)
                    (0, 0, 255),  # blue
                    (255, 255, 0),  # yellow
                  ]
    for idx,box in enumerate(boxes):
        if box.get_label() in skip_indices:
            continue
        color = color_list[box.get_label() % len(color_list)]
        if override_ids is not None:
            label = '{}'.format(override_ids[idx])
            color = color_list[idx % len(color_list)]
        else:
            label = labels[box.get_label()]
            color = color_list[ids[idx] % len(color_list)]

        drawrect(image, (int(box.x1),int(box.y1)), (int(box.x2),int(box.y2)), color, 1)
        cv2.putText(image,
                    '%s  c:%.2f' % (label,box.get_score()),
                    (int(box.x1), int(box.y1) + 15),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1e-3 * image_h,
                    color, 1)
        if ious is not None and len(ious)>idx:
            cv2.putText(image,
                        'IOU:%.2f' % ious[idx],
                        (int(box.x1), int(box.y1) + 30),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1e-3 * image_h,
                        color, 1)

    # Draw GT boxes
    if gt_boxes:
        for idx,box in enumerate(gt_boxes):
            if box.get_label() in skip_indices:
                continue
            color = color_list[box.get_label() % len(color_list)]
            if override_ids is not None:
                # label = 'ID_{}'.format(override_ids[idx])
                color = color_list[override_ids[idx] % len(color_list)]
            else:
                # label = labels[box.get_label()]
                color = color_list[ids[idx] % len(color_list)]

            drawrect(image, (int(box.xmin),int(box.ymin)), (int(box.xmax),int(box.ymax)), color, 1, style='dashed')
        
    return image          

class CartBottomVisualizer(object):
    '''
    Class visualizing cart bottom diff detections
    '''
    def __init__(self, labels, output_dir=None):
        self.labels = labels
        self.output_dir = output_dir
        self.plot_result = output_dir is not None
        if self.output_dir is not None and not os.path.exists(output_dir):
            os.makedirs(output_dir)
    
    def visualize(self, ref_img, cur_img, diff, gt=None, name='visualize'):
        def _add_gt_txt(img):
            gt_txt = 'GT:\n%s' % ('\n'.join(['%s, dir:%s' % (g['alias'], g['direction']) for g in gt]))
            cv2.putText(img_vis, 'GT:',(10, 30),cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                (0,0,255), 2)
            for gt_idx, gt_el in enumerate(gt):
                gt_txt = '%s, dir:%s' % (gt_el['alias'], gt_el['direction'])
                cv2.putText(img_vis, gt_txt,(10, 50 + 20*gt_idx),cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                    (0,0,255), 2)

        h,w  = ref_img.shape[:2]
        boxes_in = [bx['box'] for bx in diff if bx['direction']=='in'] 
        ids_in = [bx['alias'] for bx in diff if bx['direction']=='in']
        boxes_out = [bx['box'] for bx in diff if bx['direction']=='out'] 
        ids_out = [bx['alias'] for bx in diff if bx['direction']=='out']
        img_ref_vis = draw_boxes(ref_img.copy(), boxes_out, 
                             ids=self.labels, 
                             override_ids=ids_out)
        img_cur_vis = draw_boxes(cur_img.copy(), boxes_in, 
                             ids=self.labels, 
                             override_ids=ids_in)
        img_vis = np.hstack((img_ref_vis, img_cur_vis))
        img_vis = cv2.resize(img_vis, (w*4, h*2))
        _add_gt_txt(img_vis)
        if self.output_dir is not None:
            cv2.imwrite(os.path.join(self.output_dir, name), img_vis)
        else:
            cv2.imshow('visualize', img_vis)
            cv2.waitKey()