import cv2
import os
import numpy as np
import argparse
import yaml
from multiprocessing import Process
import streams.stereo.vx._main_stereo_matching as msm
from cv_blocks.video_reader.base_proc import BaseImageGrabber
from cv_blocks.calibration.calib_utils import LineMarker
from scipy import interpolate
from tqdm import tqdm

def visualize_features(img, features):
    img_viz = img.copy()
    for (px, py) in features.squeeze():
        cv2.circle(img_viz, (px,py), 1, (0,255,0), -1)
    cv2.imshow('Features', img_viz)
    cv2.waitKey()

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='generate disparity map')
    parser.add_argument('--movie-name', help='path to csv file containing files to run',type=str, default=None)
    parser.add_argument('--calib', help='calibration model name to use',type=str, required=True)
    parser.add_argument('--top-cart', help='top value for cart',type=int, default=None)
    parser.add_argument('--n-frames', help='num frames to aggregate',type=int, default=20)
    parser.add_argument('--min-disp', help='minimal disparity value',type=int, default=29)
    parser.add_argument('--max-disp', help='maximal disparity value',type=int, default=70)
    parser.add_argument('--bg-offset', help='background disparity offset',type=int, default=5)
    parser.add_argument('--output-dir', help='path to write output map to', type=str, default='/tmp')
    parser.add_argument('--skip', help='skip n frames from start',type=int, default=2)
    parser.add_argument("--viz", help="visualize results", action='store_true')
    args = parser.parse_args()

    if not os.path.isdir(args.calib):
        calib_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'models', args.calib)
    else:
        calib_dir = args.calib
    assert os.path.exists(os.path.join(calib_dir, 'cart_config.yaml'))

    # Initialize preprocessor
    cart_config = yaml.safe_load(open(os.path.join(calib_dir, 'cart_config.yaml'), 'r'))
    camera_info = cart_config['camera_info']
    h = 320
    w = 240
    DEFAULT_WIDTH_PRE_ROTATE = 640
    cam_sizes_dict = dict()
    cam_sizes_dict_vx = dict()
    for cam_name in ('left', 'right', 'back'):
        cam_dim = camera_info[cam_name]['width'] if 'width' in camera_info[cam_name] else DEFAULT_WIDTH_PRE_ROTATE
        factor = cam_dim / h
        cam_sizes_dict['height_%s' % cam_name] = str(int(w*factor)) # Transposed
        cam_sizes_dict['width_%s' % cam_name] = str(int(h*factor)) # Transposed
        cam_sizes_dict_vx['height_%s' % cam_name] = str(int(h*factor))
        cam_sizes_dict_vx['width_%s' % cam_name] = str(int(w*factor))
    map1l_full = np.genfromtxt(os.path.join(calib_dir, '0p5_remap_table_mapL1.csv'), delimiter=' ', dtype=np.float32)
    map2l_full = np.genfromtxt(os.path.join(calib_dir, '0p5_remap_table_mapL2.csv'), delimiter=' ', dtype=np.float32)
    map1l = np.genfromtxt(os.path.join(calib_dir, '0p25_remap_table_mapL1.csv'), delimiter=' ', dtype=np.float32)
    map2l = np.genfromtxt(os.path.join(calib_dir, '0p25_remap_table_mapL2.csv'), delimiter=' ', dtype=np.float32)
    map1r = np.genfromtxt(os.path.join(calib_dir, '0p25_remap_table_mapR1.csv'), delimiter=' ', dtype=np.float32)
    map2r = np.genfromtxt(os.path.join(calib_dir, '0p25_remap_table_mapR2.csv'), delimiter=' ', dtype=np.float32)
    from md_blocks.memory.buffer_manager import BufferManager
    buffer_manager = BufferManager()
    buffer_manager.allocate_default_cam_buffers(cam_sizes_dict)
    buffer_manager.allocate_default_vx_buffers(cam_sizes_dict)
    vx_input_names = ('left_cam_raw', 'right_cam_raw', 'back_cam_raw', 'spatial_th')
    vx_output_names = ('disparity', 'grayL_resized','grayB_resized',
                        'frameL_resized', 'frameB_resized',
                        'cart_disp_mask', 'left_remap', 'frameDelta', 'frameDeltaBack',
                        'frameDeltaMaskBack', 'frameL_resized_masked', 'frameB_resized_masked')
    vx_prcss = msm.init(cam_sizes_dict_vx, calib_dir, 
                        buffer_manager.init_buffer_list(vx_input_names, msm.new_BufferImportExt), 
                        buffer_manager.init_buffer_list(vx_output_names, msm.new_BufferImportExt),
                        map1l_full, map2l_full, map1l, map2l, map1r, map2r,
                        True)

    # Intialize stream
    wait_time = 50
    skip_first_frames = args.skip
    full_res = False
    live_movie = args.movie_name is None
    if live_movie:
        stream = BaseImageGrabber('basler', camera_info, target_ms=wait_time)
    else:
        movie_addr_wo_suffix = args.movie_name.split('_L.avi')[0]
        stream = BaseImageGrabber('playback', movie_addr_wo_suffix, target_ms=wait_time)

    proc = Process(target=stream.run)
    proc.start()

    mask = np.ones((h,w)) if not full_res else np.ones((h*2,w*2))
    if args.top_cart is not None:
        mask[:args.top_cart] = 0
    mask = mask.astype(np.uint8)

    cart_back_roi = None
    front_edges = None

    ave_disparity = []
    all_features = []
    for n in tqdm(range(args.n_frames+skip_first_frames)):
        # Read frame
        if n < skip_first_frames:
            continue
        try:
            img_data, _, _ = stream.grab(can_skip=False)
        except:
            break
        # Run VX
        # ======
        buffer_manager.get('left_cam_raw')[:] = img_data['data']['left']['img']
        buffer_manager.get('right_cam_raw')[:] = img_data['data']['right']['img']
        buffer_manager.get('back_cam_raw')[:] = img_data['data']['back']['img']
        msm.process_stream(vx_prcss, 0)
        disparity_image = buffer_manager.get('disparity')
        grayL_resized = buffer_manager.get('grayL_resized')
        grayL_resized = buffer_manager.get('grayL_resized')
        frameL_resized = buffer_manager.get('frameL_resized')
        left_remap = buffer_manager.get('left_remap')
        # Draw cart edges
        # ===============
        if front_edges is None:
            edge_marker = LineMarker(window_name='Mark upper cart edge', max_points=4)
            front_edges = edge_marker.mark_polygon(frameL_resized.copy(), normalize=True)
        assert len(front_edges)==4
        if cart_back_roi is None:
            # cart_back_roi = cv2.selectROI(frameL_resized)
            # cart_back_roi = [int(r/2) for r in cv2.selectROI('Mark Cart Back Part', left_remap)]
            # mid_line_roi = [int(r/2) for r in cv2.selectROI('Mark Middle line', left_remap)]
            cart_back_roi = cv2.selectROI('Mark Cart Back Part', frameL_resized)
            mid_line_roi = cv2.selectROI('Mark Middle line', frameL_resized)
            mask[:cart_back_roi[1]] = 0
        # Extract Features
        if full_res:
            left_gray = cv2.cvtColor(left_remap, cv2.COLOR_BGR2GRAY)
        else:
            left_gray = grayL_resized
        features = cv2.goodFeaturesToTrack(left_gray, maxCorners=1000,
            qualityLevel=0.02, minDistance=7, blockSize=5, mask=mask)
        all_features.append(features.squeeze())
        ave_disparity.append(disparity_image)
        # Visualize Features
        if args.viz:
            if full_res:
                visualize_features(left_remap, features)
            else:
                visualize_features(frameL_resized, features)

    all_features = np.unique(np.vstack(all_features), axis=0)

    ave_disparity = np.dstack(ave_disparity).mean(axis=-1)
    # ave_disparity = np.dstack(ave_disparity).min(axis=-1)
    # ave_disparity = np.dstack(ave_disparity).max(axis=-1)

    # visualize_features(frameL_resized, all_features)
    cv2.imshow('Average Disparity', ave_disparity.astype(np.uint8))
    # Interpolate by disparity at feature points
    f_x = all_features[:,0].astype(np.int)
    f_y = all_features[:,1].astype(np.int)
    f_disparity = ave_disparity[f_y, f_x]

    # Invalidate bg values
    valid_mask = np.bitwise_and(f_disparity > args.min_disp, f_disparity < args.max_disp)
    all_features = all_features[valid_mask]
    f_disparity = f_disparity[valid_mask]

    grid_x, grid_y = np.meshgrid(np.arange(w), np.arange(h))
    interp_disp = interpolate.griddata(all_features, f_disparity, (grid_x, grid_y), method='linear')
    interp_disp = interp_disp.astype(np.uint8)

    # Overwrite cart back with independent interpolation
    valid_back_x = np.bitwise_and(all_features[:,0] > cart_back_roi[0], all_features[:,0] < cart_back_roi[0] + cart_back_roi[2])
    valid_back_y = np.bitwise_and(all_features[:,1] > cart_back_roi[1], all_features[:,1] < cart_back_roi[1] + cart_back_roi[3])
    valid_back = np.bitwise_and(valid_back_x, valid_back_y)
    all_features_back = all_features[valid_back] - np.array([cart_back_roi[0], cart_back_roi[1]])
    f_disparity_back = f_disparity[valid_back]
    grid_x, grid_y = np.meshgrid(np.arange(cart_back_roi[2]), np.arange(cart_back_roi[3]))
    interp_disp_back = interpolate.griddata(all_features_back, f_disparity_back, (grid_x, grid_y), method='linear')
    interp_disp_back = interp_disp_back.astype(np.uint8)
    # Fill values from full interpolated disparity where back interpolation was not computed
    zero_mask = interp_disp_back == 0
    interp_disp[cart_back_roi[1]:cart_back_roi[1]+cart_back_roi[3], cart_back_roi[0]:cart_back_roi[0]+cart_back_roi[2]][~zero_mask] = interp_disp_back[~zero_mask]

    # Fill rest of the map with background value
    bg_val = args.min_disp - args.bg_offset
    interp_disp[interp_disp<args.min_disp] = bg_val

    cv2.imshow('Interpolated Disparity', interp_disp)
    os.makedirs(args.output_dir, exist_ok=True)
    cv2.imwrite(os.path.join(args.output_dir, 'cart_disparity_th.png'), interp_disp)

    proc.terminate()
    cv2.waitKey()

    # Approximate decision lines
    top_line = cart_back_roi[1] / h
    if top_line < 0.25:
        print('Warning - top line might be too high')
    mid_line = (mid_line_roi[1] + mid_line_roi[3]) / h
    bottom_line = mid_line + (mid_line - top_line)
    disp_val_far = interp_disp[mid_line_roi[1]:mid_line_roi[1]+mid_line_roi[3], mid_line_roi[0]:mid_line_roi[0]+mid_line_roi[2]][:,-10:].mean() 
    disp_val_near = (disp_val_far * 2) - 10

    print('decision lines:', [top_line, mid_line, bottom_line])
    print('disparity values:', [disp_val_far, disp_val_near])

    # Update yaml with defs
    cart_config['camera_config']['front']['decision_lines'] = dict(bottom=bottom_line, middle=mid_line, top=top_line)
    cart_config['camera_config']['front']['decision_disparity'] = dict(near=float(disp_val_near), far=float(disp_val_far))
    cart_config['edge_calib'] = dict()
    cart_config['edge_calib']['front'] = dict(left_edge=dict(p1=list(front_edges[0]), p2=list(front_edges[1])),
                                              far_edge=dict(p1=list(front_edges[1]), p2=list(front_edges[2])),
                                              right_edge=dict(p1=list(front_edges[2]), p2=list(front_edges[3])),
                                             )
    # Save yaml config
    yaml.dump(cart_config, open(os.path.join(args.output_dir, 'cart_config.yaml.NEW'), 'w'))