import cv2
import numpy as np

FINAL_LINE_COLOR = (255, 0, 0)
WORKING_LINE_COLOR = (0, 0, 255)

class LineMarker(object):
    def __init__(self, window_name='Image', max_points=5, img_size=(128,128), border=32):
        self.window_name = window_name # Name for our window
        self.max_points = max_points
        self.done = False # Flag signalling we're done
        self.current = (0, 0) # Current position, so we can draw the line-in-progress
        self.points = [] # List of points defining our polygon
        self.img_size = img_size
        self.border = border


    def on_mouse(self, event, x, y, buttons, user_param):
        # Mouse callback that gets called for every mouse event (i.e. moving, clicking, etc.)

        if self.done: # Nothing more to do
            return

        if event == cv2.EVENT_MOUSEMOVE:
            self.current = (x, y)
        elif event == cv2.EVENT_LBUTTONDOWN:
            self.points.append((x, y))


    def mark_polygon(self, image, normalize=False):
        self.points = [] # List of points defining our polygon
        # Let's create our working window and set a mouse callback to handle events
        cv2.namedWindow(self.window_name, flags=cv2.WINDOW_AUTOSIZE)
        cv2.imshow(self.window_name, image)
        cv2.waitKey(1)
        cv2.setMouseCallback(self.window_name, self.on_mouse)

        while(not self.done):
            if len(self.points) > 0:
                last_point = self.points[-1]
                cv2.circle(image, (int(last_point[0]),int(last_point[1])), 4, WORKING_LINE_COLOR, -1)
            if len(self.points) > 1:
                cv2.polylines(image, np.array([self.points]), False, FINAL_LINE_COLOR, 1)
            cv2.imshow(self.window_name, image)
            if cv2.waitKey(50) == 27: # ESC hit
                self.done = True
            elif len(self.points)==self.max_points:
                self.done = True

        cv2.destroyWindow(self.window_name)
        if normalize:
            h, w = image.shape[:2]
            self.points = [(float(px) / w, float(py) / h) for (px, py) in self.points]

        return self.points
    
if __name__=='__main__':
    my_aligner = LineMarker()
    img = '/home/walkout05/Downloads/data_from_02_06_dataset_job_2020-06-03_21_01_52/dataset_job_2020-06-03_21_01_52/product_classifier/8000380007257/product_classifier_back_rgb_crop_onboarding_2020-06-02-08_41_56_event_id_182_frame_6226_cam_back_source_MD_cls_8000380007257.jpg'
    img = cv2.imread(img, -1)
    aligned_image = my_aligner.mark_polygon(img)

    cv2.imshow('Original image', img)
    cv2.imshow('warped image', aligned_image)
    cv2.waitKey(0)