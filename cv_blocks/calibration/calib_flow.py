import os
import argparse
import pickle
from cv_blocks.video_reader.base_proc import BaseImageGrabber
from matplotlib import pyplot as plt
import numpy as np
import cv2
import glob
import yaml
import time
from cv_blocks.geometry.pose import Pose
from cv_blocks.geometry.camera import Camera
from collections import OrderedDict
from multiprocessing import Process
import shutil, copy
from scipy.spatial.distance import pdist
from cv_blocks.common.types import BoundingBox2D
'''
Camera calibration scripts
'''

def reorder_facing_cam_uv(uv, checker_shape):
    uv_new = uv.reshape(checker_shape[1], checker_shape[0],2)
    uv_new = np.fliplr(uv_new).reshape(-1,2)
    return uv_new

def visualize_matches(data1, data2, checker_shape=(8,6), permute_facing_cams=False):
    for img1, idx1, uv1 in zip(data1['raw_imgs'], data1['img_idx'], data1['uvs']):
        if not idx1 in data2['img_idx']:
            continue
        idx_in_2 = data2['img_idx'].index(idx1)
        img2 = data2['raw_imgs'][idx_in_2]
        uv2 = data2['uvs'][idx_in_2]

        uv1 = uv1.squeeze().astype(int)
        uv2 = uv2.squeeze().astype(int)
        if permute_facing_cams:
            uv2_new = reorder_facing_cam_uv(uv2, checker_shape)
        else:
            uv2_new = uv2

        img_viz = np.hstack((img1, img2))
        lr_offset = img1.shape[1]

        for idx, (p1, p2) in enumerate(zip(uv1, uv2_new)):
            cl = (np.random.random(3) * 255).astype(int).tolist()
            cv2.circle(img_viz, (p1[0],p1[1]), 4, cl, -1)
            cv2.circle(img_viz, (p2[0] + lr_offset,p2[1]), 4, cl, -1)
            if (idx % 4)==0:
                cv2.line(img_viz, (p1[0],p1[1]), (p2[0] + lr_offset,p2[1]), cl, 1)

        cv2.imshow('Couple constraints', img_viz)
        cv2.waitKey(0)


class ImageGrabber(object):
    def __init__(self, stream, proc_dir='/tmp/calibration_images', live_movie=True):
        self.stream = stream
        self.proc_dir = proc_dir
        self.img_idx = 0
        os.makedirs(self.proc_dir, exist_ok=True)
        self.live_movie = live_movie
        self.frozen_stream = False
    

    def capture(self):
        if self.frozen_stream:
            return
        img_data, _, _ = self.stream.grab(can_skip=True)
        frameL = img_data['data']['left']['img'].copy()
        frameR = img_data['data']['right']['img'].copy()
        frameB = img_data['data']['back']['img'].copy()
        self.image_dict = dict(left=frameL, right=frameR, back=frameB)

    def maybe_save_images(self, save_key="s", quit_key="q"):
        quit_proc = False
        key = cv2.waitKey(self.stream.target_ms) & 0xFF
        if key == ord(save_key):
            for k, v in self.image_dict.items():
                cam_dir = os.path.join(self.proc_dir, k)
                os.makedirs(cam_dir, exist_ok=True)
                img_path = os.path.join(cam_dir, '%04d.png' % self.img_idx)
                cv2.imwrite(img_path, v)
            self.img_idx +=1
        elif key == ord(quit_key):
            quit_proc = True
        elif key == ord("w") and not self.live_movie:
            self.frozen_stream = not self.frozen_stream

        return quit_proc
    
    def step(self):
        self.capture()
        quit_proc = self.maybe_save_images()

        return self.image_dict, quit_proc
    

class CheckerCalibrator(object):
    def __init__(self, cam_models, checker_shape=(8,6), checker_size=0.03, viz=True):
        self.checker_shape = checker_shape
        self.checker_size = checker_size
        self.cam_models = cam_models
        self.fisheye_fov_balance = 1.
        self.fisheye_fov_scale = 0.82
        self.pinhole_alpha = 0.1
        self.viz = viz

    def get_checker_points(self, images, transpose=False, viz=True, take_all=False):
        # termination criteria
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        objp = np.zeros((self.checker_shape[0]*self.checker_shape[1],3), np.float32)
        objp[:,:2] = np.mgrid[0:self.checker_shape[0],0:self.checker_shape[1]].T.reshape(-1,2)

        # Arrays to store object points and image points from all the images.
        objpoints = [] # 3d point in real world space
        imgpoints = [] # 2d points in image plane.
        images_raw = []
        image_indices = []

        for fname in sorted(images):
            # print('Running image %s' % fname)
            img_idx = int(os.path.splitext(os.path.basename(fname))[0]) 
            img = cv2.imread(fname)
            if transpose:
                img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
            gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

            # Find the chess board corners
            checker_flags = cv2.CALIB_CB_ADAPTIVE_THRESH+cv2.CALIB_CB_NORMALIZE_IMAGE
            ret, corners = cv2.findChessboardCorners(gray, self.checker_shape,checker_flags)

            # Prune images that have small distance between points
            if ret==True and pdist(corners.squeeze()).min() < 10:
                ret = False
            # If found, add object points, image points (after refining them)
            if ret == True:
                # Order checker detections to unified convention
                corners = self.reorder_corners(corners)

                # print('Success!')
                # Draw and display the corners + manual rejection
                corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
                if viz and not take_all:
                    img_viz = cv2.drawChessboardCorners(np.copy(img), self.checker_shape, corners2,ret)
                    cv2.imshow("Press 'y' to keep image", cv2.resize(img_viz, (480, 640)))
                    while True:
                        key = cv2.waitKey(0) & 0xFF
                        if key in [ord('y'), ord('n')]:
                            break
                    if key != ord('y'):
                        ret = False
                elif viz:
                    img_viz = cv2.drawChessboardCorners(np.copy(img), self.checker_shape, corners2,ret)
                    cv2.imshow("img", cv2.resize(img_viz, (480, 640)))
                    cv2.waitKey(50)

                if ret:
                    objpoints.append(objp)

                    imgpoints.append(corners2)

                    images_raw.append(img)
                    image_indices.append(img_idx)
                else:
                    print('\033[33mSkipping image %d!\033[00m' % img_idx)

        cv2.destroyAllWindows()

        img_shape = gray.shape[::-1]
        return objpoints, imgpoints, img_shape, images_raw, image_indices
    
    def is_valid_grid_constraint(self, pts, img_shape, coverage_th=0.7, top_ignore=0, bottom_ignore=0):
        '''Test if constraints are well distributed on the image'''
        block_size = np.array(img_shape).max() / 10
        u_blocks, v_blocks = int(img_shape[0] / block_size), int(img_shape[1] / block_size)
        uvs = pts.reshape(-1,2)
        u_bin, v_bin = (uvs / block_size).astype(int).T
        u_bin = np.clip(u_bin,0,u_blocks-1)
        v_bin = np.clip(v_bin,0,v_blocks-1)
        grid = np.zeros((v_blocks, u_blocks))
        grid[v_bin, u_bin] = 1
        if bottom_ignore > 0:
            coverage = grid[top_ignore:-bottom_ignore, 1:-1].mean()
        else:
            coverage = grid[top_ignore:, 1:-1].mean()
        # print(grid, coverage)
        return coverage > coverage_th, coverage
        
    
    def reorder_corners(self, corners):
        top_right = corners[0][0][0], corners[0][0][1]
        bottom_left = corners[-1][0][0], corners[-1][0][1]
        if top_right[0] < bottom_left[0] and top_right[1] > bottom_left[1]:
            print('Flipping corners order')
            return np.flip(corners, axis=0).copy() # reverse order
        else:
            return corners
    
    def iterative_stereo_calib(self, all_objpoints, all_uv_l, all_uv_r, K_l, D_l, K_r, D_r,  img_shape, sample_size=10, n_attempts=100, is_fisheye=False, stop_th=0.35, min_attempts=20):
        while True:
            best_rms = np.inf
            best_sample = None
            if is_fisheye:
                stereo_cal_flags = cv2.CALIB_FIX_INTRINSIC
            else:
                stereo_cal_flags = cv2.CALIB_FIX_PRINCIPAL_POINT | cv2.CALIB_FIX_ASPECT_RATIO

            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-6)
            valid_attempts = 0
            img_bbox = BoundingBox2D([0, 0, img_shape[0], img_shape[1]])
            for n in range(n_attempts):
                sample = np.sort(np.random.choice(len(all_objpoints), sample_size, replace=False))
                objpoints = all_objpoints[sample]
                uv_l = all_uv_l[sample]
                uv_r = all_uv_r[sample]
                uv_l_covered_enough, uv_l_coverage = self.is_valid_grid_constraint(uv_l, img_shape, coverage_th=0.4, bottom_ignore=0)
                uv_r_covered_enough, uv_r_coverage = self.is_valid_grid_constraint(uv_r, img_shape, coverage_th=0.4, bottom_ignore=0)
                if not uv_l_covered_enough:
                    print("iteration: {}, only {} of the grid is covered".format(n, uv_l_coverage))
                    continue
                elif not uv_r_covered_enough:
                    print("iteration: {}, only {} of the grid is covered".format(n, uv_r_coverage))
                    continue
                try:
                    # Run single stereo calibration
                    if is_fisheye:
                        args = cv2.fisheye.stereoCalibrate(objpoints.copy(), uv_l.copy(), uv_r.copy(), K_l.copy(), D_l.copy(), K_r.copy(), D_r.copy(), img_shape, criteria=criteria, flags=stereo_cal_flags)
                    else:
                        args = cv2.stereoCalibrate(objpoints.copy(), uv_l.copy(), uv_r.copy(), K_l.copy(), D_l.copy(), K_r.copy(), D_r.copy(), img_shape, criteria=criteria, flags=stereo_cal_flags)
                    rms = args[0]
                    # Check that calibration has valid ROI
                    if is_fisheye:
                        pass # TODO
                    else:
                        (K_l_rect, D_l_rect, K_r_rect, D_r_rect, R, T, E, F) = args[1:]
                        roi_l, roi_r = cv2.stereoRectify(K_l_rect.copy(), D_l_rect.copy(), K_r_rect.copy(), D_r_rect.copy(), img_shape, R.copy(), T.copy(), alpha=0.1, flags=0)[-2:]
                        bbox_l = BoundingBox2D(roi_l)
                        bbox_r = BoundingBox2D(roi_r)
                        min_img_ratio = min(bbox_l.area(), bbox_r.area()) / img_bbox.area()
                        if min_img_ratio < 0.85:
                            rms = np.inf
                    if rms < np.inf:
                        print('iteration: {}, rms={:.2f}, baseline=={:.2f}.valid image ratio={:.2f}, x1 diff={:.2f}, x2 diff={:.2f}'.format(n, rms, T[0,0], min_img_ratio, bbox_l.x1 - bbox_r.x1, bbox_l.x2 - bbox_r.x2))
                        valid_attempts += 1
                    if rms < best_rms:
                        best_args = args[1:] # w/o rms
                        best_sample = sample.copy()
                        best_rms = rms
                    if best_rms < stop_th and valid_attempts > min_attempts:
                        break
                except:
                    pass

            if best_rms==np.inf:
                print('Iterative stereo calibration failed!, Initiating another iteration')
            else:
                break
        return best_rms, best_sample, best_args


    def iterative_intrinsic_calib(self, all_objpoints, all_imgpoints, img_shape, sample_size=10, n_attempts=100, is_fisheye=False, stop_th=0.3, min_attempts=20, fix_pp=True, with_distorion=True):
        best_rms = np.inf
        best_K = np.zeros((3, 3))
        best_D = np.zeros((4, 1))
        best_sample = None
        rvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(sample_size)]
        tvecs = [np.zeros((1, 1, 3), dtype=np.float64) for i in range(sample_size)]
        criteria = (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-6)
        if is_fisheye:
            calibration_flags = cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC + cv2.fisheye.CALIB_CHECK_COND + cv2.fisheye.CALIB_FIX_SKEW
        else:
            if fix_pp:
                calibration_flags = cv2.CALIB_FIX_PRINCIPAL_POINT | cv2.CALIB_SAME_FOCAL_LENGTH# | cv2.CALIB_FIX_K3
            else:
                calibration_flags = 0
        if not with_distorion:
            calibration_flags |= cv2.CALIB_ZERO_TANGENT_DIST | cv2.CALIB_FIX_K1 | cv2.CALIB_FIX_K2 | cv2.CALIB_FIX_K3 | cv2.CALIB_FIX_K4 | cv2.CALIB_FIX_K5 | cv2.CALIB_FIX_K6 
        valid_attempts = 0
        for n in range(n_attempts):
            sample = np.sort(np.random.choice(len(all_objpoints), sample_size, replace=False))
            objpoints = all_objpoints[sample]
            imgpoints = all_imgpoints[sample]
            bottom_ignore = 0# 2 if fix_pp else 0
            covered_enough, coverage = self.is_valid_grid_constraint(imgpoints, img_shape, coverage_th=0.5, bottom_ignore=bottom_ignore)
            if not covered_enough and n_attempts>10:
                print("iteration: {}, only {} of the grid is covered".format(n, coverage))
                continue
            K = np.zeros((3, 3))
            D = np.zeros((4, 1))
            try:
                if is_fisheye:
                    rms, _, _, _, _ = \
                        cv2.fisheye.calibrate(
                            objpoints,
                            imgpoints,
                            img_shape,
                            K,
                            D,
                            rvecs,
                            tvecs,
                            calibration_flags,
                            criteria=criteria 
                        )
                else:
                    rms, K, D, _, _ = cv2.calibrateCamera(objpoints, imgpoints, img_shape,None,None, criteria=criteria, flags=calibration_flags)
                print('iteration: {}, rms={:.2f}'.format(n, rms))
                valid_attempts += 1
                if rms < best_rms:
                    best_K = copy.deepcopy(K)
                    best_D = copy.deepcopy(D)
                    best_rms = copy.deepcopy(rms)
                    best_sample = copy.deepcopy(sample)
                if best_rms < stop_th and valid_attempts > min_attempts:
                    break
            except:
                pass

        assert best_rms < np.inf, 'Iterative intrinsic calibration failed!'
        return best_rms, best_K, best_D, best_sample

    def calc_camera_intrinsics(self, cam_dir, transpose=False, overwrite_vals=None, fix_pp=True, use_subset=None, stop_th=0.3, with_distorion=True, sample_size=10):
        images = glob.glob(os.path.join(cam_dir, '*.png'))

        # Get checkers
        # ============
        objpoints, imgpoints, img_shape , images_raw, image_indices =\
             self.get_checker_points(images, transpose=transpose)

        # Calculate intrinsics
        # ====================
        cam_name = os.path.basename(cam_dir)
        is_fisheye = self.cam_models[cam_name]=='fisheye'
        N_OK = len(imgpoints)
        objpoints = np.reshape(np.asarray(objpoints), (N_OK, 1, self.checker_shape[0] * self.checker_shape[1], 3))
        imgpoints = np.reshape(np.asarray(imgpoints), (N_OK, 1, self.checker_shape[0] * self.checker_shape[1], 2))
        if overwrite_vals:
            K = overwrite_vals['K']
            D = overwrite_vals['dist_coeffs']
            rms = np.nan
        else:
            if use_subset is not None:
                o_idx = np.array([i for i, idx in enumerate(image_indices) if idx in use_subset])
                rms, K, D, sample = self.iterative_intrinsic_calib(objpoints[o_idx], imgpoints[o_idx], img_shape, is_fisheye=is_fisheye,
                                                                   fix_pp=fix_pp, n_attempts=1, sample_size=len(o_idx), stop_th=stop_th, with_distorion=with_distorion)
            else:
                rms, K, D, sample = self.iterative_intrinsic_calib(objpoints, imgpoints, img_shape, is_fisheye=is_fisheye,
                                                                   fix_pp=fix_pp, stop_th=stop_th, with_distorion=with_distorion, sample_size=sample_size)

        print('\033[33mComputed params:\nK:\n', K, '\nD:\n', D,'\033[00m')
        print('\033[92mCalibrated %s intrinsic camera. fisheye=%r, RMS=%.2f\033[00m' % (cam_name, is_fisheye, rms))

        # Collect data
        data = dict(K=K, dist_coeffs=D, uvs=imgpoints, img_idx=image_indices, raw_imgs=images_raw, img_shape=img_shape, sample=sample)

        return data
    
    def calibrate_stereo(self, data_l, data_r, transpose=False, reduce_factors=[1.], sample_size=10):
        common_idx = []
        for imgl, idxl, uvl in zip(data_l['raw_imgs'], data_l['img_idx'], data_l['uvs']):
            if not idxl in data_r['img_idx']:
                continue
            common_idx.append(idxl)
        stereo_rect_dict, rect_intrinsics, chosen_idx = \
            self.calibrate_stereo_pair(copy.deepcopy(data_l), copy.deepcopy(data_r), common_idx, transpose=transpose, reduce_factors=reduce_factors, sample_size=sample_size)
        return stereo_rect_dict, rect_intrinsics, chosen_idx

    def calibrate_stereo_pair(self, data_l, data_r, common_idx, transpose=False, reduce_factors=[1.], sample_size=10):
        # termination criteria
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-5)
        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        objp = np.zeros((self.checker_shape[0]*self.checker_shape[1],3), np.float32)
        objp[:,:2] = np.mgrid[0:self.checker_shape[0],0:self.checker_shape[1]].T.reshape(-1,2)
        objp *= self.checker_size

        K_l, D_l, K_r, D_r = data_l['K'].copy(), data_l['dist_coeffs'].copy(), data_r['K'].copy(), data_r['dist_coeffs'].copy()
        rect_intrinsics = dict()
        stereo_calib = OrderedDict()
        idx_in_l = [i for i in data_l['img_idx'] if i in common_idx]
        idx_in_r = [i for i in data_r['img_idx'] if i in common_idx]
        # Make sure to take the smaller image size if left!=right size
        common_img_shape = data_l['img_shape'] if data_l['img_shape'][0] < data_r['img_shape'][0] else data_r['img_shape']
        f_l = common_img_shape[0] / data_l['img_shape'][0]
        f_r = common_img_shape[0] / data_r['img_shape'][0]
        K_l *= f_l
        K_r *= f_r
        for factor in reduce_factors:
            f_scale = np.array([[factor, 1., factor], [1., factor, factor], [1., 1., 1.]])
            img_shape = tuple((np.array(common_img_shape) * factor).astype(int))
            calib_name = 'img_%dx%d' % (img_shape[1], img_shape[0])
            uv_l = [factor * f_l * data_l['uvs'][data_l['img_idx'].index(i)]\
                 for i in data_l['img_idx'] if i in common_idx]
            uv_r = [factor * f_r * data_r['uvs'][data_r['img_idx'].index(i)]\
                 for i in data_r['img_idx'] if i in common_idx]
            objpoints = [objp] * len(uv_l)
            # Rectify and compute new camera and distortion parameters
            # ========================================================
            is_fisheye = self.cam_models['left']=='fisheye' and self.cam_models['right']=='fisheye'
            N_OK = len(uv_l)
            objpoints = np.reshape(np.asarray(objpoints), (N_OK, 1, self.checker_shape[0] * self.checker_shape[1], 3))
            uv_l = np.reshape(np.asarray(uv_l), (N_OK, 1, self.checker_shape[0] * self.checker_shape[1], 2))
            uv_r = np.reshape(np.asarray(uv_r), (N_OK, 1, self.checker_shape[0] * self.checker_shape[1], 2))
            # Run iterative stereo calibration for highest resolution
            if factor==1.:
                rms, chosen_sample, ret_args = self.iterative_stereo_calib(objpoints, uv_l.copy(), uv_r.copy(), K_l.copy(), D_l.copy(), K_r.copy(), D_r.copy(), img_shape,
                                                                           is_fisheye=is_fisheye, sample_size=sample_size)
                chosen_idx = np.array(idx_in_l)[chosen_sample]
                print('\033[92mCalibrated stereo calibration. fisheye=%r, RMS=%.2f\033[00m' % (is_fisheye, rms))
                print('\033[92mBest sample: ', chosen_idx, '\033[00m')
            # Compute rectification maps
            if is_fisheye:
                stereo_rect_flags = cv2.CALIB_ZERO_DISPARITY
                _, _, _, _, R, T = ret_args
                K_l_scale = K_l * f_scale
                K_r_scale = K_r * f_scale
                R_l, R_r, P_l, P_r, Q = cv2.fisheye.stereoRectify(K_l_scale, D_l, K_r_scale, D_r, img_shape, R, T, stereo_rect_flags,
                                                                  balance=self.fisheye_fov_balance, fov_scale=self.fisheye_fov_scale)
                mapL1, mapL2 = cv2.fisheye.initUndistortRectifyMap(K_l_scale, D_l, R_l, P_l, img_shape, cv2.CV_32FC1)
                mapR1, mapR2 = cv2.fisheye.initUndistortRectifyMap(K_r_scale, D_r, R_r, P_r, img_shape, cv2.CV_32FC1)
            else:
                (K_l, D_l, K_r, D_r, R, T, E, F) = ret_args
                K_l_scale = K_l * f_scale
                K_r_scale = K_r * f_scale
                R_l, R_r, P_l, P_r, Q, roi_l, roi_r = cv2.stereoRectify(K_l_scale.copy(), D_l.copy(), K_r_scale.copy(), D_r.copy(), img_shape, R, T, alpha=0.1, flags=0)
                mapL1, mapL2 = cv2.initUndistortRectifyMap(K_l_scale.copy(), D_l.copy(), R_l.copy(), P_l.copy(), img_shape, cv2.CV_32FC1)
                mapR1, mapR2 = cv2.initUndistortRectifyMap(K_r_scale.copy(), D_r.copy(), R_r.copy(), P_r.copy(), img_shape, cv2.CV_32FC1)

            # Save skewed intrinsic values
            # ============================
            if factor==1:
                rect_intrinsics['left_rect'] = dict(K=K_l_scale.copy(), dist_coeffs=D_l.copy())
                rect_intrinsics['right_rect'] = dict(K=K_r_scale.copy(), dist_coeffs=D_r.copy())
            if self.viz:
                common_img_l = [data_l['raw_imgs'][data_l['img_idx'].index(i)] for i in data_l['img_idx'] if i in common_idx]
                common_img_r = [data_r['raw_imgs'][data_r['img_idx'].index(i)] for i in data_r['img_idx'] if i in common_idx]
                for img_l, img_r in zip(common_img_l, common_img_r):
                    img_l = cv2.resize(img_l, img_shape)
                    img_r = cv2.resize(img_r, img_shape)
                    img_l_rect = cv2.remap(img_l, mapL1, mapL2, cv2.INTER_LINEAR)
                    img_r_rect = cv2.remap(img_r, mapR1, mapR2, cv2.INTER_LINEAR)
                    disp_img = self.opencv_disparity(img_l_rect, img_r_rect, 1)
                    img_viz = np.hstack((img_l_rect, img_r_rect))
                    # draw horizontal lines every 25 px accross the side by side image
                    for i in range(20, img_viz.shape[0], 25):
                        cv2.line(img_viz, (0, i), (img_viz.shape[1], i), (255, 0, 0))
                    img_viz = np.hstack((img_viz, disp_img))
                    viz_scale = 640. / img_viz.shape[0]
                    img_viz = cv2.resize(img_viz, (0,0), fx=viz_scale, fy=viz_scale)
                    cv2.imshow('After rectification to shape (%d, %d)' % (img_shape[1], img_shape[0]),img_viz)
                    cv2.waitKey(50)
            
            cv2.destroyAllWindows()
            stereo_calib[calib_name]  = dict(l1=mapL1.copy(), l2=mapL2.copy(), r1=mapR1.copy(), r2=mapR2.copy(),
                                             rl=R_l.copy(), rr=R_r.copy(), pl=P_l.copy(), pr=P_r.copy(), q=Q.copy(),
                                             shape=img_shape, scale=factor, rot=R.copy(), trans=T.copy())
        # Validate no change in stereo params
        for _, ssc in stereo_calib.items():
            K_l_scale = K_l * ssc['scale']
            K_r_scale = K_r * ssc['scale']
            test_mapL1, test_mapL2 = cv2.initUndistortRectifyMap(K_l_scale, D_l, ssc['rl'], ssc['pl'], ssc['shape'], cv2.CV_32FC1)
            test_mapR1, test_mapR2 = cv2.initUndistortRectifyMap(K_r_scale, D_r, ssc['rr'], ssc['pr'], ssc['shape'], cv2.CV_32FC1)
            assert np.all(np.isclose(test_mapL1, ssc['l1']))
            assert np.all(np.isclose(test_mapL2, ssc['l2']))
            assert np.all(np.isclose(test_mapR1, ssc['r1']))
            assert np.all(np.isclose(test_mapR2, ssc['r2']))
            print('\033[33mRemap parameters verified:', ssc['shape'], '\033[00m')

        print('\033[33mComputed params:\nR:\n', R, '\nt:\n', T.ravel(),'\033[00m')
        return stereo_calib, rect_intrinsics, chosen_idx

    def compute_extrinsics(self, data1, data2, common_indices, permute_facing_cams=False, data_right=None):
        # Set checkboard 3d coordinates
        objp = np.zeros((self.checker_shape[0]*self.checker_shape[1],3), np.float32)
        objp[:,:2] = np.mgrid[0:self.checker_shape[0],0:self.checker_shape[1]].T.reshape(-1,2)
        objp = objp * self.checker_size
        K1 = data1['K']
        D1 = data1['dist_coeffs']
        K2 = data2['K']
        D2 = data2['dist_coeffs']
        best_n_inliers = 0
        for _img1, idx1, uv1 in zip(data1['raw_imgs'], data1['img_idx'], data1['uvs'].squeeze()):
            img1 = _img1.copy()
            if not idx1 in common_indices:
                continue
            if data_right is not None:
                if idx1 not in data_right['img_idx']:
                    continue
                idx_in_right = data_right['img_idx'].index(idx1)
                img_right = data_right['raw_imgs'][idx_in_right].copy()
                uv_right = data_right['uvs'][idx_in_right].squeeze()
            idx_in_2 = data2['img_idx'].index(idx1)
            img2 = data2['raw_imgs'][idx_in_2].copy()
            uv2 = data2['uvs'][idx_in_2].squeeze()

            
            # Solve PnP
            # =========
            success1, rv1, tv1, inl1 = cv2.solvePnPRansac(objp, uv1, K1, D1)
            # if success1:
            #     print('Computer Pose1 with %d inliers' % (len(inl1)))
            if permute_facing_cams:
                # Flip 3d points from left to right to adjust to image from oposite side
                objp_new = np.fliplr(objp.reshape(self.checker_shape[1],self.checker_shape[0],3)).reshape(-1,3)
            else:
                objp_new = objp
            success2, rv2, tv2, inl2 = cv2.solvePnPRansac(objp_new, uv2, K2, D2)
            # if success2:
            #     print('Computer Pose2 with %d inliers' % (len(inl2)))

            # Compute relative pose
            w_T_cam1 = Pose((rv1, tv1)).inv()
            w_T_cam2 = Pose((rv2, tv2)).inv()
            cam1_T_cam2 = w_T_cam1.trans_to(w_T_cam2)

            # Viz
            new_best = success1 and success2 and inl1.size + inl2.size > best_n_inliers
            if new_best and False:
                def draw(img, corners, imgpts):
                    corner = tuple(corners[0].ravel())
                    img = cv2.line(img, corner, tuple(imgpts[0].ravel()), (255,0,0), 5)
                    img = cv2.line(img, corner, tuple(imgpts[1].ravel()), (0,255,0), 5)
                    img = cv2.line(img, corner, tuple(imgpts[2].ravel()), (0,0,255), 5)
                    return img
                # project 3D points to image plane
                axis = self.checker_size * np.float32([[3,0,0], [0,3,0], [0,0,-3]]).reshape(-1,3)
                imgpts1, jac = cv2.projectPoints(axis, rv1, tv1, K1, D1)
                img_viz1 = draw(img1,uv1,imgpts1)
                imgpts2, jac = cv2.projectPoints(axis, rv2, tv2, K2, D2)
                if permute_facing_cams:
                    # Use reordered image points for visualization
                    uv2 = reorder_facing_cam_uv(uv2, self.checker_shape)
                img_viz2 = draw(img2.copy(),uv2, imgpts2)
                # Transfrom 3d points between images
                points3d_cam1 = w_T_cam1.inv().project(objp)
                points3d_cam2 = cam1_T_cam2.project(points3d_cam1)
                imgpts1, _ = cv2.projectPoints(points3d_cam1, np.zeros((3,1)), np.zeros((3,1)), K1, D1)
                imgpts2, _ = cv2.projectPoints(points3d_cam2, np.zeros((3,1)), np.zeros((3,1)), K2, D2)
                if data_right is not None:
                    img_right_viz = img_right.copy()
                for idx, (p1, p2) in enumerate(zip(imgpts1.squeeze().astype(int), imgpts2.squeeze().astype(int))):
                    cl = (np.random.random(3) * 255).astype(int).tolist()
                    cv2.circle(img_viz1, (p1[0],p1[1]), 4, cl, -1)
                    cv2.circle(img_viz2, (p2[0],p2[1]), 4, cl, -1)
                    if data_right is not None:
                        pt = uv_right[idx].astype(int)
                        cv2.circle(img_right_viz, (pt[0],pt[1]), 4, cl, -1)


                if data_right is not None:
                    cv2.imshow('ext_calib',np.hstack((img_viz1, img_right_viz, img_viz2)))
                else:
                    cv2.imshow('ext_calib',np.hstack((img_viz1, img_viz2)))
                cv2.waitKey(0)
            
            points3d_cam1 = w_T_cam1.inv().project(objp)

            if new_best:
                best_cam1_T_cam2 = cam1_T_cam2
                best_points3d_cam1 = points3d_cam1
                best_idx1 = idx1
                best_n_inliers = inl1.size + inl2.size
                print('Curr best inliers: %s, idx=%d' % (best_n_inliers, best_idx1))
                print(cam1_T_cam2, ', Norm=%.3f' % (np.linalg.norm(cam1_T_cam2.t())))

        return best_cam1_T_cam2, best_points3d_cam1, best_idx1
    
    def opencv_disparity(self, imgl, imgr, factor):
        viz_scale = 0.25
        # Compute disparity map
        # =====================
        num_disparities = int(64 * factor)
        block_size = int(15 * factor) + 1
        stereoProcessor = cv2.StereoSGBM_create(0, num_disparities, block_size)
        gray_l = cv2.cvtColor(imgl, cv2.COLOR_BGR2GRAY)
        gray_r = cv2.cvtColor(imgr, cv2.COLOR_BGR2GRAY)
        disparity = stereoProcessor.compute(gray_l, gray_r)
        disp_u8 = np.clip(disparity * viz_scale, a_min=0, a_max=255).round().astype(np.uint8)
        disp_viz = cv2.cvtColor(disp_u8.astype(np.uint8) , cv2.COLOR_GRAY2BGR)
        return disp_viz

    def verify_transform(self, calib_data, stereo_calib, xyz_test, ext_coupling, stereo_coupling, rel_pose, common_idx,
                         use_openvx=False, calib_dir=None, frame_l=None, frame_r=None, frame_b=None):
        assert ext_coupling[0]==stereo_coupling[0]
        data_l = calib_data[stereo_coupling[0]]
        data_r = calib_data[stereo_coupling[1]]
        data_back = calib_data[ext_coupling[1]]
        if 'img_320x240' in stereo_calib.keys():
            stereo_rect_dict = stereo_calib
            stereo_calib = stereo_calib['img_320x240']
        factor = stereo_calib['scale']
        if not use_openvx:
            disp_scale = 1./16
            viz_scale = 0.25
            # Compute disparity map
            # =====================
            num_disparities = int(64 * factor)
            block_size = int(15 * factor) + 1
            stereoProcessor = cv2.StereoSGBM_create(0, num_disparities, block_size)
            ix_l = data_l['img_idx'].index(common_idx)
            ix_r = data_r['img_idx'].index(common_idx)
            ix_b = data_back['img_idx'].index(common_idx)
            assert data_l['img_idx'][ix_l]==data_r['img_idx'][ix_r]
            assert data_l['img_idx'][ix_l]==data_back['img_idx'][ix_b]
            imgl = cv2.resize(data_l['raw_imgs'][ix_l], stereo_calib['shape'])
            imgr = cv2.resize(data_r['raw_imgs'][ix_r], stereo_calib['shape'])
            imgb = cv2.resize(data_back['raw_imgs'][ix_b], stereo_calib['shape'])
            gray_l = cv2.cvtColor(imgl, cv2.COLOR_BGR2GRAY)
            gray_r = cv2.cvtColor(imgr, cv2.COLOR_BGR2GRAY)
            disparity = stereoProcessor.compute(gray_l, gray_r)
        else:
            # VX disparity outputs result in S16 format 11.4 (11 bit integer, 4 bit fraction)
            # Before casting result to U8, we shift result to the right=> dividing by 2^(shift)
            # Shift = 0 => result will be 4.4 => to get real disparity value divide by 16
            # Shift = 1 => result will be 5.3 => to get real disparity value divide by 8
            # Shift = 2 => result will be 6.2 => to get real disparity value divide by 4
            disp_scale = 0.25
            viz_scale = 1
            import streams.stereo.vx._main_stereo_matching as msm
            from md_blocks.memory.buffer_manager import BufferManager
            buffer_manager = BufferManager()
            cam_sizes_dict = dict()
            cam_sizes_dict_vx = dict()
            for cam_name in ('left', 'right', 'back'):
                cam_sizes_dict['height_%s' % cam_name] = str(480)
                cam_sizes_dict['width_%s' % cam_name] = str(640)
                cam_sizes_dict_vx['height_%s' % cam_name] = str(640) # Transposed
                cam_sizes_dict_vx['width_%s' % cam_name] = str(480) # Transposed
            buffer_manager.allocate_default_cam_buffers(cam_sizes_dict)
            buffer_manager.allocate_default_vx_buffers(cam_sizes_dict)
            vx_input_names = ('left_cam_raw', 'right_cam_raw', 'back_cam_raw', 'spatial_th')
            vx_output_names = ('disparity', 'grayL_resized','grayB_resized',
                                'frameL_resized', 'frameB_resized',
                                'cart_disp_mask', 'left_remap', 'frameDelta', 'frameDeltaBack',
                                'frameDeltaMaskBack', 'frameL_resized_masked', 'frameB_resized_masked')
            vx_prcss = msm.init(cam_sizes_dict_vx, calib_dir, 
                               buffer_manager.init_buffer_list(vx_input_names, msm.new_BufferImportExt), 
                               buffer_manager.init_buffer_list(vx_output_names, msm.new_BufferImportExt),
                               stereo_rect_dict['img_640x480']['l1'], stereo_rect_dict['img_640x480']['l2'],
                               stereo_rect_dict['img_320x240']['l1'], stereo_rect_dict['img_320x240']['l2'],
                               stereo_rect_dict['img_320x240']['r1'], stereo_rect_dict['img_320x240']['r2'],
                               True)
            [frame_l, frame_r, frame_b] = [cv2.rotate(fs, cv2.ROTATE_90_COUNTERCLOCKWISE)\
                 for fs in [frame_l, frame_r, frame_b]]
            # Run VX to get disparity
            buffer_manager.get('left_cam_raw')[:] = cv2.resize(frame_l, (640, 480))
            buffer_manager.get('right_cam_raw')[:] = cv2.resize(frame_r, (640, 480))
            buffer_manager.get('back_cam_raw')[:] = cv2.resize(frame_b, (640, 480))
            msm.process_stream(vx_prcss, 0)
            disparity = buffer_manager.get('disparity')
            imgl = buffer_manager.get('frameL_resized')
            imgb = buffer_manager.get('frameB_resized')
        if False: # show disparity for debug
            plt.figure()
            plt.subplot(131)
            plt.imshow(disparity * disp_scale, cmap='gray')
            plt.colorbar()
            plt.subplot(132)
            plt.imshow(imgl)
            plt.subplot(133)
            plt.imshow(imgr)
            plt.show()

        # get front cam 2d points and disparity values
        K_scale = np.array([[factor, 1., factor], [1., factor, factor], [1., 1., 1.]])
        Kl = data_l['K'] * K_scale
        Dl = data_l['dist_coeffs']
        Kb = data_back['K'] * K_scale
        Db = data_back['dist_coeffs']
        cam_l = Camera(Pose(), Kl, D=Dl, Q=stereo_calib['q'])
        cam_b = Camera(rel_pose, Kb, D=Db)
        uv1, _ = cam_l.xyz2uv(xyz_test)
        xyz1 = cam_l.disp2xyz(disparity, uv1,disp_scale=disp_scale)
        uv2, is_visible = cam_l.project_xyz_other_cam(cam_b, xyz1, with_distortion=True)
        # uv2 = uv2[is_visible]
        h, w = imgl.shape[:2]
        uv2[:,0] = np.clip(uv2[:,0], 0, w-1)
        uv2[:,1] = np.clip(uv2[:,1], 0, h-1)

        for idx, (p1, p2, show2) in enumerate(zip(uv1.astype(int), uv2.astype(int), is_visible)):
            cl = (np.random.random(3) * 255).astype(int).tolist()
            cv2.circle(imgl, (p1[0],p1[1]), 4, cl, -1)
            if show2:
                cv2.circle(imgb, (p2[0],p2[1]), 4, cl, -1)

        disp_u8 = np.clip(disparity * viz_scale, a_min=0, a_max=255).round().astype(np.uint8)
        disp_viz = cv2.cvtColor(disp_u8.astype(np.uint8) , cv2.COLOR_GRAY2BGR)
        cv2.imshow('img',np.hstack((imgl, imgb, disp_viz)))
        cv2.waitKey(0)
            
    @staticmethod
    def dump_calib_dir(out_dir, stereo_dict, ext_dict, int_dict, transpose_maps=False):
        os.makedirs(out_dir, exist_ok=True)
        # Stereo maps
        calib_keys = ('rot', 'trans', 'scale', 'rr', 'shape', 'pr', 'rl', 'q', 'pl')
        out_stereo_dict = dict()
        for k,v in stereo_dict.items():
            out_stereo_dict[k] = dict()
            for calib_k in calib_keys:
                out_stereo_dict[k][calib_k] = v[calib_k]
        name_mapping = dict(img_1280x960='1p0', img_640x480='0p5', img_320x240='0p25')
        for name, st_dict in stereo_dict.items():
            l1_path = os.path.join(out_dir, '%s_remap_table_mapL1.csv' % name_mapping[name])
            l2_path = os.path.join(out_dir, '%s_remap_table_mapL2.csv' % name_mapping[name])
            r1_path = os.path.join(out_dir, '%s_remap_table_mapR1.csv' % name_mapping[name])
            r2_path = os.path.join(out_dir, '%s_remap_table_mapR2.csv' % name_mapping[name])
            np.savetxt(l1_path, st_dict['l1'].astype(np.float32), delimiter=" ", fmt='%.2f')
            np.savetxt(l2_path, st_dict['l2'].astype(np.float32), delimiter=" ", fmt='%.2f')
            np.savetxt(r1_path, st_dict['r1'].astype(np.float32), delimiter=" ", fmt='%.2f')
            np.savetxt(r2_path, st_dict['r2'].astype(np.float32), delimiter=" ", fmt='%.2f')
            # Add transpose maps
            if transpose_maps:
                h, w = st_dict['l1'].shape
                l1_path_tr = os.path.join(out_dir, '%s_remap_table_mapL1_transpose.csv' % name_mapping[name])
                l2_path_tr = os.path.join(out_dir, '%s_remap_table_mapL2_transpose.csv' % name_mapping[name])
                r1_path_tr = os.path.join(out_dir, '%s_remap_table_mapR1_transpose.csv' % name_mapping[name])
                r2_path_tr = os.path.join(out_dir, '%s_remap_table_mapR2_transpose.csv' % name_mapping[name])
                np.savetxt(l1_path_tr, st_dict['l2'].astype(np.float32), delimiter=" ", fmt='%.2f')
                np.savetxt(l2_path_tr, w - st_dict['l1'].astype(np.float32), delimiter=" ", fmt='%.2f')
                np.savetxt(r1_path_tr, st_dict['r2'].astype(np.float32), delimiter=" ", fmt='%.2f')
                np.savetxt(r2_path_tr, w - st_dict['r1'].astype(np.float32), delimiter=" ", fmt='%.2f')
        calib_keys = ('K', 'dist_coeffs')
        out_int_dict = dict()
        for k,v in int_dict.items():
            out_int_dict[k] = dict()
            for calib_k in calib_keys:
                out_int_dict[k][calib_k] = v[calib_k]
        calib_file = os.path.join(out_dir, 'calib.pkl')
        calib_all_dict = dict(intrinsics=out_int_dict, extrinsics=extrinsic_dict, stereo=out_stereo_dict)
        pickle.dump(calib_all_dict, open(calib_file, 'wb'))
        return name_mapping


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Calibration flow args')
    parser.add_argument('--movie-name', help='path to movie file', type=str, default=None)
    parser.add_argument('--calib-dir', help='calibration directory',type=str, default='/tmp/sys_calib')
    parser.add_argument('--checker-h', help='checkerboard height',type=int, default=8)
    parser.add_argument('--checker-w', help='checkerboard width',type=int, default=6)
    parser.add_argument('--sample-size', help='num images to use for calibration',type=int, default=8)
    parser.add_argument('--checker-size', help='checkerboard square size',type=float, default=0.03)
    parser.add_argument('--rotate', help='rotate images',action='store_true')
    parser.add_argument('--fisheye', help='use fisheye model',action='store_true')
    parser.add_argument('--cart-info', help='cart id name or file containing camera info',type=str, default='basler_sys3.01')
    parser.add_argument('--force', help='run calibration even when overwriting existing one',action='store_true')
    parser.add_argument('--ext-only', help='Assume intrinsic calib exists, calib only extrinsics',action='store_true')
    args = parser.parse_args()

    if args.ext_only:
        assert os.path.isdir(os.path.join(args.calib_dir, 'calibration')), 'Full calibration info must exist for extrinsics only mode'

    # Load camera info
    # ================
    live_movie = args.movie_name is None
    if live_movie:
        try:
            if not os.path.isfile(args.cart_info):
                camera_info_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'models/%s/cart_config.yaml' % args.cart_info)
            else:
                camera_info_file = args.cart_info
            camera_info = yaml.safe_load(open(camera_info_file, 'r'))['camera_info']
        except:
            raise NameError('Invalid name of camera id/info file: %s' % args.cart_info)
        
        # apply suitable settings for cameras
        for k in camera_info.keys():
            camera_info[k]['software_trigger'] = True
            camera_info[k]['pixel_format'] = 'RGB8'
            camera_info[k]['exposure_ns'] = 10e3
    else:
        camera_info = dict(left=None, right=None, back=None)

    if args.fisheye:
        cam_models = dict(left='fisheye', right='fisheye', back='pinhole', left_rect='pinhole', right_rect='pinhole')
    else:
        cam_models = dict(left='pinehole', right='pinehole', back='pinhole', left_rect='pinhole', right_rect='pinhole')


    # Params
    # ======
    CHECKER_SHAPE = (args.checker_w,args.checker_h)
    transpose = args.rotate
    images_dir = args.calib_dir
    # Capture checker images
    # ======================
    if not os.path.exists(os.path.join(images_dir, 'left')):
        if live_movie:
            stream = BaseImageGrabber('basler', camera_info, debug=False, target_ms=80)
        else:
            movie_addr_wo_suffix = args.movie_name.split('_L.avi')[0]
            stream = BaseImageGrabber('playback', movie_addr_wo_suffix, target_ms=50)

        img_grab_process = Process(target=stream.run)
        img_grab_process.start()
        my_grabber = ImageGrabber(stream, proc_dir=images_dir, live_movie=live_movie)
        while True:
            out, quit_proc = my_grabber.step()
            if quit_proc:
                break
            lb_vis = np.hstack((cv2.rotate(cv2.resize(out['left'], (320,240)), cv2.ROTATE_90_CLOCKWISE),
                                cv2.rotate(cv2.resize(out['right'], (320,240)), cv2.ROTATE_90_CLOCKWISE),
                                cv2.rotate(cv2.resize(out['back'], (320,240)), cv2.ROTATE_90_CLOCKWISE)))
            cv2.imshow('Left,Right,Back', lb_vis)
            cv2.waitKey(1)
        img_grab_process.terminate()
    
    intrinsics_file = os.path.join(images_dir, 'calibration', 'intrinsics.pkl')
    extrinsics_file = os.path.join(images_dir, 'calibration', 'extrinsics.pkl')
    stereo_rect_file = os.path.join(images_dir, 'calibration', 'stereo_data.pkl')
    my_calibrator = CheckerCalibrator(cam_models, checker_shape=CHECKER_SHAPE, checker_size=args.checker_size)

    os.makedirs(os.path.join(images_dir, 'calibration'), exist_ok=True)
    # Intrinsic calibration
    # =====================
    calib_data = dict()
    while True:
        if not os.path.exists(intrinsics_file) or args.force:
            cam_subdir = ('left', 'right', 'back')
            for d in cam_subdir:
                print('\033[94mComputing intrinsics for %s camera\033[00m' % d)
                calib_data[d] = my_calibrator.calc_camera_intrinsics(os.path.join(images_dir, d), transpose=transpose, sample_size=args.sample_size)
            # Save to disk
            os.makedirs(os.path.dirname(intrinsics_file), exist_ok=True)
            pickle.dump(calib_data, open(intrinsics_file, 'wb'))
        else:
            print('Loading intrinsic calibration from cache')
            calib_data = pickle.load(open(intrinsics_file, 'rb'))

        # Rectify + undistort stereo pair
        # ===============================
        if args.ext_only:
            assert os.path.exists(stereo_rect_file)
        coupling = ('left', 'right')
        if not os.path.exists(stereo_rect_file) or args.force:
            print('\033[94mComputing stereo parameters\033[00m')
            if min(max(calib_data['left']['img_shape']), max(calib_data['right']['img_shape']))==640:
                factors = [1., 0.5]
            else:
                factors = [1., 0.5, 0.25]
            stereo_rect_dict, rect_intrinsics, chosen_idx = \
                my_calibrator.calibrate_stereo(calib_data[coupling[0]], calib_data[coupling[1]], transpose=transpose,reduce_factors=factors, sample_size=args.sample_size)
            pickle.dump(stereo_rect_dict, open(stereo_rect_file, 'wb'))
        else:
            print('Loading stereo calibration from cache')
            stereo_rect_dict = pickle.load(open(stereo_rect_file, 'rb'))
            break
        cv2.imshow("Repeat calibration? (y/n)", np.ones((500, 500)))
        while True:
            key = cv2.waitKey(0) & 0xFF
            if key in [ord('y'), ord('n')]:
                cv2.destroyAllWindows()
                break
        if key == ord('y'):
            os.remove(stereo_rect_file)
            # os.remove(intrinsics_file) # don't repeat intrinsic calibration
        else:
            break

    if args.ext_only:
        # Overwrite (left,right,back) images but use old extrinsics
        # =========================================================
        calib_data_overwrite = calib_data.copy()
        cam_subdir = camera_info.keys()
        for d in ('left', 'right', 'back'):
            print('\033[94mExtracting checker locations for %s camera\033[00m' % d)
            cam_dir = os.path.join(images_dir, d)
            images = glob.glob(os.path.join(cam_dir, '*.png'))
            # Get checkers
            # ============
            objpoints, imgpoints, img_shape , images_raw, image_indices =\
                my_calibrator.get_checker_points(images, transpose=transpose)
            N_OK = len(imgpoints)
            imgpoints = np.reshape(np.asarray(imgpoints), (N_OK, 1, my_calibrator.checker_shape[0] * my_calibrator.checker_shape[1], 2))
            calib_data_overwrite[d]['img_idx'] = image_indices
            calib_data_overwrite[d]['raw_imgs'] = images_raw
            calib_data_overwrite[d]['uvs'] = imgpoints
            
    # Apply Remap for stereo pair and save images and add to calib
    # ============================================================
    max_res_key = 'img_1280x960' if 'img_1280x960' in stereo_rect_dict else 'img_640x480'
    stereo_max_dict = stereo_rect_dict[max_res_key]
    left_rect_dir = os.path.join(images_dir, 'left_rect')
    right_rect_dir = os.path.join(images_dir, 'right_rect')
    # make sure rectified images have the same size
    common_shape = calib_data['left']['img_shape'] if calib_data['left']['img_shape'][0]<calib_data['right']['img_shape'][0] \
        else calib_data['right']['img_shape']
    if not os.path.exists(left_rect_dir) or len(os.listdir(left_rect_dir))==0 or args.force or args.ext_only:
        if args.ext_only:
            img_left_rect = [cv2.remap(cv2.resize(img, common_shape), stereo_max_dict['l1'], stereo_max_dict['l2'], cv2.INTER_LINEAR) for img in calib_data_overwrite['left']['raw_imgs']]
            img_right_rect = [cv2.remap(cv2.resize(img, common_shape), stereo_max_dict['r1'], stereo_max_dict['r2'], cv2.INTER_LINEAR) for img in calib_data_overwrite['right']['raw_imgs']]
        else:
            img_left_rect = [cv2.remap(cv2.resize(img, common_shape), stereo_max_dict['l1'], stereo_max_dict['l2'], cv2.INTER_LINEAR) for img in calib_data['left']['raw_imgs']]
            img_right_rect = [cv2.remap(cv2.resize(img, common_shape), stereo_max_dict['r1'], stereo_max_dict['r2'], cv2.INTER_LINEAR) for img in calib_data['right']['raw_imgs']]
        os.makedirs(left_rect_dir, exist_ok=True)
        os.makedirs(right_rect_dir, exist_ok=True)
        for img_idx, img in zip(calib_data['left']['img_idx'], img_left_rect):
            img_path = os.path.join(left_rect_dir, '%04d.png' % img_idx)
            cv2.imwrite(img_path, img)
        for img_idx, img in zip(calib_data['right']['img_idx'], img_right_rect):
            img_path = os.path.join(right_rect_dir, '%04d.png' % img_idx)
            cv2.imwrite(img_path, img)
        cam_subdir = ['left_rect', 'right_rect']
        if args.ext_only:
            for d in cam_subdir:
                print('\033[94mExtracting checker locations for %s camera\033[00m' % d)
                cam_dir = os.path.join(images_dir, d)
                images = glob.glob(os.path.join(cam_dir, '*.png'))
                # Get checkers
                # ============
                objpoints, imgpoints, img_shape , images_raw, image_indices =\
                    my_calibrator.get_checker_points(images, transpose=False)
                N_OK = len(imgpoints)
                imgpoints = np.reshape(np.asarray(imgpoints), (N_OK, 1, my_calibrator.checker_shape[0] * my_calibrator.checker_shape[1], 2))
                calib_data_overwrite[d]['img_idx'] = image_indices
                calib_data_overwrite[d]['raw_imgs'] = images_raw
                calib_data_overwrite[d]['uvs'] = imgpoints
        else:
            for d in cam_subdir:
                print('\033[94mComputing intrinsics for %s camera\033[00m' % d)
                if d=='left_rect' and 'right_rect' in calib_data:
                    use_subset = calib_data['right_rect']['sample']
                if d=='right_rect' and 'left_rect' in calib_data:
                    use_subset = calib_data['left_rect']['sample']
                else:
                    use_subset = None
                calib_data[d] = my_calibrator.calc_camera_intrinsics(os.path.join(images_dir, d), transpose=False, fix_pp=False, stop_th=0.25, with_distorion=False, sample_size=args.sample_size, use_subset=use_subset)
            pickle.dump(calib_data, open(intrinsics_file, 'wb'))
    else:
        calib_data = pickle.load(open(intrinsics_file, 'rb'))
    

    if args.ext_only:
        # Overwrite intrinsic calibration 
        # ===============================
        calib_data = calib_data_overwrite.copy()

    ext_coupling = ('left_rect', 'back')
    extrinsic_dict = dict()
    # Visualize constraints
    if False:
        visualize_matches(calib_data[ext_coupling[0]], calib_data[ext_coupling[1]], permute_facing_cams=True, checker_shape=CHECKER_SHAPE)
    
    # Make sure we can corretly project points from front to back camera
    ext_coupling = ('left_rect', 'back')
    stereo_coupling = ('left_rect', 'right_rect')
    common_indices = [i for i in calib_data[stereo_coupling[0]]['img_idx'] if
                      i in calib_data[stereo_coupling[1]]['img_idx'] and
                      i in calib_data[ext_coupling[1]]['img_idx']]
    # Compute relative pose between cameras
    cam1_T_cam2, pts_test, ext_img_idx = my_calibrator.compute_extrinsics(copy.deepcopy(calib_data[ext_coupling[0]]), copy.deepcopy(calib_data[ext_coupling[1]]), common_indices, permute_facing_cams=True,
        data_right=copy.deepcopy(calib_data['right_rect']))
    extrinsic_dict['%s_T_%s' % (ext_coupling[0], ext_coupling[1])] = cam1_T_cam2
    pickle.dump(extrinsic_dict, open(extrinsics_file, 'wb'))

    common_idx = ext_img_idx
    for name, st_dict in stereo_rect_dict.items():
        print('Verifying extrinsics & intrinsics compatability for %s' % name)
        my_calibrator.verify_transform(copy.deepcopy(calib_data), st_dict, pts_test, ext_coupling, stereo_coupling,
                                       cam1_T_cam2, common_idx)
    
    # Write to output directory in current format
    out_dir = os.path.join(images_dir, 'calib_light') # TODO: change naming convention
    CheckerCalibrator.dump_calib_dir(out_dir, stereo_rect_dict, extrinsic_dict, calib_data)

    # Make sure we correctly project  using OpenVX
    os.makedirs(os.path.join(out_dir, '../../common'), exist_ok=True)
    shutil.copy(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'common/vx_disparity_config.ini'),\
        os.path.join(out_dir, '../../common/vx_disparity_config.ini'))
    print('Verifying extrinsics & intrinsics compatability for OpenVX flow')
    ix_l = calib_data['left']['img_idx'].index(common_idx)
    ix_r = calib_data['right']['img_idx'].index(common_idx)
    ix_b = calib_data['back']['img_idx'].index(common_idx)
    assert calib_data['left']['img_idx'][ix_l]==calib_data['right']['img_idx'][ix_r]
    assert calib_data['left']['img_idx'][ix_l]==calib_data['back']['img_idx'][ix_b]
    my_calibrator.verify_transform(calib_data, stereo_rect_dict, pts_test, ext_coupling, stereo_coupling,
                                   cam1_T_cam2, ext_img_idx,
                                   use_openvx=True,
                                   calib_dir=out_dir,
                                   frame_l=calib_data['left']['raw_imgs'][ix_l],
                                   frame_r=calib_data['right']['raw_imgs'][ix_r],
                                   frame_b=calib_data['back']['raw_imgs'][ix_b])
