from cv_blocks.geometry.pose import Pose
from cv_blocks.geometry.camera import Camera
import os
import yaml
import pickle
import numpy as np
import cv2
import misc
from cv_blocks.misc import aws_download

MODEL_BASE_DIR = os.path.join(os.path.dirname(
    os.path.realpath(__file__)), 'models')


class CartCalib(object):
    '''
    An object holding all cart calibration:
    - extrinsics
    - intrinsics
    - decision boundries
    - maps
    '''

    def __init__(self, config_id, sync=True, preset=None):
        assert preset in (None, 'left_full_res', 'right_full_res')
        self.config_id = config_id
        config_path = os.path.join(
            MODEL_BASE_DIR, config_id, 'cart_config.yaml')
        self.calib_dir = os.path.dirname(config_path)
        self.preset = preset
        self.download_cal_models_if_needed(sync)
        assert os.path.exists(
            config_path), 'cart config file %s not found' % config_path
        self.calib_dir = os.path.dirname(config_path)
        self.conf_data = yaml.safe_load(open(config_path, 'r'))
        # Modify calibration according to preset
        # ======================================
        if self.preset is not None:
            self.set_preset_calib()
        calib_data = pickle.load(
            open(os.path.join(self.calib_dir, 'calib.pkl'), 'rb'))
        self.intrinsic_data = calib_data['intrinsics']
        self.extrinsic_data = calib_data['extrinsics']
        self.stereo_data = calib_data['stereo']
        self.sensors = self.build_config()
        self.load_cart_th_image()
        if 'disparity' not in self.conf_data or 'scale' not in self.conf_data['disparity']:
            self.conf_data['disparity'] = dict(scale=0.25) # default disparity scale
        self.set_image_sizes()
        self.generate_remap_maps()

    def set_image_sizes(self, default_height=480, default_width=640):
        for cam_name, cam_info in self.conf_data['camera_info'].items():
            if 'height' not in cam_info or 'width' not in cam_info:
                cam_info['height'] = default_height
                cam_info['width'] = default_width
    
    def set_preset_calib(self):
        default_size = dict(height=480, width=640)
        full_size = dict(height=960, width=1280)
        if self.preset=='left_full_res':
            for cam_name in self.conf_data['camera_info'].keys():
                self.conf_data['camera_info'][cam_name]['height'] = full_size['height'] if cam_name=='left' else default_size['height']
                self.conf_data['camera_info'][cam_name]['width'] = full_size['width'] if cam_name=='left' else default_size['width']
        elif self.preset=='right_full_res':
            for cam_name in self.conf_data['camera_info'].keys():
                self.conf_data['camera_info'][cam_name]['height'] = full_size['height'] if cam_name=='right' else default_size['height']
                self.conf_data['camera_info'][cam_name]['width'] = full_size['width'] if cam_name=='right' else default_size['width']
        
    def generate_remap_maps(self, left=['full', 'qvga'], right=['qvga']):
        max_scale_dict = None
        for data in self.stereo_data.values():
            if max_scale_dict is None or max_scale_dict['shape'][0] < data['shape'][0]:
                max_scale_dict = data
    
        self.maps_dict = dict(left=dict(), right=dict())
        # Create maps
        for cam_name in ('left', 'right'):
            rot_mat = max_scale_dict['rl'] if cam_name=='left' else max_scale_dict['rr']
            p_mat = max_scale_dict['pl'] if cam_name=='left' else max_scale_dict['pr']
            for size_name in left:
                if size_name=='full':
                    (w, h) = self.conf_data['camera_info'][cam_name]['height'], self.conf_data['camera_info'][cam_name]['width']
                elif size_name=='qvga':
                    (w, h) = (240, 320)
                factor = w / max_scale_dict['shape'][0]
                f_scale = np.array([[factor, 1., factor], [1., factor, factor], [1., 1., 1.]])
                f_scale_col = np.array([[factor], [factor], [1.]])
                K = self.intrinsic_data[cam_name]['K'] * f_scale
                D = self.intrinsic_data[cam_name]['dist_coeffs']
                P = p_mat * np.hstack((f_scale, f_scale_col))
                mapX, mapY = cv2.initUndistortRectifyMap(K, D, rot_mat, P, (w, h), cv2.CV_32FC1)
                self.maps_dict[cam_name][size_name] = dict(x=mapX, y=mapY)
            
        # Overwrite with orig maps - TODO - understand why above is not equivalent
        map1l_full=np.genfromtxt(os.path.join(self.calib_dir, '0p5_remap_table_mapL1.csv'), delimiter=' ', dtype=np.float32)
        map2l_full=np.genfromtxt(os.path.join(self.calib_dir, '0p5_remap_table_mapL2.csv'), delimiter=' ', dtype=np.float32)
        map1l=np.genfromtxt(os.path.join(self.calib_dir, '0p25_remap_table_mapL1.csv'), delimiter=' ', dtype=np.float32)
        map2l=np.genfromtxt(os.path.join(self.calib_dir, '0p25_remap_table_mapL2.csv'), delimiter=' ', dtype=np.float32)
        map1r=np.genfromtxt(os.path.join(self.calib_dir, '0p25_remap_table_mapR1.csv'), delimiter=' ', dtype=np.float32)
        map2r=np.genfromtxt(os.path.join(self.calib_dir, '0p25_remap_table_mapR2.csv'), delimiter=' ', dtype=np.float32)
        if map1l_full.shape==self.maps_dict['left']['full']['x'].shape:
            self.maps_dict['left']['full']['x'] = map1l_full
            self.maps_dict['left']['full']['y'] = map2l_full
        self.maps_dict['left']['qvga']['x'] = map1l
        self.maps_dict['left']['qvga']['y'] = map2l
        self.maps_dict['right']['qvga']['x'] = map1r
        self.maps_dict['right']['qvga']['y'] = map2r

    def download_cal_models_if_needed(self, sync):
        os.makedirs(self.calib_dir, exist_ok=True)
        if sync:
            aws_download.download("calibration_models",
                                  self.calib_dir.split('/')[-1], self.calib_dir)

        files_to_download = ['cart_config.yaml',
                             'cart_disparity_th.png',
                             '0p25_remap_table_mapL1.csv',
                             '0p25_remap_table_mapL2.csv',
                             '0p25_remap_table_mapR1.csv',
                             '0p25_remap_table_mapR2.csv',
                             '0p5_remap_table_mapL1.csv',
                             '0p5_remap_table_mapL2.csv',
                             'calib.pkl']

        for file in files_to_download:
            if not os.path.exists(os.path.join(self.calib_dir, file)):
                raise NameError(
                    "No Stereo calibration files exist, re-download with SW or manually")
        return

    def build_config(self):
        sensors = dict()
        # get pose origin
        pose_origin_name = None
        for _, sensor_data in self.conf_data['camera_config'].items():
            if sensor_data['is_origin']:
                pose_origin_name = sensor_data['intrinsic_name']
                break
        assert pose_origin_name is not None

        for sensor_name, sensor_data in self.conf_data['camera_config'].items():
            # Intrinsics
            cam_int_data = self.intrinsic_data[sensor_data['intrinsic_name']]
            sensor_scale = sensor_data['intrinsic_scale']
            K_scale = np.array([[sensor_scale, 1., sensor_scale],
                                [1., sensor_scale, sensor_scale],
                                [1., 1., 1.]])
            K = cam_int_data['K'] * K_scale
            D = cam_int_data['dist_coeffs']
            # Extrinsic
            if sensor_data['intrinsic_name'] == pose_origin_name:
                cam_pose = Pose()
            else:
                pose_key = '%s_T_%s' % (
                    pose_origin_name, sensor_data['intrinsic_name'])
                cam_pose = self.extrinsic_data[pose_key]
            # Stereo
            Q = None
            if sensor_data['has_stereo']:
                st_calib_scaled = [st for st in self.stereo_data.values()
                                   if st['scale'] == sensor_scale][0]
                Q = st_calib_scaled['q']
            # Init sensor
            sensors[sensor_name] = Camera(cam_pose, K, D=D, Q=Q)

        return sensors

    def load_cart_th_image(self):
        from config import config  # TODO - move away from config
        try:
            disp_path = os.path.join(self.calib_dir, 'cart_disparity_th.png')
            assert os.path.exists(
                disp_path), 'disparity image file %s not found' % disp_path
            self.cart_th_img = cv2.imread(disp_path, -1).astype(np.int) - config.CART_TH_CAL_REDUCTION
            self.cart_th_img = self.cart_th_img.astype(np.uint8)
            self.min_cart_disp = min(self.cart_th_img.min() + config.CART_TH_CAL_REDUCTION, config.MINIMUM_DISP_FOR_CART) 
        except:
            raise('Warning: Failed to load cart disparity th image, running without it!')
    
    def get_plane_line(self, cam_name, disp=None):
        conf_data = self.conf_data['camera_config'][cam_name]
        if 'decision_disparity' in conf_data:
            if disp > conf_data['decision_disparity']['near']:
                return conf_data['decision_lines']['bottom']
            elif conf_data['decision_disparity']['far'] < disp < conf_data['decision_disparity']['near']:
                return conf_data['decision_lines']['middle']
            else:
                return conf_data['decision_lines']['top']
        else:
            return conf_data['decision_lines']['middle']

if __name__ == '__main__':
    calib_dir = os.path.dirname(os.path.realpath(__file__))
    config_path = os.path.join(
        calib_dir, 'models', '80deg_sys', 'cart_config.yaml')
    my_calib = CartCalib(config_path)
